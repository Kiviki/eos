#!/bin/bash

# this script runs all tests without collecting coverage statistics.
# To run all tests with coverage reporting, use `./ci/run_tests.sh` instead.

poetry run pytest
#poetry run pytest database
#poetry run pytest core
#poetry run pytest api
#poetry run pytest legacy_api
#poetry run pytest inventory_importer -Wdefault
# use -Wdefault to not convert warnings to exception in inventory_importer tests as bw2io uses very old dependencies.
