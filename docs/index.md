# Reference

```python
import sys

blah = open('file')
```

- [API](api.md)
- [core](core.md)
- [database](database.md)
- [inventory_importer](inventory_importer.md)
- [legacy API](legacy_api.md)
