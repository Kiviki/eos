stages:
  - test
  - build
  - release
  - deploy
  - database

test:
  image: python:3.11.3-bullseye
  stage: test
  services:
  - name: postgres:14
  - name: rabbitmq:3.9-alpine
  variables:
    GIT_STRATEGY: clone  # default is shallow but we need full clone for black check transition
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432
    POSTGRES_DB: eos
    POSTGRES_USER: eosuser
    POSTGRES_PASSWORD: Nh5UV85MvaDrY  # in ci tests we use the dummy passwords as set in the .env.sample file
    POSTGRES_HOST_AUTH_METHOD: trust
    POETRY_VERSION: 1.4.2
    RABBITMQ_DEFAULT_USER: eosuser
    RABBITMQ_DEFAULT_PASS: vKtLr0DsF9SNpyKeHkOM  # in ci tests we use the dummy passwords as set in the .env.sample file
    RABBITMQ_HOSTNAME: rabbitmq
    RABBITMQ_PORT: 5672
  script:
  - python3 -c "import platform; print(platform.machine())" # for debugging platform based issues
  - pip install "poetry==$POETRY_VERSION"
  - poetry install
  - poetry run isort --check-only .
  - poetry run black --check .
  - poetry run ci/ruff_lint_modified.sh
  - cp .env.sample .env
  - mkdir ./secrets
  - >
    if [ -z "$GOOGLE_SERVICE_ACCOUNT" ]; then
        echo "Fallback to local files as Google Drive service account not available."
    else
        echo $GOOGLE_SERVICE_ACCOUNT | base64 -d > ./secrets/service_account.json
    fi
  - ./ci/run_tests.sh
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    paths:
      - ./ci/junit_merged.xml
      - ./ci/coverage.xml
    reports:
      junit: ./ci/junit_merged.xml
      coverage_report:
        coverage_format: cobertura
        path: ci/coverage.xml

build-image:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --pull --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  only:
    - main
    - tags

release-tagged:
  image: docker:20.10.16
  stage: release
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
    GIT_STRATEGY: none
    AWS_REGION: "eu-central-1"
  before_script:
    - apk add --no-cache python3 py3-pip
    - pip3 install --no-cache-dir awscli
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - echo "pushing to GitLab registry..."
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - echo "Pushing to ECR..."
    - aws ecr get-login-password --region $AWS_REGION |
      docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $IMAGE_REPO:$CI_COMMIT_TAG
    - docker push $IMAGE_REPO:$CI_COMMIT_TAG
  only:
    - tags

release-latest:
  image: docker:20.10.16
  stage: release
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
    GIT_STRATEGY: none
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - main

.deploy-tagged:
  stage: deploy
  image:
    name: alpine/k8s:1.23.15
    entrypoint: [""]
  script:
    - ./ci/deploy.sh
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
  dependencies:
    - release-tagged

deploy-tagged-staging:
  extends: .deploy-tagged
  variables:
    K8S_NAMESPACE: eos-stg
    PG_K8S_SERVICE: postgres-service-staging.default.svc.cluster.local
    PG_SECRET_NAME: postgres-creds-staging
    ENV_PREFIX: "/staging"
  environment:
    name: staging

deploy-tagged-production:
  extends: .deploy-tagged
  variables:
    K8S_NAMESPACE: eos
    PG_K8S_SERVICE: postgres-service.default.svc.cluster.local
    PG_SECRET_NAME: postgres-creds
    ENV_PREFIX: ""
  environment:
    name: production


.db-seed:
  stage: database
  image:
    name: alpine/k8s:1.23.15
    entrypoint: [""]
  script:
    - ./ci/run_reset_db.sh
  rules:
    - if: $CI_COMMIT_TAG
      when: manual

db-seed-staging:
  extends: .db-seed
  variables:
    K8S_NAMESPACE: eos-stg
  environment:
    name: staging
  needs:
    - deploy-tagged-staging

db-seed-production:
  extends: .db-seed
  variables:
    K8S_NAMESPACE: eos
  environment:
    name: production
  needs:
    - deploy-tagged-production


.db-importer-bw:
  stage: database
  image:
    name: alpine/k8s:1.23.15
    entrypoint: [""]
  script:
    - ./ci/run_importer_bw.sh
  rules:
    - if: $CI_COMMIT_TAG
      when: manual

db-importer-bw-staging:
  extends: .db-importer-bw
  variables:
    K8S_NAMESPACE: eos-stg
  environment:
    name: staging
  needs:
    - deploy-tagged-staging

db-importer-bw-production:
  extends: .db-importer-bw
  variables:
    K8S_NAMESPACE: eos
  environment:
    name: production
  needs:
    - deploy-tagged-production
