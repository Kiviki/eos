from os import path

import uvicorn
from structlog import get_logger

logger = get_logger()

base_path = path.dirname(__file__)

if __name__ == "__main__":
    PORT = 8050
    logger.info("starting server on port {}".format(PORT), flush=True)

    uvicorn.run(
        app="legacy_api.app.server:fastapi_app",
        host="0.0.0.0",
        port=PORT,
        reload=True,
        reload_dirs=[
            path.abspath(path.join(base_path, "..", "..", "legacy_api")),
            path.abspath(path.join(base_path, "..", "..", "core")),
            path.abspath(path.join(base_path, "..", "..", "database")),
        ],
        workers=1,
        log_level="debug",
        access_log=False,
        # We set this so that uvicorn does not try to configure logging. we are doing that instead
        log_config=None,
    )
