from legacy_api.converter.common import NodeTypeEnum, convert_batch_item_to_result, sort_batch_into_xids
from legacy_api.dto.legacy.recipe import RecipeResultDto


def convert_batch_to_recipe_result(
    batch: list, full_resource: bool, indicators: bool, recipe_id: str
) -> RecipeResultDto | dict | None:
    batch_by_xid = sort_batch_into_xids(batch)
    batch_item = batch_by_xid.get(recipe_id, {})
    return convert_batch_item_to_recipe_result(batch_item, full_resource, indicators)


def convert_batch_item_to_recipe_result(
    batch_item: dict, full_resource: bool, indicators: bool
) -> RecipeResultDto | dict | None:
    return convert_batch_item_to_result(batch_item, full_resource, indicators, node_type=NodeTypeEnum.recipe)


def convert_get_nodes_to_recipes_result(nodes: list[dict]) -> list[str]:
    recipes_ids = []

    for node in nodes:
        # TODO Remove this check once the concpetual-ingredients are converted to LinkingActivityNode.
        if not (
            node.get("legacy_API_information")
            and node.get("legacy_API_information").get("type") == "conceptual-ingredients"
        ):
            recipes_ids.append(node.get("xid", ""))

    return recipes_ids
