import logging
from enum import Enum
from types import MappingProxyType
from typing import Any, Optional

from legacy_api.dto.legacy.recipe import RecipeResultDto
from legacy_api.dto.legacy.supply import SupplyResultDto

NEW_TO_LEGACY_API_NAME_MAPPING = MappingProxyType(
    {
        "product_name": "names",
        "flow_location": "origin",
        "activity_location": "origin",
        "raw_production": "production",
        "raw_conservation": "conservation",
    }
)

PROPS_TO_MOVE_TO_ROOT_FLOW = (
    "raw_conservation",
    "raw_production",
    "product_name",
    "transport",
)

STRINGS_TO_CONVERT_TO_RAW_NAME = ("raw_production", "raw_conservation", "raw_processing")

PROPS_TO_REROUTE = (
    "ingredients_declaration",
    "transport",
    "flow_location",
    "activity_location",
    "product_name",
    "production_amount",
    "percentage",
    "amount_in_original_source_unit",
    "raw_production",
    "raw_conservation",
    "raw_processing",
)  # Most of these properties can be rerouted to/from legacy API in the same way, and therefore grouped together.


class NodeTypeEnum(str, Enum):
    recipe = "recipe"
    supply = "supply"


def sort_batch_into_xids(batch: list) -> dict:
    batch_by_xid = {}
    for item in batch:
        if item.get("final_root"):
            batch_by_xid[item.get("final_root").get("activity").get("xid")] = item
    return batch_by_xid


def get_final_graph_by_uid(batch_item: dict) -> dict:
    final_graph_by_uid = {}
    if final_graph_from_batch := batch_item.get("final_graph"):
        for node_in_graph in final_graph_from_batch:
            final_graph_by_uid[node_in_graph.get("uid")] = node_in_graph
    return final_graph_by_uid


def extract_source_data_raw(prop_name: str, prop_datum: str | list | dict) -> str | list | dict:
    if isinstance(prop_datum, dict) and prop_datum.get("prop_type"):
        if not prop_datum.get("source_data_raw"):
            logging.warning(f"Could not find source_data_raw for {prop_name}")
        else:
            return prop_datum.get("source_data_raw")
    else:
        return prop_datum


def convert_raw_name_to_str(props: dict) -> None:
    for string_to_convert in STRINGS_TO_CONVERT_TO_RAW_NAME:
        legacy_string_to_convert = NEW_TO_LEGACY_API_NAME_MAPPING.get(string_to_convert, string_to_convert)
        if legacy_string_to_convert in props:
            props[legacy_string_to_convert] = props[legacy_string_to_convert].get("value")


def separate_amount_into_value_and_unit(props: dict) -> None:
    for amount_prop in ("amount_in_original_source_unit", "production_amount"):
        if props.get(amount_prop):
            props["amount"] = props.get(amount_prop).get("value")
            props["unit"] = props.get(amount_prop).get("unit")
            del props[amount_prop]


def convert_location_from_list_to_string(props: dict, location_name: str) -> None:
    if props.get(location_name):
        if isinstance(props.get(location_name), list):
            if len(props[location_name]) > 0:
                logging.warning(
                    f"Multiple location found when converting to legacy DTO {props[location_name]}. "
                    "Taking the first one."
                )
            props[location_name] = props[location_name][0]


def convert_prop_data(
    props: dict[str, Any],
    node: dict[str, str | list | dict],
    parent_flow: Optional[dict[str, str | list | dict]] = None,
    new_to_legacy_api_name_mapping: dict[str, str] = NEW_TO_LEGACY_API_NAME_MAPPING,
    is_supply: bool = False,
) -> dict[str, Any]:
    if props is None:
        props = {}
    for prop in PROPS_TO_REROUTE:
        legacy_prop = new_to_legacy_api_name_mapping.get(prop, prop)
        if prop_data := node.get(prop):
            if isinstance(prop_data, list):
                props[legacy_prop] = [extract_source_data_raw(prop, prop_datum) for prop_datum in prop_data]
            else:
                props[legacy_prop] = extract_source_data_raw(prop, prop_data)
    if parent_flow:
        for prop in PROPS_TO_MOVE_TO_ROOT_FLOW:
            legacy_prop = new_to_legacy_api_name_mapping.get(prop, prop)
            if prop_data := parent_flow.get(prop):
                if isinstance(prop_data, list):
                    props[legacy_prop] = [extract_source_data_raw(prop, prop_datum) for prop_datum in prop_data]
                else:
                    props[legacy_prop] = extract_source_data_raw(prop, prop_data)
    if node.get("nutrient_values"):
        props["nutrient_values"] = extract_source_data_raw("nutrient_values", node.get("nutrient_values"))
    elif parent_flow and parent_flow.get("nutrient_values"):
        props["nutrient_values"] = extract_source_data_raw("nutrient_values", parent_flow.get("nutrient_values"))

    separate_amount_into_value_and_unit(props)
    convert_location_from_list_to_string(props, new_to_legacy_api_name_mapping.get("activity_location", "location"))
    convert_raw_name_to_str(props)
    if "activity_date" in node:
        if is_supply:
            date_name = "supply-date"
        else:
            date_name = "date"

        props[date_name] = node["activity_date"]
    return props


def get_indicators_and_food_unit(parent_flow: dict, indicators: bool) -> tuple[dict, Optional[float]]:
    try:
        food_unit = round(parent_flow["daily_food_unit"]["amount_for_activity_production_amount"], 2)
    except TypeError:
        food_unit = None
    if indicators:
        try:
            vita_score = find_quantity_value(
                parent_flow["vitascore_legacy"]["amount_for_activity_production_amount"],
                "Vita score",
            )
        except TypeError:
            vita_score = None
        try:
            scarce_water_liters = round(
                parent_flow["scarce_water_consumption"]["amount_for_activity_production_amount"], 2
            )
        except TypeError:
            scarce_water_liters = None

        try:
            rainforest_rating = parent_flow["rainforest_critical_products"]["rainforest_rating"]
        except TypeError:
            rainforest_rating = None

        try:
            animal_treatment_rating = parent_flow["animal_products"]["animal_welfare_rating"]
        except TypeError:
            animal_treatment_rating = None
    else:
        vita_score = None
        rainforest_rating = None
        animal_treatment_rating = None
        scarce_water_liters = None

    match rainforest_rating:
        case 3:
            rainforest_label = True
            rainforest_rating_alphabet = "A"
        case 2:
            rainforest_label = False
            rainforest_rating_alphabet = "B"
        case 1:
            rainforest_label = False
            rainforest_rating_alphabet = "E"
        case _:
            rainforest_label = None
            rainforest_rating_alphabet = None

    match animal_treatment_rating:
        case 3:
            animal_treatment_label = True
            animal_treatment_rating_alphabet = "A"
        case 2:
            animal_treatment_label = False
            animal_treatment_rating_alphabet = "B"
        case 1:
            animal_treatment_label = False
            animal_treatment_rating_alphabet = "E"
        case _:
            animal_treatment_label = None
            animal_treatment_rating_alphabet = None

    indicators_dict = {
        "vita_score": vita_score,
        "environment": {
            "rainforest_label": rainforest_label,
            "rainforest_rating": rainforest_rating_alphabet,
            "animal_treatment_label": animal_treatment_label,
            "animal_treatment_rating": animal_treatment_rating_alphabet,
            "scarce_water_liters": scarce_water_liters,
        },
    }
    return indicators_dict, food_unit


def extract_properties_from_ingredients(props_dict: dict, node_by_uid: dict, ingredient_uid: str) -> dict:
    ingredient_item = node_by_uid.get(ingredient_uid, {})
    if ingredient_item:
        if legacy_API_information := ingredient_item.get("legacy_API_information"):
            if legacy_API_information.get("type") == "conceptual-ingredients":
                sub_flow = node_by_uid[ingredient_item.get("sub_node_uids")[0]]

                if nutrient_values := sub_flow.get("nutrient_values"):
                    props_dict["nutrient_values"] = extract_source_data_raw("nutrient_values", nutrient_values)

                if ingredient_item.get("ingredients_declaration"):
                    props_dict["ingredients_declaration"] = ingredient_item.get("ingredients_declaration")[0].get(
                        "value"
                    )
    return props_dict


def find_quantity_value(quantities_dict: dict[str, dict], quantity_term_name: str) -> Optional[float]:
    """Helper function to extract a quantity value from a dictionary of quantities."""
    for _, value in quantities_dict.items():
        if "quantity_term_name" in value and value["quantity_term_name"] == quantity_term_name:
            return value["quantity"]["value"]
    return None


# TODO: deal with link_to_xid
def convert_ingredient_ids_to_legacy(ingredients: list) -> list:
    for ingredient in ingredients:
        # convert back to legacy API:
        if ingredient["uid"]:
            ingredient["id"] = ingredient["uid"]
            del ingredient["uid"]
        if ingredient["xid"]:
            ingredient["id"] = ingredient["xid"]
            del ingredient["xid"]
    return ingredients


def convert_ingredient_declarations(ingredients: list) -> list:
    for ingredient in ingredients:
        try:
            ingredient_declarations = ingredient.get("ingredients-declaration", [{}])

            ingredients_declaration = ""

            # TODO: rework this approach once we implement working with multiple languages?
            # for now, we prioritize picking the first german declaration in the list
            for declaration in ingredient_declarations:
                if declaration.get("language") == "de":
                    ingredients_declaration = declaration.get("value")
                    break

            # if we don't have any german declaration, then pick the first one
            # as it is supposed to be the primary one
            if not ingredients_declaration:
                ingredients_declaration = ingredient_declarations[0].get("value")

            ingredient["ingredients-declaration"] = ingredients_declaration

        except TypeError:
            ingredient["ingredients-declaration"] = ""

    return ingredients


def convert_batch_item_to_result(
    batch_item: dict, full_resource: bool, indicators: bool, node_type: NodeTypeEnum
) -> RecipeResultDto | SupplyResultDto | dict | None:
    final_root = batch_item.get("final_root")

    if final_root:
        node = final_root.get("activity")

        if node_type == NodeTypeEnum.recipe:
            servings = node.get("raw_input", {}).get("recipe_portions", None)
            if servings is None:
                servings = node.get("raw_input", {}).get("servings_deprecated", None)
            if servings is None:
                servings = 1
        else:
            servings = 1  # For supplies, we do not scale the CO2 values.

        parent_flow = final_root.get("flow")
        sub_flows = final_root.get("sub_flows", [])
        ingredients_out = []
        if full_resource:
            for sub_flow in sub_flows:
                try:
                    co2_value_in_kg = find_quantity_value(
                        sub_flow["impact_assessment"]["amount_for_activity_production_amount"],
                        "IPCC 2013 climate change GWP 100a",
                    )
                    # in legacy api co2-value needs to be returned in
                    # "[g CO₂equivalent / serving (given or normalized)]"
                    # the new api uses impact assessment unit 'EOS_kg-co2-eq',
                    # so we convert kg to gram and normalize by serving:
                    co2_value = int(co2_value_in_kg * 1000 / servings)
                except TypeError:
                    co2_value = None

                sub_flow_indicator_dict, sub_flow_food_unit = get_indicators_and_food_unit(sub_flow, indicators)

                sub_flow_props = sub_flow.get("raw_input", {})
                sub_flow_props = convert_prop_data(sub_flow_props, sub_flow)

                if sub_flow.get("link_to_sub_node"):
                    ingredient_id = sub_flow.get("link_to_sub_node").get("xid")
                elif sub_flow.get("xid"):
                    ingredient_id = sub_flow.get("xid")
                else:
                    ingredient_id = sub_flow.get("uid")

                final_graph_by_uid = get_final_graph_by_uid(batch_item)
                if final_graph_by_uid and sub_flow.get("link_to_sub_node"):
                    try:
                        sub_node_of_sub_flow = final_graph_by_uid[
                            final_graph_by_uid[sub_flow.get("uid")]["sub_node_uids"][0]
                        ]
                        sub_flow_props = extract_properties_from_ingredients(
                            sub_flow_props, final_graph_by_uid, sub_node_of_sub_flow.get("uid")
                        )
                    except KeyError:
                        continue

                ingredient_type = sub_flow.get("legacy_API_information").get("type")
                ingredients_out.append(
                    {
                        "id": ingredient_id,
                        "co2-value": co2_value,
                        "foodUnit": sub_flow_food_unit,
                        **sub_flow_props,
                        "type": ingredient_type,
                    }
                )
                if indicators:
                    ingredients_out[-1]["indicators"] = sub_flow_indicator_dict

        matrix_gfm_error = node.get("matrix_gfm_error")
        if matrix_gfm_error:
            info_text = "Failed calculating co2. Probably missing matching ingredient."
        else:
            info_text = None

        try:
            co2_value_in_kg = find_quantity_value(
                node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
            )
            # in legacy api co2-value needs to be returned in "[g CO₂e(quivalent) / serving (given or normalized)]"
            # the new api uses impact assessment unit 'EOS_kg-co2-eq',
            # so we convert kg to gram and normalize by serving:
            co2_value = int(co2_value_in_kg * 1000 / servings)
        except TypeError:
            co2_value = None

        indicator_dict, food_unit = get_indicators_and_food_unit(parent_flow, indicators)

        if node.get("raw_input"):
            recipe_or_supply_props = node.get("raw_input")
            keys_to_delete = ["id", "kitchen_id"]
            for key in keys_to_delete:
                if key in recipe_or_supply_props:
                    del recipe_or_supply_props[key]
        else:
            recipe_or_supply_props = {}

        if node_type == NodeTypeEnum.recipe:
            # Unlike products, supplies, and ingredients, recipes have a location field instead of an origin field
            new_to_legacy_api_name_mapping = {
                key: value if key not in ("activity_location", "flow_location") else "location"
                for key, value in NEW_TO_LEGACY_API_NAME_MAPPING.items()
            }
        else:
            new_to_legacy_api_name_mapping = NEW_TO_LEGACY_API_NAME_MAPPING

        recipe_or_supply_props = convert_prop_data(
            recipe_or_supply_props,
            node,
            parent_flow=parent_flow,
            new_to_legacy_api_name_mapping=new_to_legacy_api_name_mapping,
            is_supply=(node_type == NodeTypeEnum.supply),
        )

        # we need to return the new and old field names, because javaland is doing the same:
        if node_type == NodeTypeEnum.recipe:
            if recipe_or_supply_props["recipe_portions"] is None:
                recipe_or_supply_props["recipe_portions"] = servings
            if recipe_or_supply_props["servings_deprecated"] is None:
                recipe_or_supply_props["servings_deprecated"] = servings

        recipe_or_supply_result_dict = {
            "id": node.get("xid"),
            "kitchen_id": node.get("access_group_xid"),
            "ingredients": ingredients_out,
            "info_text": info_text,
            "co2_value": co2_value,
            "food_unit": food_unit,
            **recipe_or_supply_props,
        }
        if indicators:
            recipe_or_supply_result_dict["indicators"] = indicator_dict

        if node_type == NodeTypeEnum.recipe:
            recipe_or_supply_result_dto = RecipeResultDto.model_validate(recipe_or_supply_result_dict)
        else:
            recipe_or_supply_result_dto = SupplyResultDto.model_validate(recipe_or_supply_result_dict)

        return recipe_or_supply_result_dto
    else:
        if batch_item:
            return batch_item
        else:
            return None
