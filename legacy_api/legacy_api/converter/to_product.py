from legacy_api.converter.common import (
    convert_ingredient_declarations,
    convert_prop_data,
    find_quantity_value,
    get_indicators_and_food_unit,
)
from legacy_api.dto.legacy.product import ProductResultDto


def convert_node_to_product_result(recipe: dict, full_resource: bool, indicators: bool) -> ProductResultDto:
    node = recipe.get("activity")
    parent_flow = recipe.get("flow")
    sub_flows = recipe.get("sub_flows", [])
    ingredients_declaration_as_single_ingredient = convert_ingredient_declarations(
        [
            {"ingredients-declaration": node.get("ingredients_declaration", [])},
        ],
    )

    converted_ingredients_declaration = ingredients_declaration_as_single_ingredient[0].get(
        "ingredients-declaration",
    )

    ingredients_out = []
    if full_resource:
        for sub_flow in sub_flows:
            sub_flow_props = sub_flow.get("raw_input", {})
            sub_flow_props = convert_prop_data(sub_flow_props, sub_flow)

            try:
                co2_value_in_kg = find_quantity_value(
                    sub_flow["impact_assessment"]["amount_for_activity_production_amount"],
                    "IPCC 2013 climate change GWP 100a",
                )
                # the new api uses impact assessment unit 'EOS_kg-co2-eq',
                # so we convert kg to gram and normalize by serving:
                co2_value = int(co2_value_in_kg * 1000)
            except TypeError:
                co2_value = None

            sub_flow_indicator_dict, sub_flow_food_unit = get_indicators_and_food_unit(sub_flow, indicators)

            if sub_flow.get("xid"):
                ingredient_id = sub_flow.get("xid")
            else:
                ingredient_id = sub_flow.get("uid")
            ingredients_out.append(
                {"id": ingredient_id, "co2-value": co2_value, "foodUnit": sub_flow_food_unit, **sub_flow_props}
            )
            if indicators:
                ingredients_out[-1]["indicators"] = sub_flow_indicator_dict

    matrix_gfm_error = node.get("matrix_gfm_error")
    if matrix_gfm_error:
        info_text = "Failed calculating co2. Probably missing matching ingredient."
    else:
        info_text = None

    try:
        co2_value_in_kg = find_quantity_value(
            node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
        )
        # the new api uses impact assessment unit 'EOS_kg-co2-eq',
        # so we convert kg to gram and normalize by serving:
        co2_value = int(co2_value_in_kg * 1000)
    except TypeError:
        co2_value = None

    indicator_dict, food_unit = get_indicators_and_food_unit(parent_flow, indicators)

    if node.get("raw_input"):
        recipe_props = node.get("raw_input")
        recipe_props["recipe_portions"] = recipe_props.get("servings_deprecated")
        keys_to_delete = ["id", "kitchen_id", "servings_deprecated"]
        for key in keys_to_delete:
            if key in recipe_props:
                del recipe_props[key]
    else:
        recipe_props = {}

    recipe_props = convert_prop_data(recipe_props, node, parent_flow)
    recipe_props["ingredients_declaration"] = converted_ingredients_declaration

    recipe_result_dict = {
        "id": node.get("xid"),
        "kitchen_id": node.get("access_group_xid"),
        "ingredients": ingredients_out,
        "info_text": info_text,
        "co2_value": co2_value,
        "food_unit": food_unit,
        **recipe_props,
    }
    if indicators:
        recipe_result_dict["indicators"] = indicator_dict

    recipe_result_dto = ProductResultDto.model_validate(recipe_result_dict)

    return recipe_result_dto
