from legacy_api.converter.common import NodeTypeEnum, convert_batch_item_to_result, sort_batch_into_xids
from legacy_api.dto.legacy.supply import SupplyResultDto


def convert_batch_to_supply_result(
    batch: list, full_resource: bool, indicators: bool, supply_id: str
) -> SupplyResultDto | dict | None:
    batch_by_xid = sort_batch_into_xids(batch)
    batch_item = batch_by_xid.get(supply_id, {})
    return convert_batch_item_to_supply_result(batch_item, full_resource, indicators)


def convert_batch_item_to_supply_result(
    batch_item: dict, full_resource: bool, indicators: bool
) -> SupplyResultDto | dict | None:
    return convert_batch_item_to_result(batch_item, full_resource, indicators, node_type=NodeTypeEnum.supply)
