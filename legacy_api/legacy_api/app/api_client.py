"Minimalistic REST API client."
import httpx
from structlog import get_logger

logger = get_logger()


class ApiClient:
    "Wrapper to provide a REST API client."

    def __init__(self):
        self.session = None

    async def connect(self) -> None:
        "Start a new client session."
        self.session = httpx.AsyncClient(timeout=60 * 60)

    async def close(self) -> None:
        "Close the connection."
        await self.session.aclose()
