import json
import uuid
from typing import Annotated, Optional, Union

from fastapi import APIRouter, Depends, Query, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.common import sort_batch_into_xids
from legacy_api.converter.to_batch_input_dto import (
    build_warning_message_for_batch_item,
    legacy_to_new_batch_converter,
    to_serialized_batch_input_dto,
)
from legacy_api.converter.to_recipe import convert_batch_item_to_recipe_result, convert_batch_to_recipe_result
from legacy_api.dto.legacy.recipe import (
    BatchItemResultDto,
    BatchItemResultDtoWithRecipe,
    BatchRecipeDto,
    RecipeResultDto,
    WrappedRecipeDto,
    WrappedRecipeResultDto,
)

logger = get_logger()
settings = Settings()
recipe_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


async def get_recipe_by_id_common(
    recipe_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> WrappedRecipeResultDto | str:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": recipe_id,
                },
            },
            "return_final_graph": True,
        }
    ]
    if kitchen_id is not None:
        batch_to_get[0]["input_root"]["existing_root"]["access_group_xid"] = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    try:
        result = api_response.json()

        recipe = convert_batch_to_recipe_result(result.get("batch"), full_resource, indicators, recipe_id)
        if isinstance(recipe, RecipeResultDto):
            return WrappedRecipeResultDto(recipe=recipe)
        else:
            if recipe:
                response.status_code = recipe["statuscode"]
            else:
                response.status_code = result.get("batch")[0]["statuscode"]
            return json.dumps({"api_response": api_response.text})
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@recipe_router.get("/{recipe_id}")
async def get_recipe_by_id(
    recipe_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called get_kitchen_recipe_by_id with credentials: {credentials}")
    return await get_recipe_by_id_common(recipe_id, response, full_resource, indicators, credentials)


async def create_new_recipe_common(
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> WrappedRecipeResultDto | str:
    if not recipe.recipe.id:
        recipe.recipe.id = str(uuid.uuid4())  # Generate ID for later identification from batch.

    if kitchen_id is not None:
        recipe.recipe.kitchen_id = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(recipe.recipe.model_dump()), transient),
    )
    try:
        result = api_response.json()
        batch_by_xid = sort_batch_into_xids(result.get("batch"))
        batch_item = batch_by_xid.get(recipe.recipe.id, {})
        recipe = convert_batch_item_to_recipe_result(batch_item, full_resource, indicators)
        if isinstance(recipe, RecipeResultDto):
            response_model = WrappedRecipeResultDto(
                statuscode=batch_item["statuscode"],
                message=batch_item["message"],
                request_id=batch_item["request_id"],
                recipe_id=recipe.id,
                recipe=recipe,
            )
            return response_model
        else:
            if recipe:
                response.status_code = recipe["statuscode"]
            else:
                response.status_code = result.get("batch")[0]["statuscode"]
            return json.dumps({"api_response": api_response.text})
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@recipe_router.post(
    "/", include_in_schema=False, response_model=Union[WrappedRecipeResultDto, str], response_model_exclude_none=True
)
@recipe_router.post("", response_model=Union[WrappedRecipeResultDto, str], response_model_exclude_none=True)
async def create_new_recipe(
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called create_new_recipe with credentials: {credentials}")
    return await create_new_recipe_common(recipe, response, full_resource, transient, indicators, credentials)


async def put_recipe_common(
    recipe_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> WrappedRecipeResultDto | str:
    recipe.recipe.id = recipe_id
    if kitchen_id is not None:
        recipe.recipe.kitchen_id = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(recipe.recipe.model_dump()), transient),
    )
    try:
        result = api_response.json()
        recipe = convert_batch_to_recipe_result(result.get("batch"), full_resource, indicators, recipe_id)
        if isinstance(recipe, RecipeResultDto):
            return WrappedRecipeResultDto(recipe=recipe)
        else:
            if recipe:
                response.status_code = recipe["statuscode"]
            else:
                response.status_code = result.get("batch")[0]["statuscode"]
            return json.dumps({"api_response": api_response.text})
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@recipe_router.put("/{recipe_id}")
async def put_recipe(
    recipe_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called put_recipe with credentials: {credentials}")
    return await put_recipe_common(recipe_id, recipe, response, full_resource, transient, indicators, credentials)


async def delete_recipe_common(
    recipe_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> None:
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": recipe_id,
                }
            }
        }
    ]
    if kitchen_id is not None:
        batch_to_delete[0]["nodes_dto"]["existing_node"]["access_group_xid"] = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status_code)

    try:
        result = api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status_code


@recipe_router.delete("/{recipe_id}", status_code=204, response_class=Response)
async def delete_recipe(
    recipe_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    logger.info(f"called delete_recipe with credentials: {credentials}")
    await delete_recipe_common(recipe_id, response, credentials)


@recipe_router.post("/batch")
async def post_batch_recipes(
    batch: list[BatchRecipeDto],
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> list[BatchItemResultDtoWithRecipe | BatchItemResultDto | str]:
    logger.info(f"called create_new_recipe with credentials: {credentials}")

    json_batch, auxiliary_batch_info = legacy_to_new_batch_converter(
        [batch_item.model_dump() for batch_item in batch], "recipe", transient
    )
    response_batch = []
    for invalid_recipe_id, invalid_request_ids in auxiliary_batch_info.invalid_nodes.items():
        for invalid_request_id in invalid_request_ids:
            warning_message = build_warning_message_for_batch_item(
                invalid_recipe_id, invalid_request_id, auxiliary_batch_info, "recipe"
            )
            base_message = f"Recipe was NOT saved: The received recipe with id {invalid_recipe_id}"
            " references a missing subrecipe or its structure is cyclic."
            if warning_message:
                message = f"{base_message}\n{warning_message}"
            else:
                message = base_message

            response_batch.append(
                {
                    "statuscode": status.HTTP_400_BAD_REQUEST,
                    "message": message,
                    "request-id": invalid_request_id,
                    "recipe-id": invalid_recipe_id,
                }
            )
    if auxiliary_batch_info.invalid_nodes:
        response.status_code = status.HTTP_400_BAD_REQUEST

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json=json_batch,
    )
    try:
        result = api_response.json()
    except Exception:
        result = None
        for valid_recipe_id, valid_request_ids in auxiliary_batch_info.valid_nodes.items():
            for valid_request_id in valid_request_ids:
                response_batch.append(
                    {
                        "statuscode": api_response.status_code,
                        "request-id": valid_request_id,
                        "recipe-id": valid_recipe_id,
                        "message": build_warning_message_for_batch_item(
                            valid_recipe_id, valid_request_id, auxiliary_batch_info, "recipe"
                        ),
                    }
                )

    if result:
        batch_by_xid = sort_batch_into_xids(result.get("batch"))
        for recipe_id, request_ids in auxiliary_batch_info.valid_nodes.items():
            try:
                batch_item = batch_by_xid.get(recipe_id, {})
                recipe = convert_batch_item_to_recipe_result(batch_item, full_resource, indicators)
                if isinstance(recipe, RecipeResultDto):
                    for request_id in request_ids:
                        response_batch.append(
                            {
                                "recipe": recipe,
                                "statuscode": status.HTTP_200_OK,
                                "recipe-id": recipe_id,
                                "request-id": request_id,
                                "message": build_warning_message_for_batch_item(
                                    recipe_id, request_id, auxiliary_batch_info, "recipe"
                                ),
                            }
                        )
                else:
                    if recipe:
                        status_code = recipe["statuscode"]
                    else:
                        status_code = result.get("batch")[0]["statuscode"]
                    for request_id in request_ids:
                        response_batch.append(
                            {
                                "statuscode": status_code,
                                "request-id": request_id,
                                "recipe-id": recipe_id,
                                "message": build_warning_message_for_batch_item(
                                    recipe_id, request_id, auxiliary_batch_info, "recipe"
                                ),
                            }
                        )
            except Exception:
                for request_id in request_ids:
                    response_batch.append(
                        {
                            "statuscode": api_response.status_code,
                            "request-id": request_id,
                            "recipe-id": recipe_id,
                            "message": build_warning_message_for_batch_item(
                                recipe_id, request_id, auxiliary_batch_info, "recipe"
                            ),
                        }
                    )

    # Sort the response according to request-id. Send the item to the last if request-id does not exist.
    # The reqeust-id should always exist if the rest of the code runs fine.
    return sorted(response_batch, key=lambda x: x.get("request-id", float("inf")))
