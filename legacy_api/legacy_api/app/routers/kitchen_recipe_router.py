import json
from typing import Annotated, Union

from fastapi import APIRouter, Depends, Query, Response
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.routers.recipe_router import (
    create_new_recipe_common,
    delete_recipe_common,
    get_recipe_by_id_common,
    put_recipe_common,
)
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_recipe import convert_get_nodes_to_recipes_result
from legacy_api.dto.legacy.recipe import GetRecipesResultDto, WrappedRecipeDto, WrappedRecipeResultDto

logger = get_logger()
settings = Settings()
kitchen_recipe_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@kitchen_recipe_router.get("/{recipe_id}")
async def get_kitchen_recipe_by_id(
    kitchen_id: str,
    recipe_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called get_kitchen_recipe_by_id with credentials: {credentials}")
    return await get_recipe_by_id_common(
        recipe_id, response, full_resource, indicators, credentials, kitchen_id=kitchen_id
    )


@kitchen_recipe_router.get("/", include_in_schema=False)
@kitchen_recipe_router.get("")
async def get_all_kitchen_recipes(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetRecipesResultDto | str:
    logger.info(f"called get_all_kitchen_recipes with credentials: {credentials}")

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/nodes/get_nodes?node_type=FoodProcessingActivityNode",
        headers={"Authorization": f"{credentials}"},
        json={"access_group": {"xid": kitchen_id}},
    )
    try:
        nodes = api_response.json()
    except Exception:
        nodes = None

    if nodes is not None:
        return GetRecipesResultDto(recipes=convert_get_nodes_to_recipes_result(nodes))
    else:
        response.status_code = api_response.status_code

        return json.dumps({"api_response": api_response.text})


@kitchen_recipe_router.post(
    "/", include_in_schema=False, response_model=Union[WrappedRecipeResultDto, str], response_model_exclude_none=True
)
@kitchen_recipe_router.post("", response_model=Union[WrappedRecipeResultDto, str], response_model_exclude_none=True)
async def create_new_kitchen_recipe(
    kitchen_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called create_new_kitchen_recipe with credentials: {credentials}")
    return await create_new_recipe_common(
        recipe, response, full_resource, transient, indicators, credentials, kitchen_id=kitchen_id
    )


@kitchen_recipe_router.put("/{recipe_id}")
async def put_kitchen_recipe(
    kitchen_id: str,
    recipe_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedRecipeResultDto | str:
    logger.info(f"called put_kitchen_recipe with credentials: {credentials}")
    return await put_recipe_common(
        recipe_id, recipe, response, full_resource, transient, indicators, credentials, kitchen_id=kitchen_id
    )


@kitchen_recipe_router.delete("/{recipe_id}", status_code=204, response_class=Response)
async def delete_kitchen_recipe(
    kitchen_id: str, recipe_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    logger.info(f"called delete_kitchen_recipe with credentials: {credentials}")
    await delete_recipe_common(recipe_id, response, credentials, kitchen_id=kitchen_id)
