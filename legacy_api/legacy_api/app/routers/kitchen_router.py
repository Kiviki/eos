import json
from typing import Union

from fastapi import APIRouter, Depends, Request, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_kitchen_ids_list import convert_access_group_ids_to_kitchen_ids
from legacy_api.dto.legacy.kitchen import GetKitchensResultDto, WrappedKitchenDto

logger = get_logger()
settings = Settings()
kitchen_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@kitchen_router.post(
    "/", include_in_schema=False, response_model=Union[WrappedKitchenDto, dict], response_model_exclude_none=True
)
@kitchen_router.post("", response_model=Union[WrappedKitchenDto, dict], response_model_exclude_none=True)
async def create_new_kitchen(
    kitchen: WrappedKitchenDto,
    response: Response,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedKitchenDto | dict:
    logger.info(f"called create_new_kitchen with credentials: {credentials}")

    kitchen_data = kitchen.model_dump().get("kitchen")
    kitchen_data["type"] = "kitchen"

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/access-groups/upsert_access_group?legacy_api_request=true",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": kitchen_data,
        },
    )
    try:
        result = api_response.json()
    except:
        result = {}

    kitchen = result.get("access_group")

    if kitchen:
        response.status_code = 201
        response.headers[
            "location"
        ] = f"{str(request.base_url).rstrip('/')}{settings.BASE_PATH}/kitchens/{kitchen['xid']}"
        return WrappedKitchenDto(kitchen=kitchen)
    else:
        response.status_code = api_response.status_code
        return {"api_response": api_response.text}


@kitchen_router.get("/", include_in_schema=False)
@kitchen_router.get("")
async def get_all_kitchens(
    response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetKitchensResultDto | str:
    logger.info(f"called get_all_kitchens with credentials: {credentials}")

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/access-groups/get_sub_access_groups?legacy_api_request=true",
        headers={"Authorization": f"{credentials}"},
    )

    try:
        result = api_response.json()
    except:
        result = {}

    kitchens = result

    if kitchens is not None:
        return GetKitchensResultDto(kitchens=convert_access_group_ids_to_kitchen_ids(kitchens))
    else:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@kitchen_router.get("/{kitchen_id}", response_model=Union[WrappedKitchenDto, str], response_model_exclude_none=True)
async def get_kitchen_by_id(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> WrappedKitchenDto | str:
    logger.info(f"called get_kitchen_by_id with credentials: {credentials}")

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/access-groups/get_access_group_by_id",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": {
                "xid": kitchen_id,
            },
        },
    )
    try:
        result = api_response.json()
    except:
        result = {}

    kitchen = result.get("access_group")

    response.status_code = api_response.status_code

    if kitchen:
        return WrappedKitchenDto(kitchen=kitchen)
    else:
        return "Kitchen not found"


@kitchen_router.put("/{kitchen_id}", response_model=Union[WrappedKitchenDto, dict], response_model_exclude_none=True)
async def put_kitchen(
    kitchen_id: str,
    kitchen: WrappedKitchenDto,
    response: Response,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedKitchenDto | dict:
    logger.info(f"called put_kitchen with credentials: {credentials}")

    kitchen_data = kitchen.model_dump().get("kitchen")
    kitchen_data["type"] = "kitchen"
    kitchen_data["xid"] = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/access-groups/upsert_access_group?legacy_api_request=true",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": kitchen_data,
        },
    )
    try:
        result = api_response.json()
    except:
        result = {}

    kitchen = result.get("access_group")

    response.status_code = api_response.status_code

    if kitchen:
        response.headers[
            "location"
        ] = f"{str(request.base_url).rstrip('/')}{settings.BASE_PATH}/kitchens/{kitchen['xid']}"
        return WrappedKitchenDto(kitchen=kitchen)
    else:
        return {"api_response": api_response.text}


@kitchen_router.delete("/{kitchen_id}", status_code=204, response_class=Response)
async def delete_kitchen(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    logger.info(f"called delete_kitchen with credentials: {credentials}")

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/access-groups/delete_access_group",
        headers={"Authorization": f"{credentials}"},
        json={"access_group": {"xid": kitchen_id}},
    )
    if api_response.status_code == status.HTTP_200_OK:
        response.status_code = status.HTTP_204_NO_CONTENT
    else:
        response.status_code = api_response.status_code
