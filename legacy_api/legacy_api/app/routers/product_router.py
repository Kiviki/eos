import json
import uuid

from fastapi import APIRouter, Depends, Response
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_batch_input_dto import to_serialized_batch_input_dto
from legacy_api.converter.to_product import convert_node_to_product_result
from legacy_api.dto.legacy.product import WrappedProductDto, WrappedProductResultDto

logger = get_logger()
settings = Settings()
product_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@product_router.get("/{product_id}")
async def get_product_by_id(
    product_id: str,
    response: Response,
    full_resource: bool = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> WrappedProductResultDto | str:
    logger.info(f"called get_product_by_id with credentials: {credentials}")

    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": product_id,
                },
            },
        }
    ]

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    try:
        result = api_response.json()
        batch_item = result.get("batch")[0]
        product = batch_item.get("final_root")
        if product:
            return WrappedProductResultDto(product=convert_node_to_product_result(product, full_resource, indicators))
        else:
            response.status_code = batch_item["statuscode"]
            return json.dumps({"api_response": api_response.text})
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


# TODO: deprecate this endpoint since it's not mentioned in Apiary API docs?
@product_router.post("/", deprecated=True)
async def create_new_product(
    product: WrappedProductDto,
    response: Response,
    full_resource: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> WrappedProductResultDto | str:
    logger.info(f"called create_new_product with credentials: {credentials}")

    wrapped_calculation = to_serialized_batch_input_dto(dict(product.product.model_dump())).get("batch")[-1]
    wrapped_node_json = wrapped_calculation.get("input_root").get("activity")  # TODO Decide what is needed here.
    wrapped_node_json["xid"] = str(uuid.uuid4())

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/group-xid/",  # FIXME: This endpoint does not exist anymore.
        headers={"Authorization": f"{credentials}", "content-type": "application/json"},
        content=wrapped_node_json,
    )
    try:
        result = api_response.json()
    except Exception:
        result = {}

    node = result.get("node")

    if node:
        return WrappedProductResultDto(product=convert_node_to_product_result(node, full_resource, indicators))
    else:
        response.status_code = api_response.status_code

        return json.dumps({"api_response": api_response.text})


@product_router.put("/{product_id}")
async def put_product(
    product_id: str,
    product: WrappedProductDto,
    response: Response,
    transient: bool = False,
    full_resource: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> WrappedProductDto | str:
    logger.info(f"called put_product with credentials: {credentials}")

    product.product.id = product_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(product.product.model_dump()), transient),
    )
    try:
        result = api_response.json()
        batch_item = result.get("batch")[0]
        product = batch_item.get("final_root")
        if product:
            return WrappedProductDto(product=convert_node_to_product_result(product, full_resource, indicators))
        else:
            response.status_code = batch_item["statuscode"]
            return json.dumps({"api_response": api_response.text})
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


# TODO: there is no such endpoint in API documentation, should it stay just in case?
@product_router.delete("/{product_id}", status_code=204, response_class=Response)
async def delete_product(
    product_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)  # noqa: B008
) -> None:
    logger.info(f"called delete_product with credentials: {credentials}")

    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": product_id,
                }
            }
        }
    ]

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status_code)

    try:
        result = api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status_code
