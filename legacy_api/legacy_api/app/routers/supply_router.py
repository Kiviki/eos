import copy
import json
import uuid
from typing import Annotated, Optional

import httpx
from fastapi import APIRouter, Depends, Query, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from core.domain.nodes import SupplySheetActivityNode
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_batch_input_dto import (
    build_warning_message_for_batch_item,
    legacy_to_new_batch_converter,
    to_serialized_batch_input_dto,
)
from legacy_api.converter.to_supply import (
    convert_batch_item_to_supply_result,
    convert_batch_to_supply_result,
    sort_batch_into_xids,
)
from legacy_api.dto.legacy.supply import (
    BatchItemResultDtoWithSupply,
    BatchSupplyDto,
    GetSuppliesResultDto,
    PutOrPostSupplyResultDto,
    SupplyResultDto,
    WrappedSupplyDto,
    WrappedSupplyResultDto,
)

logger = get_logger()
settings = Settings()
supply_router = APIRouter()
kitchen_supply_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


def get_result_from_supply_batch_item_or_id(
    api_response: httpx.Response,
    response: Response,
    supply_batch_item_or_id: str | dict | None,
    indicators: bool,
    full_resource: bool = True,
) -> WrappedSupplyResultDto | SupplyResultDto | str:
    result = api_response.json()
    if not supply_batch_item_or_id:
        response.status_code = result.get("batch")[0]["statuscode"]
        return json.dumps({"api_response": api_response.text})

    if isinstance(supply_batch_item_or_id, str):
        supply = convert_batch_to_supply_result(result.get("batch"), full_resource, indicators, supply_batch_item_or_id)
        return_wrapped = True
    else:
        supply = convert_batch_item_to_supply_result(supply_batch_item_or_id, full_resource, indicators)
        return_wrapped = False

    if isinstance(supply, SupplyResultDto):
        if return_wrapped:
            return WrappedSupplyResultDto(supply=supply)
        else:
            return supply
    else:
        if supply:
            response.status_code = supply["statuscode"]
        else:
            response.status_code = result.get("batch")[0]["statuscode"]
        return json.dumps({"api_response": api_response.text})


async def get_supply_by_id_common(
    supply_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> WrappedSupplyResultDto | str:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": supply_id,
                },
            },
            "return_final_graph": True,
        }
    ]
    if kitchen_id is not None:
        batch_to_get[0]["input_root"]["existing_root"]["access_group_xid"] = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    try:
        return get_result_from_supply_batch_item_or_id(
            api_response, response, supply_id, indicators, full_resource=full_resource
        )
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@supply_router.get("/{supply_id}")
async def get_supply_by_id(
    supply_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedSupplyResultDto | str:
    logger.info(f"called get_supply_by_id with credentials: {credentials}")
    return await get_supply_by_id_common(supply_id, response, full_resource, indicators, credentials)


@kitchen_supply_router.get("/{supply_id}")
async def get_kitchen_supply_by_id(
    kitchen_id: str,
    supply_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedSupplyResultDto | str:
    logger.info(f"called get_kitchen_supply_by_id with credentials: {credentials}")
    return await get_supply_by_id_common(
        supply_id, response, full_resource, indicators, credentials, kitchen_id=kitchen_id
    )


@kitchen_supply_router.get("/", include_in_schema=False)
@kitchen_supply_router.get("")
async def get_all_kitchen_supplies(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetSuppliesResultDto | str:
    logger.info(f"called get_all_supplies with credentials: {credentials}")

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/nodes/get_nodes?node_type=SupplySheetActivityNode",
        headers={"Authorization": f"{credentials}"},
        json={"access_group": {"xid": kitchen_id}},
    )
    try:
        nodes = api_response.json()
    except Exception:
        nodes = None

    if nodes is not None:
        return GetSuppliesResultDto(supplies=[node.get("xid", "") for node in nodes])
    else:
        response.status_code = api_response.status_code

        return json.dumps({"api_response": api_response.text})


def put_or_post_supply_result_from_api_response(
    api_response: httpx.Response, supply_id: str, response: Response
) -> PutOrPostSupplyResultDto | str:
    result = api_response.json()
    correct_supply = None
    for batch_item in result["batch"]:
        if batch_item.get("input_root", {}).get("activity", {}).get("xid", "") == supply_id:
            correct_supply = batch_item

    if correct_supply:
        result_dict = {
            "statuscode": correct_supply["statuscode"],
            "message": " ".join(correct_supply["data_errors"]),
            "supply-id": supply_id,
        }
        return PutOrPostSupplyResultDto(**result_dict)
    else:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


async def create_new_supply_common(
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> BatchItemResultDtoWithSupply | PutOrPostSupplyResultDto | str:
    if not supply.supply.id:
        supply.supply.id = str(uuid.uuid4())  # Generate ID for later identification from batch.

    if kitchen_id is not None:
        supply.supply.kitchen_id = kitchen_id

    if full_resource:
        url = f"{settings.api_v2_url}/calculation/graphs"
    else:
        url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True"

    api_response = await api_client.session.post(
        url,
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(
            dict(supply.supply.model_dump()), transient, node_type=SupplySheetActivityNode.__name__
        ),
    )
    try:
        if full_resource:
            batch_by_xid = sort_batch_into_xids(api_response.json().get("batch"))
            batch_item = batch_by_xid.get(supply.supply.id, {})
            supply_result = get_result_from_supply_batch_item_or_id(
                api_response, response, batch_item, indicators, full_resource
            )
            return BatchItemResultDtoWithSupply(
                **{
                    "statuscode": batch_item["statuscode"],
                    "message": batch_item["message"],
                    "request-id": batch_item["request_id"],
                    "supply-id": supply_result.id,
                    "supply": supply_result,
                }
            )
        else:
            return put_or_post_supply_result_from_api_response(api_response, supply.supply.id, response)
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@supply_router.post("/", include_in_schema=False, response_model_exclude_none=True)
@supply_router.post("", response_model_exclude_none=True)
async def create_new_supply(
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> PutOrPostSupplyResultDto | BatchItemResultDtoWithSupply | str:
    logger.info(f"called create_new_supply with credentials: {credentials}")
    return await create_new_supply_common(supply, response, full_resource, transient, indicators, credentials)


@kitchen_supply_router.post("/", include_in_schema=False, response_model_exclude_none=True)
@kitchen_supply_router.post("", response_model_exclude_none=True)
async def create_new_kitchen_supply(
    kitchen_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> PutOrPostSupplyResultDto | BatchItemResultDtoWithSupply | str:
    logger.info(f"called create_kitchen_new_supply with credentials: {credentials}")
    return await create_new_supply_common(
        supply, response, full_resource, transient, indicators, credentials, kitchen_id=kitchen_id
    )


async def put_supply_common(
    supply_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> PutOrPostSupplyResultDto | WrappedSupplyResultDto | str:
    supply.supply.id = supply_id
    if kitchen_id is not None:
        supply.supply.kitchen_id = kitchen_id

    if full_resource:
        url = f"{settings.api_v2_url}/calculation/graphs"
    else:
        url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True"

    api_response = await api_client.session.post(
        url,
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(
            dict(supply.supply.model_dump()), transient, node_type=SupplySheetActivityNode.__name__
        ),
    )
    try:
        if full_resource:
            return get_result_from_supply_batch_item_or_id(api_response, response, supply_id, indicators)
        else:
            return put_or_post_supply_result_from_api_response(api_response, supply_id, response)
    except Exception:
        response.status_code = api_response.status_code
        return json.dumps({"api_response": api_response.text})


@supply_router.put("/{supply_id}", response_model_exclude_none=True)
async def put_supply(
    supply_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> PutOrPostSupplyResultDto | WrappedSupplyResultDto | str:
    logger.info(f"called put_supply with credentials: {credentials}")
    return await put_supply_common(supply_id, supply, response, full_resource, transient, indicators, credentials)


@kitchen_supply_router.put("/{supply_id}", response_model_exclude_none=True)
async def put_kitchen_supply(
    kitchen_id: str,
    supply_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> PutOrPostSupplyResultDto | WrappedSupplyResultDto | str:
    logger.info(f"called put_kitchen_supply with credentials: {credentials}")
    return await put_supply_common(
        supply_id, supply, response, full_resource, transient, indicators, credentials, kitchen_id=kitchen_id
    )


async def delete_supply_common(
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> None:
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": supply_id,
                }
            }
        }
    ]
    if kitchen_id is not None:
        batch_to_delete[0]["nodes_dto"]["existing_node"]["access_group_xid"] = kitchen_id

    api_response = await api_client.session.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status_code)

    try:
        result = api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status_code


@supply_router.delete("/{supply_id}", status_code=204, response_class=Response)
async def delete_supply(
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> None:
    logger.info(f"called delete_supply with credentials: {credentials}")
    await delete_supply_common(supply_id, response, credentials)


@kitchen_supply_router.delete("/{supply_id}", status_code=204, response_class=Response)
async def delete_kitchen_supply(
    kitchen_id: str,
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> None:
    logger.info(f"called delete_kitchen_supply with credentials: {credentials}")
    await delete_supply_common(supply_id, response, credentials, kitchen_id=kitchen_id)


@supply_router.post("/batch")
async def post_batch_supplies(
    batch: list[BatchSupplyDto],
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> list[BatchItemResultDtoWithSupply | PutOrPostSupplyResultDto | str]:
    logger.info(f"called create_new_recipe with credentials: {credentials}")

    json_batch, auxiliary_batch_info = legacy_to_new_batch_converter(
        [batch_item.model_dump() for batch_item in batch], "supply", transient
    )
    response_batch = []

    if full_resource:
        url = f"{settings.api_v2_url}/calculation/graphs"
    else:
        url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True"

    api_response = await api_client.session.post(
        url,
        headers={"Authorization": f"{credentials}"},
        json=json_batch,
    )
    try:
        result = api_response.json()
    except Exception:
        result = None
        for valid_supply_id, valid_request_ids in auxiliary_batch_info.valid_nodes.items():
            for valid_request_id in valid_request_ids:
                response_batch.append(
                    {
                        "statuscode": api_response.status_code,
                        "request-id": valid_request_id,
                        "supply-id": valid_supply_id,
                        "message": build_warning_message_for_batch_item(
                            valid_supply_id, valid_request_id, auxiliary_batch_info, "supply"
                        ),
                    }
                )

    if result:
        batch_by_xid = sort_batch_into_xids(result.get("batch"))
        for supply_id, request_ids in auxiliary_batch_info.valid_nodes.items():
            try:
                if full_resource:
                    batch_item = batch_by_xid.get(supply_id, {})
                    supply = get_result_from_supply_batch_item_or_id(
                        api_response, response, batch_item, indicators, full_resource
                    )
                    item_out = BatchItemResultDtoWithSupply(
                        **{
                            "supply": supply,
                            "statuscode": status.HTTP_200_OK,
                            "supply-id": supply_id,
                        }
                    )
                else:
                    item_out = put_or_post_supply_result_from_api_response(api_response, supply_id, response)

                for request_id in request_ids:
                    copied_item_out = copy.copy(item_out)
                    copied_item_out.request_id = request_id
                    copied_item_out.message = build_warning_message_for_batch_item(
                        supply_id, request_id, auxiliary_batch_info, "supply"
                    )
                    response_batch.append(copied_item_out)

            except Exception:
                for request_id in request_ids:
                    response_batch.append(
                        {
                            "statuscode": api_response.status_code,
                            "request-id": request_id,
                            "supply-id": supply_id,
                            "message": build_warning_message_for_batch_item(
                                supply_id, request_id, auxiliary_batch_info, "supply"
                            ),
                        }
                    )

    # Sort the response according to request-id. Send the item to the last if request-id does not exist.
    # The reqeust-id should always exist if the rest of the code runs fine.
    return sorted(response_batch, key=lambda x: getattr(x, "request_id", float("inf")))
