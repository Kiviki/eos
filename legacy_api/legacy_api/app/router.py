from fastapi import APIRouter
from structlog import get_logger

from legacy_api.app.routers.kitchen_recipe_router import kitchen_recipe_router
from legacy_api.app.routers.kitchen_router import kitchen_router
from legacy_api.app.routers.product_router import product_router
from legacy_api.app.routers.recipe_router import recipe_router
from legacy_api.app.routers.supply_router import kitchen_supply_router, supply_router
from legacy_api.app.settings import Settings

settings = Settings()
logger = get_logger()
router = APIRouter(prefix=settings.BASE_PATH)

router.include_router(kitchen_router, prefix="/kitchens")
router.include_router(kitchen_recipe_router, prefix="/kitchens/{kitchen_id}/recipes")
router.include_router(recipe_router, prefix="/recipes")
router.include_router(kitchen_supply_router, prefix="/kitchens/{kitchen_id}/supplies")
router.include_router(supply_router, prefix="/supplies")
router.include_router(product_router, prefix="/products")
