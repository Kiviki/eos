from fastapi import FastAPI, Request, status
from fastapi.exceptions import RequestValidationError
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse
from structlog import get_logger

from api.app.server import custom_apple_schema
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb
from legacy_api.app.api_client import ApiClient
from legacy_api.app.configure_logging import configure_logging
from legacy_api.app.settings import Settings
from legacy_api.middlewares.namespace_auth.add_namespace import NamespaceAuthMiddleware
from legacy_api.middlewares.request_logging.middleware import add_log_context

logger = get_logger()
service_provider = ServiceProvider()
postgres_db = PostgresDb(service_provider)
api_client = ApiClient()


async def app_startup(app, schema=None):
    # only connect to db if not yet connected:
    if not postgres_db.pool:
        await postgres_db.connect(schema=schema)

    await api_client.connect()


async def app_shutdown():
    await api_client.close()


def create_app() -> FastAPI:
    settings = Settings()

    app = FastAPI(
        title=settings.TITLE,
        openapi_url=f"{settings.BASE_PATH}/openapi.json",
        docs_url=f"{settings.BASE_PATH}/docs",
        redoc_url=f"{settings.BASE_PATH}/redoc",
    )

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        exc_str = f"{exc}".replace("\n", " ").replace("   ", " ")
        logger.error(f"{request}: {exc_str}")
        content = {"status_code": 422, "message": "RequestValidationError", "data": None}
        return JSONResponse(content=content, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)

    configure_logging(settings)

    @app.on_event("startup")
    async def startup_event():
        await app_startup(app)

    @app.on_event("shutdown")
    async def shutdown_event():
        await app_shutdown()

    app.middleware("http")(add_log_context)
    app.add_middleware(NamespaceAuthMiddleware)

    setup_router(app)

    def custom_openapi():
        if app.openapi_schema:
            return app.openapi_schema
        openapi_schema = get_openapi(
            title="Eaternity API",
            version="2.5.0",
            description="This is a very custom OpenAPI schema",
            routes=app.routes,
        )
        openapi_schema["info"]["x-logo"] = {
            "url": "https://eaternity.org/img/home/Eaternity-logo-white-outline-small.png"
        }

        # Just testing here how we can add dynamically an apple schema to the openapi schema:
        openapi_schema["components"]["schemas"]["AppleTerm"] = custom_apple_schema

        # Add authentication header to all our routes except status route:
        for route, route_schema in openapi_schema["paths"].items():
            if route.startswith("/batch") or route.startswith(settings.BASE_PATH):
                for method, method_schema in route_schema.items():
                    if "parameters" not in method_schema:
                        method_schema["parameters"] = []
                    method_schema["parameters"].append(
                        {
                            "required": True,
                            "schema": {"title": "Authorization", "type": "string"},
                            "name": "Authorization",
                            "default": "Basic APIKEY",
                            "in": "header",
                        }
                    )

        app.openapi_schema = openapi_schema
        return app.openapi_schema

    app.openapi = custom_openapi

    return app


def setup_router(app: FastAPI):
    from legacy_api.app.router import router

    settings = Settings()

    app.include_router(router)

    @app.get(f"{settings.BASE_PATH}")
    @app.get(f"{settings.BASE_PATH}/status")
    async def status():
        return {
            "status": "successful",
            "service": "legacy-api",
        }


fastapi_app = create_app()
