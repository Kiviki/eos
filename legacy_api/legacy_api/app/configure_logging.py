import logging
import logging.config

import structlog

from legacy_api.app.settings import Settings


def configure_logging(settings: Settings):
    # Processors that will apply to all log records, no matter if they were created by stdlib logging or structlog
    shared_processors = [
        # Add structlog context variables to log lines
        structlog.contextvars.merge_contextvars,
        # Adds a timestamp for every log line
        structlog.processors.TimeStamper(fmt="iso"),
        # Add the name of the logger to the record
        structlog.stdlib.add_logger_name,
        # Adds the log level as a parameter of the log line
        structlog.stdlib.add_log_level,
        # Perform old school %-style formatting. on the log msg/event
        structlog.stdlib.PositionalArgumentsFormatter(),
        # Adds parameters about where in the source code the log function called from (file, line...)
        structlog.processors.CallsiteParameterAdder(
            [
                # The name of the function that the log is in
                structlog.processors.CallsiteParameter.FUNC_NAME,
                # The line number of the log
                structlog.processors.CallsiteParameter.LINENO,
            ],
        ),
        # If the log record contains a string in byte format, this will automatically convert it into a utf-8 string
        structlog.processors.UnicodeDecoder(),
    ]

    # Processors that will only apply to records generated by structlog
    structlog_only_processors = [
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ]

    processors = shared_processors + structlog_only_processors

    # Configure structlog logging
    structlog.configure(
        processors=processors,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
    )

    # Configure standard logging module
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "json_formatter": {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": structlog.processors.JSONRenderer(),
                    "foreign_pre_chain": shared_processors,
                },
                "plain_console": {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": structlog.dev.ConsoleRenderer(sort_keys=True, colors=True),
                    "foreign_pre_chain": shared_processors,
                },
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "json_formatter" if settings.JSON_LOGGING else "plain_console",
                }
            },
            "loggers": {
                "": {
                    "handlers": ["console"],
                    "level": settings.LOG_LEVEL,
                }
            },
        }
    )
