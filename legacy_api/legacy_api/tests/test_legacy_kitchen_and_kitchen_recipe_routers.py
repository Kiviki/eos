import copy
import os
from typing import Any

import pytest
from httpx import AsyncClient
from structlog import get_logger

from api.tests.test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
)
from legacy_api.tests.test_legacy_recipe_router import (
    TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT,
    TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
)

from .conftest import TEST_KITCHEN_ADMIN_ID, TEST_KITCHEN_SUPERUSER_ID, AppFixtureReturn, check_response_dict

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_superuser(
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    logger.info("finished all fixtures in test_create_and_get_and_delete_and_create_kitchen_by_superuser")
    return


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_admin(
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    return


TEST_RECIPE_ADMIN_ID = "test_kitchen_recipe_admin_legacy_id"
TEST_RECIPE_SUPERUSER_ID = "test_kitchen_recipe_superuser_legacy_id"
SERVINGS = 140
TEST_RECIPE_LEGACY_CO2_VALUE = int(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 / SERVINGS)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# *1000 because legacy co2eq value is in gram instead of kilogram
TEST_RECIPE_LEGACY_FOOD_UNIT = round(TEST_RECIPE_EXPECTED_DFU * 1000, 2)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# Note that the daily food unit is not scaled by servings even in eaternity-cloud (Javaland).
TEST_RECIPE = {
    "recipe": {
        "titles": [{"language": "de", "value": "Kürbisrisotto"}],
        "author": "Eckart Witzigmann",
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "instructions": [
            {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
            {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
        ],
        "ingredients": [
            {
                "id": "100100191",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Tomaten",
                    },
                ],
                "amount": 150,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "100100894",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln",
                    },
                ],
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}

TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT = round(
    TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT + 82.48, 2
)


@pytest.mark.asyncio
async def test_get_all_kitchens(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # First try with trailing slash
        response = await ac.get(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [TEST_KITCHEN_ADMIN_ID]}

        # Now try without a trailing slash
        response = await ac.get(
            "/api/kitchens",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [TEST_KITCHEN_ADMIN_ID]}

        response = await ac.get(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": []}


@pytest.mark.asyncio
async def test_get_all_kitchens_of_customer(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            "/api/kitchens",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [kitchen_xid]}


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing POST request
        response = await ac.post(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        temp_recipe_id = response.json().get("recipe").get("id")
        TEST_RECIPE_WITH_ID: dict[str, Any] = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = temp_recipe_id
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]

        expected_result_for_post = copy.deepcopy(TEST_RECIPE_WITH_ID)
        expected_result_for_post["message"] = ""
        expected_result_for_post["recipe-id"] = temp_recipe_id
        expected_result_for_post["request-id"] = 0
        expected_result_for_post["statuscode"] = 200

        check_response_dict(
            expected_result_for_post,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        # TODO: should we test mutation log response here as well?
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        TEST_RECIPE_WITH_ID = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        # check if supplies listing works correctly
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"recipes": [TEST_RECIPE_SUPERUSER_ID]}


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing that an admin can't post recipes into a wrong group
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 401

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        TEST_RECIPE_WITH_ID = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        # making sure that an admin can't delete a resource that's not from their group
        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 401

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )


TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID = "test_sub_recipe_superuser_legacy_id"
SERVINGS = 140
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE = int(
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE * 1000 * 1000 / SERVINGS
)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# *1000 because legacy co2eq value is in gram instead of kilogram
TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT = round(TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU * 1000, 2) + 82.47
# Additional 82.47 due to drying of tomato.
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# Note that the daily food unit is not scaled by servings even in eaternity-cloud (Javaland).
TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER = {
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "ingredients": [
            {
                "id": TEST_RECIPE_SUPERUSER_ID,
                "type": "recipes",
                "names": [{"language": "de", "value": "Kürbisrisotto"}],
                "amount": 50,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "242342343",
                "type": "conceptual-ingredients",
                "names": [{"language": "de", "value": "Tomaten"}],  # Changed from Kartoffeln for consistency with API.
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",  # Left dried here (deleted in API) to verify GreenhouseGFM is not triggered.
                "packaging": "",
            },
        ],
    }
}


TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID = "test_sub_recipe_admin_legacy_id"
TEST_RECIPE_WITH_SUBRECIPE_ADMIN = {
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "ingredients": [
            {
                "id": TEST_RECIPE_ADMIN_ID,
                "type": "recipes",
                "names": [{"language": "de", "value": "Kürbisrisotto"}],
                "amount": 50,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "242342343",
                "type": "conceptual-ingredients",
                "names": [{"language": "de", "value": "Tomaten"}],  # Changed from Kartoffeln for consistency with API.
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_with_subrecipe_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # first insert the subrecipe, so that we can actually use it afterwards:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # now add the outer recipe that contains the above subrecipe
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER,
        )
        assert response.status_code == 200

        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER)
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["id"] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"][
            "recipe-portions"
        ] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )

        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                0,
            ],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER,
        )
        assert response.status_code == 200

        # TODO: should we test mutation log response here as well?
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                0,
            ],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
        )


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_with_subrecipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # first insert the subrecipe, so that we can actually use it afterwards:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # now add the outer recipe that contains the above subrecipe
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_ADMIN,
        )
        assert response.status_code == 200

        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_ADMIN)
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["id"] = TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"][
            "recipe-portions"
        ] = TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )

        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                0,
            ],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_ADMIN,
        )
        assert response.status_code == 200

        # TODO: should we test mutation log response here as well?
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                0,
            ],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
        )
