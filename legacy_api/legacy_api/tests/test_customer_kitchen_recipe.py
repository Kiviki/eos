import os

import pytest
from httpx import AsyncClient
from structlog import get_logger

from .conftest import AppFixtureReturn
from .test_legacy_recipe_router import TEST_RECIPE

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


@pytest.mark.asyncio
async def test_create_customer_and_kitchen_and_recipe(
    app_fixture: AppFixtureReturn, create_simple_customer: tuple[str, str, bool]
) -> None:
    # Step 1: Create a customer (use fixture):
    customer_namespace_uid, customer_token, status = create_simple_customer
    assert status is True

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        logger.info("step 2: create kitchen")
        kitchen_xid = "kitchen01"
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}",
            auth=(customer_token, ""),
            json={
                "kitchen": {
                    "name": "carrot-making kitchen",
                    "location": "Switzerland",
                },
            },
        )
        assert response.status_code == 201

        logger.info("step 3: create recipe")
        recipe_xid = "recipe01"
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}",
            auth=(customer_token, ""),
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        response_json = response.json()
        assert response_json.get("recipe").get("co2-value") == 1353

        logger.info("step 4: create a second kitchen")
        kitchen_xid2 = "kitchen02"
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid2}",
            auth=(customer_token, ""),
            json={
                "kitchen": {
                    "name": "a second kitchen",
                    "location": "Switzerland",
                },
            },
        )
        assert response.status_code == 201

        logger.info("step 5: get recipe from kitchen01")
        response = await ac.get(f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}", auth=(customer_token, ""))
        assert response.status_code == 200

        logger.info("step 6: get recipe from kitchen02 should fail")
        response = await ac.get(f"/api/kitchens/{kitchen_xid2}/recipes/{recipe_xid}", auth=(customer_token, ""))
        assert response.status_code == 404, "recipe should not be found in kitchen02"

        logger.info("step 7: get recipe should work if kitchen is not specified")
        response = await ac.get(f"/api/recipes/{recipe_xid}", auth=(customer_token, ""))
        assert response.status_code == 200
