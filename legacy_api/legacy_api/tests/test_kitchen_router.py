import os
import uuid

import pytest
from httpx import AsyncClient
from structlog import get_logger

from .conftest import AppFixtureReturn, api_client, legacy_api_settings, put_json, settings
from .test_legacy_recipe_router import TEST_RECIPE

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


@pytest.mark.asyncio
async def test_kitchen_router(app_fixture: AppFixtureReturn, create_simple_customer: tuple[str, str, bool]) -> None:
    # Step 1: Create a customer (use fixture):
    customer_namespace_uid, customer_token, status = create_simple_customer
    assert status is True

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        logger.info("post kitchen")
        response = await ac.post(
            f"/api/kitchens/",
            auth=(customer_token, ""),
            json={
                "kitchen": {
                    "name": "canteen",
                    "location": "zurich, switzerland",
                    "email": "test@example.com",
                    "language": "en",
                },
            },
        )
        assert response.status_code == 201

        logger.info("put kitchen")
        kitchen_xid = "test_kitchen"
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}",
            auth=(customer_token, ""),
            json={
                "kitchen": {"name": "canteen2", "location": "zurich, switzerland"},
            },
        )
        assert response.status_code == 201

        logger.info("get kitchen by xid")
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        assert response.json()["kitchen"]["name"] == "canteen2"

        logger.info("get nonexistent kitchen")
        response = await ac.get(
            f"/api/kitchens/nonexistent",
            auth=(customer_token, ""),
        )
        assert response.status_code == 404

        logger.info("get all kitchens")
        response = await ac.get(
            f"/api/kitchens",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        assert len(response.json()["kitchens"]) == 2

        logger.info("delete kitchen by xid")
        response = await ac.delete(
            f"/api/kitchens/{kitchen_xid}",
            auth=(customer_token, ""),
        )
        assert response.status_code == 204

        logger.info("get all kitchens after delete")
        response = await ac.get(
            f"/api/kitchens",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        assert len(response.json()["kitchens"]) == 1
