import os

import pytest
from httpx import AsyncClient
from structlog import get_logger

from api.tests.conftest import FastAPIReturn
from api.tests.test_api_for_products import TEST_PRODUCT_EXPECTED_CO2, TEST_PRODUCT_EXPECTED_DFU

from .conftest import check_response_dict

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()

TEST_PRODUCT_SUPERUSER_ID = "test_product_superuser_legacy"
# *1000: kilogram instead of gram used as units of food product, *1000: gram CO2-eq used instead of kg CO2-eq.
TEST_PRODUCT_LEGACY_CO2_VALUE = int(TEST_PRODUCT_EXPECTED_CO2 * 1000 * 1000)
TEST_PRODUCT_LEGACY_FOOD_UNIT = round(
    TEST_PRODUCT_EXPECTED_DFU * 1000, 2
)  # *1000 kilogram instead of gram used as units
TEST_PRODUCT_SUPERUSER = {
    "product": {
        "id": TEST_PRODUCT_SUPERUSER_ID,
        "date": "2020-02-02",
        "gtin": "00123456789023",
        "names": [
            {
                "language": "de",
                "value": "Karottenpuree",
            },
        ],
        "amount": 200,
        "unit": "kilogram",
        "producer": "hipp",
        "ingredients-declaration": "Karotten, Tomaten",
        "nutrient-values": {
            "energy-kcal": 200,
            "fat-gram": 12.3,
            "saturated-fat-gram": 2.5,
            "carbohydrates-gram": 8.4,
            # TODO "sucrose-gram": 3.2, also support sucrose, even if not present in the ingredients' nutrition data
            "protein-gram": 2.2,
            "sodium-chloride-gram": 0.3,
        },
        "origin": "paris, frankreich",
        "transport": "ground",
        "production": "standard",
        "processing": "raw",
        "conservation": "fresh",
        "packaging": "none",
    }
}

TEST_PRODUCT_ADMIN_ID = "test_product_admin_legacy"
TEST_PRODUCT_ADMIN = {
    "product": {
        "id": TEST_PRODUCT_ADMIN_ID,
        "date": "2020-02-02",
        "gtin": "00123456789023",
        "names": [
            {
                "language": "de",
                "value": "Karottenpuree",
            },
        ],
        "amount": 200,
        "unit": "kilogram",
        "producer": "hipp",
        "ingredients-declaration": "Karotten, Tomaten",
        "nutrient-values": {
            "energy-kcal": 200,
            "fat-gram": 12.3,
            "saturated-fat-gram": 2.5,
            "carbohydrates-gram": 8.4,
            # TODO "sucrose-gram": 3.2,  also support sucrose, even if not present in ingredients' nutrition data
            "protein-gram": 2.2,
            "sodium-chloride-gram": 0.3,
        },
        "origin": "paris, frankreich",
        "transport": "ground",
        "production": "standard",
        "processing": "raw",
        "conservation": "fresh",
        "packaging": "none",
    }
}


@pytest.mark.asyncio
async def test_create_delete_and_get_product_by_superuser(
    app_fixture: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_PRODUCT_SUPERUSER,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_PRODUCT_SUPERUSER,
            response.json(),
            "product",
            expected_co2_value=TEST_PRODUCT_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_PRODUCT_LEGACY_FOOD_UNIT,
        )

        response = await ac.delete(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_PRODUCT_SUPERUSER,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_PRODUCT_SUPERUSER,
            response.json(),
            "product",
            expected_co2_value=TEST_PRODUCT_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_PRODUCT_LEGACY_FOOD_UNIT,
        )


@pytest.mark.asyncio
async def test_create_delete_and_get_product_by_admin(
    app_fixture: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_PRODUCT_ADMIN,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_PRODUCT_ADMIN,
            response.json(),
            "product",
            expected_co2_value=TEST_PRODUCT_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_PRODUCT_LEGACY_FOOD_UNIT,
        )

        response = await ac.delete(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_PRODUCT_ADMIN,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 401, (
            "should return 401 because USER_SUPER created the product and that user "
            "has a different default access_group than USER_ADMIN. "
            "It is not a 404, because both access_groups are in the same namespace."
        )

        response = await ac.get(
            f"/api/products/{TEST_PRODUCT_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            TEST_PRODUCT_ADMIN,
            response.json(),
            "product",
            expected_co2_value=TEST_PRODUCT_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_PRODUCT_LEGACY_FOOD_UNIT,
        )
