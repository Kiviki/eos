import copy
import os
import uuid
from typing import Any

import pytest
from gap_filling_modules.abstract_util import find_access_group_uid_by_name
from httpx import AsyncClient
from structlog import get_logger

from api.tests.test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_EXPECTED_DFU_ING1,
    TEST_RECIPE_EXPECTED_DFU_ING2,
)
from core.domain.matching_item import MatchingItem
from core.service.service_provider import ServiceLocator

from .conftest import TEST_KITCHEN_ADMIN_ID, TEST_KITCHEN_SUPERUSER_ID, AppFixtureReturn, check_response_dict

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()

EXPECTED_CO2 = int(TEST_RECIPE_EXPECTED_CO2 * 1000)
EXPECTED_DFU = round(TEST_RECIPE_EXPECTED_DFU, 2)
EXPECTED_DFU_ING1 = round(TEST_RECIPE_EXPECTED_DFU_ING1, 2)
EXPECTED_DFU_ING2 = round(TEST_RECIPE_EXPECTED_DFU_ING2, 2)


TEST_SUPPLY_ADMIN_ID = "test_supply_admin_legacy"
TEST_SUPPLY_SUPERUSER_ID = "test_supply_superuser_legacy"
# FIXME Currently, the ingredients_declaration and nutrient_values in supplies are not used by Kale.
TEST_SUPPLY = {
    "supply": {
        "supplier": "Vegetable Supplies Ltd.",
        "supplier-id": "el3i5y-2in5y-2hbllll01",
        "invoice-id": "778888800000001",
        "supply-date": "2023-06-20",
        "ingredients": [
            {
                "id": "100100191",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Tomaten",
                    },
                ],
                "amount": 150,
                "unit": "gram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
                "ingredients-declaration": "Hähnchenbrustfilet (53 %), Panade (28%) "
                "(Weizenmehl, Wasser, modifizierte Weizen-stärke, Weizenstärke, Speisesalz, Gewürze, Hefe), "
                "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) (Putenkeulenfleisch, "
                "Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), Maltodextrin, Dextrose, Gewürz"
                "extrakte, Stabilisator: E450), Käse (7 %), Kartoffelstärke, Stabilisator: modif"
                "izierte Stärke, Salz), Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzene"
                "iweiß, Dextrose, Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
                "nutrient-values": {
                    "energy-kjoule": 500,
                    "fat-gram": 9001,
                    "protein-gram": 33,
                    "vitamine-k-microgram": 0.21,
                    "vitamine-a1-microgram": 0.12345,
                    "manganese-microgram": 0,
                    "fibers-gram": 1111,
                },
            },
            {
                "id": "100199894",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln",
                    },
                ],
                "amount": 78,
                "unit": "gram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}

TEST_SUPPLY_UNMATCHED = copy.deepcopy(TEST_SUPPLY)
for ingredient in TEST_SUPPLY_UNMATCHED["supply"]["ingredients"]:
    ingredient["names"][0]["value"] = f"unmatched_{ingredient['names'][0]['value']}"


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # trying POST request
        response = await ac.post(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )

        assert response.status_code == 200
        assert response.json().get("supply-id") is not None
        temp_supply_id = response.json().get("supply-id")

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200

        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)
        expected_response["supply"]["id"] = temp_supply_id
        expected_response["supply"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID

        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_response["supply"]["id"] = TEST_SUPPLY_SUPERUSER_ID
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        # check if supplies listing works correctly
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"supplies": [TEST_SUPPLY_SUPERUSER_ID]}

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?transient=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/"
            f"{TEST_SUPPLY_SUPERUSER_ID}?transient=true&full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200

        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)
        expected_response["supply"]["id"] = TEST_SUPPLY_ADMIN_ID
        expected_response["supply"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )


@pytest.mark.asyncio
async def test_supplies_endpoint(app_fixture: AppFixtureReturn, create_simple_customer: tuple[str, str, str]) -> None:
    _, customer_token, _ = create_simple_customer
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)

        response = await ac.post("/api/supplies/", auth=(customer_token, ""), json=TEST_SUPPLY)
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, ""))
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = response.json()["supply"]["kitchen-id"]

        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.delete(f"/api/supplies/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 204

        response = await ac.get(f"/api/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 404

        response = await ac.put(
            f"/api/supplies/{assigned_supply_id}?full-resource=true",
            auth=(customer_token, ""),
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.delete(f"/api/supplies/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 204

        response = await ac.post("/api/supplies/?full-resource=true", auth=(customer_token, ""), json=TEST_SUPPLY)
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assert response.json()["request-id"] == 0
        assigned_supply_id = response.json()["supply-id"]
        assert response.json()["supply"]["co2-value"] == EXPECTED_CO2

        expected_response["supply"]["id"] = assigned_supply_id

        expected_response_post = copy.deepcopy(expected_response)
        expected_response_post["message"] = ""
        expected_response_post["statuscode"] = 200
        expected_response_post["request-id"] = 0
        expected_response_post["supply-id"] = assigned_supply_id
        expected_response_post["supply"]["id"] = assigned_supply_id

        check_response_dict(
            expected_response_post,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=false", auth=(customer_token, ""))
        expected_response_without_ingredients = copy.deepcopy(expected_response)
        expected_response_without_ingredients["supply"]["ingredients"] = []

        assert response.status_code == 200
        check_response_dict(
            expected_response_without_ingredients,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
        )

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, ""))
        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )


@pytest.mark.asyncio
async def test_supplies_batch(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    _ = app_fixture
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    test_supply_with_kitchen_id = copy.deepcopy(TEST_SUPPLY)
    test_supply_with_kitchen_id["supply"]["kitchen-id"] = kitchen_xid

    test_supply_with_double_onion = copy.deepcopy(test_supply_with_kitchen_id)
    test_supply_with_double_onion["supply"]["ingredients"][1]["amount"] *= 2

    test_supply_id = "test-supply-id"
    test_supply_with_kitchen_id["supply"]["id"] = test_supply_id

    test_batch = [
        {"request-id": 0, **test_supply_with_kitchen_id},
        {"request-id": 1, **test_supply_with_double_onion},
        {"request-id": 1, **test_supply_with_double_onion},
        {"request-id": 2, **test_supply_with_kitchen_id},
    ]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post("/api/supplies/batch", auth=(customer_token, ""), json=test_batch)

    assert response.status_code == 200
    assert len(response.json()) == 3
    for index in range(3):
        assert response.json()[index]["request-id"] == index
        supply_id = response.json()[index]["supply-id"]
        if index in (0, 2):
            expected_response: dict[str, Any] = copy.deepcopy(test_supply_with_kitchen_id)
            expected_response["supply"]["ingredients"] = []
            assert supply_id == test_supply_id
            assert response.json()[index]["message"] == (
                "Supply ID 'test-supply-id' in multiple batch items. " "Using the item appearing last in the list."
            )
            expected_co2 = EXPECTED_CO2
            expected_dfu = EXPECTED_DFU
        else:
            expected_response: dict[str, Any] = copy.deepcopy(test_supply_with_double_onion)
            expected_response["supply"]["ingredients"] = []
            expected_response["supply"]["id"] = supply_id

            assert response.json()[index]["message"] == (
                "Request ID '1' in multiple batch items. Using the item appearing last in the list."
            )
            expected_co2 = int(TEST_RECIPE_EXPECTED_CO2 * 2 * 1000)
            expected_dfu = round(TEST_RECIPE_EXPECTED_DFU_ING1 + TEST_RECIPE_EXPECTED_DFU_ING2 * 2, 2)

        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            get_response = await ac.get(
                f"/api/kitchens/{kitchen_xid}/supplies/{supply_id}?full-resource=false", auth=(customer_token, "")
            )

        assert get_response.status_code == 200
        check_response_dict(
            expected_response,
            get_response.json(),
            "supply",
            expected_co2_value=expected_co2,
            expected_food_unit=expected_dfu,
        )

        if index != 0:
            async with AsyncClient(
                app=app_fixture,
                base_url="http://localhost:8050",
            ) as ac:
                delete_response = await ac.delete(
                    f"/api/kitchens/{kitchen_xid}/supplies/{supply_id}", auth=(customer_token, "")
                )

            assert delete_response.status_code == 204

    # Try posting directly with full-resource=true to see if we can get CO2 Values returned.
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post("/api/supplies/batch?full-resource=true", auth=(customer_token, ""), json=test_batch)
        for index in range(3):
            assert response.json()[index]["request-id"] == index
            supply_id = response.json()[index]["supply-id"]
            if index in (0, 2):
                expected_response_post: dict[str, Any] = copy.deepcopy(test_supply_with_kitchen_id)
                expected_response_post["message"] = (
                    "Supply ID 'test-supply-id' in multiple batch items. " "Using the item appearing last in the list."
                )
                expected_response_post["supply-id"] = test_supply_id
                expected_response_post["supply"]["id"] = test_supply_id
                assert supply_id == test_supply_id

                expected_co2 = EXPECTED_CO2
                expected_dfu = EXPECTED_DFU
                expected_dfu_ing1 = EXPECTED_DFU_ING1
                expected_dfu_ing2 = EXPECTED_DFU_ING2
            else:
                expected_response_post: dict[str, Any] = copy.deepcopy(test_supply_with_double_onion)
                expected_response_post[
                    "message"
                ] = "Request ID '1' in multiple batch items. Using the item appearing last in the list."
                expected_response_post["supply-id"] = supply_id
                expected_response_post["supply"]["id"] = supply_id

                expected_co2 = int(TEST_RECIPE_EXPECTED_CO2 * 2 * 1000)
                expected_dfu = round(TEST_RECIPE_EXPECTED_DFU_ING1 + TEST_RECIPE_EXPECTED_DFU_ING2 * 2, 2)
                expected_dfu_ing1 = round(TEST_RECIPE_EXPECTED_DFU_ING1, 2)
                expected_dfu_ing2 = round(TEST_RECIPE_EXPECTED_DFU_ING2 * 2, 2)

            expected_response_post["statuscode"] = 200
            expected_response_post["request-id"] = index

            check_response_dict(
                expected_response_post,
                response.json()[index],
                "supply",
                expected_co2_value=expected_co2,
                expected_food_unit=expected_dfu,
                expected_ingredients_co2_value=[0, expected_co2],
                expected_ingredients_food_unit=[expected_dfu_ing1, expected_dfu_ing2],
            )


@pytest.mark.asyncio
async def test_supplies_unmatched(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_namespace_uid, kitchen_xid = create_simple_customer_and_kitchen
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY_UNMATCHED)

        response = await ac.post(
            f"/api/kitchens/{kitchen_xid}/supplies/", auth=(customer_token, ""), json=TEST_SUPPLY_UNMATCHED
        )
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = kitchen_xid

        assert response.status_code == 200
        assert response.json()["supply"]["co2-value"] is None, "CO2 values should be none as no matching exists."

        check_response_dict(
            expected_response,
            response.json(),
            "supply",
        )

        service_provider = ServiceLocator().service_provider

        access_group = await service_provider.access_group_service.get_access_group_by_xid(
            kitchen_xid, customer_namespace_uid
        )

        foodex2_access_group_uid = await find_access_group_uid_by_name(
            service_provider.postgres_db, "FoodEx2 Glossary Terms namespace"
        )
        tomato_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "A0DMX", str(foodex2_access_group_uid)
        )
        onion_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "A1480", str(foodex2_access_group_uid)
        )
        ingredient_terms = [tomato_term, onion_term]

        for index in range(2):
            matching_string = TEST_SUPPLY_UNMATCHED["supply"]["ingredients"][index]["names"][0]["value"]
            language = TEST_SUPPLY_UNMATCHED["supply"]["ingredients"][index]["names"][0]["language"]
            matching_term_uids = [str(ingredient_terms[index].uid)]
            await service_provider.matching_service.put_matching_item(
                MatchingItem(
                    gap_filling_module="MatchProductName",
                    uid=str(uuid.uuid4()),
                    access_group_uid=str(access_group.uid),
                    lang=language,
                    matching_string=matching_string,
                    term_uids=matching_term_uids,
                )
            )

        # The second get after matching should correctly return the CO2 values.
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=EXPECTED_CO2,
            expected_food_unit=EXPECTED_DFU,
            expected_ingredients_co2_value=[0, EXPECTED_CO2],
            expected_ingredients_food_unit=[EXPECTED_DFU_ING1, EXPECTED_DFU_ING2],
        )
