"API v2 setup."
import os
import typing

import pytest
from structlog import get_logger

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


@pytest.mark.asyncio
async def test_api_v2_setup(app_fixture: typing.Optional[typing.Callable[..., typing.Any]]) -> None:  # noqa: ARG001
    "Setup app_fixture."
    logger.info("finished app_fixture")
