import copy
import os
import uuid

import pytest
from httpx import AsyncClient
from structlog import get_logger

from api.tests.conftest import FastAPIReturn
from api.tests.test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_EXPECTED_DFU_NO_DRYING,
)
from api.tests.test_calc_router import (
    ONION_DRYING_UPSCALING,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO,
)
from core.domain.nodes.flow_node import FlowNode
from core.domain.props.link_to_xid_prop import LinkToXidProp
from legacy_api.app.server import postgres_db

from .conftest import TEST_KITCHEN_ADMIN_ID, TEST_KITCHEN_SUPERUSER_ID, AppFixtureReturn, check_response_dict

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()

SERVINGS = 140
TEST_RECIPE_LEGACY_CO2_VALUE = int(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 / SERVINGS)
TEST_RECIPE_WITHOUT_TRANSPORT_EXPECTED_CO2 = int(209.02 * 1000 / SERVINGS)
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE = 628
TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE = int(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 * 50 / (SERVINGS * 228))
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE = int(
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE - TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 * 50 / (SERVINGS * 228)
)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# *1000 because legacy co2eq value is in gram instead of kilogram

TEST_RECIPE_LEGACY_FOOD_UNIT = round(TEST_RECIPE_EXPECTED_DFU * 1000, 2)
TEST_RECIPE_NO_DRYING_LEGACY_FOOD_UNIT = round(TEST_RECIPE_EXPECTED_DFU_NO_DRYING * 1000, 2)
TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT = round(TEST_RECIPE_EXPECTED_DFU * 1000 * 50 / 228, 2)
TEST_RECIPE_LEGACY_ING1_FOOD_UNIT = 23.73
TEST_RECIPE_LEGACY_ING2_FOOD_UNIT_NO_DRYING = 16.41
TEST_RECIPE_LEGACY_ING2_FOOD_UNIT = round(TEST_RECIPE_LEGACY_FOOD_UNIT - TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, 2)
TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT = 37.17
TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT = round(  # -0.01 for rounding errors
    TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT - TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT, 2
)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# Note that the daily food unit is not scaled by servings even in eaternity-cloud (Javaland).

TEST_RECIPE_EXPECTED_SCARCE_WATER_LITER = round(
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION * 1000, 2
)  # TODO Fill this value.

TEST_RECIPE_LEGACY_VITASCORE = 320.07
TEST_RECIPE = {
    "recipe": {
        "titles": [{"language": "de", "value": "Kürbisrisotto"}],
        "author": "Eckart Witzigmann",
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "instructions": [
            {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
            {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
        ],
        "ingredients": [
            {
                "id": "100199191",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Tomaten",
                    },
                ],
                "amount": 150,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "100199894",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln",
                    },
                ],
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}
TEST_RECIPE_SUPERUSER_ID = "test_recipe_superuser_legacy_id"
TEST_RECIPE_ADMIN_ID = "test_recipe_admin_legacy_id"


TEST_RECIPE_WITH_SUBRECIPE = {
    "request-id": 1,
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "ingredients": [
            {
                "transport": "air",
                "processing": "raw",
                "packaging": "plastic",
                "id": "test_recipe_id",
                "origin": "Z\u00fcrich Schweiz",
                "production": "greenhouse",
                "conservation": "fresh",
                "type": "recipes",
                "amount": 50,
                "unit": "kilogram",
            },
            {
                "transport": "ground",
                "processing": "",
                "packaging": "",
                "id": "242342343",
                "names": [{"language": "de", "value": "Tomaten"}],
                "origin": "france",
                "production": "organic",
                "type": "conceptual-ingredients",
                "amount": 78,
                "unit": "kilogram",
            },
        ],
        "servings": 140,
        "date": "2023-06-20",
        "id": "test_recipe_with_subrecipe_id",
        "location": "Z\u00fcrich Schweiz",
        "production-portions": 500,
        "sold-portions": 300,
        "menu-line-name": "EOS test recipe with subrecipe",
        "menu-line-id": 10101010,
        "kitchen-id": "kitchen01",
    },
}


def delete_field_from_recipe_or_ingredient(
    main_recipe: dict, field_to_delete: str, delete_from: str = "recipe"
) -> dict:
    assert delete_from in (
        "recipe",
        "ingredient",
    ), f"delete_from has to be either 'recipe' or 'ingredient', not {delete_from}"
    new_recipe = copy.deepcopy(main_recipe)

    match delete_from:
        case "recipe":
            del new_recipe["recipe"][field_to_delete]
        case "ingredient":
            for ingredient in new_recipe["recipe"]["ingredients"]:
                del ingredient[field_to_delete]

    return new_recipe


def add_field_from_recipe_or_ingredient(
    main_recipe: dict, field_to_add: str, new_value: str | float | int | dict | list, add_to: str = "recipe"
) -> dict:
    assert add_to in (
        "recipe",
        "ingredient",
    ), f"add_to has to be either 'recipe' or 'ingredient', not {add_to}"
    new_recipe = copy.deepcopy(main_recipe)

    match add_to:
        case "recipe":
            new_recipe["recipe"][field_to_add] = new_value
        case "ingredient":
            for idx, ingredient in enumerate(new_recipe["recipe"]["ingredients"]):
                assert isinstance(
                    new_value, list
                ), f"For add_to='ingredient', new_value should be a list not {type(new_value)}."
                ingredient[field_to_add] = new_value[idx]

    return new_recipe


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_superuser(
    app_fixture: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing POST request without trailing slash
        response = await ac.post(
            "/api/recipes?indicators=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # testing POST request with trailing slash
        response = await ac.post(
            "/api/recipes/?indicators=true&full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["request-id"] = 0
        expected_recipe_resp["statuscode"] = 200
        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_indicators={
                "vita-score": TEST_RECIPE_LEGACY_VITASCORE,
                "scarce-water-liters": TEST_RECIPE_EXPECTED_SCARCE_WATER_LITER,
                "rainforest-label": False,
                "rainforest-rating": "E",
                "animal-treatment-label": True,
                "animal-treatment-rating": "A",
            },
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_indicators=[
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": True,
                    "rainforest-rating": "A",
                    "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO * 1000, 2),
                    "vita-score": 320.07,
                },
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": False,
                    "rainforest-rating": "E",
                    "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION * 1000, 2),
                    "vita-score": 320.07,
                },
            ],
        )
        assert response.json().get("recipe").get("id") is not None
        temp_recipe_id = response.json().get("recipe").get("id")

        response = await ac.get(
            f"/api/recipes/{temp_recipe_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200

        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        response = await ac.delete(
            f"/api/recipes/{temp_recipe_id}", headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"}
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/recipes/{temp_recipe_id}", headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"}
        )
        assert response.status_code == 404

        # TODO: should we test mutation log response here as well?
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        assert len(response.json()["recipe"]["ingredients"]) == 0  # since full-resource=false by default.

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        # Test for transient:
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}?transient=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        assert len(response.json()["recipe"]["ingredients"]) == 0  # since full-resource=false by default.

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}?full-resource=false",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
        test_recipe_with_id["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        test_recipe_with_id["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        test_recipe_with_id["recipe"]["recipe-portions"] = test_recipe_with_id["recipe"]["servings"]
        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            test_recipe_with_id,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        response = await ac.delete(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            test_recipe_with_id,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )


@pytest.mark.asyncio
async def test_create_recipe_with_missing_fields(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    recipe_xid = "recipe01"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        for removed_field, removed_from in [
            ("unit", "ingredient"),
            ("location", "recipe"),
            ("transport", "ingredient"),
            ("conservation", "ingredient"),
        ]:
            test_recipe_without_prop = delete_field_from_recipe_or_ingredient(
                TEST_RECIPE, removed_field, delete_from=removed_from
            )
            test_recipe_without_prop["recipe"]["id"] = recipe_xid
            test_recipe_without_prop["recipe"]["kitchen-id"] = kitchen_xid

            response = await ac.put(
                f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
                auth=(customer_token, ""),
                json=test_recipe_without_prop,
            )
            response.raise_for_status()

            expected_resp_recipe = copy.deepcopy(test_recipe_without_prop)
            expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
            if removed_field == "transport":
                expected_co2 = TEST_RECIPE_WITHOUT_TRANSPORT_EXPECTED_CO2
                expected_food_unit = TEST_RECIPE_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [
                    int(TEST_RECIPE_WITHOUT_TRANSPORT_EXPECTED_CO2 - TEST_RECIPE_LEGACY_CO2_VALUE)
                    - 1,  # -1 from rounding
                    int(TEST_RECIPE_LEGACY_CO2_VALUE),
                ]
                expected_ingredients_food_unit = [TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT]
            elif removed_field == "unit":
                expected_co2 = int(TEST_RECIPE_EXPECTED_CO2 * 1000 / SERVINGS)
                expected_food_unit = round(TEST_RECIPE_EXPECTED_DFU, 2)
                expected_ingredients_co2_value = [0, expected_co2]
                expected_ingredients_food_unit = [
                    round(TEST_RECIPE_LEGACY_ING1_FOOD_UNIT / 1000, 2),
                    round(TEST_RECIPE_LEGACY_ING2_FOOD_UNIT / 1000, 2),
                ]
            elif removed_field == "conservation":
                expected_co2 = int(TEST_RECIPE_LEGACY_CO2_VALUE / ONION_DRYING_UPSCALING)
                expected_food_unit = TEST_RECIPE_NO_DRYING_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [0, expected_co2]
                expected_ingredients_food_unit = [
                    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
                    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT_NO_DRYING,
                ]
            else:
                expected_co2 = TEST_RECIPE_LEGACY_CO2_VALUE
                expected_food_unit = TEST_RECIPE_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [0, expected_co2]
                expected_ingredients_food_unit = [TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT]

            check_response_dict(
                expected_resp_recipe,
                response.json(),
                "recipe",
                expected_co2_value=expected_co2,
                expected_food_unit=expected_food_unit,
                expected_ingredients_co2_value=expected_ingredients_co2_value,
                expected_ingredients_food_unit=expected_ingredients_food_unit,
            )


@pytest.mark.asyncio
async def test_put_response_as_request_with_missing_fields(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    recipe_xid = "recipe01"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        test_recipe_without_transport_and_location = delete_field_from_recipe_or_ingredient(
            TEST_RECIPE, "transport", delete_from="ingredient"
        )
        test_recipe_without_transport_and_location = delete_field_from_recipe_or_ingredient(
            test_recipe_without_transport_and_location, "location", delete_from="recipe"
        )
        test_recipe_without_transport_and_location["recipe"]["id"] = recipe_xid
        test_recipe_without_transport_and_location["recipe"]["kitchen-id"] = kitchen_xid

        # if we remove transport and location, the expected co2 is higher. The reason is that
        # 1) In TEST_RECIPE we specify transport = 'air' for which we don't have the ecotransit response,
        # therefore, there is no transport in this case
        # 2) Even though we remove the location in test_recipe_without_transport_and_location, the location GFM
        # can identify the location from the access-group of the kitchen. Since we don't specify transport,
        # we will have truck-transport to this location.
        test_recipe_without_transport_and_location_expected_co2 = TEST_RECIPE_WITHOUT_TRANSPORT_EXPECTED_CO2

        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_without_transport_and_location,
        )
        first_response_json = response.json()
        expected_resp_recipe = copy.deepcopy(test_recipe_without_transport_and_location)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        assert response.status_code == 200
        check_response_dict(
            expected_resp_recipe,
            first_response_json,
            "recipe",
            expected_co2_value=test_recipe_without_transport_and_location_expected_co2,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                int(test_recipe_without_transport_and_location_expected_co2 - TEST_RECIPE_LEGACY_CO2_VALUE)
                - 1,  # -1 from rounding
                int(TEST_RECIPE_LEGACY_CO2_VALUE),
            ],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        # Add the information back again, then rerun.
        recipe_with_information_added_back = add_field_from_recipe_or_ingredient(
            first_response_json, "transport", ["air", "ground"], add_to="ingredient"
        )
        recipe_with_information_added_back = add_field_from_recipe_or_ingredient(
            recipe_with_information_added_back, "location", "Zürich, Schweiz", add_to="recipe"
        )
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
            auth=(customer_token, ""),
            json=recipe_with_information_added_back,
        )
        response.raise_for_status()

        check_response_dict(
            recipe_with_information_added_back,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_ingredients_co2_value=[0, None],
            response_as_request=True,
        )


@pytest.mark.asyncio
async def test_recipes_xid_linking(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    sub_recipe_xid = "test_recipe_id"

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = sub_recipe_xid

    unmatched_test_recipe_with_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    unmatched_recipe_id = "unmatched-recipe-id"
    del unmatched_test_recipe_with_subrecipe["request-id"]
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["id"] = unmatched_recipe_id
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = [
        {"language": "de", "value": "K\u00fcrbisrisotto"}
    ]
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["type"] = "conceptual-ingredients"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{sub_recipe_xid}",
            auth=(customer_token, ""),
            json=test_recipe_with_id,
        )

        assert response.status_code == 200

        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )

        assert response.status_code == 200
        assert response.json()["recipe"]["co2-value"] is None

    unmatched_recipe_uid = await postgres_db.product_mgr.find_uid_by_xid(customer_uid, unmatched_recipe_id)
    edges = await postgres_db.graph_mgr.get_sub_graph_by_uid(unmatched_recipe_uid, max_depth=1)
    assert len(edges) == 1
    flow_node: FlowNode = await postgres_db.graph_mgr.find_by_uid(edges[0].child_uid)
    flow_node.link_to_sub_node = LinkToXidProp(xid=sub_recipe_xid)

    await postgres_db.graph_mgr.upsert_node_by_uid(flow_node)

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        assert response.status_code == 200
        expected_resp_recipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        check_response_dict(
            expected_resp_recipe,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
            ],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
            ],
        )

        # Test with a new product name. Should be unmatched, and no co2 values should be returned.
        unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"][0] = {
            "language": "de",
            "value": "Not K\u00fcrbisrisotto",
        }
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        assert response.status_code == 200
        assert response.json()["recipe"]["co2-value"] is None


@pytest.mark.asyncio
async def test_recipes_batch(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = "test_recipe_id"

    test_recipe_circular = copy.deepcopy(TEST_RECIPE)
    test_recipe_circular["recipe"]["id"] = "test_recipe_id"
    test_recipe_circular["recipe"]["ingredients"].append(
        {
            "transport": "air",
            "processing": "raw",
            "packaging": "plastic",
            "id": "test_recipe_with_subrecipe_id",
            "names": [{"language": "de", "value": "Parent Recipe with child recipes"}],
            "location": "spain",
            "production": "greenhouse",
            "conservation": "fresh",
            "type": "recipes",
            "amount": 50,
            "unit": "kilogram",
        },
    )

    test_recipe_with_wrong_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_wrong_subrecipe["recipe"]["ingredients"][0]["id"] = "wrong-recipe-id"

    test_recipe_transient: dict[str, dict | bool] = copy.deepcopy(test_recipe_with_id)
    test_recipe_transient["recipe"]["id"] = "test-recipe-transient"
    test_recipe_transient["transient"] = True

    test_recipe_with_subrecipe_transient = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_transient["recipe"]["ingredients"][0]["id"] = "test-recipe-transient"

    test_recipe_all_transient: dict[str, dict | bool] = copy.deepcopy(test_recipe_with_id)
    test_recipe_all_transient["recipe"]["id"] = "test-recipe-all-transient"
    test_recipe_with_subrecipe_all_transient = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_all_transient["recipe"]["ingredients"][0]["id"] = "test-recipe-all-transient"
    test_recipe_with_subrecipe_all_transient["recipe"]["id"] = "test_recipe_with_subrecipe_all_transient"

    test_recipe_with_subrecipe_no_id = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    del test_recipe_with_subrecipe_no_id["recipe"]["id"]

    circular_batch = [{**test_recipe_circular}, {**TEST_RECIPE_WITH_SUBRECIPE}]
    incorrectly_linked_batch = [{**test_recipe_with_id}, {**test_recipe_with_wrong_subrecipe}]
    repeated_batch = [
        {**test_recipe_with_id},
        {**test_recipe_with_id},
        {**TEST_RECIPE_WITH_SUBRECIPE},
        {**TEST_RECIPE_WITH_SUBRECIPE},
    ]
    valid_batch = [{**test_recipe_with_subrecipe_no_id}, {**test_recipe_with_id}]

    test_recipe_with_subrecipe_new_amount = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    for ingredient in test_recipe_with_subrecipe_new_amount["recipe"]["ingredients"]:
        ingredient["amount"] *= 2
    test_recipe_with_subrecipe_new_amount["request-id"] = 0

    valid_batch_with_repeated_recipe_id = [
        {**TEST_RECIPE_WITH_SUBRECIPE},
        {**test_recipe_with_id},
        {**test_recipe_with_subrecipe_new_amount},
    ]
    transient_batch = [{**test_recipe_transient}, {**test_recipe_with_subrecipe_transient}]
    all_transient_batch = [{**test_recipe_all_transient}, {**test_recipe_with_subrecipe_all_transient}]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response_circular = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=circular_batch,
        )

        response_repeated = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=repeated_batch,
        )

        response_unlinked = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=incorrectly_linked_batch,
        )

        response_valid = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=valid_batch,
        )

        response_valid_with_repeated_recipe_id = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=valid_batch_with_repeated_recipe_id,
        )

        response_transient = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=transient_batch,
        )

        response_transient_all = await ac.post(
            "/api/recipes/batch?full-resource=true&transient=true",
            auth=(customer_token, ""),
            json=all_transient_batch,
        )

    # Assert that request ID is arranged in ascending order.
    assert response_circular.json()[0]["request-id"] < response_circular.json()[1]["request-id"]

    assert response_circular.json()[0]["statuscode"] == 400
    assert response_circular.json()[1]["statuscode"] == 400

    response_repeated_json = response_repeated.json()
    assert response_repeated_json[0]["statuscode"] == 200
    assert response_repeated_json[1]["statuscode"] == 200
    top_recipe = response_repeated_json[0]
    sub_recipe = response_repeated_json[1]

    assert top_recipe["message"].startswith("Request ID '1' in multiple batch items.")
    assert sub_recipe["message"].startswith("Recipe ID 'test_recipe_id' in multiple batch items.")

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["ingredients"] = []
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
    )

    expected_resp_recipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    expected_resp_recipe["recipe"]["ingredients"] = []
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
    )

    invalid_item = response_unlinked.json()[0]
    valid_item = response_unlinked.json()[1]
    assert invalid_item["statuscode"] == 400

    # TODO: we should instead also check these two fields for correctness:
    del valid_item["request-id"]
    del valid_item["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["recipe-id"] = "test_recipe_id"
    expected_resp_recipe["message"] = ""
    check_response_dict(
        expected_resp_recipe,
        valid_item,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
    )

    response_valid_json = response_valid.json()
    top_recipe = response_valid_json[0]
    sub_recipe = response_valid_json[1]
    assert response_valid_json[0]["request-id"] < response_valid_json[1]["request-id"]

    assigned_recipe_uid = uuid.UUID(response_valid_json[0]["recipe"]["id"])
    assert isinstance(assigned_recipe_uid, uuid.UUID)
    assert str(assigned_recipe_uid) == response_valid_json[0]["recipe"]["id"]

    with pytest.raises(ValueError):
        uuid.UUID(response_valid_json[1]["recipe"]["id"])

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]
    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
    )

    test_recipe_with_subrecipe_with_new_id = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_with_new_id["recipe"]["id"] = str(assigned_recipe_uid)
    test_recipe_with_subrecipe_with_new_id["recipe"]["recipe-portions"] = test_recipe_with_subrecipe_with_new_id[
        "recipe"
    ]["servings"]
    check_response_dict(
        test_recipe_with_subrecipe_with_new_id,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
        ],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
        ],
    )

    response_valid_with_repeated_recipe_id_json = response_valid_with_repeated_recipe_id.json()
    top_recipe = response_valid_with_repeated_recipe_id_json[0]
    top_recipe_2 = response_valid_with_repeated_recipe_id_json[1]
    sub_recipe = response_valid_with_repeated_recipe_id_json[2]

    assert top_recipe["message"].startswith("Recipe ID 'test_recipe_with_subrecipe_id' in multiple batch items.")
    assert top_recipe_2["message"].startswith("Recipe ID 'test_recipe_with_subrecipe_id' in multiple batch items.")
    assert sub_recipe["message"] == ""

    for recipe in (top_recipe, top_recipe_2, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_new_amount)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE * 2 + 1,  # 1 due to rounding
        expected_food_unit=round(TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT * 2, 2),
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE * 2 + 1,  # 1 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE * 2 + 1,  # 1 due to rounding.
        ],
        expected_ingredients_food_unit=[
            round(TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT * 2 - 0.01, 2),  # 0.01 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT * 2,
        ],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_new_amount)
    expected_resp_recipe["request-id"] = 1
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe_2,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE * 2 + 1,  # 1 due to rounding
        expected_food_unit=round(TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT * 2, 2),
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE * 2 + 1,  # 1 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE * 2 + 1,  # 1 due to rounding.
        ],
        expected_ingredients_food_unit=[
            round(TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT * 2 - 0.01, 2),  # 0.01 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT * 2,
        ],
    )

    assert response_transient.status_code == 200
    assert response_transient_all.status_code == 200
    response_transient_all_json = response_transient_all.json()

    top_recipe = response_transient_all_json[0]
    sub_recipe = response_transient_all_json[1]

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_all_transient)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_all_transient)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_food_unit=TEST_RECIPE_WITH_SUB_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
        ],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
        ],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        get_response_sub_recipe = await ac.get(
            "api/recipes/test_recipe_id?full-resource=true",
            auth=(customer_token, ""),
        )

        response_json = get_response_sub_recipe.json()
        del response_json["recipe"]["kitchen-id"]
        expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        check_response_dict(
            expected_resp_recipe,
            response_json,
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_CO2_VALUE],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        get_response_transient_recipe = await ac.get(
            "api/recipes/test-recipe-transient?full-resource=true",
            auth=(customer_token, ""),
        )

        assert get_response_transient_recipe.status_code == 404

        get_response_all_transient_recipe = await ac.get(
            "api/recipes/test_recipe_with_subrecipe_all_transient?full-resource=true",
            auth=(customer_token, ""),
        )

        assert get_response_all_transient_recipe.status_code == 404


@pytest.mark.asyncio
async def test_recipes_batch_with_repeated_ingredients(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    onion_expected_co2 = 0.3089234344936829 * 1000
    onion_expected_dfu = 0.2104259687272727
    dfu_reduction_due_to_weight = 0.033333333333333  # DFU is smaller since (weight) < (sum of ingredient weights).
    carrot_expected_co2 = 0.012979745398003872 * 1000
    carrot_expected_dfu = 0.19288297842424243

    sub_recipe_1003 = {
        "request-id": 4,
        "recipe": {
            "id": "1003",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "weight": 900.0,
            "recipe-portions": 1,
            "production-portions": 120,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 1000.0,
                    "unit": "gram",
                },
            ],
        },
    }
    sub_recipe_1004 = {
        "request-id": 5,
        "recipe": {
            "id": "1004",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "recipe-portions": 1,
            "production-portions": 120,
            "weight": 1500.0,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    # names field is overwritten by the second "onion".
                    "names": [{"language": "de", "value": "UNMATCHED UNKNOWN"}],
                    "amount": 400.0,
                    "unit": "gram",
                },
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 600.0,
                    "unit": "gram",
                },
                {
                    "id": "carrot",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Karotten"}],
                    "amount": 500.0,
                    "unit": "gram",
                },
            ],
        },
    }

    recipe_2006 = {
        "request-id": 6,
        "recipe": {
            "id": "2006",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Menu1"}],
            "servings": 100,
            "production-portions": 0,
            "sold-portions": 50,
            "weight": 579.0,
            "date": "2000-08-17",
            "ingredients": [
                {"id": "1003", "type": "recipes", "amount": 12300.0, "unit": "gram"},
                {"id": "1004", "type": "recipes", "amount": 45600.0, "unit": "gram"},
            ],
        },
    }

    batch_with_repeated_ingredients = [sub_recipe_1003, sub_recipe_1004, recipe_2006]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=batch_with_repeated_ingredients,
        )

    assert response.status_code == 200

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1003",
            auth=(customer_token, ""),
        )
    response_json_1003 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1003)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1003,
        "recipe",
        expected_co2_value=int(onion_expected_co2),
        expected_food_unit=round(onion_expected_dfu - dfu_reduction_due_to_weight, 2),
        expected_ingredients_co2_value=[int(onion_expected_co2)],
        expected_ingredients_food_unit=[round(onion_expected_dfu, 2)],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1004",
            auth=(customer_token, ""),
        )
    response_json_1004 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1004)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1004,
        "recipe",
        expected_co2_value=int(onion_expected_co2 + 0.5 * carrot_expected_co2),
        expected_food_unit=round(onion_expected_dfu + 0.5 * carrot_expected_dfu, 2),
        expected_ingredients_co2_value=[
            int(0.4 * onion_expected_co2),
            int(0.6 * onion_expected_co2),
            int(0.5 * carrot_expected_co2),
        ],
        expected_ingredients_food_unit=[
            round(0.4 * onion_expected_dfu, 2),
            round(0.6 * onion_expected_dfu, 2),
            round(0.5 * carrot_expected_dfu, 2),
        ],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/2006",
            auth=(customer_token, ""),
        )
    response_json_2006 = response.json()

    expected_resp_recipe = copy.deepcopy(recipe_2006)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_2006,
        "recipe",
        expected_co2_value=int(
            (onion_expected_co2 + 0.5 * carrot_expected_co2) * 0.456 / 1.5 + onion_expected_co2 * 0.123 / 0.9
        ),
        expected_food_unit=round(
            (onion_expected_dfu + 0.5 * carrot_expected_dfu) * 45.6 / 1.5
            + (onion_expected_dfu - dfu_reduction_due_to_weight) * (12.3 / 0.9),
            2,
        ),
        expected_ingredients_co2_value=[
            int(onion_expected_co2 * 0.123 / 0.9),
            int((onion_expected_co2 + 0.5 * carrot_expected_co2) * 0.456 / 1.5),
        ],
        expected_ingredients_food_unit=[
            round((onion_expected_dfu - dfu_reduction_due_to_weight) * (12.3 / 0.9), 2),
            round((onion_expected_dfu + 0.5 * carrot_expected_dfu) * 45.6 / 1.5, 2),
        ],
    )
