from enum import Enum
from typing import Dict, List, Optional

from pydantic import BaseModel, ConfigDict, Field, model_validator

from legacy_api.dto.legacy.indicators import WrappedIndicatorsForIngredientsDto

from .climate_score import ClimateScoreDto
from .localized_name import LocalizedNameDto
from .nutrient_value import NutrientValueDto


class IngredientTypeDtoEnum(str, Enum):
    conceptual_ingredients = "conceptual-ingredients"
    recipes = "recipes"


class IngredientDto(BaseModel):
    id: str
    type: Optional[IngredientTypeDtoEnum] = IngredientTypeDtoEnum.conceptual_ingredients
    names: Optional[List[LocalizedNameDto]] = None
    amount: float
    unit: Optional[str] = None
    origin: Optional[str] = None
    transport: Optional[str] = None
    production: Optional[str] = None
    producer: Optional[str] = None
    processing: Optional[str] = None
    conservation: Optional[str] = None
    packaging: Optional[str] = None
    ingredients_declaration: Optional[str] = Field(None, alias="ingredients-declaration")
    nutrient_values: Optional[NutrientValueDto] = Field(None, alias="nutrient-values")
    gtin: Optional[str] = None

    @model_validator(mode="after")
    def validate_names(self) -> "IngredientDto":
        """
        Makes sure that names field is set if type is conceptual_ingredients
        """
        if self.type == IngredientTypeDtoEnum.conceptual_ingredients and not self.names:
            raise ValueError("Names field is required for conceptual_ingredients type")
        return self

    def get_ingredient_meta_data(self) -> Dict:
        return self.model_dump(exclude={"id", "type"})

    model_config = ConfigDict(populate_by_name=True)


class IngredientResultDto(IngredientDto, ClimateScoreDto, WrappedIndicatorsForIngredientsDto):
    pass
