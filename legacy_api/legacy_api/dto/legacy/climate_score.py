from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class ClimateScoreDto(BaseModel):
    co2_value: Optional[int] = Field(None, alias="co2-value")
    info_text: Optional[str] = Field(None, alias="info-text")
    model_config = ConfigDict(populate_by_name=True)
