from pydantic import BaseModel


class LocalizedNameDto(BaseModel):
    language: str
    value: str
