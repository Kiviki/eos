from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class EnvironmentDto(BaseModel):
    scarce_water_liters: Optional[float] = Field(default=None, alias="scarce-water-liters")
    rainforest_label: Optional[bool] = Field(default=None, alias="rainforest-label")
    rainforest_rating: Optional[str] = Field(default=None, alias="rainforest-rating")
    animal_treatment_label: Optional[bool] = Field(default=None, alias="animal-treatment-label")
    animal_treatment_rating: Optional[str] = Field(default=None, alias="animal-treatment-rating")
    model_config = ConfigDict(populate_by_name=True)


class IndicatorsDto(BaseModel):
    # vita_score: Optional[VitaScoreDto] = Field(None, alias="vita-score")
    vita_score: Optional[float] = Field(None, alias="vita-score")
    environment: Optional[EnvironmentDto] = Field(default=None, alias="environment")
    model_config = ConfigDict(populate_by_name=True)


class WrappedIndicatorsDto(BaseModel):
    food_unit: Optional[float] = Field(None, alias="food-unit")
    indicators: Optional[IndicatorsDto] = Field(None, alias="indicators")
    model_config = ConfigDict(populate_by_name=True)


class WrappedIndicatorsForIngredientsDto(BaseModel):
    """In the legacy API, daily food unit is called food-unit in top recipe and foodUnit in ingredients."""

    food_unit: Optional[float] = Field(None, alias="foodUnit")
    indicators: Optional[IndicatorsDto] = Field(None, alias="indicators")
    model_config = ConfigDict(populate_by_name=True)
