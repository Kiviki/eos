# TODO: not implemented yet
# from pydantic import BaseModel, Field
#
#
# class EnvironmentalFootprintsDto(BaseModel):
#     animal_treatment_label: bool = Field(alias="animal-treatment-label")
#     animal_treatment_rating: str = Field(alias="animal-treatment-rating")
#     rainforest_label: bool = Field(alias="rainforest-label")
#     rainforest_rating: str = Field(alias="rainforest-rating")
#     local_label: bool = Field(alias="local-label")
#     local_rating: str = Field(alias="local-rating")
#     season_label: bool = Field(alias="season-label")
#     season_rating: str = Field(alias="season-rating")
#     scarce_water_liters: int = Field(alias="scarce-water-liters")
#     water_footprint_rating: str = Field(alias="water-footprint-rating")
#     water_footprint_award: bool = Field(alias="water-footprint-award")
#
#     class Config:
#         allow_population_by_field_name = True
