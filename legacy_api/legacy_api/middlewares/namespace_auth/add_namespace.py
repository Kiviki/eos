from fastapi import Request
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware
from structlog import get_logger

from legacy_api.app.settings import Settings

logger = get_logger()
settings = Settings()

PREFIX = "Basic "


class NamespaceAuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if (
            request.url.path == f"{settings.BASE_PATH}"
            or request.url.path == f"{settings.BASE_PATH}/status"
            or request.url.path == f"{settings.BASE_PATH}/openapi.json"
            or request.url.path.startswith(f"{settings.BASE_PATH}/docs")
            or request.url.path.startswith(f"{settings.BASE_PATH}/redoc")
        ):
            # exclude root status / heartbeat page from authentication filter:
            return await call_next(request)
        else:
            authorization = request.headers.get("authorization")

            if authorization is None:
                return JSONResponse(status_code=401, content={"reason": "missing authorization header"})

            if not authorization.startswith(PREFIX):
                return JSONResponse(
                    status_code=401,
                    content={"reason": "authorization header must start with Basic"},
                )

            request.state.token = authorization[len(PREFIX) :]

            response = await call_next(request)

        return response
