import base64

from pydantic_settings import BaseSettings, SettingsConfigDict

from core.envprops import EnvProps


class Settings(EnvProps):
    TITLE: str = "ayce"

    JSON_LOGGING: bool = False
    LOG_LEVEL: str = "DEBUG"
    BASE_PATH: str = "/v2"

    @property
    def EATERNITY_AUTH_KEY(self) -> str:
        basic_auth_key = base64.b64encode(f"{self.EATERNITY_TOKEN}:".encode("utf-8"))
        return basic_auth_key.decode("utf-8")

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")
