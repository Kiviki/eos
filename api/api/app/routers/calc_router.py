import asyncio
import uuid
from typing import List

from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_access_controller, get_node_controller
from api.controller.access_group_controller import AccessGroupController
from api.controller.node_controller import NodeController
from api.dto.v2.node_dto import (
    BatchInputDto,
    BatchOutputDto,
    BatchWithExistingNodeInputItemDto,
    BatchWithExistingNodeOutputItemDto,
    BatchWithExistingNodesInputDto,
    BatchWithExistingNodesOutputDto,
    ExistingNodeDto,
)
from core.domain.calculation import Calculation
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.root_with_subflows_dto import ExistingRootDto
from core.service.permission_service import (
    check_if_can_delete_node,
    check_if_can_post_node,
    check_if_can_read_node,
    check_if_can_update_node,
)

logger = get_logger()

calc_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)

# FIXME: check if we can remove the unused arguments (ARG001)


async def determine_namespace_uid_and_access_group_uid(
    access_controller: AccessGroupController,
    node_controller: NodeController,
    item_in: Calculation | BatchWithExistingNodeInputItemDto,
    request: Request,
) -> Calculation:
    """Helper function to determine the namespace_uid and access_group_uid for a given item_in."""

    async def _find_access_group_uid_from_xid() -> None:
        access_group = await access_controller.get_access_group_by_xid(
            node_info.access_group_xid, item_in.namespace_uid
        )
        # get this access group's UUID and verify that it exists
        node_info.access_group_uid = uuid.UUID(access_group.uid)
        # make sure that the created node is in the correct namespace of the given access group:
        item_in.namespace_uid = access_group.namespace_uid

    async def _find_access_group_xid_from_uid() -> None:
        # get this group's XID and namespace UUID and verify that this access group exists
        group_with_xid = await access_controller.get_access_group_by_uid(str(node_info.access_group_uid))
        node_info.access_group_xid = group_with_xid.xid
        # make sure that the created node is in the correct namespace of the given access group:
        item_in.namespace_uid = group_with_xid.namespace_uid

    def _set_legacy_access_group_uid_and_namespace_uid() -> None:
        node_info.access_group_uid = uuid.UUID(request.state.user_legacy_default_access_group.uid)
        item_in.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if item_in.namespace_uid is None:
        item_in.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if isinstance(item_in, Calculation):
        root = item_in.input_root
    else:
        root = item_in.nodes_dto

    if isinstance(root, ExistingRootDto):
        node_info = root.existing_root
        existing_node = True
    elif isinstance(root, ExistingNodeDto):
        node_info = root.existing_node
        existing_node = True
    else:
        node_info = root.activity
        existing_node = False

    if node_info.uid is None:
        if node_info.xid is None:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="a node uid or xid is required")
        node_info.uid = await node_controller.get_uid_by_xid(item_in.namespace_uid, node_info.xid)

    if node_info.uid is not None:
        try:
            node_access_group_uid_from_db = await node_controller.node_service.find_access_group_uid_by_uid(
                node_info.uid
            )
        except (KeyError, TypeError) as e:
            raise PermissionError("access_group_uid is not set for this node uid.") from e
    else:
        node_access_group_uid_from_db = None

    if existing_node:
        if node_info.access_group_uid is not None:
            await _find_access_group_xid_from_uid()
        elif node_info.access_group_xid is not None:
            await _find_access_group_uid_from_xid()
        elif node_access_group_uid_from_db is not None:
            node_info.access_group_uid = node_access_group_uid_from_db
            await _find_access_group_xid_from_uid()
        else:
            _set_legacy_access_group_uid_and_namespace_uid()

        if node_access_group_uid_from_db and node_info.access_group_uid != node_access_group_uid_from_db:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="node was not found in that access_group")
    else:
        if node_access_group_uid_from_db is not None:
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state, str(node_access_group_uid_from_db)
            )
            if not check_if_can_update_node(permissions):
                raise PermissionError("user has no permission to update the access_group of node.")

        if node_info.access_group_xid is None and node_info.access_group_uid is None:
            # in case no group_id was specified, we use the default access_group:
            _set_legacy_access_group_uid_and_namespace_uid()

        if node_info.access_group_xid:
            await _find_access_group_uid_from_xid()

        elif node_info.access_group_uid:
            await _find_access_group_xid_from_uid()

    return item_in


@calc_router.get("/graphs/{calculation_uid}")
async def get_calculation_result(
    calculation_uid: str,
    request: Request,  # noqa: ARG001
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008, ARG001
) -> Calculation:
    """Get the result of a calculation."""
    loaded_calculation_result, access_group_uid = await node_controller.load_old_calculation_result(calculation_uid)
    if not loaded_calculation_result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"{calculation_uid} is not found.")

    permissions = await access_controller.get_user_permissions_by_uid(request.state, str(access_group_uid))

    if check_if_can_read_node(permissions):
        return loaded_calculation_result
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@calc_router.post("/graphs")
async def create_and_calculate_graphs(
    batch_input_dto: BatchInputDto,
    request: Request,
    return_immediately: bool = False,
    use_queue: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
) -> BatchOutputDto:
    """Generate a graph or multiple graphs and execute the orchestrator on the specified graph(s)."""
    logger.info("called create_and_calculate_graphs")

    # first pass: create all nodes (so that they can link to each other in the second pass)
    output_items: List[Calculation] = []
    for item_in in batch_input_dto.batch:
        item_out: Calculation = item_in.model_copy(deep=True)
        try:
            item_in = await determine_namespace_uid_and_access_group_uid(
                access_controller, node_controller, item_in, request
            )
            old_access_group_permission_granted = True
        except PermissionError:
            old_access_group_permission_granted = False
            item_out.statuscode = status.HTTP_401_UNAUTHORIZED
            if type(item_in.input_root) is ExistingRootDto:
                node_uid = item_in.input_root.existing_root.uid
            else:
                node_uid = item_in.input_root.activity.uid

            item_out.message = f"Node uid {node_uid} already exists in the database under a different access group."
            output_items.append(item_out)
        except HTTPException as e:
            old_access_group_permission_granted = False
            item_out.statuscode = e.status_code
            item_out.message = e.detail
            output_items.append(item_out)

        if not old_access_group_permission_granted:
            continue

        if type(item_in.input_root) is ExistingRootDto:
            input_existing_root_dto = item_in.input_root.existing_root
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state, str(input_existing_root_dto.access_group_uid)
            )
            permission_granted = check_if_can_read_node(permissions)
            if permission_granted:
                root_flow_or_activity = await node_controller.get_recipe_node(item_in.namespace_uid, item_in.input_root)

                if root_flow_or_activity:
                    # Check if the retrieved node is really in the requested access group:
                    if root_flow_or_activity.access_group_uid != item_in.input_root.existing_root.access_group_uid:
                        root_flow_or_activity = None

                if not root_flow_or_activity:
                    item_out.statuscode = status.HTTP_404_NOT_FOUND
                    item_out.message = "The requested node does not exist."
                    output_items.append(item_out)
                    continue
                else:
                    if isinstance(root_flow_or_activity, ActivityNode):
                        item_out.child_of_root_node_uid = root_flow_or_activity.uid
                        if root_flow_or_activity.get_parent_nodes():
                            item_out.root_node_uid = root_flow_or_activity.get_parent_nodes()[0].uid
                    elif isinstance(root_flow_or_activity, FlowNode):
                        item_out.root_node_uid = root_flow_or_activity.uid
                        item_out.child_of_root_node_uid = root_flow_or_activity.get_sub_nodes()[0].uid

        else:
            activity = item_in.input_root.activity
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state, str(activity.access_group_uid)
            )
            permission_granted = check_if_can_post_node(permissions)
            if permission_granted:
                root_with_sub_flows = await node_controller.upsert_recipe_node(item_in)
                item_out.child_of_root_node_uid = root_with_sub_flows.activity.uid
                if root_with_sub_flows.flow:
                    item_out.root_node_uid = root_with_sub_flows.flow.uid

        if not permission_granted:
            item_out.statuscode = status.HTTP_401_UNAUTHORIZED
            item_out.message = "You don't have rights for this action."
            output_items.append(item_out)
            continue

        item_out.statuscode = status.HTTP_200_OK
        item_out.message = ""
        output_items.append(item_out)

    # second pass: run orchestrator for each item in the batch:
    for index, calculation in enumerate(output_items):
        if not calculation.skip_calc and calculation.statuscode == status.HTTP_200_OK:
            calculation = await node_controller.init_calculation(calculation)
            if return_immediately:
                asyncio.create_task(node_controller.perform_calculation(calculation, use_queue=use_queue))
            else:
                calculation = await node_controller.perform_calculation(calculation, use_queue=use_queue)
                output_items[index] = calculation

    # third pass: set transient as deleted.
    for index, calculation in enumerate(output_items):
        if calculation.transient and calculation.statuscode == status.HTTP_200_OK:
            await node_controller.node_service.upsert_by_recipe_id(
                recipe_node=batch_input_dto.batch[index].input_root.activity, transient=True
            )

    return BatchOutputDto(batch=output_items)


@calc_router.post("/delete_graphs")
async def delete_graphs(
    batch_input_dto: BatchWithExistingNodesInputDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
) -> BatchOutputDto:
    """Delete the nodes in a graph or in multiple graphs."""
    logger.info("called delete_graphs")
    output_items: List[BatchWithExistingNodeOutputItemDto] = []
    for item_in in batch_input_dto.batch:
        try:
            item_in = await determine_namespace_uid_and_access_group_uid(
                access_controller, node_controller, item_in, request
            )
            old_access_group_permission_granted = True
        except PermissionError:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message=f"Node uid {item_in.nodes_dto.existing_node.uid} already exists in the database under"
                    " a different access group.",
                    request_id=item_in.request_id,
                )
            )
            old_access_group_permission_granted = False
        except HTTPException as e:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=e.status_code,
                    message=e.detail,
                    request_id=item_in.request_id,
                )
            )
            old_access_group_permission_granted = False

        if not old_access_group_permission_granted:
            continue

        existing_node = item_in.nodes_dto.existing_node
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state, str(existing_node.access_group_uid)
        )
        if check_if_can_delete_node(permissions):
            if existing_node.xid:
                result = await node_controller.delete_node_by_xid(existing_node.xid, item_in.namespace_uid)
            else:
                result = await node_controller.delete_node_by_uid(existing_node.uid)
            if not result:
                output_items.append(
                    BatchWithExistingNodeOutputItemDto(
                        statuscode=status.HTTP_400_BAD_REQUEST,
                        message="",
                        request_id=item_in.request_id,
                    )
                )
            else:
                output_items.append(
                    BatchWithExistingNodeOutputItemDto(
                        statuscode=status.HTTP_200_OK,
                        message="",
                        request_id=item_in.request_id,
                    )
                )
        else:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message="You don't have rights for this action.",
                    request_id=item_in.request_id,
                )
            )
    return BatchWithExistingNodesOutputDto(batch=output_items)
