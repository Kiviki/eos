import base64

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from structlog import get_logger

from api.app.injector import get_admin_controller
from api.app.settings import Settings
from api.controller.admin_controller import AdminController

logger = get_logger()
v2_router = APIRouter()
oauth_schema = HTTPBearer()
settings = Settings()


@v2_router.post("/reset_db")
async def reset_db(
    admin_controller: AdminController = Depends(get_admin_controller),
    credentials: HTTPAuthorizationCredentials = Depends(oauth_schema),
) -> str:
    logger.info("authenticated credentials: ", credentials=credentials.credentials)

    try:
        username = base64.b64decode(credentials.credentials)
    except base64.binascii.Error:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect API Key",
            headers={"WWW-Authenticate": "Bearer"},
        )

    if username != settings.EATERNITY_USERNAME + ":":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect API Key",
            headers={"WWW-Authenticate": "Bearer"},
        )
    await admin_controller.reset_db()

    return "done"
