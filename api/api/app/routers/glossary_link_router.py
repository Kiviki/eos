from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.security import APIKeyHeader
from starlette import status
from structlog import get_logger

from api.app.injector import get_access_controller, get_glossary_link_controller
from api.app.settings import Settings
from api.controller.access_group_controller import AccessGroupController
from api.controller.glossary_link_controller import GlossaryLinkController
from api.dto.v2.glossary_dto import GetAllGlossaryLinksDto, GlossaryLinkBatchDeleteDto, GlossaryLinkDto

logger = get_logger()
settings = Settings()

glossary_link_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@glossary_link_router.get("/by-gap-filling-module/{gap_filling_module}", response_model=GetAllGlossaryLinksDto)
async def get_all_glossary_link(
    gap_filling_module: str,
    credentials: APIKeyHeader = Depends(api_key_header),
    glossary_link_controller: GlossaryLinkController = Depends(get_glossary_link_controller),
) -> GetAllGlossaryLinksDto:
    return await glossary_link_controller.get_glossary_links_by_gfm(gap_filling_module)


@glossary_link_router.post("/")
async def create_glossary_link(
    request: Request,
    glossary_link_dto: GlossaryLinkDto,
    access_controller: AccessGroupController = Depends(get_access_controller),
    glossary_link_controller: GlossaryLinkController = Depends(get_glossary_link_controller),
) -> None:
    # only superusers can create glossary links for now
    if request.state.is_superuser:
        await glossary_link_controller.put_glossary_link(glossary_link_dto)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@glossary_link_router.delete("/batch-delete-glossary-links", status_code=204, response_class=Response)
async def delete_glossary_links_by_uids(
    request: Request,
    glossary_link_batch_delete_dto: GlossaryLinkBatchDeleteDto,
    glossary_link_controller: GlossaryLinkController = Depends(get_glossary_link_controller),
) -> None:
    # only superusers can delete glossary links for now
    if request.state.is_superuser:
        await glossary_link_controller.delete_glossary_links_by_uids(glossary_link_batch_delete_dto.uids)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )
