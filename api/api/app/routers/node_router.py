from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_access_controller, get_node_controller
from api.controller.access_group_controller import AccessGroupController
from api.controller.node_controller import NodeController
from api.dto.v2.access_group_dto import AccessGroupId, WrappedAccessGroupIdDto
from core.domain.nodes.node import Node
from core.service.permission_service import check_if_can_read_node

logger = get_logger()

node_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@node_router.post("/get_nodes")
async def get_all_nodes(
    request: Request,
    wrapped_access_group: Optional[WrappedAccessGroupIdDto] = None,
    node_type: Optional[str] = None,
    credentials: APIKeyHeader = Depends(api_key_header),
    node_controller: NodeController = Depends(get_node_controller),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> List[Node]:
    """Get all nodes in an access group."""
    if not wrapped_access_group:
        wrapped_access_group = WrappedAccessGroupIdDto(access_group=AccessGroupId())
    if not wrapped_access_group.namespace_uid:
        wrapped_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if not wrapped_access_group.access_group.xid and not wrapped_access_group.access_group.uid:
        wrapped_access_group.access_group.uid = request.state.user_legacy_default_access_group.uid

    logger.info(f"called get_nodes with credentials: {credentials}")
    logger.info(f"called get_nodes with namespace: {wrapped_access_group.namespace_uid}")

    if wrapped_access_group.access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_access_group.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_access_group.access_group.xid, wrapped_access_group.namespace_uid
            )
        ).uid
    else:
        # verify that this access group exists
        await access_controller.get_access_group_by_uid(wrapped_access_group.access_group.uid)

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_access_group.access_group.uid
    )

    if check_if_can_read_node(permissions):
        nodes = await node_controller.get_all_nodes_by_access_group_uid(
            wrapped_access_group.access_group.uid, node_type=node_type
        )
        return nodes
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )
