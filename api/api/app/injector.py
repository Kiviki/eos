from functools import lru_cache

from structlog import get_logger

from api.controller.access_group_controller import AccessGroupController
from api.controller.admin_controller import AdminController
from api.controller.auth_controller import AuthController
from api.controller.node_controller import NodeController
from core.service.calc_service import CalcService
from core.service.ingredients_declaration_service import IngredientsDeclarationMappingService
from core.service.service_provider import ServiceLocator, create_service_provider
from core.service.user_service import UserService

from ..controller.glossary_controller import GlossaryController
from ..controller.glossary_link_controller import GlossaryLinkController
from ..controller.ingredients_declaration_controller import IngredientsDeclarationMappingController
from ..controller.matching_controller import MatchingController

logger = get_logger()

service_locator = ServiceLocator()


@lru_cache()
def get_admin_controller() -> AdminController:
    logger.info("called get_admin_controller!")
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    access_group_service = service_provider.access_group_service
    namespace_service = service_provider.namespace_service
    user_mgr = postgres_db.get_user_mgr()
    user_service = UserService(user_mgr)
    controller = AdminController(postgres_db, access_group_service, namespace_service, user_service)

    return controller


@lru_cache()
def get_access_controller() -> AccessGroupController:
    logger.info("called get_access_group_controller!")
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    access_group_service = service_provider.access_group_service
    controller = AccessGroupController(access_group_service)

    return controller


@lru_cache()
def get_auth_controller() -> AuthController:
    logger.info("called get_auth_controller!")
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    user_mgr = postgres_db.get_user_mgr()
    user_service = UserService(user_mgr)
    controller = AuthController(user_service)

    return controller


@lru_cache()
def get_glossary_controller() -> GlossaryController:
    logger.info("called get_glossary_controller!")
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service
    controller = GlossaryController(glossary_service)

    return controller


@lru_cache()
def get_glossary_link_controller() -> GlossaryLinkController:
    logger.info("called get_glossary_link_controller!")
    service_provider = service_locator.service_provider
    glossary_link_service = service_provider.glossary_link_service
    controller = GlossaryLinkController(glossary_link_service)
    return controller


@lru_cache()
def get_node_controller() -> NodeController:
    logger.info("called get_node_controller!")
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    glossary_service = service_provider.glossary_service
    gap_filling_module_loader = service_provider.gap_filling_module_loader
    product_mgr = postgres_db.get_product_mgr()
    pg_graph_mgr = postgres_db.get_graph_mgr()
    node_service = service_provider.node_service
    access_group_service = service_provider.access_group_service
    pg_calc_mgr = postgres_db.get_calc_mgr()
    pg_term_mgr = postgres_db.get_term_mgr()
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )
    controller = NodeController(node_service, calc_service, access_group_service, glossary_service, product_mgr)

    return controller


@lru_cache()
def get_matching_item_controller() -> MatchingController:
    logger.info("called get_matching_item_controller!")
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service
    matching_service = service_provider.matching_service
    return MatchingController(matching_service, glossary_service)


@lru_cache()
def get_ingredients_declaration_mapping_controller() -> IngredientsDeclarationMappingController:
    logger.info("called get_ingredients_declaration_mapping_controller!")
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    ingredients_declaration_mapping_mgr = postgres_db.get_ingredients_declaration_mapping_mgr()

    return IngredientsDeclarationMappingController(
        IngredientsDeclarationMappingService(ingredients_declaration_mapping_mgr),
    )


def create_controller_lru_caches() -> None:
    logger.info("creating lru caches of controllers!")
    admin_controller = get_admin_controller()
    access_controller = get_access_controller()
    auth_controller = get_auth_controller()
    glossary_controller = get_glossary_controller()
    glossary_link_controller = get_glossary_link_controller()
    node_controller = get_node_controller()
    matching_item_controller = get_matching_item_controller()
    ingredients_declaration_mapping_controller = get_ingredients_declaration_mapping_controller()


def clear_controller_lru_caches() -> None:
    logger.info("clearing lru caches of controllers!")

    get_admin_controller.cache_clear()
    get_access_controller.cache_clear()
    get_auth_controller.cache_clear()
    get_glossary_controller.cache_clear()
    get_glossary_link_controller.cache_clear()
    get_node_controller.cache_clear()
    get_matching_item_controller.cache_clear()
    get_ingredients_declaration_mapping_controller.cache_clear()
