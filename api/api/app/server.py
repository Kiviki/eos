import time
from multiprocessing import Event

from fastapi import FastAPI, Request, status
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse
from structlog import get_logger

from api.app.configure_logging import configure_logging
from api.app.injector import clear_controller_lru_caches, create_controller_lru_caches
from api.app.settings import Settings
from api.middlewares.namespace_auth.add_namespace import NamespaceAuthMiddleware
from api.middlewares.request_logging.middleware import add_log_context
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.service_provider import ServiceLocator, ServiceProvider

logger = get_logger()

# TODO: this is just an example json schema... needs to be moved as an instance of a Term into the glossary...
custom_apple_schema = {
    # "$id": "https://eaternity.com/glossary/apple.schema.json",
    # "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Apple",
    "type": "object",
    "required": ["color", "weight"],
    "properties": {
        "color": {"type": "string", "description": "The color of the apple."},
        "weight": {"description": "weight in gram.", "type": "integer", "minimum": 0},
    },
}

startup_finished_event = Event()


service_locator = ServiceLocator()


# values are returned here for a cache duplication unit test in `test_basic_setup.py`
async def app_startup(app, schema=None) -> tuple[ServiceProvider, GapFillingModuleLoader]:
    service_provider = service_locator.service_provider

    # only connect to db if not yet connected:
    if not service_provider.postgres_db.pool:
        await service_provider.postgres_db.connect(schema=schema)

    await service_provider.messaging_service.start()

    # filling all services' caches
    await service_provider.init_caches()

    # initializing all gap filling modules
    await service_provider.gap_filling_module_loader.init(service_provider)

    # creating lru caches of controllers such that only one instance is created per worker
    create_controller_lru_caches()

    # Trigger a multiprocessing.Event to signal that the server is up and initialized:
    startup_finished_event.set()

    logger.info("startup of api-v2 complete")
    return service_provider, service_provider.gap_filling_module_loader


async def app_shutdown(app) -> None:
    service_provider = service_locator.service_provider

    # stopping RabbitMQ connection and clearing all services' caches
    await service_provider.shutdown_services()

    # de-initializing all gap filling modules
    service_provider.gap_filling_module_loader.clear()

    clear_controller_lru_caches()
    logger.info("shutdown of api-v2 complete")


def create_app() -> FastAPI:
    settings = Settings()

    logger.info("STARTING FASTAPI")

    app = FastAPI(
        title=settings.TITLE,
        openapi_url=f"{settings.BASE_PATH}/openapi.json",
        docs_url=f"{settings.BASE_PATH}/docs",
        redoc_url=f"{settings.BASE_PATH}/redoc",
    )

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        exc_str = f"{exc}".replace("\n", " ").replace("   ", " ")
        logger.error(f"{request}: {exc_str}")
        content = {"status_code": 422, "message": "RequestValidationError", "data": None}
        return JSONResponse(content=content, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)

    configure_logging(settings)

    @app.on_event("startup")
    async def startup_event():
        logger.info("startup of a uvicorn worker")
        await app_startup(app)

    @app.on_event("shutdown")
    async def shutdown_event():
        logger.info("shutdown of a uvicorn worker")
        await app_shutdown(app)

    app.middleware("http")(add_log_context)
    app.add_middleware(NamespaceAuthMiddleware)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            "http://localhost",
            "*",
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    setup_router(app)

    def custom_openapi():
        if app.openapi_schema:
            return app.openapi_schema
        openapi_schema = get_openapi(
            title="Eaternity API",
            version="2.5.0",
            description="This is a very custom OpenAPI schema",
            routes=app.routes,
        )
        openapi_schema["info"]["x-logo"] = {
            "url": "https://eaternity.org/img/home/Eaternity-logo-white-outline-small.png"
        }

        # Just testing here how we can add dynamically an apple schema to the openapi schema:
        openapi_schema["components"]["schemas"]["AppleTerm"] = custom_apple_schema

        app.openapi_schema = openapi_schema
        return app.openapi_schema

    app.openapi = custom_openapi

    return app


def setup_router(app: FastAPI):
    from api.app.router import router

    settings = Settings()

    app.include_router(router)

    @app.get(f"{settings.BASE_PATH}")
    @app.get(f"{settings.BASE_PATH}/status")
    async def status():
        return {"status": "successful", "service": "eos-v2"}


fastapi_app = create_app()
