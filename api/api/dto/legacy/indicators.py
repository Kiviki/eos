from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class VitaScoreDto(BaseModel):
    # Todo implement missing fields
    vita_score_points: Optional[int] = Field(None, alias="vita-score-points")
    # vita_score_rating: Optional[str] = Field(None, alias="vita-score-rating")
    # vita_score_improvement_percentage: Optional[float] = Field(None, alias="vita-improvement-percentage")
    # vita_score_award: Optional[bool] = Field(None, alias="vita-score-award")
    energy_kcals: Optional[int] = Field(None, alias="energy-kcals")
    # nutrition_label: Optional[bool] = Field(None, alias="nutrition-label")
    # nutrition_rating: Optional[str] = Field(None, alias="nutrition-rating")
    fruit_risk_factor_amount_gram: Optional[float] = Field(None, alias="fruit-risk-factor-amount-gram")
    fruit_risk_factor_points: Optional[float] = Field(None, alias="fruit-risk-factor-points")
    vegetable_risk_factor_amount_gram: Optional[float] = Field(None, alias="vegetable-risk-factor-amount-gram")
    vegetable_risk_factor_points: Optional[float] = Field(None, alias="vegetable-risk-factor-points")
    wholegrain_risk_factor_amount_gram: Optional[float] = Field(None, alias="wholegrain-risk-factor-amount-gram")
    wholegrain_risk_factor_points: Optional[float] = Field(None, alias="wholegrain-risk-factor-points")
    nuts_seeds_risk_factor_amount_gram: Optional[float] = Field(None, alias="nuts-seeds-risk-factor-amount-gram")
    nuts_seeds_risk_factor_points: Optional[float] = Field(None, alias="nuts-seeds-risk-factor-points")
    milk_risk_factor_amount_gram: Optional[float] = Field(None, alias="milk-risk-factor-amount-gram")
    milk_risk_factor_points: Optional[float] = Field(None, alias="milk-risk-factor-points")
    processed_meat_risk_factor_amount_gram: Optional[float] = Field(
        None, alias="processed-meat-risk-factor-amount-gram"
    )
    processed_meat_risk_factor_points: Optional[float] = Field(None, alias="processed-meat-risk-factor-points")
    red_meat_risk_factor_amount_gram: Optional[float] = Field(None, alias="red-meat-risk-factor-amount-gram")
    red_meat_risk_factor_points: Optional[float] = Field(None, alias="red-meat-risk-factor-points")
    salt_risk_factor_amount_gram: Optional[float] = Field(None, alias="salt-risk-factor-amount-gram")
    salt_risk_factor_points: Optional[float] = Field(None, alias="salt-risk-factor-points")
    model_config = ConfigDict(populate_by_name=True)


class IndicatorsDto(BaseModel):
    vita_score: Optional[VitaScoreDto] = Field(None, alias="vita-score")
    vita_score_legacy: Optional[VitaScoreDto] = Field(None, alias="vita-score-legacy")
    food_unit: Optional[float] = Field(None, alias="food-unit")
    model_config = ConfigDict(populate_by_name=True)


class WrappedIndicatorsDto(BaseModel):
    indicators: Optional[IndicatorsDto] = Field(None, alias="indicators")
    model_config = ConfigDict(populate_by_name=True)
