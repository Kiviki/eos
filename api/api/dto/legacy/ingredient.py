from enum import Enum
from typing import Dict, List, Optional

from pydantic import ConfigDict, Field

from api.dto.legacy.climate_score import ClimateScoreDto
from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node

from .localized_name import LocalizedNameDto


class IngredientTypeDtoEnum(str, Enum):
    conceptual_ingredients = "conceptual-ingredients"
    recipes = "recipes"


class IngredientDto(ClimateScoreDto):
    uid: Optional[str] = None
    xid: Optional[str] = None
    # WARNING: for links to sub-recipes this cannot be the same xid as the sub-recipe xid, because this is a flow and
    # the sub-recipe is an activity. This is different to the legacy_api, where the id is the same.

    link_to_xid: Optional[str] = None
    link_to_uid: Optional[str] = None

    type: Optional[IngredientTypeDtoEnum] = IngredientTypeDtoEnum.conceptual_ingredients
    names: List[LocalizedNameDto]
    amount: Optional[float] = None
    percentage: Optional[float] = None
    unit: Optional[str] = None
    origin: Optional[str] = None
    transport: Optional[str] = None
    production: Optional[str] = None
    producer: Optional[str] = None
    processing: Optional[str] = None
    conservation: Optional[str] = None
    packaging: Optional[str] = None
    ingredients_declaration: Optional[list[LocalizedNameDto]] = Field(None, alias="ingredients-declaration")
    nutrient_values: Optional[dict] = Field(None, alias="nutrient-values")
    gtin: Optional[str] = None
    name_of_sub_node: Optional[str] = None
    reference_product_of_sub_node: Optional[str] = None

    def get_ingredient_meta_data(self) -> Dict:
        return self.model_dump(exclude={"uid", "xid", "type"})

    def to_node(self) -> Node:
        node_class = FoodProductFlowNode
        node = node_class(raw_input=self.get_ingredient_meta_data())
        return node

    model_config = ConfigDict(populate_by_name=True)
