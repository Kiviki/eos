from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class ClimateScoreDto(BaseModel):
    co2_value: Optional[float] = Field(None, alias="co2-value")
    amount_for_local_node: Optional[float] = Field(None, alias="amount_for_local_node")
    impact_assessments: Optional[dict[str, dict[str, float | str]]] = Field(None, alias="impact_assessments")
    model_config = ConfigDict(populate_by_name=True)
