from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class MatchingItemDto(BaseModel):
    gap_filling_module: str = Field(alias="gap_filling_module")

    access_group_uid: Optional[str] = Field(None, alias="access_group_uid")
    lang: Optional[str] = Field(None, alias="lang")
    matching_string: Optional[str] = Field(None, alias="matching_string")

    term_uids: list[str] = Field(alias="term_uids")
    model_config = ConfigDict(populate_by_name=True)
