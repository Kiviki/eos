from enum import Enum
from typing import Dict, List, Optional, Union
from uuid import UUID

from pydantic import BaseModel, ConfigDict, Field

from core.domain.access_group_node import NodeAccessGroupTypeEnum

# access groups


# List[.] used by get_sub_access_groups as output
# List[.] used by upsert_edge as output
class AccessGroupId(BaseModel):
    xid: Optional[str] = None
    uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


# TODO: instead use existing AccessGroup-class
class AccessGroupDto(BaseModel):
    name: str
    location: str
    email: Optional[str] = None
    language: Optional[str] = None
    xid: Optional[str] = None
    uid: Optional[str] = None
    type: Optional[NodeAccessGroupTypeEnum] = NodeAccessGroupTypeEnum.kitchen
    creator: Optional[UUID] = None
    model_config = ConfigDict(populate_by_name=True)

    def get_model_meta_data(self) -> Dict:
        return self.model_dump(exclude={"xid", "uid", "type", "creator"})


# used in upsert_access_group as output
# used in get_access_group_by_id as output
class WrappedAccessGroupDto(BaseModel):
    access_group: AccessGroupDto
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


# used in upsert_access_group as input
class WrappedUpsertAccessGroupInputDto(BaseModel):
    access_group: AccessGroupDto
    namespace_uid: Optional[str] = None
    parent_access_group: Optional[AccessGroupId] = Field(default_factory=AccessGroupId)
    model_config = ConfigDict(extra="forbid")


# used in get_access_group_by_id as input
# used in delete_access_group as input
class WrappedAccessGroupIdDto(BaseModel):
    access_group: AccessGroupId
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


# used in get_sub_access_groups as input
class WrappedGetSubAccessGroupsInputDto(BaseModel):
    namespace_uid: Optional[str] = None
    parent_access_group: Optional[AccessGroupId] = None
    depth: Optional[int] = 10
    model_config = ConfigDict(extra="forbid")


class AccessGroupIdWithStatus(AccessGroupId):
    statuscode: int
    message: str


# used in upsert_edge as input
# used in get_edge as input
# used in delete_edge as input
class WrappedEdgeAccessGroupDto(BaseModel):
    child_access_group_list: List[AccessGroupId]
    parent_access_group: AccessGroupId
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


# used in upsert_edge as output
# used in get_edge as output
# used in delete_edge as output
class WrappedEdgeAccessGroupWithStatusDto(BaseModel):
    child_access_group_list_with_status: List[AccessGroupIdWithStatus]
    parent_access_group: AccessGroupId
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")
