from typing import List, Optional

from pydantic import BaseModel, ConfigDict, Field

from core.domain.calculation import Calculation
from core.domain.nodes.root_with_subflows_dto import NodeIDDto


class ExistingNodeDto(BaseModel):
    existing_node: NodeIDDto = Field(default_factory=NodeIDDto)
    model_config = ConfigDict(extra="forbid")


class BatchItemDto(BaseModel):
    request_id: Optional[int] = None
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodeInputItemDto(BatchItemDto):
    nodes_dto: ExistingNodeDto
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodeOutputItemDto(BatchItemDto):
    statuscode: int
    message: str
    model_config = ConfigDict(extra="forbid")


class BatchInputDto(BaseModel):
    batch: List[Calculation]
    model_config = ConfigDict(extra="forbid")


class BatchOutputDto(BaseModel):
    batch: List[Calculation]
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodesInputDto(BaseModel):
    batch: List[BatchWithExistingNodeInputItemDto]
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodesOutputDto(BaseModel):
    batch: List[BatchWithExistingNodeOutputItemDto]
    model_config = ConfigDict(extra="forbid")
