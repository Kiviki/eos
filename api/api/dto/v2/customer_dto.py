from typing import Optional

from pydantic import BaseModel


class CustomerDto(BaseModel):
    customer_id: str
    name: str


class CreateCustomerDto(BaseModel):
    name: str
    uid: Optional[str] = None


class CustomerCreateStatusDto(BaseModel):
    auth_token: str
    status: bool
    namespace: CreateCustomerDto


class WrappedCreateCustomerDto(BaseModel):
    customer: CreateCustomerDto
