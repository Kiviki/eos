import pytest
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from api.app.settings import Settings
from core.service.service_provider import ServiceLocator

from .conftest import FastAPIReturn

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()


@pytest.mark.asyncio
async def test_get_glossary_links_for_gfm_matchproductname(app: FastAPIReturn) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        response = await ac.get(
            "/v2/glossary-link/by-gap-filling-module/Nutrients",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        glossary_links = response.json().get("glossary_links")
        assert len(glossary_links) == 10, "There should be 10 glossary links for the 'Nutrients' gfm in the test data"
        assert glossary_links[0]["linked_term_uid"], "The first glossary link should link to a term"
        assert glossary_links[0]["linked_node_uid"] is None, "The first glossary link should link to a term"


@pytest.mark.asyncio
async def test_put_glossary_link_with_bare_minimum_dto(
    app: FastAPIReturn, term_access_group_uids: tuple[str, str, str, str]
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        # pick a term and a nutrition term that have not been linked together yet
        cocoa_xid = "A0CZM"
        cocoa_nutrition_xid = "732"

        _, eurofir_term_group_uid, foodex2_term_group_uid, _ = term_access_group_uids

        # get a (cocoa) term uid
        response = await ac.get(
            f"/v2/glossary/by-xid/{cocoa_xid}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        cocoa_term = response.json()

        # get a (cocoa nutrition) term uid
        response = await ac.get(
            f"/v2/glossary/by-xid/{cocoa_nutrition_xid}/{eurofir_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        nutrition_term = response.json()

        # create a glossary link without auth
        response = await ac.post(
            "/v2/glossary-link/",
            headers={"Authorization": "Basic blah"},
            json={
                "gap_filling_module": "Nutrients",
                "term_uids": [cocoa_term["uid"]],
                "linked_term_uid": nutrition_term["uid"],
            },
        )
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), "Should not be able to create a glossary link without auth"

        # create a glossary link
        response = await ac.post(
            "/v2/glossary-link/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "gap_filling_module": "Nutrients",
                "term_uids": [cocoa_term["uid"]],
                "linked_term_uid": nutrition_term["uid"],
            },
        )
        assert response.status_code == status.HTTP_200_OK

        # check that the glossary link was created
        response = await ac.get(
            "/v2/glossary-link/by-gap-filling-module/Nutrients",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        glossary_links = response.json().get("glossary_links")
        assert (
            len(glossary_links) == 11
        ), "Now there should be 12 glossary links for the 'Nutrients' gfm in the test data"
        assert glossary_links[-1]["term_uids"] == [
            cocoa_term["uid"]
        ], "The newly created glossary link should link to the cocoa term"
        assert (
            glossary_links[-1]["linked_term_uid"] == nutrition_term["uid"]
        ), "The newly created glossary link should link to the cocoa nutrition term"
