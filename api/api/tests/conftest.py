import asyncio
import base64
import copy
import json
import os
import uuid
from typing import Any, AsyncGenerator, Generator, Optional, Tuple
from unittest import mock

import pytest
import pytest_asyncio
from _pytest.fixtures import SubRequest
from fastapi import status
from httpx import AsyncClient, Response
from lxml import etree
from structlog import get_logger

from api.app.server import app_shutdown, app_startup, fastapi_app
from api.app.settings import Settings
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.activity.emission import ElementaryResourceEmissionNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator
from core.tests.conftest import add_flow, add_greenhouse_mock_nodes
from database.postgres.settings import PgSettings

FastAPIReturn = AsyncGenerator[Any, Any]
logger = get_logger()
settings = Settings()

service_locator = ServiceLocator()

CUSTOMER = {
    "name": "test_name",
    "namespace": str(uuid.uuid4()),
}

USER_SUPER = {
    "email": "asd@example.com",
    # password is optional right now, leaving this field as
    # a placeholder for future authentication features
    "password": "",
    "is_superuser": True,
}
USER_SUPER_ID = str(uuid.uuid4())

USER_ADMIN = {
    "email": "wasd@example.com",
    "password": "",
    "is_superuser": False,
}
USER_ADMIN_ID = str(uuid.uuid4())

USER_BASIC = {
    "email": "qwe@example.com",
    "password": "",
    "is_superuser": False,
}
USER_BASIC_ID = str(uuid.uuid4())

TEST_KITCHEN_BY_SUPERUSER = {
    "xid": "test_kitchen_id",
    "type": "kitchen",
    "name": "test__kitchen_name",
    "location": "test_location",
    "email": None,
    "language": None,
    "creator": USER_SUPER_ID,
}
TEST_KITCHEN_BY_ADMIN = {
    "xid": "test_kitchen_admin_id",
    "type": "kitchen",
    "name": "test__kitchen_admin_name",
    "location": "test_admin_location",
    "email": None,
    "language": None,
    "creator": USER_ADMIN_ID,
}
TEST_KITCHEN_BY_BASIC_USER = {
    "xid": "test_kitchen_basic_id",
    "type": "kitchen",
    "name": "test__kitchen_basic_name",
    "location": "test_basic_location",
    "email": None,
    "language": None,
    "creator": USER_BASIC_ID,
}

TOLERANCE = 1e-6


def is_close(a: float, b: float, tol: float = TOLERANCE) -> bool:
    return abs(a - b) < tol


def find_quantity_value(quantities_dict: dict, quantity_term_name: str) -> Optional[float]:
    """Helper function to extract a quantity value from a dictionary of quantities."""
    for _, value in quantities_dict.items():
        if "quantity_term_name" in value and value["quantity_term_name"] == quantity_term_name:
            return value["quantity"]["value"]
    return None


def check_mutation_log(response: Response, expected_mutation_log: dict) -> None:
    # we now have to check the second mutation, since the first is the addition of the root-flow-node
    first_mutation = response.json().get("batch")[0].get("mutations")[1]
    recursive_compare_dict(first_mutation, expected_mutation_log)


def recursive_compare_dict(response: dict, expected: dict) -> None:
    for expected_key, expected_val in expected.items():
        assert expected_key in response
        response_val = response[expected_key]

        if isinstance(expected_val, dict):
            assert isinstance(response_val, dict)
            recursive_compare_dict(response_val, expected_val)

        elif isinstance(expected_val, list):
            assert isinstance(response_val, list)

            # we recursively check for list equality
            for idx, expected_item_val in enumerate(expected_val):
                assert len(response_val) >= idx
                resp_item = response_val[idx]
                if isinstance(resp_item, dict):
                    assert isinstance(resp_item, dict)
                    recursive_compare_dict(resp_item, expected_item_val)
                else:
                    assert resp_item == expected_item_val

        else:
            assert response_val == expected_val


def change_in_every_item_in_batch(batch: dict, key: str, value: str) -> dict:
    """Helper function to change a key in every item in a batch."""
    new_batch = copy.deepcopy(batch)
    for item in new_batch:
        item[key] = value
    return new_batch


def change_in_every_activity_in_batch(batch: list[dict], key: str, value: str | dict) -> list[dict]:
    """Helper function to change a key in every activity in a batch."""
    new_batch = copy.deepcopy(batch)
    for item in new_batch:
        item["input_root"]["activity"][key] = value
    return new_batch


def check_response_codes_in_batch_calculation(response: Response, expected_response_code: int) -> None:
    """Helper Function to check the response codes in a batch response."""
    assert response.status_code == status.HTTP_200_OK
    for batch in response.json().get("batch"):
        assert batch.get("statuscode") == expected_response_code, (
            f"status code of batch item should " f"be {expected_response_code}"
        )


@pytest.fixture(scope="package")
def event_loop() -> Generator[asyncio.AbstractEventLoop, Any, None]:
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="function")
async def app() -> FastAPIReturn:
    service_locator.recreate()
    service_provider = service_locator.service_provider

    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=fastapi_app,
        base_url="http://localhost:8040",
    ):
        await postgres_db.connect(schema="test_pg")
        await postgres_db.reset_db("test_pg")

        settings = PgSettings()

        graph_mgr = postgres_db.get_graph_mgr()

        co2_emission = await graph_mgr.upsert_node_by_uid(
            ElementaryResourceEmissionNode(
                node_type=ElementaryResourceEmissionNode.__name__,
                production_amount={"value": 1.0, "unit": "kilogram"},
                key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
                name="Carbon dioxide, fossil",
                categories=["air"],
            )
        )
        # necessary for transportation & orchestration unit tests
        await postgres_db.get_product_mgr().bulk_insert_xid_uid_mappings(
            [
                (
                    settings.EATERNITY_NAMESPACE_UUID,
                    "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
                    co2_emission.uid,
                )
            ],
        )

        # add the nodes necessary to for the greenhouse gfm
        await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="FR")
        await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="CH")

        term_mgr = postgres_db.get_term_mgr()
        foodex2_term_group_uid = ""
        groups = await term_mgr.find_all_term_access_groups()
        for group in groups:
            if "FoodEx2" in group.data.get("name"):
                foodex2_term_group_uid = group.uid
                break
        # the cooling terms have to be added always because they might also be relevant if cooling
        # was not explicitly declared in the recipe (e.g. for perishable products)
        cooled_term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
            "J0131", foodex2_term_group_uid
        )

        cooling_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1, "unit": "kg*day"},
                id="('ecoinvent 3.6 cutoff', '001decfdb6f2319cd8578e05c91c247d')",
                key=[
                    "ecoinvent 3.6 cutoff",
                    "001decfdb6f2319cd8578e05c91c247d",
                ],
                flow=None,
                name="market for operation, reefer, cooling",
                type="process",
                database="ecoinvent 3.6 cutoff",
                filename=None,
                reference_product="market for operation, reefer, cooling",
            )
        )

        await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[cooled_term.uid],
                linked_node_uid=cooling_process.uid,
            )
        )

        await add_flow(graph_mgr, cooling_process, co2_emission, 0.005, "biosphere")

        await app_startup(fastapi_app, schema="test_pg")

    yield fastapi_app

    # this will shutdown the app after each test
    await app_shutdown(fastapi_app)


@pytest_asyncio.fixture(scope="function")
async def term_access_group_uids(app: FastAPIReturn) -> Tuple[str, str, str, str]:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(app=app, base_url="http://localhost:8040") as _:
        await postgres_db.connect(schema="test_pg")

    eaternity_term_group_uid = ""
    eurofir_term_group_uid = ""
    foodex2_term_group_uid = ""
    nutrient_subdivision_term_group_uid = ""

    groups = await postgres_db.get_term_mgr().find_all_term_access_groups()

    for group in groups:
        if settings.EATERNITY_USERNAME in group.data.get("name"):
            eaternity_term_group_uid = group.uid
        if "EuroFIR" in group.data.get("name"):
            eurofir_term_group_uid = group.uid
        if "FoodEx2" in group.data.get("name"):
            foodex2_term_group_uid = group.uid
        if "Nutrients Subdivision" in group.data.get("name"):
            nutrient_subdivision_term_group_uid = group.uid

    return (
        eaternity_term_group_uid,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
    )


@pytest_asyncio.fixture(scope="function")
async def basic_router_availability(app: FastAPIReturn) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        response = await ac.get("/v2")
        assert response.status_code == status.HTTP_200_OK, "/v2 should be reachable"
        assert response.json().get("status") == "successful"

        response = await ac.get("/v2/status")
        assert response.status_code == status.HTTP_200_OK, "/v2/status should be reachable"
        assert response.json().get("status") == "successful"


@pytest_asyncio.fixture(scope="function")
async def create_customer_namespace(app: FastAPIReturn, basic_router_availability: None) -> bytes:
    _ = basic_router_availability
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": "Basic wrong-key"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Invalid authentication credentials"}

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
    assert response.status_code == status.HTTP_200_OK

    assert response.json().get("namespace", {}).get("uid"), (
        "UUID of the created namespace " "must be present in the response."
    )

    assert response.json().get("auth_token"), (
        "Authentication token for the created namespace " "must be present in the response."
    )
    encoded_customer_token = response.json().get("auth_token")

    assert response.json() == {
        "auth_token": encoded_customer_token,
        "status": True,
        "namespace": {
            "name": CUSTOMER.get("name"),
            "uid": CUSTOMER.get("namespace"),
        },
    }

    encoded_customer_token = base64.b64encode(encoded_customer_token.encode("utf-8")).decode("utf-8")
    return encoded_customer_token


@pytest_asyncio.fixture(scope="function")
async def get_customer_namespace(app: FastAPIReturn, basic_router_availability: None) -> None:
    _ = basic_router_availability
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.get(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": "Basic wrong-key"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Invalid authentication credentials"}

        # create namespace first
        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        assert response.json().get("namespace", {}).get("uid"), (
            "UUID of the created namespace " "must be present in the response."
        )
        response = await ac.get(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK

    assert len(response.json()) == 1
    assert response.json()[0].get("user_id", {}), "UUID of the fetched namespace must be present in the response."


@pytest_asyncio.fixture(scope="function")
async def create_users(app: FastAPIReturn, create_customer_namespace: None) -> Tuple[bytes, bytes, bytes]:
    _ = create_customer_namespace
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_ADMIN,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_admin_user_token = response.json().get("auth_token")
        encoded_admin_user_token = base64.b64encode(encoded_admin_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_BASIC,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_basic_user_token = response.json().get("auth_token")
        encoded_basic_user_token = base64.b64encode(encoded_basic_user_token.encode("utf-8")).decode("utf-8")

        return encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token


@pytest_asyncio.fixture(scope="function")
async def create_and_get_and_delete_and_create_kitchen_by_superuser(
    app: FastAPIReturn, create_users: Tuple[bytes, bytes, bytes]
) -> str:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, _ = create_users

        # not specifying a namespace here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
            },
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        # explicitly setting a namespace here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_SUPERUSER)
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        # this can work only once we have a default group set for this user,
        # since that's how we get target namespace when it is not specified in the request
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_SUPERUSER.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_SUPERUSER.get("location")

        # add this access group as a default one for superuser
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")
        assert response.json().get("status") is True

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_SUPERUSER.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_SUPERUSER.get("location")

        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json()[0].get("permissions").get(UserPermissionsEnum.add_users_to_group) is True

        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")}},
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": f"Group {TEST_KITCHEN_BY_SUPERUSER.get('xid')} does not exist."}

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        # add this access group as a default one for superuser
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # now we can send a request without specifying target namespace
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group", {}).get("uid") is not None
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        # testing this request via UUID
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
                "parent_access_group": {"uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group", {}).get("xid") is not None
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        return TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")


@pytest_asyncio.fixture(scope="function")
async def create_and_get_and_delete_and_create_kitchen_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    postgres_db = service_locator.service_provider.postgres_db
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, _ = create_users

        # adding admin to superuser's access group just to store group creation permission somewhere
        members = [
            {
                "user_id": USER_ADMIN_ID,
                "permissions": {
                    UserPermissionsEnum.create_access_group: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                "members": members,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin user hasn't been added to superuser's group."
        assert response.json().get("members_with_status") == [
            {**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members
        ]

        # posting an access group; not specifying a namespace in this request
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert (
            response.status_code == status.HTTP_406_NOT_ACCEPTABLE
        ), "Admin's access group has been created, while it should not happen."

        # setting target namespace explicitly here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("namespace"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_201_CREATED, "Admin's access group hasn't been created."
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_ADMIN_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_ADMIN)
        TEST_KITCHEN_BY_ADMIN_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        # now we can remove admin from superuser's access group
        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                "members": [{"user_id": USER_ADMIN_ID}],
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin hasn't been removed from superuser's access group."

        # this can work only once we have a default group set for this user,
        # since that's how we get target namespace when it is not specified in the request
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                }
            },
        )
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), "Admin's access group has been received without namespace set explicitly."

        # setting origin namespace explicitly
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                },
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been received."
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_ADMIN.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_ADMIN.get("location")

        # add this access group as a default one for admin user
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # try adding a random non-existent one
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": str(uuid.uuid4()),
            },
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        # checking if we can get admin's access group
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been received."
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_ADMIN.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_ADMIN.get("location")

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert response.json()[0].get("permissions").get(UserPermissionsEnum.add_users_to_group) is True

        # deleting admin's access group
        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}},
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been deleted."

        # checking if admin's access group is deleted indeed
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, "Admin's access group hasn't been deleted"
        assert response.json() == {"detail": f"Group {TEST_KITCHEN_BY_ADMIN.get('xid')} does not exist."}

        # testing this request with specifying namespace
        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, "Admin's access group hasn't been deleted."

        # trying to create a child access group via admin user,
        # which is impossible since the user is not a member of any group
        # and, therefore, doesn't have "Create new access groups" permission stored anywhere
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
                "namespace_uid": CUSTOMER.get("namespace"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Admin user has created a child access group, " "while they should not be able to do so."
        )

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED, (
            "Admin user has not created an independent access group, " "while they should be able to do so."
        )
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_ADMIN_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        # add this access group as a default one for admin user
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # testing PUT request method
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("namespace"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group has not been updated."
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("namespace"),
        }

        return TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")


ECOTRANSIT_RESPONSES_FOLDER = "ecotransit_responses"
SAMPLE_JSONS_DIR = os.path.join(
    os.path.dirname(__file__),
    "..",
    "..",
    "..",
    "core",
    "core",
    "tests",
    "sample_jsons",
)


@pytest.fixture(scope="package", autouse=True)
def geolocation_api_mock(request: SubRequest) -> None:
    """Mocks the Google geolocation API to return a fixed result."""

    def mocked_requests_get(url: str) -> Response:
        url_to_check = url.lower()

        if (
            "test_admin_location" in url_to_check
            or "test_location" in url_to_check
            or "schweiz" in url_to_check
            or "paris" in url_to_check
        ):
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "zurich_google_maps_query_response.json",
            )
        elif "spain" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "spain_google_maps_query_response.json",
            )
        else:
            raise NotImplementedError(f"Mocking not implemented for {url}")

        with open(json_sample_path) as response_json_sample_file:
            resp_json = json.load(response_json_sample_file)

        mock_response = Response(status_code=200, content=json.dumps(resp_json).encode("utf-8"))

        return mock_response

    print("Patching 'gap_filling_modules.location_gfm._google_api_lookup'")
    patched = mock.patch("gap_filling_modules.location_gfm._google_api_lookup", new=mocked_requests_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'gap_filling_modules.location_gfm._google_api_lookup'")

    request.addfinalizer(unpatch)


XML_FILES_MAPPING = {
    "wsdl": "wsdl.xml",
    "wsdl=1": "wsdl_1.xml",
    "xsd=1": "xsd_1.xml",
    "xsd=2": "xsd_2.xml",
}


@pytest.fixture(scope="package", autouse=True)
def ecotransit_wsdl_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT WSDL call to return a fixed XML schema file."""

    def mocked_wsdls_get(self: None, url: str) -> bytes:
        _ = self
        url_to_check = url.lower()
        schema_file_type = url_to_check.split("?")[-1]

        if schema_file_type not in XML_FILES_MAPPING:
            raise NotImplementedError(f"WSDL load mocking not implemented for {url}")

        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            XML_FILES_MAPPING[schema_file_type],
        )

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_xml = bytes(response_xml_sample_file.read().encode("utf-8"))

        return mock_xml

    print("Patching 'zeep.transports.Transport._load_remote_data'")
    patched = mock.patch("zeep.transports.Transport._load_remote_data", new=mocked_wsdls_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport._load_remote_data'")

    request.addfinalizer(unpatch)


@pytest.fixture(scope="package", autouse=True)
def ecotransit_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT API call to return a fixed result in XML format."""

    def mocked_ecotransit_send_request(self: None, address: str, message: bytes, headers: dict) -> Response:
        _ = self
        _ = address
        _ = headers
        departure = {}
        destination = {}
        mode = ""

        parsed_message = etree.fromstring(message)
        root = parsed_message.getroottree().getroot()

        for stuff in root.iter():
            if "wgs84" in stuff.tag:
                if stuff.prefix == "ns1":
                    # departure coords
                    departure = dict(stuff.attrib)
                elif stuff.prefix == "ns2":
                    # destination coords
                    destination = dict(stuff.attrib)

            elif stuff.prefix == "ns3":
                if "air" in stuff.tag:
                    mode = "air"
                elif "sea" in stuff.tag:
                    mode = "sea"
                elif "road" in stuff.tag:
                    mode = "road"

            if mode:
                break

        sample_response_xml_file = (
            f"{departure.get('longitude')}_{departure.get('latitude')}_"
            f"{destination.get('longitude')}_{destination.get('latitude')}_{mode}.xml"
        )
        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            ECOTRANSIT_RESPONSES_FOLDER,
            sample_response_xml_file,
        )

        if not os.path.isfile(xml_sample_path):
            print(f"WARNING: {xml_sample_path} does not exist. Return 404 status.")
            return Response(status_code=404)

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_response = Response(status_code=200, content=bytes(response_xml_sample_file.read().encode("utf-8")))

        return mock_response

    print("Patching 'zeep.transports.Transport.post'")
    patched = mock.patch("zeep.transports.Transport.post", new=mocked_ecotransit_send_request)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport.post'")

    request.addfinalizer(unpatch)
