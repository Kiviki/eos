from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    USER_BASIC,
    USER_BASIC_ID,
    FastAPIReturn,
    change_in_every_activity_in_batch,
    change_in_every_item_in_batch,
    check_mutation_log,
    check_response_codes_in_batch_calculation,
)

service_locator = ServiceLocator()

TEST_SUPPLY_ID = "supply01"
TEST_SUB_FOOD_PRODUCT_ID = "sub_food_product01"
TEST_SUPPLY = [
    {
        "input_root": {
            "activity": {
                "xid": TEST_SUPPLY_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "supplier": "Vegetable Supplies Ltd.",
                "supplier_id": "el3i5y-2in5y-2hbllll01",
                "invoice_id": "778888800000001",
                "supply_date": "2013-06-01",
            },
            "sub_flows": [
                {
                    "xid": "100100191",
                    "node_type": FoodProductFlowNode.__name__,
                    "amount_in_original_source_unit": {"value": 150, "unit": "gram"},
                    "link_to_sub_node": {"xid": TEST_SUB_FOOD_PRODUCT_ID},
                }
            ],
        }
    },
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "nutrient_values": {
                    "energy_kcal": 200,
                    "fat_gram": 12.3,
                    "saturated_fat_gram": 2.5,
                    "carbohydrates_gram": 8.4,
                    "sucrose_gram": 3.2,
                    "protein_gram": 2.2,
                    "sodium_chloride_gram": 0.3,
                },
            },
            "activity": {
                "activity_date": "2023-06-20",
                "node_type": FoodProcessingActivityNode.__name__,
                "xid": TEST_SUB_FOOD_PRODUCT_ID,
                "raw_production": {"value": "greenhouse", "language": "en"},
                "transport": "air",
                "activity_location": "spain",
                "name": [
                    {
                        "language": "de",
                        "value": "Karottenpuree",
                    },
                ],
                "production_amount": {"value": 150, "unit": "gram"},
                "ingredients_declaration": [
                    {
                        "language": "de",
                        "value": "Karotten, Tomaten",
                    },
                ],
                "processing": "raw",
                "raw_conservation": {"value": "fresh", "language": "en"},
                "packaging": "plastic",
            },
        }
    },
]
PROPS_UNUSED = (
    "processing",
    "packaging",
    "servings_deprecated",
    "invoice_id",
    "supplier_id",
    "supply_date",
    "supplier",
)
TEST_SUPPLY_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_SUPPLY[0]["input_root"]["activity"]["node_type"],
        "raw_input": {key: val for key, val in TEST_SUPPLY[0]["input_root"]["activity"].items() if key in PROPS_UNUSED},
        **{
            key: val
            for key, val in TEST_SUPPLY[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
        # transport is moved back to raw_input for activity nodes since no GFM uses transport on activity nodes.
    },
}


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_superuser(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser

        TEST_SUPPLY_WITH_XID = change_in_every_activity_in_batch(
            TEST_SUPPLY, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_SUPPLY_WITH_XID},
        )

        # TODO: extend these tests for the impact assessment
        assert response.json()["batch"][1]["final_root"]["activity"]["impact_assessment"] is not None

        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert resp_node.get("node_type") == FoodProcessingActivityNode.__name__
        assert batch[1]["final_root"].get("activity").get("raw_input").get("name")[0].get("value") == TEST_SUPPLY[1][
            "input_root"
        ].get("activity").get("name")[0].get("value")

        # testing deletion
        node_to_delete_uid = resp_node.get("uid")
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "uid": node_to_delete_uid,
                        "access_group_uid": test_kitchen_by_superuser_uid,
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_SUPPLY_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_SUPPLY_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "access_group": {"uid": test_kitchen_by_superuser_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": None, "uid": test_kitchen_by_superuser_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_superuser_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_superuser_uid
        assert response.json().get("status") is True

        # and check if they can get this supply
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUPPLY_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)


# this test is encapsulated into a fixture
# for using the final state in `test_get_..._node_types_from_access_group` tests
@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_supply_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        TEST_SUPPLY_WITH_XID = change_in_every_activity_in_batch(
            TEST_SUPPLY, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_SUPPLY_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert resp_node.get("node_type") == FoodProcessingActivityNode.__name__
        assert batch[1]["final_root"].get("activity").get("raw_input").get("name")[0].get("value") == TEST_SUPPLY[
            1
        ].get("input_root").get("activity").get("name")[0].get("value")

        # testing deletion
        node_to_delete_uid = resp_node.get("uid")
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "uid": node_to_delete_uid,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )

        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_SUPPLY_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)

        check_mutation_log(response, TEST_SUPPLY_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "access_group": {"uid": test_kitchen_by_admin_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": None, "uid": test_kitchen_by_admin_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # and check if they can get this supply
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUPPLY_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            },
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUB_FOOD_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            },
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)

        final_root1 = response.json().get("batch")[0].get("final_root").get("activity", {})
        final_root2 = response.json().get("batch")[1].get("final_root").get("activity", {})
        assert final_root1.get("uid") is not None

        return final_root1.get("uid"), final_root2.get("uid")


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_admin(create_delete_and_get_supply_by_admin: str) -> None:
    _ = create_delete_and_get_supply_by_admin
