"Definitions of recipes, tests of API endpoints for recipe CRUD."

# ruff: noqa: ARG001
import base64
import copy
from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    CUSTOMER,
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    USER_BASIC,
    USER_BASIC_ID,
    USER_SUPER,
    USER_SUPER_ID,
    FastAPIReturn,
    change_in_every_activity_in_batch,
    change_in_every_item_in_batch,
    check_mutation_log,
    check_response_codes_in_batch_calculation,
    find_quantity_value,
    is_close,
    settings,
)

logger = get_logger()
service_locator = ServiceLocator()

TEST_RECIPE_FLOW_ID = "test_recipe_flow_id"
TEST_RECIPE_ID = "test_recipe_id"
ONION_DRYING_UPSCALING = 8.181818181825
TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING = 0.023166
TEST_RECIPE_EXPECTED_CO2 = TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING * ONION_DRYING_UPSCALING
TEST_RECIPE_EXPECTED_DFU_NO_DRYING = 0.04014796363636364
TEST_RECIPE_EXPECTED_DFU = 0.1132100114241322
TEST_RECIPE_EXPECTED_DFU_ING1 = 0.023
TEST_RECIPE_EXPECTED_DFU_ING2 = TEST_RECIPE_EXPECTED_DFU - TEST_RECIPE_EXPECTED_DFU_ING1
TEST_RECIPE_EXPECTED_VITASCORE = 42.85985265378752
TEST_RECIPE_EXPECTED_VITASCORE_LEGACY = 132.7001174110249
TEST_RECIPE_EXPECTED_CO2_OF_TOMATO_INGREDIENT = 0.0
TEST_RECIPE_EXPECTED_CO2_OF_ONION_INGREDIENT = TEST_RECIPE_EXPECTED_CO2
TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT_WITHOUT_UPSCALING = 7.8
TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT = (
    TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT_WITHOUT_UPSCALING * ONION_DRYING_UPSCALING
)
TEST_RECIPE = [
    {
        "input_root": {
            "flow": {
                "xid": TEST_RECIPE_FLOW_ID,
                "node_type": FoodProductFlowNode.__name__,
                "titles": [{"language": "de", "value": "root_flow_node_on_top_of_test_recipe"}],
                "amount_in_original_source_unit": {"value": 150 + 78, "unit": "gram"},
            },
            "activity": {
                "xid": TEST_RECIPE_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Kürbisrisotto"}],
                "author": "Eckart Witzigmann",
                "activity_date": "2023-06-20",
                "servings_deprecated": 140,
                "instructions": [
                    {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
                    {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
                ],
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100191",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 150, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100894",
                    "raw_production": {"value": "organic", "language": "en"},
                    "transport": "ground",
                    "flow_location": "france",
                    "raw_labels": {"value": "Bio Suisse", "language": "fr"},
                    "product_name": [{"language": "de", "value": "Zwiebeln"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "raw_conservation": {"value": "dried", "language": "en"},
                    "packaging": "",
                },
            ],
        },
    },
]

PROPS_UNUSED = ("processing", "packaging", "servings_deprecated", "instructions", "titles", "date", "author", "id")

TEST_RECIPE_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {key: val for key, val in TEST_RECIPE[0]["input_root"]["activity"].items() if key in PROPS_UNUSED},
        **{
            key: val
            for key, val in TEST_RECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
        # transport is moved back to raw_input for activity nodes since no GFM uses transport on activity nodes.
    },
    "mutation_type": "AddNodeMutation",
}


@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_recipe_by_superuser(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> dict:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )
        resp_raw_input = resp_node["raw_input"]

        assert resp_node["node_type"] == TEST_RECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert resp_raw_input["author"] == TEST_RECIPE[0]["input_root"]["activity"]["author"]

        # testing deletion
        node_xid_to_delete_xid = TEST_RECIPE_WITH_XID[0]["input_root"]["activity"]["xid"]
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": node_xid_to_delete_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_MUTATION_LOG)
        saved_calculation = response.json()["batch"][0]

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"), "uid": test_kitchen_by_superuser_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_superuser_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_superuser_uid
        assert response.json().get("status") is True

        # and check if they can access this recipe
        node_xid_to_access_xid = TEST_RECIPE_WITH_XID[0]["input_root"]["activity"]["xid"]
        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": node_xid_to_access_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # and if they can do the same by specifying the root-flow-id
        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # check if listing of recipe works from all possible endpoints
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_superuser_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        # check if admin can't access listing of superuser's recipe
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_superuser_uid},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Check if superuser can retrieve calculation
        response = await ac.get(
            f"/v2/calculation/graphs/{saved_calculation.get('uid')}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        saved_calculation_without_root_node_uid = copy.deepcopy(saved_calculation)
        saved_calculation_without_root_node_uid[
            "root_node_uid"
        ] = None  # root node uid is not persisted (only child_of_root_node_uid is).
        assert response.json() == saved_calculation_without_root_node_uid

        # Verify if admin cannot retrieve calculation
        response = await ac.get(
            f"/v2/calculation/graphs/{saved_calculation.get('uid')}",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        return test_recipe_post_response


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_superuser(create_delete_and_get_recipe_by_superuser: dict) -> None:
    test_recipe_post_response = create_delete_and_get_recipe_by_superuser
    batch = test_recipe_post_response.get("batch")
    resp_node = batch[0]["final_root"]["activity"]
    assert is_close(
        find_quantity_value(
            resp_node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
        ),
        TEST_RECIPE_EXPECTED_CO2,
    )


@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_recipe_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # test if admin can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_RECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )
        resp_raw_input = resp_node["raw_input"]

        assert resp_node["node_type"] == TEST_RECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert resp_raw_input["author"] == TEST_RECIPE[0]["input_root"]["activity"]["author"]

        # testing deletion
        # making sure that an admin can't delete a resource that's not from their group
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 404)

        # test that the admin can delete a resource from their group
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: check that it was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "adding read permission to user should result in status 200"
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # and check if they can access this recipe
        # by specifying the root-flow-id
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # and by specifying the root-activity-id
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        assert response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid") is not None
        return response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid")


# this fixture has to be separated so that there's no conflict
# during `test_get_..._node_types_from_access_group` test setup
@pytest_asyncio.fixture(scope="function")
async def check_admin_recipe_listing(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_delete_and_get_recipe_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    _ = create_delete_and_get_recipe_by_admin
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # check if listing of recipe works from all possible endpoints
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_admin_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_admin(check_admin_recipe_listing: None) -> None:
    _ = check_admin_recipe_listing


TEST_RECIPE_WITH_SUBRECIPE_ID = "test_recipe_with_subrecipe_id"
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE_WITHOUT_DRYING = 0.00508026315
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE = (
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE_WITHOUT_DRYING * ONION_DRYING_UPSCALING
)
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_DRYING = 0.05153010942784783
GREENHOUSE_CONTRIBUTION = (
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_DRYING
    - TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE_WITHOUT_DRYING
)
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 = (
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE + GREENHOUSE_CONTRIBUTION
)
# 14.2.: value changed from 0.00508026315 to 0.05153010942784783 due to added greenhouse for the tomato in FR
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU = 0.03716871226434797
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE = 14.071595885745143
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE_LEGACY = 36.949379341739714
TEST_RECIPE_WITH_SUBRECIPE = [
    {
        "input_root": {
            "activity": {
                "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "servings_deprecated": 140,
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": TEST_RECIPE_ID},
                    "transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "processing": "",
                    # "raw_conservation": {"value": dried", "language": "en"}  # Removed dried to test greenhouse GFM.
                    "packaging": "",
                },
            ],
        }
    },
]
TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key in PROPS_UNUSED
        },
        **{
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
    },
    "mutation_type": "AddNodeMutation",
}


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_by_superuser(
    app: FastAPIReturn, create_delete_and_get_recipe_by_superuser: dict, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "xid", TEST_RECIPE_WITH_SUBRECIPE_ID
        )
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert resp_node["node_type"] == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG)

        # check if regular user can access this subrecipe
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )


# this test is encapsulated into a fixture
# for using the final state in `test_get_..._node_types_from_access_group` tests
@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_subrecipe_by_admin(
    app: FastAPIReturn, create_delete_and_get_recipe_by_admin: str, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_admin
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "xid", TEST_RECIPE_WITH_SUBRECIPE_ID
        )
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert resp_node["node_type"] == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG)

        # check if regular user can access this subrecipe
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        assert response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid") is not None
        return response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid")


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_by_admin(create_delete_and_get_subrecipe_by_admin: None) -> None:
    _ = create_delete_and_get_subrecipe_by_admin


TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_ID = "test_recipe_with_subrecipe_with_link_to_sub_flow"
TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW = [
    {
        "input_root": {
            "activity": {
                "xid": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "servings_deprecated": 140,
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": TEST_RECIPE_FLOW_ID},
                    "transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "processing": "",
                    "raw_conservation": {"value": "dried", "language": "en"},
                    "packaging": "",
                },
            ],
        }
    },
]
TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key in PROPS_UNUSED
        },
        **{
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
    },
    "mutation_type": "AddNodeMutation",
}


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_with_link_to_sub_flow_by_superuser(
    app: FastAPIReturn, create_delete_and_get_recipe_by_superuser: dict, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert (
            resp_node["node_type"]
            == TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW[0]["input_root"]["activity"]["node_type"]
        )
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_WITHOUT_GREENHOUSE,  # Tomato is dried: No greenhouse.
        )


@pytest.mark.asyncio
async def test_transient_calculations(
    app: FastAPIReturn,
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        # not specifying a namespace here

        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        TEST_RECIPE_TRANSIENT = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_TRANSIENT = change_in_every_activity_in_batch(
            TEST_RECIPE_TRANSIENT, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        TEST_RECIPE_TRANSIENT[0]["transient"] = True
        TEST_RECIPE_TRANSIENT[0]["namespace_uid"] = CUSTOMER.get("namespace")

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_TRANSIENT},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("namespace"),
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        batch = response.json().get("batch")
        assert batch[0]["final_root"] is None  # Not found because it is transient.


@pytest.mark.asyncio
async def test_reuse_xid(
    app: FastAPIReturn,
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        # Insert basic recipe
        test_recipe_1 = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        test_recipe_1 = change_in_every_activity_in_batch(
            test_recipe_1, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        test_recipe_1[0]["namespace_uid"] = CUSTOMER.get("namespace")
        test_recipe_1[0]["skip_calc"] = True

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": test_recipe_1},
        )
        inserted_node_uid = response.json()["batch"][0]["child_of_root_node_uid"]

        # Insert a new activity-flow pair with link to sub-node.
        linking_node_xid = "linking-node-xid"
        linking_node_batch = [
            {
                "input_root": {
                    "activity": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                        "node_type": FoodProcessingActivityNode.__name__,
                    },
                    "sub_flows": [
                        {
                            "node_type": FoodProductFlowNode.__name__,
                            "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                            "link_to_sub_node": {"uid": inserted_node_uid},
                        },
                    ],
                },
                "skip_calc": True,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        linking_node_batch_with_conservation = [
            {
                "input_root": {
                    "activity": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                        "node_type": FoodProcessingActivityNode.__name__,
                        "production_amount": {"unit": "gram", "value": 50},
                    },
                    "sub_flows": [
                        {
                            "node_type": FoodProductFlowNode.__name__,
                            "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                            "raw_conservation": {"value": "fresh", "language": "en"},
                            "amount_in_original_source_unit": {"unit": "gram", "value": 50},
                        },
                    ],
                },
                "skip_calc": True,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_conservation},
        )
        assert response.status_code == 200

        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("namespace"),
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")
        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")["uid"]
            == inserted_node_uid
        )

        test_recipe_final = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        test_recipe_final[0]["input_root"]["sub_flows"][0] = {
            "node_type": FoodProductFlowNode.__name__,
            "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
            "amount_in_original_source_unit": {"unit": "gram", "value": 50},
            "raw_production": {"value": "greenhouse", "language": "en"},
            "raw_conservation": {"value": "fresh", "language": "en"},
            "flow_location": "spain",
            "transport": "air",
            "link_to_sub_node": {"xid": linking_node_xid},
        }
        test_recipe_final[0]["namespace_uid"] = CUSTOMER.get("namespace")

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": test_recipe_final},
        )
        assert response.status_code == 200
        assert is_close(
            find_quantity_value(
                response.json()["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        linking_node_batch_with_new_product_name = [
            {
                "input_root": {
                    "activity": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                        "node_type": FoodProcessingActivityNode.__name__,
                        "production_amount": {"unit": "gram", "value": 50},
                    },
                    "sub_flows": [
                        {
                            "node_type": FoodProductFlowNode.__name__,
                            "product_name": [{"language": "de", "value": "Not Kürbisrisotto"}],
                            "amount_in_original_source_unit": {"unit": "gram", "value": 50},
                        },
                    ],
                },
                "skip_calc": True,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_new_product_name},
        )
        assert response.status_code == 200

        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("namespace"),
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        # The product name changed, and the link_to_sub_node should also be 'erased.'
        assert not response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")


TEST_DIAMOND_ID = "diamond-top-recipe"
TEST_DIAMOND_SUBRECIPE_1_ID = "diamond-sub-recipe-1"
TEST_DIAMOND_SUBRECIPE_2_ID = "diamond-sub-recipe-2"
TEST_DIAMOND_SUBSUBRECIPE_ID = "diamond-sub-sub-recipe"
TEST_DIAMOND_SUB3RECIPE_ID = "diamond-sub-sub-sub-recipe"
TEST_DIAMOND_LOCATION_ON_ROOT = [
    {
        "return_final_graph": True,
        "namespace_uid": CUSTOMER.get("namespace"),
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_ID,
                "name": "Top Activity Node",
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "activity_date": "2023-06-20",
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBRECIPE_1_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Kürbisrisotto 1"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBRECIPE_2_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Kürbisrisotto 2"}],
                    "amount_in_original_source_unit": {"value": 100, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("namespace"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBRECIPE_1_ID,
                "name": "SUBRECIPE 1",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBSUBRECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subrecipe"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("namespace"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBRECIPE_2_ID,
                "name": "SUBRECIPE 2",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBSUBRECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subrecipe"}],
                    "amount_in_original_source_unit": {"value": 25, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("namespace"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBSUBRECIPE_ID,
                "name": "SUBSUBRECIPE",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUB3RECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subsubrecipe"}],
                    "amount_in_original_source_unit": {"value": 1000, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("namespace"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUB3RECIPE_ID,
                "name": "SUBSUBSUBRECIPE",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100191",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 400, "unit": "gram"},
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100894",
                    "product_name": [{"language": "de", "value": "Zwiebeln"}],
                    "amount_in_original_source_unit": {"value": 600, "unit": "gram"},
                },
            ],
        },
    },
]

TEST_DIAMOND_LOCATION_ON_BRANCHES = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_LOCATION_ON_BRANCHES[0]["input_root"]["sub_flows"][1]["flow_location"] = "Zürich Schweiz"
TEST_DIAMOND_LOCATION_ON_BRANCHES[1]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

TEST_DIAMOND_LOCATION_ON_LEAF = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_LOCATION_ON_LEAF[-2]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES[0]["input_root"]["sub_flows"][1]["flow_location"] = "france"
TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES[1]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE = {
    "Italy": True,
    "Morocco": False,
    "Netherlands": True,
    "Spain": True,
    "Switzerland": True,
}
EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE = {
    "Austria": False,
    "Belgium": False,
    "Brazil": False,
    "France": False,
    "Germany": False,
    "Italy": True,
    "Netherlands": True,
    "Spain": True,
    "Switzerland": True,
    "United Kingdom": False,
    "United States": False,
}

DIAMOND_EXPECTED_CO2 = 0.04767813956654421


def get_sub_nodes_from_graph(graph: dict, node: dict) -> list[dict]:
    sub_node_uids = node.get("sub_node_uids")
    return [elem for elem in graph if elem.get("uid") in sub_node_uids]


def find_node_from_graph_by_xid(graph: dict, xid: str) -> dict:
    nodes_with_xid = [elem for elem in graph if elem.get("xid") == xid]
    assert len(nodes_with_xid) == 1
    return nodes_with_xid[0]


@pytest.mark.asyncio
async def test_diamond(
    app: FastAPIReturn,
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('namespace')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("namespace"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_ROOT},
        )

        response_json = response.json()
        # Testing that inheritance in Transport GFM has run correctly.

        def get_origins_and_transports_of_nth_ingredient(graph: dict, n: int) -> dict:
            """Finds all origin splits and if transport exists for them."""
            sub3_recipe = find_node_from_graph_by_xid(graph, TEST_DIAMOND_SUB3RECIPE_ID)
            ingredients = get_sub_nodes_from_graph(graph, sub3_recipe)
            ingredient = ingredients[n]

            origin_splits = get_sub_nodes_from_graph(graph, get_sub_nodes_from_graph(graph, ingredient)[0])

            return_dict = {}

            for origin_split in origin_splits:
                country_name = origin_split["flow_location"][0]["term_name"]

                if "Main carriage" in [
                    elem["product_name"].get("source_data_raw", [{}])[0].get("value")
                    for elem in get_sub_nodes_from_graph(graph, get_sub_nodes_from_graph(graph, origin_split)[0])
                ]:
                    transport_exists = True
                else:
                    transport_exists = False

                return_dict[country_name] = transport_exists

            return return_dict

        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_BRANCHES},
        )

        response_json = response.json()

        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_LEAF},
        )

        response_json = response.json()
        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES},
        )

        response_json = response.json()
        assert response_json["batch"][0]["final_root"]["activity"]["impact_assessment"] is None
