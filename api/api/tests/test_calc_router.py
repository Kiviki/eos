"Testing calculation router."
import asyncio
import base64
import copy
import uuid

import pytest
from deepdiff import DeepDiff
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from api.app.settings import Settings
from core.domain.nodes import FoodProcessingActivityNode
from core.service.service_provider import ServiceLocator
from database.postgres.pg_term_mgr import IPCC_2013_GWP_20, IPCC_2013_GWP_100

from .conftest import FastAPIReturn, change_in_every_activity_in_batch, is_close
from .test_api_for_recipes import (
    ONION_DRYING_UPSCALING,
    TEST_RECIPE,
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_CO2_OF_ONION_INGREDIENT,
    TEST_RECIPE_EXPECTED_CO2_OF_TOMATO_INGREDIENT,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT,
    TEST_RECIPE_EXPECTED_VITASCORE,
    TEST_RECIPE_EXPECTED_VITASCORE_LEGACY,
    TEST_RECIPE_ID,
    TEST_RECIPE_WITH_SUBRECIPE,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE_LEGACY,
    find_quantity_value,
)

TEST_RECIPE_EXPECTED_GWP_20_CO2 = 65.91 / 1000 * ONION_DRYING_UPSCALING
TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO = 0.00144415635
TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION_WITHOUT_UPSCALING = 0.00019484992
TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION = 0.00019484992 * ONION_DRYING_UPSCALING
TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION = (
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION + TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO
)

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()


def async_client(app: FastAPIReturn) -> AsyncClient:
    """Helper function for creating an async client."""
    return AsyncClient(
        app=app, base_url="http://localhost:8040", headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"}
    )


@pytest.mark.asyncio
async def test_simple_calculation(app: FastAPIReturn) -> None:
    """Test calculation endpoint for a simple recipe."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": TEST_RECIPE},
        )
        assert response.status_code == status.HTTP_200_OK
        batch = response.json().get("batch")
        resp_root_flow = batch[0]["final_root"]["flow"]
        resp_root_activity = batch[0]["final_root"]["activity"]
        resp_raw_input = resp_root_activity["raw_input"]

        assert resp_root_flow is not None

        assert resp_root_activity["node_type"] == TEST_RECIPE[0]["input_root"]["activity"]["node_type"]
        assert batch[0]["final_root"].get("sub_flows")[0]
        assert resp_raw_input["author"] == TEST_RECIPE[0]["input_root"]["activity"]["author"]
        assert is_close(
            find_quantity_value(
                resp_root_activity["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        rainforest_critical_products_as_list = list(
            resp_root_flow["rainforest_critical_products"]["amount_for_activity_production_amount"].values()
        )

        rainforest_critical_products_amount = None
        for rainforest_critical_products in rainforest_critical_products_as_list:
            if rainforest_critical_products.get("quantity_term_xid") == "EOS_rainforest_conservation_certified":
                rainforest_critical_products_amount = rainforest_critical_products.get("quantity").get("value")

        assert rainforest_critical_products_amount
        assert is_close(rainforest_critical_products_amount, TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT)
        assert resp_root_flow["rainforest_critical_products"]["rainforest_rating"] == 3

        animal_products_as_list = list(
            resp_root_flow["animal_products"]["amount_for_activity_production_amount"].values()
        )

        animal_products_amount = None
        for animal_products in animal_products_as_list:
            if animal_products.get("quantity_term_xid") == "EOS_not_certified_for_animal_welfare":
                animal_products_amount = animal_products.get("quantity").get("value")

        assert animal_products_amount is not None
        assert is_close(animal_products_amount, 0.0)
        assert resp_root_flow["animal_products"]["animal_welfare_rating"] == 3

        ingredient_one_food_tags = frozenset(
            term["term_name"] for term in batch[0]["final_root"]["sub_flows"][0]["glossary_tags"]
        )
        ingredient_two_food_tags = frozenset(
            term["term_name"] for term in batch[0]["final_root"]["sub_flows"][1]["glossary_tags"]
        )
        assert '"FRESH" LABEL CLAIM' in ingredient_one_food_tags
        assert "Highly perishable" in ingredient_one_food_tags

        assert "DEHYDRATED OR DRIED" in ingredient_two_food_tags
        assert "Shelf-stable" in ingredient_two_food_tags
        assert "Rainforest Conservation Certified" in ingredient_two_food_tags


@pytest.mark.asyncio
async def test_different_namespace_reused_uid(app: FastAPIReturn) -> None:
    customers = [
        {"namespace": str(uuid.uuid4()), "name": "first customer"},
        {"namespace": str(uuid.uuid4()), "name": "second customer"},
    ]

    customers_user = [
        {"email": "user@firstcustomer.com", "password": "", "is_superuser": False},
        {"email": "user@secondcustomer.com", "password": "", "is_superuser": False},
    ]
    user_tokens = []
    customers_user_id = [str(uuid.uuid4()), str(uuid.uuid4())]
    kitchens = [
        {
            "xid": "test_kitchen_basic_id",
            "type": "kitchen",
            "name": "first_kitchen",
            "location": "Zurich, Switzerland",
            "email": None,
            "language": None,
            "creator": customers_user_id[0],
        },
        {
            "xid": "test_kitchen_basic_id",
            "type": "kitchen",
            "name": "second_kitchen",
            "location": "Zurich, Switzerland",
            "email": None,
            "language": None,
            "creator": customers_user_id[1],
        },
    ]

    common_uid = str(uuid.uuid4())

    customers_checked = [False, False]

    await service_locator.service_provider.postgres_db.connect(schema="test_pg")

    test_recipe_with_uid = None
    for customer_index, customer in enumerate(customers):
        async with async_client(app) as ac:
            response = await ac.put(
                f"/v2/batch/customers/{customer['namespace']}",
                headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
                json={
                    "customer": {
                        "name": customer["name"],
                    }
                },
            )

            customer["token"] = response.json().get("auth_token")
            customer["token"] = base64.b64encode(customer["token"].encode("utf-8")).decode("utf-8")

            response = await ac.put(
                f"/v2/users/{customers_user_id[customer_index]}/",
                headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
                json={
                    "user": customers_user[customer_index],
                },
            )
            customers_user[customer_index]["token"] = response.json().get("auth_token")
            user_tokens.append(
                base64.b64encode(customers_user[customer_index]["token"].encode("utf-8")).decode("utf-8")
            )

            await ac.post(
                "/v2/access-groups/upsert_access_group",
                headers={"Authorization": f"Basic {user_tokens[customer_index]}"},
                json={"access_group": kitchens[customer_index], "namespace_uid": customer["namespace"]},
            )

            test_recipe_with_uid = change_in_every_activity_in_batch(TEST_RECIPE, "uid", common_uid)
            test_recipe_with_uid = change_in_every_activity_in_batch(
                test_recipe_with_uid, "access_group_xid", kitchens[customer_index]["xid"]
            )
            test_recipe_with_uid[0]["namespace_uid"] = customer["namespace"]

            response = await ac.post(
                "/v2/calculation/graphs",
                headers={"Authorization": f"Basic {user_tokens[customer_index]}"},
                json={"batch": test_recipe_with_uid},
            )

            # Check that the first customer's node is correctly calculated.
            if customer_index == 0:
                batch = response.json().get("batch")
                assert is_close(
                    find_quantity_value(
                        batch[0]["final_root"]["activity"]["impact_assessment"][
                            "amount_for_activity_production_amount"
                        ],
                        "IPCC 2013 climate change GWP 100a",
                    ),
                    TEST_RECIPE_EXPECTED_CO2,
                )
                assert batch[0].get("statuscode") == 200
                customers_checked[customer_index] = True
            # Check that the second customer's node cannot be calculated under the same uid.
            elif customer_index == 1:
                batch = response.json().get("batch")
                assert batch[0].get("statuscode") == 401
                assert (
                    batch[0].get("message")
                    == f"Node uid {common_uid} already exists in the database under a different access group."
                )
                customers_checked[customer_index] = True

    assert len(customers_checked) == 2
    assert all(customers_checked)

    batch_to_access_wrong_ag = [
        {
            "input_root": {
                "existing_root": {
                    "uid": common_uid,
                    "access_group_xid": kitchens[1]["xid"],
                }
            },
            "namespace_uid": customers[1]["namespace"],
        }
    ]

    batch_to_delete_wrong_ag = [
        {
            "nodes_dto": {
                "existing_node": {
                    "uid": common_uid,
                    "access_group_xid": kitchens[1]["xid"],
                }
            },
            "namespace_uid": customers[1]["namespace"],
        }
    ]

    # Test accessing the item with a different access group xid.
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_access_wrong_ag},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 404
        assert batch[0].get("message") == "node was not found in that access_group"

    # Test deleting the item with a different access group xid.
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_delete_wrong_ag},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 404
        assert batch[0].get("message") == "node was not found in that access_group"

    not_existing_ag_uid = str(uuid.uuid4())
    batch_to_access_wrong_ag_uid = [
        {
            "input_root": {
                "existing_root": {
                    "uid": common_uid,
                    "access_group_uid": not_existing_ag_uid,
                }
            },
            "namespace_uid": customers[1]["namespace"],
        }
    ]

    batch_to_delete_wrong_ag_uid = [
        {
            "nodes_dto": {
                "existing_node": {
                    "uid": common_uid,
                    "access_group_uid": not_existing_ag_uid,
                }
            },
            "namespace_uid": customers[1]["namespace"],
        }
    ]

    # Test accessing the item with a different access group uid.
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_access_wrong_ag_uid},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 404
        assert batch[0].get("message") == f"Group {not_existing_ag_uid} does not exist."

    # Test deleting the item with a different access group uid.
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_delete_wrong_ag_uid},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 404
        assert batch[0].get("message") == f"Group {not_existing_ag_uid} does not exist."

    # Test deleting with the correct access_group_uid
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "uid": common_uid,
                    "access_group_xid": kitchens[0]["xid"],
                }
            },
            "namespace_uid": customers[0]["namespace"],
        }
    ]
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {user_tokens[0]}"},
            json={"batch": batch_to_delete},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 200

    # Now that the node is deleted, we should be able to create it under a different access_group_uid,
    # then access it and delete it.
    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": test_recipe_with_uid},
        )

    batch = response.json().get("batch")
    assert is_close(
        find_quantity_value(
            batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
            "IPCC 2013 climate change GWP 100a",
        ),
        TEST_RECIPE_EXPECTED_CO2,
    )
    assert batch[0].get("statuscode") == 200

    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_access_wrong_ag},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 200
        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

    async with async_client(app) as ac:
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {user_tokens[1]}"},
            json={"batch": batch_to_delete_wrong_ag},
        )
        batch = response.json().get("batch")
        assert batch[0].get("statuscode") == 200


@pytest.mark.asyncio
async def test_calculation_with_subrecipe(app: FastAPIReturn) -> None:
    """Test calculation endpoint for a recipe that contains a subrecipe."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": [*TEST_RECIPE_WITH_SUBRECIPE, *TEST_RECIPE]},
        )
        assert response.status_code == status.HTTP_200_OK
        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        assert is_close(
            batch[0]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["flow"]["vitascore"]["amount_for_activity_production_amount"],
                "Vita score",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE,
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["flow"]["vitascore_legacy"]["amount_for_activity_production_amount"],
                "Vita score",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE_LEGACY,
        )

        assert is_close(
            find_quantity_value(
                batch[1]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        assert is_close(
            batch[1]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU,
        )

        assert is_close(
            find_quantity_value(
                batch[1]["final_root"]["flow"]["vitascore"]["amount_for_activity_production_amount"],
                "Vita score",
            ),
            TEST_RECIPE_EXPECTED_VITASCORE,
        )

        assert is_close(
            find_quantity_value(
                batch[1]["final_root"]["flow"]["vitascore_legacy"]["amount_for_activity_production_amount"],
                "Vita score",
            ),
            TEST_RECIPE_EXPECTED_VITASCORE_LEGACY,
        )

        for item in batch:
            assert item["final_graph"] is None
            assert item["final_graph_table"] is None


@pytest.mark.asyncio
async def test_calculation_with_subrecipe_and_graph_and_table(app: FastAPIReturn) -> None:
    """Test calculation endpoint response with subrecipe and `return_final_graph` and `return_data_as_table` flags."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        batch = copy.deepcopy([*TEST_RECIPE_WITH_SUBRECIPE, *TEST_RECIPE])
        for batch_item in batch:
            batch_item["return_final_graph"] = True
            batch_item["return_data_as_table"] = True
            batch_item["requested_quantity_references"] = [
                "amount_for_root_node",
                "amount_for_activity_production_amount",
            ]

        response = await ac.post(
            "/v2/calculation/graphs",
            json={
                "batch": batch,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        assert is_close(
            find_quantity_value(
                batch[1]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        assert all(
            [
                sub_flow["amount_in_original_source_unit"]["unit"] == "gram"
                for sub_flow in TEST_RECIPE[0]["input_root"]["sub_flows"]
            ]
        ), "In this comparison, the unit of the subflow is assumed to be 'gram'."

        production_amount = sum(
            [
                sub_flow["amount_in_original_source_unit"]["value"] / 1000.0
                for sub_flow in TEST_RECIPE[0]["input_root"]["sub_flows"]
            ]
        )

        assert (
            TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]["xid"] == TEST_RECIPE_ID
        ), "Here we assume that the zeroth subflow is the link to the subrecipe."

        assert (
            TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["sub_flows"][0]["amount_in_original_source_unit"]["unit"]
            == "gram"
        ), "In this comparison, the unit of the subflow is assumed to be 'gram'."

        flow_amount = (
            TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["sub_flows"][0]["amount_in_original_source_unit"]["value"]
            / 1000.0
        )

        check_batch_node_calculation(
            batch[0],
            final_graph_length=26,
            final_table_length=18,
            kg_co2eq_per_root_product=TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
            kg_co2eq_per_node_production_amount=TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
            node_to_check_in_graph_index=1,
            node_to_check_in_table_index=0,
        )
        check_batch_node_calculation(
            batch[0],
            final_graph_length=26,
            final_table_length=18,
            kg_co2eq_per_root_product=TEST_RECIPE_EXPECTED_CO2 / production_amount * flow_amount,
            kg_co2eq_per_node_production_amount=TEST_RECIPE_EXPECTED_CO2,
            node_to_check_in_graph_index=15,
            node_to_check_in_table_index=2,
        )
        check_batch_node_calculation(
            batch[1],
            final_graph_length=12,
            final_table_length=8,
            kg_co2eq_per_root_product=TEST_RECIPE_EXPECTED_CO2,
            kg_co2eq_per_node_production_amount=TEST_RECIPE_EXPECTED_CO2,
            node_to_check_in_graph_index=1,
            node_to_check_in_table_index=0,
        )


@pytest.mark.asyncio
async def test_output_graph(app: FastAPIReturn) -> None:
    """Test calculation endpoint response with `return_final_graph` flag and without `return_data_as_table` flag."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        batch = copy.deepcopy(TEST_RECIPE)
        for batch_item in batch:
            batch_item["return_final_graph"] = True
            batch_item["return_data_as_table"] = False

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": batch},
        )
        assert response.status_code == status.HTTP_200_OK

        batch = response.json().get("batch")

        # checking output graph response
        check_output_graph(batch[0])

        # checking output table response
        assert batch[0].get("final_graph_table") is None


@pytest.mark.asyncio
async def test_output_graph_and_table(app: FastAPIReturn) -> None:
    """Test calculation endpoint response with `return_final_graph` and `return_data_as_table` flags."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        batch = copy.deepcopy(TEST_RECIPE)
        for batch_item in batch:
            batch_item["return_final_graph"] = True
            batch_item["return_data_as_table"] = True

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": batch},
        )
        assert response.status_code == status.HTTP_200_OK

        batch = response.json().get("batch")

        # checking output graph response
        check_output_graph(batch[0])

        # checking output table response
        check_simple_recipe_output_table(batch[0])


@pytest.mark.asyncio
async def test_output_sheet_without_graph_and_with_table(app: FastAPIReturn) -> None:
    """Test calculation endpoint response without `return_final_graph` flag and with `return_data_as_table` flag."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        batch = copy.deepcopy(TEST_RECIPE)

        for batch_item in batch:
            batch_item["return_data_as_table"] = True

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": batch},
        )
        assert response.status_code == status.HTTP_200_OK

        batch = response.json().get("batch")

        # checking output graph response
        assert batch[0].get("final_graph") is None

        # checking output table response
        check_simple_recipe_output_table(batch[0])


def check_batch_node_calculation(
    calculation: dict,
    final_graph_length: int = 0,
    final_table_length: int = 0,
    kg_co2eq_per_root_product: float = 0.0,
    kg_co2eq_per_node_production_amount: float = 0.0,
    node_to_check_in_graph_index: int = 0,
    node_to_check_in_table_index: int = 0,
) -> None:
    """Helper fcuntion to check final graph and table outputs in a calculation with a recipe and a subrecipe."""
    assert len(calculation["final_graph"]) == final_graph_length
    assert len(calculation["final_graph_table"]) == final_table_length

    assert is_close(
        find_quantity_value(
            calculation["final_graph"][node_to_check_in_graph_index]["impact_assessment"][
                "amount_for_activity_production_amount"
            ],
            "IPCC 2013 climate change GWP 100a",
        ),
        kg_co2eq_per_node_production_amount,
    )
    assert is_close(
        find_quantity_value(
            calculation["final_graph"][node_to_check_in_graph_index]["impact_assessment"]["amount_for_root_node"],
            "IPCC 2013 climate change GWP 100a",
        ),
        kg_co2eq_per_root_product,
    )
    assert is_close(
        calculation["final_graph_table"][node_to_check_in_table_index]["kg_co2eq_per_root_product"],
        kg_co2eq_per_root_product,
    )
    assert is_close(
        calculation["final_graph_table"][node_to_check_in_table_index]["kg_co2eq_per_node_production_amount"],
        kg_co2eq_per_node_production_amount,
    )


def check_output_graph(resp_data: dict) -> None:
    """Helper function for asserting data in node graph output."""
    node_list = resp_data.get("final_graph")
    assert len(node_list) == 12, "total number of returned nodes should be 12"

    node_dict = {node["uid"]: node for node in node_list}
    recipe_node = node_dict[resp_data["child_of_root_node_uid"]]

    sub_node_uids = recipe_node["sub_node_uids"]
    assert len(sub_node_uids) == 2, "root node should have 2 sub nodes"
    assert sub_node_uids[0] in node_dict, "ingredient is missing in the node list"
    assert sub_node_uids[1] in node_dict, "ingredient is missing in the node list"

    # Check if term_name is correctly serialized and, more specifically, if converted unit is kilogram:
    assert node_dict[sub_node_uids[0]]["amount"]["unit_term_name"].lower() == "kilogram"
    assert node_dict[sub_node_uids[1]]["amount"]["unit_term_name"].lower() == "kilogram"

    # the two child nodes should have a converted_amount with term_xid set to EOS_gram:
    assert node_dict[sub_node_uids[0]]["amount"]["unit_term_xid"] == "EOS_kilogram"
    assert node_dict[sub_node_uids[1]]["amount"]["unit_term_xid"] == "EOS_kilogram"
    assert is_close(
        find_quantity_value(
            recipe_node["impact_assessment"]["amount_for_activity_production_amount"],
            "IPCC 2013 climate change GWP 100a",
        ),
        TEST_RECIPE_EXPECTED_CO2,
    )


def check_simple_recipe_output_table(calculation: dict) -> None:
    """Helper function for asserting data in node table output."""
    node_table_list = calculation.get("final_graph_table")
    assert len(node_table_list) == 8, "total number of returned sheet rows should be 8"

    root_node_sheet_row = node_table_list[0]
    recipe_name = TEST_RECIPE[0].get("input_root").get("activity").get("titles")[0].get("value")

    assert root_node_sheet_row.get("name") == recipe_name
    assert root_node_sheet_row.get("level") == 0
    assert root_node_sheet_row.get("tree_index") == ""
    assert root_node_sheet_row.get("location") == "CHE.26.13.21_1"

    # TODO: Probably it would be better to add all required ecotransit responses as mocked jsons to prevent this error:
    assert root_node_sheet_row.get("errors").startswith(
        "Cannot connect to EcoTransit!"
    ), "An unexpected error has occurred during the calculation."

    assert root_node_sheet_row.get("error_squares") == "", "An error has occurred during the calculation."
    assert root_node_sheet_row.get("matrix_gfm_error") == "", "A Matrix GFM error has occurred during the calculation."
    assert is_close(root_node_sheet_row.get("kg_co2eq_per_root_product"), TEST_RECIPE_EXPECTED_CO2), "unexpected amount"
    assert is_close(
        root_node_sheet_row.get("kg_co2eq_per_node_production_amount"), TEST_RECIPE_EXPECTED_CO2
    ), "unexpected amount"
    assert is_close(
        root_node_sheet_row.get("liter_scarce_water_per_root_product"), TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION
    ), "unexpected scarce water consumption"
    assert is_close(
        root_node_sheet_row.get("liter_scarce_water_production_amount"), TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION
    ), "unexpected scarce water consumption"

    check_ingredient_node_sheet_row(
        node_table_list[2],
        brightway_node="Key: ('EDB', '437443338484ca565ae5a832d81fcc52_copy1')\n"
        "Name: market for tomato (w/o transport)\nReference product: tomato\nLocation: GLO",
        kg_co2eq_per_node_production_amount=0.0,
        kg_co2eq_per_root_product=0.0,
        location="ESP",
        matched_terms="06330 - TOMATOES (EFSA FOODEX2) [A0DMX]",
        nutrient_file="491",
        tree_index=".1.1",
        weight_lean=150.0,
    )
    check_ingredient_node_sheet_row(
        node_table_list[6],
        is_dried=True,
        is_subdivision=True,
        brightway_node="Key: ('EDB', '437443338484ca565ae5a832d81fcc52_copy1')\n"
        "Name: market for onion (w/o transport)\nReference product: onion\nLocation: GL",
        kg_co2eq_per_node_production_amount=TEST_RECIPE_EXPECTED_CO2 / (0.078 * ONION_DRYING_UPSCALING),
        kg_co2eq_per_root_product=TEST_RECIPE_EXPECTED_CO2,
        location="",
        matched_terms="10006006 - ONIONS (GS1 GPC) [A1480]",
        nutrient_file="110",
        tree_index=".2.1.1",
        weight_lean=78.0 * ONION_DRYING_UPSCALING,
        level=3,
    )


def check_ingredient_node_sheet_row(
    row: dict,
    is_dried: bool = False,
    is_subdivision: bool = False,
    brightway_node: str = "",
    kg_co2eq_per_node_production_amount: float = 0.0,
    kg_co2eq_per_root_product: float = 0.0,
    location: str = "",
    matched_terms: str = "",
    nutrient_file: str = "",
    tree_index: str = "",
    weight_lean: float = 0.0,
    level: int = 2,
) -> None:
    """Helper function for asserting data in ingredient node sheet row output."""
    assert row.get("level") == level
    assert row.get("tree_index") == tree_index
    assert is_close(row.get("weight_lean"), weight_lean, tol=1e-9)
    assert row.get("weight_unit").lower() == "gram"
    assert row.get("detailed_node_type") == "FoodProductFlowNode"
    assert row.get("general_node_type") == "flow"
    assert row.get("location") == location
    assert row.get("is_subdivision") is is_subdivision
    assert row.get("is_dried") == is_dried
    assert row.get("matched_terms") == matched_terms
    assert row.get("nutrient_file") == nutrient_file
    assert row.get("fao_code") == ""
    assert row.get("brightway_node") == brightway_node
    assert row.get("errors") == ""
    assert row.get("error_squares") == ""
    assert row.get("matrix_gfm_error") == ""
    assert is_close(row.get("kg_co2eq_per_root_product"), kg_co2eq_per_root_product), "unexpected amount"
    assert is_close(
        row.get("kg_co2eq_per_node_production_amount"), kg_co2eq_per_node_production_amount
    ), "unexpected amount"


@pytest.mark.asyncio
async def test_auxillary_root_node_calculation(app: FastAPIReturn) -> None:
    """Checks if the system process caches are calculated correctly for `fixed_depth_for_calculating_supply=2`."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        # post a recipe and save the root node and its ingredients (by setting the fixed depth to 2) as system processes
        batch = copy.deepcopy(TEST_RECIPE)
        for batch_item in batch:
            batch_item["return_final_graph"] = True
            batch_item["save_as_system_process"] = True
            batch_item["fixed_depth_for_calculating_supply"] = 7
            batch_item["requested_quantity_references"] = ["amount_for_root_node"]

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": batch},
        )
        assert response.status_code == status.HTTP_200_OK

        batch = response.json().get("batch")

        # get the root node of the calculation and check that its impact_assessment is correct
        root_node = batch[0].get("final_root", {}).get("activity")
        assert is_close(
            find_quantity_value(
                root_node["impact_assessment"]["amount_for_root_node"], "IPCC 2013 climate change GWP 100a"
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # get the ingredients of the root node and check that their impact assessments are correct
        ingredients = batch[0].get("final_root", {}).get("sub_flows")
        assert len(ingredients) == 2, "root node should have 2 sub nodes"
        assert is_close(
            find_quantity_value(
                ingredients[0]["impact_assessment"]["amount_for_root_node"], "IPCC 2013 climate change GWP 100a"
            ),
            TEST_RECIPE_EXPECTED_CO2_OF_TOMATO_INGREDIENT,
        )
        assert is_close(
            find_quantity_value(
                ingredients[1]["impact_assessment"]["amount_for_root_node"], "IPCC 2013 climate change GWP 100a"
            ),
            TEST_RECIPE_EXPECTED_CO2_OF_ONION_INGREDIENT,
        )

        # get the final graph of the calculation
        calculation = batch[0]
        node_list = calculation.get("final_graph")
        node_dict = {node["uid"]: node for node in node_list}

        # load the activity corresponding to the ingredient 1
        child_of_root_node_uid = calculation.get("child_of_root_node_uid")
        recipe_node_entry = node_dict.get(child_of_root_node_uid)
        flow_to_ingredient_1_entry = node_dict.get(recipe_node_entry.get("sub_node_uids")[1])
        graph_mgr = service_locator.service_provider.postgres_db.get_graph_mgr()
        # Have to take activity nodes from four levels deeper since
        # water scarcity calculations increased node depth by 2 and nutrient subdivision by another 2.
        sub_node = node_dict.get(flow_to_ingredient_1_entry.get("sub_node_uids")[0])
        sub_node = node_dict.get(sub_node.get("sub_node_uids")[0])
        sub_node = node_dict.get(sub_node.get("sub_node_uids")[0])
        sub_node = node_dict.get(sub_node.get("sub_node_uids")[0])

        activity_ingredient_1_node = await graph_mgr.find_by_uid(sub_node.get("sub_node_uids")[0])

        activity_ingredient_1_node_batch = {
            "batch": [
                {
                    "input_root": {
                        "activity": {
                            "uid": str(activity_ingredient_1_node.uid),
                            "node_type": FoodProcessingActivityNode.__name__,
                        },
                    }
                }
            ]
        }

        # submit a new calculation for the activity corresponding to the ingredient 1
        response = await ac.post("/v2/calculation/graphs", json=activity_ingredient_1_node_batch)
        assert response.status_code == status.HTTP_200_OK
        # Should lead to errors because activity_ingredient_1_node in the database has no access_group_uid.
        assert (
            response.json()["batch"][0]["message"]
            == f"Node uid {activity_ingredient_1_node.uid} already exists in the database "
            "under a different access group."
        )

        # Temporary fix: Add access_group_uid to ModeledActivityNode such that we can submit a post request for
        # a calculation.
        # TODO: Introduce access_group_uid for ModeledActivityNodes everywhere,
        #  including in tests and Brightway Importer.
        eaternity_user_id = (
            await service_locator.service_provider.postgres_db.pg_users_mgr.list_users_from_namespace(
                Settings().EATERNITY_NAMESPACE_UUID
            )
        )[0].user_id
        access_group, _ = await service_locator.service_provider.access_group_service.upsert_access_group_by_xid_or_uid(
            xid="ingredient-1-node-access-group",
            # TODO ModeledActivity should have their own namespace
            namespace_uid=Settings().EATERNITY_NAMESPACE_UUID,
            creator=eaternity_user_id,
        )
        await service_locator.service_provider.node_service.update_node_access_group_uid(
            activity_ingredient_1_node.uid, access_group.uid
        )

        # check that the aggregated cache is not empty
        aggregated_cache = activity_ingredient_1_node.aggregated_cache
        assert aggregated_cache is not None, "There is no aggregated cache in the ingredients"

        activity_ingredient_1_node_batch["batch"][0]["input_root"]["activity"]["access_group_uid"] = str(
            access_group.uid
        )
        response = await ac.post("/v2/calculation/graphs", json=activity_ingredient_1_node_batch)

        # check that the impact assessment obtained with the aggregated cache is correct
        new_root_node = response.json().get("batch")[0].get("final_root").get("activity")

        # check that the aggregated cache is not empty
        aggregated_cache = new_root_node["aggregated_cache"]
        assert aggregated_cache is not None, "There is no aggregated cache in the final calculation"

        ingredient_amount = flow_to_ingredient_1_entry["amount"]["amount_for_root_node"]

        assert is_close(
            find_quantity_value(
                new_root_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / (ingredient_amount * ONION_DRYING_UPSCALING),
        )


@pytest.mark.asyncio
async def test_gwp_20_alone(app: FastAPIReturn) -> None:
    "GWP 20a alone."
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        recipe_w_new_impact_assessment = copy.deepcopy(TEST_RECIPE)
        for batch_item in recipe_w_new_impact_assessment:
            batch_item["requested_impact_assessments"] = [IPCC_2013_GWP_20]

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": recipe_w_new_impact_assessment},
        )
        assert response.status_code == status.HTTP_200_OK
        batch = response.json().get("batch")

        # get the root node of the calculation and check that its impact assessment is correct
        root_node = batch[0]["final_root"].get("activity")
        assert is_close(
            find_quantity_value(
                root_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            TEST_RECIPE_EXPECTED_GWP_20_CO2,
        )

        # get the ingredients of the root node and check that their impact assessments are correct
        ingredients = batch[0]["final_root"].get("sub_flows")
        assert len(ingredients) == 2, "root node should have 2 sub nodes"
        assert is_close(
            find_quantity_value(
                ingredients[0]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            0.0,
        )
        assert is_close(
            find_quantity_value(
                ingredients[1]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            TEST_RECIPE_EXPECTED_GWP_20_CO2,
        )


@pytest.mark.asyncio
async def test_gwp_20_with_gwp_100(app: FastAPIReturn) -> None:
    "GWP 20a with GWP 100a."
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        recipe_w_new_impact_assessment = copy.deepcopy(TEST_RECIPE)
        for batch_item in recipe_w_new_impact_assessment:
            batch_item["requested_impact_assessments"] = [IPCC_2013_GWP_20, IPCC_2013_GWP_100]

        response = await ac.post(
            "/v2/calculation/graphs",
            json={"batch": recipe_w_new_impact_assessment},
        )
        assert response.status_code == status.HTTP_200_OK
        batch = response.json().get("batch")

        # get the root node of the calculation and check that its impact_assessment is correct
        root_node = batch[0]["final_root"].get("activity")
        assert is_close(
            find_quantity_value(
                root_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )
        assert is_close(
            find_quantity_value(
                root_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            TEST_RECIPE_EXPECTED_GWP_20_CO2,
        )

        # get the ingredients of the root node and check that their impact assessments are correct
        ingredients = batch[0]["final_root"].get("sub_flows")
        assert len(ingredients) == 2, "root node should have 2 sub nodes"
        assert is_close(
            find_quantity_value(
                ingredients[0]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2_OF_TOMATO_INGREDIENT,
        )
        assert is_close(
            find_quantity_value(
                ingredients[1]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2_OF_ONION_INGREDIENT,
        )
        assert is_close(
            find_quantity_value(
                ingredients[0]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            0.0,
        )
        assert is_close(
            find_quantity_value(
                ingredients[1]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 20a",
            ),
            TEST_RECIPE_EXPECTED_GWP_20_CO2,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize("input_batch", [TEST_RECIPE, [*TEST_RECIPE_WITH_SUBRECIPE, *TEST_RECIPE]])
async def test_simple_calculation_async(app: FastAPIReturn, input_batch: list) -> None:
    """Test calculation endpoint for a simple recipe."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        # specify the calculation parameters
        input_batch_with_calculation_input = copy.deepcopy(input_batch)
        for item in input_batch_with_calculation_input:
            item["return_final_graph"] = True
            item["return_data_as_table"] = True
            item["return_log"] = True

        # submit the calculation and specify that the api-call should return immediately
        async_post_response = await ac.post(
            "/v2/calculation/graphs?return_immediately=True",
            json={"batch": input_batch_with_calculation_input},
        )
        assert async_post_response.status_code == status.HTTP_200_OK

        for batch_index in range(len(input_batch)):
            calculation_uid = async_post_response.json().get("batch")[batch_index]["uid"]

            # wait until the calculation is finished and then get the response
            total_waiting_time = 0
            waiting_time_between_each_request = 1
            maximum_waiting_time = 10
            while total_waiting_time < maximum_waiting_time:
                async_get_response = await ac.get(f"/v2/calculation/graphs/{calculation_uid}")
                assert async_get_response.status_code == status.HTTP_200_OK
                async_get_response_dict = async_get_response.json()
                if async_get_response_dict["final_graph"]:  # calculation is finished
                    break
                else:
                    total_waiting_time += waiting_time_between_each_request
                    await asyncio.sleep(waiting_time_between_each_request)

            assert total_waiting_time < maximum_waiting_time, "Async calculation did not succeed."

            # perform the reference calculation to compare against
            non_async_post_response = await ac.post(
                "/v2/calculation/graphs",
                json={"batch": input_batch_with_calculation_input},
            )
            assert non_async_post_response.status_code == status.HTTP_200_OK
            non_async_post_response_dict = non_async_post_response.json()["batch"][batch_index]

            # The calculation_uid should be identical to the one in async_post response
            non_async_post_response_dict["uid"] = calculation_uid

            # We must exclude the following from the DeepDiff comparison because the uid's are newly generated
            # in the non-async post request. Therefore, they are not the same as those in the first async request.
            # When only Brightway and Client nodes (which are in the database) were added, the uid's were taken from
            # the nodes persisted in the database. Therefore, the uid's did not change from one request to another.
            # However, the nodes added by Kale (e.g., by WaterScarcityGfm) has a different uid each request.
            base_path_mutations = r"root\['mutations'\]\[\d+\]"
            base_path_final_graph = r"root\['final_graph'\]\[\d+\]"

            regex_sub_node_uids = rf"{base_path_final_graph}\['sub_node_uids'\]\[\d+\]"
            regex_uid = rf"{base_path_final_graph}\['uid'\]"

            regex_mutations_changed_fields = (
                r"node_uid|parent_node_uid|from_node_uid|new_node_uid|to_node_uid|source_node_uid"
            )
            regex_mutations = rf"{base_path_mutations}\['(?:{regex_mutations_changed_fields})'\]"
            regex_new_node_uid = rf"{base_path_mutations}\['new_node'\]\['uid'\]"
            regex_root_node_uid = r"root\['final_root'\]\['flow'\]\['uid'\]"
            regex_final_root_flow_node_uid = r"root\['root_node_uid'\]"

            # compare the dictionaries
            dict_diff = DeepDiff(
                non_async_post_response_dict,
                async_get_response_dict,
                exclude_paths={
                    "namespace_uid",
                    "request_id",
                },
                exclude_regex_paths={
                    regex_sub_node_uids,
                    regex_uid,
                    regex_mutations,
                    regex_new_node_uid,
                    regex_final_root_flow_node_uid,  # FIXME: if we don't define the (x/u)id of the root-flow in the
                    # input, it will be generated by the system and will be different in each request. Probably we want
                    # to tie together the id of the root-activity & the root-flow such that you always get both even if
                    # you only specify one of them. To be discussed.
                    regex_root_node_uid,  # FIXME(?): currently we only store the recipe_uid in the db
                },
                verbose_level=2,
            )
            assert dict_diff == {}
