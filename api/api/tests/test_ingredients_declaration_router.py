import uuid

import pytest
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from core.service.service_provider import ServiceLocator

logger = get_logger()
service_locator = ServiceLocator()


@pytest.mark.asyncio
async def test_post_put_get_delete_by_uid_ingredients_declaration_mapping(app, create_users):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users

        # POST a new declaration

        # this one has dots instead of commas, mismatched parentheses and an extra ingredient
        faulty_declaration = (
            "Hähnchenbrustfilet. (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %). Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Stärke, Maltodextrin, Milcheiweiß. Pflanzeneiweiß, Dextrose, Zucker, "
            "Gewürzextrakte. Geschmacksverstärker: E 620, UNKNOWN_INGREDIENT"
        )

        fixed_declaration = (
            "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, Zucker, "
            "Geschmacksverstärker: E 620, UNKNOWN_INGREDIENT"
        )

        declaration = {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": fixed_declaration,
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("faulty_declaration") == faulty_declaration
        assert response.json().get("fixed_declaration") == fixed_declaration

        declaration_uid = response.json().get("uid")

        # POST the same declaration and check if the response is idempotent
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": fixed_declaration,
            "uid": declaration_uid,
        }

        # PUT an updated declaration
        updated_fixed_declaration = (
            "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, Zucker, "
            "Geschmacksverstärker: E 620"
        )

        declaration = {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
        }

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # GET this declaration via UUID
        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{declaration_uid}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # GET this declaration via its faulty text
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": faulty_declaration},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # DELETE this declaration via its UUID
        response = await ac.delete(
            f"/v2/ingredients-declaration-mappings/{declaration_uid}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # GET this declaration and see that it's not found by UUID
        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{declaration_uid}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() is not None
        assert response.json() == {"detail": f"Ingredients declaration with UUID '{declaration_uid}' not found."}

        # GET this declaration and see that it's not found by faulty text
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": faulty_declaration},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() is not None
        assert response.json() == {"detail": f"Ingredients declaration with string '{faulty_declaration}' not found."}


@pytest.mark.asyncio
async def test_post_put_get_delete_by_text_ingredients_declaration_mapping(app, create_users):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users

        # POST a new declaration
        # this one has dots instead of commas in some places
        faulty_declaration = (
            "Hähnchenbrustfilet. (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %). Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Gewürzextrakte. Geschmacksverstärker: E 620"
        )

        fixed_declaration = (
            "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze. Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Geschmacksverstärker: E 620"
        )

        declaration = {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": fixed_declaration,
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("faulty_declaration") == faulty_declaration
        assert response.json().get("fixed_declaration") == fixed_declaration

        declaration_uid = response.json().get("uid")

        # POST the same declaration and check if the response is idempotent
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": fixed_declaration,
            "uid": declaration_uid,
        }

        # PUT an updated declaration
        updated_fixed_declaration = (
            "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Geschmacksverstärker: E 620"
        )

        declaration = {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
        }

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # GET this declaration via UUID
        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{declaration_uid}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # GET this declaration via its faulty text
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": faulty_declaration},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == {
            "faulty_declaration": faulty_declaration,
            "fixed_declaration": updated_fixed_declaration,
            "uid": declaration_uid,
        }

        # DELETE this declaration via its faulty text
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/delete",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": faulty_declaration},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # GET this declaration and see that it's not found by UUID
        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{declaration_uid}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() is not None
        assert response.json() == {"detail": f"Ingredients declaration with UUID '{declaration_uid}' not found."}

        # GET this declaration and see that it's not found by faulty text
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": faulty_declaration},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() is not None
        assert response.json() == {"detail": f"Ingredients declaration with string '{faulty_declaration}' not found."}


@pytest.mark.asyncio
async def test_search_and_get_all_ingredients_declaration_mappings(app, create_users):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users

        # check that we don't have any declaration posted at the moment
        response = await ac.get(
            "/v2/ingredients-declaration-mappings/all/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {"declarations": []}

        # post a few new declarations

        faulty_puree_declaration = "Prune puree (water, prunes))."
        fixed_puree_declaration = "Prune puree (water, prunes)."
        puree_declaration = {
            "faulty_declaration": faulty_puree_declaration,
            "fixed_declaration": fixed_puree_declaration,
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("faulty_declaration") == faulty_puree_declaration
        assert response.json().get("fixed_declaration") == fixed_puree_declaration

        puree_declaration_uid = response.json().get("uid")

        faulty_butter_declaration = "Vegetable oils and fats (shea, (coconut*), sunflower), Water, acid"
        fixed_butter_declaration = "Vegetable oils and fats (shea, coconut*, sunflower), water, acid"
        butter_declaration = {
            "faulty_declaration": faulty_butter_declaration,
            "fixed_declaration": fixed_butter_declaration,
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=butter_declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("faulty_declaration") == faulty_butter_declaration
        assert response.json().get("fixed_declaration") == fixed_butter_declaration

        butter_declaration_uid = response.json().get("uid")

        faulty_bananas_declaration = "Bananas, Lemon Juice Concentrate (acid)."
        fixed_bananas_declaration = "Bananas, Lemon Juice Concentrate."
        bananas_declaration = {
            "faulty_declaration": faulty_bananas_declaration,
            "fixed_declaration": fixed_bananas_declaration,
        }

        # test that PUTting a brand-new declaration works as well
        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=bananas_declaration,
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("faulty_declaration") == faulty_bananas_declaration
        assert response.json().get("fixed_declaration") == fixed_bananas_declaration

        bananas_declaration_uid = response.json().get("uid")

        # test that we can get them all
        response = await ac.get(
            "/v2/ingredients-declaration-mappings/all/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "declarations": [
                {
                    **puree_declaration,
                    "uid": puree_declaration_uid,
                },
                {
                    **butter_declaration,
                    "uid": butter_declaration_uid,
                },
                {
                    **bananas_declaration,
                    "uid": bananas_declaration_uid,
                },
            ],
        }

        # test that we can search them
        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": "water"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "declarations": [
                {
                    **puree_declaration,
                    "uid": puree_declaration_uid,
                },
                {
                    **butter_declaration,
                    "uid": butter_declaration_uid,
                },
            ],
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": "prune"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "declarations": [
                {
                    **puree_declaration,
                    "uid": puree_declaration_uid,
                },
            ],
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": "acid"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "declarations": [
                {
                    **butter_declaration,
                    "uid": butter_declaration_uid,
                },
                {
                    **bananas_declaration,
                    "uid": bananas_declaration_uid,
                },
            ],
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"declaration_part": "*"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "declarations": [
                {
                    **butter_declaration,
                    "uid": butter_declaration_uid,
                },
            ],
        }


@pytest.mark.asyncio
async def test_exception_on_post_put_incomplete_ingredients_declaration_mappings(app, create_users):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users

        faulty_puree_declaration = "Prune puree (water, prunes))."
        puree_declaration = {
            "faulty_declaration": faulty_puree_declaration,
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        puree_declaration = {
            "faulty_declaration": faulty_puree_declaration,
            "fixed_declaration": "",
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() is not None

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() is not None

        puree_declaration = {
            "faulty_declaration": "",
            "fixed_declaration": "fixed",
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() is not None

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json=puree_declaration,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() is not None


@pytest.mark.asyncio
async def test_ingredients_declaration_mapping_router_inaccessibility_for_non_superusers(app, create_users):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users

        # POST operations

        declaration = {
            "faulty_declaration": "ABC AS",
            "fixed_declaration": "Abs A",
        }

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # PUT operations

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.put(
            "/v2/ingredients-declaration-mappings/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=declaration,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # GET operations

        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{str(uuid.uuid4())}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.get(
            f"/v2/ingredients-declaration-mappings/{str(uuid.uuid4())}",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/get",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.get(
            "/v2/ingredients-declaration-mappings/all/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.get(
            "/v2/ingredients-declaration-mappings/all/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # search operations

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/search/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # DELETE operations

        response = await ac.delete(
            f"/v2/ingredients-declaration-mappings/{str(uuid.uuid4())}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.delete(
            f"/v2/ingredients-declaration-mappings/{str(uuid.uuid4())}",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/delete",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/ingredients-declaration-mappings/by-text/delete",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"declaration_part": "text"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}
