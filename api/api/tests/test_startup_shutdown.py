import pytest
from httpx import AsyncClient
from structlog import get_logger

from core.service.service_provider import ServiceLocator

logger = get_logger()
service_locator = ServiceLocator()


@pytest.mark.asyncio
async def test_startup_shutdown(app):
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
