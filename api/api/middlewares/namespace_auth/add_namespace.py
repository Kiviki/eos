import base64

from fastapi import Request
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware
from structlog import get_logger

from api.app.settings import Settings
from core.service.service_provider import ServiceLocator

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()


PREFIX = "Basic "


class NamespaceAuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if (
            request.url.path == f"{settings.BASE_PATH}"
            or request.url.path == f"{settings.BASE_PATH}/status"
            or request.url.path == f"{settings.BASE_PATH}/openapi.json"
            or request.url.path.startswith(f"{settings.BASE_PATH}/docs")
            or request.url.path.startswith(f"{settings.BASE_PATH}/redoc")
        ):
            # exclude root status / heartbeat page from authentication filter:
            return await call_next(request)
        else:
            authorization = request.headers.get("authorization")

            if authorization is None:
                return JSONResponse(status_code=401, content={"reason": "missing authorization header"})

            if not authorization.startswith(PREFIX):
                return JSONResponse(
                    status_code=401,
                    content={"reason": "authorization header must start with Basic"},
                )

            token = authorization[len(PREFIX) :]

            if not token:
                return JSONResponse(status_code=401, content={"reason": "authorization token is missed"})

            service_provider = service_locator.service_provider

            if not request.url.path.startswith(f"{settings.BASE_PATH}/batch"):
                try:
                    token = base64.b64decode(token)
                    token = token.decode()
                    token = token.strip("\n").strip(":")
                except base64.binascii.Error:
                    return JSONResponse(
                        status_code=401,
                        content={"reason": "invalid base64 encoding of authorization header"},
                    )
                except UnicodeDecodeError:
                    return JSONResponse(
                        status_code=401,
                        content={"reason": "invalid unicode encoding of authorization header"},
                    )

                user_mgr = service_provider.postgres_db.get_user_mgr()

                (
                    user,
                    default_access_group,
                ) = await user_mgr.find_user_and_default_group_info_by_auth_token(token)

                if user is None:
                    return JSONResponse(status_code=401, content={"reason": "invalid token"})

                # TODO: rename this field to `user_default_access_group` or `default_access_group`?
                request.state.user_legacy_default_access_group = default_access_group
                request.state.is_superuser = user.is_superuser
                request.state.user = user.user_id
                request.state.token = token

            response = await call_next(request)

        return response
