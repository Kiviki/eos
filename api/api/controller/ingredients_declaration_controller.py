from structlog import get_logger

from api.dto.v2.ingredients_declaration_dto import (
    IncomingIngredientsDeclarationMappingDto,
    IngredientsDeclarationMappingDto,
    IngredientsDeclarationMappingsListingDto,
)
from core.service.ingredients_declaration_service import IngredientsDeclarationMappingService

logger = get_logger()


class IngredientsDeclarationMappingController:
    def __init__(self, ingredients_declaration_mapping_service: IngredientsDeclarationMappingService):
        self.ingredients_declaration_mapping_service = ingredients_declaration_mapping_service

    async def get_ingredients_declaration_mapping_by_uid(
        self,
        declaration_uid: str,
    ) -> IngredientsDeclarationMappingDto | None:
        declaration = await self.ingredients_declaration_mapping_service.get_ingredients_declaration_mapping_by_uid(
            declaration_uid
        )

        if declaration:
            return IngredientsDeclarationMappingDto(
                faulty_declaration=declaration.faulty_declaration,
                fixed_declaration=declaration.fixed_declaration,
                uid=declaration.uid,
            )
        else:
            return None

    async def get_ingredients_declaration_mapping_by_text(
        self,
        declaration: str,
    ) -> IngredientsDeclarationMappingDto | None:
        declaration = await self.ingredients_declaration_mapping_service.get_ingredients_declaration_mapping_by_text(
            declaration
        )

        if declaration:
            return IngredientsDeclarationMappingDto(
                faulty_declaration=declaration.faulty_declaration,
                fixed_declaration=declaration.fixed_declaration,
                uid=declaration.uid,
            )
        else:
            return None

    async def search_ingredients_declaration_mappings_by_text(
        self,
        declaration_part: str,
    ) -> IngredientsDeclarationMappingsListingDto:
        declarations = (
            await self.ingredients_declaration_mapping_service.search_ingredients_declaration_mappings_by_text(
                declaration_part
            )
        )

        return IngredientsDeclarationMappingsListingDto(
            declarations=[
                IngredientsDeclarationMappingDto(
                    faulty_declaration=declaration.faulty_declaration,
                    fixed_declaration=declaration.fixed_declaration,
                    uid=declaration.uid,
                )
                for declaration in declarations
            ],
        )

    async def get_all_ingredients_declaration_mappings(self) -> IngredientsDeclarationMappingsListingDto:
        declarations = await self.ingredients_declaration_mapping_service.get_all_ingredient_declaration_mappings()

        return IngredientsDeclarationMappingsListingDto(
            declarations=[
                IngredientsDeclarationMappingDto(
                    faulty_declaration=declaration.faulty_declaration,
                    fixed_declaration=declaration.fixed_declaration,
                    uid=declaration.uid,
                )
                for declaration in declarations
            ],
        )

    async def upsert_ingredients_declaration_mapping(
        self,
        incoming_declaration_dto: IncomingIngredientsDeclarationMappingDto,
    ) -> IngredientsDeclarationMappingDto:
        updated_declaration = await self.ingredients_declaration_mapping_service.upsert_ingredients_declaration_mapping(
            faulty_declaration=incoming_declaration_dto.faulty_declaration,
            fixed_declaration=incoming_declaration_dto.fixed_declaration,
        )

        return IngredientsDeclarationMappingDto(
            faulty_declaration=updated_declaration.faulty_declaration,
            fixed_declaration=updated_declaration.fixed_declaration,
            uid=updated_declaration.uid,
        )

    async def delete_ingredients_declaration_mapping_by_uid(self, declaration_uid: str) -> bool:
        return await self.ingredients_declaration_mapping_service.delete_ingredients_declaration_mapping_by_uid(
            declaration_uid
        )

    async def delete_ingredients_declaration_mapping_by_text(self, declaration: str) -> bool:
        return await self.ingredients_declaration_mapping_service.delete_ingredients_declaration_mapping_by_text(
            declaration
        )
