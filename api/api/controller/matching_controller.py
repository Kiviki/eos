from typing import Optional
from uuid import UUID

from gap_filling_modules.match_product_name_gfm import MATCH_PRODUCT_GFM_NAME
from structlog import get_logger

from api.dto.v2.glossary_dto import GetAllMatchingTermsDto, TermDto
from api.dto.v2.matching_dto import MatchingItemDto
from core.domain.matching_item import MatchingItem
from core.domain.term import Term
from core.service.glossary_service import GlossaryService
from core.service.matching_service import MatchingService

logger = get_logger()


class MatchingController:
    def __init__(self, matching_service: MatchingService, glossary_service: GlossaryService):
        self.matching_service = matching_service
        self.glossary_service = glossary_service

    async def get_terms_by_matching_string(
        self, matching_string: str, gfm_name: str = MATCH_PRODUCT_GFM_NAME, filter_lang: Optional[str] = None
    ) -> GetAllMatchingTermsDto:
        """Returns a list of list of terms that match the given *lowercased* matching string."""
        terms: [[Term]] = await self.matching_service.get_terms_by_matching_string(
            matching_string, gfm_name, filter_lang=filter_lang
        )
        terms_dto: [[TermDto]] = []
        for term_list in terms:
            terms_dto.append(
                [
                    TermDto(
                        uid=str(term.uid),
                        xid=term.xid,
                        data=term.data,
                        name=term.name,
                        sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
                        access_group_uid=str(term.access_group_uid),
                    )
                    for term in term_list
                ]
            )
        return GetAllMatchingTermsDto(terms=terms_dto)

    async def put_matching_string(
        self,
        matching_item: MatchingItemDto,
    ) -> MatchingItemDto:
        """Creates a new matching item. Note that the matching string is lowercased."""

        # check for existence of terms
        for term_uid in matching_item.term_uids:
            try:
                self.glossary_service.get_term_by_id(UUID(term_uid))
            except KeyError as ke:
                raise KeyError(f"Term with uid {term_uid} not found") from ke

        matching_item = MatchingItem(
            gap_filling_module=matching_item.gap_filling_module,
            matching_string=matching_item.matching_string,
            term_uids=matching_item.term_uids,
            lang=matching_item.lang,
        )
        matching_item = await self.matching_service.put_matching_item(matching_item)

        return MatchingItemDto(
            gap_filling_module=matching_item.gap_filling_module,
            matching_string=matching_item.matching_string,
            term_uids=matching_item.term_uids,
            lang=matching_item.lang,
        )
