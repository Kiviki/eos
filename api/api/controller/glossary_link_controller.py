import uuid

from structlog import get_logger

from api.dto.v2.glossary_dto import GetAllGlossaryLinksDto, GlossaryLinkDto
from core.domain.glossary_link import GlossaryLink
from core.service.glossary_link_service import GlossaryLinkService

logger = get_logger()


class GlossaryLinkController:
    def __init__(self, glossary_link_service: GlossaryLinkService):
        self._glossary_link_service = glossary_link_service

    async def put_glossary_link(self, glossary_link_dto: GlossaryLinkDto) -> GlossaryLinkDto:
        glossary_link = await self._glossary_link_service.insert_glossary_link(
            GlossaryLink(
                gap_filling_module=glossary_link_dto.gap_filling_module,
                term_uids=[uuid.UUID(uid) for uid in glossary_link_dto.term_uids],
                linked_term_uid=uuid.UUID(glossary_link_dto.linked_term_uid)
                if glossary_link_dto.linked_term_uid
                else None,
                linked_node_uid=uuid.UUID(glossary_link_dto.linked_node_uid)
                if glossary_link_dto.linked_node_uid
                else None,
            )
        )
        glossary_link_dto.uid = str(glossary_link.uid)
        return glossary_link_dto

    async def get_glossary_links_by_gfm(self, gap_filling_module: str) -> GetAllGlossaryLinksDto:
        glossary_links = await self._glossary_link_service.get_glossary_links_by_gfm(gap_filling_module)
        return GetAllGlossaryLinksDto(
            glossary_links=[
                GlossaryLinkDto(
                    uid=str(gl.uid),
                    gap_filling_module=gl.gap_filling_module,
                    term_uids=[str(uid) for uid in gl.term_uids],
                    linked_term_uid=str(gl.linked_term_uid) if gl.linked_term_uid else None,
                    linked_node_uid=str(gl.linked_node_uid) if gl.linked_node_uid else None,
                )
                for gl in glossary_links
            ]
        )

    async def delete_glossary_links_by_uids(self, uids: list[str]):
        await self._glossary_link_service.delete_glossary_links_by_uids(uids)
