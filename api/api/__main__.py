import argparse
from os import path

import uvicorn
from structlog import get_logger

logger = get_logger()

base_path = path.dirname(__file__)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--reload", action="store_true")
    args = parser.parse_args()

    PORT = 8040
    logger.info("starting server on port {}".format(PORT), flush=True)

    uvicorn.run(
        app="api.app.server:fastapi_app",
        host="0.0.0.0",
        port=PORT,
        reload=args.reload,
        reload_dirs=[
            path.abspath(path.join(base_path, "..", "..", "api")),
            path.abspath(path.join(base_path, "..", "..", "core")),
            path.abspath(path.join(base_path, "..", "..", "database")),
        ],
        reload_excludes=[
            path.abspath(path.join(base_path, "..", "..", "scripts")),
            path.abspath(path.join(base_path, "..", "..", "legacy_api")),
        ],
        workers=1,
        log_level="debug",
        access_log=False,
        # We set this so that uvicorn does not try to configure logging. we are doing that instead
        log_config=None,
    )
