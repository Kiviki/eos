# Eaternity EOS

This project embodies Eaternity's vision of "Accelerating the World's Transition to a Sustainable Food System" by establishing the necessary infrastructure to calculate and publish the major environmental impacts of all foods. We have named this initiative "All You Can Eat" or AYCE, which represents a software platform serving as an open-source environmental operating system (EOS) to achieve our goals. The projects website is developed here: [http://ayce.earth](http://ayce.earth).

#### Our strategy revolves around leveraging our core strengths, which include:

- Expertise in life cycle assessment, enabling us to consistently perform comprehensive and accurate impact calculations for individual food products.
- Proficiency in building software architectures, as demonstrated by our successful development of our CO2 calculation tool for numerous restaurants. Over the past decade, we have accumulated invaluable knowledge and experience in this field.
- Utilization of recurring revenue streams to finance this operation. Having established a break-even business model, we have the freedom to allocate our resources towards research and development in the years ahead.

#### To ensure the effectiveness of our platform, we place significant emphasis on data and software architecture, specifically focusing on:

- Computational efficiency, allowing for scalable, dynamic, and instantaneous impact recalculations through the utilization of matrix calculus and cloud architecture.
- Rapid development capabilities, achieved by providing a structured framework and generic components that facilitate the addition of complexity without unnecessarily complicating the system. One approach involves implementing independent gap-filling modules that can be executed and scheduled separately, relying on a glossary for dependencies rather than direct data connections.
- Transparency and accessibility, enabling non-IT individuals to easily review the calculation process. This is achieved by ensuring transparent, deterministic graph mutations that can be comprehended by a wider audience.

We document our progress and decisions on Notion, which you can find here: [http://eaternity.notion.site](https://www.notion.so/eaternity/Project-EOS-AYCE-kale-a8f5d9809b65416aa159adf83f0f459b) (not yet public).

**We actively seek collaborations with scientists and food retailers to enhance our models and share our results with the public. Please feel free to reach out to us if you are interested in collaborating.**

## Project Setup

You need to have docker installed on your system (preferably in rootless mode).

First, create a folder `secrets` in the root directory of the project and add the `service_account.json` file for the 
Google Cloud service account.

Then copy the `.env.sample` file to `.env` and adjust the environment variables to your needs:
```bash
cp .env.sample .env
```

There are two options how you can run EOS:

## Option 1: Python on host system

This option is most suitable for development, because it allows you to easily attach debugging tools while you develop
gap-filling-modules.
If you want to contribute to the project by developing gap-filling modules,
we suggest to install PyCharm Community Edition.

### Requirements:

Please make sure to first install:

- docker >= 24.0.4
- python == 3.11.3
- poetry >= 1.4.2

### Linux x86_64

If you have multiple python versions installed, it might be necessary to first tell poetry to use the correct python
version by executing in the `eos` directory:

```bash
poetry env use /usr/local/bin/python3.11
```

If you use pyenv, first install the python version and then create a virtualenv to use in poetry:
```bash
pyenv install -v 3.11.3
pyenv virtualenv 3.11.3 eos_env
pyenv activate eos_env
pyenv virtualenv-prefix eos_env
poetry env use $(pyenv virtualenv-prefix eos_env)/bin/python3.11
```

To install the python dependencies of our project use poetry inside the project root folder:

```bash
poetry install
```

Note, that this will install all dependencies from the committed `poetry.lock` file in the root folder, which contains
also all the dependencies that are specified in the dependent poetry projects of sub-folders. Therefore, it
is **not** necessary to run `poetry install` inside the sub-folders (except you want to specifically update the version
of a sub-sub-dependency in one of the sub-folders).

### macOS aarch64

On linux x86_64, we use `pypardiso` sparse solver that depends on Intel openmp, tbb and mkl, none of which are available on aarch64, e.g. Apple Silicon M1.
On macOS aarch64 Docker Desktop will run the images as Linux on aarch64, making the pypardiso solver replacement necessary within the docker images too.
As a fallback, we use `scikit-umfpack` on aarch64.
Therefore, on aarch64 systems, it is necessary to install `SuiteSparse` and `swig`.
Further the python `tables` module is broken on aarch64, to build from source we need hdf5 dependencies.
Follow those steps on an aarch64 machine:

```bash
# make sure to install poetry for python 3.11
curl -sSL https://install.python-poetry.org | python3.11 -
# or with a homebrew install poetry: setup python environment
poetry env use $(which python3.11)

# install dependencies
brew install suite-sparse swig
brew install c-blosc hdf5 lzo
# export needed environment for building tables
export HDF5_DIR=/opt/homebrew/opt/hdf5
export BLOSC_DIR=/opt/homebrew/opt/c-blosc
export LZO_DIR=/opt/homebrew/opt/lzo
# install the dependencies
poetry install
# if the above fails you can try to
# rebuild all poetry.lock files on your aarch64 machine, the included one's are for x86_64
# this runs poetry install for you:
scripts/update_deps.sh
```

#### all OS and archictecture independent

Copy the provided sample env file and start the database service and seed the database with our schema:

```bash
docker compose up -d postgres rabbitmq
poetry run python -m database --reinit
poetry run python -m inventory_importer.bw_import_controller --edb_path /app/temp_data/brightway --download_from_gdrive 1QGgJPHuz-m7MA5zIkerN1b2EsId8dnIu --import_using_airtable  --import_all_impact_assessments --import_glossary --import_matchings_from_airtable --gfm_import_data
```

Now you can run the python services on the host system:

```bash
poetry run python -m api
poetry run python -m legacy_api
```

PyCharm debug/run configurations for above python services are stored in `.idea/runConfigurations/` and can
easily be used to reset the database to a clean state and to start the api services in the PyCharm Debugger.

For these debug configurations to work, you probably need to select a python interpreter in the PyCharm project,
in which case you should choose the poetry environment that was created by the above `poetry install`.
By default, poetry created a new environment for eos in a path like this (which should be selected as interpreter in
PyCharm):

    $HOME/.cache/pypoetry/virtualenvs/eaternity-eos-Jmd7uH6u-py3.11/bin/python

## Option 2: Everything in Docker

Alternatively, you can start all services in docker without having python installed on your system:
First build the image and seed the database:

```bash
docker compose build _image_build
docker compose up -d postgres rabbitmq
docker compose --profile initdb run --rm initdb
docker compose --profile inventory_importer run --rm inventory_importer
docker compose --profile inventory_importer_gfm_import_data run --rm inventory_importer_gfm_import_data
```

Now you can start the dockerized python services:

```bash
docker compose --profile api up -d
```

# Frontend development

We recommend to use VSCode with Volar extension.
To set it up, make sure you have node v16 (stable) installed (You might want to use `nvm` to manage node versions).
Then install dependencies and run the development server with hot-module-reloading using:

```bash
cd eos-ui
npm ci
npm run dev
```

## What to try out

After you followed option 1 or 2 above, you should be able to access the swagger ui and do example requests:

- new API at [http://localhost:8040/docs](http://localhost:8040/v2/docs)
- legacy API at [http://localhost:8050/docs](http://localhost:8050/api/docs)
- eos-ui at [http://localhost:5173](http://localhost:5173)

Please, also checkout the example requests in the `/scripts` folder to get started.

## Troubleshooting

We use multiprocessing in unittests of the `legacy_api` package to run the new api service in a separate process.
In order to be able to debug the multiprocessing code with PyCharm, you need to enable in the PyCharm settings
under `Python Debugger` the option `Gevent compatible`.

When adding a new Poetry dependency to sub-packages (for example, `core` or `api`), it is necessary to update
root folder dependencies file as well by running `poetry update *sub-package*` command in root folder.

## To check for updates of dependencies:

To show currently installed versions and available updated versions:

```bash
poetry show -l
```
