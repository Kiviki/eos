FROM python:3.11.3

LABEL maintainer="dev@eaternity.ch"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl \
    build-essential autoconf libtool pkg-config cmake \
    libhdf5-dev libgdal-dev libsuitesparse-dev swig \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /app
RUN chmod 777 /app

RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
USER user

ENV HOME=/home/user
RUN chmod 777 /home/user

ENV PATH=/home/user/.local/bin:$PATH

RUN python3 --version

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.4.2

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install --upgrade setuptools && \
    python3 -m pip install "poetry==$POETRY_VERSION"

WORKDIR /app

# to improve docker caching layers, we first only copy the pyproject.toml and poetry.lock files to install dependencies:
COPY --chown=user:user ./pyproject.toml ./poetry.lock /app/
COPY --chown=user:user ./api/pyproject.toml ./api/poetry.lock /app/api/
COPY --chown=user:user ./core/pyproject.toml ./core/poetry.lock /app/core/
COPY --chown=user:user ./database/pyproject.toml ./database/poetry.lock /app/database/
COPY --chown=user:user ./legacy_api/pyproject.toml ./legacy_api/poetry.lock /app/legacy_api/
COPY --chown=user:user ./inventory_importer/pyproject.toml ./inventory_importer/poetry.lock /app/inventory_importer/

# we need to copy empty __init__ files, because otherwise poetry install fails with an error:
COPY --chown=user:user ./api/api/__init__.py /app/api/api/
COPY --chown=user:user ./core/core/__init__.py /app/core/core/
COPY --chown=user:user ./database/database/__init__.py /app/database/database/
COPY --chown=user:user ./legacy_api/legacy_api/__init__.py /app/legacy_api/legacy_api/
COPY --chown=user:user ./inventory_importer/inventory_importer/__init__.py /app/inventory_importer/inventory_importer/

# Disabling parallel installs to prevent OOM in low memory ci runners:
ARG POETRY_PARALLEL=false
RUN poetry config installer.parallel $POETRY_PARALLEL

# Install dependencies:
RUN poetry install --no-interaction --no-ansi
RUN if [ $(arch) = "aarch64" ]; then poetry run pip install scikit-umfpack; fi;

# Now we copy all the source tree:
COPY --chown=user:user . /app

RUN chmod +x /app/start.sh
RUN chmod +x /app/start-reload.sh
