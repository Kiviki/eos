import {createRouter, createWebHashHistory} from 'vue-router'
import Calculation from '../pages/Calculation.vue'
import Glossary from '../pages/Glossary.vue'
import About from '../pages/About.vue'

const routes = [
    {
        path: '/',
        name: 'Glossary',
        component: Glossary
    },
    {
        path: '/glossary',
        name: 'Glossary',
        component: Glossary
    },
    {
        path: '/calculation',
        name: 'Calculation',
        component: Calculation
    },
    {
        path: '/about',
        name: 'About',
        component: About
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router
