"Profiling graph performance."
import argparse
import asyncio
import os

import structlog
from profiling.custom_profiling_setups import (
    ProfilingBananaJam,
    ProfilingBWCalculations,
    ProfilingEosSubBWCalculationsWithTwoBWNodes,
)

OUTPUT_BASE_FOLDER = "temp_data"

logger = structlog.get_logger()


async def main(
    calculation_choice: str,
    profiling: bool,
    logging: bool,
    use_system_processes: bool = False,
    save_as_system_process: bool = False,
    delete_system_process: bool = False,
) -> None:
    """Launch profiling calculation based on user choices.

    :param calculation_choice: The choice of the profiling calculation.
    :param profiling: Whether to enable profiling.
    :param logging: Whether to enable logging.
    :param use_system_processes: Whether to use system processes.
    :param save_as_system_process: Whether to save the aggregated cache in the database at the end of the calculation.
    :param delete_system_process: Whether to delete the aggregated cache of all nodes in the calc_graph at the end of the calculation.
    """
    out_dir = None
    if profiling:
        use_system_processes_str = "use_system_processes"
        if not use_system_processes:
            use_system_processes_str = f"no_{use_system_processes_str}"
        out_dir = os.path.join(OUTPUT_BASE_FOLDER, f"{calculation_choice}_{use_system_processes_str}")
    if calculation_choice == "recipe_with_EDB_oat_flour":
        edb_xid = "EDB_0b257d6d527b454c8cdb1e2e3db24e7b"
        expected_co2 = 0.8013926140795705
        profiling_eos = ProfilingBWCalculations(
            out_dir=out_dir,
            edb_xid=edb_xid,
            use_system_processes=use_system_processes,
            save_as_system_process=save_as_system_process,
            delete_system_process=delete_system_process,
            profiling=profiling,
            logging=logging,
            expected_co2=expected_co2,
        )
        await profiling_eos.run()
    elif calculation_choice == "recipe_with_EDB_oat_flour_and_sub_bw_nodes":
        edb_xid = "EDB_0b257d6d527b454c8cdb1e2e3db24e7b"
        expected_co2 = 1.219022916785261
        profiling_eos = ProfilingEosSubBWCalculationsWithTwoBWNodes(
            out_dir=out_dir,
            edb_xid=edb_xid,
            use_system_processes=use_system_processes,
            save_as_system_process=save_as_system_process,
            delete_system_process=delete_system_process,
            profiling=profiling,
            logging=logging,
            expected_co2=expected_co2,
        )
        await profiling_eos.run()
    elif calculation_choice == "banana_jam":
        expected_co2 = 0.13879376333529225
        expected_dfu = 0.035627972573038715
        profiling_eos = ProfilingBananaJam(
            out_dir=out_dir,
            use_system_processes=use_system_processes,
            save_as_system_process=save_as_system_process,
            delete_system_process=delete_system_process,
            profiling=profiling,
            logging=logging,
            expected_co2=expected_co2,
            expected_dfu=expected_dfu,
        )
        await profiling_eos.run()
    else:
        raise ValueError(f"calculation_choice {calculation_choice} not recognized")


if __name__ == "__main__":
    # Initialize parser
    parser = argparse.ArgumentParser()

    # Adding command line arguments
    parser.add_argument(
        "-calc",
        "--calculation_choice",
        help="Determines which calculation to run",
        choices=["recipe_with_EDB_oat_flour", "banana_jam", "recipe_with_EDB_oat_flour_and_sub_bw_nodes"],
        default="recipe_with_EDB_oat_flour",
    )

    parser.add_argument(
        "-prof",
        "--profiling",
        help="Profile the calculation",
        action=argparse.BooleanOptionalAction,
    )

    parser.add_argument(
        "-log",
        "--logging",
        help="Print the log of the calculation (recommended to turn off for profiling, decreases the performance)",
        action=argparse.BooleanOptionalAction,
    )

    parser.add_argument(
        "-use_system_processes",
        "--use_system_processes",
        help="Whether to use system processes instead of only unit processes",
        action=argparse.BooleanOptionalAction,
    )

    parser.add_argument(
        "-save_as_system_process",
        "--save_as_system_process",
        help="Whether to save the environmental flows at the end of the calculation",
        action=argparse.BooleanOptionalAction,
    )

    parser.add_argument(
        "-delete_system_process",
        "--delete_system_process",
        help="Whether to delete the aggregated environmental flows at the end of the calculation",
        action=argparse.BooleanOptionalAction,
    )

    # Read arguments from command line
    args = parser.parse_args()

    # run the profiler
    asyncio.run(
        main(
            args.calculation_choice,
            args.profiling,
            args.logging,
            args.use_system_processes,
            args.save_as_system_process,
            args.delete_system_process,
        )
    )
