if [ ! -f .env ]
then
  while IFS= read -r line; do
    export "$line"
  done < <(cat ../.env)
else
  while IFS= read -r line; do
    export "$line"
  done < <(cat .env)
fi

GDRIVE_SERVICE_ACCOUNT_FILE="../secrets/service_account.json"

# poetry run python -m profiling.profiling --calculation_choice="banana_jam" --no-logging --no-profiling --use_system_processes --no-save_as_system_process --no-delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour" --no-logging --no-profiling --no-use_system_processes --save_as_system_process --delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour" --no-logging --no-profiling --use_system_processes --save_as_system_process --no-delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour" --no-logging --no-profiling --use_system_processes --no-save_as_system_process --no-delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour_and_sub_bw_nodes" --no-logging --no-profiling --no-use_system_processes --save_as_system_process --delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour_and_sub_bw_nodes" --no-logging --no-profiling --use_system_processes --save_as_system_process --no-delete_system_process

# poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour_and_sub_bw_nodes" --no-logging --no-profiling --use_system_processes --no-save_as_system_process --no-delete_system_process

poetry run python -m profiling.profiling --calculation_choice="recipe_with_EDB_oat_flour_and_sub_bw_nodes" --no-logging --profiling --use_system_processes --no-save_as_system_process --no-delete_system_process
