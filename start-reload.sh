#! /usr/bin/env sh
set -e

export APP_MODULE=api.app.server:fastapi_app

HOST=${HOST:-0.0.0.0}
PORT=${PORT:-8040}
LOG_LEVEL=${LOG_LEVEL:-info}

# Start Uvicorn with live reload
exec poetry run uvicorn --reload --host $HOST --port $PORT --log-level $LOG_LEVEL "$APP_MODULE"