import uuid

from structlog import get_logger

from core.domain.edge import Edge
from core.domain.nodes.node import Node
from core.service.service_provider import ServiceLocator

logger = get_logger()


class BatchImporter:
    """Imports EDB's products into our database, in batches (much faster).
    Because of db table edge's REFERENCES, we always insert nodes first, then edges."""

    def __init__(self):
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider
        self.nodes_to_insert: list[Node] = []
        self.edges_to_insert: list[Edge] = []
        self.namespace_uid_xid_uid_to_insert: list[(str, str, uuid.UUID)] = []

    def insert_node(self, node: Node):
        self.nodes_to_insert.append(node)

    def insert_namespace_xid_mappings(self, namespace_uid: str, xid: str, uid: uuid.UUID):
        self.namespace_uid_xid_uid_to_insert.append((namespace_uid, xid, uid))

    def insert_edge(self, edge: Edge):
        self.edges_to_insert.append(edge)

    def clear(self):
        self.nodes_to_insert = []
        self.edges_to_insert = []
        self.namespace_uid_xid_uid_to_insert = []

    async def flush(self):
        logger.debug(f"flushing {len(self.nodes_to_insert)} nodes and {len(self.edges_to_insert)} edges to database")
        # Because of db table edge's REFERENCES, we always insert nodes first, then edges.
        if len(self.nodes_to_insert):
            await self.service_provider.postgres_db.graph_mgr.add_nodes(self.nodes_to_insert)
            for node in self.nodes_to_insert:
                self.service_provider.node_service.cache.add_node(node)
            self.nodes_to_insert = []
        if len(self.edges_to_insert):
            await self.service_provider.postgres_db.graph_mgr.add_edges(self.edges_to_insert)
            for edge in self.edges_to_insert:
                self.service_provider.node_service.cache.add_edge(edge)
            self.edges_to_insert = []
        if len(self.namespace_uid_xid_uid_to_insert):
            await self.service_provider.postgres_db.product_mgr.bulk_insert_xid_uid_mappings(
                self.namespace_uid_xid_uid_to_insert
            )
            self.namespace_uid_xid_uid_to_insert = []
