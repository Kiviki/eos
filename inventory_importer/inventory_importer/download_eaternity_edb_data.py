from __future__ import print_function

import os
import subprocess

from structlog import get_logger

from inventory_importer.settings import Settings

logger = get_logger()


def download_from_github(edb_path: str):
    """download the eaternity edb data"""
    os.makedirs(edb_path, exist_ok=True)

    settings = Settings()

    repository_url = f"https://x-access-token:{settings.GITHUB_TOKEN}@github.com/Eaternity/eaternity-edb-data.git"

    eaternity_edb_data_dir = os.path.join(edb_path, "eaternity-edb-data")
    if os.path.exists(eaternity_edb_data_dir) and os.path.isdir(eaternity_edb_data_dir):
        try:
            subprocess.check_output(["git", "-C", eaternity_edb_data_dir, "pull"])
            logger.info("Git pull eaternity-edb-data operation successful.")
        except subprocess.CalledProcessError as e:
            logger.error(f"Error during git pull of eaternity-edb-data: {e}")
    else:
        try:
            # Clone the Git repository
            subprocess.check_output(["git", "clone", repository_url, os.path.join(edb_path, "eaternity-edb-data")])
            logger.info("Git clone eaternity-edb-data successful.")
        except subprocess.CalledProcessError as e:
            logger.error(f"Error during git clone of eaternity-edb-data: {e}")
