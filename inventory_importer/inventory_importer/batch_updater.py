import uuid

from structlog import get_logger

from core.service.service_provider import ServiceLocator

logger = get_logger()


class BatchUpdater:
    """
    Deletes and updates EDB's products into our database, in batches (much faster).
    """

    def __init__(self):
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider
        self.nodes_to_delete: list[uuid.UUID] = []
        self.namespace_uid_xid_uid_to_delete: list[(str, str, uuid.UUID)] = []
        self.nodes_to_update: set[uuid.UUID] = set()  # Update node and the associated edges.
        self.namespace_uid_xid_uid_to_update: list[(str, str, uuid.UUID)] = []

    def insert_node_to_delete(self, uid: uuid.UUID):
        self.nodes_to_delete.append(uid)

    def insert_node_to_update(self, uid: uuid.UUID):
        self.nodes_to_update.add(uid)

    def insert_namespace_xid_mappings_to_delete(self, namespace_uid: str, xid: str, uid: uuid.UUID):
        self.namespace_uid_xid_uid_to_delete.append((namespace_uid, xid, uid))

    def clear(self):
        self.nodes_to_delete = []
        self.namespace_uid_xid_uid_to_delete = []
        self.nodes_to_update = []
        self.namespace_uid_xid_uid_to_update = []

    async def flush(self):
        logger.debug(f"deleting {len(self.nodes_to_delete)} nodes from database")
        if len(self.namespace_uid_xid_uid_to_delete):
            await self.service_provider.postgres_db.product_mgr.bulk_delete_xid_uid_mappings(
                self.namespace_uid_xid_uid_to_delete
            )
        if len(self.namespace_uid_xid_uid_to_update):
            await self.service_provider.postgres_db.product_mgr.bulk_insert_xid_uid_mappings(
                self.namespace_uid_xid_uid_to_update
            )
        if len(self.nodes_to_update):
            await self.service_provider.node_service.update_many_nodes_and_edges_from_local_cache(
                list(self.nodes_to_update)
            )
        if len(self.nodes_to_delete):
            await self.service_provider.node_service.delete_many_nodes_and_edges(self.nodes_to_delete)
        self.clear()
