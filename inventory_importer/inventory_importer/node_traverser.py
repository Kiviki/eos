from .node_id_mapper import BwKey, NodeIdMapper


class NodeTraverser:
    def __init__(self):
        # store traversed nodes.
        self.traversed_xid: set[str] = set()

        # staged storage for traversed nodes.
        self.traversed_xid_staged: set[str] = set()

    def reset_staged_items(self):
        self.traversed_xid_staged = set()

    def reset_all(self):
        self.traversed_xid = set()
        self.traversed_xid_staged = set()

    def commit_staged_items(self):
        self.traversed_xid.update(self.traversed_xid_staged)
        self.reset_staged_items()

    def add(self, bw_key: BwKey):
        xid = NodeIdMapper.bw_key_to_xid(bw_key)
        self.traversed_xid_staged.add(xid)

    def __contains__(self, item: BwKey):
        xid = NodeIdMapper.bw_key_to_xid(item)
        return (xid in self.traversed_xid) or (xid in self.traversed_xid_staged)

    def __len__(self):
        return len(self.traversed_xid) + len(self.traversed_xid_staged)
