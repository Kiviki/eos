"""Brightway importer.

NOTE: Only EDB/ZHAW nodes are upserted (i.e., ecoinvent cutoff and biosphere nodes are not).
"""

import argparse
import asyncio
import datetime
import logging
import re
import time
import uuid
from ast import literal_eval
from typing import Any, Callable, Tuple, Type

import pandas as pd
from bw2data import Database, Method
from bw2data.errors import UnknownObject
from gap_filling_modules.processing_gfm import ProcessingGapFillingFactory, ProcessingSettings
from pyairtable import Api
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.glossary_link import GlossaryLink
from core.domain.namespace import Namespace
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.term import Term
from core.domain.util import get_system_process_data
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceLocator
from database.postgres.settings import PgSettings
from inventory_importer.airtable_matching_importer import MatchingLinkingImporter
from inventory_importer.batch_importer import BatchImporter
from inventory_importer.batch_updater import BatchUpdater
from inventory_importer.bw_database_wrapper import BwDatabaseWrapper
from inventory_importer.node_id_mapper import BwKey, NodeIdMapper
from inventory_importer.node_traverser import NodeTraverser
from inventory_importer.settings import Settings
from inventory_importer.upsert_helper import UpsertHelper

logger = get_logger()

MAXIMUM_DEPTH = 5000
""" Use a small depth if you are debugging (e.g. 20), to avoid getting too many nodes and edges
otherwise for 'production', use a larger depth (e.g. 5000) to ensure that all the nodes are imported. """


class BwImporter:
    def __init__(self):
        logger.info("opening brightway files...")
        self.bw_databases = BwDatabaseWrapper()

        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider
        self.semaphore = asyncio.Semaphore(value=3)

        self.settings = Settings()
        self.namespace_uid = self.settings.EATERNITY_NAMESPACE_UUID
        self.gen_uids_locally = True

        # to write to postgres in batches:
        self.batch_importer = BatchImporter()
        self.batch_updater = BatchUpdater()
        self.node_id_mapper = NodeIdMapper()
        self.node_traverser = NodeTraverser()
        self.upsert_helper = UpsertHelper()

    async def import_bw_products_and_impact_assessments(
        self, args: argparse.Namespace, schema: str = "public"
    ) -> Tuple[int, int, int]:
        if len(args.import_impact_assessments_by_name) > 0 or args.import_all_impact_assessments:
            impact_assessment_import_cnt = await self.import_impact_assessments(args)
        else:
            impact_assessment_import_cnt = 0

        if (
            len(args.import_by_name) > 0
            or args.import_edb_database
            or args.import_using_glossary
            or args.import_using_airtable
        ):
            if schema != "test_pg":
                self.service_provider.clear_cache()
                await self.service_provider.init_caches()
            bw_product_import_cnt, bw_product_update_cnt = await self.import_bw_products(args)
        else:
            bw_product_import_cnt = 0
            bw_product_update_cnt = 0

        return bw_product_import_cnt, bw_product_update_cnt, impact_assessment_import_cnt

    @staticmethod
    async def _import_by_name(args: argparse.Namespace, import_function: Callable, msg: str) -> None:
        edb_db = Database("EDB")
        for p in args.import_by_name:
            hits = edb_db.search(p)
            if len(hits) > 0:
                # for now, exact match only
                for hit in hits:
                    if hit["name"] == p:
                        logger.info(f'{msg} activity "{p}" with key {hit.key}')
                        await import_function(hit.key)
            else:
                logger.info(f'no activity found for "{p}" in database "EDB"')

    async def _import_using_airtable(self, products_df: pd.DataFrame, import_function: Callable, msg: str) -> None:
        for _, row in products_df.iterrows():
            bw_key_str = row[self.settings.PRODUCTMATCHINGS_COLUMN_NAME][0].replace('"', "")
            if msg.lower() != "updating":
                foodex2_xids = MatchingLinkingImporter.foodex2_terms_string_processing(
                    row[self.settings.FOODEX2_COLUMN_NAME]
                )
            else:
                foodex2_xids = None

            # need to convert bw_key_str into a BwKey:

            # We match two regexp groups from ('BW_DB', 'BW_ID') to get the BW_DB and the BW_ID.
            # For the two match groups, we use ([^\']+) to match any string chars except the next single quote.
            result = re.search(r"^\(\'([^\']+)\', \'([^\']+)\'\)$", bw_key_str)

            if result is None:
                logger.error(f"Could not match a valid bw_key from {bw_key_str}. skipping...")
                continue

            # TODO: handling of multiple BW keys for one set of terms?
            bw_db = result.group(1)
            bw_id = result.group(2)
            bw_key: BwKey = (bw_db, bw_id)

            # check if the product exists in edb database:
            try:
                self.bw_databases.get_activity(bw_key)
            except KeyError:
                logger.error(f"bw_key {bw_key} does not exist in project. skipping...")
                continue

            try:
                await import_function(bw_key)
            except KeyError as e:
                logger.error(f"{msg} failed for bw_key {bw_key} due to missing linked activities: {e}")
                continue

            if foodex2_xids:
                try:
                    term_uids: list[uuid.UUID] = [
                        (await self.service_provider.postgres_db.pg_term_mgr.get_terms_by_xid(xid))[0].uid
                        for xid in foodex2_xids
                    ]
                except IndexError:
                    logger.error(f"Could not find one of terms: {foodex2_xids}")
                    continue

                logger.info(
                    f'associating bw_key "{bw_key}" with foodex2_xids "{foodex2_xids}" ' f"(term_uids: {term_uids})"
                )

                await self.service_provider.postgres_db.get_pg_glossary_link_mgr().delete_glossary_link(
                    term_uids,
                    "LinkTermToActivityNode",
                )

                root_process_node_uid = self.node_id_mapper[bw_key]
                await self.service_provider.postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
                    GlossaryLink(
                        gap_filling_module="LinkTermToActivityNode",
                        term_uids=term_uids,
                        linked_node_uid=root_process_node_uid,
                    )
                )

    async def _import_using_glossary(self, terms_with_edb_bw_ids: list, import_function: Callable, msg: str) -> None:
        for term in terms_with_edb_bw_ids:
            term_uid = term.get("uid")
            term_xid = term.get("xid")
            term_name = term.get("name")
            bw_id = term.get("data").get("edb-bw-id")

            bw_key: BwKey = ("EDB", bw_id)
            logger.info(
                f"{msg} process for term {term_uid} {term_xid} {term_name} with bw_id {bw_id} "
                f"into EOS namespace {self.namespace_uid} with "
                f"xid {self.node_id_mapper.bw_key_to_xid(bw_key)}"
            )
            await import_function(bw_key)

            if msg.lower() != "updating":
                # remove existing link if it exists:
                await self.service_provider.postgres_db.get_pg_glossary_link_mgr().delete_glossary_link(
                    [term_uid], "LinkTermToActivityNode"
                )

                # insert link from e.g. carrot term to node:
                root_process_node_uid = self.node_id_mapper[bw_key]

                logger.info(f"linking process for term {term_uid} {term_xid} {term_name} with bw_id: {bw_id}")
                await self.service_provider.postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
                    GlossaryLink(
                        gap_filling_module="LinkTermToActivityNode",
                        term_uids=[term_uid],
                        linked_node_uid=root_process_node_uid,
                    )
                )

    async def get_processing_ids_to_import_and_store_trigger_tags_in_gfm_cache_table(self) -> list[tuple[str, str]]:
        """Helper method to get the IDs needed for the Processing GFM and store trigger tags in the gfm-cache table."""
        processing_settings = ProcessingSettings()
        dataframe_for_processing_EBD_nodes = ProcessingGapFillingFactory.get_dataframe_from_gdrive(processing_settings)
        if dataframe_for_processing_EBD_nodes is None:
            logger.error("Dataframe with with IDs for nodes needed for the Processing GFM not found.")
            return []

        await ProcessingGapFillingFactory.set_process_trigger_tags_cache_table_and_add_matchings(
            dataframe_for_processing_EBD_nodes, processing_settings, self.service_provider
        )

        bw_keys_to_import = []
        for bw_key in dataframe_for_processing_EBD_nodes["Brightway ID or Electricity Amount"]:
            bw_key_eval = literal_eval(bw_key)
            if isinstance(bw_key_eval, tuple) and all(isinstance(key, str) for key in bw_key_eval):
                try:
                    bw_key_eval: tuple[str, str] = bw_key_eval
                    exchanges = self.bw_databases.get_activity(bw_key_eval)["exchanges"]
                    bw_keys_to_import.append(bw_key_eval)
                    for exchange in exchanges:
                        bw_key_exchange = exchange.get("input")
                        if bw_key_exchange != bw_key_eval and "biosphere" not in exchange["input"][0]:
                            bw_keys_to_import.append(bw_key_exchange)
                except SyntaxError:
                    logger.warning(f"Could not parse bw_key {bw_key}. skipping...")
                except KeyError:
                    logger.warning(f"Could not find bw_key {bw_key} in the database skipping...")
        return bw_keys_to_import

    async def import_bw_products(self, args: argparse.Namespace) -> Tuple[int, int]:
        """Adds inventory data into our EOS database.

        It picks data from EDB about e.g. carrot and imports its whole technosphere and biosphere into EOS' db
        """
        start_time = time.process_time()

        def import_country_specific_nodes(nodes_name: str, gfm_country_code_bw_key_dict: dict[str, str]) -> None:
            """Helper method to get the bw-keys of all country specific nodes that need to end up in GFM caches."""
            db_name = "ecoinvent 3.6 cutoff"
            ecoinvent_cutoff_db = Database(db_name)
            hits = ecoinvent_cutoff_db.search(
                nodes_name, limit=1000
            )  # this number is arbitrary, but should be enough for now
            if len(hits) > 0:
                for hit in hits:
                    if hit["name"] == nodes_name:
                        bw_key = hit.key
                        try:
                            self.bw_databases.get_activity(bw_key)
                            country_code = hit["location"]
                            gfm_country_code_bw_key_dict[f"{country_code}"] = bw_key
                        except KeyError:
                            logger.error(f"bw_key {bw_key} does not exist in project. skipping...")
                            continue
            else:
                logger.error(f'no activity found for "{nodes_name}" in database "{db_name}"')

        # Import electricity markets
        country_specific_nodes_to_be_imported: dict[str, dict[str, str]] = {}
        gfm_name = "ElectricityMarketMediumVoltage"
        medium_voltage_country_code_bw_key_dict: dict[str, str] = {}
        electricity_nodes_medium_voltage_name = "market for electricity, medium voltage"
        import_country_specific_nodes(electricity_nodes_medium_voltage_name, medium_voltage_country_code_bw_key_dict)
        electricity_nodes_medium_voltage_name = "market group for electricity, medium voltage"
        import_country_specific_nodes(electricity_nodes_medium_voltage_name, medium_voltage_country_code_bw_key_dict)
        country_specific_nodes_to_be_imported[gfm_name] = medium_voltage_country_code_bw_key_dict

        gfm_name = "ElectricityMarketLowVoltage"
        low_voltage_country_code_bw_key_dict: dict[str, str] = {}
        electricity_nodes_low_voltage_name = "market for electricity, low voltage"
        import_country_specific_nodes(electricity_nodes_low_voltage_name, low_voltage_country_code_bw_key_dict)
        electricity_nodes_low_voltage_name = "market group electricity, low voltage"
        import_country_specific_nodes(electricity_nodes_low_voltage_name, low_voltage_country_code_bw_key_dict)
        country_specific_nodes_to_be_imported[gfm_name] = low_voltage_country_code_bw_key_dict

        processing_ids_to_import = await self.get_processing_ids_to_import_and_store_trigger_tags_in_gfm_cache_table()
        # HEATING MIX AND INFRASTRUCTURE TERMS: (TODO Ask Science to add to airtable)
        heating_and_infrastrucutre_bw_keys_to_import = processing_ids_to_import
        # heating
        heating_mix_base_xid = "4220cbaabca343c09b19415d5ab4079f"
        for suffix in ("", "_copy1", "_copy2", "_copy3"):
            bw_key = ("EDB", f"{heating_mix_base_xid}{suffix}")
            try:
                self.bw_databases.get_activity(bw_key)
            except KeyError:
                logger.error(f"bw_key {bw_key} does not exist in project. skipping...")
                continue
            heating_and_infrastrucutre_bw_keys_to_import.append(bw_key)
        # infrastructure
        infrastructure_ids = ["0ce659c3cfd443a38761058ee62e3f10", "fddfe51c6959f41ac044089c3a892af7"]
        for infrastructure_id in infrastructure_ids:
            bw_key = ("ecoinvent 3.6 cutoff", infrastructure_id)
            try:
                self.bw_databases.get_activity(bw_key)
            except KeyError:
                logger.error(f"bw_key {bw_key} does not exist in project. skipping...")
                continue
            heating_and_infrastrucutre_bw_keys_to_import.append(bw_key)

        await self.node_id_mapper.init_xid_cache(self.namespace_uid)

        num_ids_before = len(self.node_id_mapper)

        if len(args.import_by_name) > 0:
            logger.info(f"importing products by name: {args.import_by_name}")
            await self._import_by_name(args, self.import_process, "import")
            if len(self.upsert_helper.activity_nodes_to_update):
                logger.info(f"updating products by name: {args.import_by_name}")
                self.node_traverser.reset_all()
                await self._import_by_name(args, self.find_system_process_to_delete, "update")

        if args.import_edb_database:
            logger.info("importing all products from EDB database")
            edb_db = Database("EDB")
            for bw_keys in edb_db.keys():
                logger.info(f'import activity "{bw_keys}" from database "EDB"')
                await self.import_process(bw_keys)
            for bw_keys in heating_and_infrastrucutre_bw_keys_to_import:
                logger.info(f'import heating / infrastructure activity "{bw_keys}"')
                await self.import_process(bw_keys)
            for gfm_country_code_bw_key_dict in country_specific_nodes_to_be_imported.values():
                for bw_keys in gfm_country_code_bw_key_dict.values():
                    logger.info(f'import country-specific activity "{bw_keys}"')
                    await self.import_process(bw_keys)

            if len(self.upsert_helper.activity_nodes_to_update):
                self.node_traverser.reset_all()
                for bw_keys in edb_db.keys():
                    logger.info(f'update activity "{bw_keys}" from database "EDB"')
                    await self.find_system_process_to_delete(bw_keys)
                for bw_keys in heating_and_infrastrucutre_bw_keys_to_import:
                    logger.info(f'update heating / infrastructure activity "{bw_keys}"')
                    await self.find_system_process_to_delete(bw_keys)
                for gfm_country_code_bw_key_dict in country_specific_nodes_to_be_imported.values():
                    for bw_keys in gfm_country_code_bw_key_dict.values():
                        logger.info(f'update electricity activity "{bw_keys}"')
                        await self.find_system_process_to_delete(bw_keys)

        if args.import_using_airtable:
            logger.info("importing all products specified in Airtable...")

            table = Api(self.settings.AIRTABLE_API_KEY).all(
                self.settings.AIRTABLE_BASE_ID,
                self.settings.AIRTABLE_TABLE_ID,
                view=self.settings.AIRTABLE_VIEW_NAME,
            )
            products_df = pd.DataFrame([row.get("fields") for row in table])
            products_df = products_df[
                [self.settings.PRODUCTMATCHINGS_COLUMN_NAME, self.settings.FOODEX2_COLUMN_NAME]
            ].dropna()
            await self._import_using_airtable(products_df, self.import_process, "importing")
            for bw_keys in heating_and_infrastrucutre_bw_keys_to_import:
                logger.info(f'import heating / infrastructure activity "{bw_keys}"')
                await self.import_process(bw_keys)

            for gfm_country_code_bw_key_dict in country_specific_nodes_to_be_imported.values():
                for bw_keys in gfm_country_code_bw_key_dict.values():
                    logger.info(f'import electricity activity "{bw_keys}"')
                    await self.import_process(bw_keys)

            if len(self.upsert_helper.activity_nodes_to_update):
                self.node_traverser.reset_all()
                await self._import_using_airtable(products_df, self.find_system_process_to_delete, "updating")
                for bw_keys in heating_and_infrastrucutre_bw_keys_to_import:
                    logger.info(f'update heating / infrastructure activity "{bw_keys}"')
                    await self.find_system_process_to_delete(bw_keys)
                for gfm_country_code_bw_key_dict in country_specific_nodes_to_be_imported.values():
                    for bw_keys in gfm_country_code_bw_key_dict.values():
                        logger.info(f'update electricity activity "{bw_keys}"')
                        await self.find_system_process_to_delete(bw_keys)

        # TODO: rework this
        if args.import_using_glossary:
            logger.info("importing products using 'edb-bw-id' in glossary...")

            # check which processes need to be loaded:
            terms_with_edb_bw_ids = await self.service_provider.postgres_db.pool.fetch(
                "SELECT * FROM Term WHERE data->'edb-bw-id' is not null"
            )
            await self._import_using_glossary(terms_with_edb_bw_ids, self.import_process, "importing")
            if len(self.upsert_helper.activity_nodes_to_update):
                self.node_traverser.reset_all()
                await self._import_using_glossary(terms_with_edb_bw_ids, self.find_system_process_to_delete, "updating")

        num_new_nodes = len(self.node_id_mapper) - num_ids_before

        start_time_update = time.process_time()
        if len(self.upsert_helper.activity_nodes_to_update):
            logger.info("Starting to update outdated cache.")
            await self._update_nodes_local_cache()
        else:
            logger.info("Starting to compute new system process caches.")

        await self._process_system_processes_to_update()
        await self._update_database_and_broadcast()

        if len(self.upsert_helper.activity_nodes_to_update):
            logger.info(f"Finished updating outdated cache in {time.process_time() - start_time_update} seconds.")
        else:
            logger.info(f"Finished computing cache in {time.process_time() - start_time_update} seconds.")

        num_updated_activities = len(self.upsert_helper.activity_nodes_to_update)

        logger.info(
            f"imported {num_new_nodes} new nodes and updated {num_updated_activities} outdated activities "
            f"in {time.process_time() - start_time} seconds"
        )

        start_time_update_country_specific_cache = time.process_time()
        number_of_updated_country_specific_nodes = 0

        for gfm_name, gfm_country_code_bw_key_dict in country_specific_nodes_to_be_imported.items():
            gfm_cache = await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
                gfm_name=gfm_name,
                limit=1000,  # this number is arbitrary, but should be enough for now
            )
            existing_gfm_cache = {cache_entry["cache_key"]: cache_entry["cache_data"] for cache_entry in gfm_cache}
            for country_code, bw_key in gfm_country_code_bw_key_dict.items():
                new_cache_value = str(self.node_id_mapper[bw_key])
                if country_code not in existing_gfm_cache or new_cache_value != existing_gfm_cache[country_code]:
                    await self.service_provider.postgres_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
                        gfm_name=gfm_name,
                        cache_key=country_code,
                        cache_data=str(self.node_id_mapper[bw_key]),
                        load_on_boot=True,
                    )
                    logger.info(
                        f"imported country specific code {bw_key} for country code {country_code}"
                        f" into {gfm_name} cache."
                    )
                    number_of_updated_country_specific_nodes += 1
        logger.info(
            f"imported {number_of_updated_country_specific_nodes} electricity markets into cache in "
            f"{time.process_time()-start_time_update_country_specific_cache}."
        )

        return num_new_nodes, num_updated_activities

    def _get_activity_production_amount(self, bw_key: Tuple[str, str], properties: dict) -> float:
        # WARNING: We cannot rely on the "production amount" in the activity itself, as it's sign is
        # sometimes wrong and not the same like in the real production exchange, which would be used during
        # LCA calculation in brightway.

        production_amount = None
        warning_msg = None

        if production_amount is None:
            candidates1 = [exc for exc in properties["exchanges"] if exc.get("type") == "production"]
            if len(candidates1) == 1:
                production_exc = candidates1[0]
                production_amount = production_exc.get("amount")

        if production_amount is None:
            warning_msg = (
                f"could not find production exchange for activity {bw_key} based on type. "
                f"Try finding by reference product name..."
            )
            # fallback: try to find the production exchange based on reference product name:
            candidates2 = [
                exc
                for exc in properties["exchanges"]
                if self.bw_databases.get_activity(exc.get("input")).get("name") == properties.get("reference product")
            ]
            if len(candidates2) == 1:
                production_exc = candidates2[0]
                production_amount = production_exc.get("amount")

        if production_amount is None:
            warning_msg = (
                f"could not find production exchange for activity {bw_key}. Falling back to the "
                f"production amount property..."
            )
            production_amount = properties.get("production amount")

        if production_amount is None:
            warning_msg = f"could not find production amount property for activity {bw_key}. Falling back to 1."
            production_amount = 1.0

        if warning_msg and bw_key[0] != "biosphere3":
            logger.warning(warning_msg)

        return production_amount

    def _get_activity_raw_input_and_node_type(
        self, bw_key: Tuple[str, str], properties: dict
    ) -> Tuple[dict[str, Any], Type[ElementaryResourceEmissionNode | ModeledActivityNode]]:
        # for biosphere, there is no production exchange, so we need to use the "production amount" property
        production_amount = self._get_activity_production_amount(bw_key, properties)
        if properties.get("database") == "biosphere3":
            raw_input = {
                "key": bw_key,
                "database": properties.get("database"),
                "type": properties.get("type"),
                "unit": properties.get("unit"),
                "code": properties.get("code"),
                "production_amount": production_amount,
                "categories": properties.get("categories"),
                "name": properties.get("name"),
            }
            node_class = ElementaryResourceEmissionNode
        else:
            raw_input = {
                "key": bw_key,
                "location": properties.get("location"),
                "database": properties.get("database"),
                "type": properties.get("type"),
                "reference_product": properties.get("reference product"),
                "flow": properties.get("flow"),
                "unit": properties.get("unit"),
                "production_amount": production_amount,
                # "comment": properties.get('comment'), TODO comments takes too much space;
                "classifications": properties.get("classifications"),
                "categories": properties.get("categories"),
                "activity_type": properties.get("activity type"),
                "filename": properties.get("filename"),
                "name": properties.get("name"),
                "parameters": properties.get("parameters"),
                "authors": properties.get("authors"),
            }
            node_class = ModeledActivityNode
        return raw_input, node_class

    @staticmethod
    def _get_exchange_raw_input(
        exchange: dict,
    ) -> dict[str, Any]:
        raw_input = {
            "flow": exchange.get("flow"),
            "type": exchange.get("type"),  # e.g. technosphere
            "name": exchange.get("name"),
            "classification": exchange.get("classification"),
            "production_volume": exchange.get("production volume"),
            "activity": exchange.get("activity"),
            "amount": exchange["amount"],
            "unit": exchange.get("unit"),
            # 'comment': exchange.get('comment'), TODO comments takes too much space; do we really need them?
            "pedigree": exchange.get("pedigree"),
            "uncertainty_type": exchange.get("uncertainty type"),
            "loc": exchange.get("loc"),
            "scale": exchange.get("scale"),
            "scale_without_pedigree": exchange.get("scale without pedigree"),
        }
        return raw_input

    def _get_exchanges_to_compare(self, exchanges: list[dict]) -> dict[str, dict]:
        exchanges_to_compare = {}
        for exchange in exchanges:
            raw_input = self._get_exchange_raw_input(exchange)
            exchanges_to_compare[exchange.get("xid")] = {"raw_input": raw_input, "input_activity": exchange["input"]}
        return exchanges_to_compare

    @staticmethod
    def get_exchange_xid(
        exchange: dict[str, Any], act_properties: dict, exchange_index: int, bw_key: BwKey
    ) -> str | None:
        parent_activity_xid = f'{act_properties["database"]}_{act_properties["code"]}'
        if exchange["input"] == bw_key:
            # this is an exchange from and to the same node,
            # handle it differently if type=production
            if exchange.get("type") == "production":
                # this is already handled above and inserted as "production_amount" inside raw_input
                return None
            else:
                return f"{parent_activity_xid}_index{exchange_index}"
                # this is a technosphere exchange. just add it as a graph
        else:
            return f"{parent_activity_xid}_index{exchange_index}"

    async def _process_exchange(self, exchange: dict[str, Any]) -> Tuple[FlowNode, Edge, Edge]:
        # EOS nodes uuids (that we created above):
        input_node_uid = self.node_id_mapper[exchange.get("input")]
        output_node_uid = self.node_id_mapper[exchange.get("output")]

        # check that units are the same btw this exchange and its input activity
        # normally it should be the case, if the BW db is consistent, but we check it anyway
        input_activity_properties = self.bw_databases.get_activity(exchange.get("input"))
        if "unit" in exchange and input_activity_properties.get("unit") != exchange.get("unit"):
            # in case exchange does not have a "unit" specified, it is implicitly assumed the same as the activity.
            raise ValueError(
                f"unit mismatch btw activity {exchange.get('input')} & exchange {exchange.get('xid')}:"
                f" {input_activity_properties.get('unit')} != {exchange.get('unit')}"
            )

        # we generate a reproducible xid so that later updates are possible
        # (by finding the exchange node):
        uid = await self.get_uid_by_xid_cached(xid=exchange.get("xid"))
        raw_input = self._get_exchange_raw_input(exchange)

        exchange_node = FlowNode(
            uid=uid,
            amount_in_original_source_unit={"value": raw_input.get("amount"), "unit": raw_input.get("unit")},
            flow_location=raw_input.get("location"),
            raw_input={key: val for key, val in raw_input.items() if key not in ("amount", "unit", "location")},
        )

        # Now connect this exchange to parent activity
        # Note: we insert edges in the opposite directions as in Brightway, e.g. it goes from carrot to fertilizer
        edge_up = Edge(parent_uid=exchange_node.uid, child_uid=input_node_uid, edge_type=EdgeTypeEnum.lca_link)

        # now connect this exchange to child activity
        edge_down = Edge(parent_uid=output_node_uid, child_uid=exchange_node.uid, edge_type=EdgeTypeEnum.lca_link)

        return exchange_node, edge_up, edge_down

    async def import_process(self, bw_key_of_root_process: BwKey) -> None:
        start_time = time.process_time()

        # stack of tuple(BwKey, depth):
        nodes_to_process: list[tuple[BwKey, int]] = [(bw_key_of_root_process, 0)]

        # 2. iterate down the technosphere of each product and add only activities (without exchanges/links)
        exchanges_to_insert = []
        logger.info("starting importing activities...")

        try:
            while len(nodes_to_process):
                bw_key, depth = nodes_to_process.pop()
                act_properties = self.bw_databases.get_activity(bw_key)

                if bw_key in self.node_traverser:
                    if bw_key == bw_key_of_root_process:
                        # Add system process cache if this previously visited root process is being newly imported.
                        if self.node_id_mapper[bw_key] in self.upsert_helper.all_new_nodes:
                            self.upsert_helper.system_processes_to_update.add(self.node_id_mapper[bw_key])

                        else:
                            # Add system process cache if previously visited root process has no aggregated cache.
                            cached_node = self.service_provider.node_service.cache.get_cached_node_by_uid(
                                self.node_id_mapper[bw_key]
                            )
                            if isinstance(cached_node, ActivityNode) and cached_node.aggregated_cache is None:
                                self.upsert_helper.system_processes_to_update.add(self.node_id_mapper[bw_key])

                    logger.debug(f"skipping bw_key '{bw_key}' as it has been traversed.")
                    continue  # activity already in db, skipping
                self.node_traverser.add(bw_key)
                exchanges_of_current_activity = []

                raw_input, node_class = self._get_activity_raw_input_and_node_type(bw_key, act_properties)
                # recursively insert nodes from this node's exchanges
                for exchange_index, exchange in enumerate(act_properties["exchanges"]):
                    if exchange["output"] != bw_key:
                        logger.error("exchange output does not match activity key")

                    if depth < MAXIMUM_DEPTH:
                        exchange_xid = self.get_exchange_xid(exchange, act_properties, exchange_index, bw_key)
                        if exchange_xid:
                            exchange["xid"] = exchange_xid
                            exchanges_of_current_activity.append(exchange)
                        # only recurse if source activity has not yet been added
                        if exchange["input"] not in self.node_traverser:
                            nodes_to_process.append((exchange["input"], depth + 1))

                if bw_key in self.node_id_mapper and bw_key == bw_key_of_root_process:
                    cached_node = self.service_provider.node_service.cache.get_cached_node_by_uid(
                        self.node_id_mapper[bw_key]
                    )
                    if isinstance(cached_node, ActivityNode) and cached_node.aggregated_cache is None:
                        self.upsert_helper.system_processes_to_update.add(self.node_id_mapper[bw_key])

                if (bw_key in self.node_id_mapper) and not ("biosphere" in bw_key[0] or "ecoinvent" in bw_key[0]):
                    self.upsert_helper.add_to_compare(
                        bw_key, raw_input, self._get_exchanges_to_compare(exchanges_of_current_activity)
                    )
                if bw_key not in self.node_id_mapper:
                    exchanges_to_insert += exchanges_of_current_activity
                    # removed separate call to add bw_key to node_id_mapper as get_uid_by_xid_cached does this.
                    uid = await self.get_uid_by_xid_cached(self.node_id_mapper.bw_key_to_xid(bw_key))

                    node = node_class(
                        uid=uid,
                        production_amount={"value": raw_input.get("production_amount"), "unit": raw_input.get("unit")},
                        activity_location=raw_input.get("location"),
                        raw_input={
                            key: val
                            for key, val in raw_input.items()
                            if key not in ("production_amount", "unit", "location")
                        },
                    )
                    # add the activity node to the BatchImporter,
                    # but delay the actual import to later calling flush()...
                    self.batch_importer.insert_node(node)

                    # If root process is not already in the database, we should add system process cache.
                    if bw_key == bw_key_of_root_process:
                        self.upsert_helper.system_processes_to_update.add(uid)

                    # Other new nodes are also saved because the sub nodes of this root process
                    # can itself be a root process.
                    self.upsert_helper.all_new_nodes.add(uid)

        except Exception as e:
            logger.error(f"error while importing process {bw_key_of_root_process}: {e}")

            # we clear the BatchImporter to not leave half-prepared import in the cache to be flushed later:
            self.batch_importer.clear()

            # also reset the stages id mappings (to prevent errors in next processes to import):
            self.node_id_mapper.reset_staged_items()

            self.node_traverser.reset_staged_items()

            # reraise the exception:
            raise e

        # flush activities to db:
        await self.batch_importer.flush()
        await self.upsert_helper.compare(self.node_id_mapper, self.service_provider.node_service)

        # move the stages id mappings to the final mappings:
        self.node_id_mapper.commit_staged_items()

        # move the stages node traverser to the final mappings:
        self.node_traverser.commit_staged_items()

        # 3. add all brightway exchanges that we collected above
        logger.info(f"starting to import {len(exchanges_to_insert)} exchanges...")
        for counter, exchange in enumerate(exchanges_to_insert):
            exchange_node, edge_up, edge_down = await self._process_exchange(exchange)
            self.batch_importer.insert_node(exchange_node)
            self.batch_importer.insert_edge(edge_up)
            self.batch_importer.insert_edge(edge_down)
            if counter % 10000 == 0 and counter > 0:
                logger.info(f"flushing imported {counter} exchanges...")
                await self.batch_importer.flush()

        # flush all remaining exchanges to db:
        await self.batch_importer.flush()

        logger.info(
            f'imported process {self.bw_databases.get_activity(bw_key_of_root_process)["name"]} '
            f"in {time.process_time() - start_time} seconds"
        )

    async def _process_system_processes_to_update(self) -> None:
        async def _prepare_dummy_root_node() -> Tuple[FlowNode, ModeledActivityNode]:
            dummy_root_node = FlowNode(
                uid=uuid.uuid4(), amount_in_original_source_unit={"value": 1.0, "unit": "kilogram"}
            )
            self.service_provider.node_service.cache.add_node(dummy_root_node)

            dummy_node = ModeledActivityNode(
                uid=uuid.uuid4(),
                production_amount={"value": 1.0, "unit": "kilogram"},
                raw_input={
                    "titles": [{"value": "dummy_node_for_system_caching"}],
                    "date": datetime.datetime.now().strftime("%Y-%m-%d"),
                },
            )
            self.service_provider.node_service.cache.add_node(dummy_node)

            dummy_edge = Edge(
                parent_uid=dummy_root_node.uid,
                child_uid=dummy_node.uid,
                edge_type=EdgeTypeEnum.lca_link,
            )
            self.service_provider.node_service.cache.add_edge(dummy_edge)

            for bw_node_uid in self.upsert_helper.system_processes_to_update:
                try:
                    bw_node = self.service_provider.node_service.cache.get_cached_node_by_uid(bw_node_uid)
                    if bw_node.production_amount:
                        bw_unit = bw_node.production_amount.unit
                        if bw_unit is None:
                            bw_unit = "kg"
                    else:
                        bw_unit = "kg"
                except KeyError:
                    bw_unit = "kg"

                flow = FlowNode(
                    uid=uuid.uuid4(),
                    amount_in_original_source_unit={"value": 1.0, "unit": bw_unit},
                    raw_input={
                        "type": "technosphere",
                    },
                )
                self.service_provider.node_service.cache.add_node(flow)
                edge_up = Edge(
                    parent_uid=dummy_node.uid,
                    child_uid=flow.uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
                self.service_provider.node_service.cache.add_edge(edge_up)
                edge_down = Edge(
                    parent_uid=flow.uid,
                    child_uid=bw_node_uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
                self.service_provider.node_service.cache.add_edge(edge_down)

            return dummy_root_node, dummy_node

        for xid in self.upsert_helper.delete_system_process_cache:
            node_uid = self.node_id_mapper[xid]
            node = await self.service_provider.node_service.find_by_uid(node_uid)
            if isinstance(node, ActivityNode) and node.aggregated_cache is not None:
                node.aggregated_cache = None
                node.model_fields_set.remove("aggregated_cache")
                self.service_provider.node_service.cache.add_node(node)
                self.batch_updater.insert_node_to_update(node_uid)
                self.upsert_helper.system_processes_to_update.add(node_uid)

        # Dummy recipe for system process caching of multiple nodes.
        if self.upsert_helper.system_processes_to_update:
            root_node, child_of_root_node = await _prepare_dummy_root_node()

            calculation = Calculation(
                uid=uuid.uuid4(),
                root_node_uid=root_node.uid,
                child_of_root_node_uid=child_of_root_node.uid,
                save_as_system_process=True,
                fixed_depth_for_calculating_supply=3,
                use_system_processes=False,
            )
            assert (
                not calculation.use_system_processes
            ), "use_system_processes should be False so that all FlowNodes are added."

            # Rather than use product_service.bulk_insert_xid_uid_mappings directly in the batch_importer,
            # update_all_mappings here before running orchestrator.
            await self.service_provider.product_service.update_all_mappings()
            await self.service_provider.gap_filling_module_loader.init(self.service_provider)

            calc_service = CalcService(
                service_provider=self.service_provider,
                glossary_service=self.service_provider.glossary_service,
                node_service=self.service_provider.node_service,
                gap_filling_module_loader=self.service_provider.gap_filling_module_loader,
            )
            calc_graph, orchestrator = calc_service.create_calc_graph_and_orchestrator(calculation)

            # start running the orchestrator, which will internally call gap filling modules:
            async with self.semaphore:  # limits the number of concurrent orchestrations
                await orchestrator.run()

            for uid in calc_graph.nodes_to_calculate_supply:
                if uid in self.upsert_helper.system_processes_to_update:
                    cached_node = await self.service_provider.node_service.find_by_uid(uid)
                    calc_node = calc_graph.get_node_by_uid(uid)
                    cached_node.aggregated_cache = get_system_process_data(calc_node, calc_graph)
                    self.service_provider.node_service.cache.add_node(cached_node)
                    self.batch_updater.insert_node_to_update(cached_node.uid)

    async def _update_nodes_local_cache(self) -> None:
        self.batch_importer.clear()  # Clear batch importer. It is used here for namespace_uid_xid_uid insertion.
        for bw_key in self.upsert_helper.activity_nodes_to_update:
            old_exchanges = await self.get_exchanges_of_activity(bw_key)
            for exchange_xid, exchange_uid in old_exchanges.items():
                self.stage_for_delete_uid_by_xid_cached(exchange_xid, exchange_uid)
                self.service_provider.node_service.cache.invalidate_cached_node_by_uid(exchange_uid)
            self.node_id_mapper.commit_delete_mapping()

            node_uid = self.node_id_mapper[bw_key]
            node = self.service_provider.node_service.cache.get_cached_node_by_uid(node_uid)
            if node.aggregated_cache is not None:
                self.upsert_helper.system_processes_to_update.add(node_uid)

            self.service_provider.node_service.cache.invalidate_cached_node_by_uid(node_uid)

            act_properties = self.bw_databases.get_activity(bw_key)
            raw_input, node_class = self._get_activity_raw_input_and_node_type(bw_key, act_properties)

            self.service_provider.node_service.cache.add_node(
                node_class(
                    uid=node_uid,
                    production_amount={"value": raw_input.get("production_amount"), "unit": raw_input.get("unit")},
                    activity_location=raw_input.get("location"),
                    raw_input={
                        key: val
                        for key, val in raw_input.items()
                        if key not in ("production_amount", "unit", "location")
                    },
                )
            )
            # FIXME: the node_service.cache is now missing the links from parent exchanges to the updated node_uid,
            #  because they were removed with the above call to invalidate_cached_node_by_uid(node_uid) and below we
            #  are only readding the edges to child exchanges. (Currently, this fixme does not apply because
            #  invalidate_cached_node_by_uid(node_uid) does not remove parent exchanges.

            self.batch_updater.insert_node_to_update(node_uid)

            for exchange_index, exchange in enumerate(act_properties["exchanges"]):
                if exchange["output"] != bw_key:
                    logger.error("exchange output does not match activity key")
                exchange_xid = self.get_exchange_xid(exchange, act_properties, exchange_index, bw_key)
                if exchange_xid:
                    exchange["xid"] = exchange_xid
                    exchange_node, edge_up, edge_down = await self._process_exchange(exchange)
                    self.service_provider.node_service.cache.add_node(exchange_node)
                    self.service_provider.node_service.cache.add_edge(edge_up)
                    self.service_provider.node_service.cache.add_edge(edge_down)
                    self.batch_updater.insert_node_to_update(exchange_node.uid)

    async def _update_database_and_broadcast(self) -> None:
        self.node_id_mapper.commit_staged_items()
        self.batch_updater.namespace_uid_xid_uid_to_update.extend(self.batch_importer.namespace_uid_xid_uid_to_insert)
        await self.batch_updater.flush()

    async def find_system_process_to_delete(self, bw_key_of_root_process: BwKey) -> None:
        start_time = time.process_time()
        logger.info(f"starting to find system processes to delete within {bw_key_of_root_process}.")

        # stack of tuple(BwKey, depth):
        stack: list[tuple[BwKey, int]] = [(bw_key_of_root_process, 0)]

        # Only process (mark as dirty) the activity nodes not already marked.
        # It is not necessary, but avoids having to mark the same node dirty multiple times.
        clean_nodes: list[BwKey] = []
        # Root node added to clean_nodes to align correctly with stack.
        if (
            bw_key_of_root_process in self.node_traverser
            and bw_key_of_root_process not in self.upsert_helper.activity_nodes_to_update
        ):
            clean_nodes.append(bw_key_of_root_process)

        try:
            while stack:
                bw_key, depth = stack[-1]
                if bw_key not in self.node_traverser:
                    self.node_traverser.add(bw_key)
                    if bw_key in self.upsert_helper.activity_nodes_to_update:
                        self.upsert_helper.add_delete_system_process_cache(bw_key)
                    else:
                        clean_nodes.append(bw_key)

                all_sub_nodes_visited = True
                marked = False
                for exchange in self.bw_databases.get_activity(bw_key)["exchanges"]:
                    if depth < MAXIMUM_DEPTH:
                        # only recurse if source activity has not yet been added
                        if not ("biosphere" in exchange["input"][0] or "ecoinvent" in exchange["input"][0]):
                            if exchange["input"] not in self.node_traverser:
                                stack.append((exchange["input"], depth + 1))
                                all_sub_nodes_visited = False
                                break
                            else:
                                if not marked:
                                    if self.upsert_helper.in_delete_system_process_cache(exchange["input"]):
                                        self.upsert_helper.add_delete_system_process_cache(bw_key)
                                        marked = True

                if all_sub_nodes_visited:
                    if self.upsert_helper.in_delete_system_process_cache(bw_key):
                        for parent_bw_key in clean_nodes:
                            self.upsert_helper.add_delete_system_process_cache(parent_bw_key)
                        clean_nodes.clear()
                    else:
                        clean_nodes.pop()
                    stack.pop()

        except Exception as e:
            logger.error(f"error while importing process {bw_key_of_root_process}: {e}")
            self.node_traverser.reset_staged_items()
            # reraise the exception:
            raise e

        logger.info(
            f"marked all outdated system processes contained within "
            f'{self.bw_databases.get_activity(bw_key_of_root_process)["name"]} '
            f"in {time.process_time() - start_time} seconds"
        )

        # move the stages node traverser to the final mappings:
        self.node_traverser.commit_staged_items()

    async def get_uid_by_xid_cached(self, xid: str) -> uuid.UUID:
        if xid in self.node_id_mapper:
            return self.node_id_mapper[xid]

        uid = uuid.uuid4()
        self.batch_importer.insert_namespace_xid_mappings(namespace_uid=self.namespace_uid, xid=xid, uid=uid)

        # add to cached uid mapping:
        self.node_id_mapper[xid] = uid

        return uid

    async def get_exchanges_of_activity(self, bw_key: BwKey) -> dict[str, Any]:
        """Retrieve from the node_service cache all exchanges associated with an activity.

        :param bw_key: bw_key of the activity of interest.
        :return: xid to uid mappings of all exchanges of an activity
        """
        activity_xid = self.node_id_mapper.bw_key_to_xid(bw_key)
        exchange_dict = {}
        exchange_count = await self.upsert_helper.get_exchange_count_of_activity(
            self.node_id_mapper, self.service_provider.node_service, activity_xid
        )
        index = 0
        while len(exchange_dict) < exchange_count:
            exchange_xid = f"{activity_xid}_index{index}"
            if exchange_xid in self.node_id_mapper:
                exchange_dict[exchange_xid] = self.node_id_mapper[exchange_xid]
            else:
                logger.warning(f"exchange {exchange_xid} not found in node_id_mapper.")
            index += 1
            assert index < 10000, "infinite loop detected"
        return exchange_dict

    def stage_for_delete_uid_by_xid_cached(self, xid: str, uid: uuid.UUID) -> None:
        self.batch_updater.insert_namespace_xid_mappings_to_delete(namespace_uid=self.namespace_uid, xid=xid, uid=uid)
        self.batch_updater.insert_node_to_delete(uid=uid)
        self.node_id_mapper.delete_mapping(xid)

    async def import_impact_assessments(self, args: argparse.Namespace) -> int:
        namespace_mgr = self.service_provider.postgres_db.get_namespace_mgr()

        impact_assessment_namespace = "Impact Assessment Methods Glossary Term namespace"
        namespace = await self.service_provider.namespace_service.find_namespace_by_name(impact_assessment_namespace)

        if not namespace:
            namespace = Namespace(name=impact_assessment_namespace)
            _, _, impact_assessment_access_group = await namespace_mgr.insert_or_update_namespace_wrapper(namespace)
            impact_assessment_access_group_uid = impact_assessment_access_group.uid
        else:
            impact_assessment_access_group = (
                await self.service_provider.access_group_service.get_all_access_group_ids_by_namespace(namespace.uid)
            )
            impact_assessment_access_group_uid = impact_assessment_access_group[0].uid

        root_char = self.service_provider.glossary_service.root_subterms.get("Root_Impact_Assessments")
        if root_char:
            existing_terms = await self.service_provider.glossary_service.get_sub_terms_by_uid(root_char.uid, depth=1)
            existing_term_xids = set([existing_term.xid for existing_term in existing_terms])
        else:
            root_glossary_term_uuid = PgSettings().ROOT_GLOSSARY_TERM_UUID
            logging.info("Adding root impact assessments term.")
            root_char = Term(
                uid=uuid.uuid4(),
                name="Impact Assessments",
                sub_class_of=uuid.UUID(root_glossary_term_uuid),
                xid="Root_Impact_Assessments",
                data={},
                access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
            )
            await self.service_provider.glossary_service.put_term_by_uid(root_char)
            existing_term_xids = set()

        existing_methods = await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
            gfm_name="ImpactAssessmentGapFillingWorker",
            limit=5000,  # This number was chosen arbitrarily, based on our bw dump having 851 impact_assessments.
        )
        existing_method_ids = set()
        existing_outdated_method_ids = set()
        if existing_methods:
            for existing_method in existing_methods:
                if (
                    existing_method["cache_data"].get("unit")
                    and existing_method["cache_data"].get("data")
                    and existing_method["cache_key"] in existing_term_xids
                ):
                    existing_method_ids.add(existing_method["cache_key"])
                else:
                    existing_outdated_method_ids.add(existing_method["cache_key"])

        if "EOS_daily_food_unit" not in existing_term_xids:
            dfu_term = Term(
                uid=uuid.uuid4(),
                xid="EOS_daily_food_unit",
                name="Daily food unit",
                sub_class_of=root_char.uid,
                access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
                data={
                    "description": "Daily food unit computed as ("
                    "proteins in g / 50 + fats in g / 66 + energy in kJ / 6000 "
                    "+ water in g / 2500 + dry mass in g / 600"
                    ") / 5"
                },
            )
            await self.service_provider.glossary_service.put_term_by_uid(dfu_term)

        if "EOS_vita-score" not in existing_term_xids:
            vitascore_term = Term(
                uid=uuid.uuid4(),
                xid="EOS_vita-score",
                name="Vita score",
                sub_class_of=root_char.uid,
                access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
                data={"description": "Vita score computed from global burden of diseases study."},
            )
            await self.service_provider.glossary_service.put_term_by_uid(vitascore_term)

        root_unit_term = self.service_provider.glossary_service.root_subterms["EOS_units"]
        existing_unit_terms = await self.service_provider.glossary_service.get_sub_terms_by_uid(root_unit_term.uid)
        existing_unit_term_xids = set([existing_term.xid for existing_term in existing_unit_terms])

        impact_assessment_units_xid = "EOS_units_impact_assessment"
        impact_assessment_units_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            (impact_assessment_units_xid, root_unit_term.access_group_uid)
        )
        if not impact_assessment_units_term:
            impact_assessment_units_term = Term(
                uid=uuid.uuid4(),
                xid="EOS_units_impact_assessment",
                name="Impact assessment units",
                sub_class_of=root_unit_term.uid,
                data={},
                access_group_uid=root_unit_term.access_group_uid,
            )
            await self.service_provider.glossary_service.put_term_by_uid(impact_assessment_units_term)

        start_time = time.process_time()
        imported_methods = 0
        if args.import_all_impact_assessments:
            methods_to_import = list(self.bw_databases.methods)
        elif args.import_impact_assessments_by_name:
            methods_to_import = args.import_impact_assessments_by_name
        else:
            methods_to_import = None

        imported_method_units: set[str] = set()
        terms_to_insert: list[Term] = []
        outdated_methods: list[str] = []
        for method in methods_to_import:
            impact_assessment_xid = Method(method).metadata.get("abbreviation")
            imported_method_units.add(Method(method).metadata.get("unit"))
            if impact_assessment_xid not in existing_method_ids:
                try:
                    await self.import_method(method)
                    imported_methods += 1
                except UnknownObject:
                    logger.info(f"{method} not imported as it is absent from the database")
            else:
                logger.info(f"{method} is already in database")

            if (outdated_cache_key := re.sub(" ", "_", "_".join(method)).lower()) in existing_outdated_method_ids:
                outdated_methods.append(outdated_cache_key)
                logger.info(f"deleting outdated method {method}")

            if impact_assessment_xid not in existing_term_xids:
                terms_to_insert.append(
                    Term(
                        uid=uuid.uuid4(),
                        xid=impact_assessment_xid,
                        name=" ".join(method),
                        data={"description": Method(method).metadata.get("description")},
                        sub_class_of=root_char.uid,
                        access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
                    )
                )
        await self.service_provider.glossary_service.put_many_terms(terms_to_insert)
        logger.info(f"importing {len(terms_to_insert)} impact assessment terms.")

        unit_terms_to_insert: list[Term] = []
        for method_unit in imported_method_units:
            unit_xid = f"EOS_{re.sub(' ', '-', method_unit.lower())}"
            if unit_xid not in existing_unit_term_xids:
                unit_terms_to_insert.append(
                    Term(
                        uid=uuid.uuid4(),
                        xid=unit_xid,
                        name=method_unit,
                        data={},
                        sub_class_of=impact_assessment_units_term.uid,
                        access_group_uid=root_unit_term.access_group_uid,
                    )
                )

        await self.service_provider.glossary_service.put_many_terms(unit_terms_to_insert)
        logger.info(f"importing {len(unit_terms_to_insert)} impact assessment unit terms.")

        if outdated_methods:
            await self.service_provider.postgres_db.gfm_cache_mgr.delete_many_cache_entries_by_gfm_name_and_keys(
                gfm_name="ImpactAssessmentGapFillingWorker", cache_keys=outdated_methods
            )

        logger.info(f"imported {imported_methods} new impact assessments in {time.process_time() - start_time} seconds")

        return imported_methods

    async def import_method(self, method: str) -> None:
        start_time = time.process_time()
        impact_assessments = Method(method).load()
        method_unit = Method(method).metadata.get("unit")
        complete_impact_assessments = {
            "unit": method_unit,
            "data": {
                impact_assessment[0][1]: self.complete_impact_assessment(impact_assessment)
                for impact_assessment in impact_assessments
            },
        }

        await self.service_provider.postgres_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
            gfm_name="ImpactAssessmentGapFillingWorker",
            cache_key=Method(method).metadata.get("abbreviation"),
            cache_data=complete_impact_assessments,
            load_on_boot=True,
        )
        logger.info(f"imported {method} in {time.process_time() - start_time} seconds")

    def complete_impact_assessment(self, term: Tuple[Tuple[str, str], float]) -> dict[str, Any]:
        new_dict = dict()
        new_dict["amount"] = term[1]
        activity = self.bw_databases.get_activity(term[0])
        for key in ("unit", "name", "categories"):
            if key == "categories":
                categories = activity["categories"]
                if len(categories) >= 2:
                    new_dict["subcompartment"] = activity["categories"][1]
                else:
                    new_dict["subcompartment"] = "unspecified"

                if len(categories) >= 1:
                    new_dict["compartment"] = activity["categories"][0]
                else:
                    new_dict["compartment"] = "unspecified"
            else:
                new_dict[key] = activity[key]
        return new_dict
