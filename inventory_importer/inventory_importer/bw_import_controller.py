import argparse
import asyncio
import logging
import os
import sys

import structlog

from core.service.service_provider import ServiceLocator
from inventory_importer.airtable_matching_importer import MatchingLinkingImporter
from inventory_importer.download_eaternity_edb_data import download_from_github
from inventory_importer.gfm_importer import gfm_import_data
from inventory_importer.glossary_importer import GlossaryImporter

structlog.configure(
    wrapper_class=structlog.make_filtering_bound_logger(logging.INFO),
)

logger = structlog.get_logger()


def tuple_type(strings: str) -> tuple[str, ...]:
    """Convert a string to a tuple of strings."""
    strings = strings.replace("(", "").replace(")", "")
    strings = strings.replace("'", "").replace("'", "")
    strings = strings.replace('"', "").replace('"', "")
    mapped_str = [s.lstrip() for s in map(str, strings.split(","))]
    return tuple(mapped_str)


def parse(args_: list[str]) -> argparse.Namespace:
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="InventoryImporter")
    parser.add_argument("--edb_path", default="/eos/data/edb_export", help="path to the brightway project")
    parser.add_argument("--import_edb_database", action="store_true")
    parser.add_argument("--import_using_glossary", action="store_true")
    parser.add_argument("--import_using_airtable", action="store_true")
    parser.add_argument("--import_matchings_from_airtable", action="store_true")
    parser.add_argument("--download_from_gdrive")
    parser.add_argument("--gfm_import_data", action="store_true")
    parser.add_argument("--remove_gadm_files", action="store_true")
    parser.add_argument("--countries_alpha3", nargs="+", default=[], help="a list of 3-letter country codes to import")
    parser.add_argument(
        "--import_by_name",
        nargs="+",
        default=[],
        help="a list of EDB product names to import, encapsulated by double quotes, separated by space",
    )
    parser.add_argument(
        "--import_impact_assessments_by_name",
        nargs="+",
        default=[],
        type=tuple_type,
        help="a list of impact impact assessment methods to import, each method is a tuple of length 3, "
        'should be specified as, e.g., "(IPCC 2013, climate change, GWP 100a)", for a full list '
        "see bw2data.methods",
    )
    parser.add_argument(
        "--import_all_impact_assessments",
        action="store_true",
        help="import every impact assessment existing in the local bw folder",
    )

    parser.add_argument(
        "--import_glossary",
        action="store_true",
        help="upsert glossary terms",
    )

    return parser.parse_args(args_)


async def import_controller(schema: str = "public") -> None:
    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    args: argparse.Namespace = parse(sys.argv[1:])
    await service_provider.postgres_db.connect(schema)
    await service_provider.messaging_service.start()
    await service_provider.init_caches()

    if args.import_glossary:
        logger.info("start importing glossary")
        importer = GlossaryImporter()
        await importer.upsert_glossary_and_matching()

    if args.gfm_import_data:
        logger.info("start gfm_import_data")
        await gfm_import_data(args.countries_alpha3, args.remove_gadm_files)

    if args.import_matchings_from_airtable:
        logger.info("start downloading from eaternity-edb-data")
        download_from_github(args.edb_path)

        edb_path = args.edb_path
        edb_data_path = os.path.join(edb_path, "eaternity-edb-data")
        assert os.path.isdir(edb_data_path), f"could not find local eaternity-edb-data at {edb_data_path}."

        logger.info("start importing matchings, nutrients, FAO, perishability, and tags from airtable")

        matching_linking_importer = MatchingLinkingImporter(edb_data_path)
        await matching_linking_importer.import_from_airtable()

        logger.info("done importing")

    # This is BW's way to set its local database path.
    # Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = os.path.join(args.edb_path, "Base")

    if args.download_from_gdrive is not None:
        logger.info("start downloading from gdrive")

        # import after setting the environment variable:
        from inventory_importer.bw_download_gdrive import download_from_gdrive

        download_from_gdrive(args.download_from_gdrive, args.edb_path)

    trigger_character_import = len(args.import_impact_assessments_by_name) > 0 or args.import_all_impact_assessments
    trigger_database_import = (
        len(args.import_by_name) > 0
        or args.import_edb_database
        or args.import_using_glossary
        or args.import_using_airtable
    )

    if trigger_database_import or trigger_character_import:
        edb_path = args.edb_path
        assert os.path.isdir(edb_path), f"could not find local EDB database at {edb_path}."

        # import after setting the environment variable:
        from inventory_importer.bw_importer import BwImporter

        importer = BwImporter()

        logger.info("start seeding inventory sample data and impact assessments")

        await importer.import_bw_products_and_impact_assessments(args)

        logger.info("done seeding")

    await service_provider.shutdown_services()


if __name__ == "__main__":
    asyncio.run(import_controller())
