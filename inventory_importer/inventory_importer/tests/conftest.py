import asyncio
import copy
import os
import shutil
from typing import TYPE_CHECKING, Any, Generator, Tuple

import pytest
import pytest_asyncio

from core.service.service_provider import ServiceLocator, ServiceProvider
from database.postgres.postgres_db import PostgresDb

if TYPE_CHECKING:
    from bw2data import Database

biosphere3 = {
    ("biosphere3", "aa7cac3a-3625-41d4-bc54-33e2cf11ec46"): {
        "categories": ["air", "non-urban air or from high stacks"],
        "code": "aa7cac3a-3625-41d4-bc54-33e2cf11ec46",
        "CAS number": "000124-38-9",
        "name": "Carbon dioxide, fossil",
        "database": "biosphere3",
        "unit": "kilogram",
        "type": "emission",
        "exchanges": [],
    },
    ("biosphere3", "aa7cac3a-3625-41d4-bc54-33e2cf11ec46_copy1"): {
        "categories": ["air", "non-urban air or from high stacks"],
        "code": "aa7cac3a-3625-41d4-bc54-33e2cf11ec46",
        "CAS number": "000124-38-9",
        "name": "Carbon dioxide, fossil",
        "database": "biosphere3",
        "unit": "kilogram",
        "type": "emission",
        "exchanges": [],
    },
    ("biosphere3", "78c3efe4-421c-4d30-82e4-b97ac5124993"): {
        "categories": ["air", "non-urban air or from high stacks"],
        "code": "78c3efe4-421c-4d30-82e4-b97ac5124993",
        "CAS number": "007446-09-5",
        "name": "Sulfur dioxide",
        "database": "biosphere3",
        "unit": "kilogram",
        "type": "emission",
        "exchanges": [],
    },
    ("biosphere3", "88d06db9-59a1-4719-9174-afeb1fa4026a"): {
        "categories": ["natural resource", "in ground"],
        "code": "88d06db9-59a1-4719-9174-afeb1fa4026a",
        "CAS number": "null",
        "name": "Oil, crude, in ground",
        "database": "biosphere3",
        "unit": "kilogram",
        "type": "natural resource",
        "exchanges": [],
    },
}

EDB = {  # noqa
    ("EDB", "588ab0c19f87449b9a79fbeb97f82826"): {
        "database": "EDB",
        "code": "588ab0c19f87449b9a79fbeb97f82826",
        "location": "GLO",
        "name": "fuel production",
        "reference product": "fuel production",
        "unit": "l",
        "type": "process",
        "exchanges": [
            {
                "output": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "input": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "amount": 100.0,
                "unit": "l",
                "type": "production",
            },
            {
                "output": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "input": ("biosphere3", "88d06db9-59a1-4719-9174-afeb1fa4026a"),
                "amount": -50.0,
                "unit": "kilogram",
                "type": "biosphere",
            },
            {
                "output": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "input": ("biosphere3", "aa7cac3a-3625-41d4-bc54-33e2cf11ec46"),
                "amount": 10.0,
                "unit": "kilogram",
                "type": "biosphere",
            },
            {
                "output": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "input": ("biosphere3", "78c3efe4-421c-4d30-82e4-b97ac5124993"),
                "amount": 2.0,
                "unit": "kilogram",
                "type": "biosphere",
            },
        ],
    },
    ("EDB", "cdf21b8781dd46fc9479558be2f62175"): {
        "database": "EDB",
        "code": "cdf21b8781dd46fc9479558be2f62175",
        "location": "GLO",
        "name": "electricity production",
        "reference product": "electricity production",
        "unit": "kWh",
        "type": "process",
        "exchanges": [
            {
                "output": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
                "input": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
                "amount": 10.0,
                "unit": "kWh",
                "type": "production",
            },
            {
                "output": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
                "input": ("biosphere3", "aa7cac3a-3625-41d4-bc54-33e2cf11ec46"),
                "amount": 1,
                "unit": "kilogram",
                "type": "biosphere",
            },
            {
                "output": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
                "input": ("biosphere3", "78c3efe4-421c-4d30-82e4-b97ac5124993"),
                "amount": 0.1,
                "unit": "kilogram",
                "type": "biosphere",
            },
            {
                "output": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
                "input": ("EDB", "588ab0c19f87449b9a79fbeb97f82826"),
                "amount": 2.0,
                "unit": "l",
                "type": "technosphere",
            },
        ],
    },
}

impact_assessment = [(("biosphere3", "aa7cac3a-3625-41d4-bc54-33e2cf11ec46"), 1.0)]


@pytest.fixture(scope="package")
def event_loop() -> Generator[asyncio.AbstractEventLoop, Any, None]:
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="function")
async def setup_services() -> Tuple[PostgresDb, ServiceProvider]:
    """Fixture to get a clean (resetted) db for testing."""
    print("setup_services")

    service_locator = ServiceLocator()
    service_locator.recreate()
    service_provider = service_locator.service_provider

    postgres_db = service_provider.postgres_db

    await postgres_db.connect(schema="test_pg")
    await postgres_db.reset_db("test_pg")

    # TODO: should we use the messaging service in these tests?
    # await service_provider.messaging_service.start()
    service_provider.messaging_service = None

    await service_provider.init_caches()
    await service_provider.gap_filling_module_loader.init(service_provider)

    yield postgres_db, service_provider

    await service_provider.shutdown_services()

    print("teardown_services")


@pytest.fixture(scope="function")
def create_brightway_test_database() -> str:
    """Creates a Brightway database in a temporary folder, and returns the path to that folder.

    The database is populated with just 2 activities (electricity production and fuel production) that link to
    3 biosphere emissions. Example from the book "The Computational Structure of Life Cycle Assessment" by
    Reinout Heijungs and Sangwon Suh.
    """
    # delete and create tmp db  directory
    edb_path = os.path.dirname(__file__) + "/tmp_brightway_database"
    if os.path.exists(edb_path):
        shutil.rmtree(edb_path)
    os.makedirs(edb_path)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    from bw2data import Database, Method, projects

    projects.set_current("Base")

    d = Database("biosphere3")
    d.write(biosphere3)
    d = Database("EDB")
    d.write(EDB)
    method_key = ("IPCC 2013 test importer", "climate change", "GWP 100a")
    m = Method(method_key)
    m.register()
    m.metadata["unit"] = "kg CO2-Eq"
    m.metadata["abbreviation"] = "ipcc-2013-test-abbreviation"
    m.metadata["description"] = "ipcc-2013-test-description"
    m.write(impact_assessment)

    # return absolute path to the database directory
    return os.path.abspath(edb_path)


@pytest.fixture(scope="function")
def brightway_test_database_new_unit(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_new_unit = copy.deepcopy(EDB)
    edb_new_unit[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["unit"] = "ml"
    from bw2data import Database

    return Database("EDB"), edb_new_unit


@pytest.fixture(scope="function")
def brightway_test_database_new_unit_and_location(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_new_unit_and_location = copy.deepcopy(EDB)
    edb_new_unit_and_location[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["unit"] = "ml"
    edb_new_unit_and_location[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["location"] = "New Location"
    from bw2data import Database

    return Database("EDB"), edb_new_unit_and_location


@pytest.fixture(scope="function")
def brightway_test_database_new_production_amount_unit_and_location(
    create_brightway_test_database: str,
) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_new_prod_amount_unit_and_location = copy.deepcopy(EDB)
    edb_new_prod_amount_unit_and_location[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["exchanges"][0]["amount"] += 10
    edb_new_prod_amount_unit_and_location[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["unit"] = "ml"
    edb_new_prod_amount_unit_and_location[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["location"] = "New Location"
    from bw2data import Database

    return Database("EDB"), edb_new_prod_amount_unit_and_location


@pytest.fixture(scope="function")
def brightway_test_database_modified_flow_amount(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_modified_flow = copy.deepcopy(EDB)
    edb_modified_flow[("EDB", "cdf21b8781dd46fc9479558be2f62175")]["exchanges"][1]["amount"] += 0.1
    from bw2data import Database

    return Database("EDB"), edb_modified_flow


@pytest.fixture(scope="function")
def brightway_test_database_modified_flow_input(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_modified_flow = copy.deepcopy(EDB)
    edb_modified_flow[("EDB", "cdf21b8781dd46fc9479558be2f62175")]["exchanges"][1]["input"] = (
        "biosphere3",
        "aa7cac3a-3625-41d4-bc54-33e2cf11ec46_copy1",
    )
    from bw2data import Database

    return Database("EDB"), edb_modified_flow


@pytest.fixture(scope="function")
def brightway_test_database_deleted_flow(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_deleted_flow = copy.deepcopy(EDB)
    edb_deleted_flow[("EDB", "588ab0c19f87449b9a79fbeb97f82826")]["exchanges"].pop(2)
    from bw2data import Database

    return Database("EDB"), edb_deleted_flow


@pytest.fixture(scope="function")
def brightway_test_database_added_flow(create_brightway_test_database: str) -> Tuple["Database", dict]:
    _ = create_brightway_test_database
    edb_added_flow = copy.deepcopy(EDB)
    edb_added_flow[("EDB", "cdf21b8781dd46fc9479558be2f62175")]["exchanges"].append(
        {
            "output": ("EDB", "cdf21b8781dd46fc9479558be2f62175"),
            "input": ("biosphere3", "88d06db9-59a1-4719-9174-afeb1fa4026a"),
            "amount": -50.0,
            "unit": "kilogram",
            "type": "biosphere",
        },
    )
    from bw2data import Database

    return Database("EDB"), edb_added_flow
