"Ecoinvent DB carrot calculation."
import asyncio
import logging
import os
import sys
import time
import uuid

import structlog

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.namespace import Namespace
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.mutation_serialization import mutation_to_dict
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import Orchestrator
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb
from inventory_importer.bw_import_controller import parse

# Set log level to error. Necessary when running the long test on EDB carrot
structlog.configure(
    cache_logger_on_first_use=True,
    wrapper_class=structlog.make_filtering_bound_logger(logging.ERROR),
)

E2E_SCHEMA = "test_e2e"


async def run() -> None:
    "Setup and run the calculation."
    # connect & reset db
    service_provider = ServiceProvider()
    postgres_db = PostgresDb(service_provider)
    service_provider.postgres_db = postgres_db
    await postgres_db.connect(schema=E2E_SCHEMA)
    await postgres_db.reset_db(schema=E2E_SCHEMA)

    # grab path to edb; it's the first script parameter
    edb_path = sys.argv[1]
    assert os.path.isdir(edb_path), (
        f"could not find test database at {edb_path}. It should point to a brightway2 "
        f"database folder with the EDB database."
    )

    # simulate command line arguments
    argv1 = ["--edb_path", edb_path, "--import_using_glossary"]
    args: Namespace = parse(argv1)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    # import after setting the environment variable:
    from inventory_importer.bw_importer import BwImporter  # noqa

    start_import = time.time()
    bw_importer = BwImporter()

    imported_activities_cnt = await bw_importer.import_bw_products(args)
    assert (
        imported_activities_cnt > 100
    ), f"expected at least 100 activities to be imported, got {imported_activities_cnt}"
    print(f"Importing of {imported_activities_cnt} activities took {round(time.time() - start_import)}s")

    glossary_service = GlossaryService(service_provider=service_provider)
    await glossary_service.init()

    graph_mgr = postgres_db.get_graph_mgr()

    start = time.time()
    print("starting calculation...")

    # create a recipe for the calculation; it will simply hold a carrot ingredient.
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            raw_input={
                "titles": [{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
                "author": "Eckart Witzigmann",
                "date": "2013-10-14",
                "location": "Zürich Schweiz",
                "instructions": [{"language": "de", "value": "Die Karotten gut kochen."}],
            },
        )
    )

    # create one ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_input={
                "id": "100100191",
                "type": "conceptual-ingredients",
                "names": [{"language": "de", "value": "Karotten"}],
                "amount": 1,  # FIXME this should be in grams. needs conversion in matrix_calculation_gfm.py
                "unit": "kg",
                "origin": "spain",
                "transport": "air",
                "production": {"value": "greenhouse", "language": "en"},
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # run orchestrator on recipe

    # create a new Calculation object
    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=recipe.uid)

    # first persist this calculation to db (without mutations and results yet):
    await postgres_db.get_calc_mgr().insert_calculation(calculation)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=calculation.root_node_uid,
        glossary_service=glossary_service,
    )

    # initialize orchestrator with the calculation graph:
    gfm_loader = GapFillingModuleLoader()
    await gfm_loader.init(service_provider)

    orchestrator = Orchestrator(
        calc_graph=calc_graph, glossary_service=glossary_service, gap_filling_module_loader=gfm_loader
    )

    # start running the orchestrator, which will internally call gap filling modules:
    await orchestrator.run()

    await postgres_db.get_calc_mgr().upsert_by_uid(calculation)

    lca_supply: dict[str, float] = calc_graph.get_root_node().lca_supply.get_data()
    assert lca_supply

    environmental_flows: dict[str, float] = calc_graph.get_root_node().environmental_flows.get_data()
    assert environmental_flows

    impact_assessment: dict[str, float] = calc_graph.get_root_node().impact_assessment.get_data()
    impact_assessment_amount = impact_assessment["amount"]
    assert abs(impact_assessment_amount - 0.14612553134476094 * 1000) < 0.0001 * impact_assessment_amount, (
        f"impact_assessment amount should be extremely close to Brightway value of 0.14612553134476094 * 1000"
        f" but was {impact_assessment_amount}"
    )
    # see https://github.com/Eaternity/eaternity-edb-data/blob/17eb96e7d06c7f39e6087e58b538b6e98853d004/prods/115-karotten-prod.json

    print(f"Mutation log size: {len(calc_graph.get_mutation_log())}")
    print(f"Calculation took {round(time.time() - start)}s")

    start_serialize = time.time()
    data_mutations = [mutation_to_dict(x, postgres_db.pg_term_mgr) for x in calc_graph.get_mutation_log()]
    print(f"Serialization of {len(data_mutations)} mutations took {round(time.time() - start_serialize)}s")

    await service_provider.shutdown_services()


asyncio.run(run())
