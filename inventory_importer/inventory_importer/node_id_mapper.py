import uuid
from typing import Tuple

from core.service.service_provider import ServiceLocator

BwKey = Tuple[str, str]


class NodeIdMapper:
    def __init__(self):
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider

        # store mapping to our new uuid's, to later link the nodes and not insert twice a node
        self.mapping_xid_to_uid: dict[str, uuid] = dict()

        # we need to store the new mappings of new nodes to be added separately in case the import of a process fails:
        self.mapping_xid_to_uid_staged: dict[str, uuid] = dict()

        self.mapping_xid_to_uid_staged_for_delete: set[str] = set()

    async def init_xid_cache(self, namespace_uid: str):
        self.mapping_xid_to_uid = await self.service_provider.postgres_db.product_mgr.get_all_id_mappings_of_namespace(
            namespace_uid=namespace_uid
        )

    @staticmethod
    def bw_key_to_xid(bw_key):
        xid = f"{bw_key[0]}_{bw_key[1]}"
        return xid

    def reset_staged_items(self):
        self.mapping_xid_to_uid_staged = dict()
        self.mapping_xid_to_uid_staged_for_delete = set()

    def commit_staged_items(self):
        self.mapping_xid_to_uid.update(self.mapping_xid_to_uid_staged)
        self.reset_staged_items()

    def __setitem__(self, bw_key_or_xid: str or BwKey, uid):
        if isinstance(bw_key_or_xid, str):
            self.mapping_xid_to_uid_staged[bw_key_or_xid] = uid
        else:
            xid = self.bw_key_to_xid(bw_key_or_xid)
            self.mapping_xid_to_uid_staged[xid] = uid

    def __getitem__(self, bw_key_or_xid: str or BwKey) -> uuid:
        if isinstance(bw_key_or_xid, str):
            return self._get_uid_by_xid(bw_key_or_xid)
        else:
            xid = self.bw_key_to_xid(bw_key_or_xid)
            return self._get_uid_by_xid(xid)

    def _get_uid_by_xid(self, xid: str) -> uuid:
        if xid in self.mapping_xid_to_uid:
            return self.mapping_xid_to_uid[xid]
        elif xid in self.mapping_xid_to_uid_staged:
            return self.mapping_xid_to_uid_staged[xid]
        else:
            raise KeyError(f"xid {xid} not found in NodeIdMapper")

    def commit_delete_mapping(self):
        for xid in self.mapping_xid_to_uid_staged_for_delete:
            if xid in self.mapping_xid_to_uid:
                del self.mapping_xid_to_uid[xid]
            elif xid in self.mapping_xid_to_uid_staged:
                del self.mapping_xid_to_uid_staged[xid]
        self.mapping_xid_to_uid_staged_for_delete.clear()

    def delete_mapping(self, xid):
        self.mapping_xid_to_uid_staged_for_delete.add(xid)

    def __contains__(self, item: str or BwKey):
        if isinstance(item, str):
            return (item in self.mapping_xid_to_uid) or (item in self.mapping_xid_to_uid_staged)
        else:
            xid = self.bw_key_to_xid(item)
            return (xid in self.mapping_xid_to_uid) or (xid in self.mapping_xid_to_uid_staged)

    def __len__(self):
        return len(self.mapping_xid_to_uid) + len(self.mapping_xid_to_uid_staged)
