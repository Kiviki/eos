from __future__ import print_function

import io
import os
import re
from zipfile import ZipFile

import google
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from structlog import get_logger

_SERVICE_ACCOUNT_FILE = "./secrets/service_account.json"
_SCOPES = [
    "https://www.googleapis.com/auth/devstorage.read_write",
    "https://www.googleapis.com/auth/spreadsheets",
    "https://www.googleapis.com/auth/drive",
]


logger = get_logger()


def download_from_gdrive(folder_id: str, edb_path: str):
    """download the brightway base project"""

    credentials, project = google.auth.load_credentials_from_file(filename=_SERVICE_ACCOUNT_FILE, scopes=_SCOPES)

    try:
        service = build("drive", "v3", credentials=credentials)

        logger.info(f"download from folder_id {folder_id}")
        query = f"'{folder_id}' in parents and name contains 'Base' and mimeType = 'application/zip'"

        results = service.files().list(q=query).execute()
        items = results.get("files", [])
        if not items:
            logger.error("No files found.")
            return

        file_to_use = items[0]

        os.makedirs(edb_path, exist_ok=True)

        if os.path.isfile(os.path.join(edb_path, file_to_use["name"])):
            logger.info(f"Skip downloading Base project because file already exists: {file_to_use['name']}")
        else:
            # there is a new base project zip file online that we need to download and unpack:
            base_path = os.path.join(edb_path, "Base")
            if os.path.isdir(base_path):
                raise Exception(
                    f"Base project already exists, but is outdated. Please delete the "
                    f"folder {base_path} and try again."
                )

            logger.info(f"file to download: {file_to_use['name']}")
            request = service.files().get_media(fileId=file_to_use["id"])
            zip_path = os.path.join(edb_path, file_to_use["name"])
            with io.FileIO(zip_path, mode="wb") as file:
                downloader = MediaIoBaseDownload(file, request)
                done = False
                while not done:
                    (_, done) = downloader.next_chunk()

            logger.info(f"Unpacking base project to {base_path}")
            myzip = ZipFile(file=zip_path)
            myzip.extractall(path=base_path)

        # now download the bw2package files and import them to update the Base project:
        query = f"'{folder_id}' in parents and name contains '_Full_EDB.bw2package'"
        results = service.files().list(q=query).execute()
        items = results.get("files", [])
        if not items:
            logger.error("No files found.")
            return

        # import only here so that environment variable is set before import
        import bw2io
        from bw2data import databases, projects

        projects.set_current("Base")

        # take only files with name starting with iso date:
        items_filtered = filter(lambda x: re.match(r"^\d{4}-\d{2}-\d{2}_Full_EDB.bw2package$", x["name"]), items)

        # sort by iso date prefix in the filename, so that we always take the latest up:
        items_sorted = sorted(items_filtered, key=lambda x: x["name"])

        # only import the file that was last modified:
        last_modified_file = items_sorted[-1]
        file_path = os.path.join(edb_path, last_modified_file["name"])
        if os.path.isfile(file_path):
            logger.info(f"Skip downloading bw2package because file already exists: {last_modified_file['name']}")
        else:
            logger.info(f"file to download: {last_modified_file['name']}")
            request = service.files().get_media(fileId=last_modified_file["id"])
            with io.FileIO(file_path, mode="wb") as file:
                downloader = MediaIoBaseDownload(file, request)
                done = False
                while not done:
                    (_, done) = downloader.next_chunk()

            logger.info(f"importing {file_path}")

            bw_data = bw2io.package.BW2Package.load_file(file_path)
            logger.info(f"start importing {list(bw_data[0]['data'].keys())}")

            projects.set_current("Base")

            # unfortunately, we need to delete the old EDB database before importing the new one and clean/flush,
            # due to a bug in bw2data, which would otherwise execute VACUUM within an ongoing transaction, which is
            # not allowed in sqlite.
            for db in bw_data:
                del databases[db["name"]]
            databases.clean()
            databases.flush()

            bw2io.package.BW2Package.import_file(file_path)

    except HttpError as error:
        print(f"An error occurred: {error}")
