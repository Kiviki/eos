import time

from structlog import get_logger

from core.domain.matching_item import MatchingItem
from core.domain.term import Term
from core.service.service_provider import ServiceLocator

logger = get_logger()


class GlossaryImporter:
    """Class for upserting glossary and matchings."""

    def __init__(self) -> None:
        self.service_provider = ServiceLocator().service_provider

    async def upsert_glossary_and_matching(self, schema: str = "public") -> None:
        """Upsert glossary terms."""
        start_time = time.time()
        upserted_matchings: list[MatchingItem] = []
        upserted_terms: list[Term] = []
        await self.service_provider.postgres_db.upsert_glossary_and_matching(
            schema=schema, upserted_matchings=upserted_matchings, upserted_terms=upserted_terms
        )
        await self.service_provider.glossary_service.update_cache_for_many_terms(upserted_terms)
        await self.service_provider.matching_service.update_cache_for_many_matching_items(upserted_matchings)

        end_time = time.time()
        logger.info(f"Upserted the glossary and the matching, took {end_time - start_time:.2f}s")
