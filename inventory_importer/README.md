# Eaternity EOS -- Inventory Importer

Imports LCA inventory data from Brightway into EOS.

Structured as a separate/independent Docker service, in order for EOS not to depend on Brightway
(because that would include too many deps and force us to use Conda in EOS).

### Get EDB data

* download the most recent EDB-export from
  our [google drive](https://drive.google.com/drive/u/0/folders/1QGgJPHuz-m7MA5zIkerN1b2EsId8dnIu).
  It is the most recent file like `Base.xxxxxxxxxxxxxxxxxxxxxxx.zip`
* unzip it in a folder of your choice. This will become your local Brightway directory
* The parameter `--edb_path` of the inventory importer should point to that unzipped directory.

### Install dependencies

* `cd inventory_importer`
* `poetry install`
* `cd ..`
* To import LCIA data, run: 
    * `poetry run python ./inventory_importer/inventory_importer/bw_import_controller.py --edb_path ../Base.095a1b43effec73955e31e790438de49 --import_using_glossary`
* To import all impact assessment impact assessment data (e.g., IPCC 2013 GWP 100), run:
    * `poetry run python ./inventory_importer/inventory_importer/bw_import_controller.py --edb_path ../Base.095a1b43effec73955e31e790438de49 --import_all_impact_assessments`
* To import GADM data, run: 
    * `poetry run python ./inventory_importer/inventory_importer/bw_import_controller.py --gfm_import_data`
* To import only specific GADM countries, run: 
    * `poetry run python ./inventory_importer/inventory_importer/bw_import_controller.py --gfm_import_data --countries_alpha3 CHE ESP DEU`

## Deprecated for now (TODO: need to update Dockerfile):

### Build container

    docker compose --profile inventory_importer up  inventory_importer

This will also start the postgres container

### Import data

Replace {PATH TO YOUR LOCAL EDB FOLDER} with the path above

Make sure you have initialized your EOS database first.

Then run:

    # we are using 'docker compose' to have access to the right environment (in particular: postgres db port, etc)
    # we passin the folder we created above as a volume
    # using Anaconda's 'bw2' environment, that contains brightway and EOS
    # finally, executing bw_import_database.py to import Inventory data into the docker postgres service 
    docker compose --profile inventory_importer run -v {PATH TO YOUR LOCAL EDB FOLDER}:/eos/data/edb_export inventory_importer conda run -n bw2 python -u bw_import_controller.py --edb_path "/eos/data/edb_export" --seed_sample "market for carrot (w/o transport)" "market for potato (w/o transport)" "market for soybean, organic" "tofu production, organic"

It should take over 10 minutes to import the data...

### Working with Brightway directly in PyCharm

install Brightway in conda

    conda create -n eos_bw -c conda-forge -c cmutel brightway2_nosolver
    conda activate eos_bw

install poetry

    curl -sSL https://install.python-poetry.org | python3 -

Add `export PATH="/Users/ren/.local/bin:$PATH"` to your shell configuration file.

Alternatively, you can call Poetry explicitly with `/Users/ren/.local/bin/poetry`.

Finally, install the dependencies (use `poetry show` to print the up-to-date list)

    pip install pytest requests autopep8 debugpy httpx asyncpg pytest-asyncio python-dotenv aiohttp fastapi uvicorn structlog numpy scipy
