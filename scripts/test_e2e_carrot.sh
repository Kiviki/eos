#!/bin/bash

# this test uses a kitchen that is automatically created 
# upon creating a new namespace

HOST_API="http://localhost:8040/v2"
HOST_LEGACY_API="http://localhost:8050/api"
CUST_NAMESPACE="f259f6eb-8d1e-4925-9207-4ef3442c902c"
CUST_NAME="name01"
KITCHEN_ID="kitchen01"
RECIPE_ID="recipe01"

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

echo "using EATERNITY_TOKEN: $EATERNITY_TOKEN"

echo -e "\n1) start adding customer..."
content=$(curl -s --location --request PUT "${HOST_API}/batch/customers/${CUST_NAMESPACE}" \
--header 'Content-Type: application/json' \
-u ${EATERNITY_TOKEN}: \
--data-raw '{
    "customer": {
        "name": "'${CUST_NAME}'",
        "realCustomer": false,
        "comparedToOthers": true,
        "sharedWithOthers": true,
        "kitchen-blacklist": [],
        "menu-line-blacklist": [],
        "product-blacklist": [],
        "recipe-blacklist": [],
        "normalizeApiByFU": false,
        "delegateUser": false
    }
}')

# Check if content is empty
if [ -z "$content" ]; then
  echo "Error: No content received from the API call."
  exit 1
fi
echo $content

CUSTOMER_TOKEN=$( echo $content | grep -o '"auth_token":"[^"]*' | grep -o '[^"]*$' | tr -d '\n' )
echo "use newly created customer namespace ${CUST_NAMESPACE}"
echo "use CUSTOMER_TOKEN ${CUSTOMER_TOKEN}"

echo -e "\n2) start adding kitchen ${KITCHEN_ID}"
content=$(curl --location --request PUT "${HOST_LEGACY_API}/kitchens/${KITCHEN_ID}" \
 --header 'Content-Type: Application/Json' \
 -u ${CUSTOMER_TOKEN}: \
 --data '{
	"kitchen": {
        "name": "carrot-making kitchen",
        "location": "Switzerland"
    }
 }')
echo $content

echo -e "\n3) start adding recipe ${RECIPE_ID}"
curl --location --request PUT "${HOST_LEGACY_API}/kitchens/${KITCHEN_ID}/recipes/${RECIPE_ID}" \
 --header 'Content-Type: application/json' \
  -u ${CUSTOMER_TOKEN}: \
 --data-raw '{
  "recipe": {
    "titles": [
      {
        "language": "de",
        "value": "Karrotenkuchen"
      }
    ],
    "author": "Eckart Witzigmann",
    "date": "2013-10-14",
    "location": "Zürich Schweiz",
    "servings": 10,
    "instructions": [
      {
        "language": "de",
        "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."
      },
      {
        "language": "en",
        "value": "Bake the carrot cake in the oven and enjoy while still hot."
      }
    ],
    "ingredients": [
      {
        "id": "100100894",
        "type": "conceptual-ingredients",
        "names": [
          {
            "language": "de",
            "value": "Karotten"
          }
        ],
        "amount": 1000,
        "unit": "gram",
        "origin": "france",
        "transport": "ground",
        "production": "organic",
        "processing": "",
        "conservation": "fresh",
        "packaging": ""
      }
    ]
  }
}' | python -m json.tool
