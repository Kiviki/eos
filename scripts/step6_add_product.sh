#!/bin/bash

HOST="http://localhost:8050"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
PRODUCT_ID="product01"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start adding recipe ${PRODUCT_ID}"
curl -i --location --request PUT ${HOST}'/api/products' \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{   "product": {
        "id": "'${PRODUCT_ID}'",
        "date": "2020-02-02",
        "gtin": "00123456789023",
        "names": [
          {
            "language": "de",
            "value": "Karottenpuree"
          }
        ],
        "amount": 20,
        "unit": "gram",
        "producer": "hipp",
        "ingredients-declaration": "Karotten, Tomaten",
        "nutrient-values": [{
            "energy-kcal": 200,
            "fat-gram": 12.3,
            "saturated-fat-gram": 2.5,
            "carbohydrates-gram": 8.4,
            "sucrose-gram": 3.2,
            "protein-gram": 2.2,
            "sodium-chloride-gram": 0.3
          }],
        "origin": "paris, frankreich",
        "transport": "ground",
        "production": "standard",
        "processing": "raw",
        "conservation": "fresh",
        "packaging": "none"
      }
}'
