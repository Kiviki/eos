#!/bin/bash

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

echo "restoring database ${POSTGRES_DB}..."

export PGPASSWORD=$POSTGRES_PASSWORD
dropdb -h localhost -p 5444 -U $POSTGRES_USER $POSTGRES_DB
createdb -h localhost -p 5444 -U $POSTGRES_USER $POSTGRES_DB
gunzip -c database_backup.sql.gz | psql -h localhost -p 5444 -U $POSTGRES_USER $POSTGRES_DB

echo "restore done"
