#!/bin/bash

HOST="http://localhost:8040"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
RECIPE_ID="recipe02"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start getting recipe ${RECIPE_ID}"
curl -i --location --request GET ${HOST}'/v2/calculation/'${RECIPE_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}"