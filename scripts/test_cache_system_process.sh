#!/bin/bash

HOST_API="http://localhost:8040"
if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

SYSTEM_PROCESS_NODE_ID="EDB_437443338484ca565ae5a832d81fcc52_copy1"
echo -e "\nstart creating system process cache of ${SYSTEM_PROCESS_NODE_ID}"

curl -s --location \
 --request GET ${HOST_API}'/v2/calculation/node-xid/'${SYSTEM_PROCESS_NODE_ID}'?save_as_system_process=true' \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${EATERNITY_AUTH_KEY}" | poetry run python -m json.tool
