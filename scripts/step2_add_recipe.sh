#!/bin/bash

HOST="http://localhost:8040"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
RECIPE_ID="recipe01"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start adding recipe ${RECIPE_ID}"
curl -i --location --request PUT ${HOST}'/v2/calculation/'${RECIPE_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
  "recipe": {
    "titles": [
      {
        "language": "de",
        "value": "Kürbisrisotto"
      }
    ],
    "author": "Eckart Witzigmann",
    "date": "2013-10-14",
    "location": "Zürich Schweiz",
    "servings": 140,
    "instructions": [
      {
        "language": "de",
        "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."
      },
      {
        "language": "en",
        "value": "Bake the carrot cake in the oven and enjoy while still hot."
      }
    ],
    "ingredients": [
      {
        "id": "100100191",
        "type": "conceptual-ingredients",
        "names": [
          {
            "language": "de",
            "value": "Tomaten"
          }
        ],
        "amount": 150,
        "unit": "gram",
        "origin": "spain",
        "transport": "air",
        "production": "greenhouse",
        "processing": "raw",
        "conservation": "fresh",
        "packaging": "plastic"
      },
      {
        "id": "100100894",
        "type": "conceptual-ingredients",
        "names": [
          {
            "language": "de",
            "value": "Zwiebeln"
          }
        ],
        "amount": 78,
        "unit": "gram",
        "origin": "france",
        "transport": "ground",
        "production": "organic",
        "processing": "",
        "conservation": "dried",
        "packaging": ""
      }
    ]
  }
}'
