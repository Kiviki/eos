#!/bin/bash

HOST="http://localhost:8050"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
SUPPLY_ID="supply01"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start adding recipe ${RECIPE_ID}"
curl -i --location --request PUT ${HOST}'/api/supplies/'${SUPPLY_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{   "supply": {
        "supplier": "Vegetable Supplies Ltd.",
        "supplier-id": "el3i5y-2in5y-2hbllll01",
        "invoice-id": "778888800000001",
        "supply-date": "2013-06-01",
        "ingredients": [
        {
            "id": "100100191",
            "type": "conceptual-ingredients",
            "names":  [
                {
                    "language": "de",
                    "value": "Tomaten"
                }],
            "amount": 150,
            "unit": "gram",
            "origin": "spain",
            "transport": "air",
            "production": "greenhouse",
            "processing": "raw",
            "conservation": "fresh",
            "packaging": "plastic",
            "ingredients-declaration": "Hähnchenbrustfilet (53 %), Panade (28%) (Weizenmehl, Wasser, modifizierte Weizen-stärke, Weizenstärke, Speisesalz, Gewürze, Hefe), Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) (Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz), Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
            "nutrient-values": {
               "energy-kjoule":500,
               "fat-gram":9001,
               "protein-gram": 33,
               "vitamine-k-microgram": 0.21,
               "vitamine-a1-microgram": 0.12345,
               "manganese-microgram":0,
               "fibers-gram":1111
            }
        }
        ]
    }
}'
