#!/bin/bash

HOST="http://localhost:8040"
CUST_NAMESPACE="customer01"
CUST_NAME="name01"
KITCHEN_ID="kitchen01"

echo "start adding customer..."

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
fi
EATERNITY_AUTH_KEY=$(echo -n $EATERNITY_USERNAME | base64)

echo $EATERNITY_AUTH_KEY

content=$(curl -i --location --request PUT "${HOST}/v2/batch/customers/${CUST_NAMESPACE}" \
--header 'Content-Type: application/json' \
--header "Authorization: Basic ${EATERNITY_AUTH_KEY}" \
--data-raw '{
    "customer": {
        "name": "'${CUST_NAME}'",
        "realCustomer": false,
        "comparedToOthers": true,
        "sharedWithOthers": true,
        "kitchen-blacklist": [],
        "menu-line-blacklist": [],
        "product-blacklist": [],
        "recipe-blacklist": [],
        "normalizeApiByFU": false,
        "delegateUser": false
    }
}')

echo $content

AUTH_KEY=$( echo $content | grep -o '"auth_token":"[^"]*' | grep -o '[^"]*$' | tr -d '\n' | base64 )

#AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "use newly created customer namespace ${CUST_NAMESPACE}"
echo "use auth key ${AUTH_KEY}"
echo "start adding kitchen ${KITCHEN_ID}"

curl -i --location --request PUT ${HOST}'/api/kitchens/'${KITCHEN_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
     "kitchen": {
         "name": "'"$KITCHEN_ID"'",
         "location": "schweiz"
     }
 }'
