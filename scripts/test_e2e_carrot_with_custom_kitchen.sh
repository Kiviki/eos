#!/bin/bash

# this test uses a kitchen that is created manually
# after creating a new namespace

HOST_API="http://localhost:8040"
HOST_LEGACY_API="http://localhost:8050"
CUST_NAMESPACE="77f7416f-9a3d-49d0-a53a-cc328e18c153"
CUST_NAME="name01"
KITCHEN_ID="kitchen01"

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

echo "using superuser auth key: $EATERNITY_AUTH_KEY"

echo -e "\n1) start adding customer..."
content=$(curl -s --location --request PUT "${HOST_API}/v2/batch/customers/${CUST_NAMESPACE}" \
--header 'Content-Type: application/json' \
--header "Authorization: Basic ${EATERNITY_AUTH_KEY}" \
--data-raw '{
    "customer": {
        "name": "'${CUST_NAME}'",
        "realCustomer": false,
        "comparedToOthers": true,
        "sharedWithOthers": true,
        "kitchen-blacklist": [],
        "menu-line-blacklist": [],
        "product-blacklist": [],
        "recipe-blacklist": [],
        "normalizeApiByFU": false,
        "delegateUser": false
    }
}')

echo $content

AUTH_KEY=$( echo $content | grep -o '"auth_token":"[^"]*' | grep -o '[^"]*$' | tr -d '\n' )
echo -e "\nuse newly created customer namespace ${CUST_NAMESPACE} with auth key ${AUTH_KEY}"

echo -e "\n2) start getting UUID of default access group of new namespace"

content=$(curl -s --location --request GET "${HOST_API}/v2/access-groups/" \
--header 'Content-Type: application/json' \
--header "Authorization: Basic ${AUTH_KEY}" )

echo $content

DEFAULT_ACCESS_GROUP_UID=$( echo $content | grep -o '"access_group_uid":"[^"]*' | grep -o '[^"]*$' | tr -d '\n' )

USER_ADMIN_ID="309e6d6e-f237-42b0-9498-fe90b9c04a3a"
echo -e "\n3) start adding user ${USER_ADMIN_ID}"

content=$(curl -s --location --request PUT "${HOST_API}/v2/users/${USER_ADMIN_ID}" \
--header 'Content-Type: application/json' \
--header "Authorization: Basic ${EATERNITY_AUTH_KEY}" \
--data-raw '{
    "user": {
        "email": "afsdfasdf@example.com",
        "is_superuser": "True"
    }
}')

echo $content

AUTH_KEY=$( echo $content | grep -o '"auth_token":"[^"]*' | grep -o '[^"]*$' | tr -d '\n' )
echo "use newly created admin user ${USER_ADMIN_ID} with auth key ${AUTH_KEY}"

echo -e "\n4) set default access group of new namespace as their default access group"

content=$(curl -s --location --request PUT "${HOST_API}/v2/users/${USER_ADMIN_ID}" \
--header 'Content-Type: application/json' \
--header "Authorization: Basic ${EATERNITY_AUTH_KEY}" \
--data-raw '{
    "user": {
        "email": "afsdfasdf@example.com",
        "is_superuser": "True"
    },
    "legacy_api_default_access_group_uid": "'${DEFAULT_ACCESS_GROUP_UID}'"
}')

echo $content

echo -e "\n5) start adding kitchen ${KITCHEN_ID}"

curl -s --location --request POST ${HOST_LEGACY_API}'/api/kitchens/' \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
     "kitchen": {
         "xid": "'"$KITCHEN_ID"'",
         "name": "carrot-making kitchen",
         "location": "schweiz"
     }
 }' | python -m json.tool

RECIPE_ID="recipe01"

echo -e "\n6) start adding recipe ${RECIPE_ID}"

curl -s --location --request PUT ${HOST_LEGACY_API}'/api/kitchens/'${KITCHEN_ID}'/recipes/'${RECIPE_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
  "recipe": {
    "titles": [
      {
        "language": "de",
        "value": "Karrotenkuchen"
      }
    ],
    "author": "Eckart Witzigmann",
    "date": "2013-10-14",
    "location": "Zürich Schweiz",
    "servings": 140,
    "instructions": [
      {
        "language": "de",
        "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."
      },
      {
        "language": "en",
        "value": "Bake the carrot cake in the oven and enjoy while still hot."
      }
    ],
    "ingredients": [
      {
        "id": "100100894",
        "type": "conceptual-ingredients",
        "names": [
          {
            "language": "de",
            "value": "Karotten"
          }
        ],
        "amount": 100,
        "unit": "gram",
        "origin": "france",
        "transport": "ground",
        "production": "organic",
        "processing": "",
        "conservation": "dried",
        "packaging": ""
      }
    ]
  }
}' | python -m json.tool
