"""Rainforest gap filling module."""
import uuid

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.location_util.countries import iso_3166_map_3_to_2_letter
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityPackageProp, RainforestCriticalProductsProp, ReferencelessQuantityProp
from core.domain.props.location_prop import LocationSourceEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class RainforestGapFillingWorker(AbstractGapFillingWorker):
    """Rainforest gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule Rainforest gap filling worker."""
        # Schedule on Brightway nodes with a parent that has a product_name
        if (
            isinstance(self.node, ModeledActivityNode)
            and self.node.get_parent_nodes()
            and any(
                isinstance(parent_node, FoodProductFlowNode) and parent_node.product_name
                for parent_node in self.node.get_parent_nodes()
            )
        ):
            return True
        else:
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether Rainforest gap filling worker can be executed."""
        global_gfm_state = self.get_global_gfm_state()

        # Ensure that the OriginGapFillingWorker has separated out ingredients with different origins.
        if global_gfm_state.get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Rainforest] wait for OriginGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute Rainforest gap filling worker."""
        parent_flows = self.node.get_parent_nodes()
        for parent_flow in parent_flows:
            if isinstance(parent_flow, FoodProductFlowNode) and parent_flow.product_name:
                parent_flow_product_name_terms = [prop.get_term() for prop in parent_flow.product_name.terms]

                if any(not term for term in parent_flow_product_name_terms):
                    logger.warn(
                        "Not all FoodEx2 Terms are well-defined for this node.",
                        node_uid=self.node.uid,
                    )
                    continue
                else:
                    certified = False
                    rainforest_specified = False

                    (
                        matched_certified_term_xids,
                        certified_term_containing_node,
                    ) = parent_flow.get_glossary_tag_by_inheritance(self.gfm_factory.certification_term_xids)

                    if (
                        matched_certified_term_xids
                        and certified_term_containing_node.product_name == parent_flow.product_name
                    ):
                        for label_xid in matched_certified_term_xids:
                            if label_xid == "EOS_rainforest_conservation_certified":
                                certified = True
                                rainforest_specified = True

                            if label_xid == "EOS_certified_rainforest_not_specified":
                                # Certified but not specifically for rainforest conservation.
                                # Therefore, added to food categories as certified but still critical.
                                certified = True

                    glossary_link_service = self.gfm_factory.service_provider.glossary_link_service
                    critical_product_content = await glossary_link_service.load_term_critical_product_content(
                        parent_flow_product_name_terms
                    )
                    if not critical_product_content:
                        soy_content_per_100g = 0.0
                        palm_oil_content_per_100g = 0.0
                    else:
                        soy_content_per_100g = critical_product_content.data.get("soy", 0.0)
                        palm_oil_content_per_100g = critical_product_content.data.get("palm-oil", 0.0)

                    locations, _ = parent_flow.get_prop_by_inheritance("flow_location")
                    flow_country_codes = [location.country_code for location in locations]
                    sources = [location.source for location in locations]

                    food_categories = parent_flow.food_categories

                    if (not flow_country_codes) or self.is_fish(food_categories):
                        if not flow_country_codes:
                            warning_msg = (
                                f"No location found for product {parent_flow_product_name_terms}."
                                f"Assuming that the critical product comes from a problematic country."
                            )
                            logger.warn(warning_msg)
                            calc_graph.set_data_errors_log_entry(warning_msg)
                        critical_amount_per_100g = soy_content_per_100g + palm_oil_content_per_100g
                    else:
                        critical_amount_per_100g = palm_oil_content_per_100g
                        is_beef_pork_chicken_egg_milk = self.is_beef_pork_chicken_egg_milk(food_categories)
                        for flow_country_code, source in zip(flow_country_codes, sources):
                            if flow_country_code and len(flow_country_code) == 3:
                                flow_country_code = iso_3166_map_3_to_2_letter(flow_country_code)

                            if not flow_country_code or not source:
                                critical_amount_per_100g += soy_content_per_100g / len(flow_country_codes)
                            elif source and source == LocationSourceEnum.fao_stat:
                                critical_amount_per_100g += soy_content_per_100g / len(flow_country_codes)
                            else:
                                if is_beef_pork_chicken_egg_milk:
                                    if not flow_country_code == "CH":
                                        critical_amount_per_100g += soy_content_per_100g / len(flow_country_codes)
                                else:
                                    if flow_country_code in (
                                        "BR",
                                        "AR",
                                        "CN",
                                    ):  # Todo add more "tropical countries."
                                        critical_amount_per_100g += soy_content_per_100g / len(flow_country_codes)

                    qty_prop = ReferencelessQuantityProp(
                        value=critical_amount_per_100g,
                        unit_term_uid=self.gfm_factory.gram_term.uid,
                    )

                    if certified and rainforest_specified:
                        quantities = {self.gfm_factory.rainforest_conservation_certified.uid: qty_prop}
                    elif certified:
                        quantities = {self.gfm_factory.certified_rainforest_not_specified.uid: qty_prop}
                    else:
                        quantities = {self.gfm_factory.not_certified_for_rainforest.uid: qty_prop}

                    rainforest_critical_products = RainforestCriticalProductsProp(
                        quantities=quantities, for_reference=ReferenceAmountEnum.amount_for_100g
                    )

                    await calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=parent_flow.uid,
                            prop_name="rainforest_critical_products",
                            prop=rainforest_critical_products,
                        )
                    )

    def is_fish(self, food_categories: QuantityPackageProp) -> bool:
        """Determine whether the food product is fish."""
        if not food_categories:
            return False
        for key, qty in food_categories.quantities.items():
            if key == self.gfm_factory.fish_term.uid and qty.value > 0.0:
                return True
        return False

    def is_beef_pork_chicken_egg_milk(self, food_categories: QuantityPackageProp) -> bool:
        """Determine whether the food product is one of beef, pork, chicken, egg, and milk."""
        if not food_categories:
            return False
        for key, qty in food_categories.quantities.items():
            if key in self.gfm_factory.beef_pork_chicken_egg_milk_uids and qty.value > 0.0:
                return True
        return False


class RainforestGapFillingFactory(AbstractGapFillingFactory):
    """Rainforest gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        self.rainforest_conservation_certified = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_rainforest_conservation_certified", access_group_uid)
        ]
        self.certified_rainforest_not_specified = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_certified_rainforest_not_specified", access_group_uid)
        ]
        self.not_certified_for_rainforest = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_not_certified_for_rainforest", access_group_uid)
        ]
        self.certification_term_xids = frozenset(
            {
                self.not_certified_for_rainforest.xid,
                self.rainforest_conservation_certified.xid,
                self.certified_rainforest_not_specified.xid,
            }
        )
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_gram", access_group_uid)]
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")

        self.fish_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_Fish-and-seafood", access_group_uid)
        ]

        self.beef_pork_chicken_egg_milk_uids: set[uuid.UUID] = set()
        for xid in ("EOS_Beef-and-veal", "EOS_Poultry", "EOS_Eggs", "EOS_Dairy"):
            term = self.service_provider.glossary_service.terms_by_xid_ag_uid[(xid, access_group_uid)]
            self.beef_pork_chicken_egg_milk_uids.add(term.uid)

    async def init_cache(self) -> None:
        """Initialize cache required for Rainforest gap filling factory."""
        pass

    def spawn_worker(self, node: Node) -> RainforestGapFillingWorker:
        """Spawn Rainforest gap filling worker."""
        return RainforestGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = RainforestGapFillingFactory
