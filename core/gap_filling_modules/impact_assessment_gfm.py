"""Impact assessment gap filling module."""

import re
import sys
import time
import uuid
from typing import Optional
from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.daily_food_unit_gfm import DailyFoodUnitGapFillingWorker
from gap_filling_modules.matrix_calculation_gfm import MatrixCalculationGapFillingWorker
from gap_filling_modules.vitascore_gfm import VitascoreGapFillingWorker
from gap_filling_modules.water_scarcity_gfm import SCARCE_WATER_CONSUMPTION_XID
from structlog import get_logger

from core.domain.nodes import (
    ElementaryResourceEmissionNode,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    ModeledActivityNode,
    SupplySheetActivityNode,
)
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.domain.util import get_flow_amount, get_production_amount
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100, IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class ImpactAssessmentGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "ImpactAssessmentGapFillingFactory"):
        """Computes LCIA using GWP100 AR5."""
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Only run on activity node."""
        if self.node.uid == self.node.get_calculation().child_of_root_node_uid:
            if isinstance(
                self.node,
                (FoodProcessingActivityNode, ModeledActivityNode, SupplySheetActivityNode),
            ):
                return True
            else:
                return False
        elif isinstance(self.node, ActivityNode):  # activity node
            if isinstance(self.node, ElementaryResourceEmissionNode):
                logger.debug("[ImpactAssessment] on an emission --> not scheduled.")
                return False
            return True
        else:
            logger.debug("[ImpactAssessment] not on an activity node --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        # return GapFillingWorkerStatusEnum.cancel
        global_gfm_state = self.get_global_gfm_state()
        if global_gfm_state.get(MatrixCalculationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for MatrixCalculationGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        # run impact assessment calculation after other calculations (daily food units and vita score).
        # These were only added to make test_calc_service pass (to pass the check to see if ImpactAssessmentGFM is
        # the author of the last PropMutation.
        if global_gfm_state.get(DailyFoodUnitGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for DailyFoodUnitGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        if global_gfm_state.get(VitascoreGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for VitaScoreGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        node = self.node
        # node is a root node and a FlowNode use the first subnode, this should be an activity
        if isinstance(self.node, FoodProductFlowNode) and not self.node.get_parent_nodes():
            node = self.node.get_sub_nodes()[0]
        if isinstance(node, ActivityNode) and node.environmental_flows is None:
            logger.debug("[ImpactAssessment] no environmental_flows --> cancel")
            return GapFillingWorkerStatusEnum.cancel
        else:
            logger.debug("[ImpactAssessment] MatrixCalculation has been performed on the recipe --> can_run_now")
            return GapFillingWorkerStatusEnum.ready

    async def _env_node_uid_to_biosphere_key(self, calc_graph: CalcGraph, node_uid: UUID) -> str:
        env_flow_node: Node = calc_graph.get_node_by_uid(node_uid)
        if env_flow_node is None:
            env_flow_node = await calc_graph.service_provider.node_service.find_by_uid(node_uid)
        return self.gfm_factory.service_provider.product_service.uid_to_xid_mappings[env_flow_node.uid].removeprefix(
            "biosphere3_"
        )

    def _get_char_factor(self, impact_assessment: str, biosphere_key: str) -> float | None:
        char_method_xid = self.gfm_factory.impact_assessment_name_to_term[impact_assessment].xid
        try:
            char_factor: ReferencelessQuantityProp | None = self.gfm_factory.cache_characterization_factors[
                char_method_xid
            ]["data"].get(biosphere_key, None)
        except KeyError:
            logger.debug("Outdated impact assessments. Rerun brightway import with --import_all_impact_assessments")
            old_char_method_xid = re.sub(" ", "_", impact_assessment).lower()
            char_factor: ReferencelessQuantityProp | None = self.gfm_factory.cache_characterization_factors[
                old_char_method_xid
            ]["data"].get(biosphere_key, None)

        if char_factor and char_factor.value:
            return char_factor.value
        else:
            return None

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug("start running gap-filling-module ImpactAssessmentGFM...")

        environmental_flows = self.node.environmental_flows

        requested_impact_assessments = calc_graph.get_requested_impact_assessments()

        flow_by_biosphere_key: dict[str, float] = {
            await self._env_node_uid_to_biosphere_key(calc_graph, node_uid): flow
            for node_uid, flow in environmental_flows.flow_quantities.items()
        }

        impact_assessments: dict[str, float] = {
            char_method: sum(
                [
                    flow * factor
                    for biosphere_key, flow in flow_by_biosphere_key.items()
                    if (factor := self._get_char_factor(char_method, biosphere_key)) is not None
                    and biosphere_key != SCARCE_WATER_CONSUMPTION_XID
                ]
            )
            for char_method in requested_impact_assessments
        }

        scarce_water_consumption = -sum(
            [
                flow
                for biosphere_key, flow in flow_by_biosphere_key.items()
                if biosphere_key == SCARCE_WATER_CONSUMPTION_XID
            ]
        )

        logger.debug("create PropMutation to add impact_assessments ", impact_assessment=impact_assessments)

        impact_assessment_for_production_amount: dict[UUID, ReferencelessQuantityProp] = {}
        flow_impact_assessment_for_local_flow: dict[UUID, dict[UUID, ReferencelessQuantityProp]] = {
            parent_flow.uid: {} for parent_flow in self.node.get_parent_nodes()
        }
        for method, quantity in impact_assessments.items():
            impact_assessment_term = self.gfm_factory.impact_assessment_name_to_term[method]
            try:
                char_unit_term_uid = self.gfm_factory.cache_characterization_factors[impact_assessment_term.xid].get(
                    "unit"
                )
            except KeyError:
                old_char_method_xid = re.sub(" ", "_", impact_assessment_term.name).lower()
                char_unit_term_uid = self.gfm_factory.cache_characterization_factors[old_char_method_xid].get("unit")

            impact_assessment_for_production_amount[
                impact_assessment_term.uid
            ] = ReferencelessQuantityProp.unvalidated_construct(value=float(quantity), unit_term_uid=char_unit_term_uid)

            # we also add the impact_assessment to each parent flow node
            this_production_amount = get_production_amount(self.node)
            for parent_flow in self.node.get_parent_nodes():
                flow_amount = get_flow_amount(parent_flow)

                flow_impact_assessment_for_local_flow[parent_flow.uid][
                    impact_assessment_term.uid
                ] = ReferencelessQuantityProp.unvalidated_construct(
                    value=float(quantity * flow_amount / this_production_amount), unit_term_uid=char_unit_term_uid
                )

        prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="impact_assessment",
            prop=QuantityPackageProp.unvalidated_construct(
                quantities=impact_assessment_for_production_amount,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        await calc_graph.apply_mutation(prop_mutation)

        scarce_water_consumption_prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="scarce_water_consumption",
            prop=QuantityProp(
                value=scarce_water_consumption,
                unit_term_uid=self.gfm_factory.liter_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        await calc_graph.apply_mutation(scarce_water_consumption_prop_mutation)

        for parent_flow in self.node.get_parent_nodes():
            parent_node_prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=parent_flow.uid,
                prop_name="impact_assessment",
                prop=QuantityPackageProp.unvalidated_construct(
                    quantities=flow_impact_assessment_for_local_flow[parent_flow.uid],
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                ),
            )
            await calc_graph.apply_mutation(parent_node_prop_mutation)

            parent_scarce_water_consumption_prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=parent_flow.uid,
                prop_name="scarce_water_consumption",
                prop=QuantityProp(
                    value=scarce_water_consumption * get_flow_amount(parent_flow) / get_production_amount(self.node),
                    unit_term_uid=self.gfm_factory.liter_term.uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                ),
            )
            await calc_graph.apply_mutation(parent_scarce_water_consumption_prop_mutation)


class ImpactAssessmentGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        # we cannot use a GfmCacheService instance here,
        # since original impact_assessment data is often >200 kilobytes in size
        # and GfmCacheService uses deepcopy for cache retrieval, which leads to huge performance drop

        # Now load all available impact assessment factors as we compute impact assessment according to the request.
        # This should not be detrimental to the performance as it is only executed once by gap filling module loader.
        self.cache_characterization_factors: dict[str, dict[str, str | dict]] = {}
        self.impact_assessment_name_to_term: dict[str, Term] = {}
        self.unit_terms_ag_uid: Optional[UUID] = None
        self.dimensionless_unit_uid: Optional[UUID] = None
        self.liter_term: Optional[Term] = None

    async def init_cache(self) -> None:
        logger.debug("start filling cache of ImpactAssessmentGFM with impact assessment factors...")
        start_time = time.process_time()

        cached_impact_assessments = await self.postgres_db.get_gfm_cache_mgr().get_prefill_cache_entries(
            "ImpactAssessmentGapFillingWorker",
            5000,
        )

        self.unit_terms_ag_uid = self.service_provider.glossary_service.root_subterms.get("EOS_units").access_group_uid
        self.dimensionless_unit_uid = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_dimensionless", self.unit_terms_ag_uid)
        ].uid
        self.liter_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_liter", self.unit_terms_ag_uid)
        ]

        pg_term_mgr = self.postgres_db.get_term_mgr()

        root_impact_assessment_term = await pg_term_mgr.get_terms_by_xid("Root_Impact_Assessments")
        if root_impact_assessment_term:
            all_impact_assessment_terms = await pg_term_mgr.get_sub_terms_by_uid(
                str(root_impact_assessment_term[0].uid)
            )
        else:
            all_impact_assessment_terms = None

        if not all_impact_assessment_terms:
            all_impact_assessment_terms = [
                Term(
                    uid=uuid.uuid4(),
                    data={},
                    name=IPCC_2013_GWP_100,
                    xid=IPCC_2013_GWP_100_XID,
                    sub_class_of=None,
                    access_group_uid=None,
                ),
            ]
            logger.info(
                f"No impact assessment term: only {all_impact_assessment_terms[0].name} value can be returned."
                f" Run bw_import_controller with --import_all_impact_assessments for more impact assessment methods."
            )

        self.impact_assessment_name_to_term = {
            impact_assessment_term.name: impact_assessment_term
            for impact_assessment_term in all_impact_assessment_terms
        }

        warn_missing_impact_assessment_unit = False

        for cached_impact_assessment in cached_impact_assessments:
            impact_assessment_unit = cached_impact_assessment["cache_data"].get("unit", None)
            if not impact_assessment_unit:
                warn_missing_impact_assessment_unit = True
                if cached_impact_assessment["cache_key"].startswith("ipcc-2013") or cached_impact_assessment[
                    "cache_key"
                ].startswith("ipcc_2013"):
                    impact_assessment_unit = "kg CO2-Eq"
                impact_assessment_table = cached_impact_assessment["cache_data"]
            else:
                impact_assessment_table = cached_impact_assessment["cache_data"]["data"]
            impact_assessment_xid = cached_impact_assessment["cache_key"]
            if len(impact_assessment_table) == 0:
                logger.debug(f"Impact assessment table not loaded for {impact_assessment_xid}.")

            def term_by_xid_ag_uid(unit_name: str) -> uuid.UUID:
                return self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    f"EOS_{re.sub(' ', '-', unit_name.lower())}", self.unit_terms_ag_uid
                ].uid

            self.cache_characterization_factors[impact_assessment_xid] = {
                "data": {
                    k: ReferencelessQuantityProp.unvalidated_construct(
                        value=v["amount"], unit_term_uid=term_by_xid_ag_uid(v["unit"])
                    )
                    for k, v in impact_assessment_table.items()
                },
                "unit": term_by_xid_ag_uid(impact_assessment_unit),
            }
            assert (
                len(self.cache_characterization_factors[impact_assessment_xid]) > 0
            ), "Impact assessment table data integrity is invalid."

        if warn_missing_impact_assessment_unit:
            logger.info(
                "Some Impact assessments are missing units. Assuming kg CO2-Eq for all IPCC 2013 impact assessments."
                " For a greater accuracy, run bw_import_controller again with flag --import_all_impact_assessments."
            )

        logger.debug(
            f"added {len(self.cache_characterization_factors)} impact_assessment methods to ImpactAssessmentGFM cache"
        )
        logger.debug(f"Loaded all impact assessment caches in {time.process_time() - start_time} s")
        memory_usage = sys.getsizeof(self.cache_characterization_factors) + sys.getsizeof(
            self.impact_assessment_name_to_term
        )
        logger.debug(f"Memory of impact assessment cache dictionary: {memory_usage} bytes")

    def spawn_worker(self, node: Node) -> ImpactAssessmentGapFillingWorker:
        return ImpactAssessmentGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = ImpactAssessmentGapFillingFactory
