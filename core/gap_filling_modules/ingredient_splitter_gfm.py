"Ingredient splitter gap filling module."
import re
import uuid

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.index_prop import IndexProp
from core.domain.props.names_prop import RawName
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.service.ingredients_declaration_service import IngredientsDeclarationMappingService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class IngredientSplitterGapFillingWorker(AbstractGapFillingWorker):
    "This gap filling module splits the list of ingredients into separate nodes."

    def __init__(self, node: Node, gfm_factory: "IngredientSplitterGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if not isinstance(self.node, FoodProcessingActivityNode):
            logger.debug("[IngredientSplitter] Node is not recipe or food_product --> not scheduled.")
            return False
        else:
            if self.node.ingredients_declaration:
                logger.debug("[IngredientSplitter] Found ingredients declaration inside node --> scheduled.")
                return True
            else:
                logger.debug("[IngredientSplitter] Node does not have ingredients declaration --> not scheduled.")
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        logger.debug("[IngredientSplitter] --> can_run_now.")

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                "wait for AddClientNodesGapFillingWorker before splitting ingredient declaration --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule

        if len(self.node.get_sub_nodes()) > 0:
            logger.debug("Node already has child nodes --> skipping ingredient splitting.")
            return GapFillingWorkerStatusEnum.cancel

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug(f"start running gap-filling-module IngredientSplitterGFM on {self.node}")

        ingredients_declarations = self.node.ingredients_declaration

        ingredients_declaration = ""

        # TODO: rework this approach once we implement working with multiple languages?
        # for now, we prioritize picking the first german declaration in the list
        for declaration in ingredients_declarations:
            if declaration.get("language") == "de":
                ingredients_declaration = declaration.get("value")
                break

        # if we don't have any german declaration, then pick the first one
        # as it is supposed to be the primary one
        if not ingredients_declaration:
            ingredients_declaration = ingredients_declarations[0].get("value")

        declaration_mapping_service = IngredientsDeclarationMappingService(
            self.gfm_factory.postgres_db.get_ingredients_declaration_mapping_mgr(),
        )

        fixed_ingredients_declaration = await declaration_mapping_service.get_ingredients_declaration_mapping_by_text(
            ingredients_declaration,
        )

        if fixed_ingredients_declaration:
            ingredients_declaration = fixed_ingredients_declaration.fixed_declaration

        try:
            ingredients_list = self.gfm_factory.ingredient_splitter_parser.parse(ingredients_declaration)
        except Exception as e:
            logger.error(f"Exception encountered while parsing ingredient declaration on node {self.node}:\n" f"{e}")
            raise e  # re-raise so that orchestrator will stop

        # if this not is not an activity node, add one activity node (recipe)
        # Removed old if block because the module is never scheduled on a flow node.
        await self.create_nodes_for_ingredients(ingredients_list, calc_graph, self.node)

    async def create_nodes_for_ingredients(
        self,
        ingredients_list: list[dict],
        calc_graph: CalcGraph,
        base_level_node: Node,
    ) -> None:
        # TODO: remove hard-coded DE language code?
        for ingredient in ingredients_list:
            # copy percentage to Node, if available from parser's ingredient

            # flow to link ingredient_node to recipe (=base level node)
            recipe_ingredient = FoodProductFlowNode(
                uid=uuid.uuid4(),
                product_name=[RawName(value=ingredient.get("name"), language="de")],
            )
            recipe_ingredient.declaration_index = IndexProp(value=ingredients_list.index(ingredient))

            # Attach percentage as amount_in_original_source_unit.
            if ingredient.get("percentage"):
                recipe_ingredient.amount_in_original_source_unit = QuantityProp(
                    value=ingredient["percentage"],
                    unit_term_uid=self.gfm_factory.percentage_term.uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    source_data_raw={"value": str(ingredient.get("percentage")), "unit": "Percentage"},
                )
                recipe_ingredient.amount_in_original_source_unit.set_owner_node(recipe_ingredient)

            logger.debug(f"create ingredient-flow AddNodeMutation for {recipe_ingredient}...")
            await calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=base_level_node.uid,
                    new_node=recipe_ingredient,
                    copy=True,
                )
            )

            # if an ingredient has sub-ingredients, recursively iterate over the list of sub-ingredients as well
            if isinstance(ingredient.get("subproducts"), list):
                # creating an activity node to serve as a link between the ingredient and sub-ingredients
                subrecipe_node = FoodProcessingActivityNode(
                    uid=uuid.uuid4(),
                    name=[RawName(value=ingredient.get("name"), language="de")],
                )

                logger.debug(f"create subrecipe-activity AddNodeMutation for {subrecipe_node}...")
                await calc_graph.apply_mutation(
                    AddNodeMutation(
                        created_by_module=self.__class__.__name__,
                        parent_node_uid=recipe_ingredient.uid,
                        new_node=subrecipe_node,
                        copy=True,
                    )
                )

                await self.create_nodes_for_ingredients(
                    ingredient.get("subproducts"),
                    calc_graph,
                    subrecipe_node,
                )


class IngredientSplitterGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.ingredient_splitter_parser = IngredientSplitterParser()
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]
        self.kilogram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", root_unit_term.access_group_uid)
        ]
        self.percentage_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_percentage", root_unit_term.access_group_uid)
        ]

    async def init_cache(self) -> None:
        pass

    def spawn_worker(self, node: Node) -> IngredientSplitterGapFillingWorker:
        return IngredientSplitterGapFillingWorker(node, self)


class IngredientSplitterParser:
    "Parses a string with ingredients declaration into a list of individual ingredients and sub-ingredients."

    def __init__(self):
        self.BRACKETS = {"[": "]", "(": ")"}
        self.CLOSING_BRACKETS = (")", "]")
        self.REPLACE_CHARS = [
            # unknown encoding errors
            ("Ã€", "ä"),
            ("ÃŒ", "ü"),
            # ansi-utf8 encoding errors
            ("Ã„", "Ä"),
            ("Ã–", "Ö"),
            ("Ãœ", "Ü"),
            ("Ã¤", "ä"),
            ("Ã¶", "ö"),
            ("Ã¼", "ü"),
            ("ÃŸ", "ß"),
            # cp1250-utf8 encoding errors
            ("Ă„", "Ä"),
            ("Ă–", "Ö"),
            ("Ăś", "Ü"),
            ("Ă¤", "ä"),
            ("Ă¶", "ö"),
            ("ĂĽ", "ü"),
            ("Ăź", "ß"),
            # other replacements
            ("<b>", ""),
            ("</b>", ""),  # remove html tags
            ('"', "'"),  # normalize quotes
            ("\r\n", " "),
            ("\r", " "),
            ("\n", " "),
            ("\t", " "),  # normalize whitespace
            (";", ","),  # normalize commas
            ("  ", " "),
            # hacky replacements
            ("% Fett ", " Proz. Fett "),
            ("Zutaten:", ""),
            ("Zutaten ", ""),
            ("ZUTATEN: ", ""),
            ("ZUTATEN ", ""),
            ("*", ""),
        ]
        self.USELESS_FROM_HERE_ON = (
            "kann ",  # like in "kann spuren von Nüssen enhalten"
            "unter schutzatm",  # like in "unter schutzatmosphäre verpackt"
            "enthält",  # like in "enthält glutenhaltige getreide"
            "aus kontrolliert biologisch",  # like in "aus kontrolliert biologischem Anbau"
        )

    def replace_incompatible_substrings(self, string: str) -> str:
        "Replace bad characters."
        for old_chars, new_chars in self.REPLACE_CHARS:
            string = string.replace(old_chars, new_chars)

        return string

    def discard_string_endings(self, string: str) -> str:
        "Keep everything before the useless element."
        for element in self.USELESS_FROM_HERE_ON:
            # In a FIRST pass, we need to make sure to only remove until some bracket is found, otherwise logic in
            # segment_by_comma_and_brackets() breaks and fails completely.
            # therefore, the pattern matches until but excluding some bracket (i.e. regexp "[()[\]]") is found.
            # e.g. "... , natürliches Aroma (enthält NÜSSE, MANDELN), Meersalz."
            string = re.sub(element + r".+?(?=[()[\]])", "", string, flags=re.IGNORECASE)
            # In a SECOND pass, if there's still an occurrence of `element` in `string`, we can now "safely" remove
            # until the end of the sentence.
            # e.g. "... , Erdmandelmehl*, Meersalz. * aus kontrolliert biologischem Anbau. Kann Spuren von SELLERIE"
            string = re.sub(element + r".+", "", string, flags=re.IGNORECASE)
        return string

    def move_percentages_to_back(self, string: str) -> str:
        "Change '42% Bohnen,' to 'Bohnen 42%,'."
        string = re.sub(r"(\d+[\.,]?\d*\s*%)(\s?)(.*?)([\[\(\]),\.]|$)", r"\3 \1\4", string)

        return string

    def fix_percentages(self, string: str) -> str:
        """Replace comma with periods and surround them with parentheses.

        sometimes a comma is used as a decimal -- which is a huge problem when a comma is the ingredient delimiter!
        """
        # add a space to the beginning and end temporarily
        # because sometimes we need to do a negative matching on
        # the preceding and trailing character of a sequence:
        padded = " {} ".format(string)
        # first, every example of number-comma-number (no spaces)
        # should be turned into number-period-number
        nocomma = re.sub(r"(\d),(\d)", r"\1.\2", padded)
        # if there's a space between the number and the percentage, remove it:
        attach_percent = re.sub(r"(\d)\s+%", r"\1%", nocomma)
        # remove space between leading parenthesis and a number:
        attach_open_paren = re.sub(r"\(\s+(\d)", r"(\1", attach_percent)
        # remove space between percent sign and close parenthesis:
        attach_close_paren = re.sub(r"%\s+\)", "%)", attach_open_paren)
        # if a percent is found with a decimal, not enclosed in parentheses, add parens:
        enclose1 = re.sub(r"([^\(\d])(\d+)\.(\d+)%", r"\1(\2.\3%)", attach_close_paren)
        # if a percent is found without a decimal, not enclosed in parentheses, add parens:
        enclose2 = re.sub(r"([^\(\.\d])(\d+)%", r"\1(\2%)", enclose1)

        string = enclose2.strip()

        return string

    def validate_balanced_parenthesis(self, string: str) -> str:
        logger.debug("validating string", string=string)

        # if ',' not in string:
        #     logger.error('string not a list of items', string=string)
        #     return ''

        if string.count("(") != string.count(")"):
            raise ValueError(f"mismatched parentheses in {string}")

        if string.count("[") != string.count("]"):
            raise ValueError(f"mismatched square brackets in {string}")

        return string

    def normalize(self, string: str) -> str:
        logger.debug("normalizing string", string=string)
        string = string.strip()
        string = string[:-1] if string.endswith(".") else string
        string = self.replace_incompatible_substrings(string)
        string = self.discard_string_endings(string)
        string = self.move_percentages_to_back(string)
        string = self.fix_percentages(string)

        return string

    def segment_by_comma_and_brackets(self, string: str) -> list[list | str] | None:
        """Iterate over the string, push and pop bracket on and off a stack.

        Store what was inside brackets inside the containers.
        """
        logger.debug("segmenting string by commas and brackets", string=string)

        bracket_stack = []
        element_stack = []
        element_container = []
        current_container = element_container
        last_ix = 0

        for ix, char in enumerate(string):
            if char == ",":
                current_container.append(string[last_ix:ix])
                last_ix = ix + 1

            elif char in self.BRACKETS:
                current_container.append(string[last_ix:ix])
                bracket_stack.append(char)
                element_stack.append(current_container)
                current_container = []
                last_ix = ix + 1

            elif char in self.CLOSING_BRACKETS:
                try:
                    expected_bracket = self.BRACKETS[bracket_stack.pop()]  # also raises IndexError on empty list

                    if expected_bracket != char:
                        raise IndexError()

                except IndexError:
                    logger.error("mismatched brackets", string=string)

                    return None

                current_container.append(string[last_ix:ix])
                element_stack[-1].append(current_container)
                current_container = element_stack.pop()
                last_ix = ix + 1

        if bracket_stack:
            logger.error("leftover open brackets at end of string", string=string)
            raise ValueError("leftover open brackets at end of string")

        # deal with last chunk, because it's special
        last_element = string[last_ix:]
        # if there's a dot in the last element, it introduces another clause we likely don't care about
        last_element = last_element.split(".", 1)[0]
        # and it's probably not that long
        last_element = last_element[:40]
        # and pick up any number of words containing (neither a digit nor an alphanumeric) or (a hyphen)
        last_element = re.match(r"([^\d\W]|[ \-])*", last_element)[0]
        # now append it
        if last_element:
            element_container.append(last_element)

        return element_container

    def format_product_string(self, string: str) -> str:
        string = string.strip()

        return string

    def as_products(self, chunks: list[list | str]) -> list[dict]:
        "Clean up indiviudal strings, remove empty chunks."
        cleaned_chunks = []

        for chunk in chunks:
            if isinstance(chunk, str):
                chunk = self.format_product_string(chunk)

            if chunk:  # remove empty chunks
                cleaned_chunks.append(chunk)

        # format them into a product
        formatted_chunks = []
        last_product = {}

        for chunk in cleaned_chunks:
            if isinstance(chunk, str):
                product = {
                    "name": chunk,
                    "subproducts": None,
                    "percentage": None,
                    "descriptor": None,
                }
                formatted_chunks.append(product)
                last_product = product

            else:  # chunk is a list
                if len(chunk) == 1:  # then it's an attribute
                    new_attribute = chunk[0]
                    assert isinstance(new_attribute, str)

                    if re.match(
                        r"\d+\.?\d*%", new_attribute
                    ):  # then it's a percentage  # matches "6.21%", but not "abc 6.21%"
                        last_product["percentage"] = float(new_attribute[:-1])
                    else:
                        last_product["descriptor"] = new_attribute

                else:  # previous item is a (sub)recipe
                    last_product["subproducts"] = self.as_products(chunk)

        return formatted_chunks

    def parse(self, string: str) -> list[dict]:
        logger.debug("parsing string")
        string = self.validate_balanced_parenthesis(string)
        string = self.normalize(string)
        chunks = self.segment_by_comma_and_brackets(string)

        logger.debug("turning chunks into products")
        products = self.as_products(chunks)

        logger.debug("Finished parsing string", total_parsed_products=len(products))
        return products


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = IngredientSplitterGapFillingFactory
