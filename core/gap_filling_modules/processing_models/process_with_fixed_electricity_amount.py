"""Process model that only have an electricity node with a fixed amount attached."""
import uuid

from gap_filling_modules.processing_models.process_with_electricity_node import ProcessWithElectricityNode
from structlog import get_logger

from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum

logger = get_logger()


class ProcessWithFixedElectricityAmount(ProcessWithElectricityNode):
    """Process model that only have an electricity node with a fixed amount attached."""

    async def model_specific_formula(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        *_args: ...,
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Formula for a model that only has an electricity node with a fixed amount attached."""
        return_dict = {}

        for bw_node_uid, amount in bw_nodes_and_amounts.items():
            unit_term = await self.get_unit_term_from_bw_node_uid(bw_node_uid)
            return_dict[bw_node_uid] = QuantityProp.unvalidated_construct(
                value=amount * production_amount.value,
                unit_term_uid=unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            )

        return (
            QuantityProp(
                value=production_amount.value,
                unit_term_uid=production_amount.unit_term_uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
            return_dict,
        )
