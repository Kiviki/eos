"""Process model that only have an electricity node attached."""
import uuid
from abc import ABC

from gap_filling_modules.processing_models.abstract_processing_model import (
    AbstractProcessingModel,
    ElectricityNodeByCountry,
    ProcessingTagsAndElectricityAmount,
)
from git import Optional
from structlog import get_logger

from core.service.service_provider import ServiceProvider

logger = get_logger()


class ProcessWithElectricityNode(AbstractProcessingModel, ABC):
    """Process model that only have an electricity node attached."""

    def __init__(
        self,
        process_name: str,
        process_specific_trigger_tags: list[ProcessingTagsAndElectricityAmount],
        electricity_node_by_country: ElectricityNodeByCountry,
        raw_materials_uid: uuid.UUID,
        foodex2_access_group_uid: uuid.UUID,
        service_provider: ServiceProvider,
    ):
        super().__init__(
            process_name,
            process_specific_trigger_tags,
            electricity_node_by_country,
            raw_materials_uid,
            foodex2_access_group_uid,
            service_provider,
        )

    async def load_brightway_node_and_subgraph(
        self, processing_info: ProcessingTagsAndElectricityAmount, country_code: str | None
    ) -> Optional[dict[uuid.UUID, float]]:
        """Get the correct electricity node for the country and voltage. If not found, use the default 'GLO' node."""
        bw_node_w_amount: dict[uuid.UUID, float] = {}

        # TODO: low or medium voltage?
        voltage = "low_voltage"
        electricity_node_by_country_for_voltage = getattr(self.electricity_node_by_country, voltage)

        electricity_amount = processing_info.electricity_amount

        if country_code is not None and country_code in electricity_node_by_country_for_voltage:
            bw_node_w_amount[electricity_node_by_country_for_voltage.get(country_code)] = electricity_amount
        else:
            logger.warning(f"Could not find electricity node for country {country_code} and voltage {voltage}")
            default_electricity_node_uid = electricity_node_by_country_for_voltage.get("GLO")
            if default_electricity_node_uid is None:
                logger.warning(f"Could not find default 'GLO' electricity node for voltage {voltage}.")
                return None
            bw_node_w_amount[default_electricity_node_uid] = electricity_amount
        return bw_node_w_amount
