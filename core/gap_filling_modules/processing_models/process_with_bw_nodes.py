"""Process model that have EDB Brightway nodes attached."""
import uuid
from abc import ABC

from gap_filling_modules.processing_models.abstract_processing_model import (
    AbstractProcessingModel,
    ElectricityNodeByCountry,
    ProcessingTagsAndXid,
)
from structlog import get_logger

from core.service.service_provider import ServiceProvider

logger = get_logger()


class ProcessWithBWNode(AbstractProcessingModel, ABC):
    """Process model that have EDB Brightway nodes attached."""

    def __init__(
        self,
        process_name: str,
        process_specific_trigger_tags: list[ProcessingTagsAndXid],
        electricity_node_by_country: ElectricityNodeByCountry,
        raw_materials_uid: uuid.UUID,
        foodex2_access_group_uid: uuid.UUID,
        service_provider: ServiceProvider,
    ):
        super().__init__(
            process_name,
            process_specific_trigger_tags,
            electricity_node_by_country,
            raw_materials_uid,
            foodex2_access_group_uid,
            service_provider,
        )

    async def load_brightway_node_and_subgraph(
        self, processing_info: ProcessingTagsAndXid, country_code: str | None
    ) -> dict[uuid.UUID, float]:
        """Gets the brightway node uids and relative amounts. dict key is 'raw_material' for Raw Material."""
        product_service = self.service_provider.product_service
        node_service = self.service_provider.node_service

        try:
            node_uid = product_service.xid_to_uid_mappings[processing_info.xid]
        except KeyError as e:
            logger.warn(f"Required Brightway node for processing is not loaded from the database: {e}")
            return {}

        edges = await node_service.get_sub_graph_by_uid(node_uid, max_depth=1)
        add_children_separately = False
        bw_node_w_amount: dict[uuid.UUID, float] = {}

        for edge in edges:
            sub_edge = (await node_service.get_sub_graph_by_uid(edge.child_uid, max_depth=1))[0]
            child_node = await node_service.find_by_uid(edge.child_uid)
            amount = child_node.amount_in_original_source_unit.value
            bw_node_w_amount[sub_edge.child_uid] = amount

            if country_code is not None:
                for voltage in ("low_voltage", "medium_voltage"):
                    electricity_node_by_country_for_voltage = getattr(self.electricity_node_by_country, voltage)
                    if sub_edge.child_uid in (
                        electricity_node_by_country_for_voltage.get("RER"),
                        electricity_node_by_country_for_voltage.get("GLO"),
                    ):
                        if country_code in electricity_node_by_country_for_voltage:
                            add_children_separately = True
                            bw_node_w_amount[
                                electricity_node_by_country_for_voltage.get(country_code)
                            ] = bw_node_w_amount[sub_edge.child_uid]
                            del bw_node_w_amount[sub_edge.child_uid]

        if add_children_separately:
            return bw_node_w_amount
        else:
            return_dict = {node_uid: 1.0}
            if self.raw_materials_uid in bw_node_w_amount:
                return_dict[self.raw_materials_uid] = bw_node_w_amount[self.raw_materials_uid]
            return return_dict
