from typing import TYPE_CHECKING

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.matching_service import MatchingService
from core.service.service_provider import ServiceProvider

if TYPE_CHECKING:
    from database.postgres.postgres_db import PostgresDb

logger = get_logger()

# used when retrieving from matching_service's cache
MATCH_PRODUCT_GFM_NAME = "MatchProductName"


class MatchProductNameGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "MatchProductNameGapFillingFactory"):
        """Initialize MatchProductNameGapFillingWorker.

        Matches the "raw_input"'s "names" of a product[Node] or ingredient[Node] to a tuple of Terms, using the
        matching db table. Performs lowercasing of the name before matching.
        E.g. matches a product with the name "trockene Karotten" to the Terms ("A1791", "J0116") from the glossary
        (="CARROT" and "DEHYDRATED OR DRIED" from FoodEx2).
        Then adds these Terms to the product's Props.
        """
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if isinstance(self.node, FoodProductFlowNode) and self.node.is_subdivision:
            logger.debug("[MatchProductName] subdivision node is always already matched --> skip")
            return False

        if not isinstance(self.node, FoodProductFlowNode):  # Should we include other flow nodes here?
            logger.debug("[MatchProductName] node is not a food product --> not scheduled.")
            return False
        else:
            if self.node.product_name and isinstance(self.node.product_name, (list, DeepListView)):
                logger.debug("[MatchProductName] found raw product_name --> scheduled.")
                return True
            else:
                logger.debug("[MatchProductName] node does not have raw product_name --> not scheduled.")
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            # we want to match product names *after* splitting the ingredients; else we will not have a chance to match
            # the ingredients_declaration's ingredient names to their terms.
            logger.debug("[MatchProductName] waiting for IngredientSplitter GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        else:
            logger.debug("[MatchProductName] --> ready.")
            return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug("start running gap-filling-module MatchProductName...")

        matches: [Term] = []
        # the string we want to match to a Term
        names_user_input = self.node.product_name

        # check if all inputs are empty
        if not any([name_user_input["value"] for name_user_input in names_user_input]):
            matches = await self.gfm_factory.matching_service.get_terms_by_matching_string(
                "data error",
                MATCH_PRODUCT_GFM_NAME,
            )

            logger.warn('Empty name values! Matching this product with "data error" Term...', node=self.node)
            calc_graph.set_data_errors_log_entry(
                f'Empty name values! Matching node {self.node} with "data error" Term...',
            )
        else:
            # we first try to find an exact language match. Only if that does not succeed, we ignore the language:
            for do_exact_lang_match in [True, False]:
                for name_user_input in names_user_input:  # there can be different names because of different languages
                    # handling an edge case where the value is empty or `None`
                    if not name_user_input["value"]:
                        continue  # we continue to the next name entry

                    if do_exact_lang_match:
                        filter_lang = name_user_input.get("language", None)
                    else:
                        filter_lang = None

                    matches = await self.gfm_factory.matching_service.get_terms_by_matching_string(
                        name_user_input["value"],
                        MATCH_PRODUCT_GFM_NAME,
                        filter_lang=filter_lang,
                    )

                    if len(matches) > 0:
                        # we break on the first found matching name
                        break

                if len(matches) > 0:
                    # if the first loop succeeded finding an exact language match, we do not continue searching
                    break

        if len(matches) == 0:  # no match --> let's do some logging
            # whether this node is a "compound product" (a product that is made of other food products),
            # for example Soy-sauce (Water, Soybeans, Salt, Sugar, Wheat)
            is_compound_product = len([n for n in self.node.get_sub_nodes() if isinstance(n, FoodProductFlowNode)]) > 0

            if is_compound_product:
                logger_lvl = logger.info
            else:
                logger_lvl = logger.warn
                calc_graph.set_data_errors_log_entry(
                    f"Could not match product name {names_user_input} in node {self.node} "
                    f"(is compound product: {is_compound_product})"
                )

            logger_lvl(
                "Could not match product name",
                names_user_input=names_user_input,
                node_uid=self.node.uid,
                is_compound_product=is_compound_product,
            )
        else:
            # TODO: in case no language is set and we get matches for multiple languages,
            #  we pick the first result -- fix this?
            product_name_terms: list[Term] = matches[0]  # we go with the first match (for now...)

            logger.debug("create PropMutation to add product_name and nutrient_values", prod_term=product_name_terms)
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="product_name",
                    prop=NamesProp(
                        terms=[GlossaryTermProp(term_uid=product_name.uid) for product_name in product_name_terms],
                        source_data_raw=names_user_input,
                    ),
                )
            )


class MatchProductNameGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: "PostgresDb", service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.matching_service: MatchingService = service_provider.matching_service
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service

    async def init_cache(self) -> None:
        pass  # caching is done at the matching service level

    def spawn_worker(self, node: Node) -> MatchProductNameGapFillingWorker:
        return MatchProductNameGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = MatchProductNameGapFillingFactory
