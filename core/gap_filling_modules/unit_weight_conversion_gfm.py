"""Fill in units and convert units to align with production amount."""
import re
import uuid
from typing import Tuple, Type, Union

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, ModeledActivityNode, SupplySheetActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_prop import QuantityProp, RawQuantity, ReferenceAmountEnum, ReferencelessQuantityProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceLocator, ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class UnitWeightConversionGapFillingWorker(AbstractGapFillingWorker):
    """Unit weight conversion gap filling module."""

    def __init__(self, node: Node, gfm_factory: "UnitWeightConversionGapFillingFactory"):
        """Initialize unit weight conversion gap filling module.

        :param node: Node
        :param gfm_factory: UnitWeightConversionGapFillingFactory.
        """
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Should be scheduled for all root, food_product, recipes, and flow nodes."""
        try:
            if self.node.get_calculation().child_of_root_node_uid == self.node.uid:
                logger.debug("[UnitWeightConversion] always run on root_node to add production_amount --> scheduled.")
                return True
        except IndexError:
            logger.debug("Graph does not have direct child of root node.")

        if isinstance(self.node, (FoodProcessingActivityNode, SupplySheetActivityNode)) or isinstance(
            self.node, FlowNode
        ):
            logger.debug(
                "[UnitWeightConversion] always run on food_product and recipes "
                "to add production_amount or flows to check for unit coherency --> scheduled."
            )
            return True
        else:
            logger.debug("[UnitWeightConversion] node is neither a flow nor of a food product --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Condition for running UnitWeightConvserionGFM.

        For activity nodes, we have to wait for product name and amounts are added.
        For flows, wait for child node to be added.
        """

        def gfm_state(gfm_worker: Type[AbstractGapFillingWorker], node: Node, state: NodeGfmStateEnum) -> bool:
            """Local GFM state of a worker on the node."""
            if getattr(node, GFM_STATE_PROP_NAME) and (
                gfm_status := getattr(node, GFM_STATE_PROP_NAME).worker_states.get(gfm_worker.__name__)
            ):
                return gfm_status == state
            else:
                return False

        if gfm_state(MatchProductNameGapFillingWorker, self.node, NodeGfmStateEnum.scheduled):
            # Match product name gap filling module can give further information
            # that helps towards identifying the units.
            logger.debug("[UnitWeightConversion] waiting for MatchProductNameGFM --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if isinstance(self.node, ActivityNode):
            if self.node.production_amount and isinstance(self.node.production_amount, QuantityProp):
                logger.debug("[UnitWeightConversion] Activity node already has production amount --> cancel")
                return GapFillingWorkerStatusEnum.cancel

            if not self.node.production_amount:
                if gfm_state(AddClientNodesGapFillingWorker, self.node, NodeGfmStateEnum.scheduled):
                    # For activity nodes, we ensure that all child flow nodes are already added so that
                    # we can use the flow amounts to determine the production amount as a fallback.
                    logger.debug("[UnitWeightConversion] waiting for AddClientNodeGFM --> not can_run_now.")
                    return GapFillingWorkerStatusEnum.reschedule

                elif gfm_state(AddClientNodesGapFillingWorker, self.node, NodeGfmStateEnum.finished):
                    if sub_nodes := self.node.get_sub_nodes():
                        for sub_node in sub_nodes:
                            if gfm_state(UnitWeightConversionGapFillingWorker, sub_node, NodeGfmStateEnum.scheduled):
                                # Wait for UnitWeightConversionGFM to add converted amounts to the child nodes added
                                # by the AddClientNodesGFM.
                                logger.debug(
                                    "[UnitWeightConversion] waiting for subnodes to have " "amount --> not can_run_now."
                                )
                                return GapFillingWorkerStatusEnum.reschedule

            logger.debug(
                "[UnitWeightConversion] MatchProductNameGFM/AddClientNodeGFM "
                "finished or not scheduled --> can_run_now"
            )
            return GapFillingWorkerStatusEnum.ready
        else:
            if self.node.amount:
                logger.debug("[UnitWeightConversion] Flow node already has amount. --> cancel")
                return GapFillingWorkerStatusEnum.cancel

            if len(self.node.get_sub_nodes()) > 0:
                if self.node.get_sub_nodes()[0].production_amount and isinstance(
                    self.node.get_sub_nodes()[0].production_amount, QuantityProp
                ):
                    logger.debug("[UnitWeightConversion] Flow node has a child node. --> can_run_now")
                    return GapFillingWorkerStatusEnum.ready

            reschedule_limit = 10
            if self.node.get_parent_nodes() and isinstance(self.node.get_parent_nodes()[0], ModeledActivityNode):
                # If Brightway flow node, it is less bad to assume that flow has the same unit as the child product
                # so have a lower limit.
                reschedule_limit = 3

            if self.reschedule_counter < reschedule_limit:
                logger.debug("[UnitWeightConversion] Flow node has no child node. --> reschedule")
                return GapFillingWorkerStatusEnum.reschedule
            else:
                logger.debug("[UnitWeightConversion] Run anyway without child activity .")
                return GapFillingWorkerStatusEnum.ready

    def extract_amount_and_unit_from_raw_input(self) -> Union[Tuple[float, str], Tuple[None, None]]:
        """Extract amount and unit from raw input for production / flow amount."""
        if isinstance(self.node, FlowNode):
            if isinstance(self.node.amount_in_original_source_unit, RawQuantity):
                amount_input = self.node.amount_in_original_source_unit.value
                unit_input = self.node.amount_in_original_source_unit.unit
            else:
                amount_input = None
                unit_input = None
        else:
            if isinstance(self.node.production_amount, RawQuantity):
                amount_input = self.node.production_amount.value
                unit_input = self.node.production_amount.unit
            else:
                amount_input = None
                unit_input = None
        return amount_input, unit_input

    async def add_production_amount(
        self,
        amount_input: int | float,
        unit_input: str,
        original_unit_term: Term | None,
        linked_term_data: Term | None,
        calc_graph: CalcGraph,
    ) -> None:
        """Add production amount from raw input data."""
        if isinstance(self.node, FoodProcessingActivityNode):
            # converting raw input value to kilograms such that production_amount
            # is ready for ingredient_amount_estimator_gfm.
            converter = UnitWeightConverter(self.gfm_factory)
            processed_amount, processed_unit = converter.convert_to_target(
                amount_input,
                unit_input,
                original_unit_term,
                linked_term_data,
                self.gfm_factory.GRAM_PER["KILOGRAM"],
                calc_graph,
            )

            prod_amount_prop_data = processed_amount
            prod_amount_unit_term = processed_unit
        else:
            prod_amount_prop_data = amount_input
            prod_amount_unit_term = original_unit_term

        logger.debug(
            "create PropMutation to add production amount and unit ",
            prop_data=f"{prod_amount_prop_data}",
        )
        production_amount_prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="production_amount",
            prop=QuantityProp(
                value=prod_amount_prop_data,
                unit_term_uid=prod_amount_unit_term.uid,
                for_reference=ReferenceAmountEnum.self_reference,
                source_data_raw=self.node.production_amount.model_dump() if self.node.production_amount else None,
            ),
        )
        await calc_graph.apply_mutation(production_amount_prop_mutation)

    async def add_amount_in_original_source_unit(
        self,
        amount_input: float,
        original_unit_term: Term,
        calc_graph: CalcGraph,
    ) -> None:
        """Add original amount from raw input data."""
        if original_unit_term:
            unit_term_uid = original_unit_term.uid
        else:
            unit_term_uid = self.gfm_factory.root_unit_term.uid  # If unknown, add root_unit_term.

        logger.debug(
            "create PropMutation to add original amount and unit ",
            prop_data=f"{amount_input} {original_unit_term}",
        )
        amount_prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="amount_in_original_source_unit",
            prop=QuantityProp(
                value=amount_input,
                unit_term_uid=unit_term_uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                source_data_raw=self.node.amount_in_original_source_unit.model_dump()
                if self.node.amount_in_original_source_unit
                else None,
            ),
        )

        if not self.node.amount_in_original_source_unit or not isinstance(
            self.node.amount_in_original_source_unit, QuantityProp
        ):
            await calc_graph.apply_mutation(amount_prop_mutation)

    async def run(self, calc_graph: CalcGraph) -> None:
        """Add amounts, units and match units between flow and child activity node."""
        logger.debug("start running gap-filling-module UnitWeightConversion...")

        converter = UnitWeightConverter(self.gfm_factory)

        if isinstance(self.node, ActivityNode) or not isinstance(
            self.node.amount_in_original_source_unit, QuantityProp
        ):
            # Adding production_amount or amount:

            amount_input, unit_input = self.extract_amount_and_unit_from_raw_input()

            if isinstance(self.node, (FoodProcessingActivityNode, SupplySheetActivityNode)):
                if not amount_input or not unit_input:  # Try to assume the production_amount from children nodes.
                    if subnodes := self.node.get_sub_nodes():
                        all_subnodes_have_amounts = True
                        total_subnode_amounts = 0.0
                        for subnode in subnodes:
                            if subnode_converted_amount := subnode.amount:
                                ingredient_amount = subnode_converted_amount.value
                                ingredient_unit: Term = subnode_converted_amount.get_unit_term()
                                if ingredient_unit.sub_class_of == self.gfm_factory.mass_term.uid:
                                    total_subnode_amounts += UnitWeightConverter.convert_between_same_unit_types(
                                        ingredient_amount, ingredient_unit, self.gfm_factory.GRAM_TERM, "mass-in-g"
                                    )
                                else:
                                    # Conservative estimation. Only approximate production amount from children flows
                                    # if all children flows have mass units.
                                    all_subnodes_have_amounts = False
                            else:
                                all_subnodes_have_amounts = False

                        if all_subnodes_have_amounts:
                            amount_input = total_subnode_amounts
                            unit_input = "Gram"

                if not amount_input and not unit_input:
                    amount_input = 1000
                    unit_input = "Gram"
                elif not amount_input:
                    amount_input = 1
                elif not unit_input:
                    unit_input = "Gram"
            else:
                if not amount_input:
                    if self.node.uid == calc_graph.get_root_node().uid:
                        # Assume 1 production amount as amount_in_original_source_unit if unavailable for root flow node
                        amount_input = 1.0
                        unit_input = "Production_amount"
                    else:
                        return
                elif not unit_input:
                    unit_input = "Gram"  # This default is added since in RawQuantity, unit is not required.

            if (
                isinstance(self.node, FlowNode)
                and self.node.product_name
                and isinstance(self.node.product_name, NamesProp)
            ):
                # TODO: maybe it makes sense to grab units just from the 1st term?
                #  since it is usually the main one...
                linked_term_data = self.node.product_name.terms[0].get_term().data
            else:
                linked_term_data = None

            # matching raw input unit string to a unit glossary term

            original_unit_term = converter.match_unit_term(unit_input, calc_graph)

            if hasattr(self.node, "amount_in_original_source_unit"):
                await self.add_amount_in_original_source_unit(amount_input, original_unit_term, calc_graph)

            if isinstance(self.node, ActivityNode) and not isinstance(self.node.production_amount, QuantityProp):
                await self.add_production_amount(
                    amount_input,
                    unit_input,
                    original_unit_term,
                    linked_term_data,
                    calc_graph,
                )
        else:
            linked_term_data = None
            unit_input = ""

        if isinstance(self.node, FlowNode):

            async def add_flow_node_amount(amount_to_add: float, unit_term_uid_to_add: uuid.UUID) -> None:
                amount_prop_mutation = PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="amount",
                    prop=QuantityProp(
                        value=amount_to_add,
                        unit_term_uid=unit_term_uid_to_add,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                )
                await calc_graph.apply_mutation(amount_prop_mutation)

            amount_in_original_source_unit = self.node.amount_in_original_source_unit
            if self.node.get_sub_nodes():
                child_node_production_amount: QuantityProp | None = self.node.get_sub_nodes()[0].production_amount
            else:
                child_node_production_amount: QuantityProp | None = None

            if amount_in_original_source_unit and isinstance(amount_in_original_source_unit, QuantityProp):
                flow_node_unit: Term | None = amount_in_original_source_unit.get_unit_term()
                original_flow_amount: float = amount_in_original_source_unit.value

                unit_name = flow_node_unit.name
                unit_term_uid = flow_node_unit.uid
                if unit_term_uid == self.gfm_factory.root_unit_term.uid:
                    # If unit term is abstract (unknown) use unit_input as the unit_name for conversion.
                    unit_name = unit_input
                    flow_node_unit = None

                if child_node_production_amount:
                    # Checking the consistency with child_production_amount unit.
                    child_node_unit: Term = child_node_production_amount.get_unit_term()
                    if child_node_unit:
                        if unit_term_uid == self.gfm_factory.production_amount_unit_term.uid:
                            # Handle the case where the amount_in_original_unit is given in production_amounts.
                            processed_amount = original_flow_amount * child_node_production_amount.value
                            processed_unit = child_node_unit

                        else:
                            processed_amount, processed_unit = converter.convert_to_target(
                                original_flow_amount,
                                unit_name,
                                flow_node_unit,
                                linked_term_data,
                                child_node_unit,
                                calc_graph,
                            )

                        if processed_amount and processed_unit:
                            await add_flow_node_amount(processed_amount, processed_unit.uid)

                if not self.node.amount:
                    # Some Brightway flows are missing units, children, or production amount unit.
                    # These will simply be addd as a copy of amount
                    if self.node.get_parent_nodes() and isinstance(
                        self.node.get_parent_nodes()[0], ModeledActivityNode
                    ):
                        logger.debug("Assuming that flow node and sub-activity node have the same units.")
                        await add_flow_node_amount(original_flow_amount, unit_term_uid)


class UnitWeightConversionGapFillingFactory(AbstractGapFillingFactory):
    """UnitWeightConversion gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        """Initialize unit weight conversion gap filling factory."""
        super().__init__(postgres_db, service_provider)

        self.cache_unit_terms: dict[str, Term] = {}

        self.ABSTRACT_UNITS = (
            "PIECE",
            "PIECES",
            "PCS",
        )
        self.MASS_UNITS = tuple()
        self.VOLUME_UNITS = tuple()

        self.GRAM_PER: dict[str, Term] = {}
        self.MILLILITER_PER: dict[str, Term] = {}

        self.GRAM_TERM = Term(data={}, name="", sub_class_of=None)

        self.root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.unit_term_access_group_uid = self.root_unit_term.access_group_uid
        self.mass_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_units_mass", self.unit_term_access_group_uid)
        ]
        self.transportation_units_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_units_transportation", self.unit_term_access_group_uid)
        ]
        self.freezing_units_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_units_freezing", self.unit_term_access_group_uid)
        ]
        self.production_amount_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_production_amount", self.unit_term_access_group_uid)
        ]

    async def init_cache(self) -> None:
        """Initialize cache for unit weight conversion gap filling factory."""
        category_unit_terms = await self.service_provider.glossary_service.get_sub_terms_by_uid(
            str(self.root_unit_term.uid),
            depth=1,
        )

        self.cache_unit_terms = {
            category.name: await self.service_provider.glossary_service.get_sub_terms_by_uid(category.uid)
            for category in category_unit_terms
        }

        self.MASS_UNITS = self.load_units_into_dict(self.GRAM_PER, "Mass units")
        self.VOLUME_UNITS = self.load_units_into_dict(self.MILLILITER_PER, "Volume units")

        self.GRAM_TERM = self.GRAM_PER.get("GRAM")

    def load_units_into_dict(self, processed_dict: dict, cache_key: str) -> tuple:
        """Load relevant unit information into a tuple."""
        for unit in self.cache_unit_terms.get(cache_key, []):
            unit_value = unit.data.get("mass-in-g") or unit.data.get("volume-in-ml")

            if not unit_value:
                logger.debug(f"Unit {unit.name} does not have value! Skipping..")
                continue

            processed_dict[unit.name.upper()] = unit

            if unit.data.get("alias"):
                for aliased_unit in unit.data.get("alias", []):
                    processed_dict[aliased_unit.upper()] = unit

        return tuple([key for key in processed_dict.keys()])

    def spawn_worker(self, node: Node) -> UnitWeightConversionGapFillingWorker:
        """Spawn unit weight conversion gap filling factory."""
        return UnitWeightConversionGapFillingWorker(node, self)


class UnitWeightConverter:
    """Converts units and its amount into amount in grams.

    Parts of this code were written in 2020-2022 by Holger Finger.
    """

    def __init__(self, gfm_factory: UnitWeightConversionGapFillingFactory):
        self.ABSTRACT_UNITS = gfm_factory.ABSTRACT_UNITS
        self.MASS_UNITS = gfm_factory.MASS_UNITS
        self.VOLUME_UNITS = gfm_factory.VOLUME_UNITS

        self._GRAM_PER = gfm_factory.GRAM_PER
        self._MILLILITER_PER = gfm_factory.MILLILITER_PER
        self._unit_term_access_group_uid = gfm_factory.unit_term_access_group_uid

        self.GRAM_TERM = gfm_factory.GRAM_TERM
        self.glossary_service = gfm_factory.service_provider.glossary_service

        self._mass_term = gfm_factory.mass_term
        self._transportation_units_term = gfm_factory.transportation_units_term
        self._freezing_units_term = gfm_factory.freezing_units_term
        self._production_amount_unit_term = gfm_factory.production_amount_unit_term

    def match_unit_term(self, unit: str, calc_graph: CalcGraph) -> Term | None:
        """Find term matching the unit name."""
        if unit.upper().replace(" ", "_") == self._production_amount_unit_term.name.upper():
            return self._production_amount_unit_term

        if unit.upper() in self.MASS_UNITS:
            return self._GRAM_PER[unit.upper()]

        # checking if the listed unit is among volume units
        elif unit.upper() in self.VOLUME_UNITS:
            return self._MILLILITER_PER[unit.upper()]

        else:
            try:
                unit_term = self.glossary_service.terms_by_xid_ag_uid[
                    (self.unit_to_eos_unit_xid(unit, self.glossary_service), self._unit_term_access_group_uid)
                ]
                return unit_term
            except KeyError:
                logger.debug(f"No matching unit term found for unit {unit} -- may it be abstract or unknown?")
                calc_graph.set_data_errors_log_entry(
                    f"No matching unit term found for unit {unit} -- may it be abstract or unknown?"
                )
                return None

    @staticmethod
    def mass_in_grams(mass: int | float, unit_term: Term) -> int | float:
        """Compute amount in grams."""
        try:
            return mass * unit_term.data.get("mass-in-g")
        except KeyError as e:
            logger.debug("Unhandled conversion for mass unit", unit_term=unit_term)
            raise RuntimeError("Unhandled conversion for mass unit", unit_term) from e

    @staticmethod
    def volume_in_milliliter(volume: int | float, unit_term: Term) -> int | float:
        """Compute amount in milliliter."""
        try:
            return volume * unit_term.data.get("volume-in-ml")
        except KeyError as e:
            logger.debug("Unhandled conversion for volume unit", unit=unit_term)
            raise RuntimeError("Unhandled conversion for volume unit", unit_term) from e

    def volume_to_grams(self, volume: int | float, unit_term: Term, density_gram_per_ml: int | float) -> int | float:
        """Covert volume amounts into grams."""
        volume_ml = self.volume_in_milliliter(volume, unit_term)
        grams = volume_ml * density_gram_per_ml

        return grams

    @staticmethod
    def get_term_data(term_data: dict, key: str) -> float | int:
        """Retrieve data field of the term."""
        result = term_data.get(key, 0)

        if not result:
            logger.debug(f"Empty value of {key} in term data! Assuming 0...", term_data=term_data)

        return result

    @staticmethod
    def convert_between_same_unit_types(
        origin_amount: float, origin_unit: Term, target_unit: Term, base_unit: str
    ) -> float:
        """Convert amount between two of the same unit types (e.g., mass units, energy units)."""
        amount_in_base_unit = origin_amount * origin_unit.data.get(base_unit)
        amount_in_target_unit = amount_in_base_unit / target_unit.data.get(base_unit)
        return amount_in_target_unit

    def convert_to_target(
        self,
        amount: int | float,
        unit_raw_name: str,
        unit_term: Term | None,
        ingredient_term_data: dict | None,
        target_unit_term: Term,
        calc_graph: CalcGraph,
    ) -> tuple[int | float, Term] | tuple[None, None]:
        """Convert amount to target unit."""
        if unit_term and target_unit_term == unit_term:
            return amount, unit_term

        base_unit = {
            self._transportation_units_term.uid: "payload-distance-in-ton-km",
            self._freezing_units_term.uid: "freezing-in-kg-day",
        }

        if target_unit_term.sub_class_of == self._mass_term.uid:
            processed_amount, processed_unit = self.convert(
                amount,
                unit_raw_name,
                unit_term,
                ingredient_term_data,
                calc_graph,
            )
            if processed_amount and processed_unit:
                if target_unit_term in self._GRAM_PER.values():
                    processed_amount = processed_amount / self.mass_in_grams(1.0, target_unit_term)
                elif target_unit_term in self._MILLILITER_PER.values():
                    density = self.get_term_data(ingredient_term_data, "density") if ingredient_term_data else None

                    if not density:
                        logger.debug("Assuming density equals 1...")
                        density = 1
                    processed_amount = processed_amount / self.volume_to_grams(1.0, target_unit_term, density)

                return processed_amount, target_unit_term

        if target_unit_term.sub_class_of in base_unit:
            if unit_term and unit_term.sub_class_of == target_unit_term.sub_class_of:
                return (
                    self.convert_between_same_unit_types(
                        amount, unit_term, target_unit_term, base_unit[target_unit_term.sub_class_of]
                    ),
                    target_unit_term,
                )
            else:
                return None, None
        else:
            return None, None

    def convert(
        self,
        amount: int | float,
        unit_raw_name: str,
        unit_term: Term | None,
        ingredient_term_data: dict | None,
        calc_graph: CalcGraph,
    ) -> tuple[int | float, Term] | tuple[None, None]:
        """Convert mass and volume amount to gram."""
        # checking if the listed unit is among abstract units
        if not unit_term and unit_raw_name.upper() in self.ABSTRACT_UNITS and ingredient_term_data:
            term_data_weight = self.get_term_data(ingredient_term_data, "unit-weight")

            if not term_data_weight:
                return 0, self.GRAM_TERM

            return term_data_weight * amount, self.GRAM_TERM

        # checking if the listed unit is among mass units
        elif unit_term and unit_term in self._GRAM_PER.values():
            return self.mass_in_grams(amount, unit_term), self.GRAM_TERM

        # checking if the listed unit is among volume units
        elif unit_term and unit_term in self._MILLILITER_PER.values():
            density = self.get_term_data(ingredient_term_data, "density") if ingredient_term_data else None

            if not density:
                logger.debug("Assuming density equals 1...")
                density = 1

            return self.volume_to_grams(amount, unit_term, density), self.GRAM_TERM

        else:
            logger.debug("Unknown unit during conversion to grams", unit=unit_raw_name, term_data=ingredient_term_data)
            calc_graph.set_data_errors_log_entry(
                f"Unknown unit {unit_raw_name} during conversion to grams. "
                f"(ingredient term data: {ingredient_term_data})"
            )

            return None, None

    @staticmethod
    def unit_convert_to_g_kj(qty: ReferencelessQuantityProp, terms_by_uid: dict, g_term: Term, kj_term: Term) -> float:
        """Convert mass and energy amount to gram and kilojoule."""
        unit = terms_by_uid[qty.unit_term_uid]
        if unit.sub_class_of == g_term.sub_class_of:
            return UnitWeightConverter.convert_between_same_unit_types(qty.value, unit, g_term, "mass-in-g")
        elif unit.sub_class_of == kj_term.sub_class_of:
            return UnitWeightConverter.convert_between_same_unit_types(qty.value, unit, kj_term, "energy-in-kj")

    @staticmethod
    def unit_to_eos_unit_xid(unit_string: str, glossary_service: GlossaryService) -> str:
        """Convert unit string to EOS unit XID."""
        if unit_string.upper() in glossary_service.alias_to_xid:
            return glossary_service.alias_to_xid[unit_string.upper()]
        else:
            eos_term_name = f"EOS_{re.sub(' ', '-', unit_string.lower())}"
            return eos_term_name

    @staticmethod
    def get_flow_amount_in_grams(flow_node: Node, gram_term: Term) -> float:
        """Obtain flow amount in grams."""
        flow_amount = flow_node.amount.value
        flow_unit = flow_node.amount.get_unit_term()
        if flow_unit.sub_class_of == gram_term.sub_class_of:
            return UnitWeightConverter.convert_between_same_unit_types(flow_amount, flow_unit, gram_term, "mass-in-g")
        else:
            return 0.0

    @staticmethod
    def get_production_amount_in_grams(activity_node: Node, gram_term: Term) -> float:
        """Obtain production amount in grams."""
        production_amount = activity_node.production_amount.value
        production_unit = activity_node.production_amount.get_unit_term()
        if production_unit.sub_class_of == gram_term.sub_class_of:
            return UnitWeightConverter.convert_between_same_unit_types(
                production_amount, production_unit, gram_term, "mass-in-g"
            )
        else:
            return production_amount

    @staticmethod
    def unit_term_from_unit_name(unit_name: str) -> Term:
        service_provider = ServiceLocator().service_provider
        root_unit_term = service_provider.glossary_service.root_subterms.get("EOS_units")

        if unit_name:
            try:
                eos_term_name = UnitWeightConverter.unit_to_eos_unit_xid(unit_name, service_provider.glossary_service)
                unit_term: Term | None = service_provider.glossary_service.terms_by_xid_ag_uid[
                    (
                        eos_term_name,
                        root_unit_term.access_group_uid,
                    )
                ]
            except KeyError:
                # Unknown unit: For now, we just add a root unit term for a generic unit.
                unit_term = root_unit_term
        else:
            # Unknown unit: For now, we just add a root unit term for a generic unit.
            unit_term = root_unit_term

        return unit_term


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = UnitWeightConversionGapFillingFactory
