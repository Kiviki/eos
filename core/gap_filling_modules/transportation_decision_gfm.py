"Transportation decision gap filling module."
import uuid
from typing import Optional

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util import find_access_group_uid_by_name
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.conservation_gfm import ConservationGapFillingWorker
from gap_filling_modules.transportation_mode_distance_gfm import TransportModeDistanceGapFillingWorker
from gap_filling_modules.transportation_util.enum import ServiceTransportModeEnum
from gap_filling_modules.transportation_util.settings import TransportModeDistanceGfmSettings
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker, UnitWeightConverter
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes import ActivityNode, FoodProcessingActivityNode, FoodProductFlowNode, PracticeFlowNode
from core.domain.nodes.activity.transport_activity import TransportActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp, RawName
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.props.transport_modes_distances_prop import TransportDataset, TransportModesDistancesProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class TransportDecisionGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        # transport, which is required for this gap filling module, are only added to flow nodes.
        if not isinstance(self.node, FlowNode):
            return False

        # decide if we want to run on this brightway node
        if isinstance(
            self.node, FlowNode
        ) and self.gfm_factory.service_provider.modeled_activity_node_service.should_bw_node_be_computed(
            self.node, self.__class__.__name__
        ):
            return True

        if isinstance(self.node, FoodProductFlowNode):
            return True

        return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        if getattr(self.node.transport, "cheapest_mode", None):
            logger.debug(f"[TransportDecision] {self.node} already has a transportation mode specified --> cancel.")

            return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(TransportModeDistanceGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                "[TransportDecision] wait for TransportModeDistanceGapFillingWorker to finish" " --> not can_run_now."
            )

            return GapFillingWorkerStatusEnum.reschedule

        if not (self.node.transport and isinstance(self.node.transport, TransportModesDistancesProp)):
            logger.debug(f"[TransportDecision] {self.node} has no transport --> cancel.")

            return GapFillingWorkerStatusEnum.cancel

        if all(
            [self.node.transport.distances[mode].main_carriage.value == 0.0 for mode in self.node.transport.distances]
        ):
            logger.debug(f"[TransportDecision] Transportation has distance 0.0 in {self.node} --> cancel.")

            return GapFillingWorkerStatusEnum.cancel

        if getattr(self.node, GFM_STATE_PROP_NAME) and (
            conservation_gfm_status := getattr(self.node, GFM_STATE_PROP_NAME).worker_states.get(
                ConservationGapFillingWorker.__name__
            )
        ):
            if conservation_gfm_status == NodeGfmStateEnum.scheduled:
                logger.debug("[TransportDecision] wait for Conservation to finish" " --> not can_run_now.")
                return GapFillingWorkerStatusEnum.reschedule

        # TransportationDecisionGFM requires child activity production amount, which is added by the
        # UnitWeightConversionGFM in the case of food product nodes.
        sub_node = self.node.get_sub_nodes()
        if sub_node and isinstance(sub_node[0], FoodProcessingActivityNode):
            if getattr(sub_node[0], GFM_STATE_PROP_NAME) and (
                unit_weight_conversion_gfm_status := getattr(sub_node[0], GFM_STATE_PROP_NAME).worker_states.get(
                    UnitWeightConversionGapFillingWorker.__name__
                )
            ):
                if unit_weight_conversion_gfm_status == NodeGfmStateEnum.scheduled:
                    logger.debug(
                        "[TransportDecision] wait for UnitWeightConversionGFM to finish" " --> not can_run_now."
                    )
                    return GapFillingWorkerStatusEnum.reschedule
            else:
                logger.debug("[TransportDecision] wait for UnitWeightConversionGFM to finish" " --> not can_run_now.")
                return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        transport_prop: TransportModesDistancesProp = self.node.transport

        if transport_prop is None:
            logger.error(f"No `transport` property found in {self.node}!")
            calc_graph.set_data_errors_log_entry(f"No `transport` property found in {self.node}!")

            return

        qualified_modes = await self.get_valid_transportation_modes(transport_prop)

        if not qualified_modes:
            logger.error(f"No valid transportation modes found in {self.node}!")
            calc_graph.set_data_errors_log_entry(f"No valid transportation modes found in {self.node}!")
            return

        amount = self.node.amount
        if not amount:
            err_msg = f"Cannot run TransportDecisionGFM because no amount property found in {self.node}!"
            logger.error(err_msg)
            calc_graph.set_data_errors_log_entry(err_msg)
            return

        if amount.get_unit_term().xid != "EOS_kilogram":
            err_msg = f"Cannot run TransportDecisionGFM because {self.node} has a non-kilogram " f"amount!"
            logger.error(err_msg)
            calc_graph.set_data_errors_log_entry(err_msg)
            return

        minimal_mode = await self.get_minimal_cost_transportation_mode(transport_prop, qualified_modes)

        duplicated_transport_prop = transport_prop.duplicate()
        duplicated_transport_prop.cheapest_mode = minimal_mode
        duplicated_transport_prop.qualified_modes = qualified_modes

        # write it into a prop
        prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="transport",
            prop=duplicated_transport_prop,
        )

        await calc_graph.apply_mutation(prop_mutation)

        # create nodes

        # 1) delete link between origin split & food_prod activity

        old_food_product_node: Node = self.node.get_sub_nodes()[0]
        child_food_production_amount_unit = old_food_product_node.production_amount.get_unit_term()
        remove_edge_mutation = RemoveEdgeMutation(
            created_by_module=self.__class__.__name__,
            from_node_uid=self.node.uid,
            to_node_uid=old_food_product_node.uid,
        )
        await calc_graph.apply_mutation(remove_edge_mutation)

        # 2) create transport activity

        transport_activity_node_uid = uuid.uuid4()

        # Copy over the location of old food product node.
        if old_food_product_node.activity_location:
            if isinstance(old_food_product_node.activity_location, str):
                old_food_product_node_location = old_food_product_node.activity_location
            else:
                old_food_product_node_location = [loc.duplicate() for loc in old_food_product_node.activity_location]
        else:
            old_food_product_node_location = None

        # TODO: Should be modified to generic ActivityNode in the future.
        # Currently, Making this a generic ActivityNode breaks tests.
        transport_activity_node = FoodProcessingActivityNode.model_construct(
            uid=transport_activity_node_uid,
            access_group_uid=old_food_product_node.access_group_uid,
            access_group_xid=old_food_product_node.access_group_xid,
            gfm_state=GfmStateProp.unvalidated_construct(
                worker_states={
                    # No more origin split necessary:
                    "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                    # Ingredient amount estimation should be already finished:
                    "IngredientAmountEstimatorGapFillingWorker": NodeGfmStateEnum.canceled,
                    # No Location gap filling necessary on this dummy node:
                    "LocationGapFillingWorker": NodeGfmStateEnum.canceled,
                }
            ),
            activity_location=old_food_product_node_location,
            production_amount=QuantityProp.unvalidated_construct(
                value=1.0,
                unit_term_uid=child_food_production_amount_unit.uid,
                for_reference=ReferenceAmountEnum.self_reference,
            ),
        )

        await calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                new_node=transport_activity_node,
                parent_node_uid=self.node.uid,
                copy=False,
            )
        )

        # 3) copy origin split

        transported_origin_split_flow_node_uid = uuid.uuid4()

        await calc_graph.apply_mutation(
            DuplicateNodeMutation(
                created_by_module=self.__class__.__name__,
                new_node_uid=transported_origin_split_flow_node_uid,
                source_node_uid=self.node.uid,
                parent_node_uid=transport_activity_node_uid,
                gfms_to_not_schedule=("OriginGapFillingWorker",),
                duplicate_child_nodes=False,
            )
        )
        transported_origin_split_flow_node = calc_graph.get_node_by_uid(transported_origin_split_flow_node_uid)

        # overwriting `amount` property value to 1
        # because otherwise total co2 value goes down,
        # as unnecessary consumption is copied from original node
        prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=transported_origin_split_flow_node_uid,
            prop_name="amount",
            prop=QuantityProp.unvalidated_construct(
                value=1.0,
                unit_term_uid=child_food_production_amount_unit.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        await calc_graph.apply_mutation(prop_mutation)

        add_edge_mutation = AddEdgeMutation(
            created_by_module=self.__class__.__name__,
            from_node_uid=transported_origin_split_flow_node.uid,
            to_node_uid=old_food_product_node.uid,
        )
        await calc_graph.apply_mutation(add_edge_mutation)

        # 4) create 3 carriage flow nodes

        if transport_prop.distances[minimal_mode].pre_carriage.value != 0:
            await self.set_carriage_flow_node(
                calc_graph,
                "Pre-carriage",
                transport_prop.distances[minimal_mode].pre_carriage.value,
                transport_prop.co2[minimal_mode].pre_carriage.value,
                self.gfm_factory.get_transportation_mode_term(ServiceTransportModeEnum.GROUND.value),
                transport_activity_node_uid,
            )

        await self.set_carriage_flow_node(
            calc_graph,
            "Main carriage",
            transport_prop.distances[minimal_mode].main_carriage.value,
            transport_prop.co2[minimal_mode].main_carriage.value,
            self.gfm_factory.get_transportation_mode_term(minimal_mode),
            transport_activity_node_uid,
        )

        if transport_prop.distances[minimal_mode].post_carriage.value != 0:
            await self.set_carriage_flow_node(
                calc_graph,
                "Post-carriage",
                transport_prop.distances[minimal_mode].post_carriage.value,
                transport_prop.co2[minimal_mode].post_carriage.value,
                self.gfm_factory.get_transportation_mode_term(ServiceTransportModeEnum.GROUND.value),
                transport_activity_node_uid,
            )

        # 5) create freezing node
        assert isinstance(self.node, FlowNode)
        tag_term_xids = self.node.tag_term_xids
        cooling_term = None

        # check if the product is specified as cooled or frozen
        for term_xid in tag_term_xids:
            if term_xid == self.gfm_factory.cooled_term.xid:
                cooling_term = self.gfm_factory.cooled_term
                break
            elif term_xid == self.gfm_factory.frozen_term.xid:
                cooling_term = self.gfm_factory.frozen_term
                break

        if cooling_term is not None:
            travel_time = self.get_transport_mode_travel_time(
                minimal_mode,
                transport_prop.distances[minimal_mode],
            )
            product_name = self.node.product_name
            source_data_raw = None
            if isinstance(product_name, NamesProp):
                source_data_raw = product_name.source_data_raw
            elif isinstance(product_name, (list, DeepListView)):
                source_data_raw = product_name

            await self.set_infrastructure_or_cooling_or_freezing_node(
                calc_graph,
                transport_activity_node_uid,
                travel_time,
                self.gfm_factory.kg_hour_term,
                cooling_term,
                source_data_raw,
            )

    async def get_valid_transportation_modes(
        self,
        transport_modes_distances: TransportModesDistancesProp,
    ) -> list[ServiceTransportModeEnum]:
        """Calculates duration of transportation for each transportation mode.

        Returns:
            transportation modes where duration of transportation
            is lesser than expiration period of product's perishability/storage parameters pair.
        """
        if hasattr(self.node, "storage_time") and self.node.storage_time:
            assert self.node.storage_time.get_unit_term().data.get(
                "time-in-hour"
            ), "Storage time must be in time units."
            qualifying_travel_modes = await self.get_transport_modes_checked_for_perishability(
                transport_modes_distances
            )
        else:
            qualifying_travel_modes = list(transport_modes_distances.distances.keys())

        return qualifying_travel_modes

    async def get_transport_modes_checked_for_perishability(
        self, transport_modes_distances: TransportModesDistancesProp
    ) -> list[str]:
        """Check for perishability.

        Returns:
            transportation modes where duration of transportation is lesser than
            expiration period of product's perishability/storage parameters pair.
        """
        qualifying_travel_modes = []

        for mode, distances in transport_modes_distances.distances.items():
            total_travel_time = self.get_transport_mode_travel_time(mode, distances)

            storage_time_in_hour = (
                self.node.storage_time.value * self.node.storage_time.get_unit_term().data["time-in-hour"]
            )
            if total_travel_time < storage_time_in_hour:
                qualifying_travel_modes.append(mode)

        return qualifying_travel_modes

    def get_transport_mode_travel_time(self, mode: ServiceTransportModeEnum, mode_dict: TransportDataset) -> float:
        total_travel_time = 0

        for carriage_mode in (
            "pre_carriage",
            "post_carriage",
        ):
            if getattr(mode_dict, carriage_mode).value > 0:
                total_travel_time += self.get_travel_time(
                    getattr(mode_dict, carriage_mode).value,
                    ServiceTransportModeEnum.GROUND,
                )

        total_travel_time += self.get_travel_time(mode_dict.main_carriage.value, mode)

        return total_travel_time

    @staticmethod
    def get_travel_time(distance: float, mode: str) -> float:
        # km / h
        travel_speed = {
            ServiceTransportModeEnum.AIR: 500,
            ServiceTransportModeEnum.GROUND: 45,
            ServiceTransportModeEnum.SEA: 26,
            ServiceTransportModeEnum.TRAIN: 40,
        }

        # h
        travel_loading_time = {
            ServiceTransportModeEnum.AIR: 6,
            ServiceTransportModeEnum.GROUND: 3,
            ServiceTransportModeEnum.SEA: 48,
            ServiceTransportModeEnum.TRAIN: 24,
        }

        return distance / travel_speed[mode] + travel_loading_time[mode]  # noqa

    @staticmethod
    async def get_minimal_cost_transportation_mode(
        transport_modes_distances: TransportModesDistancesProp,
        qualified_travel_modes: list,
    ) -> ServiceTransportModeEnum:
        """Get minimal cost transportation mode.

        Calculates total transportation price for each qualifying transportation mode
        and returns the name of the cheapest transportation mode.
        """
        mile_to_km_constant = 0.621371192

        # prices per mile -> converting to km
        distance_prices = {
            ServiceTransportModeEnum.AIR: 0.01 * mile_to_km_constant,
            ServiceTransportModeEnum.GROUND: 0.08 * mile_to_km_constant,
            ServiceTransportModeEnum.SEA: 0.01 * mile_to_km_constant,
            ServiceTransportModeEnum.TRAIN: 0.03 * mile_to_km_constant,
        }
        loading_prices = {
            ServiceTransportModeEnum.AIR: 2085.0,
            ServiceTransportModeEnum.GROUND: 84.62,
            ServiceTransportModeEnum.SEA: 119.6646,
            ServiceTransportModeEnum.TRAIN: 99.578,
        }
        total_prices = {}

        for mode in qualified_travel_modes:
            total_price = 0

            for carriage_mode in (
                "pre_carriage",
                "post_carriage",
            ):
                if getattr(transport_modes_distances.distances[mode], carriage_mode).value > 0:
                    total_price += (
                        distance_prices[ServiceTransportModeEnum.GROUND]
                        * getattr(transport_modes_distances.distances[mode], carriage_mode).value
                        + loading_prices[ServiceTransportModeEnum.GROUND]
                    )

            total_price += (
                distance_prices[mode] * transport_modes_distances.distances[mode].main_carriage.value
                + loading_prices[mode]
            )
            total_prices[mode] = total_price

        # pick one with the least cost
        return min(total_prices, key=total_prices.get)

    async def set_carriage_flow_node(
        self,
        calc_graph: CalcGraph,
        carriage_type: str,
        minimal_mode_distance: float,
        co2eq_value: float,
        transport_mode_term: Term,
        transport_activity_node_uid: uuid.UUID,
    ) -> None:
        # creating a carriage flow node

        carriage_flow_node = PracticeFlowNode(
            uid=uuid.uuid4(),
        )  # TODO Should not be an ingredient?
        carriage_flow_node.product_name = NamesProp.unvalidated_construct(
            terms=[GlossaryTermProp.unvalidated_construct(term_uid=transport_mode_term.uid)],
            source_data_raw=[RawName(value=carriage_type, language="en")],
        )

        carriage_flow_node.amount_in_original_source_unit = QuantityProp.unvalidated_construct(
            value=minimal_mode_distance,
            unit_term_uid=self.gfm_factory.kg_km_term.uid,
            for_reference=ReferenceAmountEnum.self_reference,
        )

        carriage_flow_node.gfm_state = GfmStateProp.unvalidated_construct(
            worker_states={
                "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                "TransportModeDistanceGapFillingWorker": NodeGfmStateEnum.canceled,
            }
        )

        logger.debug(f"create recipe AddNodeMutation for {carriage_flow_node}...")
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=transport_activity_node_uid,
            new_node=carriage_flow_node,
            copy=True,
        )
        await calc_graph.apply_mutation(add_child_node_mutation)

        # creating a carriage activity node

        carriage_activity_node = TransportActivityNode(
            uid=uuid.uuid4(),
        )
        carriage_activity_node.production_amount = QuantityProp.unvalidated_construct(
            value=1.0, unit_term_uid=self.gfm_factory.ton_km_term.uid, for_reference=ReferenceAmountEnum.self_reference
        )

        logger.debug(f"create recipe AddNodeMutation for {carriage_activity_node}...")
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=carriage_flow_node.uid,
            new_node=carriage_activity_node,
            copy=True,
        )
        await calc_graph.apply_mutation(add_child_node_mutation)

        # creating nodes for fuel consumption

        await self.create_fuel_consumption_node(
            calc_graph,
            co2eq_value / minimal_mode_distance,
            transport_mode_term,
            carriage_activity_node.uid,
        )

        # creating nodes for infrastructure

        await self.set_infrastructure_or_cooling_or_freezing_node(
            calc_graph,
            carriage_activity_node.uid,
            1,
            self.gfm_factory.ton_km_term,
            transport_mode_term,
            carriage_flow_node.product_name.source_data_raw,
        )

    async def set_infrastructure_or_cooling_or_freezing_node(
        self,
        calc_graph: CalcGraph,
        activity_node_uid: uuid.UUID,
        flow_amount: float,
        flow_unit_term: Term,
        term: Term,
        source_data_raw: Optional[list] = None,
    ) -> None:
        flow_node = PracticeFlowNode(
            uid=uuid.uuid4(),
        )
        flow_node.product_name = NamesProp.unvalidated_construct(
            terms=[GlossaryTermProp.unvalidated_construct(term_uid=term.uid)], source_data_raw=source_data_raw
        )

        flow_node.amount_in_original_source_unit = QuantityProp.unvalidated_construct(
            value=flow_amount, unit_term_uid=flow_unit_term.uid, for_reference=ReferenceAmountEnum.self_reference
        )

        flow_node.gfm_state = GfmStateProp.unvalidated_construct(
            worker_states={
                "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                "TransportModeDistanceGapFillingWorker": NodeGfmStateEnum.canceled,
            }
        )

        logger.debug(f"create recipe AddNodeMutation for {flow_node}...")
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=activity_node_uid,
            new_node=flow_node,
            copy=True,
        )
        await calc_graph.apply_mutation(add_child_node_mutation)

    async def create_fuel_consumption_node(
        self,
        calc_graph: CalcGraph,
        co2eq_value: float,
        transport_mode_term: Term,
        carriage_activity_node_uid: uuid.UUID,
    ) -> None:
        emission_flow_node = FlowNode(
            uid=uuid.uuid4(),
            amount_in_original_source_unit={"value": co2eq_value},
            raw_input={"type": "biosphere"},
        )
        emission_flow_node.product_name = NamesProp.unvalidated_construct(
            terms=[GlossaryTermProp.unvalidated_construct(term_uid=transport_mode_term.uid)],
            source_data_raw=[RawName(language="en", value=f"Fuel consumption of {transport_mode_term.name}")],
        )

        emission_flow_node.amount_in_original_source_unit = QuantityProp.unvalidated_construct(
            value=co2eq_value,
            unit_term_uid=self.gfm_factory.ton_term.uid,
            for_reference=ReferenceAmountEnum.self_reference,
        )
        emission_flow_node.gfm_state = GfmStateProp.unvalidated_construct(
            worker_states={
                "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
            }
        )

        logger.debug(f"create recipe AddNodeMutation for {emission_flow_node}...")
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=carriage_activity_node_uid,
            new_node=emission_flow_node,
            copy=True,
        )
        await calc_graph.apply_mutation(add_child_node_mutation)

        if self.gfm_factory.co2_emission_node_uid:
            emission_node = calc_graph.get_node_by_uid(self.gfm_factory.co2_emission_node_uid)

            if emission_node is None:
                node_to_add = await self.gfm_factory.service_provider.node_service.find_by_uid(
                    self.gfm_factory.co2_emission_node_uid,
                )
                logger.debug(f"create AddNodeMutation for {node_to_add}...")
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=emission_flow_node.uid,
                    new_node=node_to_add,
                    copy=True,
                )
                await calc_graph.apply_mutation(add_child_node_mutation)
            else:
                logger.debug(
                    f"do not add child node because it already exists in the graph ({emission_node}); "
                    f"only add edge."
                )
                add_edge_mutation = AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=emission_flow_node.uid,
                    to_node_uid=emission_node.uid,
                )
                await calc_graph.apply_mutation(add_edge_mutation)

            added_emission_node = calc_graph.get_node_by_uid(self.gfm_factory.co2_emission_node_uid)
            if not isinstance(added_emission_node.production_amount, QuantityProp):
                emission_unit = added_emission_node.production_amount.unit
                emission_unit_term = self.gfm_factory.service_provider.glossary_service.terms_by_xid_ag_uid[
                    (
                        UnitWeightConverter.unit_to_eos_unit_xid(
                            emission_unit, self.gfm_factory.service_provider.glossary_service
                        ),
                        self.gfm_factory.unit_term_access_group_uid,
                    )
                ]

                production_amount_prop_mutation = PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=added_emission_node.uid,
                    prop_name="production_amount",
                    prop=QuantityProp.unvalidated_construct(
                        value=1.0,
                        unit_term_uid=emission_unit_term.uid,
                        for_reference=ReferenceAmountEnum.self_reference,
                        source_data_raw=added_emission_node.production_amount.model_dump(),
                    ),
                )
                await calc_graph.apply_mutation(production_amount_prop_mutation)


class TransportDecisionGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.root_transportation_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")

        self.transportation_mode_terms: dict = {}
        self.air_term: Term = None  # noqa
        self.sea_term: Term = None  # noqa
        self.train_term: Term = None  # noqa
        self.truck_term: Term = None  # noqa
        self.cooled_term: Term = None  # noqa
        self.frozen_term: Term = None  # noqa

        self.transport_decision_gfm_settings = TransportModeDistanceGfmSettings()

        # self.service_provider.node_service.find_by_uid()
        self.co2_emission_node_uid: uuid.UUID = None  # noqa

        self.unit_term_access_group_uid: uuid.UUID | None = None

        self.kilogram_term = Term(data={}, name="", sub_class_of=None)
        self.kg_hour_term = Term(data={}, name="", sub_class_of=None)
        self.kg_km_term = Term(data={}, name="", sub_class_of=None)
        self.ton_km_term = Term(data={}, name="", sub_class_of=None)
        self.ton_term = Term(data={}, name="", sub_class_of=None)
        self.perishable_term = Term(data={}, name="", sub_class_of=None)
        self.high_perishable_term = Term(data={}, name="", sub_class_of=None)

    async def init_cache(self) -> None:
        self.air_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_AIR",
            str(self.root_transportation_term.access_group_uid),
        )
        assert self.air_term, "Could not find air transportation term."

        self.sea_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_SEA",
            str(self.root_transportation_term.access_group_uid),
        )
        assert self.sea_term, "Could not find sea transportation term."

        # TODO: unused for now
        self.train_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_TRAIN",
            str(self.root_transportation_term.access_group_uid),
        )
        assert self.train_term, "Could not find train transportation term."

        self.truck_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_GROUND",
            str(self.root_transportation_term.access_group_uid),
        )
        assert self.truck_term, "Could not find truck transportation term."

        self.transportation_mode_terms = {
            ServiceTransportModeEnum.AIR.value: self.air_term,
            ServiceTransportModeEnum.GROUND.value: self.truck_term,
            ServiceTransportModeEnum.SEA.value: self.sea_term,
            ServiceTransportModeEnum.TRAIN.value: self.train_term,
        }

        self.co2_emission_node_uid = await self.postgres_db.product_mgr.find_uid_by_xid(
            self.transport_decision_gfm_settings.EATERNITY_NAMESPACE_UUID,
            "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        )

        if not self.co2_emission_node_uid:
            logger.error("CO2 emission node is not present in the database!")

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.unit_term_access_group_uid = root_unit_term.access_group_uid
        self.kilogram_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_kilogram", str(root_unit_term.access_group_uid)
        )
        self.kg_km_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_kilogram-kilometer", str(root_unit_term.access_group_uid)
        )
        self.ton_km_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_ton-kilometer", str(root_unit_term.access_group_uid)
        )
        self.kg_hour_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_kilogram-hour", str(root_unit_term.access_group_uid)
        )
        self.ton_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_ton", str(root_unit_term.access_group_uid)
        )

        foodex2_access_group_uid = await find_access_group_uid_by_name(
            postgres_db=self.postgres_db, namespace_name="FoodEx2 Glossary Terms namespace"
        )

        self.cooled_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("J0131", foodex2_access_group_uid)
        ]
        assert self.cooled_term, "Could not find cooled storage term."

        self.frozen_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("J0136", foodex2_access_group_uid)
        ]
        assert self.frozen_term, "Could not find frozen storage term."

        root_perishability_term = self.service_provider.glossary_service.root_subterms.get("EOS_Perishability")

        self.perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.perishable_term, "Could not find 'perishable' perishability term."

        self.high_perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_HIGH-PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.high_perishable_term, "Could not find 'perishable' perishability term."

    def get_transportation_mode_term(self, mode: str) -> Term:
        return self.transportation_mode_terms[mode]

    def spawn_worker(self, node: Node) -> TransportDecisionGapFillingWorker:
        return TransportDecisionGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = TransportDecisionGapFillingFactory
