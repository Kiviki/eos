from typing import TYPE_CHECKING
from uuid import UUID

if TYPE_CHECKING:
    from database.postgres.postgres_db import PostgresDb


async def find_access_group_uid_by_name(postgres_db: "PostgresDb", namespace_name: str) -> UUID:
    groups = await postgres_db.get_term_mgr().find_all_term_access_groups()
    access_group_uid: UUID | str | None = None

    for group in groups:
        if group.data.get("name").endswith(namespace_name):
            access_group_uid = group.uid
            break

    assert access_group_uid, f"Could not find access group with name [{namespace_name}]"

    if isinstance(access_group_uid, str):
        access_group_uid = UUID(access_group_uid)

    return access_group_uid
