from enum import Enum


class GapFillingWorkerStatusEnum(str, Enum):
    ready = "ready"
    reschedule = "reschedule"
    cancel = "cancel"


class NodeGfmStateEnum(str, Enum):
    scheduled = "S"
    finished = "F"
    canceled = "C"
