"""Compute Vitascore."""

import re
import uuid
from enum import Enum

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.daily_food_unit_gfm import DailyFoodUnitGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp, VitascoreProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class VitascoreVersionEnum(str, Enum):
    """Enum for Vitascore version: legacy or current."""

    legacy = "legacy"
    current = "current"


class VitascoreGapFillingWorker(AbstractGapFillingWorker):
    """Vitascore gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "VitascoreGapFillingFactory"):
        """Compute VitaScore from aggregated global burden of disease risk factors."""
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Only run on FoodProductFlowNodes."""
        if isinstance(self.node, FoodProductFlowNode):
            return True
        else:
            logger.debug("[Vitascore] on a non-FoodProductFlowNode --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform vitascore calculation after all aggregation is complete."""

        def dfu_scheduled() -> bool:
            """Determine if Daily food unit GFM is scheduled."""
            if gfm_status := self.node.gfm_state:
                return (
                    gfm_status.worker_states.get(DailyFoodUnitGapFillingWorker.__name__)
                    == NodeGfmStateEnum.scheduled.value
                )
            else:
                return False

        if dfu_scheduled():
            logger.debug("Waiting for DailyFoodUnitGFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if not self.node.daily_food_unit:
            logger.debug("[Vitascore] cancel GFM worker because daily food unit is not present.")
            return GapFillingWorkerStatusEnum.cancel

        if not self.node.food_categories or not self.node.food_categories.quantities:
            logger.debug("[Vitascore] cancel GFM worker because food category is not present.")
            return GapFillingWorkerStatusEnum.cancel

        if aggregated_nutrients := self.node.aggregated_nutrients:
            if (
                self.gfm_factory.energy_term.uid
                not in aggregated_nutrients.amount_for_activity_production_amount().quantities
            ):
                energy_absent = True
            else:
                energy_absent = False

        elif nutrient_values := self.node.nutrient_values:
            if (
                self.gfm_factory.energy_term.uid
                not in nutrient_values.amount_for_activity_production_amount().quantities
            ):
                energy_absent = True
            else:
                energy_absent = False

        else:
            energy_absent = True

        if energy_absent:
            logger.debug("[Vitascore] cancel GFM worker because energy is not present.")
            return GapFillingWorkerStatusEnum.cancel

        logger.debug("[Vitascore] All required food categories aggregated --> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Perform vitascore calculation."""
        food_categories_prop = self.node.food_categories
        dfu_prop = self.node.daily_food_unit

        if (
            self.node.aggregated_nutrients is not None
            and self.gfm_factory.energy_term.uid in self.node.aggregated_nutrients.quantities
        ):
            nutrient_prop = self.node.aggregated_nutrients
        else:
            nutrient_prop = self.node.nutrient_values

        # {vitascore version: {for_reference_amount: {dietary_risk_factor: ReferencelessQuantityProp}}}
        vitascore_data: dict[
            VitascoreVersionEnum, dict[ReferenceAmountEnum, dict[uuid.UUID, ReferencelessQuantityProp]]
        ] = {}

        # Legacy vitascore method:
        vitascore_for_local = self.compute_legacy_vitascore(
            food_categories_prop, nutrient_prop, ReferenceAmountEnum.amount_for_activity_production_amount
        )

        if self.node.get_parent_nodes() and self.node.get_parent_nodes()[0].impact_assessment_supply_for_root:
            flow_vitascore_for_root = self.compute_legacy_vitascore(
                food_categories_prop, nutrient_prop, ReferenceAmountEnum.amount_for_root_node
            )
        else:
            flow_vitascore_for_root = None

        flow_vitascore_data_legacy: dict[ReferenceAmountEnum, dict[uuid.UUID, ReferencelessQuantityProp]] = {}

        if vitascore_for_local is not None:
            flow_vitascore_data_legacy[ReferenceAmountEnum.amount_for_activity_production_amount] = vitascore_for_local
        if flow_vitascore_for_root is not None:
            flow_vitascore_data_legacy[ReferenceAmountEnum.amount_for_root_node] = flow_vitascore_for_root

        if (vitascore_for_local is not None) or (flow_vitascore_for_root is not None):
            vitascore_data[VitascoreVersionEnum.legacy] = flow_vitascore_data_legacy

        # Current vitascore method:
        vitascore_for_local = self.compute_vitascore(
            food_categories_prop, dfu_prop, ReferenceAmountEnum.amount_for_activity_production_amount
        )

        flow_vitascore_data_current: dict[ReferenceAmountEnum, dict[uuid.UUID, ReferencelessQuantityProp]] = {}

        if self.node.get_parent_nodes() and self.node.get_parent_nodes()[0].impact_assessment_supply_for_root:
            flow_vitascore_for_root = self.compute_vitascore(
                food_categories_prop, dfu_prop, ReferenceAmountEnum.amount_for_root_node
            )
        else:
            flow_vitascore_for_root = None

        if vitascore_for_local is not None:
            flow_vitascore_data_current[ReferenceAmountEnum.amount_for_activity_production_amount] = vitascore_for_local
        if flow_vitascore_for_root is not None:
            flow_vitascore_data_current[ReferenceAmountEnum.amount_for_root_node] = flow_vitascore_for_root

        if (vitascore_for_local is not None) or (flow_vitascore_for_root is not None):
            vitascore_data[VitascoreVersionEnum.current] = flow_vitascore_data_current

        if vitascore_data:
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="vitascore",
                    prop=VitascoreProp.unvalidated_construct(quantities=vitascore_data[VitascoreVersionEnum.current]),
                )
            )
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="vitascore_legacy",
                    prop=VitascoreProp.unvalidated_construct(quantities=vitascore_data[VitascoreVersionEnum.legacy]),
                )
            )

    def compute_legacy_vitascore(
        self,
        food_category: QuantityPackageProp,
        nutrient: QuantityPackageProp,
        for_reference_amount: ReferenceAmountEnum,
    ) -> dict[uuid.UUID, ReferencelessQuantityProp] | None:
        """Computes legacy vitascore: Scaling is by daily energy intake of 2000 kcal.

        :param food_category: "food_categories" property
        :param nutrient: "nutrient_values" or "aggregated_nutrients" property
        :param for_reference_amount: whether the computed amount is for local or root node.
        :return: legacy_vitascore. {dietary_risk_factor: quantity{"value": value, "unit": unit}}.
        """
        if nutrient:
            if nutrient_for_reference := getattr(nutrient, for_reference_amount)():
                raw_energy = nutrient_for_reference.quantities.get(self.gfm_factory.energy_term.uid)
                if raw_energy:
                    energy_kj = UnitWeightConverter.unit_convert_to_g_kj(
                        raw_energy,
                        self.gfm_factory.service_provider.glossary_service.terms_by_uid,
                        self.gfm_factory.gram_term,
                        self.gfm_factory.kj_term,
                    )
                    scaling = energy_kj / 8368
                    legacy_or_current = VitascoreVersionEnum.legacy
                    return self.vitascore_formula(food_category, scaling, legacy_or_current, for_reference_amount)

    def compute_vitascore(
        self,
        food_category: QuantityPackageProp,
        daily_food_unit: QuantityProp,
        for_reference_amount: ReferenceAmountEnum,
    ) -> dict[uuid.UUID, ReferencelessQuantityProp] | None:
        """Computes current vitascore: Scaling is by daily food unit.

        :param food_category: "food_categories" property
        :param daily_food_unit: "daily_food_unit" property
        :param for_reference_amount: whether the computed amount is for local or root node.
        :return: current_vitascore. {dietary_risk_factor: quantity{"value": value, "unit": unit}}.
        """
        if daily_food_unit:
            dfu = getattr(daily_food_unit, for_reference_amount)()
            if dfu:
                scaling = dfu.value
                legacy_or_current = VitascoreVersionEnum.current
                return self.vitascore_formula(food_category, scaling, legacy_or_current, for_reference_amount)

    def vitascore_formula(
        self,
        food_category: QuantityPackageProp,
        scaling: float,
        legacy_or_current: str,
        for_reference_amount: ReferenceAmountEnum,
    ) -> dict[uuid.UUID, ReferencelessQuantityProp]:
        """Formula for computing vitascore.

        :param food_category: "food_categories" property
        :param scaling: scale to account for daily intake of food.
        :param legacy_or_current: which disability adjusted life years (DALYs) values to use (legacy or current)
        :param for_reference_amount: whether the computed amount is for local or root node.
        :return: vitascore {dietary_risk_factor: quantity{"value": value, "unit": unit}}.
        """
        vitascore: dict[uuid.UUID, ReferencelessQuantityProp] = {
            self.gfm_factory.vitascore_term.uid: ReferencelessQuantityProp(
                value=0.0, unit_term_uid=self.gfm_factory.daly_unit_term.uid
            )
        }
        if food_category:
            food_categories_for_reference = getattr(food_category, for_reference_amount)()
            if food_categories_for_reference:
                food_categories_present = food_categories_for_reference.quantities
            else:
                food_categories_present = {}

            for idx, term in enumerate(self.gfm_factory.positive_terms + self.gfm_factory.negative_terms):
                if term.xid in self.gfm_factory.used_categories_xid:
                    t_1: float = float(term.data.get("tmrel").get("lower"))
                    t_2: float = float(term.data.get("tmrel").get("upper"))
                    if term.uid in food_categories_present:
                        x_r = float(food_categories_present[term.uid].value)
                    else:
                        x_r = 0.0

                    daly_r = float(term.data.get(legacy_or_current).get("amount"))

                    if idx < len(self.gfm_factory.positive_terms):
                        f_r = scaling * (t_2 / (t_2 - t_1)) - x_r / (t_2 - t_1)
                    else:
                        f_r = x_r / (t_2 - t_1) - scaling * (t_1 / (t_2 - t_1))

                    if f_r >= 1:
                        f_r = 1

                    if f_r <= 0:
                        f_r = 0

                    vitascore[term.uid] = ReferencelessQuantityProp(
                        value=daly_r * f_r, unit_term_uid=self.gfm_factory.daly_unit_term.uid
                    )
                    vitascore[self.gfm_factory.vitascore_term.uid].value += daly_r * f_r
            return vitascore


class VitascoreGapFillingFactory(AbstractGapFillingFactory):
    """Vitascore gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.positive_terms: [Term] = []
        self.negative_terms: [Term] = []
        self.all_categories_xid_to_uid: {str: uuid.UUID} = {}
        self.used_categories_xid: [str] = []

        self.vitascore_term: Term = Term(data={}, name="", sub_class_of=None)
        self.daly_unit_term: Term = Term(data={}, name="", sub_class_of=None)
        self.gram_term: Term = Term(data={}, name="", sub_class_of=None)
        self.kj_term: Term = Term(data={}, name="", sub_class_of=None)
        self.energy_term: Term = Term(data={}, name="", sub_class_of=None)

    async def init_cache(self) -> None:
        """Initialize cache for Vitascore gap filling factory."""
        root_food_categories_term = self.service_provider.glossary_service.root_subterms.get("Root_Food_Categories")
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        root_nutr_term = self.service_provider.glossary_service.root_subterms.get("EOS_nutrient_names")
        root_char_term = self.service_provider.glossary_service.root_subterms.get("Root_Impact_Assessments")

        ag_uid = root_food_categories_term.access_group_uid
        positive_term_uid = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                "EOS_GBD-Positive", str(ag_uid)
            )
        ).uid
        negative_term_uid = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                "EOS_GBD-Negative", str(ag_uid)
            )
        ).uid
        self.positive_terms = await self.service_provider.glossary_service.get_sub_terms_by_uid(
            str(positive_term_uid), depth=1
        )
        self.negative_terms = await self.service_provider.glossary_service.get_sub_terms_by_uid(
            str(negative_term_uid), depth=1
        )

        used_categories = [
            "Diet low in fruits",
            "Diet low in whole grains",
            "Diet low in vegetables",
            "Diet low in nuts and seeds",
            "Diet low in milk",
            "Diet high in sodium (salt)",
            "Diet high in processed meat",
            "Diet high in red meat",
        ]

        self.used_categories_xid = [f"EOS_{re.sub(' ','-', category)}" for category in used_categories]
        self.all_categories_xid_to_uid = {term.xid: term.uid for term in self.positive_terms + self.negative_terms}
        for used_category_xid in self.used_categories_xid:
            assert used_category_xid in self.all_categories_xid_to_uid  # Verify that the used categories are valid.

        self.vitascore_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_vita-score", str(root_char_term.access_group_uid)
        )

        self.daly_unit_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_dalys", str(root_unit_term.access_group_uid)
        )

        self.gram_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_gram", str(root_unit_term.access_group_uid)
        )

        self.kj_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_kilojoule", str(root_unit_term.access_group_uid)
        )

        self.energy_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_energy", str(root_nutr_term.access_group_uid)
        )

    def spawn_worker(self, node: Node) -> VitascoreGapFillingWorker:
        """Spawn Vitascore gap filling worker."""
        return VitascoreGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = VitascoreGapFillingFactory
