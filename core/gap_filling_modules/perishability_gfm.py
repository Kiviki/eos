import uuid
from typing import Optional

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingWorker
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingWorker
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from structlog import get_logger

from core.domain.nodes.flow.food_product_flow import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp, SourceEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class PerishabilityGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Only run on recipe (=child of root) node."""
        if self.node.uid == self.node.get_calculation().root_node_uid:
            return True
        else:
            logger.debug("[Perishability] not on root node --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether PerishabilityGFM can be executed."""
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for AddClientNodes GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for MatchProductNameGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for IngredientSplitter GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(NutrientSubdivisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for NutrientSubdivision GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(LinkTermToActivityNodeGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for LinkTermToActivity GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(AttachFoodTagsGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for AttachFoodTags GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Perishability] waiting for OriginGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Run the PerishabilityGFM."""
        logger.debug("Running PerishabilityGFM.")

        # first swipe
        stack = [self.node.uid]
        visited: set[uuid.UUID] = set()

        # {node_uid: perishability_type_term_xid}
        aggregated_perishabilities: dict[uuid.UUID, str] = {}
        aggregated_combined_product_tags: dict[uuid.UUID, bool] = {}

        while stack:
            current_flow = calc_graph.get_node_by_uid(stack[-1])
            perishability_of_current_flow = None
            current_product_is_combining_ingredients = False
            if isinstance(current_flow, FoodProductFlowNode):
                perishability_of_current_flow = self.determine_perishability(current_flow)
                current_product_is_combining_ingredients = current_flow.is_combining_ingredients()
            if stack[-1] not in visited:
                visited.add(stack[-1])
                if perishability_of_current_flow is not None:
                    aggregated_perishabilities[current_flow.uid] = perishability_of_current_flow

            all_sub_nodes_visited = True
            perishabilities_of_sub_flows = []
            one_of_subflows_is_combined_product = False
            for sub_activity in current_flow.get_sub_nodes():
                for sub_flow in sub_activity.get_sub_nodes():
                    if sub_flow.uid not in visited:
                        stack.append(sub_flow.uid)
                        all_sub_nodes_visited = False
                    else:
                        if sub_flow.uid in aggregated_perishabilities:
                            perishabilities_of_sub_flows.append(aggregated_perishabilities[sub_flow.uid])
                        if not current_product_is_combining_ingredients and aggregated_combined_product_tags.get(
                            sub_flow.uid, False
                        ):
                            one_of_subflows_is_combined_product = True

            if all_sub_nodes_visited:
                if isinstance(current_flow, FoodProductFlowNode):
                    glossary_tags_to_append = []
                    if perishability_of_current_flow is None:
                        perishability_xid = self.find_max_perishability(perishabilities_of_sub_flows)
                        perishability_term = self.gfm_factory.perishability_terms_dict[perishability_xid]
                        perishability_term_source = SourceEnum.eos_assumed

                        logger.debug(
                            "create PropListMutation to add perishability tag",
                            node=self.node,
                            perishability_term=perishability_term,
                        )

                        glossary_tags_to_append.append(
                            GlossaryTermProp(term_uid=perishability_term.uid, source=perishability_term_source)
                        )
                        aggregated_perishabilities[current_flow.uid] = perishability_xid

                    is_combined_product = (
                        current_product_is_combining_ingredients or one_of_subflows_is_combined_product
                    )
                    aggregated_combined_product_tags[current_flow.uid] = is_combined_product
                    combined_product_mono_product_term = (
                        self.gfm_factory.combined_product_term
                        if is_combined_product
                        else self.gfm_factory.mono_product_term
                    )
                    combined_product_mono_product_term_source = SourceEnum.eos_assumed

                    logger.debug(
                        "create PropListMutation to add combined product tag",
                        node=self.node,
                        combined_product_term=combined_product_mono_product_term,
                    )

                    glossary_tags_to_append.append(
                        GlossaryTermProp(
                            term_uid=combined_product_mono_product_term.uid,
                            source=combined_product_mono_product_term_source,
                        )
                    )

                    await calc_graph.apply_mutation(
                        PropListMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=current_flow.uid,
                            prop_name="glossary_tags",
                            props=glossary_tags_to_append,
                            append=True,
                        )
                    )

                stack.pop()

    def find_max_perishability(self, perishabilities: list[str]) -> str:
        """Find the highest perishability term."""
        if len(perishabilities) == 0:
            return self.gfm_factory.shelf_stable_term.xid
        else:
            return max(perishabilities, key=lambda x: self.gfm_factory.ordered_perishability_xids[x])

    def determine_perishability(self, node: FoodProductFlowNode) -> Optional[str]:
        """Determine perishability of a food product node."""
        tag_term_xids = node.tag_term_xids
        # retrieving the highest priority (first) perishability term
        for xid in tag_term_xids:
            if xid in self.gfm_factory.perishability_terms_dict:
                return xid
        return None


class PerishabilityGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)

        root_perishability_term = self.service_provider.glossary_service.root_subterms.get("EOS_Perishability")
        self.shelf_stable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_STABLE", root_perishability_term.access_group_uid)
        ]
        assert self.shelf_stable_term, "Could not find 'shelf-stable' perishability term."

        self.perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.perishable_term, "Could not find 'perishable' perishability term."

        self.high_perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_HIGH-PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.high_perishable_term, "Could not find 'highly perishable' perishability term."

        ordered_perishability_terms = [self.shelf_stable_term, self.perishable_term, self.high_perishable_term]
        self.ordered_perishability_xids = {term.xid: index for index, term in enumerate(ordered_perishability_terms)}
        self.perishability_terms_dict = {
            perishability_term.xid: perishability_term for perishability_term in ordered_perishability_terms
        }

        root_combined_product_mono_product_term = self.service_provider.glossary_service.root_subterms.get(
            "EOS_Combined_Product_Mono_Product"
        )
        self.combined_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_COMBINED_PRODUCT", root_combined_product_mono_product_term.access_group_uid)
        ]
        assert self.combined_product_term, "Could not find 'combined product' term."
        self.mono_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_MONO_PRODUCT", root_combined_product_mono_product_term.access_group_uid)
        ]
        assert self.mono_product_term, "Could not find 'mono product' term."

    def spawn_worker(self, node: Node) -> PerishabilityGapFillingWorker:
        """Spawn perishability gap filling worker."""
        return PerishabilityGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = PerishabilityGapFillingFactory
