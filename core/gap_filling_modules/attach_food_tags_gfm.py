"""Link food categories gap filling module."""
from typing import List

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util import find_access_group_uid_by_name
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop, transform_eurofir_to_qty_pkg
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.flow.food_product_flow import (
    ConservationOptionsEnum,
    LabelsOptionsEnum,
    ProcessingOptionsEnum,
    ProductionOptionsEnum,
    RawConservation,
    RawLabels,
    RawProcessing,
    RawProduction,
)
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp
from core.domain.props.glossary_term_prop import SourceEnum
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()

# used when retrieving from matching_service's cache
CHECK_PRODUCTION_GFM_NAME = "CheckProduction"
PROCESSING_GFM_NAME = "ProcessingGFM"
CHECK_LABELS_GFM_NAME = "CheckLabels"
CHECK_CONSERVATION_GFM_NAME = "CheckConservation"


def get_product_name_with_additional_processing_terms(
    node: FoodProductFlowNode, additional_processing_terms: list[Term]
) -> tuple[list[Term], list[Term]]:
    """Temporary fix to link to nutrient terms with additional processing terms included."""
    product_name_without_processing_terms = []
    product_name_dict = {}
    additional_processing_terms_uids = set(t.uid for t in additional_processing_terms)

    for t in node.product_name.terms:
        term = t.get_term()
        if term.uid not in additional_processing_terms_uids:
            product_name_without_processing_terms.append(term)
        product_name_dict[term.uid] = term

    if node.glossary_tags:
        for t in node.glossary_tags:
            term = t.get_term()
            if term.uid in additional_processing_terms_uids:
                product_name_dict[term.uid] = term

    return product_name_without_processing_terms, list(product_name_dict.values())


class AttachFoodTagsGapFillingWorker(AbstractGapFillingWorker):
    """Attach food tags to food products according to FoodEx2 terms."""

    def __init__(self, node: Node, gfm_factory: "AttachFoodTagsGapFillingFactory"):
        """Link the matched food Terms from the glossary, e.g., ("A1791", "J0116") to food categories."""
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Determine whether to schedule attach food tags gap filling worker.

        Similar to MatchProductNameGapFillingWorker and NutritionSubdivisionGapFillingWorker;
        do not schedule if name is missing as matching product name to terms is impossible.
        """
        if not isinstance(self.node, FoodProductFlowNode):
            logger.debug("[AttachFoodTags] node is not a food product --> not scheduled.")
            return False
        else:
            logger.debug("[AttachFoodTags] node is a food product --> scheduled.")
            return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Determine whether to run attach food tags gap filling worker."""
        if getattr(self.node, GFM_STATE_PROP_NAME) and (
            match_product_name_gfm_status := getattr(self.node, GFM_STATE_PROP_NAME).worker_states.get(
                MatchProductNameGapFillingWorker.__name__
            )
        ):
            if match_product_name_gfm_status == NodeGfmStateEnum.scheduled:
                logger.debug(
                    "[AttachFoodTags] wait for raw product_name to be mapped to glossary terms --> reschedule."
                )
                return GapFillingWorkerStatusEnum.reschedule

        logger.debug("[AttachFoodTags] No product name to be matched, ready.")
        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Run attach food tags gap filling worker."""
        logger.debug("start running gap-filling-module AttachFoodTags...")
        assert isinstance(self.node, FoodProductFlowNode)

        if self.node.raw_conservation:
            await self.process_raw_tags_to_match(calc_graph, self.node.raw_conservation, CHECK_CONSERVATION_GFM_NAME)

        if self.node.raw_production:
            await self.process_raw_tags_to_match(calc_graph, self.node.raw_production, CHECK_PRODUCTION_GFM_NAME)

        if self.node.raw_processing:
            await self.process_raw_tags_to_match(calc_graph, self.node.raw_processing, PROCESSING_GFM_NAME)

        if self.node.raw_labels:
            await self.process_raw_tags_to_match(calc_graph, self.node.raw_labels, CHECK_LABELS_GFM_NAME)

        nutrition_pkg = None
        if self.node.product_name and isinstance(self.node.product_name, NamesProp):
            await self.add_perishability_tags(calc_graph, self.node.product_name)

            (
                product_name_terms_without_processing_terms,
                product_name_terms,
            ) = get_product_name_with_additional_processing_terms(
                self.node, self.gfm_factory.additional_processing_terms
            )
            if not self.node.nutrient_values:
                # load the nutrient values from the glossary and add them to the product
                # TODO maybe in its own GFM?
                nutrition_term = await self.gfm_factory.glossary_link_service.load_term_nutrients(product_name_terms)
                if not nutrition_term and self.node.is_dried():
                    # Link nutrition term without dried since processing GFM and nutrition subdivision GFM
                    # can work together to create dried nutrient values and add drying process.
                    nutrition_term = await self.gfm_factory.glossary_link_service.load_term_nutrients(
                        product_name_terms_without_processing_terms
                    )

                if not nutrition_term:
                    logger.error(
                        "Could not load nutrition values for product",
                        prod_term=product_name_terms,
                        node_uid=self.node.uid,
                    )
                    calc_graph.set_data_errors_log_entry(
                        f"Could not load nutrition values for product:  {self.node} "
                        f"(product name terms: {product_name_terms})"
                    )
                else:
                    logger.debug(
                        "create PropMutation to add nutrient_values",
                        node=self.node,
                        nutrition_term=nutrition_term,
                    )

                    nutrition_pkg = transform_eurofir_to_qty_pkg(nutrition_term.data["nutr-vals"])
                    nutrition_pkg.prop_term_uid = nutrition_term.uid

        if self.node.nutrient_values and not isinstance(self.node.nutrient_values, QuantityPackageProp):
            nutrition_pkg = nutrients_dict_to_prop(self.node.nutrient_values)
            nutrition_pkg.source_data_raw = self.node.nutrient_values
            logger.debug(
                "create PropMutation to add nutrient_values",
                node=self.node,
            )

        if nutrition_pkg:
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="nutrient_values",
                    prop=nutrition_pkg,
                )
            )

    @staticmethod
    def get_language_for_existing_matchings(
        raw_name: dict | RawConservation | RawProduction | RawLabels | RawProcessing,
    ) -> str:
        if raw_name.get("value") in LabelsOptionsEnum.__members__.values():
            if raw_name.get("value") in (
                LabelsOptionsEnum.rain_forest_alliance_certified,
                LabelsOptionsEnum.wild_fish,
                LabelsOptionsEnum.organic,
                LabelsOptionsEnum.free_range,
                LabelsOptionsEnum.grazing,
                LabelsOptionsEnum.suckler_cow,
                LabelsOptionsEnum.usda_organic,
            ):
                lang = "en"
            elif raw_name.get("value") in (LabelsOptionsEnum.bio_suisse, LabelsOptionsEnum.ab_agriculture_biologique):
                lang = "fr"
            else:
                lang = "de"
        elif (
            raw_name.get("value") in ConservationOptionsEnum.__members__.values()
            or raw_name.get("value") in ProductionOptionsEnum.__members__.values()
            or raw_name.get("value") in ProcessingOptionsEnum.__members__.values()
        ):
            lang = "en"
        else:
            lang = raw_name.get("language")
        return lang

    async def process_raw_tags_to_match(
        self,
        calc_graph: CalcGraph,
        raw_name: RawConservation | RawProduction | RawLabels | RawProcessing,
        matching_gfm_name: str,
    ) -> None:
        """Process strings to match for food glossary tags input."""
        lang = self.get_language_for_existing_matchings(raw_name)
        raw_name_to_process = {"value": raw_name.get("value"), "language": lang}

        extracted_tag_term: list = await self.gfm_factory.matching_service.get_terms_by_matching_string(
            matching_string=raw_name_to_process.get("value"),
            gap_filling_module_name=matching_gfm_name,
            filter_lang=raw_name_to_process.get("language"),
        )
        source_data_raw_list = [raw_name_to_process.get("value") for _ in extracted_tag_term]
        if not extracted_tag_term:
            # See if comma separated strings can be separately matched.
            client_raw_tags = [raw_tag.strip() for raw_tag in raw_name_to_process.get("value").split(",")]
            if client_raw_tags and len(client_raw_tags) >= 2:
                for client_raw_tag in client_raw_tags:
                    lang = self.get_language_for_existing_matchings(
                        {"value": client_raw_tag, "language": raw_name_to_process.get("language")}
                    )

                    extracted_tag_term.extend(
                        await self.gfm_factory.matching_service.get_terms_by_matching_string(
                            matching_string=client_raw_tag,
                            gap_filling_module_name=matching_gfm_name,
                            filter_lang=lang,
                        )
                    )
                    source_data_raw_list.append(client_raw_tag)

        extracted_term_props: List[GlossaryTermProp] = [
            GlossaryTermProp(term_uid=term.uid, source_data_raw=source_data_raw, source=SourceEnum.api)
            for terms_in_lang, source_data_raw in zip(extracted_tag_term, source_data_raw_list)
            for term in terms_in_lang
        ]
        logger.debug(
            "create PropMutation to add glossary_term_prop",
            node=self.node,
            glossary_term_prop=extracted_term_props,
        )
        if extracted_term_props:
            prop_mutation = PropListMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="glossary_tags",
                props=extracted_term_props,
                append=True,
            )
            await calc_graph.apply_mutation(prop_mutation)

    async def add_perishability_tags(self, calc_graph: CalcGraph, product_name: NamesProp) -> None:
        """Add perishability tags to food products."""
        product_name_terms = [prop.get_term() for prop in product_name.terms]
        if any(not term for term in product_name_terms):
            logger.warn(
                "Not all FoodEx2 Terms are well-defined for this node.",
                node_uid=self.node.uid,
            )
            calc_graph.set_data_errors_log_entry(
                f"Could not match product with terms {product_name_terms} "
                f"in node {self.node} to food tags (categories, perishability, etc.)"
            )
        else:
            # Look for perishability tag in the EDB database (EDB ID link).
            perishability_term = await self.gfm_factory.glossary_link_service.load_term_perishability(
                product_name_terms
            )
            if perishability_term:
                extracted_perishability_term_prop: List[GlossaryTermProp] = [
                    GlossaryTermProp(term_uid=perishability_term.uid, source=SourceEnum.edb_database)
                ]
                logger.debug(
                    "create PropListMutation to add perishability tag",
                    node=self.node,
                    perishability_term=extracted_perishability_term_prop,
                )

                if perishability_term:
                    prop_mutation = PropListMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=self.node.uid,
                        prop_name="glossary_tags",
                        props=extracted_perishability_term_prop,
                        append=True,
                    )
                    await calc_graph.apply_mutation(prop_mutation)


class AttachFoodTagsGapFillingFactory(AbstractGapFillingFactory):
    """Attach food tags gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.matching_service = service_provider.matching_service
        self.additional_processing_terms: list[Term] = []

    async def init_cache(self) -> None:
        foodex2_access_group_uid = await find_access_group_uid_by_name(
            self.postgres_db, "FoodEx2 Glossary Terms namespace"
        )
        # Add the following terms for matching nutrients: Dried, Food not known, Bread, Juice, Yoghurt, Jam
        for xid in ("J0116", "Z0001", "A004V", "C0300", "A03TV", "A02NH", "A01MM"):
            self.additional_processing_terms.append(
                self.service_provider.glossary_service.terms_by_xid_ag_uid[(xid, foodex2_access_group_uid)]
            )

    def spawn_worker(self, node: Node) -> AttachFoodTagsGapFillingWorker:
        """Spawn add food glossary tags gap filling worker."""
        return AttachFoodTagsGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AttachFoodTagsGapFillingFactory
