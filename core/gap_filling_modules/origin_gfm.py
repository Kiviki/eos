"""Origin gap filling module."""

import copy
import os
import uuid
from io import FileIO
from typing import Tuple
from zipfile import ZipFile

import aiofiles
import google
import httpx
import pandas as pd
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util import find_access_group_uid_by_name
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.ingredient_amount_estimator_gfm import IngredientAmountEstimatorGapFillingWorker
from gap_filling_modules.inventory_connector_gfm import InventoryConnectorGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.location_gfm import LocationGapFillingWorker
from gap_filling_modules.location_util.countries import (
    get_inherited_country_code,
    location_to_regional_term_xid_map,
    regional_term_xid_to_region_gadm_codes_map,
)
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from googleapiclient.discovery import Resource, build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import (
    AnimalProductsProp,
    GfmStateProp,
    GlossaryTermProp,
    NamesProp,
    OriginDataProp,
    QuantityPackageProp,
    QuantityProp,
)
from core.domain.props.glossary_term_prop import SourceEnum
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum, LocationSourceEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class OriginGfmSettings(EnvProps):
    TEMP_DIR: str = "./temp_data"
    DOMESTIC_FILE_NAME: str = "domestic_df.hdf5"
    IMPORT_EXPORT_FILE_NAME: str = "import_export_df.hdf5"
    ISO_M49_CODE_MAPPING_FILE_NAME: str = "iso_to_m49_mapping.hdf5"
    ITEMCODES_FILE_NAME: str = "Trade_DetailedTradeMatrix_E_ItemCodes.csv"

    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    GDRIVE_FAO_FOLDER_ID: str = "1ynyd6dMKAS-EO81lKn03up2s6PHWarsn"

    # SEEM TO BE OUTDATED.
    # FAO_IMPORT_EXPORT_ZIP_PATH: str = (
    #    "https://fenix.fao.org/faostat/static/bulkdownload/zip_files/" "Trade_DetailedTradeMatrix_E_All_Data.zip"
    # )
    # FAO_DOMESTIC_ZIP_PATH: str = (
    #    "https://fenix.fao.org/faostat/static/bulkdownload/zip_files/" "Production_Crops_Livestock_E_All_Data.zip"
    # )

    FAO_IMPORT_EXPORT_ZIP_PATH: str = (
        "https://fenixservices.fao.org/faostat/static/bulkdownloads/" "Trade_DetailedTradeMatrix_E_All_Data.zip"
    )
    FAO_DOMESTIC_ZIP_PATH: str = (
        "https://fenixservices.fao.org/faostat/static/bulkdownloads/" "Production_Crops_Livestock_E_All_Data.zip"
    )

    # SEEM TO BE OUTDATED.
    # GITHUB_MAPPING_CSV_PATH: str = (
    #    "https://gist.github.com/tadast/8827699/raw/"
    #    "f5cac3d42d16b78348610fc4ec301e9234f82821/countries_codes_and_coordinates.csv"
    # )
    GITHUB_MAPPING_CSV_PATH: str = (
        "https://gist.githubusercontent.com/metal3d/5b925077e66194551df949de64e910f6/raw/"
        "c5f20a037409d96958553e2eb6b8251265c6fd63/country-coord.csv"
    )
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class OriginGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if isinstance(self.node, ActivityNode):
            return False

        if isinstance(self.node, FoodProductFlowNode) and self.node.is_subdivision:
            logger.debug(f"{self.node} is a subdivision --> can't run Origin GFM.")
            return False

        if isinstance(self.node, FoodProductFlowNode):
            return True

        return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(LocationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for LocationGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that is_combining_ingredients check can be properly performed.
        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for AddClientNodeGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that all inventory are properly linked before origin splitting.
        if global_gfm_state.get(InventoryConnectorGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for InventoryConnectorGapFillingFactory to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that product_name is already matched.
        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for MatchProductNameGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(LinkTermToActivityNodeGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for LinkTermToActivityNodeGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if (
            global_gfm_state.get(
                IngredientAmountEstimatorGapFillingWorker.__name__,
                0,
            )
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Origin] wait for IngredientAmountEstimatorGapFillingWorker to finish --> not can_run_now.")

            return GapFillingWorkerStatusEnum.reschedule

        if isinstance(self.node, FlowNode) and self.node.get_sub_nodes():
            for sub_activity in self.node.get_sub_nodes():
                if not isinstance(sub_activity.production_amount, QuantityProp):
                    logger.debug("[Origin] waiting for sub activity production amount --> not can_run_now.")
                    return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        if isinstance(self.node, FoodProductFlowNode) and (
            self.node.is_combining_ingredients() or self.link_to_combined_product()
        ):
            await self.combined_product_origin_processing(calc_graph)
        elif isinstance(self.node, FoodProductFlowNode):
            await self.monoproduct_origin_processing(calc_graph)
        else:
            logger.debug(
                f"{self.node} does not fit Origin GFM requirements -- is neither a monoproduct nor a combined product."
            )

    def link_to_combined_product(self) -> bool:
        if self.node.link_to_sub_node is None:
            return False

        if self.node.get_sub_nodes() and self.node.get_sub_nodes()[0].get_sub_nodes():
            grandchild_node = self.node.get_sub_nodes()[0].get_sub_nodes()[0]
            if self.node.product_name and grandchild_node.product_name == self.node.product_name:
                # Identical node, the activity in between is a dummy activity.
                # the origin GFM will already run for a monoproduct origin.
                # i.e., this node is simply linking to a single ingredient.
                return False

        return True

    async def monoproduct_origin_processing(self, calc_graph: "CalcGraph") -> None:
        # TODO: move this part to `should_be_scheduled()`?

        origins_list = await self.parse_current_origins()

        if len(origins_list) == 1:
            logger.debug(f"{self.node} already has an origin specified.")
            return

        country_total_percentage_share = {}
        fao_code_term = None
        is_origin_from_fao: bool = False

        kitchen_country_code = get_inherited_country_code(self.node)
        kitchen_country_m49_code = self.gfm_factory.iso_to_m49_mapping.get(kitchen_country_code)

        if len(origins_list) == 0:
            if self.node.get_parent_nodes() and self.node.get_parent_nodes()[0].get_parent_nodes():
                grandparent_node = self.node.get_parent_nodes()[0].get_parent_nodes()[0]
                if grandparent_node.link_to_sub_node and grandparent_node.product_name == self.node.product_name:
                    # Identical node, the activity in between is a dummy activity. Do not run origin GFM since
                    # the origin GFM will already run on the parent.
                    return

            if not kitchen_country_m49_code:
                await self.set_unknown_origin(calc_graph)

                return

            # calculating origin mix depending on the kitchen

            # current foodex2 terms -> glossary link -> fao code -> data from the table
            if (
                hasattr(self.node, "product_name")
                and self.node.product_name
                and isinstance(self.node.product_name, NamesProp)
            ):
                foodex2_terms = [term.get_term() for term in self.node.product_name.terms]
            else:
                logger.warning(
                    f"No FoodEx2 term present in one of 'product_name' props of {self.node}! Setting unknown origin..."
                )
                await self.set_unknown_origin(calc_graph)

                calc_graph.set_data_errors_log_entry(
                    f"No FoodEx2 term present in one of 'product_name' props of {self.node}! Setting unknown origin..."
                )

                return

            fao_code_term = await self.gfm_factory.glossary_link_service.load_fao_code_term(foodex2_terms)

            if not fao_code_term:
                logger.warning(
                    f"No FAO code term present for FoodEx2 terms {foodex2_terms}! Using averaged import statistics..."
                )
                fao_code_term = self.gfm_factory.default_production_fao_code_term

            # handling special FAO codes for local/unknown products
            if fao_code_term.xid in (
                self.gfm_factory.local_production_fao_code_term.xid,
                self.gfm_factory.unknown_fish_production_fao_code_term.xid,
                self.gfm_factory.failed_production_fao_code_term.xid,
            ):
                logger.warning(
                    f"FAO code {fao_code_term.xid} has no trade data by default. Setting unknown origin...",
                    terms=foodex2_terms,
                    ingredient=str(self.node),
                )
                await self.set_unknown_origin(calc_graph, fao_code_term)

                calc_graph.set_data_errors_log_entry(
                    f"FAO code {fao_code_term.xid} has no trade data by default. Setting unknown origin... "
                    f"(node: {self.node}) (terms: {foodex2_terms})"
                )

                return

            fao_code = int(fao_code_term.xid)

            stats_data_year_column = "Y2021"

            domestic_production_row = self.gfm_factory.domestic_df[
                (self.gfm_factory.domestic_df["Area Code (M49)"] == kitchen_country_m49_code)
                & (self.gfm_factory.domestic_df["Item Code"] == fao_code)
            ]

            domestic_production_value = domestic_production_row[stats_data_year_column]

            # if there is no domestic production entry at all, we substitute it with zero,
            # which pretty much always leads to 'unknown origin' assumption...
            if not len(domestic_production_value):
                domestic_production_value = 0
            else:
                # if there is no domestic production entry for 2021, we try to use the 2020 one
                if pd.isnull(domestic_production_value.values[0]):
                    stats_data_year_column = "Y2020"
                    domestic_production_value = domestic_production_row[stats_data_year_column]

                domestic_production_value = domestic_production_value.values[0]

            # initializing this cache explicitly here so that
            # it won't be initiated during every unit test run,
            # otherwise setting up each test takes up a lot of time
            await self.gfm_factory.init_import_export_cache()

            # filtering this country's filtered import & export data
            try:
                filtered_df = self.gfm_factory.import_export_cache[
                    (
                        kitchen_country_m49_code,
                        fao_code,
                    )
                ]

                # using a deepcopy here in order to skip
                # "A value is trying to be set on a copy of a slice from a DataFrame" warning
                filtered_import_df = copy.deepcopy(filtered_df[(filtered_df["Element"] == "Import Quantity")])

                import_value = filtered_import_df[stats_data_year_column].sum()
                export_value = filtered_df[(filtered_df["Element"] == "Export Quantity")][stats_data_year_column].sum()
            except KeyError:
                filtered_import_df = pd.DataFrame()
                import_value = 0
                export_value = 0

                # handling the case when all 3 values equal zero
                if domestic_production_value == 0:
                    logger.warning(
                        f"FAO code {fao_code} has neither trade data nor domestic production data for "
                        f"country {kitchen_country_code}. Setting unknown origin...",
                        foodex2_terms=foodex2_terms,
                        ingredient=str(self.node),
                    )
                    await self.set_unknown_origin(calc_graph)

                    calc_graph.set_data_errors_log_entry(
                        f"FAO code {fao_code} has neither trade data nor domestic production data for "
                        f"country {kitchen_country_code}. Setting unknown origin... "
                        f"(node: {self.node}) (terms: {foodex2_terms})"
                    )

                    return

            domestic_import_export_total = domestic_production_value + import_value - export_value

            # TODO: implement points 3/4 (more advanced ones) in inconsistencies description
            # TODO: keep '=' sign here?
            if export_value >= domestic_production_value + import_value:
                logger.warning(
                    f"FAO code {fao_code} export value is bigger than or equal to the sum of domestic "
                    f"and import values of country {kitchen_country_code}! Setting unknown origin...",
                    foodex2_terms=foodex2_terms,
                    ingredient=str(self.node),
                )
                await self.set_unknown_origin(calc_graph)

                calc_graph.set_data_errors_log_entry(
                    f"FAO code {fao_code} export value is bigger than or equal to the sum of domestic "
                    f"and import values of country {kitchen_country_code}! Setting unknown origin... "
                    f"(node: {self.node}) (terms: {foodex2_terms})"
                )

                return

            domestic_origin_share = (domestic_production_value - export_value) / domestic_import_export_total

            if domestic_origin_share > 0:
                foreign_origin_share = 1 - domestic_origin_share
            else:
                foreign_origin_share = 1

            if not filtered_import_df.empty and import_value:
                # preparing import df for further operations
                filtered_import_df.sort_values(stats_data_year_column, ascending=False, inplace=True)

                # creating cumulative sum of previous rows
                # so that we stop when we reach 90% of import
                filtered_import_df["Cumulative sum"] = (
                    filtered_import_df[stats_data_year_column]
                    .rolling(
                        min_periods=1,
                        window=len(filtered_import_df[stats_data_year_column]),
                    )
                    .sum()
                )

                assert round(filtered_import_df.iloc[-1]["Cumulative sum"], 5) == round(import_value, 5)

                rows_greater_than_90_pct = (
                    len(filtered_import_df[filtered_import_df["Cumulative sum"] < import_value * 0.9]) + 1
                )  # adding extra row here to make sure that we definitely cover 90% of total import value

                if fao_code_term.xid == self.gfm_factory.default_production_fao_code_term.xid:
                    # only take the top 10 countries to avoid too many origins in this case
                    len_before = len(filtered_import_df)
                    old_import_sum = filtered_import_df[stats_data_year_column].sum()
                    filtered_import_df = filtered_import_df.iloc[:10]
                    len_after = len(filtered_import_df)
                    new_import_sum = filtered_import_df[stats_data_year_column].sum()
                    if len_after < len_before:
                        logger.warning(
                            f"To avoid too many origins for FAO code {fao_code_term.xid}, only the top 10 countries "
                            f"are taken into account for origin split. They make up for "
                            f"{round(100 * new_import_sum / old_import_sum, 2)}% of the total import value.",
                            ingredient=str(self.node),
                        )

                filtered_import_df = filtered_import_df.iloc[:rows_greater_than_90_pct]
                filtered_import_value = filtered_import_df[stats_data_year_column].sum()

                country_share = {
                    self.gfm_factory.m49_to_iso_mapping[country]: value / filtered_import_value
                    for country, value in zip(
                        filtered_import_df["Partner Country Code (M49)"], filtered_import_df[stats_data_year_column]
                    )
                }

                for country, value in country_share.items():
                    country_total_percentage_share[country] = foreign_origin_share * value

            if domestic_origin_share > 0:
                country_total_percentage_share[kitchen_country_code] = domestic_origin_share
            is_origin_from_fao = True

        else:
            stats_data_year_column = None

            for country in origins_list:
                country_total_percentage_share[country.country_code] = 1 / len(origins_list)

        # now that we know shares of each country, we can create mutations
        if not self.node.get_sub_nodes():
            logger.warning(f"No sub activity node of {self.node} available. Skipping origin splitting.")
            return

        # 1) delete an edge between recipe_ingredient1 and food_product
        old_food_product_node = self.node.get_sub_nodes()[0]

        remove_edge_mutation = RemoveEdgeMutation(
            created_by_module=self.__class__.__name__,
            from_node_uid=self.node.uid,
            to_node_uid=old_food_product_node.uid,
        )
        await calc_graph.apply_mutation(remove_edge_mutation)

        # 2) make new mutations

        activity_node_uid = uuid.uuid4()
        activity_node = FoodProcessingActivityNode.model_construct(
            uid=activity_node_uid,
            access_group_uid=old_food_product_node.access_group_uid,
            access_group_xid=old_food_product_node.access_group_xid,
            gfm_state=GfmStateProp.unvalidated_construct(
                worker_states={
                    # No more origin split necessary:
                    "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                    # Ingredient amount estimation should be already finished:
                    "IngredientAmountEstimatorGapFillingWorker": NodeGfmStateEnum.canceled,
                }
            ),
            production_amount=old_food_product_node.production_amount.duplicate()
            if old_food_product_node.production_amount
            else None,
        )

        await calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                new_node=activity_node,
                parent_node_uid=self.node.uid,
                copy=False,
            )
        )

        # create origin_splits

        for index, (
            country_iso_code,
            percentage,
        ) in enumerate(country_total_percentage_share.items()):
            origin_split_flow_node_uid = uuid.uuid4()

            await calc_graph.apply_mutation(
                DuplicateNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node_uid=origin_split_flow_node_uid,
                    source_node_uid=self.node.uid,
                    parent_node_uid=activity_node_uid,
                    gfms_to_not_schedule=("OriginGapFillingWorker",),
                    duplicate_child_nodes=False,
                )
            )
            origin_split_flow_node = calc_graph.get_node_by_uid(origin_split_flow_node_uid)

            # we need to overwrite the "amount"
            # such that it corresponds to a production_amount of the parent.
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="amount",
                    prop=QuantityProp(
                        value=percentage * activity_node.production_amount.value,
                        unit_term_uid=activity_node.production_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                )
            )

            # We still want to run conservation GFM on origin split nodes.
            old_gfm_state = dict(origin_split_flow_node.gfm_state.worker_states).copy()
            if old_gfm_state.get("ConservationGapFillingWorker"):
                if old_gfm_state["ConservationGapFillingWorker"] == NodeGfmStateEnum.finished:
                    old_gfm_state["ConservationGapFillingWorker"] = NodeGfmStateEnum.scheduled
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="gfm_state",
                    prop=GfmStateProp.unvalidated_construct(worker_states=old_gfm_state),
                )
            )

            # Erase glossary tags if source is eos_assumed
            if origin_split_flow_node.glossary_tags:
                new_glossary_tags = []
                for tag in origin_split_flow_node.glossary_tags:
                    if tag.source != SourceEnum.eos_assumed:
                        new_glossary_tags.append(tag.duplicate())

                await calc_graph.apply_mutation(
                    PropListMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=origin_split_flow_node.uid,
                        prop_name="glossary_tags",
                        props=new_glossary_tags,
                        append=False,
                    )
                )

            # we need to overwrite the "food_categories" and "animal products"
            # to avoid aggregating the food categories from each origin.
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="food_categories",
                    prop=QuantityPackageProp(quantities={}, for_reference=ReferenceAmountEnum.amount_for_100g),
                )
            )

            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="animal_products",
                    prop=AnimalProductsProp(quantities={}, for_reference=ReferenceAmountEnum.amount_for_100g),
                )
            )

            gadm_term = await self.gfm_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid=self.gfm_factory.iso_to_gadm_mapping.get(country_iso_code),
                access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
            )

            origin_location = LocationProp(
                address=gadm_term.name,
                latitude=gadm_term.data.get("centroid_lat"),
                longitude=gadm_term.data.get("centroid_lon"),
                country_code=country_iso_code,
                term_uid=gadm_term.uid,
                location_qualifier=LocationQualifierEnum.known,
                source=LocationSourceEnum.fao_stat if is_origin_from_fao else LocationSourceEnum.gadm,
            )

            await self.set_origin_prop(
                origin_split_flow_node,
                origin_location,
                calc_graph,
                fao_code_term,
                stats_data_year_column,
            )

            # 3) add a new edge between origin_split1 and old food_product

            if index == len(country_total_percentage_share.items()) - 1:
                add_edge_mutation = AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=origin_split_flow_node.uid,
                    to_node_uid=old_food_product_node.uid,
                )
                await calc_graph.apply_mutation(add_edge_mutation)

                food_product_node = old_food_product_node
            else:
                new_food_product_node_uid = uuid.uuid4()

                await calc_graph.apply_mutation(
                    DuplicateNodeMutation(
                        created_by_module=self.__class__.__name__,
                        new_node_uid=new_food_product_node_uid,
                        source_node_uid=old_food_product_node.uid,
                        parent_node_uid=origin_split_flow_node.uid,
                        duplicate_child_nodes=True,
                        gfms_to_not_schedule=("OriginGapFillingWorker",),
                    )
                )
                food_product_node = calc_graph.get_node_by_uid(new_food_product_node_uid)

            # making sure that this parent product node also has origin specified
            # fao_code_term and stats_data_year_column are not added to the sub-activity nodes as these properties
            # only belong to the flow nodes (the information is already available from the parent flow node).
            await self.set_origin_prop(
                food_product_node,
                origin_location,
                calc_graph,
            )

        # Need to clear self.node.location property, because it was now already split, so that the transport GFM will
        # run from the actual origin to the destination given by the parent of this node.
        prop_mutation = PropListMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="flow_location",
            props=[],
            append=False,
        )
        await calc_graph.apply_mutation(prop_mutation)

    # TODO: if we don't have FAO code specified in a monoproduct,
    #  then it'd be better to get it from parent "layers"
    # TODO: ideally "monoproducts" and "combined products" shouldn't depend on node type
    #  --> instead, we can define them like "monoproducts are nodes that have only brightway process below" etc.
    # TODO: destination, ??

    async def combined_product_origin_processing(self, calc_graph: "CalcGraph") -> None:
        origins_list = await self.parse_current_origins()

        if len(origins_list) == 1:
            logger.debug(f"{self.node} already has an origin specified.")

            return

        if len(origins_list) == 0:
            logger.debug(f"{self.node} already has no origin specified.")

            return

        else:
            node_to_process = self.node.get_sub_nodes()[0]

            # 1) save all subnodes
            current_node_subs = list(node_to_process.get_sub_nodes())

            production_amount_value: float = node_to_process.production_amount.value
            production_amount_unit: Term = node_to_process.production_amount.get_unit_term()

            # 2) destroy all edges from the ActivityNode (node_to_process) to its sub-FlowNodes:
            for sub_node in current_node_subs:
                remove_edge_mutation = RemoveEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=node_to_process.uid,
                    to_node_uid=sub_node.uid,
                )
                await calc_graph.apply_mutation(remove_edge_mutation)

            country_percentage_share = 1 / len(origins_list)

            # 3) create splits
            for index, country in enumerate(origins_list):
                # 4a) create flow
                flow_between_recipes_node = FoodProductFlowNode(uid=uuid.uuid4())
                add_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node=flow_between_recipes_node,
                    parent_node_uid=node_to_process.uid,
                    copy=True,
                )
                await calc_graph.apply_mutation(add_node_mutation)

                # make sure that the parent node is combined using flow nodes with corresponding country shares:
                await calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=flow_between_recipes_node.uid,
                        prop_name="amount",
                        prop=QuantityProp(
                            value=country_percentage_share * production_amount_value,
                            unit_term_uid=production_amount_unit.uid,
                            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                        ),
                    )
                )

                # 4b) create activity
                origin_split_recipe_node_uid = uuid.uuid4()

                await calc_graph.apply_mutation(
                    DuplicateNodeMutation(
                        created_by_module=self.__class__.__name__,
                        new_node_uid=origin_split_recipe_node_uid,
                        source_node_uid=node_to_process.uid,
                        parent_node_uid=flow_between_recipes_node.uid,
                        gfms_to_not_schedule=("OriginGapFillingWorker",),
                        duplicate_child_nodes=False,
                    )
                )
                origin_split_recipe_node = calc_graph.get_node_by_uid(origin_split_recipe_node_uid)

                if country.country_code in self.gfm_factory.iso_to_gadm_mapping:
                    gadm_term = (
                        await self.gfm_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                            term_xid=self.gfm_factory.iso_to_gadm_mapping.get(country.country_code),
                            access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
                        )
                    )
                    country.term_uid = gadm_term.uid

                await self.set_origin_prop(origin_split_recipe_node, country, calc_graph)
                await self.set_origin_prop(flow_between_recipes_node, country, calc_graph)

                for sub_node in current_node_subs:
                    # 5a) in the very end, link old ones to new split
                    if index == len(origins_list) - 1:
                        add_edge_mutation = AddEdgeMutation(
                            created_by_module=self.__class__.__name__,
                            from_node_uid=origin_split_recipe_node_uid,
                            to_node_uid=sub_node.uid,
                        )
                        await calc_graph.apply_mutation(add_edge_mutation)
                    # 5b) duplicate old ones for new splits
                    else:
                        new_sub_node_uid = uuid.uuid4()

                        await calc_graph.apply_mutation(
                            DuplicateNodeMutation(
                                created_by_module=self.__class__.__name__,
                                new_node_uid=new_sub_node_uid,
                                source_node_uid=sub_node.uid,
                                parent_node_uid=origin_split_recipe_node.uid,
                                duplicate_child_nodes=True,
                            )
                        )

            # 6) Remove the location list from the current node, so that it won't be used by transport (which needs to
            # operate only on the newly created sub-nodes per origin):
            for node in (self.node, node_to_process):
                if isinstance(node, ActivityNode):
                    prop_name = "activity_location"
                else:
                    prop_name = "flow_location"

                await calc_graph.apply_mutation(
                    PropListMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=node.uid,
                        prop_name=prop_name,
                        props=[],
                        append=False,
                    )
                )

    async def parse_current_origins(self) -> list[LocationProp]:
        location_props = self.node.flow_location

        if location_props:
            locations = []

            for location_prop in location_props:
                location_qualifier = location_prop.location_qualifier

                if location_term := location_prop.get_term():
                    term_xid = location_term.xid
                else:
                    term_xid = ""

                # handle regional terms
                if term_xid in location_to_regional_term_xid_map.values():
                    # blacklisting already listed countries
                    already_listed_regions = [l_prop.get_term().xid for l_prop in location_props]

                    for region in regional_term_xid_to_region_gadm_codes_map.get(term_xid, []):
                        if region not in already_listed_regions:
                            country_code = self.gfm_factory.gadm_to_iso_mapping.get(region.split(".")[0])
                            glossary_service = self.gfm_factory.service_provider.glossary_service
                            gadm_term = await glossary_service.get_term_by_xid_and_access_group_uid(
                                term_xid=region,
                                access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
                            )
                            locations.append(
                                LocationProp(
                                    address=gadm_term.name,
                                    latitude=gadm_term.data.get("centroid_lat"),
                                    longitude=gadm_term.data.get("centroid_lon"),
                                    country_code=country_code,
                                    term_uid=gadm_term.uid,
                                    location_qualifier=location_qualifier,
                                    source=LocationSourceEnum.gadm,
                                )
                            )

                    continue

                locations.append(
                    LocationProp(
                        address=location_prop.address,
                        latitude=location_prop.latitude,
                        longitude=location_prop.longitude,
                        country_code=location_prop.country_code,
                        term_uid=location_prop.term_uid,
                        location_qualifier=location_qualifier,
                        source=LocationSourceEnum.google,
                    )
                )

            return locations
        else:
            return []

    async def set_unknown_origin(self, calc_graph: CalcGraph, fao_code_term: Term = None) -> None:
        unknown_location = LocationProp(
            address="",
            latitude=0.0,
            longitude=0.0,
            country_code=None,
            term_uid=None,
            location_qualifier=LocationQualifierEnum.unknown,
            source=LocationSourceEnum.unknown,
        )

        await self.set_origin_prop(
            self.node,
            unknown_location,
            calc_graph,
            fao_code_term=self.gfm_factory.failed_production_fao_code_term if fao_code_term is None else fao_code_term,
        )

    async def set_origin_prop(
        self,
        node: Node,
        location: LocationProp,
        calc_graph: "CalcGraph",
        fao_code_term: Term = None,
        stats_data_year_column: str = None,
    ) -> None:
        if isinstance(node, ActivityNode):
            prop_name = "activity_location"
        else:
            prop_name = "flow_location"

        prop_mutation = PropListMutation(
            created_by_module=self.__class__.__name__,
            node_uid=node.uid,
            prop_name=prop_name,
            props=[location.duplicate()],
            append=False,
        )

        await calc_graph.apply_mutation(prop_mutation)

        # using a separate property for storing FAO term
        origin_data_dict = {}

        if fao_code_term:
            origin_data_dict["fao_code"] = GlossaryTermProp.unvalidated_construct(term_uid=fao_code_term.uid)

        if stats_data_year_column:
            origin_data_dict["stats_data_year_column"] = stats_data_year_column[1:]

        if origin_data_dict:
            # Seems not to be tested.
            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=node.uid,
                prop_name="origin_data",
                prop=OriginDataProp.unvalidated_construct(**origin_data_dict),
            )

            await calc_graph.apply_mutation(prop_mutation)


class OriginGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.domestic_df: pd.DataFrame = None  # noqa
        self.import_export_cache: dict[tuple, pd.DataFrame] = None  # noqa
        self.import_export_file_path: str = None  # noqa
        self.iso_to_m49_mapping: dict[str, int] = None  # noqa
        self.m49_to_iso_mapping: dict[int, str] = None  # noqa
        self.gadm_to_iso_mapping: dict[str, str] = None  # noqa
        self.iso_to_gadm_mapping: dict[str, str] = None  # noqa

        self.unknown_region_term: Term = None  # noqa
        self.root_gadm_term = self.service_provider.glossary_service.root_subterms.get("Root_GADM")
        assert self.root_gadm_term, "No root GADM term found."

        self.local_production_fao_code_term: Term = None  # noqa
        self.default_production_fao_code_term: Term = None  # noqa
        self.unknown_fish_production_fao_code_term: Term = None  # noqa
        self.failed_production_fao_code_term: Term = None  # noqa
        self.root_fao_term = self.service_provider.glossary_service.root_subterms.get("Root_FAO")
        assert self.root_fao_term, "No root FAO term found."

        self.origin_gfm_settings = OriginGfmSettings()
        self.gfm_dir = os.path.join(self.origin_gfm_settings.TEMP_DIR, "origin_gfm")
        os.makedirs(self.gfm_dir, exist_ok=True)
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", self.service_provider.glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

    async def init_cache(self) -> None:
        foodex2_access_group_uid = await find_access_group_uid_by_name(
            postgres_db=self.postgres_db, namespace_name="FoodEx2 Glossary Terms namespace"
        )

        self.unknown_region_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("R0001", foodex2_access_group_uid)
        ]
        assert self.unknown_region_term, "No 'unknown region' term found."

        # loading custom FAO codes
        self.local_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="100000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.local_production_fao_code_term, "No 'local production' term found."

        self.default_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="200000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.default_production_fao_code_term, "No 'default production' term found."

        self.unknown_fish_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="300000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.unknown_fish_production_fao_code_term, "No 'unknown fish production' term found."

        self.failed_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="400000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.failed_production_fao_code_term, "No 'failed production' term found."

        self.import_export_file_path = os.path.join(self.gfm_dir, self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME)
        domestic_file_path = os.path.join(self.gfm_dir, self.origin_gfm_settings.DOMESTIC_FILE_NAME)
        code_mapping_file_path = os.path.join(self.gfm_dir, self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME)

        await self.make_sure_hdf_files_exist(domestic_file_path, code_mapping_file_path)

        # loading the lightest 2 files from local storage
        domestic_df = pd.read_hdf(domestic_file_path)
        aggregated_for_groups = ["Area Code (M49)"]
        self.domestic_df = self.add_aggregated_columns(domestic_df, aggregated_for_groups)

        code_mapping_data: pd.DataFrame = pd.read_hdf(code_mapping_file_path)  # noqa
        self.iso_to_m49_mapping = dict(zip(code_mapping_data["Alpha-2 code"], code_mapping_data["Numeric code"]))
        self.m49_to_iso_mapping = dict(zip(code_mapping_data["Numeric code"], code_mapping_data["Alpha-2 code"]))
        # TODO: get this from new spreadsheet
        self.iso_to_gadm_mapping = dict(zip(code_mapping_data["Alpha-2 code"], code_mapping_data["Alpha-3 code"]))
        self.gadm_to_iso_mapping = dict(zip(code_mapping_data["Alpha-3 code"], code_mapping_data["Alpha-2 code"]))

        await self.check_fao_codes_terms_presence()

    def add_aggregated_columns(self, input_df: pd.DataFrame, aggregated_for_groups: list[str]) -> pd.DataFrame:
        """Aggregate over all products and add the aggregated columns to the DataFrame."""
        # Group by the relevant country codes.
        grouped = input_df.groupby(aggregated_for_groups)

        # Apply custom aggregation
        agg_rules = {col: "first" for col in input_df.columns}
        agg_rules["Y2020"] = "sum"
        agg_rules["Y2021"] = "sum"
        aggregated_df = grouped.agg(agg_rules)

        # Set 'Item Code' and 'Item' for the summed entries
        aggregated_df["Item Code"] = int(self.default_production_fao_code_term.xid)
        aggregated_df["Item"] = "default_fao_statistics"

        # Concatenate the totals DataFrame with the original DataFrame.
        output_df = pd.concat([input_df, aggregated_df], ignore_index=True)

        return output_df

    async def init_import_export_cache(self) -> None:
        if not self.import_export_cache:
            import_export_df: pd.DataFrame = pd.read_hdf(self.import_export_file_path)  # noqa

            # TODO: Do we want to drop all rows that don't have tonnes as unit?
            import_export_df.drop(import_export_df[(import_export_df["Unit"] != "tonnes")].index, inplace=True)

            aggregate_for_groups = ["Reporter Country Code (M49)", "Partner Country Code (M49)"]
            enhanced_import_export_df = self.add_aggregated_columns(import_export_df, aggregate_for_groups)

            grouped_df = enhanced_import_export_df.groupby(["Reporter Country Code (M49)", "Item Code"])

            self.import_export_cache = {x: grouped_df.get_group(x) for x in grouped_df.groups}
        else:
            return

    def get_drive_file_ids_by_name(self) -> Tuple[Resource, dict[str, str]]:
        if not os.path.isfile(self.origin_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE):
            logger.warning("Google Drive service account file not found. Falling back to local FAO files.")
            return None, None

        credentials, _ = google.auth.load_credentials_from_file(
            filename=self.origin_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE,
            scopes=[
                "https://www.googleapis.com/auth/devstorage.read_write",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive",
            ],
        )

        try:
            drive_service = build("drive", "v3", credentials=credentials)
        except HttpError as error:
            print(f"An error occurred: {error}")
            return None, None

        query = (
            f"'{self.origin_gfm_settings.GDRIVE_FAO_FOLDER_ID}' in parents "
            f"and name = '{self.origin_gfm_settings.DOMESTIC_FILE_NAME}'"
            f"or name = '{self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME}'"
            f"or name = '{self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME}'"
            f"or name = '{self.origin_gfm_settings.ITEMCODES_FILE_NAME}'"
        )
        drive_request_obj = drive_service.files().list(q=query, pageSize=4)
        results = drive_request_obj.execute()
        items = results.get("files", [])
        drive_file_ids_by_name = {item["name"]: item["id"] for item in items}

        # FIXME: not sure why this is necessary, but otherwise unit tests fail with warnings of unclosed SSL sessions:
        drive_request_obj.http.close()

        return drive_service, drive_file_ids_by_name

    async def get_file_from_third_party_source(self, request_url: str, filename: str) -> None:
        save_file_path = os.path.join(self.gfm_dir, filename)

        async with httpx.AsyncClient(timeout=60 * 30) as session:
            resp = await session.get(request_url)
            if resp.status_code == 200:
                try:
                    async with aiofiles.open(save_file_path, mode="wb") as f:
                        await f.write(resp.read())
                except Exception as e:
                    os.remove(save_file_path)
                    logger.error("Error downloading third-party file", error=e)
                    raise e
            else:
                logger.error("Error downloading third-party file", status=resp.status_code)
                raise Exception("Error downloading third-party file")

    def download_file_from_google_drive(self, drive_service: Resource, filename: str, file_id: str) -> None:
        logger.info("download file from gdrive", filename=filename, id=file_id)

        save_file_path = os.path.join(self.gfm_dir, filename)

        request = drive_service.files().get_media(fileId=file_id)
        fh = FileIO(save_file_path, "wb")
        downloader = MediaIoBaseDownload(fh, request)

        done = False

        while not done:
            try:
                _, done = downloader.next_chunk()
            except Exception as e:
                fh.close()
                os.remove(save_file_path)
                raise Exception from f"Exception {e} downloading file from Google Drive!"

        logger.info("Download finished.", filename=filename, id=file_id)
        fh.close()
        request.http.close()

    def upload_hdf_file_to_google_drive(self, drive_service: Resource, filename: str) -> None:
        logger.debug("upload hdf5 file to gdrive", filename=filename)

        file_path = os.path.join(self.gfm_dir, filename)
        file_metadata = {
            "name": filename,
            "parents": [
                self.origin_gfm_settings.GDRIVE_FAO_FOLDER_ID,
            ],
        }
        media = MediaFileUpload(file_path, resumable=True)
        request = drive_service.files().create(body=file_metadata, media_body=media, fields="id").execute()

        logger.debug("hdf5 file uploaded", filename=filename)
        request.http.close()

    async def make_sure_hdf_files_exist(self, domestic_file_path: str, code_mapping_file_path: str) -> None:
        # checking if these files exist
        if (
            not os.path.exists(self.import_export_file_path)
            or not os.path.exists(domestic_file_path)
            or not os.path.exists(code_mapping_file_path)
        ):
            drive_service, drive_file_ids_by_name = self.get_drive_file_ids_by_name()
        else:
            return

        if not os.path.exists(os.path.join(self.gfm_dir, self.origin_gfm_settings.ITEMCODES_FILE_NAME)):
            if (
                drive_file_ids_by_name is not None
                and self.origin_gfm_settings.ITEMCODES_FILE_NAME in drive_file_ids_by_name.keys()
            ):
                self.download_file_from_google_drive(
                    drive_service,
                    self.origin_gfm_settings.ITEMCODES_FILE_NAME,
                    drive_file_ids_by_name[self.origin_gfm_settings.ITEMCODES_FILE_NAME],
                )

        # making sure import/export file exists
        if not os.path.exists(self.import_export_file_path):
            if (
                drive_file_ids_by_name is not None
                and self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME in drive_file_ids_by_name.keys()
            ):
                self.download_file_from_google_drive(
                    drive_service,
                    self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME,
                    drive_file_ids_by_name[self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME],
                )
            else:
                await self.process_import_export_data_from_fao(drive_service)

        # making sure domestic production file exists
        if not os.path.exists(domestic_file_path):
            if (
                drive_file_ids_by_name is not None
                and self.origin_gfm_settings.DOMESTIC_FILE_NAME in drive_file_ids_by_name.keys()
            ):
                self.download_file_from_google_drive(
                    drive_service,
                    self.origin_gfm_settings.DOMESTIC_FILE_NAME,
                    drive_file_ids_by_name[self.origin_gfm_settings.DOMESTIC_FILE_NAME],
                )
            else:
                await self.process_domestic_production_data_from_fao(drive_service)

        # making sure M49/ISO codes mapping file exists
        if not os.path.exists(code_mapping_file_path):
            if (
                drive_file_ids_by_name is not None
                and self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME in drive_file_ids_by_name.keys()
            ):
                self.download_file_from_google_drive(
                    drive_service,
                    self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME,
                    drive_file_ids_by_name[self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME],
                )
            else:
                await self.process_codes_mapping_data_from_github(drive_service)

    async def process_import_export_data_from_fao(self, drive_service: Resource) -> None:
        # load import/export data from FAO
        await self.get_file_from_third_party_source(
            request_url=self.origin_gfm_settings.FAO_IMPORT_EXPORT_ZIP_PATH,
            filename="import_export.zip",
        )

        # unzip the archive
        with ZipFile(os.path.join(self.gfm_dir, "import_export.zip"), "r") as zip_file:
            zip_file.extract(
                member="Trade_DetailedTradeMatrix_E_All_Data_NOFLAG.csv",
                path=self.gfm_dir,
            )

        # open source CSV file
        trade_data = pd.read_csv(
            os.path.join(self.gfm_dir, "Trade_DetailedTradeMatrix_E_All_Data_NOFLAG.csv"),
            encoding="latin-1",
        )

        # clean it up

        # drop columns
        trade_data.drop(list(trade_data)[12:46], axis=1, inplace=True)
        # drop import/export values
        trade_data.drop(
            trade_data[(trade_data["Element"] == "Export Value") | (trade_data["Element"] == "Import Value")].index,
            inplace=True,
        )
        # drop rows where Y2020 and Y2021 are zero
        trade_data.drop(
            trade_data[
                ((trade_data["Y2020"].isnull()) | (trade_data["Y2020"] == 0))
                & ((trade_data["Y2021"].isnull()) | (trade_data["Y2021"] == 0))
            ].index,
            inplace=True,
        )

        # additional data type conversion
        trade_data["Reporter Country Code (M49)"].replace(regex=r"'", value="", inplace=True)
        trade_data["Reporter Country Code (M49)"] = trade_data["Reporter Country Code (M49)"].astype(int)
        trade_data["Partner Country Code (M49)"].replace(regex=r"'", value="", inplace=True)
        trade_data["Partner Country Code (M49)"] = trade_data["Partner Country Code (M49)"].astype(int)

        # save as .hdf5
        trade_data.to_hdf(
            os.path.join(self.gfm_dir, self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME),
            mode="w",
            key="df",
        )

        # upload to GDrive
        if drive_service is not None:
            self.upload_hdf_file_to_google_drive(drive_service, self.origin_gfm_settings.IMPORT_EXPORT_FILE_NAME)

    async def check_fao_codes_terms_presence(self) -> None:
        pg_term_mgr = self.postgres_db.pg_term_mgr

        fao_terms = await pg_term_mgr.get_sub_terms_by_uid(self.root_fao_term.uid, max_depth=10)

        if len(fao_terms) < 500:
            await self.process_fao_codes_data_from_fao(self.root_fao_term.uid, self.root_fao_term.access_group_uid)

    async def process_fao_codes_data_from_fao(
        self, fao_root_term_uid: uuid.UUID, fao_access_group_uid: uuid.UUID
    ) -> None:
        # load import/export data from FAO
        if not os.path.exists(os.path.join(self.gfm_dir, self.origin_gfm_settings.ITEMCODES_FILE_NAME)):
            if not os.path.exists(os.path.join(self.gfm_dir, "import_export.zip")):
                await self.get_file_from_third_party_source(
                    request_url=self.origin_gfm_settings.FAO_IMPORT_EXPORT_ZIP_PATH,
                    filename="import_export.zip",
                )

            # unzip the archive
            with ZipFile(os.path.join(self.gfm_dir, "import_export.zip"), "r") as zip_file:
                zip_file.extract(
                    member="Trade_DetailedTradeMatrix_E_ItemCodes.csv",
                    path=self.gfm_dir,
                )

        # open source CSV file
        fao_codes_data = pd.read_csv(
            os.path.join(self.gfm_dir, "Trade_DetailedTradeMatrix_E_ItemCodes.csv"),
            encoding="latin-1",
        )

        terms_to_insert = []

        for _, row in fao_codes_data.iterrows():
            fao_id = str(row.get("Item Code"))
            fao_description = row.get("Item")

            # check if a term with such FAO code exists, so that there won't be any upsert conflict
            existing_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                fao_id,
                str(fao_access_group_uid),
            )

            if existing_term:
                term_uid = existing_term.uid
            else:
                term_uid = uuid.uuid4()

            new_term = Term(
                uid=term_uid,
                xid=fao_id,
                name=fao_description,
                sub_class_of=fao_root_term_uid,
                data={},
                access_group_uid=fao_access_group_uid,
            )
            terms_to_insert.append(new_term)

        logger.debug("inserting terms into glossary", num_terms=len(terms_to_insert))

        await self.service_provider.glossary_service.put_many_terms(terms_to_insert)

    async def process_domestic_production_data_from_fao(self, drive_service: Resource) -> None:
        # load domestic data from FAO
        await self.get_file_from_third_party_source(
            request_url=self.origin_gfm_settings.FAO_DOMESTIC_ZIP_PATH,
            filename="domestic.zip",
        )

        # unzip the archive
        with ZipFile(os.path.join(self.gfm_dir, "domestic.zip"), "r") as zip_file:
            zip_file.extract(
                member="Production_Crops_Livestock_E_All_Data_NOFLAG.csv",
                path=self.gfm_dir,
            )

        # open source CSV file
        domestic_data = pd.read_csv(
            os.path.join(self.gfm_dir, "Production_Crops_Livestock_E_All_Data_NOFLAG.csv"),
            encoding="latin-1",
        )

        # clean it up

        # drop columns
        domestic_data.drop(list(domestic_data)[9:68], axis=1, inplace=True)
        # drop side information

        # In the newer files, unit is "t" not "tonnes."
        # domestic_data.drop(domestic_data[(domestic_data["Unit"] != "tonnes")].index, inplace=True)
        domestic_data.drop(domestic_data[(domestic_data["Unit"] != "t")].index, inplace=True)

        # drop rows where Y2020 and Y2021 are zero
        domestic_data.drop(
            domestic_data[
                (
                    ((domestic_data["Y2021"].isnull()) | (domestic_data["Y2021"] == 0))
                    & ((domestic_data["Y2020"].isnull()) | (domestic_data["Y2020"] == 0))
                )
            ].index,
            inplace=True,
        )

        # additional data type conversion
        domestic_data["Area Code (M49)"].replace(regex=r"'", value="", inplace=True)
        domestic_data["Area Code (M49)"] = domestic_data["Area Code (M49)"].astype(int)

        # save as .hdf5
        domestic_data.to_hdf(
            os.path.join(self.gfm_dir, self.origin_gfm_settings.DOMESTIC_FILE_NAME),
            mode="w",
            key="df",
        )

        # upload to GDrive
        if drive_service is not None:
            self.upload_hdf_file_to_google_drive(drive_service, self.origin_gfm_settings.DOMESTIC_FILE_NAME)

    async def process_codes_mapping_data_from_github(self, drive_service: Resource) -> None:
        filename = "mapping.csv"

        # load mapping csv from GitHub
        await self.get_file_from_third_party_source(
            request_url=self.origin_gfm_settings.GITHUB_MAPPING_CSV_PATH,
            filename=filename,
        )

        # open source CSV file
        mapping_data = pd.read_csv(os.path.join(self.gfm_dir, filename))

        # data clean-up & type conversion
        mapping_data.replace(regex=r'"', value="", inplace=True)  # noqa
        mapping_data["Alpha-2 code"].replace(regex=r" ", value="", inplace=True)
        mapping_data["Alpha-3 code"].replace(regex=r" ", value="", inplace=True)
        mapping_data["Numeric code"] = mapping_data["Numeric code"].astype(int)

        # save as .hdf5
        mapping_data.to_hdf(
            os.path.join(self.gfm_dir, self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME),
            mode="w",
            key="df",
        )

        # upload to GDrive
        if drive_service is not None:
            self.upload_hdf_file_to_google_drive(drive_service, self.origin_gfm_settings.ISO_M49_CODE_MAPPING_FILE_NAME)

    def spawn_worker(self, node: Node) -> OriginGapFillingWorker:
        return OriginGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = OriginGapFillingFactory
