"""Location gap filling module."""

import os
import re
import urllib.parse
import uuid
from functools import lru_cache
from io import FileIO
from typing import Dict, Optional, Tuple

import aiofiles
import fiona
import geopandas
import google
import httpx
import shapely.ops
from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util import find_access_group_uid_by_name
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.location_util.countries import (
    CHINA_SPECIAL_ADMINISTRATIVE_REGIONS,
    iso_3166_map_2_to_3_letter,
    iso_3166_official_2_letter_country_codes,
    iso_3166_official_3_letter_country_codes,
    location_rewrite_map,
    location_to_regional_term_xid_map,
)
from google.auth.exceptions import TransportError
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from pydantic_settings import SettingsConfigDict
from pyproj import Geod
from shapely.geometry import Point
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum, LocationSourceEnum
from core.domain.term import Term
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.gfm_cache_service import GfmCacheService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


# Location gap filling module. For a given node, map its string "location" if provided, to a location
# object, containing full location data, and set that to be the node's location / origin data.

# The following rules are used:
# For each of the following kind of node (left), do the action (right). "Regular treatment" means infer location as
# best as possible:
# kitchen location -> regular treatment
# recipe location -> if kitchen provided, then kitchen location (not yet implemented), else regular treatment. Todo
# ingredient origin -> regular treatment
# supply nodes -> location not applicable


class LocationGfmSettings(EnvProps):
    """Settings specific to location gap filling module."""

    TEMP_DIR: str = "./temp_data"
    GADM_BASE_URL: str = "https://geodata.ucdavis.edu/gadm/gadm4.1/gpkg/"
    GADM_FILE_TEMPLATE: str = "gadm41_{country_code_3_letter}.gpkg"
    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    GDRIVE_GADM_FOLDER_ID: str = "10eFawlhQinxzqSGJXmU02N9jODy_dPhk"
    GCP_KEY: str = "GCP_KEY"
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


def _google_api_lookup(url: httpx.URL | str) -> httpx.Response:
    response = httpx.get(url=url)
    return response


@lru_cache(maxsize=16)
def _load_country_gadm(
    gadm_file_path: str, load_layer_idx: int = None
) -> Tuple[geopandas.geodataframe.GeoDataFrame, int]:
    logger.debug("load country gadm file", gadm_file_path=gadm_file_path, load_layer_idx=load_layer_idx)
    if load_layer_idx is None:
        layers = fiona.listlayers(gadm_file_path)
        load_layer_idx = len(layers) - 1

    if gadm_file_path.endswith("CHN.gpkg") and load_layer_idx > 2:
        # all the Hong-Kong data in layer 3 is NA in GADM v4.1, so we use layer 2 instead for China as a workaround:
        load_layer_idx = 2

    # load the GADM polygons into memory:
    country_regions_gdf = geopandas.read_file(gadm_file_path, engine="fiona", layer=load_layer_idx)

    return country_regions_gdf, load_layer_idx


class LocationGapFillingWorker(AbstractGapFillingWorker):
    """Location gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule location gap filling worker."""
        # Cancel if location Prop is already filled:
        if isinstance(self.node, ActivityNode):
            if isinstance(self.node, ModeledActivityNode):
                return self.gfm_factory.service_provider.modeled_activity_node_service.should_bw_node_be_computed(
                    self.node, self.__class__.__name__
                )
            elif isinstance(self.node, ElementaryResourceEmissionNode):
                return False
            else:
                if self.node.activity_location:
                    # Always schedule on activity as one has to copy the location onto the flow node.
                    return True
                else:
                    return False
        else:
            if self.gfm_factory.service_provider.modeled_activity_node_service.should_bw_node_be_computed(
                self.node, self.__class__.__name__
            ):
                return True
            else:
                if self.node.flow_location and isinstance(self.node.flow_location, str):
                    return True

                # for root nodes we can grab kitchen location as a last measure
                elif not self.node.get_parent_nodes() and not self.node.get_sub_nodes()[0].activity_location:
                    return True

                else:
                    return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether location gap filling worker can run on this node."""
        if isinstance(self.node, ActivityNode):
            all_parents_have_location = True
            for parent_node in self.node.get_parent_nodes():
                if getattr(parent_node, GFM_STATE_PROP_NAME) and (
                    location_gfm_status := getattr(parent_node, GFM_STATE_PROP_NAME).worker_states.get(
                        LocationGapFillingWorker.__name__
                    )
                ):
                    if location_gfm_status == NodeGfmStateEnum.scheduled:
                        logger.debug("[Location] Wait for parent location gfm to finish --> reschedule.")
                        return GapFillingWorkerStatusEnum.reschedule

                if not parent_node.flow_location:
                    all_parents_have_location = False

            if (
                all_parents_have_location
                and self.node.activity_location
                and isinstance(self.node.activity_location, (DeepListView, list))
            ):
                # Cancel if location Prop is already filled:
                logger.debug(f"[Location] Node {self.node} and its parents already have locations specified.")
                return GapFillingWorkerStatusEnum.cancel

        # can always run (because it doesn't need any preconditions before it can run)
        logger.debug("[Location] --> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute location gap filling worker."""
        # given a Node with a raw location or origin property, convert that property to a complete and precise
        # location that our code can work with
        logger.debug("start running gap-filling-module LocationGapFillingWorker")

        # the user-submitted value or kitchen location
        if isinstance(self.node, ActivityNode):
            prop_name = "activity_location"
        else:
            prop_name = "flow_location"

        location_prop = getattr(self.node, prop_name)

        if not self.node.get_parent_nodes() or (location_prop and isinstance(location_prop, str)):
            raw_location, is_raw_from_location_field = await self._return_node_raw_location(location_prop)

            if not raw_location:
                logger.debug(f"Node {self.node} has no  raw origin/location input.")
                return

            locations = [
                location_prop.duplicate()
                for location_prop in await self.get_location(calc_graph, raw_location, is_raw_from_location_field)
            ]

            logger.debug("create PropMutation to add location terms.", locations=locations)

            prop_mutation = PropListMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name=prop_name,
                props=locations,
                append=False,
            )
            await calc_graph.apply_mutation(prop_mutation)
        else:
            locations = location_prop

        # for brightway nodes add location to parent_flow_nodes as well
        bw_node_service = self.gfm_factory.service_provider.modeled_activity_node_service
        should_bw_node_be_computed = bw_node_service.should_bw_node_be_computed(self.node, self.__class__.__name__)
        is_non_bw_activity = isinstance(self.node, ActivityNode) and (not isinstance(self.node, ModeledActivityNode))

        if should_bw_node_be_computed or is_non_bw_activity:
            for parent_node in self.node.get_parent_nodes():
                # Add location property of activity always to the parent flows.
                if not parent_node.flow_location:
                    logger.debug(f"Node {parent_node} received locations {locations} from its child.")
                    prop_mutation = PropListMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=parent_node.uid,
                        prop_name="flow_location",
                        props=[elem.duplicate() for elem in locations],
                        append=False,
                    )
                    await calc_graph.apply_mutation(prop_mutation)
                else:
                    if (parent_node.uid in bw_node_service.flow_uids_below_bw_nodes_to_compute) or is_non_bw_activity:
                        if parent_node.flow_location != self.node.activity_location:
                            logger.debug(
                                f"Parent node location {parent_node.flow_location} different"
                                f" from its child's {locations}."
                            )
                            await self._handle_conflicting_locations(
                                calc_graph, parent_node, [elem.duplicate() for elem in locations]
                            )

    async def _handle_conflicting_locations(
        self, calc_graph: CalcGraph, parent_node: Node, locations: list[LocationProp]
    ) -> None:
        # 1) Remove edge between activity and parent.
        remove_edge_mutation = RemoveEdgeMutation(
            created_by_module=self.__class__.__name__, from_node_uid=parent_node.uid, to_node_uid=self.node.uid
        )
        await calc_graph.apply_mutation(remove_edge_mutation)

        # 2) Add new activity below parent.

        new_activity = self.node.__class__.model_construct(
            uid=uuid.uuid4(),
            production_amount=self.node.production_amount.duplicate() if self.node.production_amount else None,
        )
        await calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=parent_node.uid,
                new_node=new_activity,
                copy=True,
            )
        )

        # 3) Add new flow below new_activity.
        new_flow_uid = uuid.uuid4()
        await calc_graph.apply_mutation(
            DuplicateNodeMutation(
                created_by_module=self.__class__.__name__,
                new_node_uid=new_flow_uid,
                source_node_uid=parent_node.uid,
                parent_node_uid=new_activity.uid,
                duplicate_child_nodes=False,
            )
        )
        await calc_graph.apply_mutation(
            PropListMutation(
                created_by_module=self.__class__.__name__,
                node_uid=new_flow_uid,
                prop_name="flow_location",
                props=locations,
                append=False,
            )
        )

        # It takes "production_amount" of original activity to produce the "production_amount" of
        # new activity since both activities are producing the same output flow.
        if self.node.production_amount:
            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=new_flow_uid,
                    prop_name="amount",
                    prop=self.node.production_amount.duplicate(),
                )
            )

        # 4) add edge between self.node and new_flow:
        await calc_graph.apply_mutation(
            AddEdgeMutation(
                created_by_module=self.__class__.__name__, from_node_uid=new_flow_uid, to_node_uid=self.node.uid
            )
        )

    async def _return_node_raw_location(self, location_prop: list[LocationProp] | str) -> tuple[str, bool]:
        """Loads raw location from node's "location" raw input fields."""
        if location_prop and isinstance(location_prop, str):
            return location_prop, True

        if not self.node.get_parent_nodes():
            return await self._return_node_raw_kitchen_location(), False

        return "", False

    async def _return_node_raw_kitchen_location(self) -> str:
        # FIXME: currently we have the access-group and thereby the kitchen location only
        # on the recipe node. Probably we want to have it on the root-flow node as well in the future.
        kitchen_info: AccessGroup = await self.gfm_factory.postgres_db.access_group_mgr.get_by_uid(
            self.node.get_sub_nodes()[0].access_group_uid,
        )
        # TODO: should we fail (raise NotImplementedError) in case there's no kitchen location set?
        # TODO: change this behaviour in case "GLO" location gets a separate term
        #  (since for now we treat it as "unknown location")
        try:
            kitchen_location_string = kitchen_info.data.get("location", "GLO")
        except AttributeError:
            kitchen_location_string = "GLO"

        logger.debug(f"Root node {self.node} has no raw location input --> using kitchen location data instead.")

        return kitchen_location_string

    async def get_location(
        self, calc_graph: CalcGraph, raw_location: str, is_raw_from_location_field: bool
    ) -> [LocationProp]:
        """Retrieve location information.

        Given a node, return a valid Location object, respecting blacklists, etc
        """
        if is_raw_from_location_field:
            source_location = raw_location
        else:
            source_location = None

        # get the location for sub-brightway nodes:
        bw_service = self.gfm_factory.service_provider.modeled_activity_node_service
        if bw_service.should_bw_node_be_computed(self.node, self.__class__.__name__):
            raw_location = bw_service.get_location_of_brightway_node(self.node, raw_location)

        if raw_location == "" or self._is_on_blacklist(raw_location):
            location = self._return_unknown_location()
            location.source_data_raw = source_location
            return [location]

        # check for e.g. "at,de,fr/ita":
        country_codes = self._match_multi_country_heuristic(raw_location)
        if len(country_codes) > 0:
            # if we have a list of country codes, we need to convert them individually into Location objects:
            locations = []
            for iso_code in country_codes:
                location = await self.get_location(calc_graph, iso_code, is_raw_from_location_field)
                locations.extend(location)
            return locations

        location = self._search_cached_locations(raw_location=raw_location)
        if location:
            logger.debug(f"Cache hit on raw_location `{raw_location}`")
            location_kwargs = {key: getattr(location, key) for key in location.__dataclass_fields__}
            location_kwargs["source_data_raw"] = source_location
            return [LocationProp.unvalidated_construct(**location_kwargs)]

        if not location:
            # check direct matching to a specific geographic region:
            location = self._get_geographic_region(raw_location)
            if location:
                location.source_data_raw = source_location
                return [location]

        if not location:
            location = await self._get_location_from_extracted_coordinates(raw_location, calc_graph)

        if not location:
            # Google's maps API doesn't work well with country codes, so we do it ourselves
            country_name = self._return_country_name_given_iso_code(raw_location)
            if country_name:
                # found a match, so we use country_name instead of country code in google maps api:
                raw_location = country_name

        if not location and raw_location.upper() in ("GLO", "ROW"):
            # TODO: Should we add a new term for GLO? or is GLO same like unknown location?
            location = self._return_unknown_location()

        if not location:
            # Google's maps api always returns something, but it can be complete nonsense. That's why it's our last pick
            location = await self._api_geolocate_string(raw_location, calc_graph)

        if not location:
            location = self._return_unknown_location()

        logger.debug(f"Mapped customer provided location `{raw_location}` to location `{location}`")
        self._update_cached_locations(raw_location=raw_location, remapped_location=location)

        location.source_data_raw = source_location
        return [location]

    def _search_cached_locations(self, raw_location: str) -> LocationProp | None:
        return self.gfm_factory.cached_location_lookups.get(raw_location)

    def _update_cached_locations(self, raw_location: str, remapped_location: LocationProp) -> None:
        # todo: add cache eviction
        self.gfm_factory.cached_location_lookups[raw_location] = remapped_location

    async def _get_location_from_extracted_coordinates(
        self,
        raw_location: str,
        calc_graph: CalcGraph,
    ) -> LocationProp | None:
        # treating raw location as coordinates, then mapping that to a real-world location, to return a Location
        # object
        try:
            # I've assumed that this heuristic is more reliable than a regex
            stripped_coords = float(raw_location.strip(" ").strip(",").strip("/"))
            logger.debug(f"using stripped coordinates {stripped_coords}.")
        except ValueError:
            # not float values
            return

        # this regex matches strings like "-2.123", "45.26", "0.00". Example fails: "0"
        lat_long_regex = re.compile(
            r"-?"
            r"\d{1-2}"
            r"\."
            r"\d{2-3}"  # optional minus  # one or two digits  # full stop  # two or three digits
        )
        results = re.findall(lat_long_regex, raw_location)

        if len(results) == 2:
            latitude = results[0]
            longitude = results[1]

            location = await self._api_geolocate_lat_long(latitude=latitude, longitude=longitude, calc_graph=calc_graph)

            return location

    async def _api_geolocate_string(self, raw_location: str, calc_graph: CalcGraph) -> LocationProp | None:
        # given a string, query the Google API for it and return the result
        return await self._api_geolocate_helper(raw_location, calc_graph)

    async def _api_geolocate_lat_long(
        self,
        latitude: float,
        longitude: float,
        calc_graph: CalcGraph,
    ) -> LocationProp | None:
        # given latitude and longitude, query the Google API for it and return the result
        val = latitude, longitude
        return await self._api_geolocate_helper(val, calc_graph)

    async def _api_geolocate_helper(self, lookup_val: str | Tuple, calc_graph: CalcGraph) -> LocationProp | None:
        # should not be directly called. Instead, call _api_geolocate_lat_long() or _api_geolocate_string()
        if isinstance(lookup_val, tuple):
            lat = str(lookup_val[0])
            lng = str(lookup_val[1])
            lookup_val = f"{lat},{lng}"
            search_by = "latlng"
        else:
            search_by = "address"

        # checking API queries cache
        cached_query = await self.gfm_factory.gmaps_query_cache_service.get_cache_entry_by_gfm_name_and_key(
            self.__class__.__name__,
            lookup_val,
        )

        if cached_query:
            result = cached_query.get("results")[0]
        else:
            creds = LocationGfmSettings()
            api_key = creds.GCP_KEY

            try:
                quoted_address = urllib.parse.quote_plus(string=lookup_val, encoding="utf-8")
                quoted_address = quoted_address.casefold()
                url = (
                    f"https://maps.googleapis.com/maps/api/geocode/json?key={api_key}&{search_by}={quoted_address}"
                    f"&sensor=false&language=de"
                )
            except ValueError as e:
                raise e from "Geocoding URL is wrongly constructed"

            response = _google_api_lookup(url)

            if not str(response.status_code).startswith("2"):
                logger.warning(f"Non 200 status code `{response.status_code}` received in Geolocation request")
                calc_graph.set_data_errors_log_entry(
                    f"Non 200 status code `{response.status_code}` received in Geolocation request to {url}"
                )

            try:
                result = response.json().get("results")[0]
            except IndexError as e:
                if response.json().get("status") == "OVER_QUERY_LIMIT":
                    raise RuntimeError("Google Maps API returned OVER_QUERY_LIMIT") from e

                # TODO: save to query cache invalid responses as well?

                logger.warning(
                    "Could not extract result from query to Google Maps API for address",
                    quoted_address=quoted_address,
                    response_status=response.json().get("status"),
                )
                calc_graph.set_data_errors_log_entry(
                    f"Could not extract result from query to Google Maps API for address `{quoted_address}`"
                )

                return

            await self.gfm_factory.gmaps_query_cache_service.set_cache_entry_by_gfm_name_and_key(
                self.__class__.__name__,
                lookup_val,
                response.json(),
            )

        country_code = None
        for ac_dict in result.get("address_components"):
            if "country" in ac_dict.get("types"):
                country_code = ac_dict.get("short_name")

        formatted_address = result.get("formatted_address")
        latitude = result.get("geometry").get("location").get("lat")
        longitude = result.get("geometry").get("location").get("lng")
        google_place_types = set(result["types"])

        if not (formatted_address and latitude and longitude):
            return

        gadm_id = await self._get_gadm_id(country_code, google_place_types, latitude, longitude)

        location = LocationProp(
            address=formatted_address,
            latitude=latitude,
            longitude=longitude,
            country_code=country_code,
            term_uid=self.gfm_factory.get_gadm_terms_by_xid(gadm_id),
            location_qualifier=LocationQualifierEnum.known,
            source=LocationSourceEnum.google,
        )

        return location

    def _get_geographic_region(self, raw_location: str) -> LocationProp | None:
        term_xid = location_to_regional_term_xid_map.get(raw_location.casefold())
        if term_xid:
            location = LocationProp(
                address="",
                latitude=None,
                longitude=None,
                country_code=None,
                term_uid=self.gfm_factory.get_geographic_terms_by_xid(term_xid),
                location_qualifier=LocationQualifierEnum.known,
                source=LocationSourceEnum.foodex,
            )

            return location
        else:
            return None

    async def _get_gadm_id(self, country_code: str, google_place_types: set, latitude: float, longitude: float) -> str:
        # map coordinate to GADM region:
        coordinates = Point(longitude, latitude)

        if "continent" in google_place_types:
            gadm_id = "TODO:CONTINENT"
            return gadm_id

        # we first need to know the iso_3166 3-letter country code to know in which GADM file we need to search in:
        if country_code in iso_3166_map_2_to_3_letter:
            country_code_3_letter = iso_3166_map_2_to_3_letter[country_code]
        else:
            # TODO: check if we might actually need to do a radius search instead here
            #  (e.g. if the coordinates are in some tiny island or border regions?)
            countries_at_point = self.gfm_factory.world.sindex.query(coordinates, predicate="within")
            country = self.gfm_factory.world.iloc[countries_at_point[0]]
            country_code_3_letter = country.iso_a3

        if country_code_3_letter == CHINA_SPECIAL_ADMINISTRATIVE_REGIONS:
            # there is no Hong Kong or Macau in GADM v4.1, so we use China instead:
            country_code_3_letter = "CHN"

        if "country" in google_place_types:
            gadm_id = country_code_3_letter
            return gadm_id

        country_regions_gdf, loaded_layer_idx = await self.gfm_factory.get_country_gadm(country_code_3_letter)

        # lookup coordinate in GADM country polygons:
        regions_index = country_regions_gdf.sindex.query(coordinates, predicate="within")
        region = country_regions_gdf.iloc[regions_index[0]]

        # Now use corresponding zoom level of administrative area returned by google api to determine the GADM ID level:
        if "administrative_area_level_1" in google_place_types:
            gadm_id = region.GID_1
        elif set.intersection({"colloquial_area", "establishment", "natural_feature"}, google_place_types):
            # we can check the size of the returned google result to determine if it is a city or a village:
            gadm_id = region.GID_0
        else:
            # use highest resolution GADM ID available:
            gadm_ids = region[region.index.str.startswith("GID_")]
            gadm_ids_sorted_by_resolution = gadm_ids.sort_index(key=lambda x: (x.str[-1].astype(int)))
            gadm_id = gadm_ids_sorted_by_resolution.iloc[-1]

        return gadm_id

    @staticmethod
    def _is_on_blacklist(raw_location: str) -> bool:
        # placeholder.
        blacklist = []
        return raw_location.casefold() in blacklist

    @staticmethod
    def _match_multi_country_heuristic(location: str) -> [str]:
        # identify raw locations where the value is 2-3 letter country codes separated by punctuation
        pattern = re.compile(r"^[A-Z]{2,3}(?:[,;/]\s*[A-Z]{2,3})+$")
        iso_codes = pattern.match(location)
        if iso_codes:
            # yes, it is a valid comma separated list of country codes:
            iso_codes = iso_codes.group()
            codes_list = re.findall(r"\b[A-Z]{2,3}\b", iso_codes)
            return codes_list
        else:
            return []

    @staticmethod
    def _return_unknown_location() -> LocationProp:
        return LocationProp(
            address="",
            latitude=0.0,
            longitude=0.0,
            country_code=None,
            term_uid=None,
            location_qualifier=LocationQualifierEnum.unknown,
            source=LocationSourceEnum.unknown,
        )

    # TODO: currently unused
    def _return_default_location(self) -> LocationProp:
        # assumption: if we use the default location, then the true location is unknown to us
        return LocationProp(
            address="Schweiz",
            latitude=46.8182,
            longitude=8.22751,
            country_code="CH",
            term_uid=self.gfm_factory.get_gadm_terms_by_xid("CHE"),
            location_qualifier=LocationQualifierEnum.unknown,
            source=LocationSourceEnum.default,
        )

    @staticmethod
    def _return_country_name_given_iso_code(raw_location: str) -> str:
        # could be changed to return a location instead
        lookup_key = raw_location.strip().upper()

        new_location = location_rewrite_map.get(lookup_key)
        if new_location:
            return new_location

        country_name = iso_3166_official_3_letter_country_codes.get(lookup_key)
        if country_name:
            return country_name

        return iso_3166_official_2_letter_country_codes.get(lookup_key)


class LocationGapFillingFactory(AbstractGapFillingFactory):
    """Location gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        # naming it differently from service class because we already have local cache in this GFM
        self.gmaps_query_cache_service: GfmCacheService = service_provider.gfm_cache_service
        self.cached_location_lookups: Dict[str, LocationProp] = {}
        self.location_gfm_settings = LocationGfmSettings()
        self.root_gadm_term: Optional[Term] = None
        self.foodex2_access_group_uid: Optional[uuid.UUID] = None
        self.gadm_dir = os.path.join(self.location_gfm_settings.TEMP_DIR, "location_gfm")
        os.makedirs(self.gadm_dir, exist_ok=True)

        self.world = geopandas.read_file(geopandas.datasets.get_path("naturalearth_lowres"))

        if not os.path.isfile(self.location_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE):
            logger.warning("Google Drive service account file not found. Falling back to local GADM files.")
            self.drive_file_ids_by_name = {}
            return

        credentials, project = google.auth.load_credentials_from_file(
            filename=self.location_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE,
            scopes=[
                "https://www.googleapis.com/auth/devstorage.read_write",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive",
            ],
        )

        try:
            self.drive_service = build("drive", "v3", credentials=credentials)
        except HttpError as error:
            print(f"An error occurred: {error}")

        gdrive_folder_id = self.location_gfm_settings.GDRIVE_GADM_FOLDER_ID

        query = f"'{gdrive_folder_id}' in parents and name contains 'gpkg'"
        drive_request_obj = self.drive_service.files().list(q=query, pageSize=1000)
        try:
            results = drive_request_obj.execute()
            items = results.get("files", [])

            self.drive_file_ids_by_name = {item["name"]: item["id"] for item in items}

            # FIXME: not sure why this is necessary. Otherwise unit tests fail with warnings of unclosed SSL sessions:
            drive_request_obj.http.close()
        except TransportError:
            logger.warning("Unable to connect to gdrive for GADM. Continuing with local cache.")

    async def init_cache(self) -> None:
        """Initialize cache for location gap filling workers."""
        # placeholder cache
        logger.debug("start filling cache of LocationGFM ...")

        self.root_gadm_term = self.service_provider.glossary_service.root_subterms.get("Root_GADM")
        self.foodex2_access_group_uid = await find_access_group_uid_by_name(
            postgres_db=self.postgres_db, namespace_name="FoodEx2 Glossary Terms namespace"
        )

        germany = LocationProp(
            address="Deutschland",
            latitude=50.205,
            longitude=9.186,
            country_code="DE",
            term_uid=self.get_gadm_terms_by_xid("DEU"),
            location_qualifier=LocationQualifierEnum.known,
            source=LocationSourceEnum.default,
        )
        france = LocationProp(
            address="France",
            latitude=46.228,
            longitude=2.214,
            country_code="FR",
            term_uid=self.get_gadm_terms_by_xid("FRA"),
            location_qualifier=LocationQualifierEnum.known,
            source=LocationSourceEnum.default,
        )
        self.cached_location_lookups = {
            "germany": germany,
            "france": france,
        }

        await self.gmaps_query_cache_service.init_individual_gfm_cache("LocationGapFillingWorker")

    async def import_data(
        self, seed_glossary: bool = True, countries_alpha3: Optional[list[str]] = None, remove_gadm_files: bool = False
    ) -> None:
        """Import location data."""
        # we use the lower level pg_term_mgr here to directly access the database:
        pg_term_mgr = self.postgres_db.pg_term_mgr

        await pg_term_mgr.get_sub_terms_by_uid(sub_class_of_uid=pg_term_mgr.root_term_uid, max_depth=1)
        root_terms = await pg_term_mgr.get_sub_terms_by_uid(pg_term_mgr.root_term_uid, 1)

        gadm_roots = [term for term in root_terms if term.xid == "Root_GADM"]
        assert len(gadm_roots) == 1

        gadm_root_term = gadm_roots[0].uid
        gadm_access_group_uid = gadm_roots[0].access_group_uid

        gadm_terms = await pg_term_mgr.get_sub_terms_by_uid(gadm_root_term, max_depth=10)

        gadm_terms_by_xid = {term.xid: term for term in gadm_terms}

        try:
            if not countries_alpha3:
                countries_alpha3 = list(iso_3166_map_2_to_3_letter.values())

            num_terms_inserted = 0
            logger.info(f"Start importing GADM data of {len(countries_alpha3)} countries...")
            for country_code_3_letter in countries_alpha3:
                if country_code_3_letter in CHINA_SPECIAL_ADMINISTRATIVE_REGIONS:
                    # skip, because these special regions are part of CHN in GADM:
                    continue

                gadm_filename = self.get_gadm_filename(country_code_3_letter)
                gadm_file_path = os.path.join(self.gadm_dir, gadm_filename)

                try:
                    await self.make_sure_gadm_file_exists(country_code_3_letter, gadm_file_path, gadm_filename)
                except FileNotFoundError:
                    logger.error("could not find GADM file", country_code_3_letter=country_code_3_letter)
                    continue

                if seed_glossary:
                    # Load and parse gadm metadata and import into glossary...
                    logger.info(f"Start seeding GADM glossary of {country_code_3_letter}...")
                    num_terms_inserted += await self.seed_gadm_glossary(
                        country_code_3_letter, gadm_file_path, gadm_terms_by_xid, gadm_root_term, gadm_access_group_uid
                    )

                # make sure to cache the file in gdrive if it is not already:
                if gadm_filename not in self.drive_file_ids_by_name:
                    logger.debug("upload file to gdrive", gadm_filename=gadm_filename)
                    file_metadata = {
                        "name": gadm_filename,
                        "parents": [self.location_gfm_settings.GDRIVE_GADM_FOLDER_ID],
                    }
                    media = MediaFileUpload(gadm_file_path, resumable=True)
                    request = (
                        self.drive_service.files().create(body=file_metadata, media_body=media, fields="id").execute()
                    )

                    logger.info("GADM file uploaded", gadm_filename=gadm_filename)
                    try:
                        request.http.close()
                    except AttributeError:
                        pass

                if remove_gadm_files:
                    os.remove(gadm_file_path)

            logger.info(f"Finished importing GADM data. Inserted {num_terms_inserted} terms into glossary.")

        except HttpError as error:
            print(f"An error occurred: {error}")

    async def make_sure_gadm_file_exists(
        self, country_code_3_letter: str, gadm_file_path: str, gadm_filename: str
    ) -> None:
        """Verify the existence of GADM file."""
        # Option 1: first check if we have the file locally already...
        if os.path.exists(gadm_file_path):
            logger.debug("gadm file already exists locally", gadm_filename=gadm_filename)
            pass

        elif gadm_filename in self.drive_file_ids_by_name:
            # Option 2: prefer to download from drive if it exists there:
            file_to_use = self.drive_file_ids_by_name[gadm_filename]
            logger.debug("download file from gdrive", gadm_filename=gadm_filename)

            request = self.drive_service.files().get_media(fileId=file_to_use)
            fh = FileIO(gadm_file_path, "wb")
            downloader = MediaIoBaseDownload(fh, request)

            done = False
            while not done:
                try:
                    status, done = downloader.next_chunk()
                except Exception as e:
                    fh.close()
                    os.remove(gadm_file_path)
                    raise Exception(f"Error downloading file from gdrive {e}") from e
            logger.debug("download finished")
            fh.close()
            request.http.close()

        else:
            # Option 3: download from GADM server:
            logger.debug("download file from gadm", gadm_filename=gadm_filename)
            try:
                await self.download_from_gadm(country_code_3_letter, gadm_filename, gadm_file_path)
            except Exception as e:
                logger.error("download failed", gadm_filename=gadm_filename, error=e)
                raise FileNotFoundError("Could not find GADM file and could not download it") from e

    async def seed_gadm_glossary(
        self,
        country_code_3_letter: str,
        gadm_file_path: str,
        gadm_terms_by_xid: dict,
        gadm_root_term: uuid.UUID,
        gadm_access_group_uid: uuid.UUID,
    ) -> int:
        """Seed glossary of terms for GADM and return the number of imported terms."""
        logger.debug("seed glossary with GADM terms", country_code_3_letter=country_code_3_letter)

        terms_to_insert: [Term] = []
        layers = fiona.listlayers(gadm_file_path)
        for load_layer_idx in range(len(layers)):
            logger.debug(
                "opening layer of geopackage to seed glossary",
                country_code_3_letter=country_code_3_letter,
                load_layer_idx=load_layer_idx,
            )

            country_regions_gdf, loaded_layer_idx = await self.get_country_gadm(country_code_3_letter, load_layer_idx)

            if loaded_layer_idx < load_layer_idx:
                logger.warning(
                    "Could not load higher resolution GADM layer. Skip this and higher resolution layers.",
                    country_code_3_letter=country_code_3_letter,
                    load_layer_idx=load_layer_idx,
                )
                break

            if (country_regions_gdf["GID_0"] == "NA").any():
                logger.warning(
                    "Loaded GADM layer contains a GID_0 set as 'NA'. Fixing it to the country of the file.",
                    country_code_3_letter=country_code_3_letter,
                    loaded_layer_idx=loaded_layer_idx,
                )
                country_regions_gdf["GID_0"] = country_code_3_letter

            if (country_regions_gdf[f"GID_{loaded_layer_idx}"] == "NA").any():
                logger.warning(
                    "Loaded GADM layer contains a region set as 'NA'. skipping that region.",
                    country_code_3_letter=country_code_3_letter,
                    loaded_layer_idx=loaded_layer_idx,
                )
                country_regions_gdf = country_regions_gdf.loc[country_regions_gdf[f"GID_{loaded_layer_idx}"] != "NA"]

            if load_layer_idx == 0 and len(country_regions_gdf) > 1:
                # It seems that this happens for disputed territories between two countries.
                logger.warning(
                    "GADM layer 0 contains more than one country.", country_code_3_letter=country_code_3_letter
                )

            missing_country_regions_gdf_in_db = False
            for _index, row in country_regions_gdf.iterrows():
                gadm_id = row.get(f"GID_{load_layer_idx}")

                if gadm_id in gadm_terms_by_xid:
                    # already imported before.
                    continue
                else:
                    missing_country_regions_gdf_in_db = True

                if load_layer_idx == 0:
                    name = row.get("COUNTRY")
                    sub_class_of = gadm_root_term
                else:
                    name = row.get(f"NAME_{load_layer_idx}")
                    parent_xid = row.get(f"GID_{load_layer_idx - 1}")
                    if parent_xid == "NA":
                        # find the nearest parent that is not NA:
                        for skip_nr in range(1, load_layer_idx):
                            parent_xid = row.get(f"GID_{load_layer_idx - skip_nr}")
                            if parent_xid != "NA":
                                break
                    if parent_xid == "NA":
                        # fallback to root country term to attach to:
                        parent_xid = country_code_3_letter
                    sub_class_of = gadm_terms_by_xid[parent_xid].uid

                # centroid calculation

                centroid = row.get("geometry").centroid

                if not centroid.within(row.get("geometry")):
                    centroid = row.get("geometry").representative_point()

                row["centroid_lat"] = centroid.y
                row["centroid_lon"] = centroid.x

                # surface area calculation

                # creating a geod to take Earth's shape into account
                geod = Geod(ellps="WGS84")
                # normalizing geometry multipolygon to get positive area value
                normalized_region_multipolygon = shapely.ops.orient(row.get("geometry"))
                row["area_m2"], _ = geod.geometry_area_perimeter(normalized_region_multipolygon)

                row.drop("geometry", inplace=True)

                new_term = Term(
                    uid=uuid.uuid4(),
                    xid=gadm_id,
                    name=name,
                    sub_class_of=sub_class_of,
                    data=dict(row),
                    access_group_uid=gadm_access_group_uid,
                )
                gadm_terms_by_xid[gadm_id] = new_term
                terms_to_insert.append(new_term)

            if load_layer_idx == 0 and not missing_country_regions_gdf_in_db:
                logger.info(f"GADM glossary of {country_code_3_letter} already exists in the database. Skipping.")
                break

        if len(terms_to_insert) > 0:
            logger.debug(
                "inserting terms into glossary",
                country_code_3_letter=country_code_3_letter,
                num_terms=len(terms_to_insert),
            )
            await self.service_provider.glossary_service.put_many_terms(terms_to_insert)
        else:
            logger.debug(
                "glossary already contained all GADM terms of country",
                country_code_3_letter=country_code_3_letter,
            )

        return len(terms_to_insert)

    async def get_country_gadm(
        self, country_code_3_letter: str, load_layer_idx: int | None = None
    ) -> Tuple[geopandas.geodataframe.GeoDataFrame, int]:
        """Get GADM data for a country."""
        gadm_filename = self.get_gadm_filename(country_code_3_letter)

        gadm_file_path = os.path.join(self.gadm_dir, gadm_filename)
        await self.make_sure_gadm_file_exists(country_code_3_letter, gadm_file_path, gadm_filename)

        country_regions_gdf, loaded_layer_idx = _load_country_gadm(gadm_file_path, load_layer_idx)

        return country_regions_gdf, loaded_layer_idx

    def get_gadm_terms_by_xid(self, gadm_id: str) -> uuid.UUID | None:
        """Get GADM term uid from xid."""
        gadm_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            (gadm_id, self.root_gadm_term.access_group_uid)
        )

        if gadm_term:
            return gadm_term.uid
        else:
            return None

    def get_geographic_terms_by_xid(self, term_xid: str) -> uuid.UUID | None:
        """Get geographic term uid from xid."""
        term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get((term_xid, self.foodex2_access_group_uid))

        if term:
            return term.uid
        else:
            return None

    def get_gadm_filename(self, country_code_3_letter: str) -> str:
        """Obtain GADM file's name."""
        gadm_filename = self.location_gfm_settings.GADM_FILE_TEMPLATE.format(
            country_code_3_letter=country_code_3_letter
        )
        return gadm_filename

    async def download_from_gadm(self, country_code_3_letter: str, gadm_filename: str, gadm_file_path: str) -> None:
        """Download GADM data."""
        logger.debug("Download GADM country data...", country_code_3_letter=country_code_3_letter)
        request_url = self.location_gfm_settings.GADM_BASE_URL + gadm_filename
        async with httpx.AsyncClient(timeout=60 * 30) as session:
            resp = await session.get(request_url)
            if resp.status_code == 200:
                try:
                    async with aiofiles.open(gadm_file_path, mode="wb") as f:
                        await f.write(resp.read())
                except Exception as e:
                    os.remove(gadm_file_path)
                    logger.error("Error downloading GADM file", error=e)
                    raise e
            else:
                logger.error("Error downloading GADM file", status=resp.status_code)
                raise Exception("Error downloading GADM file")

    def spawn_worker(self, node: Node) -> LocationGapFillingWorker:
        """Spawn location gap filling worker."""
        return LocationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = LocationGapFillingFactory
