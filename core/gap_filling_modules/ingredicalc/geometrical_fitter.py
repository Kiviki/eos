# TODO this code is not used ATM; either remove it or use/test it
"""
Code created in 2019-2020 by Jan Machacek, Kristin Schlaepfer and Jens Hinkelmann.
"""
import cvxpy as cp
import numpy as np
from gap_filling_modules.ingredicalc.helpers import (
    make_constraint_matrix,
    make_equality_matrix_and_vector,
    make_expected_geometric_series,
    make_fixed_precentages_matrix_and_vector,
)
from structlog import get_logger

logger = get_logger()


def run(flat_nutrients, fixed_percentages):
    # goal: minimize the difference to the series 0.5, 0.25, 0.125, ...
    log = logger.bind(nr_products_to_adjust=len(flat_nutrients), method="geometrical series")
    try:
        log.debug("Starting geometrical series approximation")

        # N products
        product_order = sorted(list(flat_nutrients.keys()))
        N = len(product_order)

        # values
        x = cp.Variable(N)

        # target series
        a = make_expected_geometric_series(product_order, N)

        # N x N constraint matrix
        C = make_constraint_matrix(product_order, N)

        # N x 1 constraint vector
        d = np.zeros(N)

        # N x 1 constraint vector
        # amounts bigger than zero
        e = np.zeros(N)

        # (nr_recipes) x N matrix F, (nr_recipes) x 1 vector g
        # Sum to 1, also on sublevels
        F, g = make_equality_matrix_and_vector(product_order, N)

        # (nr_fixed_percentages) x N matrix H, (nr_fixed_percentages) x 1 vector i
        # set percentages, set fixed relationships
        H, i = make_fixed_precentages_matrix_and_vector(product_order, fixed_percentages, N)

        objective = cp.Minimize(cp.norm(x - a))
        constraints = [C @ x <= d, x >= e, F @ x == g]
        if fixed_percentages:
            constraints.append(H @ x == i)

        problem = cp.Problem(objective, constraints)
        log.debug("Problem definition", problem=str(problem))
        problem.solve()
        log.debug("Solution", solution=str(problem.solution))

        if problem.solution.status != "optimal" and fixed_percentages:
            log.warning(
                "Optimisation did not converge, retrying without fixed_percentages",
                problem=str(problem),
                solution=str(problem.solution),
            )

            x = cp.Variable(N)
            objective = cp.Minimize(cp.norm(x - a))
            constraints = [C @ x <= d, x >= e, F @ x == g]
            problem = cp.Problem(objective, constraints)
            problem.solve()
            log.debug("Solution without fixed_percentages", solution=str(problem.solution))

        if problem.solution.status != "optimal":
            log.error("Optimisation did not converge", problem=str(problem), solution=str(problem.solution))
            return None

        solution_vector = x.value * 100.0

        log.debug("Finished geometrical series approximation")
        return {k: v for k, v in zip(product_order, solution_vector)}
    finally:
        log.unbind("nr_products_to_adjust", "method")
