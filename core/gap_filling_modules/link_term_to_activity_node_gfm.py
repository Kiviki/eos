"Link term activity to Node gap filling module."
from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.glossary_link import GlossaryLink
from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.flow.practice_flow import PracticeFlowNode
from core.domain.nodes.node import Node
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.term import Term, sorted_uuid_tuple
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class LinkTermToActivityNodeGapFillingWorker(AbstractGapFillingWorker):
    """Links a product Term to an LCA process.

    Looks for a Node of type product|ingredient, with a Prop with key `product_name`.
    In this Prop, we will find a Term.
    We then search in glossary_link for a link btw this Term and an LCA process.
    Finally, we load this LCA process as child to the current product|ingredient Node.
    """

    def __init__(self, node: Node, gfm_factory: "LinkTermToActivityNodeGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Here we just filter for the right node type.

        We will check later in `run()` for prop `product_name`.
        If instead we checked here for prop `product_name`, MatchProductNameGFM would not have added the prop.
        """
        if not isinstance(
            self.node, (FoodProductFlowNode, PracticeFlowNode)
        ):  # Should we include other flow nodes here?
            logger.debug("[LinkTermToActivityNode] node is not a food product or practice --> not scheduled.")
            return False
        else:
            logger.debug("[LinkTermToActivityNode] node is a food product or practice --> scheduled.")
            return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        logger.debug(f"start checking whether LinkTermToActivityNode GFM can be run on {self.node}")

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(NutrientSubdivisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                "[LinkTermToActivityNode] recipe root-node does not yet have ingredients amount estimation calculated"
                " --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule
        elif self.node.get_sub_nodes():
            logger.debug("[LinkTermToActivityNode] cancel GFM worker because a conflict may happen.")
            return GapFillingWorkerStatusEnum.cancel
        elif self.node.product_name and isinstance(self.node.product_name, NamesProp):
            logger.debug("[LinkTermToActivityNode] found product_name --> can_run_now")
            return GapFillingWorkerStatusEnum.ready
        # TODO: is this amount overkill? needs to be discussed later
        elif self.reschedule_counter >= 10:
            logger.debug(
                f"[LinkTermToActivityNode] cancel GFM worker " f"because rescheduled {self.reschedule_counter} times."
            )
            return GapFillingWorkerStatusEnum.cancel
        else:
            logger.debug("[LinkTermToActivityNode] node does not have product_name prop --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug(f"start running gap-filling-module LinkTermToActivityNode GFM on {self.node}")
        terms: list[Term] = []
        if self.node.product_name and isinstance(self.node.product_name, NamesProp):
            terms = [p.get_term() for p in self.node.product_name.terms]
        terms_cache_key: tuple[UUID] = sorted_uuid_tuple(terms)

        if terms_cache_key in self.gfm_factory.cache_matching_glossary_links_to_node_ids:
            lca_process_node_uid: UUID = self.gfm_factory.cache_matching_glossary_links_to_node_ids[terms_cache_key]

            # if node already exists in the graph, we need to just add an edge:
            if calc_graph.get_node_by_uid(lca_process_node_uid):
                logger.debug(f"this LCA node already exists in the graph, node uid {lca_process_node_uid}")
                add_edge_mutation = AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=self.node.uid,
                    to_node_uid=lca_process_node_uid,
                )
                await calc_graph.apply_mutation(add_edge_mutation)
            else:
                lca_process: Node = await self.gfm_factory.postgres_db.get_graph_mgr().find_by_uid(lca_process_node_uid)
                logger.debug(f"create AddNodeMutation for new {lca_process}...")
                lca_unit: str = lca_process.production_amount.unit

                # we have a food_product_flow: add the lca process directly as child
                await calc_graph.apply_mutation(
                    AddNodeMutation(
                        created_by_module=self.__class__.__name__,
                        parent_node_uid=self.node.uid,
                        new_node=lca_process,
                        copy=True,
                    )
                )

                lca_unit_term = self.gfm_factory.service_provider.glossary_service.terms_by_xid_ag_uid[
                    (
                        UnitWeightConverter.unit_to_eos_unit_xid(
                            lca_unit, self.gfm_factory.service_provider.glossary_service
                        ),
                        self.gfm_factory.unit_term_access_group_uid,
                    )
                ]

                lca_production_amount: float = lca_process.production_amount.value
                production_amount_prop_mutation = PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=lca_process.uid,
                    prop_name="production_amount",
                    prop=QuantityProp(
                        value=lca_production_amount,
                        unit_term_uid=lca_unit_term.uid,
                        for_reference=ReferenceAmountEnum.self_reference,
                        source_data_raw=lca_process.production_amount.model_dump()
                        if lca_process.production_amount
                        else None,
                    ),
                )
                await calc_graph.apply_mutation(production_amount_prop_mutation)

        else:
            logger.warning("Could not find Brightway mapping for FoodEx2 terms in cache", terms=terms)
            calc_graph.set_data_errors_log_entry(f"Could not find Brightway mapping for FoodEx2 terms {terms} in cache")


class LinkTermToActivityNodeGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.cache_matching_glossary_links_to_node_ids: dict[tuple[UUID], UUID] = {}  # term uid: brightway_process uid
        self.unit_term_access_group_uid: UUID | None = None

    async def init_cache(self) -> None:
        pg_glossary_link_mgr = self.postgres_db.get_pg_glossary_link_mgr()
        glossary_links: list[GlossaryLink] = await pg_glossary_link_mgr.get_data_of_gfm("LinkTermToActivityNode")

        for glossary_link in glossary_links:
            terms = [self.service_provider.glossary_service.terms_by_uid[t] for t in glossary_link.term_uids]
            assert glossary_link.linked_node_uid is not None, f"linked_node_uid is None for {glossary_link}"
            self.cache_matching_glossary_links_to_node_ids[sorted_uuid_tuple(terms)] = glossary_link.linked_node_uid

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.unit_term_access_group_uid = root_unit_term.access_group_uid

        logger.debug(f"added {len(glossary_links)} terms to LinkTermToActivityNode GFM's cache")

    def spawn_worker(self, node: Node) -> LinkTermToActivityNodeGapFillingWorker:
        return LinkTermToActivityNodeGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = LinkTermToActivityNodeGapFillingFactory
