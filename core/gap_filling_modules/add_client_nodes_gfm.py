"Add client Nodes gap filling module."
from uuid import UUID, uuid4

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from structlog import get_logger

from core.domain.edge import Edge
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.nodes.activity.food_processing_activity import FoodProcessingActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class AddClientNodesGapFillingWorker(AbstractGapFillingWorker):
    "Add client nodes to the root node."

    def __init__(self, node: Node):
        """Load child nodes of recipes, sub-recipes and ingredients."""
        super().__init__(node)

    def should_be_scheduled(self) -> bool:
        "Scheduled for FoodProductFlowNode and all activity nodes that are not inventory nodes."
        # FIXME run also on root-FoodProductFlowNode not just activity nodes?
        if not isinstance(self.node, ActivityNode):
            return False

        # do not run on inventory nodes, because the inventory connector will do that specifically:
        if isinstance(self.node, (ModeledActivityNode, ElementaryResourceEmissionNode)):
            return False

        # run on all activity nodes that are not inventory nodes:
        return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        "No special conditions needed, always ready."
        logger.debug("--> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    async def add_node_mutation(
        self, calc_graph: CalcGraph, node_uid: UUID, parent_uid: UUID, duplicate_node_uid: UUID | None = None
    ) -> None:
        """Helper function to add a node mutation to the calc_graph."""
        # check if node already exists in this calc-graph:
        add_new_node_to_graph = False
        existing_node_in_graph = None

        if duplicate_node_uid:
            add_new_node_to_graph = True
        else:
            existing_node_in_graph = calc_graph.get_node_by_uid(node_uid)
            if existing_node_in_graph is None:
                add_new_node_to_graph = True

        if add_new_node_to_graph:
            node_to_add = await calc_graph.service_provider.node_service.find_by_uid(node_uid)
            parent_node = calc_graph.get_node_by_uid(parent_uid)

            if node_to_add is None:
                raise ValueError(f"node not found for uid {node_uid}")
            if parent_node is None:
                raise ValueError(f"parent node not found for uid {parent_uid}")

            original_node_uid = node_to_add.uid
            if duplicate_node_uid:
                node_to_add.uid = duplicate_node_uid
                node_to_add.xid = None

            if isinstance(node_to_add, FlowNode) and isinstance(parent_node, FlowNode):
                # we have a link from a flow to a flow, therefore we need to add on activity in between
                # Step 1: add an auxiliary activity node in between:
                logger.debug(f"create auxiliary activity node for {node_to_add}...")
                if isinstance(node_to_add.amount, QuantityProp):
                    flow_amount = node_to_add.amount.amount_for_activity_production_amount()
                    production_amount = QuantityProp.unvalidated_construct(
                        amount=flow_amount.value,
                        unit_term_uid=flow_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.self_reference,
                    )
                else:
                    # FIXME: if "unit": "production amount", then convert properly
                    production_amount = node_to_add.amount_in_original_source_unit
                auxiliary_activity_node = FoodProcessingActivityNode(
                    uid=uuid4(),
                    access_group_uid=parent_node.access_group_uid if parent_node.access_group_uid else None,
                    access_group_xid=parent_node.access_group_xid if parent_node.access_group_xid else None,
                    production_amount=production_amount,
                )
                auxiliary_activity_node.gfm_state = GfmStateProp.unvalidated_construct(
                    worker_states={
                        "AddClientNodesGapFillingWorker": NodeGfmStateEnum.canceled,
                    }
                )
                logger.debug(f"create AddNodeMutation for {auxiliary_activity_node}...")
                add_auxiliary_activity_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=parent_uid,
                    new_node=auxiliary_activity_node,
                    copy=True,
                )
                await calc_graph.apply_mutation(add_auxiliary_activity_node_mutation)
                # step 2: connect to the linked Flow:
                logger.debug(f"create AddNodeMutation for {node_to_add}...")
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=auxiliary_activity_node.uid,
                    new_node=node_to_add,
                    copy=True,
                )
                await calc_graph.apply_mutation(add_child_node_mutation)
                # step 3: connect the linked flow to its subactivity
                if not duplicate_node_uid:
                    # If duplicate_node_uid, all child nodes must also be duplicated. Handled in
                    # recursively_add_edges defined below.
                    edges = await calc_graph.service_provider.node_service.get_sub_graph_by_uid(
                        original_node_uid, max_depth=1
                    )
                    assert len(edges) == 1, f"Flow nodes should have exactly one subactivity, but got {edges}"
                    await self.add_node_mutation(calc_graph, edges[0].child_uid, node_to_add.uid)
            else:
                logger.debug(f"create AddNodeMutation for {node_to_add}...")
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=parent_uid,
                    new_node=node_to_add,
                    copy=True,
                )
                await calc_graph.apply_mutation(add_child_node_mutation)

                # need to also load the linked sub-recipes:
                if isinstance(node_to_add, FlowNode) and node_to_add.link_to_sub_node is not None:
                    link_to_xid = None
                    link_to_uid = None
                    link_to_sub_node = node_to_add.link_to_sub_node
                    if type(link_to_sub_node) is LinkToXidProp:
                        link_to_xid = link_to_sub_node.xid
                        if link_to_xid is None:
                            raise ValueError(f"link_to_sub_node xid is None for {node_to_add}")
                    elif type(link_to_sub_node) is LinkToUidProp:
                        link_to_uid = link_to_sub_node.uid
                        if link_to_uid is None:
                            raise ValueError(f"link_to_sub_node uid is None for {node_to_add}")
                    else:
                        raise ValueError(f"unknown type for link_to_sub_node {type(link_to_sub_node)}")

                    if link_to_uid is None:
                        link_to_uid = await self.find_uid_from_xid(
                            xid=link_to_xid,
                            node=node_to_add,
                            calc_graph=calc_graph,
                        )

                    new_uid = uuid4()
                    await self.add_node_mutation(
                        calc_graph,
                        link_to_uid,
                        node_to_add.uid,
                        duplicate_node_uid=new_uid if node_to_add.link_to_sub_node.duplicate_sub_node else None,
                    )
                    if node_to_add.link_to_sub_node.duplicate_sub_node:
                        # Must recursively duplicate child edges.
                        async def recursively_add_edges(edges_to_add: list[Edge], parent_node_uid: UUID) -> None:
                            for edge in edges_to_add:
                                duplicate_child_node_uid = uuid4()
                                await self.add_node_mutation(
                                    calc_graph,
                                    edge.child_uid,
                                    parent_node_uid,
                                    duplicate_node_uid=duplicate_child_node_uid,
                                )
                                added_child_node = await calc_graph.service_provider.node_service.find_by_uid(
                                    edge.child_uid
                                )
                                if (
                                    isinstance(added_child_node, ModeledActivityNode)
                                    and added_child_node.aggregated_cache
                                ):
                                    # Truncate the recursive addition once we encounter a ModeledActivityNode with
                                    # aggregated_cache, adding its children should be handled by InventoryConnectorGFM.
                                    return

                                recursive_edges_to_add = (
                                    await calc_graph.service_provider.node_service.get_sub_graph_by_uid(
                                        edge.child_uid, max_depth=1
                                    )
                                )
                                await recursively_add_edges(recursive_edges_to_add, duplicate_child_node_uid)

                        edges = await calc_graph.service_provider.node_service.get_sub_graph_by_uid(
                            link_to_uid, max_depth=1
                        )
                        await recursively_add_edges(edges, new_uid)

        else:
            logger.debug(
                f"dont add child node because it already exists in the graph ({existing_node_in_graph}); only add edge."
            )
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_uid,
                to_node_uid=existing_node_in_graph.uid,
            )
            await calc_graph.apply_mutation(add_edge_mutation)

    async def run(self, calc_graph: CalcGraph) -> None:
        "Run the worker."
        logger.debug(f"start running gap-filling-module AddClientNodesGFM on {self.node}")

        # use max_depth=2 to get all sub-flows and their sub-activities.
        # There will be a separately scheduled worker on these newly added sub-activities:
        edges = await calc_graph.service_provider.node_service.get_sub_graph_by_uid(self.node.uid, max_depth=2)

        for edge in edges:
            await self.add_node_mutation(calc_graph, edge.child_uid, edge.parent_uid)

    @staticmethod
    async def find_uid_from_xid(xid: str, node: Node, calc_graph: CalcGraph) -> UUID:
        # use this node's access_group to get namespace, in which to lookup for the xid:
        access_group_uid = node.access_group_uid

        access_group = await calc_graph.service_provider.access_group_service.get_access_group_by_uid(
            str(access_group_uid)
        )

        uid = await calc_graph.service_provider.postgres_db.product_mgr.find_uid_by_xid(
            xid=xid, namespace_uid=access_group.namespace_uid
        )

        if uid is None:
            raise ValueError(f"xid {xid} not found in " f"namespace {access_group.namespace_uid}")

        return uid


class AddClientNodesGapFillingFactory(AbstractGapFillingFactory):
    "Factory."

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)

    async def init_cache(self) -> None:
        "Do nothing."
        pass

    def spawn_worker(self, node: Node) -> AddClientNodesGapFillingWorker:
        "Spawn worker."
        return AddClientNodesGapFillingWorker(node)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AddClientNodesGapFillingFactory
