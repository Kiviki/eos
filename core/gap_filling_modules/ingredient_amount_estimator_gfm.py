"Ingredient amount estimator gap filling module."
import uuid
from typing import Optional, Tuple
from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop
from gap_filling_modules.ingredicalc.optimiser import run
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker, UnitWeightConverter
from structlog import get_logger

from core.domain.deep_mapping_view import DeepMappingView
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.props import (
    IngredientAmountEstimatorStatusProp,
    QuantityPackageProp,
    QuantityProp,
    ReferencelessQuantityProp,
)
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()

# TODO: for now, this default amount will be equal to 1 -- this needs to be discussed & fixed later
DEFAULT_CONVERTED_AMOUNT = 1
IngredientKey = Tuple[tuple, str, UUID, int, str]


class IngredientAmountEstimatorGapFillingWorker(AbstractGapFillingWorker):
    """Estimate the amount of each ingredient in a product, based on the nutrient values of individual ingredients.

    And the overall nutrient values of the product. Relies on a library `ingredicalc` previousely written at Eaternity.
    Uses CVXPY under the hood, a library for convex optimization problems.
    """

    def __init__(self, node: Node, gfm_factory: "IngredientAmountEstimatorGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        "Scheduled if the node holds nutrient_values and is a Foodprocessing activity."
        if not isinstance(self.node, FoodProcessingActivityNode):
            logger.debug("[IngredientAmountEstimator] Node is not recipe or food_product --> not scheduled.")
            return False
        else:
            parent_nutrient_values_available = False
            if self.node.get_parent_nodes():
                for parent_node in self.node.get_parent_nodes():
                    parent_nutrient_values = parent_node.nutrient_values
                    if parent_node.nutrient_values and isinstance(parent_node.nutrient_values, (DeepMappingView, dict)):
                        parent_nutrient_values_available = True
                    elif parent_nutrient_values and parent_nutrient_values.quantities:
                        parent_nutrient_values_available = True
                    if parent_nutrient_values_available:
                        break

            if parent_nutrient_values_available:
                logger.debug("[IngredientAmountEstimator] Found nutrient_values inside node --> scheduled.")
                return True
            else:
                logger.debug(
                    f"[IngredientAmountEstimator] nutrient_values is empty " f"on node {self.node} --> not scheduled."
                )
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to estimate ingredient amounts *after* matching for product names and performing subdivision.

        Because it attaches the Term's nutrient values.
        """
        # check if sub-node and parent-node have the same nutrient-values, in this case we don't need to run the GFM
        parent_food_product_flow_nodes = [
            parent_flow for parent_flow in self.node.get_parent_nodes() if isinstance(parent_flow, FoodProductFlowNode)
        ]
        child_food_product_flow_nodes = [
            child_flow for child_flow in self.node.get_sub_nodes() if isinstance(child_flow, FoodProductFlowNode)
        ]
        if len(parent_food_product_flow_nodes) == 1 and len(child_food_product_flow_nodes) == 1:
            if parent_food_product_flow_nodes[0].nutrient_values == child_food_product_flow_nodes[0].nutrient_values:
                logger.debug(
                    f"[IngredientAmountEstimator] Parent and sub-node have the same nutrient values, "
                    f"no need to run the GFM on {self.node} --> cancel."
                )
                return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(UnitWeightConversionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for UnitWeightConversionGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for AddClientNodesGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for MatchProductNameGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for IngredientSplitterGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(NutrientSubdivisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[IngredientAmountEstimator] waiting for NutrientSubdivision GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(LinkTermToActivityNodeGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[IngredientAmountEstimator] waiting for LinkTermToActivity GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        # ensure that this GFM is run first on the subnodes.
        # if any child nodes of the current node still have
        # ingredient amount estimator scheduled, it is *not* ready to run on it
        # have to recurse the whole tree, some intermediate notes might not have nutrient values and wont be scheduled
        def has_child_scheduled(current_node: Node) -> bool:
            for sub_flow_node in current_node.get_sub_nodes():
                for sub_activity_node in sub_flow_node.get_sub_nodes():
                    if not isinstance(sub_activity_node, FoodProcessingActivityNode):
                        continue
                    if (
                        sub_activity_node.gfm_state.worker_states.get(self.__class__.__name__)
                        == NodeGfmStateEnum.scheduled
                    ):
                        return True
                    if has_child_scheduled(sub_activity_node):
                        return True
            return False

        if not hasattr(self.node, "production_amount") or not (
            self.node.production_amount and isinstance(self.node.production_amount, QuantityProp)
        ):
            logger.debug("[IngredientAmountEstimator] waiting for production amount to be added --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        else:
            logger.debug("[IngredientAmountEstimator] Found nutrient_values and production_amount --> ready.")
            if has_child_scheduled(self.node):
                return GapFillingWorkerStatusEnum.reschedule
            else:
                return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        "Rund the GFM."
        logger.debug(f"start running gap-filling-module IngredientAmountEstimatorGFM on {self.node}")

        # collecting data for algorithm -- see ingredicalc.optimiser.run() for details
        flat_nutrients: dict[IngredientKey, QuantityPackageProp | None] = {}
        fixed_percentages: dict[IngredientKey, Optional[float]] = {}
        missing_leaf_uids: set[uuid.UUID] = set()

        async def visit_sub_ingredients(parent_food_product: Node, parent_level: Tuple[int, ...]) -> None:
            # Populates flat_nutrients and fixed_percentages: recursively visits all sub-ingredients; captures the
            # ingredients and their nutrient values.
            # regarding the graph structure: sub-ingredients nodes have been created by ingredient_splitter_gfm,
            # which interlaces a food_product_flow and a food_product for each ingredient level.
            this_level_index = 0

            sub_nodes = parent_food_product.get_sub_nodes()
            sub_nodes = sorted(
                sub_nodes,
                key=lambda x: (
                    x.declaration_index.value if hasattr(x, "declaration_index") and x.declaration_index else 0
                ),
            )

            for recipe_ingredient in sub_nodes:
                if isinstance(recipe_ingredient, FoodProductFlowNode):
                    # this tuple informs the optimiser about the hierarchy of the ingredients, e.g.:
                    # "ingredients_declaration": 'Main0 (Sub00, Sub01), Main1 (Sub10, Sub11)',
                    # (0,)    is for  Main0
                    # (0, 0)  is for  Sub00  (the last 0 here is given by `this_level_index`)
                    # (0, 1)  is for  Sub01
                    # (1,)    is for  Main1
                    # (1, 0)  is for  Sub10
                    # (1, 1)  is for  Sub11
                    level_tuple = (*parent_level, this_level_index)

                    # nutrient_values are attached by MatchProductNameGFM
                    nutrients = recipe_ingredient.nutrient_values

                    try:
                        nutrient_xid = nutrients.get_prop_term().xid
                    except AttributeError:
                        nutrient_xid = None

                    if recipe_ingredient.is_subdivision:
                        if recipe_ingredient.is_dried() and recipe_ingredient.nutrient_upscale_ratio:
                            # No direct ingredient amount estimation is necessary for nutrient scale dummy node.
                            continue

                        is_subdivision = True
                    else:
                        is_subdivision = False

                    # we have to pass a key to the optimiser, and it gives it back to us, so we can match results
                    # we need to pass the child's uid (not grandchild uid), because we update amounts on child nodes
                    key: IngredientKey = (level_tuple, "_", recipe_ingredient.uid, is_subdivision, nutrient_xid)
                    # 2nd element is namespace, not used
                    # 4th element is important: If it is True, it will not apply the order constraint!
                    # 5th element is ID of nutrient file used

                    if nutrients is not None and isinstance(nutrients, QuantityPackageProp) and nutrients.quantities:
                        flat_nutrients[key] = nutrients.amount_for_100g()
                    else:
                        # we don't have nutrient values for this ingredient, but need to pass it to the optimiser
                        flat_nutrients[key] = None

                        # having no nutrients is a problem, if this is a leaf node (none or only brightway sub-nodes)
                        if not recipe_ingredient.get_sub_nodes() or all(
                            [
                                isinstance(sub_node, ModeledActivityNode)
                                for sub_node in recipe_ingredient.get_sub_nodes()
                            ]
                        ):
                            missing_leaf_uids.add(recipe_ingredient.uid)

                    sub_nodes = recipe_ingredient.get_sub_nodes()

                    if len(sub_nodes) > 0 and isinstance(sub_nodes[0], FoodProcessingActivityNode):
                        # skipping estimation of ingredients with converted amount
                        sub_sub_nodes = sub_nodes[0].get_sub_nodes()
                        has_converted_amount = [node.amount is not None for node in sub_sub_nodes]

                        if not all(has_converted_amount):
                            # if there is at least 1 product with unknown amount,
                            # sub_product nutrients should be used instead
                            flat_nutrients[key] = None

                    # Convert raw percentage float into amount property.
                    percentage_value = None
                    add_original_amount_source = False
                    if original_amount := recipe_ingredient.amount_in_original_source_unit:
                        percentage_term = self.gfm_factory.percentage_term
                        if isinstance(original_amount, (DeepMappingView, dict)) and (
                            original_amount.get("unit", "").lower() == percentage_term.name.lower()
                            or original_amount.get("unit", "").lower() in percentage_term.data.get("alias")
                        ):
                            percentage_value = original_amount.get("value")
                            add_original_amount_source = True

                    if percentage_value:
                        # convert percentage to QuantityProp if it is a float
                        await calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=recipe_ingredient.uid,
                                prop_name="amount_in_original_source_unit",
                                prop=QuantityProp(
                                    value=percentage_value,
                                    unit_term_uid=self.gfm_factory.percentage_term.uid,
                                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                    source_data_raw=(
                                        recipe_ingredient.amount_in_original_source_unit
                                        if add_original_amount_source
                                        else None
                                    ),
                                ),
                            )
                        )

                    if original_amount := recipe_ingredient.amount_in_original_source_unit:
                        if (
                            isinstance(original_amount, QuantityProp)
                            and original_amount.get_unit_term().uid == self.gfm_factory.percentage_term.uid
                        ):
                            # percentage provided as a number btw 0 and 100, but optimiser expects a probability
                            fixed_percentages[key] = original_amount.value / 100

                    elif original_amount := recipe_ingredient.amount:
                        parent_product_amount = recipe_ingredient.get_parent_nodes()[0].production_amount
                        parent_product_amount_in_g = UnitWeightConverter.convert_between_same_unit_types(
                            parent_product_amount.value,
                            parent_product_amount.get_unit_term(),
                            self.gfm_factory.gram_term,
                            "mass-in-g",
                        )
                        original_amount_in_g = UnitWeightConverter.convert_between_same_unit_types(
                            original_amount.value,
                            original_amount.get_unit_term(),
                            self.gfm_factory.gram_term,
                            "mass-in-g",
                        )
                        fixed_percentages[key] = original_amount_in_g / parent_product_amount_in_g

                    this_level_index += 1

                    for sub_node in recipe_ingredient.get_sub_nodes():
                        if isinstance(sub_node, FoodProcessingActivityNode):
                            await visit_sub_ingredients(sub_node, level_tuple)  # recurse

        await visit_sub_ingredients(self.node, ())

        if len(flat_nutrients) == 0:
            logger.debug("cannot run ingredient amount estimation, because there are no ingredients with nutrients")
            return

        if missing_leaf_uids:
            # Don't have to raise for missing leaf if the len(flat_nutrients) == len(fixed_percentages) and
            # all percentages are defined.:
            if len(flat_nutrients) == len(fixed_percentages) and len(missing_leaf_uids) == len(flat_nutrients):
                if set(flat_nutrients.keys()) == set(fixed_percentages.keys()):
                    for key in flat_nutrients:
                        _, _, missing_leaf_uid, _, _ = key
                        missing_leaf_node = calc_graph.get_node_by_uid(missing_leaf_uid)

                        if missing_leaf_node.get_sub_nodes():
                            flow_amount = QuantityProp(
                                value=missing_leaf_node.get_sub_nodes()[0].production_amount.value
                                * fixed_percentages[key],
                                unit_term_uid=missing_leaf_node.get_sub_nodes()[0].production_amount.unit_term_uid,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                            )
                        else:
                            # Leaf nodes attach to ModeledActivityNodes with production_amount = 1kg.
                            flow_amount = QuantityProp(
                                value=1.0 * fixed_percentages[key],
                                unit_term_uid=self.gfm_factory.kilogram_term.uid,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                            )  # in kg

                        await calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=missing_leaf_uid,
                                prop_name="amount",
                                prop=flow_amount,
                            )
                        )
                    return

            raise ValueError(
                "No nutrients found for "
                f"ingredients {[calc_graph.get_node_by_uid(elem) for elem in missing_leaf_uids]}!"
                " Ask Science team to provide this data."
            )

        # read the nutrient values from this recipe or product; again: no Views object here, else fails in optimiser
        # we have already checked for the presence of nutrient_values in should_be_scheduled()
        incoming_nutrients_prop = None
        for parent_node in self.node.get_parent_nodes():
            parent_nutrient_values = parent_node.nutrient_values
            if parent_nutrient_values and isinstance(parent_nutrient_values, (DeepMappingView, dict)):
                incoming_nutrients_prop = nutrients_dict_to_prop(parent_nutrient_values)
                incoming_nutrients_prop.source_data_raw = parent_nutrient_values

                await calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=parent_node.uid,
                        prop_name="nutrient_values",
                        prop=incoming_nutrients_prop.duplicate(),
                    )
                )
                break
            elif parent_nutrient_values and parent_nutrient_values.quantities:
                incoming_nutrients_prop = parent_nutrient_values.duplicate()
                break

        if (
            not incoming_nutrients_prop
            or len([val for val in incoming_nutrients_prop.quantities.values() if val is not None and val.value > 0])
            == 0
        ):
            logger.debug(f"No nutrients available in {self.node}")
            return

        all_level_tuples = {level_tuple for (level_tuple, _, _, _, _) in flat_nutrients.keys()}
        keys_to_delete = []
        for key, ingredient_amount in flat_nutrients.items():
            (level_tuple, _, _uid, _, _) = key
            if (level_tuple + (0,)) in all_level_tuples:
                if ingredient_amount is not None:
                    keys_to_delete.append(key)
            else:
                assert ingredient_amount is not None, "ingredient_amount should not be None for leaf ingredients"

        for key in keys_to_delete:
            logger.debug(f"Deleting intermediate ingredient nutrient values for {key}.")
            flat_nutrients[key] = None

        # run estimation algorithm
        solution, error_squares, flat_nutrients_converted, solution_status = run(
            incoming_nutrients_prop, flat_nutrients, fixed_percentages
        )  # incoming_nutrients must be duplicated since otherwise, the property is modified directly.

        solution_uids = [uid for (_, _, uid, _, _), ingredient_amount in solution.items()]
        if len(set(solution_uids)) != len(solution_uids):
            raise ValueError("a node uid appears multiple times in the recipe. This should not happen")

        # write the estimated amount percentages to the ingredients
        estimated_nutrients = {}

        root_amount_in_kg = self.node.production_amount.value
        # The production_amount of recipe should be uniformized to kilogram by unit_weight_conversion_gfm.
        assert self.node.production_amount.get_unit_term().xid == "EOS_kilogram"

        weight_stack = [1.0]
        for ingredient_key, ingredient_amount in solution.items():
            # The ingredient_amount is in percentage of the product that is estimated.
            # We convert it here to kg to produce one unit of the root product:
            ingredient_kg_amount_per_root_unit = ingredient_amount * root_amount_in_kg / 100

            ingredient_nutrients = flat_nutrients_converted[ingredient_key]
            if ingredient_nutrients is not None:
                for nutrient, val in ingredient_nutrients.quantities.items():
                    if nutrient not in estimated_nutrients:
                        estimated_nutrients[nutrient] = ReferencelessQuantityProp(
                            value=0.0, unit_term_uid=val.unit_term_uid
                        )
                    estimated_nutrients[nutrient].value += val.value * ingredient_amount / 100

            (level_tuple, _, uid, _, _) = ingredient_key

            # weight_stack should always have 1 element more than len(level_tuple)
            if len(level_tuple) >= len(weight_stack):
                # we are going deeper into the ingredient tree
                weight_stack.append(ingredient_kg_amount_per_root_unit)
            elif len(level_tuple) < len(weight_stack) - 1:
                # we are going up in the ingredient tree
                weight_stack = weight_stack[: len(level_tuple)]
                weight_stack.append(ingredient_kg_amount_per_root_unit)
            else:
                # we are going to the next sibling
                weight_stack[-1] = ingredient_kg_amount_per_root_unit

            # normalize with the parent weight, such that this amount describes amount per 1kg of the parent node:
            ingredient_kg_amount_per_parent_unit = ingredient_kg_amount_per_root_unit / weight_stack[-2]

            await calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=uid,
                    prop_name="amount",
                    prop=QuantityProp(
                        value=ingredient_kg_amount_per_parent_unit,
                        unit_term_uid=self.gfm_factory.kilogram_term.uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),  # in kg
                )
            )

        await calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_solution_status",
                prop=IngredientAmountEstimatorStatusProp(value=solution_status),
            )
        )

        await calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_error_squares",
                prop=QuantityPackageProp(quantities=error_squares, for_reference=ReferenceAmountEnum.amount_for_100g),
            )
        )

        await calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_estimated_nutrients",
                prop=QuantityPackageProp(
                    quantities=estimated_nutrients, for_reference=ReferenceAmountEnum.amount_for_100g
                ),
            )
        )


class IngredientAmountEstimatorGapFillingFactory(AbstractGapFillingFactory):  # noqa: D101
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", self.root_unit_term.access_group_uid)
        ]
        self.kilogram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", self.root_unit_term.access_group_uid)
        ]
        self.percentage_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_percentage", self.root_unit_term.access_group_uid)
        ]

    async def init_cache(self) -> None:  # noqa: D102
        pass

    def spawn_worker(self, node: Node) -> IngredientAmountEstimatorGapFillingWorker:  # noqa: D102
        return IngredientAmountEstimatorGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = IngredientAmountEstimatorGapFillingFactory
