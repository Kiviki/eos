from pydantic_settings import BaseSettings, SettingsConfigDict

from core.envprops import EnvProps


class TransportModeDistanceGfmSettings(EnvProps):
    ECOTRANSIT_CALCULATION_METHOD: str = "ccwg-tradelane"
    ECOTRANSIT_CALCULATION_YEAR: int = 2013

    ECOTRANSIT_CUSTOMER: str = "ECOTRANSIT_CUSTOMER"
    ECOTRANSIT_PASSWORD: str = "ECOTRANSIT_PASSWORD"
    ECOTRANSIT_WSDL_URL: str = "ECOTRANSIT_WSDL_URL"

    EATERNITY_NAMESPACE_UUID: str = "EATERNITY_NAMESPACE_UUID"

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8", extra="allow")
