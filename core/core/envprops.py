from pydantic_settings import BaseSettings, SettingsConfigDict


class EnvProps(BaseSettings):
    LOG_LEVEL: str = "WARNING"
    RABBITMQ_DEFAULT_USER: str = ""
    RABBITMQ_DEFAULT_PASS: str = ""
    GCP_KEY: str = ""
    GDRIVE_GADM_FOLDER_ID: str = ""
    GDRIVE_FAO_FOLDER_ID: str = ""
    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    PRODUCTMATCHINGS_COLUMN_NAME: str = "ID Brightway (from Brightway Datasets)"
    MAX_CONCURRENT_ORCHESTRATIONS: int = 3
    # used for seeding initial namespace for inventory nodes:
    EATERNITY_USERNAME: str = "EATERNITY_USERNAME"
    EATERNITY_TOKEN: str = "EATERNITY_TOKEN"  # this is NOT base64 encoded
    EATERNITY_NAMESPACE_UUID: str = "EATERNITY_NAMESPACE_UUID"
    ROOT_GLOSSARY_TERM_UUID: str = "ROOT_GLOSSARY_TERM_UUID"

    ECOTRANSIT_CALCULATION_METHOD: str = ""
    ECOTRANSIT_CALCULATION_YEAR: str = ""
    ECOTRANSIT_CUSTOMER: str = ""
    ECOTRANSIT_PASSWORD: str = ""
    ECOTRANSIT_WSDL_URL: str = ""

    AIRTABLE_API_KEY: str = ""
    AIRTABLE_BASE_ID: str = ""
    AIRTABLE_TABLE_ID: str = ""
    AIRTABLE_VIEW_NAME: str = ""
    AIRTABLE_MATCHING_TABLE_ID: str = ""
    GITHUB_TOKEN: str = ""

    POSTGRES_PASSWORD: str = ""
    POSTGRES_USER: str = "eosuser"
    POSTGRES_DB: str = "eos"
    POSTGRES_HOST: str = "localhost"
    POSTGRES_PORT: str = "5444"
    POSTGRES_SCHEMA: str = "public"
    POSTGRES_CONNECTIONS: int = 5

    PYDANTIC_VALIDATE_ASSIGNMENT: bool = False
    VERIFY_NODE_READ_ONLY: bool = True

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8", extra="allow")
