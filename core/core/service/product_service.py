"""Product service for caching node key to uid mappings."""
import asyncio
import uuid
from typing import TYPE_CHECKING, Optional

from structlog import get_logger

from database.postgres.pg_product_mgr import PgProductMgr
from database.postgres.settings import PgSettings

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


logger = get_logger()


class ProductService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self._lock = asyncio.Lock()
        self.uid_to_xid_mappings: dict[uuid.UUID, str] = {}
        self.xid_to_uid_mappings: dict[str, uuid.UUID] = {}
        self.product_mgr: Optional[PgProductMgr] = None

    async def init(self) -> None:
        self.product_mgr = self.service_provider.postgres_db.product_mgr
        if self.service_provider.messaging_service is not None:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "product", self._on_cache_invalidation_msg
            )
        await self.load_all_mappings()

    async def load_all_mappings(self) -> None:
        # TODO: Maybe extend cached uid_to_xid_mappings to namespaces other than Eaternity namespace in the future.
        self.uid_to_xid_mappings = await self.product_mgr.get_all_uid_to_xid_mappings_of_namespace(
            PgSettings().EATERNITY_NAMESPACE_UUID
        )
        self.xid_to_uid_mappings = await self.product_mgr.get_all_id_mappings_of_namespace(
            PgSettings().EATERNITY_NAMESPACE_UUID
        )

    async def _on_cache_invalidation_msg(self, _: bytes) -> None:
        async with self._lock:
            # reload node from database and replace the one in our cache:
            await self.load_all_mappings()

        logger.debug("uid_to_xid_mappings was updated from db")

    def clear_cache(self) -> None:
        self.uid_to_xid_mappings.clear()
        self.xid_to_uid_mappings.clear()

    async def bulk_insert_xid_uid_mappings(
        self, namespace_uid_xid_uids_to_insert: list[tuple[str, str, uuid.UUID]]
    ) -> None:
        await self.product_mgr.bulk_insert_xid_uid_mappings(namespace_uid_xid_uids_to_insert)
        await self.update_all_mappings()

    async def update_all_mappings(self) -> None:
        if self.service_provider.messaging_service is None:
            # no messaging service, so we invalidate the cache directly just within this instance:
            await self._on_cache_invalidation_msg(b"")
        else:
            logger.debug("emitting a cache invalidation for product_service")
            await self.service_provider.messaging_service.emit_invalidate("product", b"")
