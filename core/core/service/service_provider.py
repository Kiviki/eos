from typing import Optional, Type

from structlog import get_logger

from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.access_group_service import AccessGroupService
from core.service.calc_service import CalcService
from core.service.gfm_cache_service import GfmCacheService
from core.service.glossary_link_service import GlossaryLinkService
from core.service.glossary_service import GlossaryService
from core.service.matching_service import MatchingService
from core.service.messaging_service import MessagingService
from core.service.modeled_activity_node_service import ModeledActivityNodeService
from core.service.namespace_service import NamespaceService
from core.service.node_service import NodeService
from core.service.product_service import ProductService
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class ServiceProvider:
    """Groups all services for GFMs in one place."""

    def __init__(self):
        self.postgres_db: Optional[PostgresDb] = None
        self.gfm_cache_service: Optional[GfmCacheService] = None
        self.glossary_service: Optional[GlossaryService] = None
        self.matching_service: Optional[MatchingService] = None
        self.messaging_service: Optional[MessagingService] = None
        self.namespace_service: Optional[NamespaceService] = None
        self.access_group_service: Optional[AccessGroupService] = None
        self.node_service: Optional[NodeService] = None
        self.gap_filling_module_loader: Optional[GapFillingModuleLoader] = None
        self.glossary_link_service: Optional[GlossaryLinkService] = None
        self.modeled_activity_node_service: Optional[ModeledActivityNodeService] = None
        self.product_service: Optional[ProductService] = None
        self.calc_service: Optional[CalcService] = None

    async def init_caches(self) -> None:
        """Initializes all but messaging services and their caches."""
        await self.glossary_service.init()
        await self.gfm_cache_service.init()
        await self.matching_service.init()
        await self.access_group_service.init()
        await self.namespace_service.init()
        await self.node_service.init()
        await self.glossary_link_service.init()
        await self.product_service.init()

    async def shutdown_services(self) -> None:
        """Shuts down all services and clears their caches."""
        if self.messaging_service:
            await self.messaging_service.stop()

        self.clear_cache()

        if self.postgres_db.pool:
            await self.postgres_db.disconnect()

    def clear_cache(self) -> None:
        """Clears caches of all services."""
        self.glossary_service.clear_cache()
        self.matching_service.clear_cache()
        self.gfm_cache_service.clear_cache()
        self.glossary_link_service.clear_cache()
        self.node_service.clear_cache()
        self.product_service.clear_cache()


def create_service_provider() -> ServiceProvider:
    service_provider = ServiceProvider()
    service_provider.postgres_db = PostgresDb(service_provider)
    service_provider.gfm_cache_service = GfmCacheService(service_provider)
    service_provider.glossary_service = GlossaryService(service_provider)
    service_provider.matching_service = MatchingService(service_provider)
    service_provider.namespace_service = NamespaceService(service_provider)
    service_provider.messaging_service = MessagingService(service_provider)
    service_provider.access_group_service = AccessGroupService(service_provider)
    service_provider.node_service = NodeService(service_provider)
    service_provider.glossary_link_service = GlossaryLinkService(service_provider)
    service_provider.gap_filling_module_loader = GapFillingModuleLoader()
    service_provider.modeled_activity_node_service = ModeledActivityNodeService()
    service_provider.product_service = ProductService(service_provider)
    service_provider.calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        service_provider.gap_filling_module_loader,
    )

    return service_provider


class ServiceLocator(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ServiceLocator, cls).__new__(cls)
            cls._instance.service_provider = create_service_provider()

        return cls._instance

    @classmethod
    def recreate(cls: Type["ServiceLocator"]) -> None:
        cls._instance.service_provider = create_service_provider()

    def set_service_provider(self, service_provider: ServiceProvider) -> None:
        self._instance.service_provider = service_provider
