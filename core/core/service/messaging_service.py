import asyncio
import logging
import os
import uuid
from typing import Any, Awaitable, Callable, Coroutine, Dict
from uuid import UUID

import aio_pika
from aio_pika import ExchangeType, RobustQueue
from aio_pika.abc import AbstractIncomingMessage, ConsumerTag
from aio_pika.patterns import RPC, Master, NackMessage, RejectMessage
from pydantic_settings import BaseSettings, SettingsConfigDict
from structlog import get_logger

from core.service.calc_service import OrchestratorAtLimitException

logger = get_logger()

from core.envprops import EnvProps


class MessagingSettings(EnvProps):
    RABBITMQ_HOSTNAME: str = "localhost"
    RABBITMQ_PORT: int = 5672
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class MessagingService:
    def __init__(self, service_provider):
        self.service_provider = service_provider
        self.connection = None
        self.channel = None
        self.cache_inv_exchange = None
        self.orchestration_worker = None
        self.queue_by_obj_types = dict()
        self.background_tasks = set()

    async def start(self):
        s = MessagingSettings()

        logging.info("starting connection to message broker...")
        self.connection = await aio_pika.connect_robust(
            f"amqp://{s.RABBITMQ_DEFAULT_USER}:{s.RABBITMQ_DEFAULT_PASS}@{s.RABBITMQ_HOSTNAME}:{s.RABBITMQ_PORT}/",
        )

        self.channel = await self.connection.channel()
        await self.channel.set_qos(prefetch_count=1)

        self.cache_inv_exchange = await self.channel.declare_exchange(
            "cache_invalidation_topic",
            ExchangeType.TOPIC,
        )

        self.orchestration_worker = await RPC.create(self.channel)

        async def calculation_worker(*, calculation_uid_int: int) -> None:
            calculation_uid = UUID(int=calculation_uid_int)
            print(f"Processing calculation {calculation_uid}")

            from core.service.service_provider import ServiceLocator

            service_locator = ServiceLocator()
            service_provider = service_locator.service_provider

            calculation = await service_provider.postgres_db.pg_calc_mgr.find_by_uid(calculation_uid)
            if not calculation:
                logger.error("calculation_worker cannot find calculation uid", calculation_uid=calculation_uid)
                raise RejectMessage(requeue=False)

            try:
                await service_provider.calc_service.calculate(calculation, raise_if_full=True)
            except OrchestratorAtLimitException:
                logger.error("calculation_worker cannot start calculation", calculation_uid=calculation_uid)
                raise NackMessage(requeue=True)

            logger.info("calculation_worker finished", calculation_uid=calculation_uid)

        await self.orchestration_worker.register("orchestration_worker", calculation_worker, auto_delete=True)

    async def enqueue_calculation(self, calculation_uid: UUID):
        logger.info("enqueuing calculation", calculation_uid=calculation_uid)

        create_task_result = await self.orchestration_worker.call(
            "orchestration_worker", kwargs=dict(calculation_uid_int=calculation_uid.int)
        )

        logger.info("calculation done", calculation_uid=calculation_uid, create_task_result=create_task_result)

        return create_task_result

    async def subscribe_cache_invalidation(self, obj_type: str, on_msg: Callable[[bytes], Coroutine]):
        logger.debug("subscribing cache invalidation messages", obj_type=obj_type)

        # generate a unique queue name to make sure that every subscriber is in its own queue (for broadcasting):
        queue_name = str(uuid.uuid4())
        queue = await self.channel.declare_queue(name=queue_name, exclusive=True)

        await queue.bind(self.cache_inv_exchange, routing_key=obj_type)

        async def on_message(message: AbstractIncomingMessage) -> None:
            async with message.process():
                pid = str(os.getpid())
                routing_key = message.routing_key
                logger.debug("received cache invalidation", routing_key=routing_key, pid=pid)

                task = asyncio.create_task(on_msg(message.body))

                # Need to keep task ref to prevent Heisenbug (https://news.ycombinator.com/item?id=34754276):
                self.background_tasks.add(task)

                # To prevent keeping references to finished tasks forever,
                # make each task remove its own reference from the set after completion:
                task.add_done_callback(self.background_tasks.discard)

        consumer_tag = await queue.consume(on_message)
        self.queue_by_obj_types[obj_type]: Dict[str, (RobustQueue, ConsumerTag)] = (queue, consumer_tag)

        logger.debug("subscribed cache invalidation messages", obj_type=obj_type)

    async def stop(self):
        for obj_type, (queue, consumer_tag) in self.queue_by_obj_types.items():
            logger.debug("destroy queue", obj_type=obj_type)
            await queue.unbind(self.cache_inv_exchange, routing_key=obj_type)
            await queue.cancel(consumer_tag)

        await self.orchestration_worker.close()

        if self.background_tasks:
            done, _ = await asyncio.wait(self.background_tasks, return_when=asyncio.ALL_COMPLETED)

        logger.info("closing rabbitmq connection")
        await self.connection.close()

    async def emit_invalidate(self, obj_type: str, obj_key: bytes):
        pid = str(os.getpid())
        logger.debug("emitting a cache invalidation", pid=pid)

        await self.cache_inv_exchange.publish(
            aio_pika.Message(body=obj_key),
            routing_key=obj_type,
        )
