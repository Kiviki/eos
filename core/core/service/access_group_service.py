from structlog import get_logger

from api.dto.v2.access_group_dto import AccessGroupId
from core.domain.access_group_node import AccessGroup, AccessGroupEdge
from database.postgres.pg_access_mgr import PostgresAccessMgr

logger = get_logger()


class AccessGroupService:
    def __init__(self, service_provider):
        self.service_provider = service_provider
        self.access_mgr = None

    async def init(self):
        self.access_mgr = self.service_provider.postgres_db.access_group_mgr

    async def create_access_group(self, **kwargs) -> (AccessGroup, bool):
        new_access_group_node = AccessGroup(**kwargs)

        return await self.access_mgr.upsert_access_group_by_uid(node=new_access_group_node)

    async def get_all_access_group_ids_by_namespace(self, namespace_uid: str) -> list[AccessGroupId]:
        return await self.access_mgr.get_all_access_group_ids_by_namespace(namespace_uid)

    async def get_all_sub_access_group_ids_by_uid(self, access_group_uid: str, depth: int) -> list[AccessGroupId]:
        return await self.access_mgr.get_all_sub_access_group_ids_by_uid(access_group_uid, depth)

    # TODO: unused -- deprecate?
    async def get_all_created_access_groups_by_user(self, user_id: str) -> list[AccessGroupId]:
        return await self.access_mgr.get_all_created_access_groups_by_user(user_id)

    async def get_all_group_memberships_by_user(self, user_id: str) -> list[AccessGroupId]:
        return await self.access_mgr.get_all_group_memberships_by_user(user_id)

    async def get_all_sub_group_memberships_by_user_and_uid(
        self,
        access_group_uid: str,
        depth: int,
        user_id: str,
    ) -> list[AccessGroupId]:
        return await self.access_mgr.get_all_sub_group_memberships_by_user_and_uid(access_group_uid, depth, user_id)

    async def verify_access_group_membership_by_user_and_group_uid(self, group_uid: str, user_id: str) -> bool:
        return await self.access_mgr.verify_access_group_membership_by_user_and_group_uid(group_uid, user_id)

    async def get_access_group_by_xid(self, access_group_xid: str, namespace_uid: str) -> AccessGroup:
        return await self.access_mgr.get_by_xid(access_group_xid, namespace_uid)

    async def get_access_group_by_uid(self, access_group_uid: str) -> AccessGroup:
        return await self.access_mgr.get_by_uid(access_group_uid)

    async def upsert_access_group_by_xid_or_uid(self, **kwargs) -> (AccessGroup, bool):
        return await self.access_mgr.upsert_access_group_by_uid(AccessGroup(**kwargs))

    async def delete_access_group_by_uid(self, group_uid: str) -> None:
        return await self.access_mgr.delete_access_group_by_uid(group_uid)

    async def get_access_group_edge(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
    ) -> AccessGroupEdge | None:
        return await self.access_mgr.get_access_group_edge(
            child_access_group_uid=child_access_group_uid,
            parent_access_group_uid=parent_access_group_uid,
        )

    async def upsert_access_group_edge(self, **kwargs) -> None:
        new_edge = AccessGroupEdge(**kwargs)
        logger.debug(f"starting adding new edge {new_edge}")

        return await self.access_mgr.upsert_access_group_edge(edge=new_edge)

    async def delete_access_group_edge(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
    ) -> bool:
        return await self.access_mgr.delete_access_group_edge(
            child_access_group_uid=child_access_group_uid,
            parent_access_group_uid=parent_access_group_uid,
        )

    # TODO: not sure if we need it.. deprecate?
    async def get_node_types_from_access_group_by_group_uid(self, access_group_node_uid: str) -> list[dict[str, list]]:
        return await self.access_mgr.get_node_types_from_access_group_by_group_uid(access_group_node_uid)
