"Node service."
import asyncio
import uuid
from dataclasses import dataclass
from typing import TYPE_CHECKING, Any, Optional

import asyncpg
from structlog import get_logger

from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.prop import Prop
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.domain.props.names_prop import RawName

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


@dataclass
class MatchingToXidOrUid:
    product_name: list[RawName]
    link_to_sub_node: LinkToUidProp | LinkToXidProp


logger = get_logger()

CACHED_NODE_TYPES = [
    ModeledActivityNode.__name__,
    ElementaryResourceEmissionNode.__name__,
    FlowNode.__name__,
]


class NodeCache:
    """Caches LCA nodes and edges at startup time.

    These nodes are heavily used in LCA calculations and never updated, so it makes sense to cache them.
    ATM this is just backed by a simple in-memory dict. Also, there's no way to reinit the cache
    TODO implement smarter / faster caching, if necessary
    """

    def __init__(self):
        self._node_cache: dict[uuid.UUID, Node] = dict()
        self._node_child_edge_cache: dict[uuid.UUID, list[Edge]] = dict()
        self._node_parent_edge_cache: dict[uuid.UUID, list[Edge]] = dict()

    def get_cached_node_by_uid(self, uid: uuid) -> Optional[Node]:
        """Get cached node by uid.

        Raises:
            KeyError if node is not found
        """
        # We need to copy the node here, otherwise the node is modified in the cache:
        return Node.from_node(self._node_cache[uid])

    def add_node(self, node: Node) -> None:
        self._node_cache[node.uid] = node

    def add_edge(self, edge: Edge) -> None:
        if edge.parent_uid not in self._node_child_edge_cache:
            # initialize the edge cache for this edge's parent node
            self._node_child_edge_cache[edge.parent_uid] = [edge]
        else:
            self._node_child_edge_cache[edge.parent_uid].append(edge)

        if edge.child_uid not in self._node_parent_edge_cache:
            # initialize the edge cache for this edge's child node
            self._node_parent_edge_cache[edge.child_uid] = [edge]
        else:
            self._node_parent_edge_cache[edge.child_uid].append(edge)

    async def get_parents_by_uid(self, node_uid: uuid.UUID) -> list[Edge]:
        if node_uid not in self._node_parent_edge_cache:
            raise KeyError("edges of that node are not cached")
        else:
            return self._node_parent_edge_cache[node_uid]

    async def get_sub_graph_by_uid(self, node_uid: uuid.UUID, max_depth: int) -> list[Edge]:
        """Only max_depth 1 is supported ATM."""
        if max_depth > 1:
            raise ValueError("only max_depth=1 is supported atm")
        elif node_uid not in self._node_child_edge_cache:
            raise KeyError("edges of that node are not cached")
        else:
            return self._node_child_edge_cache[node_uid]

    def invalidate_cached_node_by_uid(self, uid: uuid) -> None:
        "Make sure that the node is removed from cache if it was in there."
        if uid in self._node_cache:
            del self._node_cache[uid]

        if uid in self._node_child_edge_cache:
            edges_to_children = self._node_child_edge_cache[uid]
            del self._node_child_edge_cache[uid]
            for edge in edges_to_children:
                if edge.child_uid in self._node_parent_edge_cache:
                    self._node_parent_edge_cache[edge.child_uid].remove(edge)

        if uid in self._node_parent_edge_cache:
            # edges_to_parents = self._node_parent_edge_cache[uid]
            del self._node_parent_edge_cache[uid]
            # for edge in edges_to_parents:
            #     self._node_child_edge_cache[edge.parent_uid].remove(edge)

    def clear_cache(self) -> None:
        self._node_cache.clear()
        self._node_child_edge_cache.clear()
        self._node_parent_edge_cache.clear()

    def __len__(self):
        return len(self._node_cache)


class NodeService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.graph_mgr = None
        self._node_cache_loaded: bool = False
        self._lock = asyncio.Lock()
        self.cache = NodeCache()

    async def init(self) -> None:
        self.graph_mgr = self.service_provider.postgres_db.graph_mgr

        if self.service_provider.messaging_service is not None:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "node", self._on_cache_invalidation_msg
            )

        await self.ensure_cache_is_loaded()

    async def _broadcast_cache_invalidation(self, node_uid: uuid.UUID) -> None:
        self.cache.invalidate_cached_node_by_uid(node_uid)
        if self.service_provider.messaging_service is None:
            # no messaging service, so we invalidate the cache directly just within this instance:
            await self._on_cache_invalidation_msg(node_uid.bytes)
        else:
            logger.debug("emitting a cache invalidation for node", node_uid=node_uid)
            await self.service_provider.messaging_service.emit_invalidate("node", node_uid.bytes)

    async def _on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        node_uid = uuid.UUID(bytes=msg_bytes)

        async with self._lock:
            # reload node from database and replace the one in our cache:
            node = await self.graph_mgr.find_by_uid(node_uid)
            if node is None:
                self.cache.invalidate_cached_node_by_uid(node_uid)
            else:
                lca_links = await self.graph_mgr.get_sub_graph_by_uid(node_uid, max_depth=1)
                self.cache.invalidate_cached_node_by_uid(node_uid)
                self.cache.add_node(node)
                for lca_link in lca_links:
                    self.cache.add_edge(lca_link)

        logger.debug("node was updated from db", node_uid=node_uid)

    def clear_cache(self) -> None:
        self.cache.clear_cache()
        self._node_cache_loaded = False

    async def get_recipe(
        self,
        uid: uuid.UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
        lock_for_update: bool = False,
        ignore_deleted: bool = False,
    ) -> Optional[Node]:
        """Get the root-flow or the root-activity defined by the uid.

        This method also add parent and child nodes if corresponding edges exist in the database.
        """
        recipe_node = await self.graph_mgr.find_by_uid(
            uid, conn, lock_for_update=lock_for_update, ignore_deleted=ignore_deleted
        )

        if not recipe_node:
            return None

        parent_edges = await self.get_parents_by_uid(uid, conn)
        parent_edges = [edge for edge in parent_edges if edge.child_uid == uid]  # TODO: is still this needed?
        if isinstance(recipe_node, ActivityNode) and len(parent_edges) > 1:
            raise ValueError("Root activity has more than one parent, it should maximally have one parent root-flow.")
        if isinstance(recipe_node, FlowNode) and len(parent_edges) > 0:
            raise ValueError("Root flow has more than zero parents, it should have no parents.")
        for edge in parent_edges:
            parent_node = await self.graph_mgr.find_by_uid(edge.parent_uid, conn, ignore_deleted=ignore_deleted)
            recipe_node.add_parent_node(parent_node)
            parent_node.add_sub_node(recipe_node)

        child_edges = await self.get_sub_graph_by_uid(uid, max_depth=1, conn=conn)
        child_edges = [edge for edge in child_edges if edge.parent_uid == uid]  # TODO: is still this needed?
        if isinstance(recipe_node, FlowNode) and len(child_edges) > 1:
            raise ValueError("Root flow has more than one child, it should maximally have one root-activity as child.")
        for edge in child_edges:
            child_node = await self.graph_mgr.find_by_uid(edge.child_uid, conn, ignore_deleted=ignore_deleted)
            recipe_node.add_sub_node(child_node)
            child_node.add_parent_node(recipe_node)

        return recipe_node

    async def ensure_cache_is_loaded(self) -> None:
        if self._node_cache_loaded:
            # have this first check here, so we don't even need to acquire the lock if the cache is already loaded
            logger.debug("Node and edge caches are already initialized. Do nothing.")
            return

        async with self._lock:
            if self._node_cache_loaded:
                # we need this a second time here, in case another coroutine was waiting for the lock while a first one
                # was loading the cache
                logger.debug("Node and edge caches were already initialized in the meantime. Do nothing.")
                return

            import time

            start = time.time()

            logger.info("Starting to fill the node cache...")
            lca_nodes = await self.graph_mgr.get_nodes_by_type(CACHED_NODE_TYPES)
            for lca_node in lca_nodes:
                self.cache.add_node(lca_node)

            logger.info("Starting to fill the edge cache...")
            lca_links = await self.graph_mgr.get_lca_links()
            for lca_link in lca_links:
                self.cache.add_edge(lca_link)

            self._node_cache_loaded = True

            duration = time.time() - start
            logger.info(
                "Loaded node and edge cache", node_count=len(lca_nodes), edge_count=len(lca_links), duration=duration
            )

    async def get_sub_graph_by_uid(
        self, uid: uuid.UUID, max_depth: int = 1, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        if max_depth == 1:
            try:
                return await self.cache.get_sub_graph_by_uid(uid, max_depth)
            except (ValueError, KeyError):
                pass
                # cache was not used. We need to instead use connection to postgres:

        edges = await self.graph_mgr.get_sub_graph_by_uid(uid, max_depth=max_depth, conn=conn)
        return edges

    async def get_parents_by_uid(
        self, uid: uuid.UUID, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        try:
            return await self.cache.get_parents_by_uid(uid)
        except KeyError:
            pass
            # cache was not used. We need to instead use connection to postgres:

        edges = await self.graph_mgr.get_parents_by_uid(uid, conn)
        return edges

    async def upsert_node_by_uid(self, node: Node) -> Node:
        updated_node = await self.graph_mgr.upsert_node_by_uid(node)
        if updated_node.node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(updated_node.uid)
        return updated_node

    async def find_by_uid(self, uid: uuid.UUID) -> Optional[Node]:
        try:
            return self.cache.get_cached_node_by_uid(uid)
        except KeyError:
            node = await self.graph_mgr.find_by_uid(uid)
            return node

    async def add_edge(self, edge: Edge) -> Edge:
        new_edge: Edge = await self.graph_mgr.add_edge(edge=edge)
        self.cache.invalidate_cached_node_by_uid(edge.parent_uid)
        if edge.edge_type == EdgeTypeEnum.lca_link:
            await self._broadcast_cache_invalidation(new_edge.parent_uid)
        return new_edge

    async def update_node_prop(self, node_uid: uuid.UUID, prop_name: str, prop: Prop, append: bool = False) -> None:
        node_type = await self.graph_mgr.update_node_prop(node_uid, prop_name, prop, append)
        if node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(node_uid)

    async def update_node_access_group_uid(self, node_uid: uuid.UUID, access_group_uid: uuid.UUID) -> None:
        node_type = await self.graph_mgr.update_node_access_group_uid(node_uid, access_group_uid)
        if node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(node_uid)

    async def delete_node_and_edges(self, node_uid: uuid.UUID) -> bool:
        was_deleted = await self.graph_mgr.delete_node_and_edges(str(node_uid))
        if was_deleted:
            # we need to invalidate the cache:
            self.cache.invalidate_cached_node_by_uid(node_uid)
        return was_deleted

    async def delete_many_nodes_and_edges(self, node_uids: list[uuid.UUID]) -> None:
        await self.graph_mgr.delete_many_nodes_and_edges([str(node_uid) for node_uid in node_uids])
        for node_uid in node_uids:
            await self._broadcast_cache_invalidation(node_uid)

    async def update_many_nodes_and_edges_from_local_cache(self, node_uids: list[uuid.UUID]) -> None:
        nodes_to_add = []
        edges_to_add = []
        for node_uid in node_uids:
            edges_to_add.extend(await self.get_sub_graph_by_uid(node_uid, max_depth=1))
            nodes_to_add.append(self.cache.get_cached_node_by_uid(node_uid))

        # Because of db table edge's REFERENCES, we always insert nodes first, then edges.
        await self.graph_mgr.add_nodes(nodes_to_add)
        await self.graph_mgr.add_edges(edges_to_add)
        # FIXME: this is missing logic to actually delete the old edges from the database!

        for node in nodes_to_add:
            if node.node_type in CACHED_NODE_TYPES:
                await self._broadcast_cache_invalidation(node.uid)

    async def create_recipe(self, **kwargs: Any) -> Node:  # noqa: ANN401
        """Directly insert a recipe and its sub-recipe & ingredients."""
        if "uid" not in kwargs:
            kwargs["uid"] = str(uuid.uuid4())

        new_recipe = Node(**kwargs)

        return await self.graph_mgr.upsert_node_by_uid(new_recipe)

    async def find_access_group_uid_by_uid(self, uid: uuid.UUID) -> uuid.UUID:
        try:
            cached_node = self.cache.get_cached_node_by_uid(uid)
            access_group_uid = cached_node.access_group_uid
        except KeyError:
            access_group_uid = None

        if not access_group_uid:
            access_group_uid = await self.graph_mgr.find_access_group_uid_by_uid(uid)
            if access_group_uid is not None:
                access_group_uid = uuid.UUID(access_group_uid)

        return access_group_uid

    async def upsert_by_recipe_id(self, recipe_node: Node, transient: bool = False) -> Node:
        async with self.graph_mgr.get_pool().acquire() as conn:
            async with conn.transaction():
                # check if it already exists and if so we need to remove the old ingredients:
                # ignore deleted so that the nodes are cleanly removed even if deleted_at is set.
                existing_recipe_node = await self.get_recipe(
                    recipe_node.uid, conn, lock_for_update=True, ignore_deleted=True
                )
                existing_sub_node_dict: dict[int, MatchingToXidOrUid] = {}

                if existing_recipe_node:
                    for parent_node in existing_recipe_node.get_parent_nodes():
                        await self.graph_mgr.delete_node_and_edges(parent_node.uid, conn)
                    # delete all direct ingredient child nodes and recreate them further below
                    # (because id's of linked recipes could have changed in the new recipe_node):
                    for sub_node in existing_recipe_node.get_sub_nodes():
                        if sub_node.product_name and sub_node.link_to_sub_node and sub_node.declaration_index:
                            matching_to_xid_or_uid = MatchingToXidOrUid(
                                product_name=sub_node.product_name, link_to_sub_node=sub_node.link_to_sub_node
                            )
                            existing_sub_node_dict[sub_node.declaration_index.value] = matching_to_xid_or_uid

                        # this could be improved in one transaction or somehow reassociating existing ingredient nodes,
                        # instead of deleting the old ones...
                        await self.graph_mgr.delete_node_and_edges(sub_node.uid, conn)

                recipe_node = await self.graph_mgr.upsert_node_by_uid(recipe_node, conn, set_as_deleted=transient)

                if recipe_node.get_parent_nodes():
                    root_flow_node = recipe_node.get_parent_nodes()[0]
                    await self.graph_mgr.upsert_node_by_uid(root_flow_node, conn, set_as_deleted=transient)
                    await self.graph_mgr.add_edge(
                        conn=conn,
                        edge=Edge(
                            parent_uid=root_flow_node.uid,
                            child_uid=recipe_node.uid,
                            edge_type=EdgeTypeEnum.root_to_recipe,
                        ),
                    )

                # now add new updated ingredient nodes:
                for index, sub_node in enumerate(recipe_node.get_sub_nodes()):
                    # Attempt to retrieve link_to_sub_node from persisted subnodes in the database if
                    # no new link_to_sub_node is provided.
                    if index in existing_sub_node_dict and sub_node.product_name and not sub_node.link_to_sub_node:
                        # TODO: Decide what other fields to compare here.
                        if existing_sub_node_dict[index].product_name == sub_node.product_name:
                            sub_node.link_to_sub_node = existing_sub_node_dict[index].link_to_sub_node

                    # insert sub-node into db:
                    created_sub_node = await self.graph_mgr.upsert_node_by_uid(sub_node, conn, set_as_deleted=transient)

                    edge = Edge(
                        parent_uid=recipe_node.uid, child_uid=created_sub_node.uid, edge_type=EdgeTypeEnum.ingredient
                    )

                    await self.graph_mgr.add_edge(conn=conn, edge=edge)

                    ###################################################################################################
                    # The linking to sub-recipes now happens in add_client_nodes_gfm dynamically during a calculation.
                    # That allows us to run batch requests where there might be sub-recipes that are not yet created.
                    # Leaving the old code here, just in case we need to revert back to this approach at some point:
                    #
                    # if isinstance(sub_node, LegacyRecipeIngredientFlowNode):
                    #
                    #     if created_sub_node.raw_input.linked_recipe_uid is None:
                    #         # in batch requests it can happen that the linked recipe does not exist yet, when saving
                    #         # the node during the first pass through the batch. So then we need to skip it here and
                    #         # instead they will be linked during calculation in gfm.
                    #         continue
                    #
                    #     # find sub-recipe in db:
                    #     sub_recipe_node = await self.graph_mgr.find_by_uid(sub_node.uid, conn)
                    #
                    #     edge = Edge(parent_uid=sub_recipe_node.uid,
                    #                 child_uid=created_sub_node.raw_input.linked_recipe_uid,
                    #                 edge_type=EdgeTypeEnum.link_to_subrecipe)
                    #
                    #     await self.graph_mgr.add_edge(conn=conn, edge=edge)
                    ###################################################################################################

        return recipe_node
