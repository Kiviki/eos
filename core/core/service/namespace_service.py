from typing import TYPE_CHECKING

from structlog import get_logger

from api.dto.v2.customer_dto import CreateCustomerDto
from core.domain.namespace import Namespace
from database.postgres.pg_namespace_mgr import PostgresNamespaceMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


logger = get_logger()


class NamespaceService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.namespace_mgr: PostgresNamespaceMgr = None  # noqa

    async def init(self):
        self.namespace_mgr = self.service_provider.postgres_db.get_namespace_mgr()

    async def upsert_namespace(self, **kwargs) -> tuple[CreateCustomerDto, str]:
        new_namespace = Namespace(**kwargs)

        namespace, token, _ = await self.namespace_mgr.insert_or_update_namespace_wrapper(new_namespace)

        return CreateCustomerDto(name=new_namespace.name, uid=namespace.uid), token

    # TODO: unused for now
    async def find_all_namespaces(self) -> list[Namespace]:
        return await self.namespace_mgr.find_all()

    # TODO: used only by glossary router unit tests for now
    async def find_namespace_by_uid(self, namespace_uid: str) -> Namespace:
        return await self.namespace_mgr.find_by_namespace_uid(namespace_uid)

    async def find_namespace_by_name(self, namespace_name: str) -> Namespace:
        return await self.namespace_mgr.find_by_namespace_name(namespace_name)
