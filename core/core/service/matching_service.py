import json
import time
import unicodedata
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from structlog import get_logger

from core.domain.matching_item import MatchingItem
from core.domain.term import Term
from core.service.glossary_service import GlossaryService
from database.postgres.pg_matching_mgr import PgMatchingMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()


# we normalize the matching string to NFD to make sure that we don't have any unicode ambiguity issues
UNICODE_NORMALIZATION_FORM = "NFD"


class MatchingService:
    """
    A service that exposes matchings to the rest of the system. Keeps a cache of matchings in memory.
    """

    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.pg_matching_mgr: PgMatchingMgr = None  # noqa
        self.glossary_service: GlossaryService = None  # noqa

        # Cache; allows multiple terms to be associated with a matching str.
        # key: matching str (lowercased!); value: set of (term_uid: str, gap_filling_module_name: str, lang: str)
        self._terms_by_matching_str: dict[str, set[(tuple[str], str, str)]] = dict()

    async def init(self):
        if self._terms_by_matching_str:
            raise RuntimeError("Matching cache is already initialized! Should not call init again!")

        self.pg_matching_mgr = self.service_provider.postgres_db.get_pg_matching_mgr()
        self.glossary_service = self.service_provider.glossary_service

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "matching", self._on_cache_invalidation_msg
            )

        logger.info("start initializing matching cache...")  # we load all in memory
        start_time = time.time()
        all_items: list[MatchingItem] = await self.pg_matching_mgr.get_matching_items()
        for item in all_items:
            self._update_matching_item_in_cache(item)
        logger.info(
            f"done initializing matching cache with {len(self._terms_by_matching_str)} items, "
            f"took {time.time() - start_time:.2f}s"
        )

    def clear_cache(self):
        self._terms_by_matching_str.clear()

    def _update_matching_item_in_cache(self, matching_item: MatchingItem) -> None:
        """If term_uids are missing in the matching_item, the cached term will be deleted."""

        def delete_preexisting_matching(match_item: MatchingItem, match_set: set[tuple]):
            for m in match_set:
                _, gfm_name, lang = m
                if gfm_name == match_item.gap_filling_module and lang == match_item.lang:
                    matching_set.remove(m)
                    break

        matching_string: str = unicodedata.normalize(
            UNICODE_NORMALIZATION_FORM, matching_item.matching_string.casefold()
        )

        # If term_uids are present, we are adding the term into cache
        if matching_item.term_uids:
            assert type(matching_item.term_uids) is list
            assert type(matching_item.term_uids[0]) is str

            if matching_string not in self._terms_by_matching_str:
                self._terms_by_matching_str[matching_string] = set()
            else:
                # need to check for existence in set to prevent duplicate primary keys:
                matching_set = self._terms_by_matching_str[matching_string]
                delete_preexisting_matching(matching_item, matching_set)

            # insert new terms
            self._terms_by_matching_str[matching_string].add(
                (tuple(matching_item.term_uids), matching_item.gap_filling_module, matching_item.lang)
            )
        else:
            if matching_string in self._terms_by_matching_str:
                matching_set = self._terms_by_matching_str[matching_string]
                delete_preexisting_matching(matching_item, matching_set)
                if len(matching_set) == 0:
                    del self._terms_by_matching_str[matching_string]

    def _remove_matching_item_from_cache(self, matching_item: MatchingItem) -> None:
        matching_string_norm = unicodedata.normalize(
            UNICODE_NORMALIZATION_FORM, matching_item.matching_string.casefold()
        )
        matching_set = self._terms_by_matching_str[matching_string_norm]
        matching_set.remove((tuple(matching_item.term_uids), matching_item.gap_filling_module, matching_item.lang))

    async def _on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        my_dict = json.loads(msg_bytes)

        matching_item = MatchingItem(**my_dict)

        # update cache:
        self._update_matching_item_in_cache(matching_item)
        logger.debug("matching_item was updated in cache", matching_string=matching_item.matching_string)

    async def get_terms_by_matching_string(
        self, matching_string: str, gap_filling_module_name: str, filter_lang: Optional[str] = None
    ) -> [Term]:
        """
        Returns a list of terms that match the given *lowercased* matching string.
        :param filter_lang: if set, then filter results to only return matching languages
        :param matching_string:
        :param gap_filling_module_name: filters the terms by the given gap filling module
        :return: a list of terms; empty list if no match
        """
        if not matching_string:
            return []

        matching_string_norm: str = unicodedata.normalize(UNICODE_NORMALIZATION_FORM, matching_string.casefold())

        if matching_string_norm not in self._terms_by_matching_str:
            return []
        else:
            matching_set = self._terms_by_matching_str.get(matching_string_norm)
            return [
                [self.glossary_service.get_term_by_id(UUID(term_uid)) for term_uid in term_uids]
                for term_uids, gfm_name, lang in matching_set
                if gfm_name == gap_filling_module_name and (filter_lang is None or lang == filter_lang)
            ]

    async def put_matching_item(self, matching_item: MatchingItem) -> MatchingItem:
        # first normalize the string before inserting to database:
        matching_item.matching_string = unicodedata.normalize(
            UNICODE_NORMALIZATION_FORM, matching_item.matching_string.casefold()
        )

        matching_item = await self.pg_matching_mgr.insert_matching(matching_item)

        # update cache
        self._update_matching_item_in_cache(matching_item)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug(
                "emitting a cache invalidation for matching string", matching_string=matching_item.matching_string
            )
            data = matching_item.as_dict()
            data_bytes = json.dumps(data).encode("utf-8")
            await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)

        return matching_item

    async def update_cache_for_many_matching_items(self, matching_items: list[MatchingItem]) -> None:
        for matching_item in matching_items:
            # update cache
            self._update_matching_item_in_cache(matching_item)

            # Optional messaging_service in case multiple worker caches need to be synchronized
            # (many unittests don't need it, so better to keep it optional)
            if self.service_provider.messaging_service is not None:
                logger.debug(
                    "emitting a cache invalidation for matching string", matching_string=matching_item.matching_string
                )
                data = matching_item.as_dict()
                data_bytes = json.dumps(data).encode("utf-8")
                await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)

    async def delete_matching_item(self, matching_item_to_delete: MatchingItem) -> None:
        await self.pg_matching_mgr.delete_matching(matching_item_to_delete.uid)
        assert matching_item_to_delete.term_uids is None

        # update cache
        self._update_matching_item_in_cache(matching_item_to_delete)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug(
                "emitting a cache invalidation for matching string",
                matching_string=matching_item_to_delete.matching_string,
            )
            data = matching_item_to_delete.as_dict()
            data_bytes = json.dumps(data).encode("utf-8")
            await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)
