"Calculation Service."
import asyncio
import typing
from typing import Tuple

from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes.activity.emission import ElementaryResourceEmissionNode
from core.domain.nodes.activity.food_processing_activity import FoodProcessingActivityNode
from core.domain.nodes.flow.food_product_flow import FoodProductFlowNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.nodes.root_with_subflows_dto import RootWithSubFlowsDto
from core.domain.util import get_system_process_data
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.sheet_converter import OutputGraphToTableConverter
from core.orchestrator.exceptions import GapFillingModuleError
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import Orchestrator
from core.service.glossary_service import GlossaryService
from core.service.node_service import NodeService

if typing.TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()


class CalcServiceSettings(EnvProps):
    "Calculation Service settings."
    MAX_CONCURRENT_ORCHESTRATIONS: int = 3
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class OrchestratorAtLimitException(Exception):  # noqa: D101
    pass


class CalcService:
    "Calculation Service."

    def __init__(
        self,
        service_provider: "ServiceProvider",
        glossary_service: GlossaryService,
        node_service: NodeService,
        gap_filling_module_loader: GapFillingModuleLoader,
    ):
        self.service_provider = service_provider
        self.glossary_service = glossary_service
        self.gap_filling_module_loader = gap_filling_module_loader
        self.node_service = node_service
        self.semaphore = asyncio.Semaphore(value=CalcServiceSettings().MAX_CONCURRENT_ORCHESTRATIONS)

    def create_calc_graph_and_orchestrator(self, calculation: Calculation) -> Tuple[CalcGraph, Orchestrator]:
        """Create calculation graph and GFM orchestrator.

        Returns:
            tuple: a tuple of calculation graph and the orchestrator.
        """
        # create the calculation graph:
        logger.debug("creating the calculation graph...")
        calc_graph = CalcGraph(
            self.service_provider,
            root_node_uid=calculation.root_node_uid,
            glossary_service=self.glossary_service,
            calculation=calculation,
        )

        # initialize orchestrator with the calculation graph:
        logger.debug("creating the orchestrator...")
        orchestrator = Orchestrator(
            calc_graph=calc_graph,
            glossary_service=self.glossary_service,
            gap_filling_module_loader=self.gap_filling_module_loader,
        )
        return calc_graph, orchestrator

    async def calculate(self, calculation: Calculation, raise_if_full: bool = False) -> Tuple[Calculation, Node]:
        """Run the calculation.

        Args:
            calculation: Calculation object
            raise_if_full: If True, it'll raise `OrchestratorAtLimitException` if the semaphore is already at its limit.

        Returns:
            tuple: a tuple of calculation and root node
        """
        # We have to try prepending root flow by accessing the node table because currently in the calculation database,
        # child_of_root_node_uid is persisted as the root_node_uid. See FIXME in pg_calculation_mgr.
        if calculation.root_node_uid is None:
            parent_edges = await self.service_provider.node_service.get_parents_by_uid(
                calculation.child_of_root_node_uid
            )
            if parent_edges:
                root_flow_node = await self.service_provider.node_service.find_by_uid(parent_edges[0].parent_uid)
                if root_flow_node:
                    calculation.root_node_uid = root_flow_node.uid

        if calculation.root_node_uid is None:
            child_of_root_node_uid = calculation.child_of_root_node_uid
            graph_mgr = self.service_provider.postgres_db.graph_mgr
            # FIXME: decide on which properties we want to copy over from the recipe. Examples could be
            # the access group or the location.
            child_of_root_node = await self.service_provider.node_service.find_by_uid(child_of_root_node_uid)
            if isinstance(child_of_root_node, FoodProcessingActivityNode):
                root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode())
            else:
                # By default we add a generic FlowNode.
                root_flow = await graph_mgr.upsert_node_by_uid(FlowNode())
            await graph_mgr.add_edge(
                Edge(
                    parent_uid=root_flow.uid, child_uid=child_of_root_node_uid, edge_type=EdgeTypeEnum.link_to_subrecipe
                )
            )
            calculation.root_node_uid = root_flow.uid

        calc_graph, orchestrator = self.create_calc_graph_and_orchestrator(calculation)

        if raise_if_full and self.semaphore.locked():
            raise OrchestratorAtLimitException("The orchestrator is already at its limit of maximum concurrent runs.")

        # start running the orchestrator, which will internally call gap filling modules:
        async with self.semaphore:  # limits the number of concurrent orchestrations
            try:
                await orchestrator.run()
            except GapFillingModuleError as e:
                calculation.data_errors.append("Calculation failure error: " + e.error_msg)
                calculation.success = False

            calculated_root_node = calc_graph.get_root_node()
            calculated_child_of_root_node = calc_graph.child_of_root_node()

            if calculation.return_log:
                calculation.mutations = calc_graph.get_mutation_log()

            if calculation.return_final_graph:
                calculation.final_graph = await calc_graph.get_serialized_graph(calculation.exclude_node_props)

            if calculation.return_final_root:
                if calculation.final_root is None:
                    calculation.final_root = RootWithSubFlowsDto(
                        flow=calculated_root_node,
                        activity=calculated_child_of_root_node,
                        sub_flows=list(calculated_child_of_root_node.get_sub_nodes()),
                    )
                else:
                    calculation.final_root.flow = calculated_root_node
                    calculation.final_root.activity = calculated_child_of_root_node
                    calculation.final_root.sub_flows = list(calculated_child_of_root_node.get_sub_nodes())

            if calculation.save_as_system_process and len(calculation.data_errors) == 0:
                await self.save_as_system_process(calc_graph)

            calculation.data_errors.extend(calc_graph.get_data_errors_log())

            if calculation.return_data_as_table:
                calculation.final_graph_table = OutputGraphToTableConverter().run(
                    calc_graph,
                    "\n\n".join(calculation.data_errors),
                )

            pg_calc_mgr = self.service_provider.postgres_db.get_calc_mgr()
            await pg_calc_mgr.upsert_by_uid(calculation)

        return calculation, calculated_root_node

    async def save_as_system_process(self, calc_graph: CalcGraph) -> None:
        """Save nodes as system processes.

        To separate the loaded cache from the calculated properties of a currently running calculation, we wrap the
        specific properties in a Prop object with the name "aggregated_cache". This way, we can easily distinguish them.
        :param calc_graph: CalcGraph of the calculation
        """
        for current_node_uid in calc_graph.nodes_to_calculate_supply:
            current_node = calc_graph.get_node_by_uid(current_node_uid)
            if isinstance(current_node, ElementaryResourceEmissionNode):
                # Emission nodes do not have environmental flows to cache.
                continue
            aggregated_cache = get_system_process_data(current_node, calc_graph)
            await self.node_service.update_node_prop(
                current_node.uid, "aggregated_cache", aggregated_cache, append=False
            )

    async def find_by_uid(self, uid: str) -> Calculation:
        "Find UID in postgres calculation manager."
        pg_calc_mgr = self.service_provider.postgres_db.get_calc_mgr()
        return await pg_calc_mgr.find_by_uid(uid)
