from structlog import get_logger

from core.domain.ingredients_declaration import IngredientsDeclarationMapping
from database.postgres.pg_ingredients_declaration_mgr import PostgresIngredientsDeclarationMappingMgr

logger = get_logger()


class IngredientsDeclarationMappingService:
    def __init__(self, ingredients_declaration_mapping_mgr: PostgresIngredientsDeclarationMappingMgr):
        self.ingredients_declaration_mapping_mgr = ingredients_declaration_mapping_mgr

    async def get_ingredients_declaration_mapping_by_uid(
        self,
        declaration_uid: str,
    ) -> IngredientsDeclarationMapping | None:
        return await self.ingredients_declaration_mapping_mgr.get_ingredients_declaration_mapping_by_uid(
            declaration_uid
        )

    async def get_ingredients_declaration_mapping_by_text(
        self,
        declaration: str,
    ) -> IngredientsDeclarationMapping | None:
        return await self.ingredients_declaration_mapping_mgr.get_ingredients_declaration_mapping_by_text(declaration)

    async def search_ingredients_declaration_mappings_by_text(
        self,
        declaration_part: str,
    ) -> list[IngredientsDeclarationMapping]:
        return await self.ingredients_declaration_mapping_mgr.search_ingredients_declaration_mappings_by_text(
            declaration_part
        )

    async def get_all_ingredient_declaration_mappings(self) -> list[IngredientsDeclarationMapping]:
        return await self.ingredients_declaration_mapping_mgr.get_all_ingredients_declaration_mappings()

    async def upsert_ingredients_declaration_mapping(self, **kwargs) -> IngredientsDeclarationMapping:
        declaration_to_upsert = IngredientsDeclarationMapping(**kwargs)

        return await self.ingredients_declaration_mapping_mgr.upsert_ingredients_declaration_mapping(
            declaration_to_upsert
        )

    async def delete_ingredients_declaration_mapping_by_uid(self, declaration_uid: str) -> bool:
        return await self.ingredients_declaration_mapping_mgr.delete_ingredients_declaration_mapping_by_uid(
            declaration_uid
        )

    async def delete_ingredients_declaration_mapping_by_text(self, declaration: str) -> bool:
        return await self.ingredients_declaration_mapping_mgr.delete_ingredients_declaration_mapping_by_text(
            declaration
        )
