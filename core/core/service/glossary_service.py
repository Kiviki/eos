import uuid
from typing import TYPE_CHECKING, List, Optional
from uuid import UUID

from structlog import get_logger

from core.domain.term import Term
from database.postgres.settings import PgSettings

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()
settings = PgSettings()

TermsByXidKey = (str, UUID)


class GlossaryService:
    """A service that exposes the glossary to the rest of the system. Keeps a cache of terms in memory."""

    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.pg_term_mgr = None

        # terms' cache. Key: term uid; value: term
        self.terms_by_uid: dict[UUID, Term] = dict()
        # additional dict with tuple key: (term_xid, access_group_uid)
        self.terms_by_xid_ag_uid: dict[TermsByXidKey, Term] = dict()

        self.alias_to_xid: dict[str, str] = dict()

        self.root_term = None
        self.root_subterms: dict[str, Term] = dict()

    async def init(self) -> None:
        if self.terms_by_uid and self.terms_by_xid_ag_uid:
            raise RuntimeError("Glossary cache is already initialized! Should not call init again!")

        self.pg_term_mgr = self.service_provider.postgres_db.get_term_mgr()

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "term", self._on_cache_invalidation_msg
            )

        logger.info("start initializing glossary cache...")
        all_terms = await self.pg_term_mgr.find_all()

        for term in all_terms:
            self._put_term_into_cache(term)
            for aliased_unit in term.data.get("alias", []):
                self.alias_to_xid[aliased_unit.upper()] = term.xid

        logger.info("done initializing glossary cache...")
        logger.debug("start initializing root glossary term subterms cache...")

        self.root_term = self.terms_by_uid.get(uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID))

        for term in all_terms:
            if term.sub_class_of == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
                self.root_subterms[term.xid] = term

        logger.debug("done initializing root glossary term subterms cache...")

    def _put_term_into_cache(self, term: Term) -> None:
        # first check if it already existed in cache:
        old_term = self.terms_by_uid.get(term.uid, None)
        if old_term:
            # we need to make sure that the key in `terms_by_xid_ag_uid` is correctly updated
            # First remove item from dict using the old key
            key: TermsByXidKey = (old_term.xid, old_term.access_group_uid)
            del self.terms_by_xid_ag_uid[key]

        # now insert new item:
        self.terms_by_uid[term.uid] = term
        key: TermsByXidKey = (term.xid, term.access_group_uid)
        if key in self.terms_by_xid_ag_uid:
            raise ValueError("tuple term_xid access_group_uid must be unique. found duplicate key with different uid")
        self.terms_by_xid_ag_uid[key] = term

        if term.uid == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
            self.root_term = term

        if term.sub_class_of == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
            self.root_subterms[term.xid] = term

    def _remove_term_from_cache(self, term_uid: uuid.UUID) -> None:
        # allow deletion only by uid because we need to keep both dict's in sync
        term = self.terms_by_uid.get(term_uid, None)
        if term:
            xid_key: TermsByXidKey = (term.xid, term.access_group_uid)
            del self.terms_by_xid_ag_uid[xid_key]
            del self.terms_by_uid[term_uid]

    def clear_cache(self) -> None:
        self.terms_by_uid.clear()
        self.terms_by_xid_ag_uid.clear()

    async def _on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        term_uid = UUID(bytes=msg_bytes)

        # reload term from database and replace the one in our cache:
        term = await self.pg_term_mgr.get_term_by_uid(term_uid)

        if term is not None:
            self._put_term_into_cache(term)
        else:
            self._remove_term_from_cache(term_uid)

        logger.debug("term was updated from db", term_uid=term_uid)

    def get_term_by_id(self, uid: UUID) -> Optional[Term]:
        return self.terms_by_uid[uid]

    async def get_term_by_uid(self, uid: UUID) -> Optional[Term]:
        if uid in self.terms_by_uid:
            return self.terms_by_uid[uid]
        else:
            return await self.pg_term_mgr.get_term_by_uid(uid)

    async def get_terms_by_xid(self, term_xid: str) -> Optional[list]:
        # TODO: this cannot efficiently use the cache currently.
        #  We should check if we can completely get rid of this access type (i.e. by xid but without namespace)
        terms = await self.pg_term_mgr.get_terms_by_xid(term_xid)
        return terms

    async def get_term_by_xid_and_access_group_uid(self, term_xid: str, access_group_uid: str) -> Optional[Term]:
        # TODO: does not have to be async
        key = (term_xid, UUID(access_group_uid))
        return self.terms_by_xid_ag_uid.get(key, None)

    async def put_term_by_uid(self, term: Term) -> Optional[Term]:
        term = await self.pg_term_mgr.insert_term(term)

        # first we update our local cache in this worker
        self._put_term_into_cache(term)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug("emitting a cache invalidation for glossary term", term_uid=term.uid)
            await self.service_provider.messaging_service.emit_invalidate("term", term.uid.bytes)

        return term

    async def put_many_terms(self, terms: List[Term]) -> None:
        await self.pg_term_mgr.insert_many_terms(terms)
        await self.update_cache_for_many_terms(terms)

    async def update_cache_for_many_terms(self, terms: List[Term]) -> None:
        for term in terms:
            # first we update our local cache in this worker
            self._put_term_into_cache(term)

            # Optional messaging_service in case multiple worker caches need to be synchronized
            # (many unittests don't need it, so better to keep it optional)
            if self.service_provider.messaging_service is not None:
                logger.debug("emitting a cache invalidation for glossary term", term_uid=term.uid)
                await self.service_provider.messaging_service.emit_invalidate("term", term.uid.bytes)

    async def get_sub_terms_by_uid(self, term_uid: str, depth: int = 10) -> [Term]:
        all_terms = await self.pg_term_mgr.get_sub_terms_by_uid(
            sub_class_of_uid=term_uid,
            max_depth=depth,
        )

        return all_terms
