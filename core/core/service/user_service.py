import uuid
from typing import Any

from structlog import get_logger

from core.domain.token import Token
from core.domain.user import User
from database.postgres.pg_users_mgr import PostgresUsersMgr

logger = get_logger()


class UserService:
    def __init__(self, user_mgr: PostgresUsersMgr):
        self.user_mgr = user_mgr

    async def create_user(self, **kwargs: Any) -> str:  # noqa: ANN401
        new_user = User(**kwargs)
        logger.info(f"new_user = {new_user}")

        token = await self.user_mgr.insert_or_update_user(new_user)

        return token

    async def find_user_by_id(self, user_id: str) -> User | None:
        return await self.user_mgr.find_user_info_by_user_id(user_id)

    async def validate_token(self, auth_token: str) -> bool:
        return await self.user_mgr.validate_token(auth_token)

    async def upsert_token(self, **kwargs: Any) -> str:  # noqa: ANN401
        new_token = Token(**kwargs)

        return await self.user_mgr.update_access_token(new_token)

    async def list_user_tokens(self, user_id: str) -> list[Token]:
        return await self.user_mgr.list_user_tokens(user_id)

    async def list_users_from_namespace(self, namespace_uid: uuid.UUID) -> [User]:
        return await self.user_mgr.list_users_from_namespace(namespace_uid)
