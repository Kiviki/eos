import importlib
import inspect
import pkgutil
from types import ModuleType
from typing import TYPE_CHECKING, Optional, TypedDict

import gap_filling_modules
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory
from structlog import get_logger

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


logger = get_logger()

GfmDocumentation = TypedDict(
    "GfmDocumentation",
    {
        "name": Optional[str],
        "description": Optional[str],
        "should_be_scheduled": Optional[str],
        "should_be_scheduled_source": Optional[str],
        "can_run_now": Optional[str],
        "can_run_now_source": Optional[str],
        "run": Optional[str],
        "run_source": Optional[str],
    },
)


class GapFillingModuleLoader:
    def __init__(self, blacklist: Optional[list[str]] = None):
        """
        :param blacklist: A list of gfm's names (e.g. 'gap_filling_modules.location_gfm'). If provided, these will not
        run. Else, all gfm's will be run. Useful for testing
        """
        self.pg_graph_mgr = None
        self.glossary_service = None

        # Dynamically load gap filling modules from sub folder:
        def iter_namespace(ns_pkg):
            # Specifying the second argument (prefix) to iter_modules makes the
            # returned name an absolute name instead of a relative one. This allows
            # import_module to work without having to do additional modification to
            # the name.
            return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")

        self.gap_filling_modules: dict[str:ModuleType] = {
            name: importlib.import_module(name)
            for finder, name, ispkg in iter_namespace(gap_filling_modules)
            # either no blacklist is provided, or the module is not blacklisted
            # in any case, skip if it's a package
            if (blacklist is None or name not in blacklist) and not ispkg
        }

        gfm_modules = self.gap_filling_modules.values()
        gfm_workers_list: list[str] = []
        for gfm_module in gfm_modules:
            gfm_workers_list.extend(
                [
                    name
                    for name, obj in inspect.getmembers(gfm_module)
                    if inspect.isclass(obj) and inspect.getmodule(obj) == gfm_module and name.endswith("Worker")
                ]
            )
        self.gfm_workers = set(gfm_workers_list)

        logger.debug("detected gap filling modules: ", gap_filling_modules=self.gap_filling_modules)

        self.initialized_modules: list[AbstractGapFillingFactory] = []
        self.initialized_modules_documentation: list[GfmDocumentation] = []

    async def init(self, service_provider: "ServiceProvider"):
        postgres_db = service_provider.postgres_db

        # since we skip abstract module, we add + 1 to the latter part of the condition
        if len(self.gap_filling_modules) == len(self.initialized_modules) + 1:
            logger.debug(f"No gap filling modules to initialize.")
            return

        for name, mod in self.gap_filling_modules.items():
            if not hasattr(mod, "GapFillingFactory"):
                # sometimes it's useful to disable a GFM by commenting it out the 'GapFillingFactory = ' line at bottom
                logger.warning(f"gap filling module {name} has no GFM registered")

            else:
                if mod.GapFillingFactory.__name__ == AbstractGapFillingFactory.__name__:
                    # skipping the abstract class
                    continue

                logger.debug(f"initializing gap filling factory: {mod.GapFillingFactory.__name__}")
                gap_filling_module_factory: AbstractGapFillingFactory = mod.GapFillingFactory(
                    postgres_db=postgres_db, service_provider=service_provider
                )

                await gap_filling_module_factory.init_cache()

                self.initialized_modules.append(gap_filling_module_factory)

                # extract documentation from the gfm source code
                worker = gap_filling_module_factory.spawn_worker(None)  # type: ignore
                doc: GfmDocumentation = {
                    "name": worker.__class__.__name__.replace("GapFillingWorker", ""),
                    "description": inspect.getdoc(worker.__init__),
                    "should_be_scheduled": inspect.getcomments(worker.should_be_scheduled),
                    "should_be_scheduled_source": inspect.getsource(worker.should_be_scheduled),
                    "can_run_now": inspect.getcomments(worker.can_run_now),
                    "can_run_now_source": inspect.getsource(worker.can_run_now),
                    "run": inspect.getcomments(worker.run),
                    "run_source": inspect.getsource(worker.run),
                }
                self.initialized_modules_documentation.append(doc)

        logger.info(f"initialized {len(self.initialized_modules)} gap filling modules.")

    def clear(self) -> None:
        """
        Clears a list of initialized gap filling modules and their documentations.
        Used by server shutdown method.
        """

        self.initialized_modules.clear()
        self.initialized_modules_documentation.clear()
