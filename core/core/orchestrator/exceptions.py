from uuid import UUID


class GapFillingModuleError(Exception):
    def __init__(self, gfm_name: str, scheduler_iteration: int, error_msg: str, node_uid: UUID, traceback):
        self.gfm_name = gfm_name
        self.scheduler_iteration = scheduler_iteration
        self.error_msg = error_msg
        self.node_uid = node_uid
        self.traceback = traceback

    def __str__(self):
        return "{} failed on node {} at scheduler iteration {} with error: {} \n\n {}".format(
            self.gfm_name, self.node_uid, self.scheduler_iteration, self.error_msg, self.traceback
        )
