"Orchestrator."
import time
import traceback

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, GLOBAL_GFM_STATE_PROP_NAME, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.graph_manager.abstract_graph_observer import AbstractGraphObserver
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.exceptions import GapFillingModuleError
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.glossary_service import GlossaryService

logger = get_logger()


class Orchestrator(AbstractGraphObserver):
    "Orchestrator."

    def __init__(
        self,
        calc_graph: CalcGraph,
        glossary_service: GlossaryService,
        gap_filling_module_loader: GapFillingModuleLoader,
        print_as_tree_between_gfm_runs: bool = False,
    ):
        super().__init__()

        self.calc_graph = calc_graph
        self.glossary_service = glossary_service
        self.gap_filling_module_loader = gap_filling_module_loader
        self.scheduled_gap_filling_modules: list[AbstractGapFillingWorker] = []
        self.scheduled_gap_filling_module_counts: dict[str, int] = {}
        self.perf_counter_ns_per_gfm: dict[str, int] = {}
        self.run_counts_per_gfm: dict[str, int] = {}
        self._new_nodes: list[Node] = []
        self.print_as_tree_between_gfm_runs = print_as_tree_between_gfm_runs  # useful for debugging

        # register the orchestrator to listen for 'notify_new_graph_node'
        self.calc_graph.add_graph_observer(self)

    async def run(self) -> None:
        max_scheduler_iters = 10_000_000  # prevents infinite loop

        logger.info("starting to run orchestrator...")
        start_orchestrator_ns = time.perf_counter_ns()

        # start by loading root node from database:
        root_node = await self.calc_graph.service_provider.node_service.find_by_uid(self.calc_graph.root_node_uid)
        if isinstance(root_node, ActivityNode):
            raise ValueError("Root node must be FlowNode.")
        else:
            assert isinstance(root_node, FlowNode) and not root_node.get_parent_nodes()
            initial_mutation = AddNodeMutation(
                created_by_module=Orchestrator.__name__,
                new_node=root_node,
                copy=True,
            )
            await self.calc_graph.apply_mutation(initial_mutation)
            edges = await self.calc_graph.service_provider.node_service.get_sub_graph_by_uid(root_node.uid, max_depth=1)
            assert edges, "FlowNode has to contain children = an activity"
            child_of_root_node_uid = edges[0].child_uid

        # FIXME: workaround for FlowNode is root node, we also pass the child recipe nodes uid
        child_of_root_node = await self.calc_graph.service_provider.node_service.find_by_uid(child_of_root_node_uid)

        # Fill in calculation's child_of_root_node_uid if it is empty.
        if not self.calc_graph.calculation.child_of_root_node_uid:
            self.calc_graph.calculation.child_of_root_node_uid = child_of_root_node_uid

        assert isinstance(child_of_root_node, ActivityNode)
        initial_mutation = AddNodeMutation(
            created_by_module=Orchestrator.__name__,
            new_node=child_of_root_node,
            parent_node_uid=root_node.uid,
            copy=True,
        )
        await self.calc_graph.apply_mutation(initial_mutation)
        await self.add_gap_filling_workers_on_new_nodes()

        logger.debug("start scheduler and running gap filling modules...")
        counter = 0
        while len(self.scheduled_gap_filling_modules):
            counter += 1

            if counter > max_scheduler_iters:
                logger.error("stopping orchestrator because max iterations reached...")
                break

            if counter % 50_000 == 0:
                logger.warn(
                    f"Long running calculation! Orchestrator scheduler iteration: {counter}. "
                    f"There are still {len(self.scheduled_gap_filling_modules)} gap filling modules scheduled. "
                    f"There are {len(self.calc_graph)} nodes in the graph."
                )

            mod = self.scheduled_gap_filling_modules.pop()
            logger.debug(f"run next gap filling module {mod}...")

            # TODO: for now convert generator to set to fix bugs where it is used multiple times with "in" checks:
            status = mod.can_run_now()

            if status == GapFillingWorkerStatusEnum.ready:
                try:
                    start_ns = time.perf_counter_ns()
                    await mod.run(self.calc_graph)
                    dur_ns = time.perf_counter_ns() - start_ns

                    if self.print_as_tree_between_gfm_runs:
                        logger.debug(f"after running the gap filling module: {mod.__class__.__name__}")
                        logger.debug(self.calc_graph.format_as_tree())

                    await self.update_gfm_stats(dur_ns, mod)
                    await self.update_gfm_state_prop_of_node([mod], new_state=NodeGfmStateEnum.finished)
                except Exception as e:
                    logger.warn(
                        "error running gap filling module",
                        gfm_name=mod.__class__.__name__,
                        scheduler_iteration=counter,
                        node_uid=mod.node.uid,
                        error_msg=str(e),
                        traceback=traceback.format_exc(),
                    )
                    raise GapFillingModuleError(
                        gfm_name=mod.__class__.__name__,
                        scheduler_iteration=counter,
                        node_uid=mod.node.uid,
                        error_msg=str(e),
                        traceback=traceback.format_exc(),
                    ) from e
                await self.add_gap_filling_workers_on_new_nodes()
            elif status == GapFillingWorkerStatusEnum.reschedule:
                logger.debug(f"gap filling module cannot run now. Reschedule it for later: {mod.__class__.__name__}")

                mod.reschedule_counter += 1

                if mod.reschedule_counter > 2000:
                    logger.warn(
                        f"gap filling module was rescheduled more than 2000 times, "
                        f"cancelling it: {mod.__class__.__name__}"
                    )

                    continue

                if mod.reschedule_counter > 500 and mod.reschedule_counter % 500 == 0:
                    logger.warn(
                        f"gap filling module was rescheduled more than "
                        f"{mod.reschedule_counter} times: {mod.__class__.__name__}"
                    )

                # add to head of list. meaning: module will run last in the scheduled list
                self.scheduled_gap_filling_modules.insert(0, mod)
            elif status == GapFillingWorkerStatusEnum.cancel:
                logger.debug(f"gap filling module cancelled: {mod.__class__.__name__}")
                await self.update_gfm_state_prop_of_node([mod], new_state=NodeGfmStateEnum.canceled)

        dur_orchestrator_sec = (time.perf_counter_ns() - start_orchestrator_ns) / 1e9
        logger.info("finished all gap filling modules in orchestrator. Duration [sec]: ", duration=dur_orchestrator_sec)

        if dur_orchestrator_sec > 60.0:
            logger.warn("orchestrator took more than 60 seconds. Duration [sec]: ", duration=dur_orchestrator_sec)
            await self.print_gfm_stats()

    async def update_gfm_stats(self, dur_ns: int, mod: AbstractGapFillingWorker) -> None:
        if mod.__class__.__name__ not in self.perf_counter_ns_per_gfm:
            self.perf_counter_ns_per_gfm[mod.__class__.__name__] = 0
        self.perf_counter_ns_per_gfm[mod.__class__.__name__] += dur_ns

        if mod.__class__.__name__ not in self.run_counts_per_gfm:
            self.run_counts_per_gfm[mod.__class__.__name__] = 0
        self.run_counts_per_gfm[mod.__class__.__name__] += 1

    async def print_gfm_stats(self) -> None:
        "Log stats at info level."
        logger.warn("run counts per gfm:", run_counts_per_gfm=self.run_counts_per_gfm)

        logger.warn(
            "total runtime per gfm [sec]:",
            perf_counter_ns_per_gfm={k: v / 1e9 for k, v in self.perf_counter_ns_per_gfm.items()},
        )

        logger.warn(
            "average runtime per gfm [ms]",
            avg_runtime_ms_per_gfm={
                k: self.perf_counter_ns_per_gfm[k] / 1e6 / v for k, v in self.run_counts_per_gfm.items()
            },
        )

    async def update_gfm_state_prop_of_node(
        self, gfm_workers: list[AbstractGapFillingWorker], new_state: NodeGfmStateEnum
    ) -> None:
        """Updates the gfm_state property of a node with the given gfm_name and new_state.

        Args:
        gfm_workers: the gap filling module workers of which the status should be updated
                     Requirement: all workers must have the same node
        new_state: NodeGfmStateEnum (i.e. "scheduled" or "finished" or "cancelled")
                   Requirement: "finished"/"cancelled" are only allowed if gfm's state was "scheduled" before.
        """
        gfm_names = [worker.__class__.__name__ for worker in gfm_workers]
        node = gfm_workers[0].node
        assert all([worker.node.uid == node.uid for worker in gfm_workers]), "all workers must have the same node"

        prev_gfm_state = node.gfm_state
        if prev_gfm_state:
            gfm_state_data = dict(prev_gfm_state.worker_states)
        else:
            gfm_state_data = {}

        if new_state == NodeGfmStateEnum.canceled or new_state == NodeGfmStateEnum.finished:
            # check if all gfm's were scheduled before:
            for gfm_name in gfm_names:
                assert (
                    gfm_state_data[gfm_name] == NodeGfmStateEnum.scheduled.value
                ), f"cannot set gfm_state to {new_state} because it was not scheduled before"

        for gfm_name in gfm_names:
            gfm_state_data[gfm_name] = new_state.value

        gfm_state_mutation = PropMutation(
            created_by_module=Orchestrator.__name__,
            node_uid=node.uid,
            prop_name=GFM_STATE_PROP_NAME,
            prop=GfmStateProp.unvalidated_construct(worker_states=gfm_state_data),
        )
        await self.calc_graph.apply_mutation(gfm_state_mutation)

        # Update the scheduled_gfm_counts and check if there is a global change of scheduled gfm types:
        changed_global_state = False
        for gfm_name in gfm_names:
            if gfm_name not in self.scheduled_gap_filling_module_counts:
                # init counter of newly encountered gfm_name:
                self.scheduled_gap_filling_module_counts[gfm_name] = 0

            if new_state == NodeGfmStateEnum.scheduled:
                if self.scheduled_gap_filling_module_counts[gfm_name] == 0:
                    # counts changes from 0 to 1
                    changed_global_state = True
                self.scheduled_gap_filling_module_counts[gfm_name] += 1

            else:  # finished or cancelled
                self.scheduled_gap_filling_module_counts[gfm_name] -= 1
                if self.scheduled_gap_filling_module_counts[gfm_name] == 0:
                    # counts changed from 1 to 0
                    changed_global_state = True
                assert self.scheduled_gap_filling_module_counts[gfm_name] >= 0, "must not be negative"

        # Update global scheduling state in root node if there was a change:
        if changed_global_state:
            # recalculate the global scheduling state (i.e. for all gfm types if there are any globally scheduled):
            global_gfm_state_data = {}
            for gfm_name, count in self.scheduled_gap_filling_module_counts.items():
                if count > 0:
                    new_state = NodeGfmStateEnum.scheduled
                else:
                    new_state = NodeGfmStateEnum.finished
                global_gfm_state_data[gfm_name] = new_state.value

            # create the mutation on the root node:
            global_gfm_state_mutation = PropMutation(
                created_by_module=Orchestrator.__name__,
                node_uid=self.calc_graph.get_root_node().uid,
                prop_name=GLOBAL_GFM_STATE_PROP_NAME,
                prop=GfmStateProp.unvalidated_construct(worker_states=global_gfm_state_data),
            )
            await self.calc_graph.apply_mutation(global_gfm_state_mutation)

            # Would it be better to remove global_gfm_state from the root_node and keep it only in calculation?
            if self.calc_graph.calculation:
                self.calc_graph.calculation.global_gfm_state = self.calc_graph.get_root_node().global_gfm_state
            else:
                self.calc_graph.calculation = Calculation(
                    global_gfm_state=self.calc_graph.get_root_node().global_gfm_state
                )

    def notify_new_graph_node(self, node: Node) -> None:
        self._new_nodes.append(node)

    async def add_gap_filling_workers_on_new_nodes(self) -> None:
        logger.debug(f"there are {len(self._new_nodes)} new nodes in the graph... scheduling gap filling workers...")

        # for each new node, we spawn gap filling workers:
        for node_view in self._new_nodes:
            logger.debug(f"spawning gap filling modules on new_node ({node_view})")

            preset_states = node_view.gfm_state
            preset_gfms_blacklist = []

            if preset_states:
                preset_states = dict(preset_states.worker_states)

                for module in preset_states.keys():
                    if preset_states[module] != NodeGfmStateEnum.scheduled:
                        preset_gfms_blacklist.append(module)

            newly_scheduled_gfms: list[AbstractGapFillingWorker] = []

            calculation = self.calc_graph.calculation
            gfms_for_which_to_save_most_shallow_nodes = (
                calculation.gfms_for_which_to_save_most_shallow_nodes if calculation else []
            )

            for gap_filling_factory in self.gap_filling_module_loader.initialized_modules:
                gap_filling_worker: AbstractGapFillingWorker = gap_filling_factory.spawn_worker(node_view)

                # if we have a list of GFMs that should run on this module and current GFM is not in it, we skip it
                if preset_gfms_blacklist and gap_filling_worker.__class__.__name__ in preset_gfms_blacklist:
                    continue

                if gap_filling_worker.should_be_scheduled():
                    # spawn gap filling modules on new node in graph:
                    self.scheduled_gap_filling_modules.append(gap_filling_worker)
                    newly_scheduled_gfms.append(gap_filling_worker)

                    # add node to set of nodes that can change dynamically for this gfm:
                    gfm_name = gap_filling_worker.__class__.__name__
                    if gfm_name in gfms_for_which_to_save_most_shallow_nodes:
                        self.calc_graph.nodes_that_can_change_dynamically_for_each_gfm.setdefault(gfm_name, set()).add(
                            node_view.uid
                        )

            if len(newly_scheduled_gfms) > 0:
                await self.update_gfm_state_prop_of_node(newly_scheduled_gfms, new_state=NodeGfmStateEnum.scheduled)

        # clear list:
        self._new_nodes = []
