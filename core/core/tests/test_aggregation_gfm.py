"""Test Aggregation gap filling module."""
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.aggregation_gfm import AggregationGapFillingFactory
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop

from core.domain.calculation import Calculation
from core.domain.nodes import FoodProcessingActivityNode
from core.domain.nodes.node import Node
from core.domain.term import Term
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.access_group_service import AccessGroupService
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeWithDeclaration, is_close, prepend_flow_node
from core.tests.test_ingredient_amount_estimator_gfm import nutrients_mixer
from core.tests.test_orchestration_gfm import run_orchestrator_on_recipe
from database.postgres.postgres_db import PostgresDb


async def _run_calculation(
    services: Tuple[PostgresDb, ServiceProvider],
    ingredient_declaration: str,
    nutrient_values: dict[str, float],
    production_amount: float = 100,
) -> Tuple[Node, PostgresDb, ServiceProvider]:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount={"value": production_amount},
            ingredients_declaration=[
                {
                    "language": "de",
                    "value": ingredient_declaration,
                },
            ],
            raw_input={
                "titles": [
                    {
                        "language": "en",
                        "value": "A sample recipe with 2 ingredients, to test the ingredient estimator",
                    },
                ]
            },
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=nutrient_values)

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    return root_node, postgres_db, service_provider


def setup_nutrients_units_food_categories_terms(
    service_provider: ServiceProvider,
) -> Tuple[dict, dict, Term, Term, Term]:
    root_nutr_term = service_provider.glossary_service.root_subterms["EOS_nutrient_names"]
    root_unit_term = service_provider.glossary_service.root_subterms["EOS_units"]
    root_food_categories_term = service_provider.glossary_service.root_subterms["Root_Food_Categories"]

    nutrs = ["fat", "sodium", "energy", "protein", "carbohydrates", "water"]
    nutr_name_to_term = {
        nutr: service_provider.glossary_service.terms_by_xid_ag_uid[
            (f"EOS_{nutr}", root_nutr_term.access_group_uid)
        ].uid
        for nutr in nutrs
    }

    gram_term = service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_gram", root_unit_term.access_group_uid)]
    kcal_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_kilocalorie", root_unit_term.access_group_uid)
    ]
    production_amount_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_production_amount", root_unit_term.access_group_uid)
    ]

    food_categories = [
        "EOS_Diet-low-in-vegetables",
        "EOS_Plant-based-products",
        "EOS_Vegetables",
        "EOS_Diet-low-in-milk",
    ]
    food_cat_to_term = {
        food_cat: service_provider.glossary_service.terms_by_xid_ag_uid[
            (food_cat, root_food_categories_term.access_group_uid)
        ].uid
        for food_cat in food_categories
    }
    return nutr_name_to_term, food_cat_to_term, gram_term, kcal_term, production_amount_term


@pytest.mark.asyncio
async def test_aggregation_gfm(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """Test two things.

    1) AggregationGFM is only scheduled on the root node
    2) nutrients and food categories are computed correctly (no matrix calculation is done here).
    """
    known_root_diet_low_in_vege = 50
    known_carrot_diet_low_in_vege = 100

    known_root_plant_based = 100
    known_carrot_plant_based = 100
    known_potato_plant_based = 100

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    known_carrot_prot = carrot_nutrients["protein_gram"]
    known_potato_prot = potato_nutrients["protein_gram"]

    known_carrot_fat = carrot_nutrients["fat_gram"]
    known_potato_fat = potato_nutrients["fat_gram"]
    known_root_fat = (known_potato_fat + known_carrot_fat) / 2.0

    known_carrot_salt = carrot_nutrients["sodium_milligram"]
    known_potato_salt = potato_nutrients["sodium_milligram"]
    known_root_salt = (known_potato_salt + known_carrot_salt) / 2.0

    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        "Karotten, Kartoffeln",
        mix_50_50_carrot_potato,
    )

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    gap_filling_factory = AggregationGapFillingFactory(postgres_db, service_provider)

    await gap_filling_factory.init_cache()

    child_of_root_node = root_node.get_sub_nodes()[0]
    gap_filling_worker = gap_filling_factory.spawn_worker(root_node)
    assert (
        gap_filling_worker.should_be_scheduled()
    ), "GFM has not been scheduled to run, but it should always be scheduled for a root node."

    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0])
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "GFM has been scheduled to run, but it should not be scheduled for a sub-node."

    nutrient_values = root_node.nutrient_values.amount_for_activity_production_amount().quantities

    assert nutrient_values[nutr_name_to_term["fat"]].unit_term_uid == gram_term.uid
    assert is_close(nutrient_values[nutr_name_to_term["fat"]].value, known_root_fat)

    assert nutrient_values[nutr_name_to_term["sodium"]].unit_term_uid == gram_term.uid
    assert is_close(nutrient_values[nutr_name_to_term["sodium"]].value, known_root_salt / 1000)

    assert nutrient_values[nutr_name_to_term["energy"]].unit_term_uid == kcal_term.uid

    food_categories = root_node.food_categories.amount_for_activity_production_amount().quantities
    assert food_categories[food_cat_to_term["EOS_Diet-low-in-vegetables"]].unit_term_uid == gram_term.uid
    assert is_close(food_categories[food_cat_to_term["EOS_Diet-low-in-vegetables"]].value, known_root_diet_low_in_vege)
    assert food_categories[food_cat_to_term["EOS_Plant-based-products"]].unit_term_uid == gram_term.uid
    assert is_close(food_categories[food_cat_to_term["EOS_Plant-based-products"]].value, known_root_plant_based)

    sub_flow_nutrients = [sn.aggregated_nutrients for sn in child_of_root_node.get_sub_nodes()]
    sub_activity_prod_amount = [
        sn.get_sub_nodes()[0].production_amount.value for sn in child_of_root_node.get_sub_nodes()
    ]

    assert is_close(
        sub_flow_nutrients[0].amount_for_activity_production_amount().quantities[nutr_name_to_term["protein"]].value,
        known_carrot_prot * 0.5,
    )
    assert is_close(
        sub_flow_nutrients[1].amount_for_activity_production_amount().quantities[nutr_name_to_term["protein"]].value,
        known_potato_prot * 0.5,
    )
    assert is_close(
        sub_flow_nutrients[0].amount_for_100g().quantities[nutr_name_to_term["protein"]].value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_prot * 10,
    )
    assert is_close(
        sub_flow_nutrients[1].amount_for_100g().quantities[nutr_name_to_term["protein"]].value
        * 10
        * sub_activity_prod_amount[1],
        known_potato_prot * 10,
    )

    sub_flow_food_categories = [sn.food_categories for sn in child_of_root_node.get_sub_nodes()]
    assert is_close(
        sub_flow_food_categories[0]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .value,
        known_carrot_diet_low_in_vege * 0.5,
    )
    assert is_close(
        sub_flow_food_categories[0]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value,
        known_carrot_plant_based * 0.5,
    )
    assert is_close(
        sub_flow_food_categories[1]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value,
        known_potato_plant_based * 0.5,
    )

    assert is_close(
        sub_flow_food_categories[0].amount_for_100g().quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]].value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_diet_low_in_vege * 10,
    )
    assert is_close(
        sub_flow_food_categories[0].amount_for_100g().quantities[food_cat_to_term["EOS_Plant-based-products"]].value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_plant_based * 10,
    )
    assert is_close(
        sub_flow_food_categories[1].amount_for_100g().quantities[food_cat_to_term["EOS_Plant-based-products"]].value
        * 10
        * sub_activity_prod_amount[1],
        known_potato_plant_based * 10,
    )


@pytest.mark.asyncio
async def test_aggregation_missing_water_gfm(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Aggregation occurs correctly by comparing it with ingredient amount esimated version."""
    nutrient_values = {
        "energy_kcal": 100,
        "fat_gram": 0,
        "saturated_fat_gram": 0,
        "carbohydrates_gram": 25,
        "sucrose_gram": 18,
        "protein_gram": 0,
        "sodium_chloride_gram": 0,
    }

    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services, "Karotten, Kartoffeln", nutrient_values, 90.0
    )

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)
    child_of_root_node = root_node.get_sub_nodes()[0]

    for nutr in nutr_name_to_term:
        # Check that the production amount is indeed 90g
        activity_amount = sum(
            sn.aggregated_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term[nutr]].value
            for sn in child_of_root_node.get_sub_nodes()
        )

        assert is_close(
            root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value * 0.9,
            activity_amount,
        )
        # Aggregated and declared nutrient values are not the same.
        if nutr not in ("water", "sodium", "chlorine"):
            assert not is_close(
                root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
                root_node.nutrient_values.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
            )
        assert is_close(
            root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
            child_of_root_node.ingredient_amount_estimator_estimated_nutrients.quantities[
                nutr_name_to_term[nutr]
            ].value,
        )


@pytest.mark.asyncio
async def test_aggregation_gfm_with_combined_ingredients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """If nutrients can be correctly aggregated for combined food products."""
    carrot_amount = 48.6423213 / 100
    potato_amount = 1.3576787 / 100
    onion_amount = (50 - 1e-8) / 100
    tomato_amount = 1e-8 / 100
    raw_onion_ratio = 48.76715965534985 / 50
    dried_onion_ratio = (50 - 50 * raw_onion_ratio) / 50
    upscale_ratio = 5.025125628140706

    total_rainforest_critical_product_amount = (
        onion_amount * (raw_onion_ratio * 40 + upscale_ratio * dried_onion_ratio * 40) + carrot_amount * 35
    )

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    onion_nutrients = load_seeded_nutrients["onion"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        onion_nutrients,
    )

    child_of_root_node = root_node.get_sub_nodes()[0]

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    known_main_ingr1_fat = carrot_amount * carrot_nutrients["fat_gram"] + potato_amount * potato_nutrients["fat_gram"]
    known_main_ingr2_fat = (
        onion_amount * dried_onion_ratio * upscale_ratio * onion_nutrients["fat_gram"]
        + onion_amount * raw_onion_ratio * onion_nutrients["fat_gram"]
        + tomato_amount * tomato_nutrients["fat_gram"]
    )

    known_root_diet_low_in_vege = (carrot_amount + onion_amount + tomato_amount) * 100

    main_ingr1_flow_nutrients = child_of_root_node.get_sub_nodes()[0].aggregated_nutrients
    main_ingr2_flow_nutrients = child_of_root_node.get_sub_nodes()[1].aggregated_nutrients
    main_ingr1_node_nutrients = [
        sn.aggregated_nutrients for sn in child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    ]
    main_ingr2_node_nutrients = [
        sn.aggregated_nutrients for sn in child_of_root_node.get_sub_nodes()[1].get_sub_nodes()[0].get_sub_nodes()
    ]

    root_node_food_categories = root_node.food_categories
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    not_certified_for_rainforest_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_rainforest", access_group_uid)
    ]

    assert is_close(
        root_node.rainforest_critical_products.quantities[not_certified_for_rainforest_term.uid].value,
        total_rainforest_critical_product_amount,
    )

    assert (
        main_ingr1_node_nutrients[0]
        .amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        sum(
            nutr.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
            for nutr in main_ingr1_node_nutrients
        ),
        known_main_ingr1_fat * 20,
    )

    assert (
        main_ingr2_node_nutrients[0]
        .amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        sum(
            nutr.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
            for nutr in main_ingr2_node_nutrients
        ),
        known_main_ingr2_fat * 20,
    )

    assert (
        main_ingr1_flow_nutrients.amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        main_ingr1_flow_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value,
        known_main_ingr1_fat,
    )

    assert (
        main_ingr2_flow_nutrients.amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        main_ingr2_flow_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value,
        known_main_ingr2_fat,
    )

    assert (
        root_node_food_categories.amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        root_node_food_categories.amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .value,
        known_root_diet_low_in_vege,
    )

    dried_carrot_node = child_of_root_node
    for _ in range(4):
        dried_carrot_node = dried_carrot_node.get_sub_nodes()[0]
    dried_carrot_node = dried_carrot_node.get_sub_nodes()[1].get_sub_nodes()[0]

    carrot_node = child_of_root_node
    for _ in range(4):
        carrot_node = carrot_node.get_sub_nodes()[0]

    dried_carrot_node_nutrients = dried_carrot_node.get_parent_nodes()[0].nutrient_values
    dried_carrot_flow = dried_carrot_node.get_parent_nodes()[0]

    dried_carrot_nutrient_values = dried_carrot_flow.nutrient_values
    dried_carrot_fat = dried_carrot_nutrient_values.quantities[nutr_name_to_term["fat"]]

    assert (
        dried_carrot_node_nutrients.amount_for_100g().quantities[nutr_name_to_term["fat"]].unit_term_uid
        == gram_term.uid
    )

    assert is_close(
        dried_carrot_node_nutrients.amount_for_100g().quantities[nutr_name_to_term["fat"]].value,
        dried_carrot_fat.value,
    )

    carrot_bw_activity = child_of_root_node
    for _ in range(6):
        carrot_bw_activity = carrot_bw_activity.get_sub_nodes()[0]

    bw_parent_flow = carrot_bw_activity.get_parent_nodes()[0]

    assert not bw_parent_flow.is_dried()

    carrot_bw_flow_nutrients = bw_parent_flow.nutrient_values.amount_for_activity_production_amount().quantities
    carrot_node_food_categories = carrot_node.get_parent_nodes()[0].food_categories.amount_for_100g().quantities

    carrot_food_categories = {
        food_cat_to_term["EOS_Plant-based-products"]: {"value": 100, "unit": "g"},
        food_cat_to_term["EOS_Vegetables"]: {"value": 100, "unit": "g"},
        food_cat_to_term["EOS_Diet-low-in-vegetables"]: {"value": 100, "unit": "g"},
    }

    gap_filling_factory = AggregationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    diff_between_food_categories = 0
    for key, val in carrot_food_categories.items():
        diff_between_food_categories += abs(val["value"] - carrot_node_food_categories[key].value)
    assert is_close(diff_between_food_categories, 0.0)

    diff_between_nutrients = 0
    for key, val in nutrients_dict_to_prop(carrot_nutrients).quantities.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - carrot_bw_flow_nutrients[key].value / 10)
    assert is_close(diff_between_nutrients, 0.0)


@pytest.mark.asyncio
async def test_aggregation_gfm_with_minimum_recipe(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    pg_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    service_provider.access_group_service = AccessGroupService(service_provider)
    await service_provider.access_group_service.init()

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    calc_graph, _ = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe, return_log=True)
    root_node = calc_graph.get_root_node()
    child_of_root_node = calc_graph.child_of_root_node()

    nutrients_from_aggregation_gfm = root_node.nutrient_values.amount_for_activity_production_amount()

    assert len(calc_graph.data_errors_log) == 0

    assert nutrients_from_aggregation_gfm

    nutrients_from_ingredient_amount_estimator_gfm = (
        child_of_root_node.ingredient_amount_estimator_estimated_nutrients.amount_for_100g().quantities
    )

    nutrients_quantities = nutrients_from_aggregation_gfm.quantities

    gap_filling_factory = AggregationGapFillingFactory(pg_db, service_provider)
    await gap_filling_factory.init_cache()

    diff_between_nutrients = 0
    for key, val in nutrients_from_ingredient_amount_estimator_gfm.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - nutrients_quantities[key].value / 10)

    assert is_close(
        diff_between_nutrients, 0.0
    ), "nutrients estimated by ingredient amount estimator differs from that of daily food unit gap-filling module."

    flow = child_of_root_node.get_sub_nodes()[0]
    flow_nutrients_declared = flow.nutrient_values.quantities
    flow_nutrients_from_aggregation = flow.aggregated_nutrients.amount_for_activity_production_amount().quantities

    diff_between_nutrients = 0
    for key, val in flow_nutrients_declared.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - flow_nutrients_from_aggregation[key].value / 10)

    assert is_close(
        diff_between_nutrients, 0.0
    ), "nutrients from eurofir differs from that of daily food unit gap-filling module."

    recipe_sub_nodes = child_of_root_node.get_sub_nodes()

    sub_flow_lcl_plant = [
        sn.food_categories.amount_for_activity_production_amount()
        .quantities.get(food_cat_to_term["EOS_Plant-based-products"])
        .value
        for sn in recipe_sub_nodes
    ]
    sub_flow_root_plant = [
        sn.food_categories.amount_for_root_node().quantities.get(food_cat_to_term["EOS_Plant-based-products"]).value
        for sn in recipe_sub_nodes
    ]

    parent_supply_for_root = (
        child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0].impact_assessment_supply_for_root.value
    )

    assert is_close(
        sum([abs(r - lp * parent_supply_for_root) for (r, lp) in zip(sub_flow_root_plant, sub_flow_lcl_plant)]), 0.0
    ), "Plant based products not scaled correctly for root."
