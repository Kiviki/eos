"Test Nutrient Subdivision Gap filling module."
import uuid
from typing import Optional, Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop
from gap_filling_modules.match_product_name_gfm import MATCH_PRODUCT_GFM_NAME, MatchProductNameGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProcessingActivityNode, ModeledActivityNode
from core.domain.nodes.flow.food_product_flow import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import TermAccessGroupIdsRType, prepend_flow_node
from database.postgres.postgres_db import PostgresDb

# this data comes from the nutrition files
LOW_FAT_COCOA = 10.9  # g/100g
HIGH_FAT_COCOA = 20.6
MED_FAT_COCOA = (LOW_FAT_COCOA + HIGH_FAT_COCOA) / 2


@pytest.mark.asyncio
async def test_nutrient_subdivision(
    setup_services: Tuple[PostgresDb, ServiceProvider], get_term_access_group_uids: TermAccessGroupIdsRType
) -> None:
    """Test the nutrient subdivision gap filling module.

    - run nutrition_subdivision_GFM, nothing happens
    - add subdivision Terms for cocoa
    - run nutrition_subdivision_GFM, subdivision happens
    """
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    dummy_activity = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            uid=uuid.uuid4(),
            gfm_state=GfmStateProp.unvalidated_construct(
                worker_states={"IngredientAmountEstimatorGapFillingWorker": "S"}
            ),
        )
    )

    # create one ingredient & add it to the graph
    cocoa_ingredient = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": "Kakaopulver"}],
            nutrient_values=nutrients_dict_to_prop(
                {
                    "fat_gram": MED_FAT_COCOA,
                }
            ),
            type="recipe-ingredients",
        )
    )
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=dummy_activity.uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(cocoa_ingredient)
    calc_graph.add_node(dummy_activity)
    calc_graph.add_edge(dummy_activity.uid, cocoa_ingredient.uid)

    # attach Term to ingredient & matching
    cocoa_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A03HG", foodex2_term_group_uid)
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )
    await calc_graph.apply_mutation(
        PropMutation(
            created_by_module=MatchProductNameGapFillingWorker.__class__.__name__,
            node_uid=cocoa_ingredient.uid,
            prop_name="product_name",
            prop=NamesProp(
                terms=[GlossaryTermProp(term_uid=cocoa_term.uid)],
                source_data_raw=[{"language": "de", "value": "Kakaopulver"}],
            ),
        )
    )

    # now run the NutrientSubdivision GFM
    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(cocoa_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    product_name_prop = cocoa_ingredient.product_name
    assert len(product_name_prop.terms) == 1

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "A03HG"

    assert cocoa_ingredient.is_subdivision is False, "is_subdivision should not be set yet"
    assert len(cocoa_ingredient.get_sub_nodes()) == 0, "At this stage, the ingredient should not have any sub-divisions"

    # This is the 2nd part of the test: add the subdivision data and run the GFM again

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
    )

    # now run the NutrientSubdivision GFM again
    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(cocoa_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert len(cocoa_ingredient.get_sub_nodes()) == 1, "At this stage, cocoa should have sub-divisions"

    splitted_nodes = cocoa_ingredient.get_sub_nodes()[0].get_sub_nodes()
    # partially defatted (732), partially defatted (732) (dried), low-fat (733)
    assert len(splitted_nodes) == 3, "At this stage, cocoa should have 3 sub-divisions"

    for node in splitted_nodes:
        node_name = node.product_name.source_data_raw[0]["value"]

        assert node_name == "Kakaopulver" or "Kakaopulver (dried)"
        assert node.is_subdivision, "subdivided ingredient should be marked as such"

        if "(dried)" in node_name:
            assert node.is_dried(), "No `is_dried` property attached!"
            assert "J0116" in frozenset(
                tag.get_term().xid for tag in node.glossary_tags
            ), "No dried state Term attached!"


async def add_subdivision_data(
    cocoa_term: Term,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: uuid,
    eurofir_term_group_uid: uuid,
    nutrient_subdivision_term_group_uid: uuid,
    use_nutrition_term: bool = True,
    cocoa_process: Optional[Node] = None,
) -> list[Term]:
    "Create two more ingredient for the subdivision & add them to the graph."
    cocoa_ingredients_subdivided: list[Term] = []

    for name, nutrition_id in [("Kakaopulver schwach entölt", 732), ("Kakaopulver stark entölt", 733)]:
        nutrition_term: Term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            str(nutrition_id), eurofir_term_group_uid
        )

        assert nutrition_term, f"Could not find nutrition term with xid {nutrition_id} for {name}"

        t = Term(
            uid=uuid.uuid4(),
            name=name,
            xid="dummy_" + name,
            access_group_uid=foodex2_term_group_uid,
            data={},
            sub_class_of=cocoa_term.sub_class_of,
        )
        cocoa_ingredients_subdivided.append(await service_provider.glossary_service.put_term_by_uid(t))

        if cocoa_process:
            await service_provider.glossary_link_service.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                    term_uids=[t.uid],
                    linked_node_uid=cocoa_process.uid,
                )
            )

        # link nutrition term to the ingredient
        if use_nutrition_term:
            await service_provider.glossary_link_service.insert_glossary_link(
                GlossaryLink(
                    term_uids=[t.uid],
                    gap_filling_module="Nutrients",
                    linked_term_uid=nutrition_term.uid,
                )
            )

    # add the subdivision term to the Glossary
    subdivision_term = Term(
        name="Kakaopulver_subdivision",
        sub_class_of=None,
        data={
            "subproduct_terms_uuids": [
                [str(cocoa_ingredients_subdivided[0].uid)],
                [str(cocoa_ingredients_subdivided[1].uid)],
            ],
        },
        access_group_uid=uuid.UUID(nutrient_subdivision_term_group_uid),
        uid=uuid.uuid4(),
        xid=str(cocoa_term.uid),
    )
    await service_provider.glossary_service.put_term_by_uid(subdivision_term)

    return cocoa_ingredients_subdivided


@pytest.mark.asyncio
async def test_nutrient_subdivision_exception_without_nutrition_term(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    "Exception should happen when nutrition term is missing."
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # attach Term to ingredient & matching
    cocoa_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A03HG", foodex2_term_group_uid)
    assert cocoa_term, "Could not find cocoa term"
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )

    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            title=[
                {
                    "language": "de",
                    "value": "Kakaopulver recipe",
                },
            ],
            ingredients_declaration=[
                {
                    "language": "de",
                    "value": "Kakaopulver",
                },
            ],
        )
    )

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
        use_nutrition_term=False,
    )

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    root_flow = await prepend_flow_node(pg_db.get_graph_mgr(), recipe.uid, nutrient_values={"fat_gram": 0.0})

    calculation = Calculation(
        uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.data_errors) > 0
    assert calculation.data_errors[0].startswith(
        "Calculation failure error: No nutrition term found in subdivision term",
    )


@pytest.mark.parametrize(
    "cocoa_fat",
    [
        LOW_FAT_COCOA,
        MED_FAT_COCOA,
        HIGH_FAT_COCOA,
    ],
)
@pytest.mark.asyncio
async def test_nutrient_subdivision_with_recipe(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    cocoa_fat: float,
) -> None:
    """Run 3x with different cocoa fat in the recipe.

    Test that the subdivision is working both ways
    (= that the weight decrease constraint is deactivated).
    """
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # attach Term to ingredient & matching
    cocoa_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A03HG", foodex2_term_group_uid)
    assert cocoa_term, "Could not find cocoa term"
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )

    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            ingredients_declaration=[
                {
                    "language": "de",
                    "value": "Kakaopulver",
                },
            ],
        )
    )

    cocoa_process = await pg_db.get_graph_mgr().upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for cacao (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="cacao",
        )
    )
    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[cocoa_term.uid],
            linked_node_uid=cocoa_process.uid,
        )
    )

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
        cocoa_process=cocoa_process,
    )

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()

    root_flow = await prepend_flow_node(
        pg_db.get_graph_mgr(),
        recipe.uid,
        nutrient_values={
            "fat_gram": cocoa_fat,
        },
    )

    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()

    for subdivision_node in root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes():
        unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
        recipe_node = root_node.get_sub_nodes()[0]
        recipe_node_unit = recipe_node.production_amount.get_unit_term()

        recipe_node_production_amount_gram = recipe_node.production_amount.value * recipe_node_unit.data["mass-in-g"]

        ingredient_amount_gram = round(subdivision_node.amount.amount_for_root_node().value, 3) * unit.data["mass-in-g"]

        # Scale the ingredient_amount such that it is for 100g of the recipe_node production.
        ingredient_amount = ingredient_amount_gram / recipe_node_production_amount_gram * 100

        name = service_provider.glossary_service.terms_by_uid[subdivision_node.product_name.terms[0].term_uid].name

        if cocoa_fat == LOW_FAT_COCOA:
            # we set the recipe's fat to equal the LOW fat cocoa; the ingredient amount estimator should choose
            # 0g of the HIGH fat cocoa and 100g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 0 <= ingredient_amount < 0.1, "should not use dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 0 <= ingredient_amount < 0.1, "should not use high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 99 < ingredient_amount <= 100, "should use much of low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        elif cocoa_fat == MED_FAT_COCOA:
            # we set the recipe's fat to equal the MED fat cocoa; the ingredient amount estimator should choose
            # 50g of the HIGH fat cocoa and 50g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 22.5 <= ingredient_amount < 23.5, "should use around 23g of dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 24.0 < ingredient_amount < 25.0, "should use around 25g of high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 51.5 < ingredient_amount < 52.5, "should use around 52g of low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        elif cocoa_fat == HIGH_FAT_COCOA:
            # we set the recipe's fat to equal the HIGH fat cocoa; the ingredient amount estimator should choose
            # 100g of the HIGH fat cocoa and 0g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 62.5 < ingredient_amount < 63.0, "should use around 63g of dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 32.0 < ingredient_amount < 32.5, "should use around 32g of high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 5.0 < ingredient_amount < 5.5, "should use only 5g low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        else:
            raise ValueError("invalid cocoa fat")
