"Test vitascore gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.vitascore_gfm import VitascoreGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props import QuantityPackageProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.access_group_service import AccessGroupService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import is_close
from core.tests.test_aggregation_gfm import setup_nutrients_units_food_categories_terms
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings

MILK_XID = "EOS_Diet-low-in-milk"


@pytest.mark.asyncio
async def test_vitascore_gfm(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    root_char_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    root_unit_term = glossary_service.root_subterms["EOS_units"]

    vitascore_char_term = glossary_service.terms_by_xid_ag_uid[("EOS_vita-score", root_char_term.access_group_uid)]
    dfu_unit_term = glossary_service.terms_by_xid_ag_uid[("EOS_daily_food_unit", root_unit_term.access_group_uid)]

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    service_provider.access_group_service = AccessGroupService(service_provider)
    await service_provider.access_group_service.init()

    default_eaternity_access_group = (
        await service_provider.access_group_service.get_all_access_group_ids_by_namespace(
            PgSettings().EATERNITY_NAMESPACE_UUID
        )
    )[0]

    diet_low_in_vegetables_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "EOS_Diet-low-in-vegetables", default_eaternity_access_group.uid
    )

    diet_high_in_red_meat_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "EOS_Diet-high-in-red-meat", default_eaternity_access_group.uid
    )

    diet_low_in_whole_grain = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "EOS_Diet-low-in-whole-grains", default_eaternity_access_group.uid
    )

    node_uid = uuid.uuid4()
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    flow_node_uid = uuid.uuid4()
    parent_flow_node = FoodProductFlowNode(uid=flow_node_uid)

    root_node_uid = uuid.uuid4()
    root_node = FoodProcessingActivityNode(uid=root_node_uid)

    calculation = Calculation(
        requested_quantity_references=[
            ReferenceAmountEnum.amount_for_100g,
            ReferenceAmountEnum.amount_for_root_node,
            ReferenceAmountEnum.amount_for_activity_production_amount,
        ]
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider, root_node_uid=node_uid, glossary_service=glossary_service, calculation=calculation
    )
    calc_graph.add_node(child_of_root_node)
    calc_graph.add_node(root_node)
    calc_graph.add_node(parent_flow_node)

    calc_graph.add_edge(root_node.uid, parent_flow_node.uid)
    calc_graph.add_edge(parent_flow_node.uid, child_of_root_node.uid)

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=root_node_uid,
            prop_name="impact_assessment_supply_for_root",
            prop=QuantityProp(
                value=0.5,
                unit_term_uid=production_amount_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_root_node,
            ),
        )
    )

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="amount",
            prop=QuantityProp(
                value=10.0,
                unit_term_uid=gram_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = VitascoreGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)
    gap_filling_worker_flow = gap_filling_factory.spawn_worker(parent_flow_node)

    assert not gap_filling_worker.should_be_scheduled()

    assert gap_filling_worker_flow.should_be_scheduled()
    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    nutr_data: dict = {}
    for nutr, nutr_val in {
        nutr_name_to_term["protein"]: 20,
        nutr_name_to_term["fat"]: 10,
    }.items():
        nutr_data[nutr] = ReferencelessQuantityProp(value=nutr_val, unit_term_uid=gram_term.uid)

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities=nutr_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="daily_food_unit",
            prop=QuantityProp(
                value=1.0,
                unit_term_uid=dfu_unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    nutr_data[nutr_name_to_term["energy"]] = ReferencelessQuantityProp(value=2000, unit_term_uid=kcal_term.uid)
    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities=nutr_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="food_categories",
            prop=QuantityPackageProp(
                quantities={
                    diet_low_in_vegetables_term.uid: ReferencelessQuantityProp(value=400, unit_term_uid=gram_term.uid)
                },
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities={
                    diet_low_in_vegetables_term.uid: ReferencelessQuantityProp(value=100, unit_term_uid=kcal_term.uid)
                },
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities={
                    nutr_name_to_term["energy"]: ReferencelessQuantityProp(value=2000, unit_term_uid=kcal_term.uid)
                },
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="daily_food_unit",
            prop=QuantityProp(
                value=1.0,
                unit_term_uid=dfu_unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    food_category_data: dict = {
        diet_low_in_vegetables_term.uid: ReferencelessQuantityProp(value=400, unit_term_uid=gram_term.uid),
        diet_low_in_whole_grain.uid: ReferencelessQuantityProp(value=20, unit_term_uid=gram_term.uid),
        diet_high_in_red_meat_term.uid: ReferencelessQuantityProp(value=22, unit_term_uid=gram_term.uid),
    }
    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="food_categories",
            prop=QuantityPackageProp(
                quantities=food_category_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities={
                    nutr_name_to_term["energy"]: ReferencelessQuantityProp(value=1000, unit_term_uid=kcal_term.uid),
                },
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="daily_food_unit",
            prop=QuantityProp(
                value=0.5,
                unit_term_uid=dfu_unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    del food_category_data[diet_low_in_vegetables_term.uid]
    food_category_data[diet_low_in_whole_grain.uid] = ReferencelessQuantityProp(value=10, unit_term_uid=gram_term.uid)
    food_category_data[diet_high_in_red_meat_term.uid] = ReferencelessQuantityProp(
        value=11, unit_term_uid=gram_term.uid
    )
    await calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="food_categories",
            prop=QuantityPackageProp(
                quantities=food_category_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    # Obtained in a separate calculation
    known_vitascore_flow_root = 111.3148991013072
    known_vitascore_flow_root_legacy = 294.7784693627451
    known_vitascore_flow_local = 164.49111111111114
    known_vitascore_flow_local_legacy = 411.4088888888889

    known_milkscore_flow_root = 6.087058823529412
    known_milkscore_flow_root_legacy = 6.048823529411765
    known_milkscore_flow_local = 7.96
    known_milkscore_flow_local_legacy = 7.91

    await gap_filling_worker_flow.run(calc_graph)
    vitascore_flow = parent_flow_node.vitascore
    vitascore_legacy_flow = parent_flow_node.vitascore_legacy

    # Vitascore
    assert is_close(
        known_vitascore_flow_root,
        vitascore_flow.quantities.get(ReferenceAmountEnum.amount_for_root_node).get(vitascore_char_term.uid).value,
    )
    assert is_close(
        known_vitascore_flow_local,
        vitascore_flow.quantities.get(ReferenceAmountEnum.amount_for_activity_production_amount)
        .get(vitascore_char_term.uid)
        .value,
    )
    assert is_close(
        known_vitascore_flow_local_legacy,
        vitascore_legacy_flow.quantities.get(ReferenceAmountEnum.amount_for_activity_production_amount)
        .get(vitascore_char_term.uid)
        .value,
    )
    assert is_close(
        known_vitascore_flow_root_legacy,
        vitascore_legacy_flow.quantities.get(ReferenceAmountEnum.amount_for_root_node)
        .get(vitascore_char_term.uid)
        .value,
    )

    # Milkscore
    assert is_close(
        known_milkscore_flow_root,
        vitascore_flow.quantities.get(ReferenceAmountEnum.amount_for_root_node).get(food_cat_to_term[MILK_XID]).value,
    )
    assert is_close(
        known_milkscore_flow_local,
        vitascore_flow.quantities.get(ReferenceAmountEnum.amount_for_activity_production_amount)
        .get(food_cat_to_term[MILK_XID])
        .value,
    )
    assert is_close(
        known_milkscore_flow_local_legacy,
        vitascore_legacy_flow.quantities.get(ReferenceAmountEnum.amount_for_activity_production_amount)
        .get(food_cat_to_term[MILK_XID])
        .value,
    )
    assert is_close(
        known_milkscore_flow_root_legacy,
        vitascore_legacy_flow.quantities.get(ReferenceAmountEnum.amount_for_root_node)
        .get(food_cat_to_term[MILK_XID])
        .value,
    )

    # Serializaion / Deserialization
    flow_food_categories_serialized = parent_flow_node.food_categories.model_dump()

    parent_flow_amount_for_root_node = parent_flow_node.food_categories.amount_for_root_node().quantities

    for key, val in parent_flow_amount_for_root_node.items():
        assert is_close(
            flow_food_categories_serialized[ReferenceAmountEnum.amount_for_root_node][str(key)]["quantity"]["value"],
            val.value,
        )

    assert parent_flow_node.food_categories.for_reference == ReferenceAmountEnum.amount_for_activity_production_amount
    deserialized_food_categories = QuantityPackageProp(**flow_food_categories_serialized)
    assert deserialized_food_categories.for_reference == ReferenceAmountEnum.amount_for_100g

    for key, val in parent_flow_node.food_categories.amount_for_100g().quantities.items():
        assert is_close(
            deserialized_food_categories.quantities[key].value,
            val.value,
        )
