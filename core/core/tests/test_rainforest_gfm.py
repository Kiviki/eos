import uuid
from typing import Tuple

import pytest

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.term import Term
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import is_close, prepend_flow_node
from core.tests.test_ingredient_amount_estimator_gfm import nutrients_mixer
from database.postgres.postgres_db import PostgresDb

CARROT_AMOUNT = 0.7398682383665325
ONION_AMOUNT = 1 - CARROT_AMOUNT

DRIED_CARROT_RATIO = 0.010804165522511348
DRIED_ONION_RATIO = 0.0012654643492673225

CARROT_UPSCALE = 5.070993914807302
ONION_UPSCALE = 5.025125628140706

SCALED_CARROT_AMOUNT_PER_100G = 100 * CARROT_AMOUNT * (DRIED_CARROT_RATIO * CARROT_UPSCALE + (1 - DRIED_CARROT_RATIO))
SCALED_ONION_AMOUNT_PER_100G = 100 * ONION_AMOUNT * (DRIED_ONION_RATIO * ONION_UPSCALE + (1 - DRIED_ONION_RATIO))


def get_base_terms(
    service_provider: ServiceProvider,
) -> Tuple[Term, Term, Term, Term, Term]:
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    rainforest_conservation_certified = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_rainforest_conservation_certified", access_group_uid)
    ]
    certified_rainforest_not_specified = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_certified_rainforest_not_specified", access_group_uid)
    ]
    not_certified_for_rainforest = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_rainforest", access_group_uid)
    ]
    carrot_critical_amount = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("CriticalProduct_sample_1", access_group_uid)
    ]
    onion_critical_amount = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("CriticalProduct_sample_2", access_group_uid)
    ]
    return (
        rainforest_conservation_certified,
        certified_rainforest_not_specified,
        not_certified_for_rainforest,
        carrot_critical_amount,
        onion_critical_amount,
    )


async def _run_calculation(
    services: Tuple[PostgresDb, ServiceProvider],
    nutrient_values: dict[str, float],
    carrot_label: dict | None,
    onion_label: dict | None,
    unknown_origin: bool = False,
    carrot_amount: float | None = None,
    onion_amount: float | None = None,
) -> Tuple[Node, PostgresDb, ServiceProvider]:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[
                {
                    "language": "en",
                    "value": "A sample recipe with 2 ingredients, to test the ingredient estimator",
                },
            ],
            activity_location="Switzerland" if unknown_origin else None,
        )
    )

    # create first ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            flow_location="ES" if not unknown_origin else None,
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": carrot_amount, "unit": "kilogram"} if carrot_amount else None,
            raw_labels=carrot_label,
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create second ingredient
    onion_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            flow_location="CN" if not unknown_origin else None,
            product_name=[{"language": "de", "value": "Zwiebeln"}],
            amount_in_original_source_unit={"value": onion_amount, "unit": "kilogram"} if onion_amount else None,
            raw_labels=onion_label,
        )
    )

    # add edge recipe --> onion_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=onion_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    root_flow = await prepend_flow_node(
        graph_mgr, recipe.uid, nutrient_values=nutrient_values if not (carrot_amount and onion_amount) else None
    )
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    return root_node, postgres_db, service_provider


@pytest.mark.asyncio
async def test_rainforest_gfm_known_origin_rating_2(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    mix_70_30_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.7, 0.3))

    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        mix_70_30_carrot_potato,
        {"value": "Bio Suisse", "language": "fr"},
        {"value": "Migros Bio Ausland", "language": "de"},
    )

    (
        rainforest_conservation_certified,
        certified_rainforest_not_specified,
        not_certified_for_rainforest,
        carrot_critical_amount,
        onion_critical_amount,
    ) = get_base_terms(service_provider)

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g()
        .quantities.get(rainforest_conservation_certified.uid)
        .value,
        carrot_critical_amount.data.get("palm-oil") * SCALED_CARROT_AMOUNT_PER_100G / 100,
    )

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g()
        .quantities.get(certified_rainforest_not_specified.uid)
        .value,
        (onion_critical_amount.data.get("palm-oil") + onion_critical_amount.data.get("soy"))
        * SCALED_ONION_AMOUNT_PER_100G
        / 100,
    )

    assert root_node.rainforest_critical_products.get_rainforest_rating() == 2


@pytest.mark.asyncio
async def test_rainforest_gfm_known_origin_rating_1(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    mix_70_30_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.7, 0.3))

    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        mix_70_30_carrot_potato,
        None,
        {"value": "Migros Bio Ausland", "language": "de"},
    )

    (
        rainforest_conservation_certified,
        certified_rainforest_not_specified,
        not_certified_for_rainforest,
        carrot_critical_amount,
        onion_critical_amount,
    ) = get_base_terms(service_provider)

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g().quantities.get(not_certified_for_rainforest.uid).value,
        carrot_critical_amount.data.get("palm-oil") * SCALED_CARROT_AMOUNT_PER_100G / 100,
    )

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g()
        .quantities.get(certified_rainforest_not_specified.uid)
        .value,
        (onion_critical_amount.data.get("palm-oil") + onion_critical_amount.data.get("soy"))
        * SCALED_ONION_AMOUNT_PER_100G
        / 100,
    )

    assert root_node.rainforest_critical_products.get_rainforest_rating() == 1


@pytest.mark.asyncio
async def test_rainforest_gfm_known_origin_rating_3_due_to_small_amounts(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    mix_70_30_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.7, 0.3))

    carrot_amount = 0.025
    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        mix_70_30_carrot_potato,
        carrot_label=None,
        onion_label={"value": "Bio Suisse", "language": "fr"},
        carrot_amount=carrot_amount,
        onion_amount=1 - carrot_amount,
    )

    (
        rainforest_conservation_certified,
        certified_rainforest_not_specified,
        not_certified_for_rainforest,
        carrot_critical_amount,
        onion_critical_amount,
    ) = get_base_terms(service_provider)

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g().quantities.get(not_certified_for_rainforest.uid).value,
        carrot_critical_amount.data.get("palm-oil") * carrot_amount,
    )

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g()
        .quantities.get(rainforest_conservation_certified.uid)
        .value,
        (onion_critical_amount.data.get("palm-oil") + onion_critical_amount.data.get("soy")) * (1 - carrot_amount),
    )

    assert root_node.rainforest_critical_products.get_rainforest_rating() == 3


@pytest.mark.asyncio
async def test_rainforest_gfm_unknown_origin_rating_1(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    mix_70_30_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.7, 0.3))

    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        mix_70_30_carrot_potato,
        None,
        None,
        unknown_origin=True,
    )

    (
        rainforest_conservation_certified,
        certified_rainforest_not_specified,
        not_certified_for_rainforest,
        carrot_critical_amount,
        onion_critical_amount,
    ) = get_base_terms(service_provider)

    assert is_close(
        root_node.rainforest_critical_products.amount_for_100g().quantities.get(not_certified_for_rainforest.uid).value,
        (carrot_critical_amount.data.get("palm-oil") + carrot_critical_amount.data.get("soy"))
        * SCALED_CARROT_AMOUNT_PER_100G
        / 100
        + (onion_critical_amount.data.get("palm-oil") + onion_critical_amount.data.get("soy"))
        * SCALED_ONION_AMOUNT_PER_100G
        / 100,
    )

    assert root_node.rainforest_critical_products.get_rainforest_rating() == 1
