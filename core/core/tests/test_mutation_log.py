"Tests for the mutation log."

import pytest
from structlog import get_logger

from core.graph_manager.calc_graph import CalcGraph
from core.tests.conftest import ElaborateRecipe, MinimumRecipeWithDeclaration
from core.tests.test_orchestration_gfm import run_orchestrator_on_recipe

logger = get_logger()


@pytest.mark.asyncio
async def test_basic_recipe_replay(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    "Tests if orchestration run on minimum recipe stores all modifications of the calculation graph in mutation logs."
    pg_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    calc_graph, calculation = await run_orchestrator_on_recipe(
        service_provider, pg_db, root_flow, recipe, return_log=True
    )

    # Create a new empty graph:
    replay_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )

    # Rerun the saved mutation log on a new graph:
    for mutation in calculation.mutations:
        # The next line has the effect to delete/reset child and parent nodes from the recipe and other nodes.
        # Else it will violate the conditions in Node.add_parent_node() and Node.add_child_node().
        serialized_mut = mutation.as_dict()
        mutation_from_dict = mutation.from_dict(serialized_mut)
        await replay_graph.apply_mutation(mutation_from_dict)

    # check if both graphs are the same:
    await check_graph_equality(calc_graph, replay_graph)


@pytest.mark.asyncio
async def test_basic_recipe_replay_of_saved_log(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests if the saved mutation logs in the database are correctly loaded again.

    By checking if the resulting graph is the same as the original.
    """
    pg_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    calc_graph, calculation = await run_orchestrator_on_recipe(
        service_provider, pg_db, root_flow, recipe, return_log=True
    )

    reloaded_calculation = await pg_db.get_calc_mgr().find_by_uid(calculation.uid)

    # Create a new empty graph:
    replay_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )

    # Rerun the saved mutation log on a new graph:
    for mutation in reloaded_calculation.mutations:
        await replay_graph.apply_mutation(mutation)

    # check if both graphs are the same:
    await check_graph_equality(calc_graph, replay_graph)


@pytest.mark.asyncio
async def test_more_elaborate_recipe_replay(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Tests if the orchestration-run stores all modification of the calculation graph in mutation logs."""
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    calc_graph, calculation = await run_orchestrator_on_recipe(
        er.service_provider, er.pg_db, er.root_flow, er.recipe, return_log=True
    )

    # Create a new empty graph:
    replay_graph = CalcGraph(
        er.service_provider,
        root_node_uid=er.root_flow.uid,
        glossary_service=glossary_service,
    )

    # Rerun the saved mutation log on a new graph:
    for mutation in calculation.mutations:
        # The next line has the effect to delete/reset child and parent nodes from the recipe and other nodes.
        # Else it will violate the conditions in Node.add_parent_node() and Node.add_child_node().
        mutation_from_dict = mutation.from_dict(mutation.as_dict())
        await replay_graph.apply_mutation(mutation_from_dict)

    # check if both graphs are the same:
    await check_graph_equality(calc_graph, replay_graph)


@pytest.mark.asyncio
async def test_more_elaborate_recipe_replay_of_saved_log(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Tests if the saved mutation logs in the database are correctly loaded again.

    By checking if the resulting graph is the same as the original.
    """
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    calc_graph, calculation = await run_orchestrator_on_recipe(
        er.service_provider, er.pg_db, er.root_flow, er.recipe, return_log=True
    )

    reloaded_calculation = await er.pg_db.get_calc_mgr().find_by_uid(calculation.uid)

    # Create a new empty graph:
    replay_graph = CalcGraph(
        er.service_provider,
        root_node_uid=er.root_flow.uid,
        glossary_service=glossary_service,
    )

    # Rerun the saved mutation log on a new graph:
    for mutation in reloaded_calculation.mutations:
        await replay_graph.apply_mutation(mutation)

    # check if both graphs are the same:
    await check_graph_equality(calc_graph, replay_graph)


async def check_graph_equality(calc_graph: CalcGraph, replay_graph: CalcGraph) -> None:
    "Verify that both graphs are equal."
    # check if both graphs have the same number of nodes:
    assert len(calc_graph._nodes_by_uid) == len(replay_graph._nodes_by_uid)  # noqa

    # check if node uid's are the same:
    source_keys = sorted(calc_graph.get_node_uids())
    replay_keys = sorted(replay_graph.get_node_uids())
    assert source_keys == replay_keys, "node ids should be the same"

    # check if all node properties are the same:
    for node_uid in source_keys:
        calc_graph_node = calc_graph.get_node_by_uid(node_uid)
        replay_graph_node = replay_graph.get_node_by_uid(node_uid)
        assert calc_graph_node == replay_graph_node
