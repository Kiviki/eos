"Test Matrix calculation and impact assessment."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.impact_assessment_gfm import ImpactAssessmentGapFillingFactory
from gap_filling_modules.matrix_calculation_gfm import MatrixCalculationGapFillingFactory
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes import ElementaryResourceEmissionNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props.gfm_state_prop import GfmStateProp
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import GLOBAL_GFM_STATE_PROP_NAME, Orchestrator
from core.service.service_provider import ServiceProvider
from core.tests.conftest import ElaborateRecipe, MinimumRecipeRType, is_close
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


@pytest.mark.asyncio
async def test_matrix_calculation_and_impact_assessment_gfm(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    root_char_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=er.root_flow.uid,
        return_log=False,
    )

    # create the calculation graph
    calc_graph = CalcGraph(
        er.service_provider,
        root_node_uid=calculation.root_node_uid,
        glossary_service=glossary_service,
    )

    # instead of running each GFM that is necessary to be run before matrix_calculation GFM,
    # we explicitly call an orchestrator to run them all at once, including matrix_calculation GFM

    # initialize orchestrator with the calculation graph and skip unnecessary GFMs
    logger.debug("creating the orchestrator...")
    loader = GapFillingModuleLoader(
        blacklist=[
            "gap_filling_modules.ingredient_splitter_gfm",
            # 'gap_filling_modules.impact_assessment_gfm',   # later this one will be tested individually again
            "gap_filling_modules.location_gfm",
        ]
    )
    await loader.init(er.service_provider)

    orchestrator = Orchestrator(
        calc_graph=calc_graph, glossary_service=glossary_service, gap_filling_module_loader=loader
    )

    await orchestrator.run()

    flow_quantities: dict[uuid.UUID, float] = calc_graph.child_of_root_node().environmental_flows.flow_quantities
    assert is_close(flow_quantities[er.co2_process.uid], 120)
    assert (
        calc_graph.child_of_root_node()
        .environmental_flows.emission_node_uid_to_production_amount_unit(er.co2_process.uid, calc_graph=calc_graph)
        .name.lower()
        == "kilogram"
    )
    assert is_close(flow_quantities[er.sulfur_process.uid], 14)
    assert is_close(flow_quantities[er.crude_oil_process.uid], -100)

    # checking that impact_assessment GFM has run correctly
    assert is_close(
        calc_graph.child_of_root_node()
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )
    assert is_close(
        calc_graph.child_of_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )

    # testing impact_assessment GFM explicitly

    # this GFM has actually been run before in the previous calculation run --
    # here we're checking whether everything works on the second run,
    # which shouldn't happen at all
    gap_filling_factory = ImpactAssessmentGapFillingFactory(er.pg_db, er.service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(calc_graph.child_of_root_node())
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking that impact_assessment GFM has run correctly
    assert is_close(
        calc_graph.child_of_root_node()
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )
    assert is_close(
        calc_graph.child_of_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )


@pytest.mark.asyncio
async def test_matrix_calculation_failure(
    create_more_elaborate_recipe_without_lca_process: ElaborateRecipe,
) -> None:
    er = create_more_elaborate_recipe_without_lca_process
    glossary_service = er.service_provider.glossary_service

    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=er.root_flow.uid,
        return_log=False,
    )

    # create the calculation graph
    calc_graph = CalcGraph(
        er.service_provider,
        root_node_uid=calculation.root_node_uid,
        glossary_service=glossary_service,
    )

    # instead of running each GFM that is necessary to be run before matrix_calculation GFM,
    # we explicitly call an orchestrator to run them all at once, including matrix_calculation GFM

    # initialize orchestrator with the calculation graph and skip unnecessary GFMs
    logger.debug("creating the orchestrator...")
    loader = GapFillingModuleLoader(
        blacklist=[
            "gap_filling_modules.ingredient_splitter_gfm",
            "gap_filling_modules.impact_assessment_gfm",
            "gap_filling_modules.location_gfm",
        ]
    )
    await loader.init(er.service_provider)

    orchestrator = Orchestrator(
        calc_graph=calc_graph, glossary_service=glossary_service, gap_filling_module_loader=loader
    )

    await orchestrator.run()
    assert calc_graph.get_root_node().matrix_gfm_error.value.startswith("Error: ")


@pytest.mark.asyncio
async def test_matrix_calculation_with_product_node_type(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service

    node_uid = uuid.uuid4()
    root_flow = FoodProductFlowNode(uid=node_uid)
    product_node = FoodProcessingActivityNode(uid=uuid.uuid4())

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_flow)
    calc_graph.add_node(product_node)
    calc_graph.add_edge(root_flow.uid, product_node.uid)

    gap_filling_factory = MatrixCalculationGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(root_flow)

    assert gap_filling_worker.should_be_scheduled(), "matrix calculation should be able to run on root_flow"


@pytest.mark.asyncio
async def test_matrix_calculation_gfm_with_no_sub_nodes_recipe(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, recipe, _, _, root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_flow)

    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    gap_filling_factory = MatrixCalculationGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(calc_graph.get_root_node())

    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready, (
        "should be ready to run because there " "are no sub-nodes and no ingredient declarations"
    )


@pytest.mark.asyncio
async def test_matrix_calculation_gfm_without_other_gfms(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, ingredient, _, root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        calculation=Calculation(root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid),
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_flow)

    # adding an ingredient sub-node
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_flow.uid, recipe.uid)
    calc_graph.add_node(ingredient)
    calc_graph.add_edge(recipe.uid, ingredient.uid)

    gap_filling_factory = MatrixCalculationGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(calc_graph.get_root_node())

    assert gap_filling_worker.should_be_scheduled()

    calc_graph.get_root_node().__apply_mutation__(
        GLOBAL_GFM_STATE_PROP_NAME,
        GfmStateProp(
            worker_states={
                UnitWeightConversionGapFillingWorker.__name__: NodeGfmStateEnum.scheduled.value,
            }
        ),
    )
    calc_graph.calculation.global_gfm_state = (
        calc_graph.get_root_node().global_gfm_state
    )  # This is usually done in orchestrator.
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.reschedule

    calc_graph.get_root_node().__apply_mutation__(
        GLOBAL_GFM_STATE_PROP_NAME,
        GfmStateProp(
            worker_states={
                AddClientNodesGapFillingWorker.__name__: NodeGfmStateEnum.scheduled.value,
                UnitWeightConversionGapFillingWorker.__name__: NodeGfmStateEnum.scheduled.value,
            }
        ),
    )
    calc_graph.calculation.global_gfm_state = (
        calc_graph.get_root_node().global_gfm_state
    )  # This is usually done in orchestrator.
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.reschedule

    calc_graph.get_root_node().__apply_mutation__(
        GLOBAL_GFM_STATE_PROP_NAME,
        GfmStateProp(worker_states={AddClientNodesGapFillingWorker.__name__: NodeGfmStateEnum.scheduled.value}),
    )
    calc_graph.calculation.global_gfm_state = (
        calc_graph.get_root_node().global_gfm_state
    )  # This is usually done in orchestrator.
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.reschedule


@pytest.mark.asyncio
async def test_impact_assessment_gfm_with_wrong_node_type(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service

    # create calculation graph
    node_uid = uuid.uuid4()
    root_node = FoodProductFlowNode(uid=node_uid)

    emission_node = ElementaryResourceEmissionNode(uid=uuid.uuid4())

    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_node)
    calc_graph.add_node(emission_node)
    calc_graph.add_edge(root_node.uid, emission_node.uid)

    gap_filling_factory = ImpactAssessmentGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(calc_graph.get_root_node())

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_impact_assessment_gfm_rescheduled_without_matrix_calculation_gfm(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, recipe, _, _, root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_flow)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    gap_filling_factory = ImpactAssessmentGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(calc_graph.child_of_root_node())

    assert gap_filling_worker.should_be_scheduled()
    # we expect it to cancel itself, because there are no environmental flows present:
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.cancel
