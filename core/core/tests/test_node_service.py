"Test Node Service."
import asyncio
from typing import Tuple

import pytest
import pytest_asyncio
import structlog

from core.domain.nodes.node import Node
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.service.messaging_service import MessagingService
from core.service.node_service import NodeService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeForUnitWeightGFM, MinimumRecipeWithTwoOrigins, is_close
from database.postgres.postgres_db import PostgresDb

logger = structlog.get_logger()


@pytest_asyncio.fixture
async def setup_messaging_service(
    setup_services: Tuple[PostgresDb, ServiceProvider],
) -> None:
    """Fixture to get a clean (resetted) db for testing."""
    _, service_provider = setup_services

    # add messaging_service to service_provider:
    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()

    # reinit so that node service is using the messaging_service:
    await service_provider.node_service.init()

    # yield the messaging_service so that the test can use it:
    yield service_provider.messaging_service

    # teardown after test is complete:
    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None


@pytest.mark.asyncio
async def test_number_lca_nodes_in_cache(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Test node service synchronization."""
    (
        postgres_db,
        service_provider,
        _,
        lca_process,
        _root_flow,
    ) = create_minimum_recipe_with_small_declaration_and_two_origins

    # we need to initialize the node_service again so that it fills the cache after the minimum_recipe was created:
    service_provider.node_service.clear_cache()
    await service_provider.node_service.ensure_cache_is_loaded()

    # check if we can get the lca_process out of the node_service1 cache:
    assert len(service_provider.node_service.cache) == 31, (
        "There should be 31 nodes in the cache. "
        "6 brightway_process from tractor_dummy_lca, "
        "1 emissions from tractor_dummy_lca, "
        "1 intermediary_flow from tractor_dummy_lca, "
        "10 elementary_flows from tractor_dummy_lca, "
        "11 brightway_process from seed_glossary_links, "
        "1 emissions from seed_glossary_links, "
        "1 elementary_flows from seed_glossary_links."
    )


@pytest.mark.asyncio
async def test_node_service_cache_invalidation(
    create_minimum_recipe: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
    setup_messaging_service: MessagingService,  # noqa: ARG001
) -> Node:
    """Test node service synchronization."""
    postgres_db, service_provider, _, _, lca_process, _root_node = create_minimum_recipe
    _msg_service = setup_messaging_service

    # create 2 instances of NodeService to test if caches are synchronized:
    node_service1 = NodeService(service_provider)
    node_service2 = NodeService(service_provider)
    await node_service1.init()
    await node_service2.init()

    # check if we can get the lca_process out of the node_service1 cache:
    retrieved_node = await node_service1.find_by_uid(lca_process.uid)
    assert retrieved_node is not None
    assert retrieved_node.uid == lca_process.uid

    root_unit_term = service_provider.glossary_service.root_subterms.get("EOS_units")

    gram_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "EOS_gram",
        str(root_unit_term.access_group_uid),
    )

    # Changed to product_name from is_dried, which will be eventually removed.
    retrieved_node.production_amount = QuantityProp(
        value=789.0, unit_term_uid=gram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    await node_service1.upsert_node_by_uid(retrieved_node)

    # now we check if the node_service2 cache is synchronized by retrieving that updated property from a node from it:
    received_prop_data = None
    for i in range(100):
        await asyncio.sleep(0.1)

        retrieved_node2 = await node_service2.find_by_uid(lca_process.uid)
        if retrieved_node2 is None:
            continue

        if retrieved_node2.production_amount:
            logger.debug(f"we received a node that contains the new property after {i/1000} seconds.")
            received_prop_data = retrieved_node2.production_amount
            break

    # check if we received the same property value that we had set using node_service1:
    assert received_prop_data is not None
    assert received_prop_data.get_unit_term() == gram_term
    assert is_close(received_prop_data.value, 789.0)
    assert received_prop_data.for_reference == ReferenceAmountEnum.self_reference


@pytest.mark.asyncio
async def test_simultaneous_upsert_and_read_of_recipe(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
) -> None:
    """Test if we can simultaneously upsert many recipes."""
    (
        pg_db,
        service_provider,
        recipe,
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
        root_flow,
    ) = create_minimum_recipe_for_unit_weight_gfm

    recipe.add_sub_node(apple_juice_ingredient)
    recipe.add_sub_node(orange_juice_ingredient)
    recipe.add_sub_node(abstract_orange_juice_ingredient)
    recipe.add_sub_node(piece_ingredient)

    node_service = NodeService(service_provider)
    await node_service.init()

    logger.info("start tasks to check if simultaneous upserting of recipes would result in deadlock.")
    tasks = []
    for _task_idx in range(300):
        # We add alternately 2 tasks that upsert and read the number of sub nodes.
        # These will then execute simultaneously. This tests for two different types of issues:
        # 1) If there are deadlocks between simultaneous writes.
        # 2) If a simultaneous write & read of the same recipe could result in an incomplete recipe when reading.
        tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe)))
        tasks.append(asyncio.create_task(check_number_of_sub_nodes(node_service, recipe, 4)))

    await asyncio.gather(*tasks)
    logger.info("finished all tasks to reproduce deadlock.")


async def check_number_of_sub_nodes(node_service: NodeService, recipe: Node, expected_num: int) -> None:
    edges = await node_service.get_sub_graph_by_uid(recipe.uid, max_depth=2)
    assert len(edges) == expected_num, f"There should be {expected_num} edge for the recipe node itself."


async def upsert_by_recipe_id(node_service: NodeService, recipe: Node) -> None:
    await node_service.upsert_by_recipe_id(recipe)
