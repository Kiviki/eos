"Tests for product name matching gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum

# Has to run AttachFoodTagsGapFillingWorker now since we attach nutrient values there (because nutrient values
# need some additional terms coming from glossary tags).
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory

from core.domain.nodes import FoodProcessingActivityNode
from core.domain.nodes.flow.food_product_flow import FoodProductFlowNode
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_match_product_name_gfm(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    product_name_prop = carrot_ingredient.product_name
    assert len(product_name_prop.terms) == 1

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "A1791"  # Karotten

    nutrients_prop = carrot_ingredient.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "131"  # nutrients for Karotten


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_wrong_node_type(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    # default NodeType is 'recipe'
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(child_of_root_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_no_raw_input(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_no_names_field(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        transport="air",
        flow_location="spain",
        amount_in_original_source_unit={"value": 150, "unit": "gram"},
        id="100100191",
        type="conceptual-ingredients",
        processing="raw",
        raw_conservation={"value": "fresh", "language": "en"},
        packaging="plastic",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_empty_names_field(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        transport="air",
        flow_location="spain",
        product_name=[
            {
                "language": "de",
                "value": "",  # <-- empty names field
            }
        ],
        amount_in_original_source_unit={"value": 150, "unit": "gram"},
        id="100100191",
        type="conceptual-ingredients",
        processing="raw",
        raw_conservation={"value": "fresh", "language": "en"},
        packaging="plastic",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    product_name_prop = ingredient_node.product_name
    assert len(product_name_prop.terms) == 1

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "EOS_data_error"

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    assert len(calc_graph.get_data_errors_log()) == 2
    assert calc_graph.get_data_errors_log()[0].startswith("Empty name values!")
    assert calc_graph.get_data_errors_log()[1].startswith("Could not load nutrition values")

    assert ingredient_node.nutrient_values is None


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_lowercase_names_field(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    product_name_prop = ingredient_node.product_name
    prop_term = product_name_prop.terms[0].get_term()
    assert prop_term.xid == "A00ZT"  # FoodEx2 xid for POTATO

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    nutrients_prop = ingredient_node.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "508"  # nutrients for potato


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_composed_name(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Matching for dried potatoes (trockene Kartoffeln)."""
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "getrockene kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    product_name_props = ingredient_node.product_name
    assert len(product_name_props.terms) == 2, "Two terms should be matched: POTATO and DEHYDRATED OR DRIED"
    prop_term_xids = [p.get_term().xid for p in product_name_props.terms]
    assert prop_term_xids == ["A00ZT", "J0116"], "FoodEx2 xid for POTATO and DEHYDRATED OR DRIED"

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    nutrients_prop = ingredient_node.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "508"  # nutrients for Karotten
