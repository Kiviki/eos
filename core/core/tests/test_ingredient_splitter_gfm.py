"Tests for ingredient splitter gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingFactory, IngredientSplitterParser
from structlog import get_logger

from core.domain.ingredients_declaration import IngredientsDeclarationMapping
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeWithDeclaration
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


def get_indexes_sequence_of_ingredients_in_declaration(ingredients_list: list) -> list:
    result_list = []

    for ingredient in ingredients_list:
        result_list.append(ingredients_list.index(ingredient))

        if isinstance(ingredient.get("subproducts"), list):
            result_list.extend(get_indexes_sequence_of_ingredients_in_declaration(ingredient.get("subproducts")))

    return result_list


def get_indexes_sequence_of_ingredients_in_mutation_log(mutation_log: list) -> list:
    mutation_log_ingredient_indexes_list = []

    for mutation in mutation_log:
        if isinstance(mutation.new_node, FoodProductFlowNode):
            mutation_log_ingredient_indexes_list.append(mutation.new_node.declaration_index.value)

    return mutation_log_ingredient_indexes_list


def check_mutation_log_ingredient_indexes(ingredients_declaration: str, mutation_log: list) -> None:
    parsed_ingredients_list = IngredientSplitterParser().parse(ingredients_declaration)
    parsed_ingredient_indexes_list = get_indexes_sequence_of_ingredients_in_declaration(parsed_ingredients_list)

    mutation_log_ingredient_indexes_list = get_indexes_sequence_of_ingredients_in_mutation_log(mutation_log)

    assert parsed_ingredient_indexes_list == mutation_log_ingredient_indexes_list


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm(create_minimum_recipe_with_declaration: MinimumRecipeWithDeclaration) -> None:
    postgres_db, service_provider, recipe, root_flow = create_minimum_recipe_with_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    # running the GFM
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    assert len(calc_graph.get_mutation_log()) == 35, (
        f"There should be 35 nodes attached to node {recipe}"
        f"after the ingredient splitter has been run "
        f"instead of {len(calc_graph.get_mutation_log())}."
    )

    # checking if the order of ingredients in the mutation log matches the order in the declaration
    check_mutation_log_ingredient_indexes(
        recipe.ingredients_declaration[0].get("value"),
        calc_graph.get_mutation_log(),
    )


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_wrong_ingredients_declaration(
    create_minimum_recipe_with_wrong_ingredients_declaration: MinimumRecipeWithDeclaration,
) -> None:
    postgres_db, service_provider, recipe, _root_flow = create_minimum_recipe_with_wrong_ingredients_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    faulty_declaration = recipe.ingredients_declaration[0].get("value")

    fixed_declaration = (
        "Hähnchenbrustfilet (53 %), Panade (28%) "
        "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
        "Weizenstärke, Speisesalz, Gewürze, Hefe), "
        "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
        "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
        "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
        "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
        "Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, Zucker, "
        "Geschmacksverstärker: E 620"
    )

    declaration_object = IngredientsDeclarationMapping(
        faulty_declaration=faulty_declaration,
        fixed_declaration=fixed_declaration,
    )

    assert await postgres_db.get_ingredients_declaration_mapping_mgr().upsert_ingredients_declaration_mapping(
        declaration_object,
    ), "Failed to insert ingredients declaration."

    fixed_declaration = (
        "Hähnchenbrustfilet (53 %), Panade (28%) "
        "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
        "Weizenstärke, Speisesalz, Gewürze, Hefe), "
        "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
        "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
        "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
        "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
        "Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, Zucker, "
        "Gewürzextrakte, Geschmacksverstärker: E 620"
    )

    declaration_object = IngredientsDeclarationMapping(
        faulty_declaration=faulty_declaration,
        fixed_declaration=fixed_declaration,
    )

    assert await postgres_db.get_ingredients_declaration_mapping_mgr().upsert_ingredients_declaration_mapping(
        declaration_object,
    ), "Failed to update ingredients declaration."

    # running the GFM
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    assert len(calc_graph.get_mutation_log()) == 35, (
        f"Failed to attach ingredients to node {recipe} -- got "
        f"{len(calc_graph.get_mutation_log())} ingredients instead of 35."
    )

    # checking if the order of ingredients in the mutation log matches the order in the declaration
    check_mutation_log_ingredient_indexes(
        fixed_declaration,
        calc_graph.get_mutation_log(),
    )


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_no_raw_input(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(child_of_root_node)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)

    assert not gap_filling_worker.should_be_scheduled()


# the following two tests cannot be parametrized, hence why they are duplicated
# see https://github.com/pytest-dev/pytest-asyncio/issues/112


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_empty_ingredients_declaration(
    create_minimum_recipe_with_empty_fields: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
) -> None:
    """A variation of the first test.

    But now we test if an empty ingredients declaration
    does not cause this GFM to be scheduled.
    """
    postgres_db, service_provider, recipe, _, _root_flow = create_minimum_recipe_with_empty_fields
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert not gap_filling_worker.should_be_scheduled(), "GFM has been scheduled to run, while it should not be."


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_no_ingredients_declaration(
    create_minimum_recipe_with_no_fields: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
) -> None:
    """A variation of the first test.

    But now we test if absence of ingredients declaration
    does not cause this GFM to be scheduled.
    """
    postgres_db, service_provider, recipe, _, _root_flow = create_minimum_recipe_with_no_fields
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert not gap_filling_worker.should_be_scheduled(), "GFM has been scheduled to run, while it should not be."


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_two_foreign_one_german_ingredient_declarations(
    create_minimum_recipe_with_foreign_and_german_ingredients_declarations: MinimumRecipeWithDeclaration,
) -> None:
    (
        postgres_db,
        service_provider,
        recipe,
        _root_flow,
    ) = create_minimum_recipe_with_foreign_and_german_ingredients_declarations
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    # running the GFM
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    assert len(calc_graph.get_mutation_log()) == 35, (
        f"There should be 35 nodes attached to node {recipe}"
        f"after the ingredient splitter has been run "
        f"instead of {len(calc_graph.get_mutation_log())}."
    )

    # checking if the order of ingredients in the mutation log matches the order in the declaration
    check_mutation_log_ingredient_indexes(
        recipe.ingredients_declaration[2].get("value"),
        calc_graph.get_mutation_log(),
    )


@pytest.mark.asyncio
async def test_ingredient_splitter_gfm_with_two_foreign_ingredient_declarations(
    create_minimum_recipe_with_foreign_and_undefined_ingredients_declarations: Tuple[
        PostgresDb, ServiceProvider, Node, Node
    ],
) -> None:
    (
        postgres_db,
        service_provider,
        recipe,
        _root_flow,
    ) = create_minimum_recipe_with_foreign_and_undefined_ingredients_declarations
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)

    # running the GFM
    gap_filling_factory = IngredientSplitterGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    assert len(calc_graph.get_mutation_log()) == 35, (
        f"There should be 35 nodes attached to node {recipe}"
        f"after the ingredient splitter has been run "
        f"instead of {len(calc_graph.get_mutation_log())}."
    )

    # checking if the order of ingredients in the mutation log matches the order in the declaration
    check_mutation_log_ingredient_indexes(
        recipe.ingredients_declaration[0].get("value"),
        calc_graph.get_mutation_log(),
    )
