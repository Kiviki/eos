"Tests for location gap filling module."
import json
import os
from uuid import UUID

import pytest
from gap_filling_modules.location_gfm import (
    GapFillingWorkerStatusEnum,
    LocationGapFillingFactory,
    LocationProp,
    LocationQualifierEnum,
    LocationSourceEnum,
)
from pydantic import ValidationError

from core.graph_manager.calc_graph import CalcGraph
from core.tests.conftest import MinimumRecipeRType

# test that geolocation works with addresses or lat long values

# test that the correct property name is applied to the node?
# test that both cities and countries return the correct country code
# test that empty / unprovided locations 1) do not cause exceptions, 2) are mapped to unknown location
# test junk locations: is anything returned? Default location or what? Is Unknown qualifier correctly set?
# test coordinate -> location mapping
# test incomplete coordinates : correclty processed?
# test 0,0 coordinate?
# test blacklist?
# test multi-country strings, e.g. "ita/fr/China"
# test return unknown location?
# test return country name given iso code?


## test Location values & validation ##
def test_location_creation_on_valid_arguments() -> None:
    # should not raise
    LocationProp(
        address="This address should not raise",
        latitude=2.34,
        longitude=12.00,
        country_code="DE",  # does not have to match lat long
        location_qualifier=LocationQualifierEnum.known,
        source=LocationSourceEnum.default,
    )


def test_location_creation_raises_on_invalid_arguments() -> None:
    with pytest.raises(ValidationError):
        # noinspection PyTypeChecker
        LocationProp(
            address="This address should raise",
            latitude="string",  # should raise
            longitude=12.34,
            country_code="DE",  # does not have to match lat long
            location_qualifier=LocationQualifierEnum.known,
            source=LocationSourceEnum.default,
        )


@pytest.mark.asyncio
async def test_city_lookup(create_minimum_recipe: MinimumRecipeRType) -> None:
    (
        postgres_db,
        service_provider,
        recipe,
        carrot_ingredient,
        carrot_process,
        root_node,
    ) = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_node.uid, recipe.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 2

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == recipe.uid
    assert location_mutation.prop_name == "activity_location"
    assert location_mutation.props[0].country_code == "CH"
    assert location_mutation.props[0].get_term().xid == "CHE.26.13.21_1"
    assert isinstance(location_mutation.props[0].term_uid, UUID)

    assert mutation_log[1].node_uid == root_node.uid

    # checking if GFM external queries cache works correctly
    assert len(service_provider.gfm_cache_service._cache) == 1
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1
    assert (
        len(
            await service_provider.gfm_cache_service.get_cache_entry_by_gfm_name_and_key(
                "LocationGapFillingWorker",
                "Zürich Schweiz",
            )
        )
        == 2
    )


@pytest.mark.asyncio
async def test_country_lookup(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(recipe.uid, carrot_ingredient.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 1

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == carrot_ingredient.uid
    assert location_mutation.prop_name == "flow_location"
    assert location_mutation.props[0].country_code == "ES"
    assert location_mutation.props[0].get_term().xid == "ESP"

    # checking if GFM external queries cache works correctly
    assert len(service_provider.gfm_cache_service._cache) == 1
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1
    assert (
        len(
            await service_provider.gfm_cache_service.get_cache_entry_by_gfm_name_and_key(
                "LocationGapFillingWorker",
                "Spain",
            )
        )
        == 2
    )


@pytest.mark.asyncio
async def test_multi_country_lookup(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # testing with a multi country input
    carrot_ingredient.flow_location = "ES, NL"

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(recipe.uid, carrot_ingredient.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log[0].props) == 2

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == carrot_ingredient.uid
    assert location_mutation.prop_name == "flow_location"
    assert location_mutation.props[0].country_code == "ES"
    assert location_mutation.props[0].get_term().xid == "ESP"

    assert location_mutation.node_uid == carrot_ingredient.uid
    assert location_mutation.prop_name == "flow_location"
    assert location_mutation.props[1].country_code == "NL"
    assert location_mutation.props[1].get_term().xid == "NLD"


@pytest.mark.asyncio
async def test_geographic_region_lookup(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # testing with a multi country input
    carrot_ingredient.flow_location = "EU"

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(recipe.uid, carrot_ingredient.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 1

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == carrot_ingredient.uid
    assert location_mutation.prop_name == "flow_location"
    assert location_mutation.props[0].get_term().xid == "R0366"
    assert location_mutation.props[0].term_uid


@pytest.mark.asyncio
async def test_city_lookup_with_cached_query(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_node.uid, recipe.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    # add Google Maps query response to GFM queries cache
    sample_json_path = os.path.join(
        os.path.dirname(__file__),
        "sample_jsons",
        "zurich_google_maps_query_response.json",
    )

    with open(sample_json_path) as response_json_sample_file:
        response_json_sample = json.load(response_json_sample_file)

    # throwing latitude a bit off from fixture value so that we get different GADM region
    response_json_sample["results"][0]["geometry"]["location"]["lat"] = 47.39

    await service_provider.gfm_cache_service.set_cache_entry_by_gfm_name_and_key(
        "LocationGapFillingWorker",
        "Zürich Schweiz",
        response_json_sample,
    )

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 2

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == recipe.uid
    assert location_mutation.prop_name == "activity_location"
    assert location_mutation.props[0].country_code == "CH"
    # different GADM region here since GFM has used external queries cache instead of calling Google Maps API
    assert location_mutation.props[0].get_term().xid == "CHE.26.13.27_1"

    assert mutation_log[1].node_uid == root_node.uid

    # checking if GFM external queries cache works correctly
    assert len(service_provider.gfm_cache_service._cache) == 1
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1


@pytest.mark.asyncio
async def test_city_lookup_with_cached_query_in_db(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # add Google Maps query response to GFM queries cache
    sample_json_path = os.path.join(
        os.path.dirname(__file__),
        "sample_jsons",
        "zurich_google_maps_query_response.json",
    )

    with open(sample_json_path) as response_json_sample_file:
        response_json_sample = json.load(response_json_sample_file)

    # throwing latitude a bit off from fixture value so that we get different GADM region
    response_json_sample["results"][0]["geometry"]["location"]["lat"] = 47.385

    await postgres_db.get_gfm_cache_mgr().set_cache_entry_by_gfm_name_and_key(
        "LocationGapFillingWorker",
        "Zürich Schweiz",
        response_json_sample,
    )

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_node.uid, recipe.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 0
    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 2

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == recipe.uid
    assert location_mutation.prop_name == "activity_location"
    assert location_mutation.props[0].country_code == "CH"
    # different GADM region here since GFM has used external queries DB cache instead of calling Google Maps API
    assert location_mutation.props[0].get_term().xid == "CHE.26.13.13_1"

    assert mutation_log[1].node_uid == root_node.uid

    # checking if GFM external queries cache works correctly
    assert len(service_provider.gfm_cache_service._cache) == 1
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1


@pytest.mark.asyncio
async def test_city_lookup_with_prefilled_cached_query_in_db(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # add Google Maps query response to GFM queries cache
    sample_json_path = os.path.join(
        os.path.dirname(__file__),
        "sample_jsons",
        "zurich_google_maps_query_response.json",
    )

    with open(sample_json_path) as response_json_sample_file:
        response_json_sample = json.load(response_json_sample_file)

    await postgres_db.get_gfm_cache_mgr().set_cache_entry_by_gfm_name_and_key(
        "LocationGapFillingWorker",
        "Zürich Schweiz",
        response_json_sample,
        load_on_boot=True,
    )

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_node.uid, recipe.uid)

    # running the GFM
    gap_filling_factory = LocationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 2

    location_mutation = mutation_log[0]
    assert location_mutation.node_uid == recipe.uid
    assert location_mutation.prop_name == "activity_location"
    assert location_mutation.props[0].country_code == "CH"
    assert location_mutation.props[0].get_term().xid == "CHE.26.13.21_1"

    assert mutation_log[1].node_uid == root_node.uid

    # checking if GFM external queries cache works correctly
    assert len(service_provider.gfm_cache_service._cache) == 1
    assert len(service_provider.gfm_cache_service._cache["LocationGapFillingWorker"]) == 1
