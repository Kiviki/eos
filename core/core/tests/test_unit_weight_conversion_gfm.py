"Tests for unit weight conversion gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingFactory
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp, QuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeForUnitWeightGFM, MinimumRecipeRType
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class TermCollection:
    "Collection of terms."

    def __init__(self, service_provider: ServiceProvider, foodex2_term_group_uid: str, eaternity_term_group_uid: str):
        foodex2_term_group_uid = uuid.UUID(foodex2_term_group_uid)
        eaternity_term_group_uid = uuid.UUID(eaternity_term_group_uid)
        self.carrot_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("A1791", foodex2_term_group_uid)
        ]
        self.apple_juice_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("A039M", foodex2_term_group_uid)
        ]
        self.orange_juice_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("A03AM", foodex2_term_group_uid)
        ]
        self.unknown_unit_juice_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            (
                "A03AM",
                foodex2_term_group_uid,
            )
        ]
        self.abstract_unit_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            (
                "A1791",
                foodex2_term_group_uid,
            )
        ]

        self.gram_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", eaternity_term_group_uid)
        ]
        self.kilogram_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", eaternity_term_group_uid)
        ]
        self.millimeter_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_millimeter", eaternity_term_group_uid)
        ]
        self.milliliter_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_milliliter", eaternity_term_group_uid)
        ]
        self.square_meter_year_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_square-meter-year", eaternity_term_group_uid)
        ]


def prepare_child_activity(term_collection: TermCollection) -> FoodProcessingActivityNode:
    "Construct a child activity conveniently."
    child_activity_uid = uuid.uuid4()
    child_activity = FoodProcessingActivityNode(uid=child_activity_uid)

    # checking if both unit and weight have been converted
    child_activity.production_amount = QuantityProp(
        value=1, unit_term_uid=term_collection.gram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    return child_activity


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_solid_food(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    "A test for checking conversion to mass of a solid food product."
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(carrot_ingredient.uid, child_activity.uid)

    # add carrot[Term] to carrot_ingredient, using a mutation

    logger.debug("create PropMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=carrot_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=term_collection.carrot_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.kilogram_term.uid

    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 150


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_liquids(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """A variation of the previous test to test out volume to mass conversion."""
    postgres_db, service_provider, _, apple_juice_ingredient, _, _, _, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=apple_juice_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(apple_juice_ingredient)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(apple_juice_ingredient.uid, child_activity.uid)

    # add apple_juice[Term] to apple_juice_ingredient, using a mutation
    logger.debug("create PropMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=apple_juice_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=term_collection.apple_juice_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(apple_juice_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have been converted
    assert (
        calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.milliliter_term.uid
    )

    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 293 * 1.04


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_liquids_with_no_density_declared(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    "Test out volume to mass conversion with an ingredient that as no density declared in its glossary term."
    postgres_db, service_provider, _, _, orange_juice_ingredient, _, _, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=orange_juice_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(orange_juice_ingredient)

    # add orange_juice[Term] to orange_juice_ingredient, using a mutation
    logger.debug("create PropMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=orange_juice_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=term_collection.orange_juice_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(orange_juice_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(orange_juice_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have been converted
    assert (
        calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.milliliter_term.uid
    )

    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 854


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_solid_food_without_match_product_name_gfm(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    "Test running in 'autonomous' mode (without using information from the matching glossary term)."
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(carrot_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."

    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have been converted
    assert calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.kilogram_term.uid

    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 150


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_for_liquids_without_match_product_name_gfm(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """A variation of the previous test to test out volume to mass conversion."""
    postgres_db, service_provider, _, apple_juice_ingredient, _, _, _, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=apple_juice_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(apple_juice_ingredient)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(apple_juice_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(apple_juice_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."

    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have been converted
    assert (
        calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.milliliter_term.uid
    )

    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 293


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_for_ingredient_with_unknown_unit(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """A test for checking failure of unit weight conversion if the declared unit is unknown."""
    (
        postgres_db,
        service_provider,
        _,
        _,
        _,
        unknown_unit_juice_ingredient,
        _,
        _,
    ) = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=unknown_unit_juice_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(unknown_unit_juice_ingredient)

    # add orange_juice[Term] to unknown_unit_juice_ingredient, using a mutation
    logger.debug("create PropMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=unknown_unit_juice_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=term_collection.unknown_unit_juice_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(unknown_unit_juice_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(unknown_unit_juice_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if no unit and weight have been converted
    assert calc_graph.get_root_node().amount is None


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_for_ingredient_with_unknown_unit_and_no_matching(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """A variation of the previous test without matched glossary term."""
    postgres_db, service_provider, _, _, _, unknown_unit_ingredient, _, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=unknown_unit_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(unknown_unit_ingredient)

    child_activity = prepare_child_activity(
        TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)
    )
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(unknown_unit_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(unknown_unit_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."

    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if no unit and weight have been converted
    assert calc_graph.get_root_node().amount is None


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_for_ingredient_with_abstract_unit(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """A test for checking solid foods weight conversion of abstract units (like "piece") to mass."""
    postgres_db, service_provider, _, _, _, _, abstract_unit_ingredient, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=abstract_unit_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(abstract_unit_ingredient)

    # add carrot[Term] to abstract_unit_ingredient, using a mutation
    logger.debug("create PropMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=abstract_unit_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=term_collection.abstract_unit_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    child_activity = prepare_child_activity(term_collection)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(abstract_unit_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(abstract_unit_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have been converted
    assert calc_graph.get_root_node().amount.unit_term_uid == term_collection.gram_term.uid
    assert calc_graph.get_root_node().amount.value == 62.1 * 7


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_for_ingredient_with_abstract_unit_and_no_matching(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """Test out conversion failure in case of product not being matched to its Term."""
    postgres_db, service_provider, _, _, _, _, abstract_unit_ingredient, _ = create_minimum_recipe_for_unit_weight_gfm
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=abstract_unit_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(abstract_unit_ingredient)

    child_activity = prepare_child_activity(
        TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)
    )
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(abstract_unit_ingredient.uid, child_activity.uid)

    # running the GFM
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(abstract_unit_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."

    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if both unit and weight have not been converted
    assert calc_graph.get_root_node().amount is None


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_wrong_node_type(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service

    node_uid = uuid.uuid4()
    child_activity_uid = uuid.uuid4()
    flow_node = FlowNode(uid=node_uid)

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        calculation=Calculation(root_node_uid=flow_node.uid, child_of_root_node_uid=child_activity_uid),
        glossary_service=glossary_service,
    )
    calc_graph.add_node(flow_node)

    child_activity = ElementaryResourceEmissionNode(uid=child_activity_uid)
    calc_graph.add_node(child_activity)
    calc_graph.add_edge(flow_node.uid, child_activity.uid)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_activity)

    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_no_raw_input(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    # Currently, the GFM is scheduled even if the raw_input is missing, as it checked for unit consistency.
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_gather_from_children(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    node_uid = uuid.uuid4()
    recipe_node = FoodProcessingActivityNode(uid=node_uid)

    child_flow_1_uid = uuid.uuid4()
    child_flow_1 = FoodProductFlowNode(uid=child_flow_1_uid)

    child_flow_1.amount = QuantityProp(
        value=700, unit_term_uid=term_collection.gram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    child_flow_2_uid = uuid.uuid4()
    child_flow_2 = FoodProductFlowNode(uid=child_flow_2_uid)

    child_flow_2.amount = QuantityProp(
        value=0.6, unit_term_uid=term_collection.kilogram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe_node)
    calc_graph.add_node(child_flow_1)
    calc_graph.add_node(child_flow_2)
    calc_graph.add_edge(recipe_node.uid, child_flow_1.uid)
    calc_graph.add_edge(recipe_node.uid, child_flow_2.uid)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe_node)

    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # assert calc_graph.get_root_node().amount_in_original_source_unit.value == 1300
    # assert calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.gram_term.uid

    assert calc_graph.get_root_node().production_amount.value == 1.300
    assert calc_graph.get_root_node().production_amount.unit_term_uid == term_collection.kilogram_term.uid


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_gather_from_children_wrong_unit(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    node_uid = uuid.uuid4()
    recipe_node = FoodProcessingActivityNode(uid=node_uid)

    child_flow_1_uid = uuid.uuid4()
    child_flow_1 = FoodProductFlowNode(uid=child_flow_1_uid)

    child_flow_1.amount = QuantityProp(
        value=700,
        unit_term_uid=term_collection.square_meter_year_term.uid,
        for_reference=ReferenceAmountEnum.self_reference,
    )

    child_flow_2_uid = uuid.uuid4()
    child_flow_2 = FoodProductFlowNode(uid=child_flow_2_uid)

    child_flow_2.amount = QuantityProp(
        value=0.6, unit_term_uid=term_collection.kilogram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe_node)
    calc_graph.add_node(child_flow_1)
    calc_graph.add_node(child_flow_2)
    calc_graph.add_edge(recipe_node.uid, child_flow_1.uid)
    calc_graph.add_edge(recipe_node.uid, child_flow_2.uid)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe_node)

    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # assert calc_graph.get_root_node().amount_in_original_source_unit.value == 1000
    # assert calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.gram_term.uid

    assert calc_graph.get_root_node().production_amount.value == 1.000
    assert calc_graph.get_root_node().production_amount.unit_term_uid == term_collection.kilogram_term.uid


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_gather_from_children_no_converted_amount(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    eaternity_term_group_uid, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    term_collection = TermCollection(service_provider, foodex2_term_group_uid, eaternity_term_group_uid)

    node_uid = uuid.uuid4()
    recipe_node = FoodProcessingActivityNode(uid=node_uid)

    child_flow_1_uid = uuid.uuid4()
    child_flow_1 = FoodProductFlowNode(uid=child_flow_1_uid)

    child_flow_2_uid = uuid.uuid4()
    child_flow_2 = FoodProductFlowNode(uid=child_flow_2_uid)

    child_flow_2.amount = QuantityProp(
        value=0.6, unit_term_uid=term_collection.kilogram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe_node)
    calc_graph.add_node(child_flow_1)
    calc_graph.add_node(child_flow_2)
    calc_graph.add_edge(recipe_node.uid, child_flow_1.uid)
    calc_graph.add_edge(recipe_node.uid, child_flow_2.uid)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe_node)

    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # assert calc_graph.get_root_node().amount_in_original_source_unit.value == 1000
    # assert calc_graph.get_root_node().amount_in_original_source_unit.unit_term_uid == term_collection.gram_term.uid

    assert calc_graph.get_root_node().production_amount.value == 1.000
    assert calc_graph.get_root_node().production_amount.unit_term_uid == term_collection.kilogram_term.uid


# the following two tests cannot be parametrized, hence why they are duplicated
# see https://github.com/pytest-dev/pytest-asyncio/issues/112
@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_empty_unit_and_weight_fields(
    create_minimum_recipe_with_empty_fields: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
) -> None:
    """Test if empty unit and weight fields cause GFM still to be scheduled.

    A variation of the first test.
    """
    postgres_db, service_provider, _, carrot_ingredient, _root_flow = create_minimum_recipe_with_empty_fields
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM should be scheduled to run."


@pytest.mark.asyncio
async def test_unit_weight_conversion_gfm_with_no_unit_and_weight_fields(
    create_minimum_recipe_with_no_fields: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
) -> None:
    """Test if absence of values in unit and weight fields does not cause this GFM to be scheduled."""
    postgres_db, service_provider, _, carrot_ingredient, _root_flow = create_minimum_recipe_with_no_fields
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    # Currently, the GFM is scheduled even if the raw_input is missing, as it checked for unit consistency.
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."
