"Tests for Calculation Service."
import uuid

import pytest
import structlog
from gap_filling_modules.impact_assessment_gfm import ImpactAssessmentGapFillingWorker

from core.domain.calculation import Calculation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import (
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    MinimumRecipeRType,
    is_close,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_calc_service(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_node.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5
    # FIXME: somehow many mutations are skipped
    # FIXME: improve assertation to validate all or many more worker module names?
    for i, dm in enumerate(calculation.mutations):
        print(i, dm.created_by_module)
    # -6 when aggregation GFM is active
    assert calculation.mutations[-5].created_by_module == ImpactAssessmentGapFillingWorker.__name__

    root_char_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    # FIXME: root_node should have the impact_assessment
    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_carrot_in_kg = carrot_ingredient.amount_in_original_source_unit.value
    expected_co2 = (
        2
        * (  # factor 2 because the root-flow-node that we appended has twice the amount of the original recipe
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
            + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        )
        * amount_carrot_in_kg
    )
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"
