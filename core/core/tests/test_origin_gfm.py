"Tests for origin gap filling module."
import uuid

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.origin_gfm import OriginGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.flow_node import FlowNode
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import MinimumRecipeRType, MinimumRecipeWithDeclaration, MinimumRecipeWithTwoOrigins


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_no_origin(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests a case when no origin is specified for both combined product and monoproduct."""
    postgres_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # recipe -> food_product_flow -> food_product -> origin split ingredient nodes
    origin_split_nodes = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    assert len(origin_split_nodes) == 6

    ingredient_amount_per_country = {
        "CH": 96.3653,
        "ES": 1.2834,
        "IT": 1.0832,
        "DK": 0.7651,
        "NL": 0.2775,
        "PT": 0.2255,
    }

    for node in origin_split_nodes:
        assert isinstance(node, FlowNode)

        assert node.origin_data
        assert node.origin_data.stats_data_year_column
        assert node.origin_data.stats_data_year_column == str(2021)

        # asserting that everything's good with origin prop
        assert len(node.flow_location) == 1
        prop_data = node.flow_location[0]
        assert prop_data
        assert prop_data.country_code in ingredient_amount_per_country.keys()
        assert prop_data.location_qualifier == LocationQualifierEnum.known

        # asserting that subdivision nodes are all present
        subdivision_nodes = node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
        assert len(subdivision_nodes) == 2

        for subdivision_node in subdivision_nodes:
            # asserting that ingredient amount have all been converted
            if not subdivision_node.is_dried():
                unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                assert (
                    round(
                        subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                        4,
                    )
                    == ingredient_amount_per_country[prop_data.country_code]
                )

                assert round(node.amount.value, 6) == round(
                    ingredient_amount_per_country[prop_data.country_code] / 100, 6
                )

            # asserting that brightway_process nodes are present
            assert len(subdivision_node.get_sub_nodes()) == 1


@pytest.mark.asyncio
@pytest.mark.parametrize("mode", ["delete_fao_code_link", "replace_fao_code_link_with_default_fao_code"])
async def test_origin_gfm_ingredient_with_no_unknown_origin_and_no_fao_code(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
    mode: str,
) -> None:
    """Tests a case when no origin is specified for both combined product and monoproduct."""
    postgres_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service
    matching_service = service_provider.matching_service
    glossary_link_service = service_provider.glossary_link_service

    # delete FAO glossary links for the test
    matchings = await matching_service.get_terms_by_matching_string("Karotten", "MatchProductName", filter_lang="de")
    fao_term = await glossary_link_service.load_fao_code_term(matchings[0])
    root_fao_term_access_group_uid = fao_term.access_group_uid
    assert isinstance(fao_term, Term)

    existing_fao_glossary_links = await glossary_link_service.get_glossary_links_by_gfm("FAO")

    await glossary_link_service.delete_glossary_links_by_uids([link.uid for link in existing_fao_glossary_links])
    fao_term = await glossary_link_service.load_fao_code_term(matchings[0])
    assert fao_term is None

    if mode == "replace_fao_code_link_with_default_fao_code":
        # add the default fao-code as a new glossary link
        default_production_fao_code_term = await glossary_service.get_term_by_xid_and_access_group_uid(
            term_xid="200000",
            access_group_uid=str(root_fao_term_access_group_uid),
        )
        new_glossary_link = GlossaryLink(
            gap_filling_module="FAO",
            term_uids=[matching.uid for matching in matchings[0]],
            linked_term_uid=default_production_fao_code_term.uid,
        )
        await glossary_link_service.insert_glossary_link(new_glossary_link)
        fao_term = await glossary_link_service.load_fao_code_term(matchings[0])
        assert isinstance(fao_term, Term)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)

    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # # recipe -> food_product_flow -> food_product -> origin split ingredient nodes
    origin_split_nodes = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    assert len(origin_split_nodes) == 11

    ingredient_amount_per_country = {
        "CH": 61.01389063186762,
        "FR": 12.621521941897171,
        "DE": 10.513144513451197,
        "IT": 5.303505955882405,
        "AT": 2.5770379541385706,
        "ES": 2.402282387382289,
        "US": 1.5966798909006936,
        "BR": 1.3133181451036504,
        "NL": 1.2856277688136875,
        "GB": 0.7328344670971775,
        "BE": 0.640156343465541,
    }

    countries_which_have_mocked_ecotransit_responses = [
        "CH",
        "ES",
        "IT",
        "NL",
    ]  # we don't yet have mocked ecotransit responses for all countries

    for node in origin_split_nodes:
        assert isinstance(node, FlowNode)

        assert node.origin_data
        assert node.origin_data.stats_data_year_column
        assert node.origin_data.stats_data_year_column == str(2021)

        # asserting that everything's good with origin prop
        assert len(node.flow_location) == 1
        prop_data = node.flow_location[0]
        assert prop_data
        assert prop_data.country_code in ingredient_amount_per_country.keys()
        assert prop_data.location_qualifier == LocationQualifierEnum.known

        # asserting that subdivision nodes are all present
        if prop_data.country_code in countries_which_have_mocked_ecotransit_responses:
            subdivision_nodes = node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
        else:  # no transport nodes have been added in between
            subdivision_nodes = node.get_sub_nodes()[0].get_sub_nodes()
        assert len(subdivision_nodes) == 2

        for subdivision_node in subdivision_nodes:
            # asserting that ingredient amount have all been converted
            if not subdivision_node.is_dried():
                unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                assert round(
                    subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                    4,
                ) == round(ingredient_amount_per_country[prop_data.country_code], 4)

                assert round(node.amount.value, 6) == round(
                    ingredient_amount_per_country[prop_data.country_code] / 100, 6
                )

            # asserting that brightway_process nodes are present
            assert len(subdivision_node.get_sub_nodes()) == 1


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_1_origin(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    "Test with one origin."
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    carrot_ingredient.flow_location = [LocationProp(address="Spain", location_qualifier=LocationQualifierEnum.known)]
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(recipe.uid, carrot_ingredient.uid)

    # running the GFM
    gap_filling_factory = OriginGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 0


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_two_recipe_origins(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Tests a case when 2 origins are specified for a combined product and no origins for monoproducts."""
    postgres_db, service_provider, recipe, _, root_flow = create_minimum_recipe_with_small_declaration_and_two_origins
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # recipe -> recipe origin split flow nodes
    origin_split_recipe_nodes = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    assert len(origin_split_recipe_nodes) == 2

    for origin_split_recipe_node in origin_split_recipe_nodes:
        assert len(origin_split_recipe_node.get_sub_nodes()[0].activity_location) == 1
        location_prop = origin_split_recipe_node.get_sub_nodes()[0].activity_location[0]
        recipe_origin_country_code = location_prop.country_code
        assert recipe_origin_country_code in (
            "ES",
            "CH",
        )

        # recipe origin split flow nodes -> split recipe node -> main food_product_flow node
        root_ingredient_node = origin_split_recipe_node.get_sub_nodes()[0].get_sub_nodes()[0]
        unit = root_ingredient_node.amount.amount_for_root_node().get_unit_term()
        assert round(root_ingredient_node.amount.amount_for_root_node().value * unit.data["mass-in-g"], 4) == 500.0

        # TODO: !!! should this be checked & edited at all?
        # is_origin_counted is not a part of amount and the following was never checked.
        # if converted_amount_props.get("is_origin_counted"):
        assert round(origin_split_recipe_node.amount.value, 6) == 0.5

        # Chain of nested nodes:
        # -> main food_product_flow node
        #   -> food_product (in destination location)
        #     -> transported_food_product_flow
        #       -> food_product (in origin location)
        #         -> origin split ingredient nodes
        origin_split_nodes = (
            root_ingredient_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
        )

        if recipe_origin_country_code == "CH":
            assert len(origin_split_nodes) == 6
        elif recipe_origin_country_code == "ES":
            assert len(origin_split_nodes) == 5
        else:
            raise ValueError(f"Unexpected recipe origin country code: {recipe_origin_country_code}")

        ingredient_amount_per_country = {
            "CH": {
                "CH": 96.3653 / 2,
                "ES": 1.2834 / 2,
                "IT": 1.0832 / 2,
                "DK": 0.7651 / 2,
                "NL": 0.2775 / 2,
                "PT": 0.2255 / 2,
            },
            "ES": {
                "ES": 93.0707 / 2,
                "FR": 4.9827 / 2,
                "PT": 1.2275 / 2,
                "NL": 0.4360 / 2,
                "DE": 0.2831 / 2,
            },
        }

        country_codes = {
            "CH": ingredient_amount_per_country["CH"].keys(),
            "ES": ingredient_amount_per_country["ES"].keys(),
        }

        for node in origin_split_nodes:
            assert isinstance(node, FlowNode)

            # asserting that everything's good with origin prop
            prop_data = node.flow_location[0]
            assert prop_data
            assert prop_data.country_code in country_codes[recipe_origin_country_code]

            # asserting that subdivision nodes are all present
            subdivision_nodes = node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
            assert len(subdivision_nodes) == 2

            for subdivision_node in subdivision_nodes:
                # asserting that ingredient amount have all been converted
                if not subdivision_node.is_dried():
                    unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                    node_value = round(
                        subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                        4,
                    )
                    test_value = round(
                        ingredient_amount_per_country[recipe_origin_country_code][prop_data.country_code],
                        4,
                    )
                    assert round(abs(node_value - test_value), 4) <= 0.0001

                    node_value = round(subdivision_node.amount.value, 6)
                    # the value of the not dried subdivision should be 1 kg, as that number is counting relative to
                    # the production_amount of the direct parent activity: 1 kg
                    test_value = 1.0
                    assert round(abs(node_value - test_value), 4) <= 0.00001

                # asserting that brightway_process nodes are present
                assert len(subdivision_node.get_sub_nodes()) == 1
