"Test Calculation queue."
import uuid

import pytest
import structlog

from core.domain.calculation import Calculation
from core.service.messaging_service import MessagingService
from core.tests.conftest import MinimumRecipeRType

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_calculation_queue(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests basic queue of calculations."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_flow = create_minimum_recipe

    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()
    await service_provider.gap_filling_module_loader.init(service_provider)

    # create a calculation
    calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )
    await service_provider.postgres_db.pg_calc_mgr.insert_calculation(calculation)

    # now enqueue the calculation for processing
    await service_provider.messaging_service.enqueue_calculation(calculation_uid)
    calculation = await service_provider.postgres_db.pg_calc_mgr.find_by_uid(calculation.uid)

    assert len(calculation.mutations) > 5

    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None
