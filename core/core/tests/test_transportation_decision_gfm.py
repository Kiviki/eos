"Tests for transportation decision gap filling module."
import uuid

import numpy as np
import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.conservation_gfm import ConservationGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory
from gap_filling_modules.transportation_decision_gfm import TransportDecisionGapFillingFactory
from gap_filling_modules.transportation_mode_distance_gfm import TransportModeDistanceGapFillingFactory
from gap_filling_modules.transportation_util.enum import ServiceTransportModeEnum
from gap_filling_modules.unit_weight_conversion_gfm import (
    UnitWeightConversionGapFillingFactory,
    UnitWeightConversionGapFillingWorker,
)
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes import ElementaryResourceEmissionNode, FoodProcessingActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import GfmStateProp, NamesProp, TransportModesDistancesProp
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import (
    MinimumRecipeRType,
    MinimumRecipeWithDeclaration,
    MinimumRecipeWithTwoOrigins,
    TermAccessGroupIdsRType,
)
from database.postgres.postgres_db import PostgresDb

from .conftest import assert_transport_allclose, is_close

logger = get_logger()


BASIC_TRANSPORT_MODES_DATA = {
    "road": {
        "distances": {
            "main_carriage": 1629.16742,
            "pre_carriage": 1008.0083455472976,
            "post_carriage": 9.733414896129048,
            "total": 2646.9091804434265,
        },
        "co2": {
            "main_carriage": 0.122126599,
            "pre_carriage": 0.07556290992199455,
            "post_carriage": 0.0007296419283416428,
        },
    },
    "sea": {
        "distances": {
            "main_carriage": 1145.62824,
            "pre_carriage": 1008.0083455472976,
            "post_carriage": 345.7198974516188,
            "total": 2499.3564829989164,
        },
        "co2": {
            "main_carriage": 0.0177100863,
            "pre_carriage": 0.07510956985638165,
            "post_carriage": 0.02667124641541553,
        },
    },
    "air": {
        "distances": {
            "main_carriage": 1334.543,
            "pre_carriage": 1008.0083455472976,
            "post_carriage": 9.040895297799983,
            "total": 2351.5922408450974,
        },
        "co2": {
            "main_carriage": 1.28445241,
            "pre_carriage": 0.08064659362214979,
            "post_carriage": 0.0007380273937297906,
        },
    },
}

TRANSPORT_DATA_FOR_ZURICH = {
    "CH": {
        "road": {
            "distances": {
                "main_carriage": 104.435052,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 104.435052,
            },
            "co2": {
                "main_carriage": 0.00866023448,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 1629.16742,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1629.16742,
            },
            "co2": {
                "main_carriage": 0.122126599,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "IT": {
        "road": {
            "distances": {
                "main_carriage": 744.706759,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 744.706759,
            },
            "co2": {
                "main_carriage": 0.0563098788,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "DK": {
        "road": {
            "distances": {
                "main_carriage": 1171.10237,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1171.10237,
            },
            "co2": {
                "main_carriage": 0.0866346466,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 743.567627,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 743.567627,
            },
            "co2": {
                "main_carriage": 0.0559465938,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 2014.30979,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 2014.30979,
            },
            "co2": {
                "main_carriage": 0.151539758,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
}

TRANSPORT_DATA_FOR_SWITZERLAND = {
    "CH": {
        "road": {
            "distances": {
                "main_carriage": 163.7402977783409,  # From Area
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 163.7402977783409,
            },
            "co2": {"main_carriage": 0.013427246535166712, "pre_carriage": 0, "post_carriage": 0},
        }
    },
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 1598.69564,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1598.69564,
            },
            "co2": {
                "main_carriage": 0.119955051,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "IT": {
        "road": {
            "distances": {
                "main_carriage": 708.321977,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 708.321977,
            },
            "co2": {
                "main_carriage": 0.0532901736,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "DK": {
        "road": {
            "distances": {
                "main_carriage": 1257.01659,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1257.01659,
            },
            "co2": {
                "main_carriage": 0.0925568316,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 803.511844,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 803.511844,
            },
            "co2": {
                "main_carriage": 0.0598233292,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 1983.83801,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1983.83801,
            },
            "co2": {
                "main_carriage": 0.149359738,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
}

TRANSPORT_DATA_FOR_SPAIN = {
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 573.6071490336897,  # From Area
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 573.6071490336897,
            },
            "co2": {"main_carriage": 0.0451036013032305, "pre_carriage": 0, "post_carriage": 0},
        }
    },
    "FR": {
        "road": {
            "distances": {
                "main_carriage": 1148.32515,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1148.32515,
            },
            "co2": {
                "main_carriage": 0.0840715648,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 577.345759,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 577.345759,
            },
            "co2": {
                "main_carriage": 0.0429978706,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 1759.71659,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1759.71659,
            },
            "co2": {
                "main_carriage": 0.130555201,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
    "DE": {
        "road": {
            "distances": {
                "main_carriage": 2026.05423,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 2026.05423,
            },
            "co2": {
                "main_carriage": 0.150345435,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
    },
}


def cheapest_mode_to_transport_modes_distances_prop(
    transport: TransportModesDistancesProp,
) -> TransportModesDistancesProp:
    cheapest_mode = transport.cheapest_mode
    return TransportModesDistancesProp.unvalidated_construct(
        co2={cheapest_mode: transport.co2[cheapest_mode]}, distances={cheapest_mode: transport.distances[cheapest_mode]}
    )


@pytest.mark.asyncio
async def test_transportation_decision_with_small_declaration(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests a case when we decide transport mode for ingredients where origin is calculated by Origin GFM."""
    postgres_db, service_provider, recipe, root_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(root_flow)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # recipe -> food_product_flow -> food_product -> origin split ingredient nodes
    origin_split_nodes = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    assert len(origin_split_nodes) == 6

    for origin_split_node in origin_split_nodes:
        # asserting that everything's good with transportation choices prop
        prop_data = cheapest_mode_to_transport_modes_distances_prop(origin_split_node.transport)
        assert prop_data
        origin_country_code = origin_split_node.flow_location[0].country_code

        assert_transport_allclose(prop_data, TRANSPORT_DATA_FOR_ZURICH.get(origin_country_code))

        picked_mode = [mode for mode in TRANSPORT_DATA_FOR_ZURICH.get(origin_country_code)][0]

        transportation_sub_nodes = origin_split_node.get_sub_nodes()[0].get_sub_nodes()
        assert len(transportation_sub_nodes) == 3 if picked_mode == ServiceTransportModeEnum.GROUND.value else 5

        carriage_distances_set = {
            v for k, v in TRANSPORT_DATA_FOR_ZURICH.get(origin_country_code).get(picked_mode).get("distances").items()
        }
        carriage_co2_set = {
            round(v * 1000, 7)
            for k, v in TRANSPORT_DATA_FOR_ZURICH.get(origin_country_code).get(picked_mode).get("co2").items()
        }

        for carriage_transportation_node in transportation_sub_nodes:
            assert isinstance(carriage_transportation_node, FlowNode)
            if carriage_transportation_node.flow_location:
                continue
            product_name = carriage_transportation_node.product_name

            assert isinstance(product_name, NamesProp)

            if product_name.terms[0].get_term().name == "PRESERVED BY CHILLING":
                continue

            assert product_name.source_data_raw[0]["value"] in (
                "Main carriage",
                "Post-carriage",
                "Pre-carriage",
            )

            unit = carriage_transportation_node.amount.get_unit_term()
            payload_in_kg_km = unit.data["payload-distance-in-ton-km"] * 1000
            amount_kg_km = carriage_transportation_node.amount.value * payload_in_kg_km
            assert True in [np.allclose(amount_kg_km, co2) for co2 in carriage_distances_set]

            # For carriage_transporation_node, amount holds the information that was in
            # "ingredient_amount" in the old (pre 2023-11-21) code.
            assert is_close(
                carriage_transportation_node.amount.value * payload_in_kg_km,
                carriage_transportation_node.amount_in_original_source_unit.value,
            )

            for lci_node in carriage_transportation_node.get_sub_nodes()[0].get_sub_nodes():
                # checking fuel consumption node
                if isinstance(lci_node.get_sub_nodes()[0], ElementaryResourceEmissionNode):
                    amount = round(
                        lci_node.amount.value * carriage_transportation_node.amount.value * payload_in_kg_km,
                        7,
                    )
                    assert True in [np.allclose(amount, co2) for co2 in carriage_co2_set]
                # checking infrastructure nodes
                else:
                    assert is_close(lci_node.amount_in_original_source_unit.value, 1)
                    assert is_close(lci_node.amount.value, 1)


@pytest.mark.asyncio
async def test_transportation_decision_with_two_recipe_origins(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Tests a case when 2 origins are specified for a combined product and no origins for monoproducts."""
    postgres_db, service_provider, recipe, _, root_flow = create_minimum_recipe_with_small_declaration_and_two_origins
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert root_node.get_sub_nodes()[0].activity_location[0].source_data_raw == "Zürich Schweiz"

    # recipe -> recipe origin split flow nodes
    origin_split_recipe_nodes = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()

    assert len(origin_split_recipe_nodes) == 2
    assert origin_split_recipe_nodes[0].get_sub_nodes()[0].activity_location[0].country_code == "ES"
    assert origin_split_recipe_nodes[1].get_sub_nodes()[0].activity_location[0].country_code == "CH"

    for origin_split_recipe_node in origin_split_recipe_nodes:
        location_prop = origin_split_recipe_node.get_sub_nodes()[0].activity_location[-1]
        recipe_origin_country_code = location_prop.country_code
        assert recipe_origin_country_code in (
            "ES",
            "CH",
        )

        # recipe origin split flow nodes -> split recipe node -> main food_product_flow node
        root_ingredient_node = origin_split_recipe_node.get_sub_nodes()[0].get_sub_nodes()[0]

        # Chain of nested nodes:
        # -> main food_product_flow node
        #   -> food_product (in destination location)
        #     -> transported_food_product_flow
        #       -> food_product (in origin location)
        #         -> origin split ingredient nodes
        origin_split_nodes = (
            root_ingredient_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
        )

        if recipe_origin_country_code == "CH":
            assert len(origin_split_nodes) == 6
        elif recipe_origin_country_code == "ES":
            assert len(origin_split_nodes) == 5
        else:
            raise ValueError(f"Unexpected recipe origin country code: {recipe_origin_country_code}")

        transport_data_per_country = {
            "CH": TRANSPORT_DATA_FOR_SWITZERLAND,
            "ES": TRANSPORT_DATA_FOR_SPAIN,
        }

        for origin_split_node in origin_split_nodes:
            # asserting that everything's good with transportation choices prop
            prop_data = cheapest_mode_to_transport_modes_distances_prop(origin_split_node.transport)
            assert prop_data
            origin_country_code = origin_split_node.flow_location[0].country_code
            assert_transport_allclose(
                prop_data, transport_data_per_country.get(recipe_origin_country_code).get(origin_country_code)
            )

            picked_mode = [
                mode for mode in transport_data_per_country.get(recipe_origin_country_code).get(origin_country_code)
            ][0]

            transportation_sub_nodes = origin_split_node.get_sub_nodes()[0].get_sub_nodes()
            # additionally, to the carriages and the Flow of the product itself, the carrot ingredient has to be cooled
            # since it is highly perishable
            assert len(transportation_sub_nodes) == 3 if picked_mode == ServiceTransportModeEnum.GROUND.value else 5

            carriage_distances_set = {
                v
                for k, v in transport_data_per_country.get(recipe_origin_country_code)
                .get(origin_country_code)
                .get(picked_mode)
                .get("distances")
                .items()
            }
            carriage_co2_set = {
                round(v * 1000, 7)
                for k, v in transport_data_per_country.get(recipe_origin_country_code)
                .get(origin_country_code)
                .get(picked_mode)
                .get("co2")
                .items()
            }

            for carriage_transportation_node in transportation_sub_nodes:
                if carriage_transportation_node.flow_location:
                    continue

                product_name = carriage_transportation_node.product_name

                assert isinstance(product_name, NamesProp)

                if "PRESERVED BY CHILLING" in [term.get_term().name for term in product_name.terms]:
                    continue

                assert product_name.source_data_raw[0]["value"] in (
                    "Main carriage",
                    "Post-carriage",
                    "Pre-carriage",
                )

                unit = carriage_transportation_node.amount.get_unit_term()
                payload_in_kg_km = unit.data["payload-distance-in-ton-km"] * 1000
                amount_kg_km = carriage_transportation_node.amount.value * payload_in_kg_km
                assert True in [np.allclose(amount_kg_km, co2) for co2 in carriage_distances_set]

                # For carriage_transporation_node, amount holds the information that was in
                # "ingredient_amount" in the old (pre 2023-11-21) code.
                assert is_close(
                    carriage_transportation_node.amount.value * payload_in_kg_km,
                    carriage_transportation_node.amount_in_original_source_unit.value,
                )

                for lci_node in carriage_transportation_node.get_sub_nodes()[0].get_sub_nodes():
                    # checking fuel consumption node
                    if isinstance(lci_node.get_sub_nodes()[0], ElementaryResourceEmissionNode):
                        assert (
                            round(
                                lci_node.amount.value * carriage_transportation_node.amount.value * payload_in_kg_km,
                                7,
                            )
                            in carriage_co2_set
                        )
                    # checking infrastructure nodes
                    else:
                        assert is_close(lci_node.amount_in_original_source_unit.value, 1)
                        assert is_close(lci_node.amount.value, 1)


async def setup_perishable_graph(
    carrot_ingredient: Node,
    glossary_service: GlossaryService,
    postgres_db: PostgresDb,
    service_provider: ServiceProvider,
    conservation: dict = "",
) -> CalcGraph:
    "Create a perishable graph."
    if conservation:
        carrot_ingredient.raw_conservation = conservation

    carrot_ingredient.gfm_state = GfmStateProp(worker_states={})

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    # adding auxiliary food product node
    food_product_node = FoodProcessingActivityNode(uid=uuid.uuid4())
    calc_graph.add_node(food_product_node)
    calc_graph.add_edge(carrot_ingredient.uid, food_product_node.uid)

    transport_mode_distance_gfm_factory = TransportModeDistanceGapFillingFactory(postgres_db, service_provider)
    await transport_mode_distance_gfm_factory.init_cache()
    transport_mode_distance_gfm_worker = transport_mode_distance_gfm_factory.spawn_worker(food_product_node)
    distances, co2 = transport_mode_distance_gfm_worker.convert_valid_modes_into_transport_modes_distances_prop(
        BASIC_TRANSPORT_MODES_DATA
    )

    # adding transport modes data
    logger.debug("create PropMutation to add transport")
    kwargs_dict = {"distances": distances, "co2": co2}
    if carrot_ingredient.transport:
        kwargs_dict["source_data_raw"] = carrot_ingredient.transport

    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=carrot_ingredient.uid,
        prop_name="transport",
        prop=TransportModesDistancesProp(**kwargs_dict),
    )
    await calc_graph.apply_mutation(prop_mutation)

    # Adding production amount to the child activity through unit_weight_conversion
    unit_weight_conversion_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await unit_weight_conversion_factory.init_cache()

    gap_filling_worker = unit_weight_conversion_factory.spawn_worker(food_product_node)
    await gap_filling_worker.run(calc_graph)

    # Match product name such that food tags can be attached.
    match_product_name_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await match_product_name_factory.init_cache()

    gap_filling_worker = match_product_name_factory.spawn_worker(carrot_ingredient)
    await gap_filling_worker.run(calc_graph)

    # Adding appropriate food tags
    attach_food_tags_factory = AttachFoodTagsGapFillingFactory(postgres_db, service_provider)
    await attach_food_tags_factory.init_cache()

    gap_filling_worker = attach_food_tags_factory.spawn_worker(carrot_ingredient)
    await gap_filling_worker.run(calc_graph)

    conservation_factory = ConservationGapFillingFactory(postgres_db, service_provider)
    await conservation_factory.init_cache()

    gap_filling_worker = conservation_factory.spawn_worker(carrot_ingredient)
    await gap_filling_worker.run(calc_graph)

    gap_filling_worker = unit_weight_conversion_factory.spawn_worker(carrot_ingredient)
    await gap_filling_worker.run(calc_graph)

    # Mark UnitWeightConversionGFM as finished.
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=food_product_node.uid,
        prop_name="gfm_state",
        prop=GfmStateProp(worker_states={UnitWeightConversionGapFillingWorker.__name__: NodeGfmStateEnum.finished}),
    )
    await calc_graph.apply_mutation(prop_mutation)

    return calc_graph


@pytest.mark.asyncio
async def test_transportation_decision_with_high_perishable_product(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    """Tests a case when a product has high perishability and the conservation is set to not conserved."""
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, perishability_term_group_uid, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    calc_graph = await setup_perishable_graph(
        carrot_ingredient,
        glossary_service,
        postgres_db,
        service_provider,
        conservation={"value": "not conserved", "language": "en"},
    )

    # running the GFM
    gap_filling_factory = TransportDecisionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # change: 20.02.: carrot is now cooled and therefore sea transport is also allowed
    assert sorted(calc_graph.get_root_node().transport.qualified_modes) == sorted(
        [
            "road",
            "sea",
            "air",
        ]
    )
    assert calc_graph.get_root_node().transport.cheapest_mode == "sea"


@pytest.mark.asyncio
async def test_transportation_decision_with_high_perishable_product_and_no_storage_regime(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    """Tests a case when a product has high perishability defined and no storage options defined."""
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, perishability_term_group_uid, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    calc_graph = await setup_perishable_graph(carrot_ingredient, glossary_service, postgres_db, service_provider)

    # running the GFM
    gap_filling_factory = TransportDecisionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert sorted(calc_graph.get_root_node().transport.qualified_modes) == sorted(["road", "air", "sea"])
    assert calc_graph.get_root_node().transport.cheapest_mode == "sea"
    assert calc_graph.get_root_node().transport.source_data_raw == "air"


@pytest.mark.asyncio
async def test_transportation_decision_with_dried_high_perishable_product(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    "Case when a dried subdivision product has high perishability and not conserved defined as conservation."
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, perishability_term_group_uid, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    calc_graph = await setup_perishable_graph(
        carrot_ingredient,
        glossary_service,
        postgres_db,
        service_provider,
        conservation={"value": "not conserved, dried", "language": "en"},
    )

    # running the GFM
    gap_filling_factory = TransportDecisionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert sorted(calc_graph.get_root_node().transport.qualified_modes) == sorted(
        [
            "road",
            "sea",
            "air",
        ]
    )
    assert calc_graph.get_root_node().transport.cheapest_mode == "sea"
    assert calc_graph.get_root_node().transport.source_data_raw == "air"


@pytest.mark.asyncio
async def test_transportation_decision_with_dried_perishable_product_and_no_storage_regime(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    "Case when a dried subdivision product has high perishability defined and no storage options defined."
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, perishability_term_group_uid, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    calc_graph = await setup_perishable_graph(
        carrot_ingredient,
        glossary_service,
        postgres_db,
        service_provider,
        conservation={"value": "dried", "language": "en"},
    )

    # running the GFM
    gap_filling_factory = TransportDecisionGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert sorted(calc_graph.get_root_node().transport.qualified_modes) == sorted(
        [
            "road",
            "sea",
            "air",
        ]
    )
    assert calc_graph.get_root_node().transport.cheapest_mode == "sea"
