"Test greenhouse model gap filling module."
import uuid
from typing import Optional

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.greenhouse_gfm import GreenhouseGapFillingFactory
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.props import GfmStateProp, LocationProp
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType, MinimumRecipeWithTomato
from database.postgres.postgres_db import PostgresDb


async def set_up_and_run_greenhouse_gfm(
    postgres_db: PostgresDb,
    service_provider: ServiceProvider,
    ingredient: FoodProductFlowNode,
    should_be_scheduled: Optional[bool] = True,
    can_run_now: Optional[bool] = True,
    vegetable_should_be_grown_in_greenhouse: Optional[bool] = True,
) -> None:
    """Run Greenhouse GFM."""
    ingredient.gfm_state = GfmStateProp(worker_states={})
    parent_node = FoodProcessingActivityNode(uid=uuid.uuid4(), activity_date="2023-06-10")

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=ingredient.uid,
        glossary_service=service_provider.glossary_service,
    )

    calc_graph.add_node(ingredient)
    calc_graph.add_node(parent_node)
    calc_graph.add_edge(parent_node_uid=parent_node.uid, child_node_uid=ingredient.uid)

    gap_filling_worker = GreenhouseGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient)
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "GreenhouseGFM should not be scheduled for FoodProductFlowNode."

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # Then, check the production.
    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient)
    await gap_filling_worker.gfm_factory.init_cache()
    if gap_filling_worker.should_be_scheduled():
        assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
        await gap_filling_worker.run(calc_graph)

    # Then, run LinkTermToActivityGFM.
    gap_filling_worker = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient)
    await gap_filling_worker.gfm_factory.init_cache()
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # Run GreenhouseGFM on the ModeledActivityNode.
    brightway_node = calc_graph.get_root_node().get_sub_nodes()[0]
    gap_filling_worker = GreenhouseGapFillingFactory(postgres_db, service_provider).spawn_worker(brightway_node)
    await gap_filling_worker.gfm_factory.init_cache()
    if not should_be_scheduled:
        assert not gap_filling_worker.should_be_scheduled()
        return
    assert gap_filling_worker.should_be_scheduled(), "GreenhouseGFM should be scheduled for ModeledActivityNode."
    if not can_run_now:
        assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.cancel
        return
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    if not vegetable_should_be_grown_in_greenhouse:
        assert isinstance(calc_graph.get_root_node().get_sub_nodes()[0], ModeledActivityNode)
        return

    greenhouse_activity = calc_graph.get_root_node().get_sub_nodes()[0]
    assert isinstance(greenhouse_activity, FoodProcessingActivityNode)

    sub_nodes_of_greenhouse_activity = greenhouse_activity.get_sub_nodes()
    assert len(sub_nodes_of_greenhouse_activity) == 5, "There should be 5 sub_nodes of the GreenhouseActivityNode."
    number_of_foodproduct_flow_nodes = len(
        [sub_node for sub_node in sub_nodes_of_greenhouse_activity if isinstance(sub_node, FoodProductFlowNode)]
    )
    number_of_flow_nodes = len(
        [
            sub_node
            for sub_node in sub_nodes_of_greenhouse_activity
            if isinstance(sub_node, FlowNode) and not isinstance(sub_node, FoodProductFlowNode)
        ]
    )
    assert number_of_foodproduct_flow_nodes == 1, "There should be 1 FoodProductFlowNode."
    assert (
        number_of_flow_nodes == 4
    ), "There should be 4 FlowNodes (1 for electricity, 1 for heating, 1 for plastic, and 1 for glass)."


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_in_netherlands(
    create_minimum_recipe_with_tomato: MinimumRecipeWithTomato,
) -> None:
    """In this case, the vegetable should be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = [
        LocationProp(address="Netherlands", country_code="NL", location_qualifier="KNOWN")
    ]
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=True,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_and_spain(create_minimum_recipe_with_tomato: MinimumRecipeWithTomato) -> None:
    """In this case, the vegetable should not be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = [LocationProp(address="Spain", country_code="Es", location_qualifier="KNOWN")]
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=False,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_in_unknown_location(
    create_minimum_recipe_with_tomato: MinimumRecipeWithTomato,
) -> None:
    """In this case, the vegetable should not be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = None
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=False,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_carrot_in_netherlands(create_minimum_recipe: MinimumRecipeRType) -> None:
    """In this case, the vegetable should be grown in a greenhouse."""
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    carrot_ingredient.flow_location = [
        LocationProp(address="Netherlands", country_code="NL", location_qualifier="KNOWN")
    ]
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        carrot_ingredient,
        should_be_scheduled=True,
        can_run_now=False,
        vegetable_should_be_grown_in_greenhouse=True,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_in_netherlands_with_production_set_to_greenhouse(
    create_minimum_recipe_with_tomato: MinimumRecipeWithTomato,
) -> None:
    """In this case, the vegetable should be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = [
        LocationProp(address="Netherlands", country_code="NL", location_qualifier="KNOWN")
    ]
    tomato_ingredient.raw_production = {"value": "greenhouse", "language": "en"}
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=True,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_in_netherlands_with_production_set_to_organic(
    create_minimum_recipe_with_tomato: MinimumRecipeWithTomato,
) -> None:
    """In this case, the vegetable should be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = [
        LocationProp(address="Netherlands", country_code="NL", location_qualifier="KNOWN")
    ]
    tomato_ingredient.raw_production = {"value": "organic", "language": "en"}
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=True,
    )


@pytest.mark.asyncio
async def test_greenhouse_gfm_for_tomato_in_netherlands_with_production_set_to_standard(
    create_minimum_recipe_with_tomato: MinimumRecipeWithTomato,
) -> None:
    """In this case, the vegetable should be grown in a greenhouse."""
    postgres_db, service_provider, _, tomato_ingredient, _, _root_flow = create_minimum_recipe_with_tomato
    tomato_ingredient.flow_location = [
        LocationProp(address="Netherlands", country_code="NL", location_qualifier="KNOWN")
    ]
    tomato_ingredient.raw_production = {"value": "standard", "language": "en"}
    await set_up_and_run_greenhouse_gfm(
        postgres_db,
        service_provider,
        tomato_ingredient,
        should_be_scheduled=True,
        can_run_now=True,
        vegetable_should_be_grown_in_greenhouse=False,
    )
