"Test for link term to activity node gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingFactory
from structlog import get_logger

from core.domain.nodes import ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType, TermAccessGroupIdsRType
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


@pytest.mark.asyncio
async def test_link_term_to_activity_node_gfm(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    postgres_db, service_provider, _, carrot_ingredient, carrot_process, _root_node = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    # add carrot[Term] to carrot_ingredient, using a mutation
    carrot_term: Term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
        "A1791",
        foodex2_term_group_uid,
    )
    logger.debug("create PropListMutation to add product_name")
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=carrot_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=carrot_term.uid)]),
    )
    await calc_graph.apply_mutation(prop_mutation)

    # now run our GFM
    gap_filling_factory = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    # assert that this GFM has correctly liked carrot[Term] to carrot_process[Node]
    await gap_filling_worker.run(calc_graph)
    subs = carrot_ingredient.get_sub_nodes()
    assert subs
    assert len(subs) == 1, (
        f"could not find the process node {carrot_process} that should have been attached to the "
        f"carrot_ingredient {carrot_ingredient} by GFM {gap_filling_worker}"
    )
    added_carrot_process: Node = subs[0]
    assert added_carrot_process.uid == carrot_process.uid

    assert not added_carrot_process.get_sub_nodes(), "added_carrot_process should not have sub-proc. attached to it yet"


@pytest.mark.asyncio
async def test_link_term_to_activity_node_gfm_with_wrong_node_type(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    # default NodeType is 'recipe'
    child_of_root_node = ModeledActivityNode(uid=node_uid)

    # create the calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(child_of_root_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_link_term_to_activity_node_gfm_with_no_product_name(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    # running GFM's `should_be_scheduled` and `can_run_now` methods
    gap_filling_factory = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)

    assert gap_filling_worker.should_be_scheduled()

    # assert that the gap filling module tries to reschedule to wait for product_name prop:
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.reschedule

    # assume that the module was rescheduled 10 times:
    gap_filling_worker.reschedule_counter = 10

    # assert that the gap filling module cancels itself:
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.cancel

    # assert that this GFM has not been run:
    subs = carrot_ingredient.get_sub_nodes()
    assert subs == []
