from typing import Annotated, List, Optional
from uuid import UUID

from pydantic import BaseModel, ConfigDict, Field, field_validator

from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode

FlowNodeTypes = Annotated[object, FlowNode.subclasses_with_self()]
ActivityNodeTypes = Annotated[object, ActivityNode.subclasses_with_self()]


class NodeIDDto(BaseModel):
    uid: Optional[UUID] = Field(default=None)
    xid: Optional[str] = Field(default=None)
    access_group_xid: Optional[str] = Field(default=None)
    access_group_uid: Optional[UUID] = Field(default=None)
    model_config = ConfigDict(extra="forbid")


class RootWithSubFlowsDto(BaseModel):
    flow: Optional[FlowNodeTypes] = Field(default=None)
    activity: ActivityNodeTypes = Field()
    sub_flows: List[FlowNodeTypes] = Field(default=[])
    model_config = ConfigDict(extra="forbid")

    @field_validator("flow", mode="before")
    @staticmethod
    def root_flow_validator(v: FlowNodeTypes) -> FlowNodeTypes:
        if isinstance(v, FlowNode) or v is None:
            return v
        type_lookup = FlowNode.subclasses_with_self_as_dict()
        if v["node_type"] not in type_lookup:
            raise ValueError(f"Unknown node_type: {v['node_type']}")
        return type_lookup[v["node_type"]].model_validate(v)

    @field_validator("activity", mode="before")
    @staticmethod
    def activity_validator(v: ActivityNodeTypes) -> ActivityNodeTypes:
        if isinstance(v, ActivityNode):
            return v
        type_lookup = ActivityNode.subclasses_with_self_as_dict()
        if v["node_type"] not in type_lookup:
            raise ValueError(f"Unknown node_type: {v['node_type']}")
        return type_lookup[v["node_type"]].model_validate(v)

    @field_validator("sub_flows", mode="before")
    @staticmethod
    def flow_validator(v: List[FlowNodeTypes]) -> List[FlowNodeTypes]:
        type_lookup = FlowNode.subclasses_with_self_as_dict()
        deserialized = []
        for flow_item in v:
            if isinstance(flow_item, FlowNode):
                deserialized.append(flow_item)
                continue
            if flow_item["node_type"] not in type_lookup:
                raise ValueError(f"Unknown Flow node_type: {flow_item['node_type']}")
            deserialized.append(type_lookup[flow_item["node_type"]].model_validate(flow_item))
        return deserialized


class ExistingRootDto(BaseModel):
    existing_root: NodeIDDto = Field(default_factory=NodeIDDto)
    model_config = ConfigDict(extra="forbid")
