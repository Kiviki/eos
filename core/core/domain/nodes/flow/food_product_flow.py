from enum import Enum
from typing import Any, Dict, Literal, Optional, Union

from pydantic import Field
from structlog import get_logger
from typing_extensions import TypedDict

from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes.flow_node import FlowNode
from core.domain.props.animal_products_prop import AnimalProductsProp
from core.domain.props.index_prop import IndexProp
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.quantity_prop import QuantityProp
from core.domain.props.rainforest_critical_products_prop import RainforestCriticalProductsProp
from core.domain.props.raw_nutrient_values_per_100g import RawNutrientValuesPer100g
from core.domain.props.vitascore_prop import VitascoreProp

logger = get_logger()


class ProductionOptionsEnum(str, Enum):
    organic = "organic"
    bio = "bio"
    standard = "standard"
    greenhouse = "greenhouse"


class ConservationOptionsEnum(str, Enum):
    fresh = "fresh"
    not_conserved = "not conserved"
    cooled = "cooled"
    frozen = "frozen"
    canned = "canned"
    dried = "dried"


class ProcessingOptionsEnum(str, Enum):
    drying = "drying"
    dairy_drying = "dairy drying"
    grain_drying = "grain drying"
    legume_drying = "legume drying"
    nut_drying = "nut drying"
    oilseed_drying = "oilseed drying"
    spice_drying = "spice drying"
    fruit_drying = "fruit drying"
    vegetable_drying = "vegetable drying"
    fruit_jam_production = "fruit jam production"
    fruit_yoghurt_production_cow_milk = "fruit yoghurt production (cow milk)"
    fruit_yoghurt_production_soy_milk = "fruit yoghurt production (soy milk)"
    freezing = "freezing"
    fruit_juice_concentrate_production = "fruit juice concentrate production"
    tree_nut_chopping = "tree nut chopping"
    roasting = "roasting"
    nut_roasting = "nut roasting"
    dried_fruit_grinding = "dried fruit grinding"
    grinding = "grinding"
    fruit_juice_production = "fruit juice production"
    baking = "baking"
    transportation_for_processing = "transportation for processing"
    cooled_transportation_for_processing = "cooled transportation for processing"
    mixing = "mixing"
    heating = "heating"
    cooking_with_fat_or_oil = "cooking with fat or oil"
    uht_pasteurization = "uht pasteurization"
    artificial_carbonation = "artificial carbonation"
    cutting = "cutting"
    shredding = "shredding"
    fermenting = "fermenting"
    smoking = "smoking"
    puffing = "puffing"
    freeze_drying = "freeze-drying"
    cooling = "cooling"


class LabelsOptionsEnum(str, Enum):
    bio_suisse = "Bio Suisse"
    coop_naturaplan = "Coop Naturaplan"
    rain_forest_alliance_certified = "Rain Forest Alliance certified"
    max_havelaar_small_producer_organizations = "Max Havelaar Small Producer Organizations"
    max_havelaar_hired_labour = "Max Havelaar Hired labour"
    claro = "Claro"
    spar_natur_pur_ausland = "Spar Natur pur Ausland"
    migros_bio_ausland_max_havelaar = "Migros Bio Ausland + Max Havelaar"
    aldi_natur_aktiv_ausland_eu_bio_max_havelaar = "Aldi Natur Aktiv Ausland + EU Bio + Max Havelaar"
    demeter = "Demeter"
    naturland = "Naturland"
    wild_fish = "Wild Fish"
    migros_bio_ausland = "Migros Bio Ausland"
    eu_bio = "EU Bio"
    ab_agriculture_biologique = "AB Agriculture Biologique"
    biotrend_ausland = "Biotrend Ausland"
    aldi_natur_aktiv_ausland = "Aldi Natur Aktiv Ausland"
    usda_organic = "USDA Organic"
    organic = "organic"
    free_range = "free-range"
    grazing = "grazing"
    suckler_cow = "suckler-cow"
    sat = "SAT (Schwein artgerechte Tierhaltung)"
    rat = "RAT (Rind artgerechte Tierhaltung)"


class RawConservation(TypedDict):
    value: str | ConservationOptionsEnum
    language: str


class RawLabels(TypedDict):
    value: str | LabelsOptionsEnum
    language: str


class RawProduction(TypedDict):
    value: str | ProductionOptionsEnum
    language: str


class RawProcessing(TypedDict):
    value: str | ProcessingOptionsEnum
    language: str


class FoodProductFlowNode(FlowNode):
    aggregated_nutrients: Optional[QuantityPackageProp] = Field(default=None)
    animal_products: Optional[AnimalProductsProp] = Field(default=None)
    daily_food_unit: Optional[QuantityProp] = Field(default=None)
    declaration_index: Optional[IndexProp] = Field(default=None)
    default_max_evaporation: Optional[QuantityProp] = Field(default=None)
    food_categories: Optional[QuantityPackageProp] = Field(default=None)
    rainforest_critical_products: Optional[RainforestCriticalProductsProp] = Field(default=None)
    node_type: Literal["FoodProductFlowNode"] = Field(default="FoodProductFlowNode")
    nutrient_upscale_ratio: Optional[QuantityProp] = Field(default=None)
    nutrient_values: Optional[Union[RawNutrientValuesPer100g, QuantityPackageProp]] = Field(default=None)
    storage_time: Optional[QuantityProp] = Field(default=None)
    vitascore: Optional[VitascoreProp] = Field(default=None)
    vitascore_legacy: Optional[VitascoreProp] = Field(default=None)

    # Raw fields that we receive from the API:
    raw_conservation: Optional[RawConservation] = Field(default=None)
    raw_labels: Optional[RawLabels] = Field(default=None)
    raw_production: Optional[RawProduction] = Field(default=None)
    raw_processing: Optional[RawProcessing] = Field(default=None)

    is_subdivision: bool = Field(default=False)

    @property
    def tag_term_xids(self) -> frozenset[str]:
        """Property returning a set of xids of terms in glossary_tags."""
        # TODO Eventually, product_name and glossary_tags should be merged into glossary_tags.
        product_name_terms = self.product_name.terms if isinstance(self.product_name, NamesProp) else DeepListView([])
        glossary_tags = self.glossary_tags if self.glossary_tags else DeepListView([])
        return frozenset(p.get_term().xid for p in list(product_name_terms) + list(glossary_tags))

    def is_dried(self) -> bool:
        """Function returning whether dried term (with xid J0116) is present as a part of glossary_tags."""
        # TODO Eventually, product_name and glossary_tags should be merged into glossary_tags.
        if self.product_name and isinstance(self.product_name, NamesProp):
            if "J0116" in [tag.get_term().xid for tag in self.product_name.terms]:
                return True

        if self.glossary_tags is None:
            return False
        elif isinstance(self.glossary_tags, str):
            raise ValueError(
                f"Dried term not in product_name and {self.glossary_tags} has not been processed by Kale yet."
            )
        else:
            return "J0116" in [tag.get_term().xid for tag in self.glossary_tags]

    def is_combining_ingredients(self) -> bool:
        """Method returning whether the product is combining ingredients."""
        for sub_activity in self.get_sub_nodes():
            if not sub_activity.get_sub_nodes():
                return False

            if (
                len(
                    [
                        sub_flow
                        for sub_flow in sub_activity.get_sub_nodes()
                        if (
                            isinstance(sub_flow, FoodProductFlowNode)
                            and not sub_flow.is_subdivision
                            and sub_flow.product_name != self.product_name
                        )
                    ]
                )
                > 1
            ):
                return True
        return False

    def is_combined_product(self) -> Optional[bool]:
        """Checks if a product is tagged to be a combined product, a monoproduct or not yet determined."""
        tag_term_xids = self.tag_term_xids
        if "EOS_COMBINED_PRODUCT" in tag_term_xids:
            return True
        elif "EOS_MONO_PRODUCT" in tag_term_xids:
            return False
        else:
            return None

    @property
    def _additional_info_useful_for_debugging(self) -> Dict[str, Any]:
        """Method that generates a dict of additional info useful for debugging.

        This method can be overridden by subclasses.

        returns: Dict[str, Any] of additional info useful for debugging
        """
        additional_info = super()._additional_info_useful_for_debugging
        # is_subdivision
        if self.is_subdivision:
            additional_info["is_subdivision"] = True
        else:
            additional_info["is_subdivision"] = False
        # is_dried
        if self.is_dried():
            additional_info["is_dried"] = True
        else:
            additional_info["is_dried"] = False
        return additional_info
