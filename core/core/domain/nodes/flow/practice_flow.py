"""Practice Flow Nodes.

Implements the Node type for practices in Hestia's glossary.
"""
from typing import Literal

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.flow_node import FlowNode

logger = get_logger()


class PracticeFlowNode(FlowNode):
    """Node used for flows of practices."""

    node_type: Literal["PracticeFlowNode"] = Field(default="PracticeFlowNode")
