from typing import Type

from core.domain.nodes.activity.emission import ElementaryResourceEmissionNode
from core.domain.nodes.activity.food_processing_activity import FoodProcessingActivityNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.activity.supply_sheet_activity import SupplySheetActivityNode
from core.domain.nodes.activity.transport_activity import TransportActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow.food_product_flow import FoodProductFlowNode
from core.domain.nodes.flow.practice_flow import PracticeFlowNode
from core.domain.nodes.flow_node import FlowNode


def node_class_from_type(node_type: str) -> Type:
    # TODO: this might be better implemented dynamically like in the props module...
    match node_type:
        case ElementaryResourceEmissionNode.__name__:
            return ElementaryResourceEmissionNode
        case ModeledActivityNode.__name__:
            return ModeledActivityNode
        case FoodProductFlowNode.__name__:
            return FoodProductFlowNode
        case PracticeFlowNode.__name__:
            return PracticeFlowNode
        case FoodProcessingActivityNode.__name__:
            return FoodProcessingActivityNode
        case TransportActivityNode.__name__:
            return TransportActivityNode
        case SupplySheetActivityNode.__name__:
            return SupplySheetActivityNode
        case FlowNode.__name__:
            return FlowNode
        case ActivityNode.__name__:
            return ActivityNode
