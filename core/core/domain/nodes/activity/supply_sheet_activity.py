"""Node type for processing a supply order sheet (a collect of supply items)."""

from typing import Literal

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.activity_node import ActivityNode

logger = get_logger()


class SupplySheetActivityNode(ActivityNode):
    node_type: Literal["SupplySheetActivityNode"] = Field(default="SupplySheetActivityNode")
