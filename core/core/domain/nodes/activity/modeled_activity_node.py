from typing import Literal, Optional

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.activity_node import ActivityNode
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp

logger = get_logger()


class ModeledActivityNode(ActivityNode):
    node_type: Literal["ModeledActivityNode"] = Field(default="ModeledActivityNode")
    aggregated_cache: Optional[EnvironmentalFlowsProp] = Field(default=None)
