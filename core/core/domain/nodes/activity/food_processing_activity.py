from typing import Literal, Optional

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.activity_node import ActivityNode
from core.domain.props import IngredientAmountEstimatorStatusProp, QuantityPackageProp
from core.domain.props.names_prop import RawName

logger = get_logger()


class FoodProcessingActivityNode(ActivityNode):
    node_type: Literal["FoodProcessingActivityNode"] = Field(default="FoodProcessingActivityNode")
    ingredients_declaration: Optional[list[RawName]] = Field(default=None)
    ingredient_amount_estimator_solution_status: Optional[IngredientAmountEstimatorStatusProp] = Field(default=None)
    ingredient_amount_estimator_error_squares: Optional[QuantityPackageProp] = Field(default=None)
    ingredient_amount_estimator_estimated_nutrients: Optional[QuantityPackageProp] = Field(default=None)
