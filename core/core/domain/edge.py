import uuid
from enum import Enum


class EdgeTypeEnum(str, Enum):
    root_to_recipe = "root_to_recipe"
    ingredient = "ingredient"
    link_to_subrecipe = "link_to_subrecipe"
    lca_link = "lca_link"
    """ A link between LCA activities and flows (nota: Brightway flows/exchanges are modelled as EOS Nodes) """


class Edge:
    def __init__(self, parent_uid: uuid, child_uid: uuid, edge_type: EdgeTypeEnum = EdgeTypeEnum.ingredient):
        if child_uid == parent_uid:
            raise ValueError("edge child and parent are the same. this is not allowed.")

        self.parent_uid: uuid = parent_uid
        self.child_uid: uuid = child_uid
        self.edge_type: EdgeTypeEnum = edge_type

    def __repr__(self):
        return f"Edge(parent_uid=${self.parent_uid}, child_uid=${self.child_uid}, edge_type=${self.edge_type})"
