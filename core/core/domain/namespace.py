from dataclasses import dataclass
from typing import Optional


@dataclass
class Namespace:
    name: str
    uid: Optional[str] = None
