from enum import Enum
from typing import Literal, Optional

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.prop import Prop


class IngredientTypeDtoEnum(str, Enum):
    conceptual_ingredients = "conceptual-ingredients"
    recipes = "recipes"


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class LegacyAPIInformationProp(Prop):
    prop_type: Literal["LegacyAPIInformationProp"] = Field(default="LegacyAPIInformationProp")

    type: Optional[IngredientTypeDtoEnum] = None
