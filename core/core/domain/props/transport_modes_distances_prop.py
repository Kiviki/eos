"""Module containing property with node information."""
import typing
from typing import Any, Dict, List, Literal, Optional, cast

from gap_filling_modules.transportation_util.enum import ServiceTransportModeEnum
from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class TransportDataset(Prop):
    """Data container for transport modes distances information."""

    prop_type: Literal["TransportDataset"] = Field(default="TransportDataset")

    main_carriage: ReferencelessQuantityProp = Field()
    pre_carriage: ReferencelessQuantityProp = Field()
    post_carriage: ReferencelessQuantityProp = Field()
    total: Optional[ReferencelessQuantityProp] = Field(default=None)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        dump_dict = {
            slot: getattr(self, slot).model_dump_base(exclude=exclude) if getattr(self, slot) is not None else None
            for slot in self.__slots__
        }
        return dump_dict

    def __eq__(self, other: object) -> bool:
        """Compare other objects with TransportModesDistancesProp."""
        if type(self) is type(other):
            other = typing.cast(TransportDataset, other)
            return (
                self.main_carriage == other.main_carriage
                and self.pre_carriage == other.pre_carriage
                and self.post_carriage == other.post_carriage
                and self.total == other.total
            )
        return False


def get_referenceless_quantity_prop(qty: dict | ReferencelessQuantityProp) -> ReferencelessQuantityProp:
    if isinstance(qty, ReferencelessQuantityProp):
        return qty
    else:
        return ReferencelessQuantityProp(**qty)


def compose_transport_data_set(data_set: dict) -> TransportDataset:
    if data_set.get("total"):
        total_transport_data = get_referenceless_quantity_prop(data_set["total"])
    else:
        total_transport_data = None

    return TransportDataset.unvalidated_construct(
        main_carriage=get_referenceless_quantity_prop(data_set["main_carriage"]),
        pre_carriage=get_referenceless_quantity_prop(data_set["pre_carriage"]),
        post_carriage=get_referenceless_quantity_prop(data_set["post_carriage"]),
        total=total_transport_data,
    )


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class TransportModesDistancesProp(Prop):
    """Property containing Transport modes distances information."""

    prop_type: Literal["TransportModesDistancesProp"] = Field(default="TransportModesDistancesProp")

    distances: Dict[ServiceTransportModeEnum, TransportDataset] = Field()
    co2: Dict[ServiceTransportModeEnum, TransportDataset] = Field()
    qualified_modes: Optional[List[ServiceTransportModeEnum]] = Field(default=None)
    cheapest_mode: Optional[ServiceTransportModeEnum] = Field(default=None)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize the entire model."""
        output = {
            "prop_type": self.prop_type,
            "distances": {key: val.model_dump_base(exclude=exclude) for key, val in self.distances.items()},
            "co2": {key: val.model_dump_base(exclude=exclude) for key, val in self.co2.items()},
        }

        for field_name in ("qualified_modes", "cheapest_mode", "source_data_raw"):
            if getattr(self, field_name):
                output[field_name] = getattr(self, field_name)

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in TransportModesDistancesProp using data dictionary."""
        self.distances = {key: compose_transport_data_set(val) for key, val in data["distances"].items()}
        self.co2 = {key: compose_transport_data_set(val) for key, val in data["co2"].items()}

        for field_name in ("qualified_modes", "cheapest_mode", "source_data_raw"):
            if data.get(field_name):
                setattr(self, field_name, data[field_name])

    def __eq__(self, other: object) -> bool:
        """Compare other objects with TransportModesDistancesProp."""
        if type(self) is type(other):
            other_prop = cast(TransportModesDistancesProp, other)
            return (
                self.co2 == other_prop.co2
                and self.distances == other_prop.distances
                and self.cheapest_mode == other_prop.cheapest_mode
                and self.qualified_modes == other_prop.qualified_modes
            )
        return False
