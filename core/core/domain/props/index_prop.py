"""Module containing property with a single integer."""

from typing import Any, Dict, Literal, Type

from pydantic import ConfigDict, Field, field_validator
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class IndexProp(Prop):
    """Property containing a single index."""

    prop_type: Literal["IndexProp"] = Field(default="IndexProp")

    value: int = Field()

    @field_validator("value")
    @classmethod
    def validate_index(cls: Type["IndexProp"], value: int) -> int:
        """Validate language code of names."""
        if value < 0:
            raise ValueError(f"Only whole number indices starting from 0 are allowed. Instead, {value} was given.")
        else:
            return value

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in IndexProp using data dictionary."""
        self.value = data["value"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with IndexProp."""
        if type(self) is type(other):
            return self.value == other.value
        return False
