"""Quantities (value & unit) without a reference. Should not be a part of Node class."""

from typing import Any, Dict, Literal, Set, Union
from uuid import UUID

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs

from core.domain.prop import Prop
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.term import Term


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class ReferencelessQuantityProp(Prop):
    """Property for quantities without a reference. Should not be a part of Node class."""

    prop_type: Literal["ReferencelessQuantityProp"] = Field(default="ReferencelessQuantityProp")

    value: float = Field()
    unit_term_uid: UUID = Field()

    @property
    def _props_to_remove_from_repr(self) -> Set[str]:
        """Returns a set of properties to be excluded from __repr__."""
        return {"_owner_node", "prop_type", "unit_term_uid", "unit_term_xid"}

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls, values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        # these are included when serializing the model, but we only use term_uid to define the term to be used:
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values
        if "unit_term_xid" in values_kwargs:
            del values_kwargs["unit_term_xid"]
        if "unit_term_name" in values_kwargs:
            del values_kwargs["unit_term_name"]
        return values

    def model_dump_extra(self, dump_dict: Dict[str, Any]) -> Dict[str, Any]:
        """Additional ReferencelessQuantityProp information is serialized into the dump dictionary."""
        unit_term = self.get_unit_term()
        dump_dict["unit_term_uid"] = str(self.unit_term_uid)
        dump_dict["unit_term_xid"] = str(unit_term.xid)
        dump_dict["unit_term_name"] = str(unit_term.name)
        return dump_dict

    def get_unit_term(self) -> Term:
        """Obtain term associated with the unit."""
        return GlossaryTermProp.get_term_from_uid(self.unit_term_uid)

    def referenceless_quantitiy_from_dict(self, data: dict) -> None:
        """Compose class from dict."""
        self.value = data["value"]
        if isinstance(data["unit_term_uid"], str):
            self.unit_term_uid = UUID(data["unit_term_uid"])
        else:
            self.unit_term_uid = data["unit_term_uid"]

    def from_dict(self, data: dict) -> None:
        """Wrapper to avoid super()."""
        self.referenceless_quantitiy_from_dict(data)
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object):
        """Check for equality of two ReferencelessQuantityProp."""
        if type(other) is type(self):
            if self.unit_term_uid != other.unit_term_uid:
                return False
            if self.value != other.value:
                return False
            return True
        else:
            return False
