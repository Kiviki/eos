# ruff: noqa: F401 Import all props without using such that other modules can easily import all Props from this module.

from typing import Union

from pydantic import BaseModel, Field

from core.domain.prop import Prop
from core.domain.props.animal_products_prop import AnimalProductsProp
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.index_prop import IndexProp
from core.domain.props.ingredient_amount_estimator_status_prop import IngredientAmountEstimatorStatusProp
from core.domain.props.json_prop import JsonProp
from core.domain.props.legacy_api_information_prop import LegacyAPIInformationProp
from core.domain.props.location_prop import LocationProp
from core.domain.props.matrix_gfm_error_prop import MatrixGfmErrorProp
from core.domain.props.names_prop import NamesProp
from core.domain.props.origin_data_prop import OriginDataProp
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.quantity_prop import QuantityProp
from core.domain.props.rainforest_critical_products_prop import RainforestCriticalProductsProp
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp
from core.domain.props.transport_modes_distances_prop import TransportModesDistancesProp
from core.domain.props.vitascore_prop import VitascoreProp


class AnyPropWrapper(BaseModel):
    prop: Union[Prop, Union[Prop.get_subclasses()]] = Field(discriminator="prop_type")


def prop_from_dict(data: dict) -> Prop:
    deserialized = AnyPropWrapper(prop=data)
    return deserialized.prop
