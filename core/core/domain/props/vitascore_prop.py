"""Quantity packages (e.g., nutrient values) with multiple quantities (with a common reference)."""

from typing import Any, Dict, Literal, Optional, Type, Union
from uuid import UUID

from pydantic import ConfigDict, Field, field_validator, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs

from core.domain.prop import Prop
from core.domain.props.quantity_package_prop import QuantityPackageProp, to_uuid
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class VitascoreProp(Prop):
    """Property specific to Vitascore.

    QuantityPackageProp cannot be used because Vitascore does not scale linearly with
    the reference amount. For example, the vitascore in 100g of red meat is not doulbe the vitascore in 50g of red meat.
    """

    prop_type: Literal["VitascoreProp"] = Field(default="VitascoreProp")

    quantities: Dict[str, Dict[UUID, ReferencelessQuantityProp]] = Field()

    @field_validator("quantities")
    @classmethod
    def validate_keys(cls: Type["VitascoreProp"], value: dict) -> dict:
        """Validate keys of quantities field."""
        invalid_keys = set(value.keys()) - set(ReferenceAmountEnum)

        if invalid_keys:
            raise ValueError(
                f"Invalid keys: {', '.join(invalid_keys)}. Only {', '.join(ReferenceAmountEnum)} are allowed."
            )

        return value

    @classmethod
    def pre_init_modifications(cls: Type["VitascoreProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        # name and xid of the quantity term are included when serializing, but removed for deserialization:
        try:
            if isinstance(values, ArgsKwargs):
                values_kwargs = values.kwargs
            else:
                values_kwargs = values

            if "quantities" in values_kwargs:
                return values

            keys_to_delete = []

            values_kwargs["quantities"] = {}
            for key, val in values_kwargs.items():
                if key in set(ReferenceAmountEnum):
                    keys_to_delete.append(key)
                    values_kwargs["quantities"][key] = {
                        to_uuid(term_uid): qty if isinstance(qty, ReferencelessQuantityProp) else qty["quantity"]
                        for term_uid, qty in val.items()
                    }

            for key in keys_to_delete:
                if key in values_kwargs:
                    del values_kwargs[key]
        except ValueError:
            pass

        return values

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["VitascoreProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        return cls.pre_init_modifications(values)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize model into dict."""
        _ = exclude

        output = {"prop_type": self.prop_type}
        for key, val in self.quantities.items():
            output[key] = QuantityPackageProp.serialize_quantities(val)
        return output

    def from_dict(self, data: dict) -> None:
        """Construct model from dict."""
        new_data = self.pre_init_modifications(data)
        new_quantities = {}
        for reference_key, val in new_data["quantities"].items():
            new_quantities[reference_key] = {
                key: qty if isinstance(qty, ReferencelessQuantityProp) else ReferencelessQuantityProp(**qty)
                for key, qty in val.items()
            }
        self.quantities = new_quantities
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object):
        """Check for equality of two VitascoreProps."""
        if type(self) is type(other):
            for for_reference, qty_for_reference in self.quantities.items():
                if for_reference not in other.quantities:
                    return False
                else:
                    for qty_uid, qty in qty_for_reference.items():
                        if qty_uid not in other.quantities[for_reference]:
                            return False
                        elif qty != other.quantities[for_reference][qty_uid]:
                            return False
            return True
        else:
            return False
