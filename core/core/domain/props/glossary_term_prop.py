"""Modeule containing property containing a glossary term."""

import uuid
from enum import Enum
from typing import Any, Dict, Literal, Optional, Union

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs

from core.domain.prop import Prop
from core.domain.term import Term


class SourceEnum(str, Enum):
    api: str = "api"
    product_name: str = "product_name"
    edb_database: str = "edb_database"
    eos_assumed: str = "eos_assumed"

    def __repr__(self) -> str:
        return self.value


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class GlossaryTermProp(Prop):
    """Property containing a glossary term.

    Only the term_uid is stored, and it is serialized into term_xid and term_name when serializing.
    """

    prop_type: Literal["GlossaryTermProp"] = Field(default="GlossaryTermProp")
    term_uid: uuid.UUID = Field()

    source: Optional[SourceEnum] = Field(default=None)

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls, values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        """Modify initialize kwargs to pass validation."""
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        if "term_xid" in values_kwargs:
            del values_kwargs["term_xid"]
        if "term_name" in values_kwargs:
            del values_kwargs["term_name"]
        return values

    def model_dump_extra(self, dump_dict: Dict[str, Any]) -> Dict[str, Any]:
        """Additional GlossaryTermProp information is serialized into the dump dictionary."""
        if self.term_uid is not None:
            term = self.get_term()
            dump_dict["term_name"] = term.name
            dump_dict["term_xid"] = term.xid
            dump_dict["term_uid"] = str(self.term_uid)
        return dump_dict

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in GlossaryTermProp using data dictionary."""
        if isinstance(data["term_uid"], str):
            self.term_uid = uuid.UUID(data["term_uid"])
        else:
            self.term_uid = data["term_uid"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def get_term(self) -> Optional[Term]:
        """Obtain associated glossary term."""
        return self.get_term_from_uid(self.term_uid)

    def __eq__(self, other: object) -> bool:
        """Compare other objects with GlossaryTermProp."""
        if type(other) is type(self):
            return self.term_uid == other.term_uid
        return False

    @staticmethod
    def get_term_from_uid(term_uid: Optional[uuid.UUID]) -> Optional[Term]:
        """Obtain term associated with term_uid."""
        if not term_uid:
            return None
        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        term = glossary_service.get_term_by_id(term_uid)
        return term
