"""Animal products prop."""
from typing import Any, Dict, Literal, Optional, Type, Union

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.quantity_package_prop import QuantityPackageProp

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class AnimalProductsProp(QuantityPackageProp):
    """Quantity package property with multiple quantities (with a common reference)."""

    prop_type: Literal["AnimalProductsProp"] = Field(default="AnimalProductsProp")

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["AnimalProductsProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        values = QuantityPackageProp.pre_init_modifications(values)
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        if "animal_welfare_rating" in values_kwargs:
            del values_kwargs["animal_welfare_rating"]
        return values

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        output = QuantityPackageProp.model_dump_base(self, exclude=exclude)
        output["animal_welfare_rating"] = self.get_animal_welfare_rating()
        return output

    def get_animal_welfare_rating(self) -> int | None:
        # Retrieve amount for 100g such that we can do cutoff based on percentages.
        try:
            quantities_for_100g = self.amount_for_100g()
        except (ValueError, TypeError, AttributeError):
            quantities_for_100g = None

        if not quantities_for_100g:
            logger.debug(f"Cannot retrieve rainforest rating for owner node [{self.get_owner_node()}].")
            return None

        amounts: dict = {
            "EOS_animal_welfare_certified": 0.0,
            "EOS_not_certified_for_animal_welfare": 0.0,
        }

        for key, val in quantities_for_100g.quantities.items():
            term = GlossaryTermProp.get_term_from_uid(key)
            unit = val.get_unit_term()
            assert unit.data.get("mass-in-g")
            mass_in_g = val.value * unit.data.get("mass-in-g")
            amounts[term.xid] += mass_in_g

        is_animal_welfare_certified_present: bool = False
        is_not_certified_for_animal_welfare_present: bool = False

        cutoff_value = 1.0

        if amounts["EOS_animal_welfare_certified"] > cutoff_value:
            is_animal_welfare_certified_present = True

        if amounts["EOS_not_certified_for_animal_welfare"] > cutoff_value:
            is_not_certified_for_animal_welfare_present = True

        if not is_not_certified_for_animal_welfare_present:
            return 3
        elif is_animal_welfare_certified_present and is_not_certified_for_animal_welfare_present:
            return 2
        else:
            return 1

    def __eq__(self, other: object):
        """Check for equality of two QuantityPackageProps."""
        if type(self) is type(other):
            if self.for_reference != other.for_reference:
                return False
            if self.prop_term_uid != other.prop_term_uid:
                return False
            for qty_uid, qty in self.quantities.items():
                if qty_uid not in other.quantities:
                    return False
                elif qty != other.quantities[qty_uid]:
                    return False
            return True
        else:
            return False
