from typing import Literal

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.prop import Prop


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class LinkToXidProp(Prop):
    prop_type: Literal["LinkToXidProp"] = Field(default="LinkToXidProp")
    xid: str = Field()
    duplicate_sub_node: bool = Field(default=True)
