"""Module containing property with environmental flows."""
import re
from typing import TYPE_CHECKING, Any, Dict, Literal, Optional, Set, Union, cast
from uuid import UUID

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.quantity_prop import RawQuantity
from core.domain.term import Term

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph

logger = get_logger()


def to_uuid(uid: Optional[Union[str, UUID]]) -> Optional[UUID]:
    """Convert string or UUID into UUID."""
    if isinstance(uid, UUID):
        return uid
    elif isinstance(uid, str):
        return UUID(uid)
    elif uid is None:
        return None
    else:
        raise ValueError(f"{uid} is neither a UUID nor a string.")


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class EnvironmentalFlowsProp(Prop):
    """Property containing environmental flows information as dictionary of {node_uid:ReferencelessQuantityProp}."""

    prop_type: Literal["EnvironmentalFlowsProp"] = Field(default="EnvironmentalFlowsProp")

    # choose efficient data type for flow quantities, because there exist a lot of them and add up to huge memory:
    flow_quantities: Dict[UUID, float] = Field()  # i.e. {emission_node_uid: qty_value}

    calculated_using_eos_commit: Optional[str] = Field(default=None)
    is_most_shallow_node_for_gfms: Optional[list[str]] = Field(default_factory=list)
    has_sub_nodes_that_can_change_dynamically_for_gfms: Optional[dict[str, bool]] = Field(default_factory=dict)

    @staticmethod
    def emission_node_uid_to_production_amount_unit(
        emission_node_uid: UUID, calc_graph: Optional["CalcGraph"] = None
    ) -> Term:
        if calc_graph:
            emission_node = calc_graph.get_node_by_uid(emission_node_uid)
            if emission_node:
                return emission_node.production_amount.get_unit_term()

        # fallback option if the calc graph is not available:
        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        node_service = service_provider.node_service
        glossary_service = service_provider.glossary_service

        emission_node = node_service.cache.get_cached_node_by_uid(emission_node_uid)

        # Assume the importer did not yet convert the production_amount to a ReferencelessQuantityProp
        assert isinstance(emission_node.production_amount, RawQuantity)

        unit_string: str = emission_node.production_amount.unit
        if unit_string.upper() in glossary_service.alias_to_xid:
            unit_term_xid = glossary_service.alias_to_xid[unit_string.upper()]
        else:
            unit_term_xid = f"EOS_{re.sub(' ', '-', unit_string.lower())}"

        root_unit_term = glossary_service.root_subterms.get("EOS_units")
        unit_term_access_group_uid = root_unit_term.access_group_uid

        unit_term: Term | None = glossary_service.terms_by_xid_ag_uid[
            (
                unit_term_xid,
                unit_term_access_group_uid,
            )
        ]
        return unit_term

    @property
    def _props_to_add_to_repr(self) -> Dict[str, Any]:
        """Returns a dict of properties to be add to __repr__."""
        return {"len(flow_quantities)": len(self.flow_quantities)}

    @property
    def _props_to_remove_from_repr(self) -> Set[str]:
        """Returns a set of properties to be excluded from __repr__."""
        return {"_owner_node", "prop_type", "flow_quantities"}

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize the entire model."""
        output = {"prop_type": self.prop_type}

        # we want to prevent serializing the flow_quantities if they requested to be excluded since they may be large
        if not exclude or "flow_quantities" not in exclude:
            output["flow_quantities"] = {str(key): qty for key, qty in self.flow_quantities.items()}
        if self.calculated_using_eos_commit:
            output["calculated_using_eos_commit"] = self.calculated_using_eos_commit
        if self.is_most_shallow_node_for_gfms:
            output["is_most_shallow_node_for_gfms"] = self.is_most_shallow_node_for_gfms
        if self.has_sub_nodes_that_can_change_dynamically_for_gfms:
            output[
                "has_sub_nodes_that_can_change_dynamically_for_gfms"
            ] = self.has_sub_nodes_that_can_change_dynamically_for_gfms

        if self.source_data_raw:
            output["source_data_raw"] = self.source_data_raw
        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]

        return output

    def from_dict(self, data: dict) -> None:
        """Construct model from dict."""
        self.flow_quantities = {UUID(key): qty for key, qty in data["flow_quantities"].items()}
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with EnvironmentalFlowsProp."""
        if type(self) is type(other):
            other_prop = cast(EnvironmentalFlowsProp, other)
            if len(self.flow_quantities) != len(other_prop.flow_quantities):
                return False
            for qty_uid, qty in self.flow_quantities.items():
                if qty_uid not in other_prop.flow_quantities:
                    return False
                elif qty != other_prop.flow_quantities[qty_uid]:
                    return False
            return True
        else:
            return False
