"""Module containing property with node information."""

from typing import Any, Dict, Literal, Type

from gap_filling_modules.abstract_util.enum import NodeGfmStateEnum
from pydantic import ConfigDict, Field, field_validator
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class GfmStateProp(Prop):
    """Property containing Gfm states."""

    prop_type: Literal["GfmStateProp"] = Field(default="GfmStateProp")

    worker_states: Dict[str, NodeGfmStateEnum] = Field()

    @field_validator("worker_states")
    @classmethod
    def validate_workers(cls: Type["GfmStateProp"], value: Dict[str, NodeGfmStateEnum]) -> Dict[str, NodeGfmStateEnum]:
        """Validate whether module name is one of gap-filling-modules."""
        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        gfm_workers = service_provider.gap_filling_module_loader.gfm_workers

        invalid_keys = set(value.keys()) - gfm_workers

        if invalid_keys:
            raise ValueError(f"{list(value.keys())} contains unrecognized gap filling workers.")
        else:
            return value

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in GfmStateProp using data dictionary."""
        self.worker_states = data["worker_states"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with GfmStateProp."""
        if type(other) is type(self):
            return self.worker_states == other.worker_states
        return False
