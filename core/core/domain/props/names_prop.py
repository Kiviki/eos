"""Module containing property with node names information."""

from typing import Any, Dict, List, Literal, Optional

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger
from typing_extensions import TypedDict

from core.domain.prop import Prop
from core.domain.props import GlossaryTermProp

logger = get_logger()


class RawName(TypedDict):
    language: str
    value: str


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class NamesProp(Prop):
    """Property containing node names information."""

    prop_type: Literal["NamesProp"] = Field(default="NamesProp")

    terms: List[GlossaryTermProp] = Field()

    source_data_raw: Optional[list[RawName]] = Field(default=None)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize the entire model."""
        output = {
            "prop_type": self.prop_type,
            "terms": [glossary_term.model_dump_base(exclude=exclude) for glossary_term in self.terms],
        }

        if self.source_data_raw:
            output["source_data_raw"] = self.source_data_raw

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in NamesProp using data dictionary."""
        self.terms = [
            glossary_term if isinstance(glossary_term, GlossaryTermProp) else GlossaryTermProp(**glossary_term)
            for glossary_term in data["terms"]
        ]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with NamesProp."""
        if type(self) is type(other):
            return self.terms == other.terms
        return False
