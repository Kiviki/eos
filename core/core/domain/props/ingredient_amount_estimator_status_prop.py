"""Module containing a property with ingredient amount estimator status."""

from enum import Enum
from typing import Any, Dict, Literal

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop

logger = get_logger()


class IngredientAmountEstimatorStatusEnum(str, Enum):
    optimal = "optimal"
    optimal_inaccurate = "optimal_inaccurate"
    infeasible = "infeasible"
    infeasible_inaccurate = "infeasible_inaccurate"
    unbounded = "unbounded"
    unbounded_inaccurate = "unbounded_inaccurate"

    def __repr__(self) -> str:
        return self.value


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class IngredientAmountEstimatorStatusProp(Prop):
    """Property containing ingredient amount estimator status."""

    prop_type: Literal["IngredientAmountEstimatorStatusProp"] = Field(default="IngredientAmountEstimatorStatusProp")

    value: IngredientAmountEstimatorStatusEnum = Field()

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in IngredientAmountEstimatorStatusProp using data dictionary."""
        self.value = data["value"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with IngredientAmountEstimatorStatusProp."""
        if type(other) is type(self):
            return self.value == other.value
        return False
