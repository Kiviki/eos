"""Quantity property containing value, unit, and reference amount."""
from enum import Enum
from typing import Any, Dict, Literal, Optional, Type, Union

import numpy as np
from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp

logger = get_logger()


class ReferenceAmountEnum(str, Enum):
    """Enum for reference amounts."""

    # Amount for root node.
    amount_for_root_node = "amount_for_root_node"

    # Amount for local activity production amount.
    # For flow node properties, it is for parent activity production amount.
    amount_for_activity_production_amount = "amount_for_activity_production_amount"

    # Amount for 100g of flow product.
    # For activity node properties, it is 100g of parent flow
    # (amount_for_100g should only be relevant for flow nodes in the future when root node is a flow node).
    amount_for_100g = "amount_for_100g"

    # Specifically for production_amount property, which references itself.
    self_reference = "self_reference"

    def __repr__(self) -> str:
        return self.value


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class RawQuantity(Prop):
    """Raw quantity dataclass."""

    prop_type: Literal["RawQuantity"] = Field(default="RawQuantity")

    value: float = Field()
    unit: Optional[str] = Field(default=None)

    def short_string_representation(self, precision_for_representing_floats: int = 2) -> str:
        """Short string representation of LocationProp for debugging."""
        unit_repr = self.unit
        if not self.unit:
            unit_repr = "[unit not (yet) determined]"

        return f"{np.round(self.value, precision_for_representing_floats)} {unit_repr}"

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Method to dump RawQuantity into a dictionary."""
        output = {"value": self.value}
        if self.unit:
            output["unit"] = self.unit

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class QuantityProp(ReferencelessQuantityProp):
    """Quantity property with reference amount."""

    prop_type: Literal["QuantityProp"] = Field(default="QuantityProp")
    for_reference: ReferenceAmountEnum = Field()

    @classmethod
    def pre_init_modifications(cls: Type["QuantityProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        # name and xid of the quantity term are included in serialization, but removed for deserialization:
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        keys_to_delete = ["unit_term_xid", "unit_term_name"]
        for_reference = None
        for key in values_kwargs.keys():
            if key in set(ReferenceAmountEnum):
                if not for_reference:
                    for_reference = key
                keys_to_delete.append(key)

        if not for_reference:
            return values

        value_for_reference = values_kwargs[for_reference]

        values_kwargs["for_reference"] = for_reference
        values_kwargs["value"] = value_for_reference

        for key in keys_to_delete:
            if key in values_kwargs:
                del values_kwargs[key]

        return values

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["QuantityProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        return cls.pre_init_modifications(values)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Model dump has to be defined as __slots__ only sees the slots in itself not of parents."""
        try:
            requested_references = self._owner_node.get_calculation().requested_quantity_references
        except AttributeError:
            requested_references = []

        if len(requested_references) == 0 or requested_references == self.for_reference:
            output = {
                self.for_reference.value
                if isinstance(self.for_reference, ReferenceAmountEnum)
                else self.for_reference: self.value
            }
        elif len(requested_references) == 1:
            try:
                amount_for_reference = getattr(self, requested_references[0])()
                output = {
                    requested_references[0].value
                    if isinstance(requested_references[0], ReferenceAmountEnum)
                    else requested_references[0]: amount_for_reference.value
                }
            except (TypeError, ValueError):
                output = {
                    self.for_reference.value
                    if isinstance(self.for_reference, ReferenceAmountEnum)
                    else self.for_reference: self.value
                }
        else:
            output = {}
            for req_ref in requested_references:
                try:
                    amount_for_reference = getattr(self, req_ref)()
                    if isinstance(req_ref, ReferenceAmountEnum):
                        dict_key = req_ref.value
                    else:
                        dict_key = req_ref
                    output[dict_key] = amount_for_reference.value
                except (TypeError, ValueError):
                    pass
            if not output:
                output = {
                    self.for_reference.value
                    if isinstance(self.for_reference, ReferenceAmountEnum)
                    else self.for_reference: self.value
                }

            if not output:
                output = {
                    self.for_reference.value
                    if isinstance(self.for_reference, ReferenceAmountEnum)
                    else self.for_reference: self.value
                }

        if hasattr(self, "model_dump_extra"):
            self.model_dump_extra(output)

        output["prop_type"] = self.prop_type
        if self.source_data_raw:
            output["source_data_raw"] = self.source_data_raw

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def from_dict(self, data: dict) -> None:
        """Compose class from dict.

        :param data: dictionary data.
        """
        new_data = self.pre_init_modifications(data)
        self.referenceless_quantitiy_from_dict(new_data)
        self.for_reference = new_data["for_reference"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def amount_for_reference(self, target_reference: ReferenceAmountEnum) -> Union["QuantityProp", None]:
        """Get amount_for_target_reference."""
        if self.for_reference == target_reference:
            return self

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        gram_term = glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

        if not (
            self.for_reference == ReferenceAmountEnum.amount_for_100g
            or target_reference == ReferenceAmountEnum.amount_for_100g
        ):
            converted_value = self._owner_node.convert_quantity_between_references(
                self.value, self.for_reference, target_reference
            )
        elif self.for_reference == ReferenceAmountEnum.amount_for_100g:
            converted_value = self._owner_node.convert_quantity_between_references(
                self.value, ReferencelessQuantityProp(value=100.0, unit_term_uid=gram_term.uid), target_reference
            )
        else:  # target_reference == ReferenceAmountEnum.amount_for_100g:
            converted_value = self._owner_node.convert_quantity_between_references(
                self.value, self.for_reference, ReferencelessQuantityProp(value=100.0, unit_term_uid=gram_term.uid)
            )

        return QuantityProp(value=converted_value, unit_term_uid=self.unit_term_uid, for_reference=target_reference)

    def amount_for_100g(self) -> Union["QuantityProp", None]:
        """Return amount for 100g."""
        target_reference = ReferenceAmountEnum.amount_for_100g
        return self.amount_for_reference(target_reference)

    def amount_for_root_node(self) -> Union["QuantityProp", None]:
        """Return amount for root node."""
        target_reference = ReferenceAmountEnum.amount_for_root_node
        return self.amount_for_reference(target_reference)

    def amount_for_activity_production_amount(self) -> Union["QuantityProp", None]:
        """Return amount for production amount."""
        target_reference = ReferenceAmountEnum.amount_for_activity_production_amount
        return self.amount_for_reference(target_reference)

    def __eq__(self, other: object):
        """Check for equality of two QuantityProps."""
        if type(other) is type(self):
            if self.unit_term_uid != other.unit_term_uid:
                return False
            if self.value != other.value:
                return False
            if self.for_reference != other.for_reference:
                return False
            return True
        else:
            return False

    def short_string_representation(self, precision_for_representing_floats: int = 2) -> str:
        """Short string representation of LocationProp for debugging."""
        return f"{np.round(self.value, precision_for_representing_floats)} {self.get_unit_term().name}"
