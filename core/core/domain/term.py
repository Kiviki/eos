import copy
from dataclasses import dataclass
from typing import Optional
from uuid import UUID

from fastjsonschema import JsonSchemaException, compile


@dataclass
class Term:
    name: str
    sub_class_of: Optional[UUID]
    data: dict
    access_group_uid: Optional[UUID] = None
    uid: Optional[UUID] = None
    xid: Optional[str] = None

    def validate_json_schema(self, prop_term_data: dict) -> bool:
        # TODO: EuroFIR terms fail on this line since they contain {'format': 'EuroFIR'}
        #  which is unrecognized
        data_to_validate = copy.deepcopy(self.data)

        try:
            data_to_validate.pop("format")
        except KeyError:
            pass

        validate = compile(data_to_validate)

        try:
            validate(prop_term_data)
        except JsonSchemaException:
            return False

        return True

    def __eq__(self, other):
        if isinstance(other, Term):
            if self.uid != other.uid:
                return False
            if self.access_group_uid != other.access_group_uid:
                return False
            if self.name != other.name:
                return False
            if self.sub_class_of != other.sub_class_of:
                return False
            if self.data != other.data:
                return False
            if self.xid != other.xid:
                return False
            return True
        else:
            return False


def sorted_uuid_tuple(terms: list[Term]) -> tuple[UUID]:
    """Returns a sorted tuple of terms' uuids. Useful for example when we have a list of terms as key in a dict."""
    return tuple(sorted([t.uid for t in terms]))  # noqa
