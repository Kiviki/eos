from dataclasses import dataclass
from typing import Optional


@dataclass
class IngredientsDeclarationMapping:
    faulty_declaration: str
    fixed_declaration: str
    uid: Optional[str] = None
