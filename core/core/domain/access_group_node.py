from dataclasses import dataclass
from enum import Enum
from typing import Optional


class NodeAccessGroupTypeEnum(str, Enum):
    kitchen = "kitchen"
    producer = "producer"
    factory = "factory"  # should include all types of access groups


@dataclass
class AccessGroup:
    uid: Optional[str] = None
    xid: Optional[str] = None
    type: NodeAccessGroupTypeEnum = NodeAccessGroupTypeEnum.kitchen
    namespace_uid: Optional[str] = None
    creator: Optional[str] = None
    data: Optional[dict] = None


@dataclass
class AccessGroupEdge:
    child_access_group_uid: Optional[str] = None
    parent_access_group_uid: Optional[str] = None
    data: Optional[dict] = None
