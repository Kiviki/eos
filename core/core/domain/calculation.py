"Calculation data class."
from dataclasses import field
from typing import Optional, Union
from uuid import UUID

from pydantic import BaseModel, Field

from core.domain.nodes.root_with_subflows_dto import ExistingRootDto, RootWithSubFlowsDto
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

CALCULATION_DATA_INPUT_FIELDS = [
    "request_id",
    "namespace_uid",
    "skip_calc",
    "input_root",
    "save_as_system_process",
    "fixed_depth_for_calculating_supply",
    "gfms_for_which_to_save_most_shallow_nodes",
    "use_system_processes",
    "gfms_for_which_to_load_all_relevant_nodes",
    "exclude_node_props",
    "return_log",
    "return_data_as_table",
    "return_final_graph",
    "return_final_root",
    "requested_quantity_references",
    "requested_impact_assessments",
]
CALCULATION_DATA_RESULT_FIELDS = [
    "global_gfm_state",
    "success",
    "statuscode",
    "message",
    "data_errors",
    "final_graph",
    "final_graph_table",
    "final_root",
]


class Calculation(BaseModel):
    """Calculation data class.

    Attributes:
        uid (Optional[UUID]): The uid of the calculation.
        root_node_uid (Optional[UUID]): The uid of the root node of the calculation.
        skip_calc (Optional[bool]): Whether to skip the calculation.
        input_root (Optional[Union[ActivityWithSubFlowsDto, ExistingRootDto]]): The root activity of the
            calculation.
        save_as_system_process (bool): Whether to save nodes as system processes.
        fixed_depth_for_calculating_supply (int): The depth for which the supply is calculated.
            If save_as_system_process, this will be the depth for which nodes are stored as system processes.
        gfms_for_which_to_save_most_shallow_nodes (list[str]): The GFMs for which to save the most shallow nodes.
        use_system_processes (bool): Whether to use system processes.
        gfms_for_which_to_load_all_relevant_nodes (list[str]): Only use system-processes for nodes
            where no node below can get modified by GFMs contained in this list.
        exclude_node_props (list[str]): The node props to exclude.
        return_log (bool): Whether to return the mutation log.
        return_data_as_table (bool): Whether to return the data as a table.
        return_final_graph (bool): Whether to return the final graph.
        return_final_root (bool): Whether to return the final root activity with subflows.
        requested_quantity_references (Optional[list[ReferenceAmountEnum]]): List holding quantity references
            (e.g., amount_for_100g, amount_for_activity_production_amount) requested by the client.
        requested_impact_assessments (Optional[list[str]]): List holding impact_assessments (e.g., IPCC 2013 GWP 100a)
        requested by the client.
        mutations (Optional[list[AbstractMutation]]): Can contain the mutation log of the calculation.
        success (bool): Whether the calculation was successful.
        statuscode (int): Statuscode of the calculation.
        message (str): Message of the calculation.
        data_errors (Optional[list[str]]): Contains data_errors that occurred during the calculation.
        final_graph: Optional[List[dict]] = Final graph of the calculation.
        final_graph_table: Optional[List[dict]] = Final graph of the calculation as a table.
    """

    uid: Optional[UUID] = None
    child_of_root_node_uid: Optional[UUID] = None
    root_node_uid: Optional[UUID] = None
    request_id: Optional[int] = None
    namespace_uid: Optional[str] = None
    transient: bool = Field(default=False)

    # global GFM state (to give access to this property to all nodes).
    global_gfm_state: Optional[GfmStateProp] = None

    # data_input fields:
    skip_calc: Optional[bool] = False
    input_root: Optional[Union[RootWithSubFlowsDto, ExistingRootDto]] = Field(default=None)
    # parameters for saving system processes:
    save_as_system_process: bool = False
    fixed_depth_for_calculating_supply: int = 1  # Increased from 0 to 1 so that the root flow node is accounted for.
    gfms_for_which_to_save_most_shallow_nodes: list[str] = field(default_factory=lambda: [])
    # parameters for using system processes:
    use_system_processes: bool = True
    gfms_for_which_to_load_all_relevant_nodes: list[str] = field(default_factory=lambda: [])
    exclude_node_props: list[str] = field(default_factory=lambda: ["environmental_flows"])
    # parameters for returning data:
    return_log: bool = False
    return_data_as_table: bool = False
    return_final_graph: bool = False
    return_final_root: bool = True
    requested_quantity_references: Optional[list[ReferenceAmountEnum]] = field(default_factory=lambda: [])
    requested_impact_assessments: Optional[list[str]] = field(default_factory=lambda: [])

    # data_mutations, only contains the mutation log:
    mutations: Optional[list[AbstractMutation]] = field(default_factory=lambda: [])

    # data_result fields:
    success: bool = True
    statuscode: Optional[int] = Field(default=None)
    message: Optional[str] = Field(default=None)
    data_errors: Optional[list[str]] = field(default_factory=lambda: [])
    final_graph: Optional[list[dict]] = Field(None, alias="final_graph")
    final_graph_table: Optional[list[dict]] = Field(None, alias="final_graph_table")
    final_root: Optional[RootWithSubFlowsDto] = Field(default=None)
