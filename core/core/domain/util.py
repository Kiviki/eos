"Helper functions for accessing node properties."

from typing import TYPE_CHECKING

import git

from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph


def get_flow_amount(node: Node) -> float:
    "Helper to return the flow amount value."
    assert isinstance(node, FlowNode), "This method should only be called on flows"
    return node.amount.value


def get_system_process_data(node: ActivityNode, calc_graph: "CalcGraph") -> EnvironmentalFlowsProp:
    """Get the aggregation data for the given node.

    Args:
        node: The node for which to get the aggregation data.
        calc_graph: The calculation graph to inspect.

    Returns:
        The aggregation data for the given node.
    """
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha

    aggregated_cache_kwargs = {
        "flow_quantities": node.environmental_flows.flow_quantities,
        "calculated_using_eos_commit": sha,
    }
    if node.uid in calc_graph.meta_data_for_most_shallow_nodes:
        meta_data = calc_graph.meta_data_for_most_shallow_nodes[node.uid]
        aggregated_cache_kwargs["is_most_shallow_node_for_gfms"] = meta_data.is_most_shallow_node_for_gfms
        aggregated_cache_kwargs[
            "has_sub_nodes_that_can_change_dynamically_for_gfms"
        ] = meta_data.has_sub_nodes_that_can_change_dynamically_for_gfms
    return EnvironmentalFlowsProp.unvalidated_construct(**aggregated_cache_kwargs)


def get_production_amount(node: Node) -> float:
    "The quantity of the product that is produced by the activity."
    return node.production_amount.value
