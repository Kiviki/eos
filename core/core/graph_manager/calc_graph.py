import typing
from dataclasses import dataclass, field
from typing import List, Optional, Type
from uuid import UUID

from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.graph_manager.abstract_graph_observer import AbstractGraphObserver
from core.graph_manager.mutations.abstract_mutation import AbstractMutation
from core.service.glossary_service import GlossaryService

if typing.TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

from database.postgres.pg_term_mgr import IPCC_2013_GWP_100

logger = get_logger()


@dataclass
class MetaDataForMostShallowNodes:
    """Stores meta data for the most shallow nodes in the graph."""

    is_most_shallow_node_for_gfms: list[str] = field(default_factory=lambda: [])
    has_sub_nodes_that_can_change_dynamically_for_gfms: dict[str, bool] = field(default_factory=lambda: {})


class CalcGraph:
    """Encapsulates Nodes and Edges to give outside a read-only-view to these.

    Mutations should go through a central interface and logged.
    """

    def __init__(
        self,
        service_provider: "ServiceProvider",
        root_node_uid: UUID,
        glossary_service: GlossaryService,
        calculation: Optional[Calculation] = None,
    ) -> None:
        """Initialize the CalcGraph.

        Args:
            service_provider: service provider
            root_node_uid: uid of the root node
            glossary_service: glossary service
            calculation: optional calculation object
        """
        self.service_provider = service_provider  # Service provider for various services.
        self.root_node_uid: UUID = root_node_uid  # UID of the root node.
        self.glossary_service = glossary_service  # Glossary service for term-related operations.
        self.calculation = calculation  # Optional calculation object to make calculation parameters accessible.
        if not self.calculation:
            # Initialize empty Calculation if it doesn't already exist.
            self.calculation = Calculation(root_node_uid=self.root_node_uid)

        self._nodes_by_uid: dict[int, Node] = {}  # Dictionary to store nodes by int representation of UUID.

        # all the nodes that might change dynamically in the future for each gfm
        self.nodes_that_can_change_dynamically_for_each_gfm: dict[str, set[UUID]] = {}

        # all the meta data for the most shallow nodes
        self.meta_data_for_most_shallow_nodes: dict[UUID, MetaDataForMostShallowNodes] = {}

        # nodes for which we want to calculate the supply
        self.nodes_to_calculate_supply: set[UUID] = set()

        # set of environmental flows required for the requested impact_assessments
        self.required_environmental_flows: set[UUID] = set()

        self._mutation_log: list[AbstractMutation] = []  # List to log mutations.
        self._graph_observers: list[AbstractGraphObserver] = []  # List of graph observers.
        self.data_errors_log: list[str] = []  # List to log data errors.

    # TODO: unused for now
    # async def load_node_from_db(self, uid: UUID) -> None:
    #     node = await self.pg_graph_mgr.find_by_uid(uid)
    #     self.add_node(node)

    def get_root_node(self) -> Optional[Node]:
        return self.get_node_by_uid(self.root_node_uid)

    def child_of_root_node(self) -> Optional[Node]:
        # FIXME better name, or remove that getter method when
        # root-node as flow node fully works
        root_node = self.get_root_node()
        assert root_node
        return root_node.get_sub_nodes()[0]

    def get_node_by_uid(self, uid: UUID) -> Optional[Node]:
        # Changed from type(uid) is UUID to isinstance(uid, UUID) because the uid returned from the database has
        # the type asyncpg.asyncpg.pgproto.pgproto.UUID.
        if not isinstance(uid, UUID):
            raise ValueError("uid must be of type UUID")
        return self._nodes_by_uid.get(uid.int)

    def get_node_uids(self) -> list[UUID]:
        return [UUID(int=uid_int) for uid_int in self._nodes_by_uid.keys()]

    def __len__(self):
        return len(self._nodes_by_uid)

    def get_mutation_log(self) -> [AbstractMutation]:
        return self._mutation_log

    def get_data_errors_log(self) -> [str]:
        return self.data_errors_log

    def set_data_errors_log_entry(self, log: str) -> None:
        self.data_errors_log.append(log)

    def add_node(self, node: Node) -> None:
        node.set_calculation(self.calculation)

        # TODO: need to properly implement this use case..
        if node.uid.int in self._nodes_by_uid:
            raise ValueError("node with that uid already exists in the graph.")

        self._nodes_by_uid[node.uid.int] = node
        for obs in self._graph_observers:
            obs.notify_new_graph_node(node)

    def add_edge(self, parent_node_uid: UUID, child_node_uid: UUID) -> None:
        child_node = self._nodes_by_uid[child_node_uid.int]
        parent_node = self._nodes_by_uid[parent_node_uid.int]
        parent_node.add_sub_node(child_node)
        child_node.add_parent_node(parent_node)

    def add_graph_observer(self, graph_observer: AbstractGraphObserver) -> None:
        self._graph_observers.append(graph_observer)

    def remove_edge(self, parent_node_uid: UUID, child_node_uid: UUID) -> None:
        child_node = self._nodes_by_uid[child_node_uid.int]
        parent_node = self._nodes_by_uid[parent_node_uid.int]
        self._nodes_by_uid[parent_node_uid.int].remove_sub_node(child_node)
        self._nodes_by_uid[child_node_uid.int].remove_parent_node(parent_node)

    def _get_cutoff_graph(self, stop_at_node_types: set[Type[Node]]) -> dict[int, Node]:
        output_nodes: dict[int, Node] = {}

        root_node = self.get_root_node()

        # special case (just in case filter directly filters out the root node:
        if isinstance(root_node, tuple(stop_at_node_types)):
            return {}

        # starting at the root_node, we traverse the graph using the filter:
        output_nodes[root_node.uid.int] = root_node
        stack: list[Node] = [root_node]

        # graph traversal using node stack:
        while len(stack) > 0:
            # iterate over the sub_nodes:
            for sub_node in stack.pop().get_sub_nodes():
                # don't do anything if hitting stopping condition:
                if isinstance(sub_node, tuple(stop_at_node_types)):
                    continue

                # if the sub_node was not yet processed before:
                if sub_node.uid.int not in output_nodes:
                    output_nodes[sub_node.uid.int] = sub_node

                    # for further processing add it to stack:
                    stack.append(sub_node)

        return output_nodes

    async def get_serialized_graph(self, exclude_node_props: List[str]) -> [dict]:
        """Filter the graph and return a list of all serialized nodes."""
        node_list_by_uid = self._get_cutoff_graph(
            stop_at_node_types={
                ModeledActivityNode,
                ElementaryResourceEmissionNode,
            }
        )
        serialized_graph = []
        for node in node_list_by_uid.values():
            node_dict = node.model_dump(mode="json", exclude_none=True, exclude=set(exclude_node_props))
            node_dict["sub_node_uids"] = [str(child_node.uid) for child_node in node.get_sub_nodes()]
            serialized_graph.append(node_dict)
        return serialized_graph

    def get_requested_impact_assessments(self) -> list[str]:
        if self.calculation:
            requested_impact_assessments = self.calculation.requested_impact_assessments
            if not requested_impact_assessments:
                requested_impact_assessments = [IPCC_2013_GWP_100]
        else:
            requested_impact_assessments = [IPCC_2013_GWP_100]
        return requested_impact_assessments

    async def apply_mutation(self, mutation: AbstractMutation) -> None:
        await mutation.apply(self)
        self._mutation_log.append(mutation)

    def format_as_tree(self) -> str:
        """A tree representation of the graph. Useful for debugging."""
        return f"CalcGraph: {len(self._nodes_by_uid)} nodes: \n " + self.get_root_node().format_as_tree()
