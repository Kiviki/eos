from typing import Optional

from core.graph_manager.mutations.abstract_mutation import AbstractMutation
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation


def mutation_from_dict(data: dict) -> Optional[AbstractMutation]:
    """Given a dict containing the mutation type, return the correct mutation object."""
    if data["mutation_type"] == "AddNodeMutation":
        mutation = AddNodeMutation.from_dict(data)
    elif data["mutation_type"] == "AddEdgeMutation":
        mutation = AddEdgeMutation.from_dict(data)
    elif data["mutation_type"] == "DuplicateNodeMutation":
        mutation = DuplicateNodeMutation.from_dict(data)
    elif data["mutation_type"] == "PropMutation":
        mutation = PropMutation.from_dict(data)
    elif data["mutation_type"] == "PropListMutation":
        mutation = PropListMutation.from_dict(data)
    elif data["mutation_type"] == "RemoveEdgeMutation":
        mutation = RemoveEdgeMutation.from_dict(data)
    else:
        raise NotImplementedError
    return mutation
