from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from structlog import get_logger

from core.domain.nodes.node import Node
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph

logger = get_logger()


@dataclass(kw_only=True)
class AddNodeMutation(AbstractMutation):
    """Mutation that adds a new node (and corresponding edge) to the graph."""

    parent_node_uid: Optional[UUID] = None
    new_node: Node
    copy: bool

    async def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        if self.copy:
            # copy, so that we keep a reference to the original node, for when we replay the mutation log.
            copied_node = self.new_node.model_copy()
            copied_node.set_owner_node_for_props()
            calc_graph.add_node(copied_node)
        else:
            self.new_node.set_owner_node_for_props()
            calc_graph.add_node(self.new_node)

        if self.parent_node_uid:
            calc_graph.add_edge(self.parent_node_uid, self.new_node.uid)

    def serialize_attributes(self, data: dict) -> dict:
        """Serialize all non-trivial class attributes."""
        data = super(AddNodeMutation, self).serialize_attributes(data)
        if isinstance(self.new_node, Node):
            data["new_node"] = self.new_node.model_dump(mode="json", exclude_none=True)
        return data

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        data = super(AddNodeMutation, AddNodeMutation).deserialize_attributes(data)
        data["new_node"] = Node.from_dict(data["new_node"])
        if data.get("parent_node_uid"):
            data["parent_node_uid"] = UUID(data["parent_node_uid"])
        return data
