"""Sheet converter for Kale exports. Currently used mostly for Valora exports."""

from typing import Optional

from core.domain.deep_mapping_view import DeepListView, DeepMappingView
from core.domain.nodes import (
    ElementaryResourceEmissionNode,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    ModeledActivityNode,
    PracticeFlowNode,
)
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import (
    GlossaryTermProp,
    JsonProp,
    LocationProp,
    QuantityPackageProp,
    QuantityProp,
    TransportModesDistancesProp,
)
from core.domain.props.location_prop import LocationQualifierEnum
from core.domain.props.names_prop import NamesProp
from core.graph_manager.calc_graph import CalcGraph
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID

CONSERVATION_TAG_XID_TO_NAME = {
    "J0001": "generic conserved",
    "J0003": "not conserved",
    "J0131": "cooled",
    "J0136": "frozen",
    "J0111": "canned",
    "J0116": "dried",
}

PERISHABILITY_TAG_XID_TO_NAME = {
    "EOS_STABLE": "stable",
    "EOS_PERISHABLE": "perishable",
    "EOS_HIGH-PERISHABLE": "highly-perishable",
}


class OutputGraphToTableConverter:
    """Converter of CalcGraph instances to a JSON-type table.

    Code refactored from Valora Dagster pipeline.
    """

    def __init__(self):
        self.data: list = []
        self.data_errors: str = ""

    @staticmethod
    def _get_conservation(flow_node: FoodProductFlowNode) -> str:
        conservation_tags = []
        for xid in flow_node.tag_term_xids:
            if xid in CONSERVATION_TAG_XID_TO_NAME:
                conservation_tags.append(CONSERVATION_TAG_XID_TO_NAME[xid])
        return ",".join(conservation_tags)

    @staticmethod
    def _get_perishability(flow_node: FoodProductFlowNode) -> str:
        perishability_tags = []
        for xid in flow_node.tag_term_xids:
            if xid in PERISHABILITY_TAG_XID_TO_NAME:
                perishability_tags.append(PERISHABILITY_TAG_XID_TO_NAME[xid])
        return ",".join(perishability_tags)

    @staticmethod
    def _get_parent_node_amount(parent_flow_node: Node) -> float | None:
        """Get `ingredient_amount` property value from `parent_flow_node` node."""
        # if there is no "ingredient_amount" property in parent flow node, `None` is returned
        if parent_flow_node:
            if parent_flow_node.amount is None:
                return None
            unit = parent_flow_node.amount.get_unit_term()
            if "mass-in-g" in unit.data:
                conversion_factor = unit.data["mass-in-g"]
            else:
                conversion_factor = 1.0

            try:
                parent_ingredient_amount_prop = parent_flow_node.amount.amount_for_root_node()
            except ValueError:
                parent_ingredient_amount_prop = None

            if parent_ingredient_amount_prop:
                parent_ingredient_amount = parent_ingredient_amount_prop.value * conversion_factor
                return parent_ingredient_amount

        return None

    @staticmethod
    def _get_node_amount(node: Node) -> dict | float:
        """Get amount value from the given node."""
        if isinstance(node, FlowNode):
            if (
                node.amount_in_original_source_unit
                and isinstance(node.amount_in_original_source_unit, QuantityProp)
                and node.amount_in_original_source_unit.source_data_raw
            ):
                if isinstance(node.amount_in_original_source_unit.source_data_raw, str):
                    amount_in_original_source_unit = node.amount_in_original_source_unit.source_data_raw
                elif isinstance(node.amount_in_original_source_unit.source_data_raw, (DeepMappingView, dict)):
                    amount_in_original_source_unit = node.amount_in_original_source_unit.source_data_raw.get("value")
                else:
                    amount_in_original_source_unit = None
            else:
                amount_in_original_source_unit = None
        else:
            if node.production_amount and isinstance(node.production_amount, QuantityProp):
                if node.production_amount.source_data_raw:
                    amount_in_original_source_unit = node.production_amount.source_data_raw.get("value")
                else:
                    amount_in_original_source_unit = node.production_amount.value
            else:
                amount_in_original_source_unit = None

        if not amount_in_original_source_unit and (isinstance(node, FlowNode) and node.amount):
            unit = node.amount.get_unit_term()
            try:
                flow_for_root = node.amount.amount_for_root_node()
            except ValueError:
                flow_for_root = None
            if flow_for_root is not None:
                if "mass-in-g" in unit.data:
                    amount_in_original_source_unit = unit.data["mass-in-g"] * flow_for_root.value
                else:
                    amount_in_original_source_unit = flow_for_root.value

        return amount_in_original_source_unit

    @staticmethod
    def _check_if_node_is_unskippable(node: Node) -> bool:
        """Check if the given node is of a type that must be provided in output."""
        if isinstance(node, (ModeledActivityNode, ElementaryResourceEmissionNode)):
            return False
        else:
            return True

    @staticmethod
    def _parse_quantity_package(qty_pkg: Optional[QuantityPackageProp]) -> dict:
        """Parse nutrient dict into columns with appropriate names."""
        if not qty_pkg:
            return {}
        else:
            return {
                f"{GlossaryTermProp.get_term_from_uid(key).name}": data.value
                for key, data in qty_pkg.quantities.items()
            }

    @staticmethod
    def _parse_nutrient_package_into_columns(
        aggregated_nutrient: Optional[QuantityPackageProp], description: str
    ) -> dict:
        """Parse nutrient dict into columns with appropriate names."""
        if not aggregated_nutrient:
            return {}
        else:
            return {
                (
                    "aggregated_nutrients_"
                    f"{description}_{GlossaryTermProp.get_term_from_uid(nutrient).name}_{data.get_unit_term().name}"
                ): data.value
                for nutrient, data in aggregated_nutrient.quantities.items()
            }

    def _parse_nutrient_values(self, node: Node) -> dict:
        """Parse `nutrient_values` property into columns for each individual nutrient."""
        food_node = None
        if isinstance(node, FoodProductFlowNode):
            food_node = node
        elif node.get_parent_nodes() and isinstance(node.get_parent_nodes()[0], FoodProductFlowNode):
            food_node = node.get_parent_nodes()[0]

        if food_node and food_node.aggregated_nutrients:
            try:
                aggregated_nutrient_content_in_root = food_node.aggregated_nutrients.amount_for_root_node()
                root_columns = self._parse_nutrient_package_into_columns(
                    aggregated_nutrient_content_in_root, "per_root"
                )
            except ValueError:
                root_columns = {}
            aggregated_nutrient_content_in_local_product = (
                food_node.aggregated_nutrients.amount_for_activity_production_amount()
            )
            local_columns = self._parse_nutrient_package_into_columns(
                aggregated_nutrient_content_in_local_product, "per_node_production_amount"
            )
        else:
            root_columns = {}
            local_columns = {}

        if food_node and food_node.daily_food_unit:
            try:
                dfu_in_root = food_node.daily_food_unit.amount_for_root_node()
                if dfu_in_root:
                    root_columns["aggregated_daily_food_unit_per_root"] = dfu_in_root.value
            except ValueError:
                pass

            dfu_in_local_product = food_node.daily_food_unit.amount_for_activity_production_amount()
            if dfu_in_local_product:
                local_columns["aggregated_daily_food_unit_per_node_production_amount"] = dfu_in_local_product.value
        return root_columns | local_columns

    @staticmethod
    def _get_node_location(current_node: Node) -> str:
        if isinstance(current_node, ActivityNode):
            location_prop = current_node.activity_location
        else:
            location_prop = current_node.flow_location

        if location_prop:
            if isinstance(location_prop, str):
                return location_prop

            location_terms = [loc.get_term().xid for loc in location_prop if loc.get_term()]
            if location_terms:
                if len(location_terms) == 1:
                    location = location_terms[0]
                else:
                    location = str(location_terms)
            elif (
                isinstance(location_prop, (list, DeepListView))
                and isinstance(location_prop[0], LocationProp)
                and location_prop[0].location_qualifier == LocationQualifierEnum.unknown
            ):
                location = LocationQualifierEnum.unknown.value
            else:
                location = ""
        else:
            location = ""
        return location

    @staticmethod
    def _get_node_name(current_node: Node) -> str:
        current_node_name = None
        if hasattr(current_node, "product_name"):
            if current_node.product_name and isinstance(current_node.product_name, NamesProp):
                if current_node.product_name.source_data_raw is not None:
                    current_node_name = current_node.product_name.source_data_raw[0].get("value")
                else:
                    current_node_name = ",".join([term.get_term().name for term in current_node.product_name.terms])
            elif current_node.product_name:
                current_node_name = current_node.product_name[0].get("value")

        if current_node_name is None and current_node.raw_input is not None:
            raw_input = current_node.raw_input
            if hasattr(raw_input, "names") and raw_input.names:
                current_node_name = raw_input.names[0].get("value")
            elif hasattr(raw_input, "titles") and raw_input.titles:
                current_node_name = raw_input.titles[0].get("value")

        return current_node_name

    def _iterate_graph(
        self, node: Node, level: int, tree_index: str = "", parent_flow_node: Node | None = None
    ) -> None:
        """Recursively iterate the given CalcGraph."""
        if node.raw_input:
            raw_input = node.raw_input
        else:
            raw_input = JsonProp.unvalidated_construct()  # to avoid errors

        node_name = self._get_node_name(node)
        amount = self._get_node_amount(node)

        if node_name is None and isinstance(node, ActivityNode) and node.get_parent_nodes():
            # Try getting the name from parent flow (primary product of the activity) for activity nodes.
            node_name = self._get_node_name(node.get_parent_nodes()[0])

        if node_name is None:  # this node has been subdivided
            try:  # to get the name of this node's first child
                first_child = node.get_sub_nodes()[0]
                # Check if first_child is subdivision.
                if hasattr(first_child, "is_subdivision") and first_child.is_subdivision:
                    if hasattr(first_child, "product_name"):
                        if first_child.product_name and isinstance(first_child.product_name, NamesProp):
                            if first_child.product_name.source_data_raw is not None:
                                node_name = first_child.product_name.source_data_raw[0].get("value")
                            else:
                                node_name = first_child.product_name.terms[0].get_term().name
                        elif first_child.product_name:
                            node_name = first_child.product_name[0].get("value")

                    if node_name is None and first_child.raw_input is not None:
                        if "names" in first_child.raw_input.model_fields_set:
                            node_name = first_child.raw_input.names[0].get("value")
                        elif "titles" in first_child.raw_input.model_fields_set:
                            node_name = first_child.raw_input.titles[0].get("value")
                        else:
                            node_name = type(node).__name__
            except Exception:  # if the first child has no name, we use the node_type
                node_name = type(node).__name__

        if node_name is None:
            node_name = type(node).__name__

        if isinstance(node, FoodProductFlowNode):
            is_subdivision = bool(node.is_subdivision)
        else:
            is_subdivision = False

        if isinstance(node, FoodProductFlowNode):
            is_dried = node.is_dried()
        else:
            is_dried = False

        location = self._get_node_location(node)

        ingredient_amount = self._get_parent_node_amount(parent_flow_node)
        if not location and parent_flow_node:
            # If location information is not available in the node activity node, see if the information is available
            # in the parent_flow_node (i.e., the row for transportation food processing activity node contains
            # the destination location).
            location = self._get_node_location(parent_flow_node)

        if isinstance(node, FoodProcessingActivityNode) and node.ingredient_amount_estimator_error_squares:
            error_squares = str(self._parse_quantity_package(node.ingredient_amount_estimator_error_squares))
        else:
            error_squares = ""

        if isinstance(node, FoodProcessingActivityNode) and node.ingredient_amount_estimator_solution_status:
            solution_status = node.ingredient_amount_estimator_solution_status.value
        else:
            solution_status = ""

        kg_co2eq_per_root_product = ""
        kg_co2eq_per_node_production_amount = ""
        if node.impact_assessment:
            try:
                char_for_root = node.impact_assessment.amount_for_root_node()
            except ValueError:
                char_for_root = None
            char_for_local_activity = node.impact_assessment.amount_for_activity_production_amount()

            if char_for_root:
                for key, val in char_for_root.quantities.items():
                    if GlossaryTermProp.get_term_from_uid(key).xid == IPCC_2013_GWP_100_XID:
                        kg_co2eq_per_root_product = val.value

            if char_for_local_activity:
                for key, val in char_for_local_activity.quantities.items():
                    if GlossaryTermProp.get_term_from_uid(key).xid == IPCC_2013_GWP_100_XID:
                        kg_co2eq_per_node_production_amount = val.value

        liter_scarce_water_per_root_product = ""
        liter_scarce_water_production_amount = ""
        if node.scarce_water_consumption:
            scarce_water_for_root = node.scarce_water_consumption.amount_for_root_node()
            scarce_water_for_local_activity = node.scarce_water_consumption.amount_for_activity_production_amount()

            if scarce_water_for_root:
                liter_scarce_water_per_root_product = scarce_water_for_root.value

            if scarce_water_for_local_activity:
                liter_scarce_water_production_amount = scarce_water_for_local_activity.value

        if isinstance(node, FlowNode):
            node_production_amount = None
        else:
            # in eos, 1 kg of production_amount is assumed, if not otherwise specified
            if node.production_amount:
                node_production_amount = node.production_amount.value
            else:
                node_production_amount = 1.0

        general_node_type = "flow" if isinstance(node, FlowNode) else "activity"

        if isinstance(node, FlowNode) and node.matrix_gfm_error:
            matrix_gfm_error = node.matrix_gfm_error.value
        else:
            matrix_gfm_error = ""

        # Add conservation:
        if parent_flow_node and isinstance(parent_flow_node, FoodProductFlowNode):
            conservation_tags = self._get_conservation(parent_flow_node)
            perishability_tags = self._get_perishability(parent_flow_node)
            if parent_flow_node.raw_processing:
                processing_tags = str(parent_flow_node.raw_processing.get("value", ""))
            else:
                processing_tags = ""
            is_combined_product = parent_flow_node.is_combined_product()
        elif isinstance(node, FoodProductFlowNode):
            conservation_tags = self._get_conservation(node)
            perishability_tags = self._get_perishability(node)
            if node.raw_processing:
                processing_tags = str(node.raw_processing.get("value", ""))
            else:
                processing_tags = ""
            is_combined_product = node.is_combined_product()
        else:
            conservation_tags = ""
            perishability_tags = ""
            processing_tags = ""
            is_combined_product = ""

        # we don't "print" this type of node, but we provide its amount to its children
        if isinstance(node, (FoodProductFlowNode, PracticeFlowNode)) and node.get_sub_nodes():
            if self._check_if_node_is_unskippable(node.get_sub_nodes()[0]):
                parent_flow_node = node

                self._iterate_graph(
                    node.get_sub_nodes()[0],
                    level,
                    parent_flow_node=parent_flow_node,
                    tree_index=tree_index,
                )

                return

        if hasattr(node, "product_name") and node.product_name and isinstance(node.product_name, NamesProp):
            matched_terms = [f"{prop.get_term().name} [{prop.get_term().xid}]" for prop in node.product_name.terms]
            matched_terms = ", ".join(matched_terms)
        else:
            matched_terms = ""

        brightway_node = ""
        brightway_node_from_cache = None
        if (
            isinstance(node, FlowNode)
            and node.get_sub_nodes()
            and node.get_sub_nodes()
            and isinstance(node.get_sub_nodes()[0], ModeledActivityNode)
        ):
            brightway_node_from_cache = node.get_sub_nodes()[0]

        if brightway_node_from_cache:
            raw_input_data = brightway_node_from_cache.raw_input
            required_fields = {"key", "name", "reference_product"}

            if raw_input_data and required_fields.issubset(raw_input_data.model_fields_set):
                brightway_node = (
                    f"Key: {str(tuple(raw_input_data.key))}\n"
                    f"Name: {raw_input_data.name}\n"
                    f"Reference product: {raw_input_data.reference_product}"
                )
                if brightway_node_from_cache.activity_location and isinstance(
                    brightway_node_from_cache.activity_location, str
                ):
                    brightway_node += f"\nLocation: {brightway_node_from_cache.activity_location}"
            else:
                brightway_node = ""
        else:
            brightway_node = ""

        if isinstance(node, FlowNode) and getattr(node.origin_data, "fao_code_term", None):
            fao_code_term = node.origin_data.fao_code.get_term()
            fao_code = f"{fao_code_term.xid}\n" f"Name: {fao_code_term.name}"
        else:
            fao_code = ""

        if (
            isinstance(node, FoodProductFlowNode)
            and node.nutrient_values
            and node.nutrient_values.get_prop_term() is not None
        ):
            nutrient_file = node.nutrient_values.get_prop_term().xid
        else:
            nutrient_file = ""

        if node.get_sub_nodes():
            if isinstance(node, ActivityNode):
                location_prop = node.activity_location
            else:
                location_prop = node.flow_location

            if self._check_if_node_is_unskippable(node.get_sub_nodes()[0]) and location_prop:
                child_node = node.get_sub_nodes()[0]

                if (
                    isinstance(child_node, FlowNode)
                    and child_node.transport
                    and isinstance(child_node.transport, TransportModesDistancesProp)
                ):
                    transport_mode_distances = str(child_node.transport.model_dump())
                    if child_node.transport.qualified_modes:
                        transport_mode_options = ", ".join(child_node.transport.qualified_modes)
                    else:
                        transport_mode_options = ""
                    if child_node.transport.cheapest_mode:
                        transport = child_node.transport.cheapest_mode
                    else:
                        transport = ""
                else:
                    transport_mode_distances = ""
                    transport_mode_options = ""
                    transport = ""
            else:
                transport = ""
                transport_mode_options = ""
                transport_mode_distances = ""
        else:
            transport = ""
            transport_mode_options = ""
            transport_mode_distances = ""

        nutr_vals = {}
        if isinstance(node, FoodProductFlowNode):
            node_with_nutrients = node
        elif node.get_parent_nodes() and isinstance(node.get_parent_nodes()[0], FoodProductFlowNode):
            node_with_nutrients = node.get_parent_nodes()[0]
        else:
            node_with_nutrients = None

        if (
            node_with_nutrients
            and node_with_nutrients.nutrient_values
            and node_with_nutrients.nutrient_values.source_data_raw
        ):
            nutr_vals = node_with_nutrients.nutrient_values.source_data_raw

        nutrients_declaration = {f"nutrients_declaration_per_100g_{key}": val for key, val in nutr_vals.items()}

        end_product = getattr(raw_input, "end_product", "")

        if isinstance(node, FoodProductFlowNode) and node.nutrient_upscale_ratio:
            nutrient_upscale_ratio = node.nutrient_upscale_ratio.value
        else:
            nutrient_upscale_ratio = ""

        weight_unit = ""
        if ingredient_amount:
            weight_unit = "Gram"
        else:
            if isinstance(node, FlowNode):
                if (
                    node.amount_in_original_source_unit
                    and isinstance(node.amount_in_original_source_unit, QuantityProp)
                    and node.amount_in_original_source_unit.source_data_raw
                ):
                    weight_unit = node.amount_in_original_source_unit.source_data_raw.get("unit")
                else:
                    if node.amount:
                        converted_unit = node.amount.get_unit_term()
                        if "mass-in-g" in converted_unit.data:
                            weight_unit = "Gram"
                        else:
                            weight_unit = converted_unit.name

            else:
                if node.production_amount and isinstance(node.production_amount, QuantityProp):
                    if node.production_amount.source_data_raw:
                        weight_unit = node.production_amount.source_data_raw.get("unit")
                    else:
                        weight_unit = node.production_amount.get_unit_term().name

        row = (
            {
                "name": "  " * level + node_name,
                "end_product": end_product,
                "supplier": getattr(raw_input, "supplier", ""),
                "business_unit": getattr(raw_input, "business_unit", ""),
                "article_number": getattr(raw_input, "article_number", ""),
                "gtin": getattr(raw_input, "gtin", ""),
                "level": level,
                "tree_index": tree_index,
                "weight_lean": ingredient_amount if ingredient_amount is not None else amount,
                "weight_unit": weight_unit,
                "detailed_node_type": type(node).__name__,
                "general_node_type": general_node_type,
                "production_date": getattr(node, "activity_date", ""),
                "location": location,
                "transport": transport,
                "conservation": conservation_tags,
                "perishability": perishability_tags,
                "processing": processing_tags,
                "is_combined_product": is_combined_product,
                "is_subdivision": is_subdivision,
                "is_dried": is_dried,
                "matched_terms": matched_terms,
                "nutrient_file": nutrient_file,
                "fao_code": fao_code,
                "brightway_node": brightway_node,
                "transport_mode_distances": transport_mode_distances,
                "transport_mode_options": transport_mode_options,
                "errors": self.data_errors if level == 0 else "",
                "matrix_gfm_error": matrix_gfm_error,
                "error_squares": error_squares,
                "solution_status": solution_status,
                "kg_co2eq_per_root_product": kg_co2eq_per_root_product,
                "kg_co2eq_per_node_production_amount": kg_co2eq_per_node_production_amount,
                "liter_scarce_water_per_root_product": liter_scarce_water_per_root_product,
                "liter_scarce_water_production_amount": liter_scarce_water_production_amount,
                "node_production_amount": node_production_amount,
                "nutrient_upscale_ratio": nutrient_upscale_ratio,
            }
            | nutrients_declaration
            | self._parse_nutrient_values(node)
        )
        self.data.append(row)

        # continue with subnodes
        sub_nodes = [sub_node for sub_node in node.get_sub_nodes() if self._check_if_node_is_unskippable(sub_node)]

        # make sure order is correct
        sub_nodes = sorted(
            sub_nodes,
            key=lambda x: x.declaration_index.value if (hasattr(x, "declaration_index") and x.declaration_index) else 0,
        )

        for index, sub_node in enumerate(sub_nodes):
            self._iterate_graph(sub_node, level + 1, tree_index=tree_index + "." + str(index + 1))

    def run(self, calc_graph: CalcGraph, data_errors: str) -> list[dict]:
        """Run the convertor."""
        self.data_errors = data_errors
        root_node = calc_graph.get_node_by_uid(calc_graph.root_node_uid)
        self._iterate_graph(root_node, level=0, tree_index="")

        return self.data
