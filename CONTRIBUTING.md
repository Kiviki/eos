# Contributing to eos

Hello fellow developer! Here we collected some informations to get you up to speed on the steps
needed to get your MR accepted.

## code formatting

For formatting our code we use:

- [isort](https://pypi.org/project/isort/) isort your imports, so you don't have to.
- [black](https://pypi.org/project/black/) is the uncompromising Python code formatter.

These tools are installed when you follow the guidelines in our README.md, please configure your code-editor to run both tools on save.
Configuration settings are in our pyproject.toml file, there should be no need to do anything extra.

### PyCharm

- [configure black](https://black.readthedocs.io/en/stable/integrations/editors.html#pycharm-intellij-idea) by choosing one of the 3 options presented
- **isort** as an [External Tool](https://www.jetbrains.com/help/pycharm/configuring-third-party-tools.html)
  1. Go to `External tool — isort configuration` and set it to `$PyInterpreterDirectory$/isort`
  2. Set `Arguments` as `$FilePath$ --settings-path=$ProjectFileDir$`
  3. Set `Working directory` field as `$ProjectFileDir$`
  4. Go to `Settings > Keymap > External Tools > External Tools`, and set a shortcut for it, for example `Ctrl + Alt + I`. To sort your imports please use the short-cut.

### VSCode

- Use the Plugin [black-formatter](https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter)
- Use the plugin [Python isort](https://marketplace.visualstudio.com/items?itemName=ms-python.isort)
- For better unittest experience, we suggest to enable in your vscode user preferences the new implementation (currently in beta):
```
"python.experiments.optInto": ["pythonTestAdapter"]
```
- This is our recommended content of `.vscode/settings.json`:
```
{
    "python.testing.pytestArgs": [
        "--no-cov",
    ],
    "python.testing.cwd": "${workspaceFolder}",
    "python.testing.unittestEnabled": false,
    "python.testing.pytestEnabled": true,
    "testing.showAllMessages": true,

     // Enable the new pythonTestAdapter to instantly show test progress:
     // Needs to be added to vscode user settings:
    "python.experiments.optInto": ["pythonTestAdapter"],

    "[python]": {
        "editor.defaultFormatter": "ms-python.black-formatter",
        "editor.formatOnSave": true,
        "editor.codeActionsOnSave": {
            "source.organizeImports": true
        },
    },
}
```
- This is our recommended content of `.vscode/launch.json`:
```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: reinitDb",
            "type": "python",
            "request": "launch",
            "module": "database",
            "args": [
                "--reinit"
            ],
            "justMyCode": true
        },
        {
            "name": "Python: inventory_importer",
            "type": "python",
            "request": "launch",
            "module": "inventory_importer.bw_import_controller",
            "args": [
                "--edb_path",
                "./temp_data/brightway",
                "--gfm_import_data",
                "--import_using_airtable",
                "--download_from_gdrive",
                "1QGgJPHuz-m7MA5zIkerN1b2EsId8dnIu"
            ],
            "justMyCode": true
        },
        {
            "name": "Python: api",
            "type": "python",
            "request": "launch",
            "module": "api",
            "justMyCode": true
        },
        {
            "name": "Python: legacy_api",
            "type": "python",
            "request": "launch",
            "module": "legacy_api",
            "justMyCode": true
        }
    ]
}
```

## code linting

- [ruff](https://pypi.org/project/ruff/) An extremely fast Python linter, written in Rust.

Configuration settings are in our pyproject.toml file, there should be no need to do anything extra.

### PyCharm

[PyCharm ruff plugin](https://plugins.jetbrains.com/plugin/20574-ruff)

### VSCode

[VSCode Ruff plugin](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff)

other option is to add ruff to your existing Language Server setup.

## Performance Profiling

In the `profiling/` directory, you will find tools to profile the performance of the code. Here are the steps to get started with performance profiling:

1. **Run the Profiler:**

   Use the provided example script to launch the profiler for a specific configuration. You can find the script at `profiling/run_profiling_example.sh`.

2. **Visualize the Results:**

   After running the profiler, you can visualize the results as a flamegraph. An example script to create a flamegraph is available at `profiling/create_flamegraph_example.sh`.

3. **Create Custom Profiling Setups:**

   To profile a new setup, follow these steps:

   - Add a new Python class to the `profiling/profiling/custom_profiling_setups.py` file.
   - Make sure your new class inherits from `AbstractProfilingEOS`.
   - Modify the `init_calculation` method in your new class to suit your profiling needs.

By following these steps, you can effectively profile the performance of the code and create custom profiling setups as necessary.
