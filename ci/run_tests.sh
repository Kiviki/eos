#!/bin/bash

# this script need to be executed in the parent directory.

export COVERAGE_FILE=./ci/.coverage

# erase existing coverage file to start collecting new stats over all three projects:
poetry run coverage erase

#poetry run pytest --junitxml=./ci/junit_merged.xml --cov-append || FAILED=true

poetry run pytest database --junitxml=./ci/junit_db.xml --cov-append || FAILED=true
poetry run pytest core --junitxml=./ci/junit_core.xml --cov-append || FAILED=true
poetry run pytest api --junitxml=./ci/junit_api.xml --cov-append || FAILED=true
poetry run pytest legacy_api --junitxml=./ci/junit_legacy_api.xml --cov-append || FAILED=true
poetry run pytest inventory_importer --junitxml=./ci/junit_inventory_importer.xml --cov-append -Wdefault || FAILED=true
# use -Wdefault to not convert warnings to exception in inventory_importer tests as bw2 uses very old dependencies.

# generate coverage reports for all tests together
poetry run coverage xml -o ./ci/coverage.xml
poetry run coverage report

# combine all test results:
poetry run junitparser merge ./ci/junit_db.xml ./ci/junit_core.xml ./ci/junit_api.xml ./ci/junit_legacy_api.xml ./ci/junit_inventory_importer.xml ./ci/junit_merged.xml

# signal to gitlab ci if pipeline run succeeded:
if [ $FAILED ]
then
  echo "Tests failed." >/dev/stderr
  exit 1
else
  echo "Tests succeeded."
fi
