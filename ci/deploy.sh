#!/bin/sh

# activate k8s context:
aws eks --region eu-central-1 update-kubeconfig --name eos-eks-1


# create $K8S_NAMESPACE namespace if it does not exist yet:
kubectl create namespace $K8S_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -


# copy database credentials to $K8S_NAMESPACE namespace:
kubectl get secret $PG_SECRET_NAME -n default -o yaml | sed s/"namespace: default"/"namespace: $K8S_NAMESPACE"/ | sed '/creationTimestamp: \|resourceVersion: \|uid: /d' | kubectl apply -n $K8S_NAMESPACE -f -


# copy ecotransit credentials to $K8S_NAMESPACE namespace:
kubectl create secret generic ecotransit-creds --namespace=$K8S_NAMESPACE \
  --from-literal=user="$(echo $CI_ECOTRANSIT_CUSTOMER | base64 -d)" \
  --from-literal=password="$(echo $CI_ECOTRANSIT_PASSWORD | base64 -d)" \
  --from-literal=wsdl="$(echo $CI_ECOTRANSIT_WSDL_URL | base64 -d)" \
  --save-config \
  --dry-run=client \
  -o yaml | \
  kubectl apply -f -


# create secret to access gdrive:
echo $GOOGLE_SERVICE_ACCOUNT | base64 -d > service_account.json
kubectl create secret generic google-cloud-service-account --namespace=$K8S_NAMESPACE --from-file=service_account.json \
  --save-config \
  --dry-run=client \
  -o yaml | \
  kubectl apply -f -
rm service_account.json


# deploy new eos helm chart:
helm upgrade eos ./ci/helm_chart --install --no-hooks --values=./ci/helm_chart/values.yaml \
  --set images.tag=$CI_COMMIT_TAG \
  --set images.repository=$IMAGE_REPO \
  --set postgres_host=$PG_K8S_SERVICE \
  --set postgres_secret_name=$PG_SECRET_NAME \
  --set seed.username=$EATERNITY_USERNAME \
  --set seed.auth_token=$CI_EATERNITY_TOKEN \
  --set seed.namespace_uuid=$EATERNITY_NAMESPACE_UUID \
  --set gcp_key=$GCP_KEY \
  --set rabbitmq.pass=$RABBITMQ_PASS \
  --set cert_arn=$EATERNITY_CERT_ARN \
  --set importer.google_drive_folder_id=$GOOGLE_DRIVE_FOLDER_ID \
  --set importer.github.token=$GITHUB_TOKEN \
  --set importer.airtable.api_key=$AIRTABLE_API_KEY \
  --set importer.airtable.base_id=$AIRTABLE_BASE_ID \
  --set importer.airtable.table_id=$AIRTABLE_TABLE_ID \
  --set importer.airtable.matching_table_id=$AIRTABLE_MATCHING_TABLE_ID \
  --set importer.airtable.view_name=$AIRTABLE_VIEW_NAME \
  --set api.path_prefix=$ENV_PREFIX/v2 \
  --set legacy.path_prefix=$ENV_PREFIX/api \
  --timeout 15m0s \
  --namespace $K8S_NAMESPACE


echo "Eos upgrade complete"
