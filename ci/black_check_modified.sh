#!/bin/bash

BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
if [ -z "${BRANCH}" ]
then
  BRANCH="main"
fi
echo black check against branch: $BRANCH

LAST_ERROR=0
for filename in $(git diff --name-only "remotes/origin/${BRANCH}...")
do
  if [[ ! -f "$filename" ]]; then
    echo skipping filename $filename, because it does not exist
  elif [[ "${filename: -3}" == ".py" ]]
  then
    poetry run black --check "${filename}" || LAST_ERROR=$?
  else
    echo skipping $filename
  fi
done

exit $LAST_ERROR