#!/bin/sh

# activate k8s context:
aws eks --region eu-central-1 update-kubeconfig --name eos-eks-1

echo "Deleting old job if exists..."
kubectl delete -n $K8S_NAMESPACE --ignore-not-found job/eos-importer-bw

echo "Creating new job..."
kubectl create -n $K8S_NAMESPACE job --from=cronjob/eos-importer-bw eos-importer-bw
kubectl wait -n $K8S_NAMESPACE --for=condition=ready pod --selector=job-name=eos-importer-bw --timeout=600s
kubectl logs -n $K8S_NAMESPACE --follow job/eos-importer-bw

echo "Importer complete... restart eos deployment..."
kubectl rollout restart -n $K8S_NAMESPACE deployment/eos

echo "Done"

