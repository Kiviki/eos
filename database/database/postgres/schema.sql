CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
-- speeding up full-text index search using trigrams
CREATE EXTENSION IF NOT EXISTS "pg_trgm" WITH SCHEMA public;
-- support for full-text search using GIN (Generalized Inverted Index)
CREATE EXTENSION IF NOT EXISTS "btree_gin" WITH SCHEMA public;

CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE public.namespace (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(), -- globally unique
    name TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE public.users (
    user_id uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    email varchar(25) UNIQUE,
    -- TODO: remove this field once we have authentication via Cognito implemented
    password TEXT,
    is_superuser BOOLEAN,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    is_service_account BOOLEAN,
    legacy_api_default_access_group_uid uuid
);

CREATE TABLE public.access_group_node (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT NOT NULL DEFAULT public.uuid_generate_v4() CHECK (xid <> ''),
	type TEXT, -- i.e. 'kitchen' 'producer' 'factory'
    data JSONB,
	namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
	creator uuid REFERENCES public.users(user_id) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT xid_namespace UNIQUE (xid, namespace_uid)
);
-- index for faster lookup by `verify_access_group_creation_by_user()` method
CREATE INDEX access_group_creator_index ON public.access_group_node (xid, creator);

-- adding a foreign key to `legacy_api_default_access_group_uid` column
-- of `user` table
ALTER TABLE public.users
    ADD CONSTRAINT users_legacy_api_default_access_group_uid_fk
        FOREIGN KEY (legacy_api_default_access_group_uid)
            REFERENCES public.access_group_node (uid)
            ON DELETE SET NULL;

CREATE TABLE public.namespace_xid_mappings (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT DEFAULT public.uuid_generate_v4(),
    namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
    UNIQUE(xid, namespace_uid)
);

CREATE TABLE public.term (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT,  -- an optional external id provided by the data source (to allow later updates)
    name TEXT,
    sub_class_of uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    data JSONB,
    access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE SET NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE (access_group_uid, xid)
);

CREATE TABLE public.group_membership(
    user_id uuid REFERENCES public.users (user_id) ON DELETE CASCADE,
    access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE,
    permissions JSONB,
    CONSTRAINT pk_membership PRIMARY KEY (user_id, access_group_uid)
);
-- index for faster lookup by `verify_access_group_membership_by_user()` methods
CREATE INDEX user_group_membership_index ON public.group_membership (user_id, access_group_uid);


CREATE TABLE public.node (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE, -- optional (only for kitchen resources)
    node_type TEXT,  -- "recipe", "ingredient", "supply", "product", etc
    data JSONB,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMPTZ
);
CREATE INDEX node_type_index ON public.node (node_type); -- faster for populating pg_graph_mgr's cache

---------------------------------------------------------------------
--------- Our graph of recipes and products and supplies: -----------
---------------------------------------------------------------------

-- Example: we have a recipe with id=parent_recipe01 with two ingredients id=sub_recipe01, id=sub_recipe02
-- We would have the following rows in the "node" table
-- node:   (uid=parent_recipe01, data={...})
-- node:   (uid=sub_recipe01, data={...})
-- node:   (uid=sub_recipe02, data={...})
-- We would have the following entries in the "edge" table:
-- edge:   (parent_uid=parent_recipe01, child_uid=sub_recipe01)
-- edge:   (parent_uid=parent_recipe01, child_uid=sub_recipe02)

CREATE TABLE public.edge (
    edge_type TEXT,
    parent_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    child_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pkey_link PRIMARY KEY (parent_uid, child_uid, edge_type)
);
CREATE INDEX edge_type_index ON public.edge (edge_type); -- faster for populating pg_graph_mgr's cache

CREATE TABLE public.calculation (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    data_input JSONB,
    data_mutations JSONB,
    data_result JSONB,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

-- polymorphic association table, using "exclusive belongs to" pattern
-- see https://hashrocket.com/blog/posts/modeling-polymorphic-associations-in-a-relational-database
CREATE TABLE public.glossary_link (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    gap_filling_module TEXT,
    linked_term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,         -- link to a Term (used for nutrition)
    linked_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,  -- or link to a Node (used for processes)
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

-- to attach a group of terms (e.g. "dried", "banana") to a nutrition file (term) or a process (node)
CREATE TABLE public.glossary_grouping (
    term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    glossary_link_uid uuid REFERENCES public.glossary_link (uid) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);


CREATE TABLE public.matching (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    gap_filling_module TEXT,
    access_group_uid uuid,
    lang TEXT,
    matching_string TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT uq_gfm_lang_matching_string UNIQUE (gap_filling_module, lang, matching_string)
);

-- association table to allow a matching_string to map to multiple terms
CREATE TABLE public.matching_terms (
    matching_uid uuid REFERENCES public.matching (uid) ON DELETE CASCADE,
    term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    CONSTRAINT pk_matching_terms PRIMARY KEY (matching_uid, term_uid)
);


CREATE TABLE public.access_group_edge (
    parent_access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE,
--     TODO: make this field unique?
    child_access_group_uid uuid REFERENCES public.access_group_node (uid)  ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_group_edge PRIMARY KEY (child_access_group_uid, parent_access_group_uid)
);

CREATE TABLE user_auth_tokens (
  user_id uuid REFERENCES public.users (user_id) ON DELETE CASCADE,
  access_token TEXT UNIQUE DEFAULT public.uuid_generate_v4()::text CHECK (access_token <> ''),
  name TEXT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT pk_user_token_name PRIMARY KEY (user_id, name)
);

CREATE TABLE public.ingredients_declaration_matching (
    declaration_uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    ingredients_declaration TEXT UNIQUE NOT NULL CHECK (ingredients_declaration <> ''),
    fixed_ingredients_declaration TEXT NOT NULL CHECK (fixed_ingredients_declaration <> ''),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
-- creating GIN index for faster text search query performance
CREATE INDEX faulty_ingredients_declaration_index
    ON public.ingredients_declaration_matching
        USING gin(ingredients_declaration);

CREATE TABLE gap_filling_modules_cache (
  gfm_name TEXT,
  cache_key TEXT,
  cache_data JSONB,
  load_on_boot BOOLEAN DEFAULT FALSE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT pk_gfm_cache_key PRIMARY KEY (gfm_name, cache_key)
);