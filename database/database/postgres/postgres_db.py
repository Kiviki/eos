import json
import os.path
import uuid
from typing import TYPE_CHECKING, Optional

import asyncpg
from structlog import get_logger

from core.domain.matching_item import MatchingItem
from core.domain.namespace import Namespace
from core.domain.term import Term
from database.postgres.pg_access_mgr import PostgresAccessMgr
from database.postgres.pg_calculation_mgr import PostgresCalculationMgr
from database.postgres.pg_gfm_cache_mgr import PostgresGfmCacheMgr
from database.postgres.pg_glossary_link_mgr import PgGlossaryLinkMgr
from database.postgres.pg_graph_mgr import PostgresGraphMgr
from database.postgres.pg_ingredients_declaration_mgr import PostgresIngredientsDeclarationMappingMgr
from database.postgres.pg_matching_mgr import PgMatchingMgr
from database.postgres.pg_namespace_mgr import PostgresNamespaceMgr
from database.postgres.pg_product_mgr import PgProductMgr
from database.postgres.pg_term_mgr import PgTermMgr
from database.postgres.pg_users_mgr import PostgresUsersMgr
from database.postgres.settings import PgSettings

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


logger = get_logger()


class PostgresDb:
    """Postgres database."""

    def __init__(self, service_provider: "ServiceProvider") -> None:
        """Initialize the postgres db."""
        self.service_provider = service_provider
        self.pool = None
        self.pg_users_mgr = None
        self.access_group_mgr = None
        self.namespace_mgr = None
        self.gfm_cache_mgr = None
        self.graph_mgr = None
        self.pg_calc_mgr = None
        self.product_mgr = None
        self.pg_term_mgr = None
        self.pg_glossary_link_mgr = None
        self.pg_matching_mgr = None
        self.ingredients_declaration_mapping_mgr = None

    def is_connected(self) -> bool:
        """Check if we are connected to the database."""
        if self.pool:
            return True

        return False

    async def disconnect(self) -> None:
        """Disconnect from the database."""
        await self.pool.close()

        self.pool = None

    async def connect(self, schema: Optional[str] = None) -> None:
        """Connect to the database."""
        if self.pool:
            # make sure we don't recreate connection.
            return

        settings = PgSettings()

        if schema is None:
            schema = settings.POSTGRES_SCHEMA

        logger.info(f"connecting to schema {schema}")

        async def init_connection(conn: asyncpg.connection.Connection) -> None:
            # Set the jsonb encoder, to allow us to easily INSERT and SELECT jsonb fields as python dict.
            # prefix = b'\x01'
            await conn.set_type_codec(
                "jsonb",
                encoder=json.dumps,
                decoder=json.loads,
                schema="pg_catalog",  # needs to be the postgres system schema and not our `public` schema, because the
                # `jsonb` datatype is internally stored by postgres in that system schema.
            )

        self.pool = await asyncpg.create_pool(
            host=settings.POSTGRES_HOST,
            port=settings.POSTGRES_PORT,
            user=settings.POSTGRES_USER,
            database=settings.POSTGRES_DB,
            password=settings.POSTGRES_PASSWORD,
            init=init_connection,
            server_settings={"search_path": schema},
            min_size=settings.POSTGRES_CONNECTIONS,
            max_size=settings.POSTGRES_CONNECTIONS,
        )
        logger.info("pg pool created")

        self.access_group_mgr = PostgresAccessMgr(self.pool)
        self.pg_users_mgr = PostgresUsersMgr(self.pool, self.access_group_mgr)
        self.namespace_mgr = PostgresNamespaceMgr(self.pool, self.access_group_mgr, self.pg_users_mgr)
        self.pg_term_mgr = PgTermMgr(self.pool)
        self.gfm_cache_mgr = PostgresGfmCacheMgr(self.pool)
        self.graph_mgr = PostgresGraphMgr(self.pool, self.service_provider.glossary_service)
        self.pg_calc_mgr = PostgresCalculationMgr(self.pool, self.service_provider.glossary_service)
        self.product_mgr = PgProductMgr(self.pool)
        self.pg_glossary_link_mgr = PgGlossaryLinkMgr(
            self.pool, self.pg_term_mgr, self.graph_mgr, self.service_provider.glossary_service
        )
        self.pg_matching_mgr = PgMatchingMgr(self.pool)
        self.ingredients_declaration_mapping_mgr = PostgresIngredientsDeclarationMappingMgr(self.pool)

    async def clear_db(self, schema: str) -> None:
        """Clear the database.

        :param schema: either 'public' or anything else for testing
        """
        query = f"""
            DO $$ DECLARE
                r RECORD;
            BEGIN
                FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = '{schema}') LOOP
                    EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
                END LOOP;
            END $$;
            """
        await self.pool.execute(query)
        logger.info("success cleared db.")

    async def define_schema(self, schema: str) -> None:
        """Define the schema.

        Lets us swap btw 'public' and 'test' schema, so that tests don't 'pollute' the main db.
        schema: either 'public' or anything else for testing.

        :param schema: either 'public' or anything else for testing
        """
        logger.info("start executing postgres schema definition...")
        my_path = os.path.abspath(os.path.dirname(__file__))

        with open(os.path.join(my_path, "schema.sql"), mode="r") as f:
            db_schema = f.read()

            if schema != "public":
                logger.info(f"using test schema {schema}")
                assert " " not in schema, f"schema name '{schema}' cannot contain spaces"

                # tricky: we want to replace most 'public' occurrences, but NOT public.uuid_generate_v4()
                db_schema = db_schema.replace("public.", f"{schema}.")  # matching on dot as well to be more specific
                db_schema = db_schema.replace(
                    "CREATE SCHEMA IF NOT EXISTS public;", f"CREATE SCHEMA IF NOT EXISTS {schema};"
                )
                db_schema = db_schema.replace(
                    f"{schema}.uuid_generate_v4()", "public.uuid_generate_v4()"
                )  # replace back

            await self.pool.execute(db_schema)
            logger.info("successfully added all tables.")

    async def upsert_namespace_by_name_and_get_access_group_uid(self, namespace_name: str) -> uuid.UUID:
        """Create the namespace if it does not exist yet and returns the access group uid.

        :param namespace_name: the namespace name
        """
        namespace = await self.namespace_mgr.find_by_namespace_name(namespace_name)
        if not namespace:
            settings = PgSettings()
            if namespace_name == settings.EATERNITY_USERNAME:
                access_group = await self.namespace_mgr.seed_namespaces()
            else:
                namespace = Namespace(name=namespace_name)
                (
                    _,
                    _,
                    access_group,
                ) = await self.namespace_mgr.insert_or_update_namespace_wrapper(namespace)
            access_group_uid = access_group.uid
        else:
            access_groups = await self.access_group_mgr.get_all_access_group_ids_by_namespace(namespace.uid)
            assert len(access_groups) == 1
            access_group = access_groups[0]
            access_group_uid = access_group.uid
        return access_group_uid

    async def generate_access_group_uid_dict(self) -> dict[str, uuid.UUID]:
        """Generate access group uid dict."""
        settings = PgSettings()
        namespace_names = {
            "default_eaternity_access_group_uid": settings.EATERNITY_USERNAME,
            "fao_codes_access_group_uid": "FAO Codes Glossary Terms namespace",
            "foodex2_access_group_uid": "FoodEx2 Glossary Terms namespace",
            "eurofir_access_group_uid": "EuroFIR Glossary Terms namespace",
            "gadm_access_group_uid": "GADM Glossary Terms namespace",
            "products_access_group_uid": "Products Glossary Terms namespace",
            "perishability_access_group_uid": "Perishability Glossary Terms namespace",
            "combined_products_mono_products_access_group_uid": "Combined/Mono Products Glossary Terms namespace",
            "subdivision_access_group_uid": "Nutrients Subdivision Glossary Terms namespace",
            "transport_access_group_uid": "Transportation Glossary Terms namespace",
        }
        namespace_access_group_uid_dict = {}
        for namespace_name_key, namespace_official_name in namespace_names.items():
            access_group_uid = await self.upsert_namespace_by_name_and_get_access_group_uid(namespace_official_name)
            if namespace_name_key not in ("products_access_group_uid"):  # we don't use the products namespace currently
                namespace_access_group_uid_dict[namespace_name_key] = access_group_uid
        return namespace_access_group_uid_dict

    async def upsert_glossary_and_matching(
        self,
        schema: str = "public",
        upserted_matchings: Optional[list[MatchingItem]] = None,
        upserted_terms: Optional[list[Term]] = None,
    ) -> None:
        """Upsert glossary and matching.

        :param schema: either 'public' or anything else for testing
        """
        namespace_access_group_uid_dict = await self.generate_access_group_uid_dict()

        # seed glossary
        (
            fao_root_term,
            water_scarcity_root_term,
            critical_product_content,
            gadm_root_term,
            nutrients_root_term,
        ) = await self.pg_term_mgr.seed_glossary(**namespace_access_group_uid_dict, upserted_terms=upserted_terms)

        # seed matching to terms
        await self.pg_matching_mgr.seed_matching(upserted_matchings=upserted_matchings)

        # The following code is only executed when testing, because we don't want to pollute the main db.
        if schema != "public" and schema != "test_e2e":
            impact_assessment_access_group_uid = await self.upsert_namespace_by_name_and_get_access_group_uid(
                namespace_name="Impact Assessment Methods Glossary Term namespace"
            )

            root_glossary_term_uuid = PgSettings().ROOT_GLOSSARY_TERM_UUID
            logger.info("Adding root impact assessments term.")
            impact_assessment_root_term = Term(
                uid=uuid.uuid4(),
                name="Impact Assessments",
                sub_class_of=uuid.UUID(root_glossary_term_uuid),
                xid="Root_Impact_Assessments",
                data={},
                access_group_uid=impact_assessment_access_group_uid,
            )
            await self.pg_term_mgr.upsert_term(impact_assessment_root_term, upserted_terms=upserted_terms)

            # seed sample data
            await self.pg_term_mgr.seed_sample_data(
                namespace_access_group_uid_dict["fao_codes_access_group_uid"],
                namespace_access_group_uid_dict["foodex2_access_group_uid"],
                namespace_access_group_uid_dict["eurofir_access_group_uid"],
                namespace_access_group_uid_dict["gadm_access_group_uid"],
                impact_assessment_access_group_uid,
                fao_root_term,
                water_scarcity_root_term,
                critical_product_content,
                gadm_root_term,
                nutrients_root_term,
                impact_assessment_root_term,
                upserted_terms=upserted_terms,
            )

            # seed sample glossary links
            methane_emission_uid = await self.pg_glossary_link_mgr.seed_glossary_links(
                self.graph_mgr,
                self.pg_term_mgr,
                namespace_access_group_uid_dict["default_eaternity_access_group_uid"],
                namespace_access_group_uid_dict["perishability_access_group_uid"],
                namespace_access_group_uid_dict["fao_codes_access_group_uid"],
                namespace_access_group_uid_dict["foodex2_access_group_uid"],
                namespace_access_group_uid_dict["eurofir_access_group_uid"],
            )

            # seed sample data for matching
            await self.pg_matching_mgr.seed_sample_data()

            await self.product_mgr.bulk_insert_xid_uid_mappings(
                [
                    (
                        PgSettings().EATERNITY_NAMESPACE_UUID,
                        process_key,
                        process_uid,
                    )
                    for process_key, process_uid in {
                        "biosphere3_0795345f-c7ae-410c-ad25-1845784c75f5": methane_emission_uid,
                    }.items()
                ]
            )

            # seed sample data for matching
            await self.pg_matching_mgr.seed_sample_data(upserted_matchings=upserted_matchings)

            # Left in the old format to test backward compatibility with old importer which does not import units.
            SAMPLE_GWP_100 = {
                "0795345f-c7ae-410c-ad25-1845784c75f5": {
                    "name": "Methane, fossil",
                    "compartment": "air",
                    "subcompartment": "unspecified",
                    "amount": 29.7,
                    "unit": "kilogram",
                },
                "349b29d1-3e58-4c66-98b9-9d1a076efd2e": {
                    "name": "Carbon dioxide, fossil",
                    "compartment": "air",
                    "subcompartment": "unspecified",
                    "amount": 1.0,
                    "unit": "kilogram",
                },
                "aa7cac3a-3625-41d4-bc54-33e2cf11ec46": {
                    "name": "Carbon dioxide, fossil",
                    "compartment": "air",
                    "subcompartment": "non-urban air or from high stacks",
                    "amount": 1.0,
                    "unit": "kilogram",
                },
                "baf58fc9-573c-419c-8c16-831ac03203b9": {
                    "name": "Methane Mock, fossil",
                    "compartment": "air",
                    "subcompartment": "unspecified",
                    "amount": 0.0,
                    "unit": "kilogram",
                },
            }
            # WARNING: The values here may be different from the actual GWP 20 values.
            SAMPLE_GWP_20 = {
                "unit": "kg CO2-Eq",
                "data": {
                    "0795345f-c7ae-410c-ad25-1845784c75f5": {
                        "name": "Methane, fossil",
                        "compartment": "air",
                        "subcompartment": "unspecified",
                        "amount": 84.5,
                        "unit": "kilogram",
                    },
                    "349b29d1-3e58-4c66-98b9-9d1a076efd2e": {
                        "name": "Carbon dioxide, fossil",
                        "compartment": "air",
                        "subcompartment": "unspecified",
                        "amount": 1.0,
                        "unit": "kilogram",
                    },
                    "aa7cac3a-3625-41d4-bc54-33e2cf11ec46": {
                        "name": "Carbon dioxide, fossil",
                        "compartment": "air",
                        "subcompartment": "non-urban air or from high stacks",
                        "amount": 1.0,
                        "unit": "kilogram",
                    },
                },
            }
            await self.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
                gfm_name="ImpactAssessmentGapFillingWorker",
                cache_key="ipcc-2013cg.bd5af3f67229a1cc291b8ecb7f316fcf",
                cache_data=SAMPLE_GWP_100,
                load_on_boot=True,
            )
            await self.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
                gfm_name="ImpactAssessmentGapFillingWorker",
                cache_key="ipcc-2013cg.13c1130fb359a78e9b2f59a87653fc37",
                cache_data=SAMPLE_GWP_20,
                load_on_boot=True,
            )

    async def reset_db(self, schema: str = "public") -> None:
        """Clear the database, define the schema and initialize the glossary and the matching."""
        self.pg_term_mgr.insert_instead_of_upsert = True
        await self.clear_db(schema=schema)
        await self.define_schema(schema=schema)
        await self.upsert_glossary_and_matching(schema=schema)

    def get_user_mgr(self) -> PostgresUsersMgr:
        """Get the user manager."""
        return self.pg_users_mgr

    def get_access_group_mgr(self) -> PostgresAccessMgr:
        """Get the access group manager."""
        return self.access_group_mgr

    def get_namespace_mgr(self) -> PostgresNamespaceMgr:
        """Get the namespace manager."""
        return self.namespace_mgr

    def get_gfm_cache_mgr(self) -> PostgresGfmCacheMgr:
        """Get the gfm cache manager."""
        return self.gfm_cache_mgr

    def get_graph_mgr(self) -> PostgresGraphMgr:
        """Get the graph manager."""
        return self.graph_mgr

    def get_calc_mgr(self) -> PostgresCalculationMgr:
        """Get the calculation manager."""
        return self.pg_calc_mgr

    def get_product_mgr(self) -> PgProductMgr:
        """Get the product manager."""
        return self.product_mgr

    def get_term_mgr(self) -> PgTermMgr:
        """Get the term manager."""
        return self.pg_term_mgr

    def get_pg_glossary_link_mgr(self) -> PgGlossaryLinkMgr:
        """Get the glossary link manager."""
        return self.pg_glossary_link_mgr

    def get_pg_matching_mgr(self) -> PgMatchingMgr:
        """Get the matching manager."""
        return self.pg_matching_mgr

    def get_ingredients_declaration_mapping_mgr(self) -> PostgresIngredientsDeclarationMappingMgr:
        """Get the ingredients declaration mapping manager."""
        return self.ingredients_declaration_mapping_mgr
