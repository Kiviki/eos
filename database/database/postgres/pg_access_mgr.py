import uuid
from typing import Optional

import asyncpg
from structlog import get_logger

from api.dto.v2.access_group_dto import AccessGroupId
from core.domain.access_group_node import AccessGroup, AccessGroupEdge
from core.domain.user import PERMISSION_INHERITANCE_MAP, UserGroupRoleEnum, UserPermissions

logger = get_logger()


class PostgresAccessMgr:
    def __init__(self, pool: asyncpg.pool.Pool):
        self.pool = pool

    def get_pool(self) -> asyncpg.pool.Pool:
        return self.pool

    async def get_all_access_group_ids_by_namespace(
        self, namespace_uid: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[AccessGroupId]:
        if conn is None:
            conn = self.pool

        access_groups = await conn.fetch(
            "SELECT uid, xid FROM access_group_node WHERE namespace_uid = $1", namespace_uid
        )

        values = [
            AccessGroupId(
                xid=record.get("xid"),
                uid=str(record.get("uid")),
            )
            for record in access_groups
        ]

        return values

    async def get_all_sub_access_group_ids_by_uid(
        self, access_group_uid: str, depth: int, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list:
        if conn is None:
            conn = self.pool

        access_groups = await conn.fetch(
            """
            WITH RECURSIVE cte (parent_access_group_uid, child_access_group_uid, depth) AS (
                    SELECT parent_access_group_uid, child_access_group_uid, 1
                    FROM access_group_edge
                    WHERE access_group_edge.parent_access_group_uid = $1
                UNION ALL
                    SELECT access_group_edge.parent_access_group_uid, access_group_edge.child_access_group_uid, depth+1
                    FROM access_group_edge
                        JOIN cte r
                            ON access_group_edge.parent_access_group_uid = r.child_access_group_uid
                    WHERE depth < $2
            )
            SELECT access_group_node.xid, access_group_node.uid FROM cte
                LEFT JOIN access_group_node
                    ON cte.child_access_group_uid = access_group_node.uid;
            """,
            access_group_uid,
            depth,
        )

        values = [
            AccessGroupId(
                xid=record.get("xid"),
                uid=str(record.get("uid")),
            )
            for record in access_groups
        ]

        return values

    # TODO: unused -- deprecate?
    async def get_all_created_access_groups_by_user(
        self,
        user_id: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[AccessGroupId]:
        if conn is None:
            conn = self.pool

        access_groups = await conn.fetch("SELECT xid, uid FROM access_group_node WHERE creator = $1", user_id)

        values = [
            AccessGroupId(
                xid=record.get("xid"),
                uid=str(record.get("uid")),
            )
            for record in access_groups
        ]

        return values

    async def verify_access_group_membership_by_user_and_group_uid(
        self,
        group_id: str,
        user_id: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        if conn is None:
            conn = self.pool

        membership = await conn.fetchrow(
            "SELECT access_group_uid FROM group_membership " "WHERE user_id = $1 AND access_group_uid = $2;",
            user_id,
            group_id,
        )

        return bool(membership)

    async def get_by_xid(self, xid: str, namespace_uid: str) -> Optional[AccessGroup]:
        record = await self.pool.fetchrow(
            "SELECT * FROM access_group_node WHERE xid = $1 AND namespace_uid = $2", xid, namespace_uid
        )

        return await self.process_fetched_access_group(record)

    async def get_by_uid(self, uid: str) -> Optional[AccessGroup]:
        record = await self.pool.fetchrow("SELECT * FROM access_group_node WHERE uid = $1", uid)

        return await self.process_fetched_access_group(record)

    @staticmethod
    async def process_fetched_access_group(record: asyncpg.Record) -> None | AccessGroup:
        try:
            row = dict(record)
        except TypeError:
            return None

        access_group = AccessGroup(
            uid=str(row.get("uid")),
            xid=str(row.get("xid")),
            type=row.get("type"),
            data=row.get("data"),
            creator=row.get("creator"),
            namespace_uid=str(row.get("namespace_uid")),
        )
        logger.info("got access_group from db.", access_group=access_group)

        return access_group

    async def upsert_access_group_by_uid(
        self, node: AccessGroup, conn: Optional[asyncpg.connection.Connection] = None
    ) -> (AccessGroup, bool):
        # explicitly generating a UUID here helps avoid the situation
        # where a group's XID and UUID are different
        # TODO: discuss if they can be in a form of 2 different UUIDs
        if node.uid is None:
            node.uid = str(uuid.uuid4())

        if node.xid is None:
            # check if group with this UUID exists so that XID is not overwritten
            fetched_group_by_uid = await self.get_by_uid(node.uid)

            if fetched_group_by_uid:
                node.xid = fetched_group_by_uid.xid
            else:
                node.xid = node.uid

        if conn is None:
            conn = self.pool

        group_uuid = await conn.fetchrow(
            """
            INSERT INTO access_group_node(xid, type, data, namespace_uid, creator)
            VALUES($1, $2, $3::jsonb, $4, $5)
            ON CONFLICT ON CONSTRAINT xid_namespace
            DO UPDATE SET xid = $1, data = $3::jsonb
            RETURNING uid, (xmax != 0) AS is_updated;
            """,
            node.xid,
            node.type,
            node.data,
            node.namespace_uid,
            node.creator,
        )

        is_updated = group_uuid["is_updated"]
        node.uid = str(group_uuid.get("uid"))

        # TODO: refactor dict key names into using some kind of code (?)
        #  instead of long enum strings
        # we add group creator as a member and grant them admin permissions
        permissions = {permission: True for permission in UserGroupRoleEnum.ADMIN}

        await self.upsert_member_into_group_by_group_uid(
            node.creator,
            group_uuid.get("uid"),
            permissions,
        )

        logger.info("saved access group in db.", node=node, group_uuid=group_uuid.get("uid"))

        return node, is_updated

    async def delete_access_group_by_uid(self, uid: str, conn: Optional[asyncpg.connection.Connection] = None) -> None:
        if conn is None:
            conn = self.pool

        await conn.execute(
            "DELETE FROM access_group_node WHERE uid = $1",
            uid,
        )

        logger.info("removed access group in db.", group_uid=uid)

    async def upsert_access_group_edge(
        self, edge: AccessGroupEdge, conn: Optional[asyncpg.connection.Connection] = None
    ) -> None:
        if conn is None:
            conn = self.pool

        await conn.execute(
            """
            INSERT INTO access_group_edge(child_access_group_uid, parent_access_group_uid)
            VALUES($1, $2)
            ON CONFLICT ON CONSTRAINT pk_group_edge DO NOTHING
            """,
            edge.child_access_group_uid,
            edge.parent_access_group_uid,
        )

        logger.info("saved access edge in db.", edge=edge)

    async def get_access_group_edge(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> AccessGroupEdge | None:
        if conn is None:
            conn = self.pool

        record = await conn.fetchrow(
            """
            SELECT *
            FROM access_group_edge
            WHERE child_access_group_uid = $1 AND parent_access_group_uid = $2
            """,
            child_access_group_uid,
            parent_access_group_uid,
        )

        logger.info("received access edge in db.", edge=record)

        try:
            dict(record)
        except TypeError:
            return None

        return AccessGroupEdge(
            child_access_group_uid=str(record.get("child_access_group_uid")),
            parent_access_group_uid=str(record.get("parent_access_group_uid")),
        )

    async def delete_access_group_edge(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        if conn is None:
            conn = self.pool

        result = await conn.execute(
            """
            DELETE
            FROM access_group_edge
            WHERE child_access_group_uid = $1 AND parent_access_group_uid = $2
            """,
            child_access_group_uid,
            parent_access_group_uid,
        )

        # checking if DELETE operation finished successfully
        if result.startswith("DELETE "):
            logger.info(
                "removed access edge in db.",
                parent_access_group_uid=parent_access_group_uid,
                child_access_group_uid=child_access_group_uid,
            )
            return True
        else:
            return False

    # TODO: not sure if we need it.. deprecate?
    async def get_node_types_from_access_group_by_group_uid(
        self, uid: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[dict[str, list]]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            "SELECT uid, node_type FROM node WHERE access_group_uid = $1",
            uid,
        )
        logger.info("found node entries by type in db")

        nodes_by_type = dict()

        for record in records:
            nodes_by_type[record.get("node_type")] = nodes_by_type.get(record.get("node_type"), []) + [
                str(record.get("uid"))
            ]

        return [{"node_type": node_type, "nodes_list": nodes_by_type[node_type]} for node_type in nodes_by_type]

    async def get_all_group_memberships_by_user(
        self, user_id: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[AccessGroupId]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            SELECT xid, access_group_uid FROM group_membership
            LEFT JOIN access_group_node agn on agn.uid = group_membership.access_group_uid
            WHERE user_id = $1
            """,
            user_id,
        )

        # TODO: should we also return permissions per group?
        groups = [
            AccessGroupId(
                xid=record.get("xid"),
                uid=str(record.get("access_group_uid")),
            )
            for record in records
        ]

        return groups

    async def get_all_sub_group_memberships_by_user_and_uid(
        self, access_group_uid: str, depth: int, user_id: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[AccessGroupId]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            WITH RECURSIVE cte (access_group_uid, depth, xid) AS (
                SELECT
                    agn.uid AS access_group_uid,
                    1 AS depth,
                    agn.xid
                FROM
                    access_group_edge age
                    JOIN access_group_node agn ON age.child_access_group_uid = agn.uid
                    LEFT JOIN group_membership parent_gm ON
                        parent_gm.access_group_uid = age.parent_access_group_uid
                        AND
                        parent_gm.user_id = $3
                    LEFT JOIN group_membership child_gm ON
                        child_gm.access_group_uid = age.child_access_group_uid
                        AND
                        child_gm.user_id = $3
                WHERE
                    age.parent_access_group_uid = $1
                    AND (
                        parent_gm.permissions ->> 'child_access_group_read' = 'true'
                        OR
                        child_gm.permissions ->> 'read' = 'true'
                    )

                UNION ALL

                SELECT
                    age.child_access_group_uid,
                    r.depth + 1,
                    agn.xid
                FROM
                    access_group_edge age
                    JOIN cte r ON age.parent_access_group_uid = r.access_group_uid
                    JOIN access_group_node agn ON age.child_access_group_uid = agn.uid
                    LEFT JOIN group_membership parent_gm ON
                        parent_gm.access_group_uid = age.parent_access_group_uid
                        AND
                        parent_gm.user_id = $3
                    LEFT JOIN group_membership child_gm ON
                        child_gm.access_group_uid = age.child_access_group_uid
                        AND
                        child_gm.user_id = $3
                WHERE
                    r.depth < $2
                    AND (
                        parent_gm.permissions ->> 'child_access_group_read' = 'true'
                        OR
                        child_gm.permissions ->> 'read' = 'true'
                    )
            )
            SELECT
                xid,
                access_group_uid AS uid
            FROM
                cte;
            """,
            access_group_uid,
            depth,
            user_id,
        )

        # TODO: should we also return permissions per group?
        groups = [
            AccessGroupId(
                xid=record.get("xid"),
                uid=str(record.get("uid")),
            )
            for record in records
        ]

        return groups

    async def verify_user_service_account_status(
        self, user_id: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> bool:
        if conn is None:
            conn = self.pool

        is_service_account = await conn.fetchval(
            """
            SELECT is_service_account FROM users WHERE user_id = $1;
            """,
            user_id,
        )

        return bool(is_service_account)

    async def upsert_member_into_group_by_group_uid(
        self,
        user_id: str,
        access_group_uid: str,
        permissions: dict,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> None:
        if conn is None:
            conn = self.pool

        # check if target member is a service account
        is_service_account = await self.verify_user_service_account_status(user_id)

        if is_service_account:
            # if they already are in a group -> abort
            if len(await self.get_all_group_memberships_by_user(user_id)) == 1:
                return

        await conn.execute(
            """
            INSERT INTO group_membership (user_id, access_group_uid, permissions)
            VALUES($1, $2, $3::jsonb)
            ON CONFLICT ON CONSTRAINT pk_membership
            DO UPDATE SET permissions = $3::jsonb
            """,
            user_id,
            access_group_uid,
            permissions,
        )

        logger.info("saved membership")

    async def delete_member_from_group_by_group_uid(
        self, user_id: str, access_group_uid: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> None:
        if conn is None:
            conn = self.pool

        # check if target member is a service account
        is_service_account = await self.verify_user_service_account_status(user_id)

        # if they are and their access group is the same as the target one -> abort,
        # since service account must be tied to their original access group
        if is_service_account:
            if self.verify_access_group_membership_by_user_and_group_uid(access_group_uid, user_id):
                return

        await conn.execute(
            """
            DELETE FROM group_membership WHERE user_id = $1 AND access_group_uid = $2
            """,
            user_id,
            access_group_uid,
        )

        logger.info("membership deleted")

    async def get_all_members_by_group_uid(
        self, access_group_uid: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            SELECT user_id, permissions FROM group_membership WHERE access_group_uid = $1
            """,
            access_group_uid,
        )
        members = list()

        for record in records:
            members.append(
                UserPermissions(
                    user_id=str(record.get("user_id")),
                    permissions=record.get("permissions"),
                )
            )

        return members

    async def get_member_permissions(
        self, access_group_uid: str, user_id: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> dict:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            WITH RECURSIVE cte (ag_uid, depth) AS (
                    SELECT $1::uuid, 0
                UNION ALL
                    SELECT access_group_edge.parent_access_group_uid, depth+1
                    FROM access_group_edge
                    JOIN cte r
                    ON access_group_edge.child_access_group_uid = r.ag_uid
                    WHERE depth < 5
            )
            SELECT access_group_uid, permissions FROM group_membership gm
                INNER JOIN cte ON gm.access_group_uid = cte.ag_uid
                WHERE gm.user_id = $2;
            """,
            access_group_uid,
            user_id,
        )

        if records:
            permissions = {}
            for record in records:
                if record.get("access_group_uid") == uuid.UUID(access_group_uid):
                    add_permissions = record.get("permissions")
                else:
                    # we need to join the permisson dictionaries of all parent access_groups together, while setting the
                    # dict entry to True if the user has this permission in any of the parent access_groups:
                    add_permissions = {
                        to_perm: True
                        for from_perm, to_perm in PERMISSION_INHERITANCE_MAP.items()
                        if record.get("permissions").get(from_perm, False)
                    }

                # combine permissions
                permissions = {
                    key: permissions.get(key, False) or add_permissions.get(key, False)
                    for key in set(permissions) | set(add_permissions)
                }

            return permissions
        else:
            return {}
