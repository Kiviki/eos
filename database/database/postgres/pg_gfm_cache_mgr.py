from typing import Optional

import asyncpg
from structlog import get_logger

logger = get_logger()


class PostgresGfmCacheMgr:
    def __init__(self, pool: asyncpg.pool.Pool):
        self.pool = pool

    async def get_cache_entry_by_gfm_name_and_key(self, gfm_name: str, cache_key: str) -> Optional[dict]:
        record = await self.pool.fetchrow(
            "SELECT cache_data FROM gap_filling_modules_cache WHERE gfm_name = $1 AND cache_key = $2",
            gfm_name,
            cache_key,
        )

        if not record:
            return None

        return record["cache_data"]

    async def set_cache_entry_by_gfm_name_and_key(
        self,
        gfm_name: str,
        cache_key: str,
        cache_data: dict,
        load_on_boot: bool = False,
    ) -> bool:
        operation_status = await self.pool.fetchval(
            """
            INSERT INTO gap_filling_modules_cache(gfm_name, cache_key, cache_data, load_on_boot) 
            VALUES($1, $2, $3::jsonb, $4) 
            ON CONFLICT (gfm_name, cache_key)
                DO UPDATE SET cache_data = $3::jsonb, load_on_boot = $4, updated_at = NOW()
            RETURNING cache_data 
            """,
            gfm_name,
            cache_key,
            cache_data,
            load_on_boot,
        )

        if operation_status:
            logger.info(
                "inserted gfm cache entry into db.",
                gfm_name=gfm_name,
                cache_key=cache_key,
            )

            return bool(operation_status)
        else:
            return False

    async def delete_many_cache_entries_by_gfm_name_and_keys(
        self,
        gfm_name: str,
        cache_keys: list[str],
    ):
        await self.pool.executemany(
            """
            DELETE FROM gap_filling_modules_cache WHERE cache_key = $1 AND gfm_name = $2;
            """,
            [(cache_key, gfm_name) for cache_key in cache_keys],
        )

    async def get_prefill_cache_entries(self, gfm_name: str, limit: int) -> Optional[list[dict]]:
        # TODO: use limit here?
        records = await self.pool.fetch(
            """
            SELECT * FROM gap_filling_modules_cache
            WHERE gfm_name = $1 AND load_on_boot IS TRUE
            LIMIT $2
            """,
            gfm_name,
            limit,
        )

        values = [dict(record) for record in records]

        return values
