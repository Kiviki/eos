from typing import Optional

import asyncpg
from structlog import get_logger

from core.domain.calculation import CALCULATION_DATA_INPUT_FIELDS, CALCULATION_DATA_RESULT_FIELDS, Calculation
from core.graph_manager.mutations import mutation_from_dict
from core.service.glossary_service import GlossaryService

logger = get_logger()


class PostgresCalculationMgr:
    """Manager for calculations in the postgres database."""

    def __init__(self, pool: asyncpg.pool.Pool, glossary_service: GlossaryService):
        self.pool = pool
        self.glossary_service = glossary_service

    @staticmethod
    def _convert_pg_record_to_calculation(record: asyncpg.Record) -> Calculation:
        """Convert the dictionary coming from a record in the database to a Calculation object."""
        calculation_dict = {
            "uid": record["uid"],
            "child_of_root_node_uid": record[
                "root_node_uid"
            ],  # FIXME: As a hack, we store child_of_root_node_uid as rood_node_uid in the database.
            **record["data_input"],
            **record["data_result"],
        }
        if record["data_mutations"]:
            calculation_dict["mutations"] = [mutation_from_dict(mutation) for mutation in record["data_mutations"]]
        calculation = Calculation(**calculation_dict)
        return calculation

    @staticmethod
    def _convert_calculation_to_pg_dict(calculation: Calculation) -> dict:
        """Convert a Calculation object to a dictionary that can be stored in the database."""
        calculation_dict = calculation.model_dump(mode="json")
        db_record_dict = {
            "uid": calculation_dict["uid"],
            "root_node_uid": calculation_dict[
                "child_of_root_node_uid"
            ],  # FIXME: As a hack, we store child_of_root_node_uid as rood_node_uid in the database.
            "data_input": {k: calculation_dict[k] for k in CALCULATION_DATA_INPUT_FIELDS},
            "data_mutations": calculation_dict["mutations"] if calculation.return_log else None,
            "data_result": {k: calculation_dict[k] for k in CALCULATION_DATA_RESULT_FIELDS},
        }
        return db_record_dict

    async def find_by_uid(self, uid: str) -> Optional[Calculation]:
        """Find a calculation by its uid."""
        record = await self.pool.fetchrow("SELECT * FROM calculation WHERE uid = $1", uid)
        if record is None:
            return None
        try:
            calculation = self._convert_pg_record_to_calculation(record)
            logger.debug("got calculation from db.")
            return calculation
        except TypeError as e:
            raise ValueError(f"Could not deserialize {record} to calculation: {e}") from e

    async def insert_calculation(self, calculation: Calculation) -> Calculation:
        """Insert a calculation into the database."""
        db_record_dict = self._convert_calculation_to_pg_dict(calculation)

        await self.pool.execute(
            """
            INSERT INTO calculation(uid, root_node_uid, data_input, data_mutations, data_result)
            VALUES($1, $2, $3, $4, $5)
            """,
            db_record_dict["uid"],
            db_record_dict["root_node_uid"],
            db_record_dict["data_input"],
            db_record_dict["data_mutations"],
            db_record_dict["data_result"],
        )

        logger.info("inserted calculation into db.", calculation=calculation)

        return calculation

    async def upsert_by_uid(self, calculation: Calculation) -> Calculation:
        """Upsert a calculation into the database."""
        db_record_dict = self._convert_calculation_to_pg_dict(calculation)

        await self.pool.fetchrow(
            """
            INSERT INTO calculation(
                uid,
                root_node_uid,
                data_input,
                data_mutations,
                data_result,
                updated_at
            )
            VALUES($1, $2, $3, $4, $5, NOW())
            ON CONFLICT (uid)
            DO UPDATE SET
                root_node_uid = $2,
                data_input = $3,
                data_mutations = $4,
                data_result = $5,
                updated_at = NOW()
            RETURNING *;
            """,
            db_record_dict["uid"],
            db_record_dict["root_node_uid"],
            db_record_dict["data_input"],
            db_record_dict["data_mutations"],
            db_record_dict["data_result"],
        )

        logger.debug("upserted calculation into db.")

        return calculation

    # TODO: unused for now
    async def delete_calculation_by_id(self, uid: str) -> None:
        """Delete a calculation by its uid."""
        await self.pool.execute(
            "DELETE FROM calculation WHERE uid = $1",
            uid,
        )
