import hashlib
import uuid
from typing import Optional

import asyncpg
from asyncpg import Record
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.namespace import Namespace
from core.domain.token import Token
from core.domain.user import User
from database.postgres.pg_access_mgr import PostgresAccessMgr
from database.postgres.pg_users_mgr import PostgresUsersMgr
from database.postgres.settings import PgSettings

logger = get_logger()


class PostgresNamespaceMgr:
    def __init__(self, pool: asyncpg.pool.Pool, pg_access_mgr: PostgresAccessMgr, pg_user_mgr: PostgresUsersMgr):
        self.pool = pool
        self.access_mgr = pg_access_mgr
        self.pg_user_mgr = pg_user_mgr

    # TODO: unused for now
    async def find_all(self) -> list[Namespace]:
        result_namespaces = await self.pool.fetch(
            "SELECT * FROM Namespace;",
        )

        if len(result_namespaces) > 0:
            namespaces = []

            for namespace_row in result_namespaces:
                namespace_row = dict(namespace_row)

                namespace = Namespace(
                    uid=str(namespace_row.get("uid")),
                    name=namespace_row.get("name"),
                )
                namespaces.append(namespace)

            return namespaces
        else:
            return []

    # TODO: used only by glossary router unit tests for now
    async def find_by_namespace_uid(self, namespace_uid: str) -> Optional[Namespace]:
        row = await self.pool.fetchrow(
            "SELECT * FROM Namespace WHERE uid = $1",
            namespace_uid,
        )

        return await self.process_namespace_entry(row)

    async def find_by_namespace_name(self, namespace_name: str) -> Optional[Namespace]:
        row = await self.pool.fetchrow(
            "SELECT * FROM Namespace WHERE name = $1",
            namespace_name,
        )

        return await self.process_namespace_entry(row)

    @staticmethod
    async def process_namespace_entry(row: Record) -> Optional[Namespace]:
        logger.info(f"record {row}")

        if row:
            namespace = Namespace(
                uid=str(row.get("uid")),
                name=row.get("name"),
            )
            logger.info("got namespace from db.", namespace=namespace)

            return namespace
        else:
            return None

    async def insert_or_update_namespace_wrapper(self, namespace: Namespace) -> tuple[Namespace, str, AccessGroup]:
        """Wrapper method for creating/updating a namespace and its associated service account user."""
        # create/update a namespace
        namespace = await self.insert_or_update_namespace_by_uid(namespace=namespace)

        # create/update a service account
        _, service_account = await self.insert_or_update_service_account_user(
            namespace_uid=namespace.uid,
            legacy_api_default_access_group_uid=None,
        )

        # create an access group
        default_access_group = AccessGroup(
            uid=str(uuid.UUID(hashlib.md5(namespace.uid.encode("utf-8")).hexdigest())),
            namespace_uid=namespace.uid,
            creator=service_account.user_id,
            data={
                "name": f"Default access group for namespace {namespace.name}",
                # TODO: maybe change this behaviour? users can change this location via service account though
                "location": "CH",
            },
        )

        # post this access group and add service account to it with "Admin" role
        await self.access_mgr.upsert_access_group_by_uid(node=default_access_group)

        # fill service user's `legacy_api_default_access_group_uid` field
        token, service_account = await self.insert_or_update_service_account_user(
            namespace_uid=namespace.uid,
            legacy_api_default_access_group_uid=default_access_group.uid,
        )

        return namespace, token, default_access_group

    async def insert_or_update_namespace_by_uid(self, namespace: Namespace) -> Namespace:
        # TODO: currently in the code this condition can never happen -- remove this if-block?
        if namespace.uid is None:
            namespace.uid = str(uuid.uuid4())

        # inserting/updating a namespace entry
        await self.pool.execute(
            """
            INSERT INTO Namespace(uid, name, updated_at)
            VALUES($1, $2, NOW())
            ON CONFLICT (uid)
            DO UPDATE SET name = $2, updated_at = NOW();
            """,
            namespace.uid,
            namespace.name,
        )

        logger.info("upserted namespace into db.", namespace=namespace)

        return namespace

    async def insert_or_update_service_account_user(
        self, namespace_uid: str, legacy_api_default_access_group_uid: str | None
    ) -> tuple[str, User]:
        # creating a service account user
        # generated user id is based on namespace's id
        service_account_user_id = uuid.UUID(hashlib.md5(namespace_uid.encode("utf-8")).hexdigest())
        service_account_user = User(
            user_id=str(service_account_user_id),
            email=None,
            # TODO: rework this line once we add password-based authentication
            password="",
            is_superuser=False,
            is_service_account=True,
            legacy_api_default_access_group_uid=legacy_api_default_access_group_uid,
        )

        # receiving this user's token
        token = await self.pg_user_mgr.insert_or_update_user(service_account_user)

        logger.info(
            "upserted service account user into db.",
            namespace=namespace_uid,
            legacy_api_default_access_group_uid=legacy_api_default_access_group_uid,
            token=token,
        )

        return token, service_account_user

    async def seed_namespaces(self) -> AccessGroup:
        settings = PgSettings()

        namespace = Namespace(
            name=settings.EATERNITY_USERNAME,
            uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        _, _, default_access_group = await self.insert_or_update_namespace_wrapper(namespace)

        superuser = User(
            user_id=str(uuid.uuid4()),
            email="superuser@eaternity.ch",
            # TODO: rework this line once we add password-based authentication
            password="",
            legacy_api_default_access_group_uid=default_access_group.uid,
            is_superuser=True,
            is_service_account=False,
        )
        await self.pg_user_mgr.insert_or_update_user(superuser)

        updated_token = Token(
            user_id=superuser.user_id,
            token=settings.EATERNITY_TOKEN,
            name="Default",
        )

        # update the access token to be as defined in env variable:
        await self.pg_user_mgr.update_access_token(updated_token)

        return default_access_group
