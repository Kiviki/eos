from typing import Optional

import asyncpg
from structlog import get_logger

from core.domain.matching_item import MatchingItem

logger = get_logger()


class PgMatchingMgr:
    def __init__(self, pool: asyncpg.pool.Pool):
        self.pool = pool

    async def get_matching_items(self, gap_filling_module: str = None) -> list[MatchingItem]:
        QUERY = """
            SELECT
                m.uid,
                m.gap_filling_module,
                m.access_group_uid,
                m.lang,
                m.matching_string,
                ARRAY_AGG(mt.term_uid) AS term_uids
            FROM matching AS m
            INNER JOIN matching_terms AS mt ON m.uid = mt.matching_uid
            {where_clause}
            GROUP BY m.uid, m.gap_filling_module, m.access_group_uid, m.lang, m.matching_string """

        if gap_filling_module is None:
            result_data = await self.pool.fetch(QUERY.format(where_clause=""))
        else:
            result_data = await self.pool.fetch(
                QUERY.format(where_clause="WHERE m.gap_filling_module = $1"), gap_filling_module
            )

        values = [dict(record) for record in result_data]
        if len(values) > 0:
            gfm_data_items = []
            for data_row in values:
                gfm_data = MatchingItem(
                    uid=str(data_row.get("uid")),
                    term_uids=[str(uid) for uid in data_row.get("term_uids")],
                    gap_filling_module=data_row.get("gap_filling_module"),
                    access_group_uid=data_row.get("access_group_uid"),
                    lang=data_row.get("lang"),
                    matching_string=data_row.get("matching_string"),
                )
                gfm_data_items.append(gfm_data)
            return gfm_data_items
        else:
            return []

    async def delete_matching(self, matching_item_uid: str) -> None:
        async with self.pool.acquire() as connection:
            async with connection.transaction():
                await connection.execute(
                    """
                    DELETE FROM matching_terms WHERE matching_uid = $1
                    """,
                    str(matching_item_uid),
                )
                await connection.execute(
                    """
                    DELETE FROM matching WHERE uid = $1
                    """,
                    str(matching_item_uid),
                )

    async def insert_matching(self, matching: MatchingItem) -> MatchingItem:
        """Insert a new matching item into the db. Matching string will be lowercased."""
        async with self.pool.acquire() as connection:
            async with connection.transaction():
                matching_uid = await connection.fetchval(  # insert matching item first
                    """
                    INSERT INTO matching(gap_filling_module, access_group_uid, lang, matching_string)
                    VALUES($1, $2, $3, $4)
                    ON CONFLICT (gap_filling_module, lang, matching_string)
                      DO UPDATE SET access_group_uid = $2, updated_at = NOW()
                    RETURNING uid
                    """,
                    matching.gap_filling_module,
                    matching.access_group_uid,
                    matching.lang,
                    matching.matching_string.lower(),
                )  # <--- lowercased

                # clear all previously matched terms of this matching item:
                await connection.execute(
                    """
                    DELETE FROM matching_terms WHERE matching_uid = $1
                    """,
                    str(matching_uid),
                )

                for term_uid in matching.term_uids:  # then insert in association table
                    await connection.execute(
                        """
                        INSERT INTO matching_terms(matching_uid, term_uid)
                          VALUES($1, $2)
                        ON CONFLICT (matching_uid, term_uid)
                          DO NOTHING
                        """,
                        str(matching_uid),
                        term_uid,
                    )

        return matching

    async def insert_matching_to_terms_xids(
        self,
        gap_filling_module: str,
        access_group_uid: str | None,
        lang: str,
        matching_string: str,
        term_xids: list[str],
        upserted_matchings: Optional[list[MatchingItem]] = None,
    ) -> None:
        # TODO maybe refactor with insert_matching?

        term_uids = []
        async with self.pool.acquire() as connection:
            async with connection.transaction():
                matching_uid = await connection.fetchval(  # insert matching item first
                    """
                    INSERT INTO matching(gap_filling_module, access_group_uid, lang, matching_string)
                    VALUES($1, $2, $3, $4)
                    ON CONFLICT (gap_filling_module, lang, matching_string)
                      DO UPDATE SET access_group_uid = $2, updated_at = NOW()
                    RETURNING uid
                    """,
                    gap_filling_module,
                    access_group_uid,
                    lang,
                    matching_string.lower(),
                )  # <--- lowercased

                # clear all previously matched terms of this matching item:
                await connection.execute(
                    """
                    DELETE FROM matching_terms WHERE matching_uid = $1
                    """,
                    str(matching_uid),
                )

                for term_xid in term_xids:  # then insert in association table
                    records = await connection.fetch(
                        """
                        select uid from term where xid = $1
                        """,
                        term_xid,
                    )
                    term_uid = records[0].get("uid")

                    await connection.execute(
                        """
                        insert into matching_terms(matching_uid, term_uid)
                          values($1, $2)
                        on conflict (matching_uid, term_uid)
                          do nothing
                        """,
                        str(matching_uid),
                        term_uid,
                    )
                    if not isinstance(term_uid, str):
                        term_uids.append(str(term_uid))
                    else:
                        term_uids.append(term_uid)

        if upserted_matchings is not None:
            upserted_matchings.append(
                MatchingItem(
                    gap_filling_module=gap_filling_module,
                    uid=matching_uid,
                    matching_string=matching_string,
                    term_uids=term_uids,
                )
            )

    async def seed_matching(self, upserted_matchings: Optional[list[MatchingItem]] = None) -> None:
        logger.info("start seeding matching...")

        await self.insert_matching_to_terms_xids(
            "CheckProduction", None, "en", "organic", ["P0128"], upserted_matchings=upserted_matchings
        )

        await self.insert_matching_to_terms_xids(
            "CheckProduction", None, "de", "bio", ["P0128"], upserted_matchings=upserted_matchings
        )

        await self.insert_matching_to_terms_xids(
            "CheckProduction", None, "en", "standard", ["EOS_prod_standard"], upserted_matchings=upserted_matchings
        )

        await self.insert_matching_to_terms_xids(
            "CheckProduction", None, "en", "greenhouse", ["EOS_prod_greenhouse"], upserted_matchings=upserted_matchings
        )

        conservation_string_to_tag_term_xid = {
            "fresh": "P0120",
            "not conserved": "J0003",
            "cooled": "J0131",
            "frozen": "J0136",
            "canned": "J0111",
            "dried": "J0116",
        }

        for string, xid in conservation_string_to_tag_term_xid.items():
            await self.insert_matching_to_terms_xids("CheckConservation", None, "en", string, [xid])

        for string in (
            "Bio Suisse",
            "Coop Naturaplan",
            "Rain Forest Alliance certified",
            "Max Havelaar Small Producer Organizations",
            "Max Havelaar Hired labour",
            # UTZ is inapplicable since the critical products (soy and palm oil) do not fall under this certification.
            # "UTZ certified multi-group",
            # "UTZ certified individual and multi-site",
            "Claro",
            "Spar Natur pur Ausland",
            "Migros Bio Ausland + Max Havelaar",
            "Aldi Natur Aktiv Ausland + EU Bio + Max Havelaar",
            "Demeter",
            "Naturland",
            "Wild Fish",
        ):
            if string in ("Rain Forest Alliance certified", "Wild Fish"):
                lang = "en"
            elif string == "Bio Suisse":
                lang = "fr"
            else:
                lang = "de"
            await self.insert_matching_to_terms_xids(
                "CheckLabels",
                None,
                lang,
                string,
                ["EOS_rainforest_conservation_certified"],
                upserted_matchings=upserted_matchings,
            )

        for string in (
            "Migros Bio Ausland",
            "EU Bio",
            "AB Agriculture Biologique",
            "Biotrend Ausland",
            "Aldi Natur Aktiv Ausland",
            "USDA Organic",
        ):
            if string == "AB Agriculture Biologique":
                lang = "fr"
            elif string == "USDA Organic":
                lang = "en"
            else:
                lang = "de"
            await self.insert_matching_to_terms_xids(
                "CheckLabels",
                None,
                lang,
                string,
                ["EOS_certified_rainforest_not_specified"],
                upserted_matchings=upserted_matchings,
            )

        for string in (
            "organic",
            "free-range",
            "grazing",
            "suckler-cow",
            "SAT (Schwein artgerechte Tierhaltung)",
            "RAT (Rind artgerechte Tierhaltung)",
        ):
            if string in ("SAT (Schwein artgerechte Tierhaltung)", "RAT (Rind artgerechte Tierhaltung)"):
                lang = "de"
            else:
                lang = "en"
            await self.insert_matching_to_terms_xids(
                "CheckLabels",
                None,
                lang,
                string,
                ["EOS_animal_welfare_certified"],
                upserted_matchings=upserted_matchings,
            )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "en",
            "data error",
            ["EOS_data_error"],
            upserted_matchings=upserted_matchings,
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "data error",
            ["EOS_data_error"],
            upserted_matchings=upserted_matchings,
        )

    async def seed_sample_data(self, upserted_matchings: Optional[list[MatchingItem]] = None) -> None:
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Karotten",
            ["A1791"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for CARROT
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Kartoffeln",
            ["A00ZT"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for POTATO
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "en",
            "electricity",
            ["EOS_electricity"],
            upserted_matchings=upserted_matchings,  # see PgTermMgr.seed_glossary()
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Tomaten",
            ["A0DMX"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for tomato
        )
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Eier",
            ["A0725"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for eggs
        )
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Hähnchen",
            ["A01SP"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for eggs
        )
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Zwiebeln",
            ["A1480"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for onion
        )
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Apfelsaft",
            ["A039M"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for apple juice
        )
        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "Orangensaft",
            ["A03AM"],
            upserted_matchings=upserted_matchings,  # FoodEx2 xid for orange juice
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "getrockene Kartoffeln",
            ["A00ZT", "J0116"],  # FoodEx2 xid for POTATO & DEHYDRATED OR DRIED
            upserted_matchings=upserted_matchings,
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "getrockene Karotten",
            ["A1791", "J0116"],  # FoodEx2 xid for CARROT & DEHYDRATED OR DRIED
            upserted_matchings=upserted_matchings,
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "neue getrocknete Karotten",
            ["A1791", "A01BS", "J0116"],  # FoodEx2 xid for CARROT & DEHYDRATED OR DRIED (for processing gfm)
        )

        await self.insert_matching_to_terms_xids(
            "MatchProductName",
            None,
            "de",
            "frozen Karotten",
            ["A1791", "J0136"],  # FoodEx2 xid for CARROT & PRESERVED BY FREEZING
            upserted_matchings=upserted_matchings,
        )
