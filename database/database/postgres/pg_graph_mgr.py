from typing import Optional, Type
from uuid import UUID, uuid4

import asyncpg
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import node_class_from_type
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.prop import Prop
from core.domain.props.quantity_prop import RawQuantity
from core.service.glossary_service import GlossaryService

logger = get_logger()


class PostgresGraphMgr:
    def __init__(self, pool: asyncpg.pool.Pool, glossary_service: GlossaryService):
        self.pool = pool
        self.glossary_service = glossary_service

    def get_pool(self) -> asyncpg.pool.Pool:
        return self.pool

    @staticmethod
    def _convert_pg_record_to_node(record: asyncpg.Record) -> Node:
        init_data = {
            "uid": UUID(int=record["uid"].int),
            "access_group_uid": record.get("access_group_uid", None),
            "node_type": record["node_type"],
        } | record["data"]

        node_class = node_class_from_type(node_type=record["node_type"])
        node = node_class(**init_data)
        return node

    @staticmethod
    def _convert_node_to_pg_dict(node: Node) -> dict:
        """Convert a Calculation object to a dictionary that can be stored in the database."""
        node_data_dict = node.model_dump(
            mode="json", exclude_none=True, exclude={"uid", "access_group_uid", "node_type"}
        )
        db_record_dict = {
            "uid": node.uid,
            "access_group_uid": node.access_group_uid,
            "node_type": node.node_type,
            "data": node_data_dict,
        }
        return db_record_dict

    @staticmethod
    def _convert_pg_record_to_edge(record: asyncpg.Record) -> Edge:
        return Edge(
            parent_uid=UUID(int=record["parent_uid"].int),
            child_uid=UUID(int=record["child_uid"].int),
            edge_type=EdgeTypeEnum[record["edge_type"]],
        )

    async def get_nodes_by_type(
        self, node_types: [str], conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Node]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch(
            """
                SELECT *
                FROM node
                WHERE node_type = ANY($1)
                """,
            node_types,
        )
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_lca_links(self, conn: Optional[asyncpg.connection.Connection] = None) -> list[Edge]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch("SELECT * FROM edge WHERE edge_type = 'lca_link'")
        try:
            edges = []

            for record in records:
                edge: Edge = self._convert_pg_record_to_edge(record)
                edges.append(edge)
            return edges
        except TypeError:
            return []

    async def get_nodes_by_access_group_uid(
        self,
        access_group_uid: str,
        node_type: Optional[str] = None,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[Node]:
        if conn is None:
            conn = self.pool

        query = "SELECT * FROM node WHERE access_group_uid = $1"
        args = [access_group_uid]
        if node_type:
            query += " AND node_type = $2"
            args.append(node_type)

        records = await conn.fetch(query, *args)
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_parents_by_uid(self, uid: UUID, conn: Optional[asyncpg.connection.Connection] = None) -> list[Edge]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
                    SELECT child_uid, parent_uid, edge_type
                    FROM edge
                    WHERE edge.child_uid = $1
            """,
            uid,
        )

        return [self._convert_pg_record_to_edge(record) for record in records]

    async def get_sub_graph_by_uid(
        self, uid: UUID, max_depth: int = 10, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            WITH RECURSIVE cte (child_uid, parent_uid, edge_type, depth) AS (
                    SELECT child_uid, parent_uid, edge_type, 1
                    FROM edge
                    WHERE edge.parent_uid = $1
                UNION ALL
                    SELECT edge.child_uid, edge.parent_uid, edge.edge_type, depth+1
                    FROM edge
                    JOIN cte r
                    ON edge.parent_uid = r.child_uid
                    WHERE depth < $2
            )
            SELECT * FROM cte;
            """,
            uid,
            max_depth,
        )

        return [self._convert_pg_record_to_edge(record) for record in records]

    async def find_by_uid(
        self,
        uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
        lock_for_update: bool = False,
        ignore_deleted: bool = False,
    ) -> Optional[Node]:
        if conn is None:
            conn = self.pool

        query = "SELECT * FROM node WHERE uid = $1"
        if lock_for_update:
            query += " FOR UPDATE"

        record = await conn.fetchrow(query, uid)

        if record is None or (record.get("deleted_at") and not ignore_deleted):
            # node does not exist
            return None
        try:
            node = self._convert_pg_record_to_node(record)
            node.is_persistent = True
            logger.debug("got node from db.", node=node)
            return node
        except TypeError as e:
            logger.exception("exception while getting node from db.", e=e)
            return None

    async def find_access_group_uid_by_uid(
        self,
        uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> Optional[str]:
        if conn is None:
            conn = self.pool

        query = "SELECT access_group_uid FROM node WHERE uid = $1"

        record = await conn.fetchrow(query, uid)

        if record is None:
            # node does not exist
            return None
        try:
            if access_group_uid := record["access_group_uid"]:
                return str(access_group_uid)
            else:
                raise TypeError("access_group_uid is set to null.")
        except (TypeError, KeyError) as e:
            logger.exception("exception while getting node from db.", e=e)
            raise e

    # TODO: unused for now
    async def find_by_data(self, query: str) -> Optional[Node]:
        """:param query: a SQL query on the JSONB 'data' field, e.g. '{"raw_input":{"name": "carrot"}}'."""
        record = await self.pool.fetchrow(f"SELECT * FROM node WHERE data @> '{query}'")
        try:
            node = self._convert_pg_record_to_node(record)
            node.is_persistent = True
            logger.debug("got node from db.", node=node)
            return node
        except TypeError:
            return None

    async def upsert_node_by_uid(
        self, node: Node, conn: Optional[asyncpg.connection.Connection] = None, set_as_deleted: bool = False
    ) -> Node:
        if node.uid is None:
            node.uid = uuid4()

        if conn is None:
            conn = self.pool

        node_dict = self._convert_node_to_pg_dict(node)
        if set_as_deleted:
            query = """
            INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
            VALUES($1, $2, $3, $4::jsonb, NOW(), NOW())
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW(), deleted_at = NOW();
            """
        else:
            query = """
            INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
            VALUES($1, $2, $3, $4::jsonb, NOW(), NULL)
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW(), deleted_at = NULL;
            """

        await conn.execute(
            query,
            node_dict["uid"],
            node_dict["access_group_uid"],
            node_dict["node_type"],
            node_dict["data"],
        )
        if not node.is_persistent:
            node.is_persistent = True
        logger.info("saved node in db.", node=node)

        return node

    async def update_node_access_group_uid(
        self,
        node_uid: UUID,
        access_group_uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> str:
        if conn is None:
            conn = self.pool

        node_type = await conn.fetchval(
            """
            UPDATE node
            SET access_group_uid = $2, updated_at = NOW()
            WHERE uid = $1
            RETURNING node_type;
            """,
            node_uid,
            access_group_uid,
        )
        logger.debug("updated access_group_uid of node in db.", node_uid=node_uid)
        return node_type

    async def update_node_prop(
        self,
        node_uid: UUID,
        prop_name: str,
        prop: Prop | list[Prop],
        append: bool = False,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> str:
        if conn is None:
            conn = self.pool

        if isinstance(prop, (DeepListView, list)):
            prop_json = [p.model_dump() for p in prop]
        else:
            prop_json = prop.model_dump()
        if append:
            raise NotImplementedError
        else:
            node_type = await conn.fetchval(
                """
                UPDATE node
                SET data = jsonb_set(data, ('{'||$2||'}')::text[], $3::jsonb), updated_at = NOW()
                WHERE uid = $1
                RETURNING node_type;
                """,
                node_uid,
                prop_name,
                prop_json,
            )
            logger.debug("updated prop of node in db.", node_uid=node_uid)
            return node_type

    async def add_nodes(self, nodes: list[Node]) -> None:
        """(faster) Bulk insert."""
        collected_values = []
        for node in nodes:
            if node.uid is None:
                node.uid = uuid4()
            node_dict = self._convert_node_to_pg_dict(node)
            collected_values.append(
                (
                    node_dict["uid"],
                    node_dict["access_group_uid"],
                    node_dict["node_type"],
                    node_dict["data"],
                )
            )

        await self.pool.executemany(
            """
            INSERT INTO node(uid, access_group_uid, node_type, data)
            VALUES($1, $2, $3, $4::jsonb)
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW();
            """,
            collected_values,
        )

    async def add_edge(self, edge: Edge, conn: Optional[asyncpg.connection.Connection] = None) -> Edge:
        if conn is None:
            conn = self.pool

        if edge.parent_uid is None:
            raise ValueError("need parent_uid")
        if edge.child_uid is None:
            raise ValueError("need child_uid")
        if edge.edge_type is None:
            raise ValueError("need edge_type")

        await conn.execute(
            """
            INSERT INTO edge(parent_uid, child_uid, edge_type, updated_at)
            VALUES($1, $2, $3, NOW())
            ON CONFLICT (parent_uid, child_uid, edge_type)
            DO NOTHING;
            """,
            edge.parent_uid,
            edge.child_uid,
            edge.edge_type.name,
        )
        logger.debug("saved edge in db.", edge=edge)
        return edge

    async def add_edges(self, edges: list[Edge]) -> None:
        """(faster) Bulk insert."""
        collected_values = []
        for edge in edges:
            if edge.parent_uid is None:
                raise ValueError("need parent_uid")
            if edge.child_uid is None:
                raise ValueError("need child_uid")
            if edge.edge_type is None:
                raise ValueError("need edge_type")
            collected_values.append((edge.parent_uid, edge.child_uid, edge.edge_type.name))

        await self.pool.executemany(
            """
            INSERT INTO edge(parent_uid, child_uid, edge_type)
            VALUES($1, $2, $3)
            ON CONFLICT (parent_uid, child_uid, edge_type)
            DO NOTHING;
            """,
            collected_values,
        )

    async def delete_node_and_edges(self, uid: str, conn: Optional[asyncpg.connection.Connection] = None) -> bool:
        if conn is None:
            conn = self.pool

        result_rows = await conn.execute(
            # the following is using on delete cascade, so will also do:
            # DELETE FROM edge WHERE parent_uid = $1 OR child_uid = $1;
            """
            DELETE FROM node WHERE uid = $1;
            """,
            uid,
        )

        # checking if DELETE operation finished successfully
        if result_rows.startswith("DELETE "):
            logger.info("deleted node in db.", uid=uid)
            return True
        else:
            return False

    async def delete_many_nodes_and_edges(self, uids: list[str]) -> None:
        await self.pool.executemany(
            # the following is using on delete cascade, so will also do:
            # DELETE FROM edge WHERE parent_uid = $1 OR child_uid = $1;
            """
            DELETE FROM node WHERE uid = $1;
            """,
            [(uid,) for uid in uids],
        )

    async def seed_sample_processing_data(
        self, methane_emission_uid: UUID, country_codes_for_additional_low_voltage_electricity_processes: list[str]
    ) -> tuple[
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        list[ModeledActivityNode],
    ]:
        """Seed one sample processing Brightway node each for region specific and region unspecified processing."""
        from core.tests.test_processing_gfm import (
            EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY,
            EXPECTED_OTHER_AMOUNT,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_GLO,
            METHANE_EMISSION_UNIT_PROCESSING,
            RAW_MATERIAL_AMOUNT,
            RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY,
        )

        nodes_to_upsert: list[ModeledActivityNode | FlowNode] = []
        edges_to_upsert = []

        def add_flow(
            parent_uid: UUID,
            child_uid: UUID,
            amount: float,
            unit: str,
            node_type: Type[FlowNode],
        ) -> None:
            flow_node = node_type(uid=uuid4(), amount_in_original_source_unit=RawQuantity(value=amount, unit=unit))
            nodes_to_upsert.append(flow_node)
            edges_to_upsert.append(
                Edge(
                    parent_uid=parent_uid,
                    child_uid=flow_node.uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
            )
            edges_to_upsert.append(
                Edge(
                    parent_uid=flow_node.uid,
                    child_uid=child_uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
            )

        region_unspecified_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="average drying process",
            database="EDB",
        )
        nodes_to_upsert.append(region_unspecified_example_process)

        region_unspecified_with_raw_material_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="bread baking",
            database="EDB",
        )
        nodes_to_upsert.append(region_unspecified_with_raw_material_example_process)

        region_specific_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="milk processing, to yoghurt",
            database="EDB",
        )
        nodes_to_upsert.append(region_specific_example_process)

        raw_material_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="Raw material",
            database="EDB",
        )
        nodes_to_upsert.append(raw_material_process)

        low_voltage_electricity_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilowatt hour"},
            name="market group for electricity, low voltage",
            database="ecoinvent 3.6 cutoff",
        )
        nodes_to_upsert.append(low_voltage_electricity_process)

        additional_low_voltage_electricity_processes: list[ModeledActivityNode] = []
        for country_code in country_codes_for_additional_low_voltage_electricity_processes:
            country_specific_electricity_mix = ModeledActivityNode(
                uid=uuid4(),
                activity_location=country_code,
                production_amount={"value": 1.0, "unit": "kilowatt hour"},
                name="market for electricity, low voltage",
                database="ecoinvent 3.6 cutoff",
            )
            nodes_to_upsert.append(country_specific_electricity_mix)
            additional_low_voltage_electricity_processes.append(country_specific_electricity_mix)

        add_flow(
            region_unspecified_with_raw_material_example_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_PROCESSING,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_unspecified_with_raw_material_example_process.uid,
            raw_material_process.uid,
            RAW_MATERIAL_AMOUNT,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_unspecified_example_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_PROCESSING,
            "kilogram",
            FlowNode,
        )
        add_flow(
            low_voltage_electricity_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_GLO,
            "kilogram",
            FlowNode,
        )
        for country_specific_electricity_mix in additional_low_voltage_electricity_processes:
            add_flow(
                country_specific_electricity_mix.uid,
                methane_emission_uid,
                METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC,
                "kilogram",
                FlowNode,
            )
        add_flow(
            region_specific_example_process.uid,
            region_unspecified_example_process.uid,
            EXPECTED_OTHER_AMOUNT,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_specific_example_process.uid,
            low_voltage_electricity_process.uid,
            EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_specific_example_process.uid,
            raw_material_process.uid,
            RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY,
            "kilogram",
            FlowNode,
        )

        await self.add_nodes(nodes_to_upsert)
        await self.add_edges(edges_to_upsert)

        return (
            region_unspecified_example_process,
            region_unspecified_with_raw_material_example_process,
            region_specific_example_process,
            raw_material_process,
            low_voltage_electricity_process,
            additional_low_voltage_electricity_processes,
        )
