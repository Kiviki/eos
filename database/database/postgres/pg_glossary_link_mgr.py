import uuid
from typing import TYPE_CHECKING

import asyncpg
from structlog import get_logger

from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.term import Term
from database.postgres.pg_graph_mgr import PostgresGraphMgr
from database.postgres.pg_term_mgr import PgTermMgr

if TYPE_CHECKING:
    from core.service.glossary_service import GlossaryService


logger = get_logger()


class PgGlossaryLinkMgr:
    def __init__(
        self,
        pool: asyncpg.pool.Pool,
        pg_term_mgr: PgTermMgr,
        pg_graph_mgr: PostgresGraphMgr,
        glossary_service: "GlossaryService",
    ):
        self.pool = pool
        self.glossary_service = glossary_service
        self.pg_term_mgr = pg_term_mgr
        self.pg_graph_mgr = pg_graph_mgr

    async def get_data_of_gfm(self, gap_filling_module: str) -> list[GlossaryLink]:
        assert gap_filling_module, "gap_filling_module must be set"

        result_data = await self.pool.fetch(
            "SELECT * FROM glossary_link WHERE gap_filling_module = $1", gap_filling_module
        )

        values = [dict(record) for record in result_data]

        if len(values) == 0:
            return []
        else:
            gfm_data_items = []

            for data_row in values:
                gfm_data = GlossaryLink(
                    uid=data_row.get("uid"),
                    term_uids=[],  # filled below
                    gap_filling_module=data_row.get("gap_filling_module"),
                    linked_term_uid=data_row.get("linked_term_uid"),
                    linked_node_uid=data_row.get("linked_node_uid"),
                )
                gfm_data_items.append(gfm_data)

            # get the term_uids groupings for each glossary_link & add them to each GlossaryLink object
            groupings = await self.pool.fetch(
                "SELECT term_uid, glossary_link_uid FROM glossary_grouping WHERE glossary_link_uid = any($1::uuid[])",
                [str(glossary_link.uid) for glossary_link in gfm_data_items],
            )
            groupings_dict = {}  # key: glossary_link_uid, value: list of term_uids

            for g in groupings:
                groupings_dict.setdefault(g[1], []).append(g[0])

            for gfm_data_item in gfm_data_items:
                gfm_data_item.term_uids = groupings_dict.get(gfm_data_item.uid, [])

            return gfm_data_items

    async def get_glossary_links(self, term_uids: list[uuid.UUID], gap_filling_module: str) -> list[GlossaryLink]:
        """Get the glossary_links for a list of terms and a gap_filling_module.

        E.g. given the GFM 'MatchProductName'
        and the terms 'dried' and 'banana', this function returns the glossary_links containing the nutrition data.
        """
        groupings = await self.pool.fetch(
            "SELECT term_uid, glossary_link_uid "
            "FROM glossary_grouping "
            "WHERE glossary_link_uid IN ("
            "SELECT glossary_link_uid FROM glossary_grouping WHERE term_uid = any($1::uuid[])"
            ");",
            [str(term_uid) for term_uid in term_uids],
        )

        if len(groupings) == 0:
            return []

        else:
            groupings_dict = {}  # key: glossary_link_uid, value: list of term_uids

            for g in groupings:
                groupings_dict.setdefault(g[1], []).append(g[0])

            glossary_links_data = await self.pool.fetch(
                """SELECT * FROM glossary_link WHERE gap_filling_module = $1 AND uid = any($2::uuid[])""",
                gap_filling_module,
                [str(uid) for uid in groupings_dict.keys()],
            )

            if glossary_links_data is None:
                return []
            else:
                return [
                    GlossaryLink(
                        uid=glossary_link_data.get("uid"),
                        term_uids=groupings_dict.get(glossary_link_data.get("uid"), []),
                        gap_filling_module=glossary_link_data.get("gap_filling_module"),
                        linked_term_uid=glossary_link_data.get("linked_term_uid"),
                        linked_node_uid=glossary_link_data.get("linked_node_uid"),
                    )
                    for glossary_link_data in [dict(d) for d in glossary_links_data]
                ]

    async def insert_glossary_link(self, glossary_link: GlossaryLink) -> GlossaryLink:
        glossary_link_uid = uuid.uuid4() if glossary_link.uid is None else glossary_link.uid

        await self.pool.execute(
            """
            INSERT INTO glossary_link(uid, gap_filling_module, linked_term_uid, linked_node_uid)
            VALUES($1, $2, $3, $4)
            """,
            glossary_link_uid,
            glossary_link.gap_filling_module,
            glossary_link.linked_term_uid,
            glossary_link.linked_node_uid,
        )
        # insert multiple rows into glossary_grouping
        await self.pool.executemany(
            """INSERT INTO glossary_grouping(term_uid, glossary_link_uid) VALUES ($1, $2)""",
            [(term_uid, glossary_link_uid) for term_uid in glossary_link.term_uids],
        )

        return glossary_link

    async def delete_glossary_link(self, term_uids: list[uuid.UUID], gap_filling_module: str) -> None:
        """Deletes a glossary_link from the db. Probably not very efficent, but only used by BWImporter for now."""
        glossary_links = await self.get_glossary_links(term_uids, gap_filling_module)

        valid_glossary_links = [
            str(glossary_link.uid) for glossary_link in glossary_links if set(glossary_link.term_uids) == set(term_uids)
        ]

        if len(glossary_links) > 0 and valid_glossary_links:
            await self.pool.execute("""DELETE FROM glossary_link WHERE uid = any($1::uuid[])""", valid_glossary_links)

            await self.pool.execute(
                """DELETE FROM glossary_grouping WHERE glossary_link_uid = any($1::uuid[])""",
                valid_glossary_links,
            )

    async def delete_glossary_links_by_uids(self, uids: list[str] | list[uuid.UUID]) -> None:
        await self.pool.executemany(
            """
            DELETE FROM glossary_link WHERE uid = $1;
            """,
            [(str(uid),) for uid in uids],
        )

    async def seed_glossary_links(
        self,
        graph_mgr: PostgresGraphMgr,
        term_mgr: PgTermMgr,
        default_eaternity_access_group_uid: str,
        perishability_access_group_uid: str,
        fao_access_group_uid: str,
        foodex2_access_group_uid: str,
        eurofir_access_group_uid: str,
    ) -> uuid.UUID:
        """Seed the glossary_link table with some data for testing purposes."""
        logger.info("start seeding glossary_link...")

        # TODO: Do we need to add these processes here? They are also added in conftest.py

        carrot_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for carrot (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="carrot",
        )
        potato_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for potato (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="potato",
        )
        tomato_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for tomato (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="tomato",
        )
        onion_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GL",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for onion (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="onion",
        )
        egg_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GL",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for egg (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="egg",
        )
        chicken_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GL",
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for egg (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="egg",
        )

        truck_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1, "unit": "tonkm"},
            id="('EDB', '59e1cf02ff66424392fe9b1e893771a2')",
            key=[
                "EDB",
                "59e1cf02ff66424392fe9b1e893771a2",
            ],
            flow=None,
            name="market for transport infrastructure, freight, lorry, unspecified",
            type="process",
            database="EDB",
            filename=None,
            reference_product="transport infrastructure, freight, lorry, unspecified",
        )

        air_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1, "unit": "ton kilometer"},
            key=[
                "EDB",
                "55cebdd5bf3d450d8ca06490f54af5f3",
            ],
            flow=None,
            name="transport, freight, aircraft infrastructure, unspecified distances",
            type="process",
            database="EDB",
            filename=None,
            reference_product="transport, freight, aircraft infrastructure",
        )

        sea_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "ton kilometer"},
            key=[
                "EDB",
                "b9bbf27802ab228a297bf10e06c93059_copy1",
            ],
            flow="2741cea8-327f-4e0f-9401-b10858dc68f8",
            name="transport, freight, sea, container ship infrastructure",
            type="process",
            database="EDB",
            filename="5687441a-90e8-4727-8e19-58013da8a0b9_2741cea8-327f-4e0f-9401-b10858dc68f8.spold",
            reference_product="transport, freight, sea, container ship infrastructure",
        )

        greenhouse_glass_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "square meter-year"},
            key=["ecoinvent 3.6 cutoff", "0ce659c3cfd443a38761058ee62e3f10"],
            flow="ca28222f-2bef-4c4f-b966-6a3e8932fa46",
            name="market for greenhouse, glass walls and roof",
            filename="8ff05636-237c-4fe8-bb46-3a0dcf6b3816_ca28222f-2bef-4c4f-b966-6a3e8932fa46.spold",
            reference_product="greenhouse, glass walls and roof",
        )

        greenhouse_plastic_process = ModeledActivityNode(
            uid=uuid.uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "square meter-year"},
            key=["ecoinvent 3.6 cutoff", "fddfe51c6959f41ac044089c3a892af7"],
            flow="794ea6cc-787f-4993-b8ef-e4b626ae0dcd",
            name="market for greenhouse, plastic walls and roof",
            filename="40202578-9172-4974-810c-4b581769456a_794ea6cc-787f-4993-b8ef-e4b626ae0dcd.spold",
            reference_product="greenhouse, plastic walls and roof",
        )

        methane_emission = ElementaryResourceEmissionNode(
            uid=uuid.uuid4(),
            production_amount={"value": 1.0, "unit": "kilogram"},
            id="('biosphere3', '0795345f-c7ae-410c-ad25-1845784c75f5')",
            key=["biosphere3", "0795345f-c7ae-410c-ad25-1845784c75f5"],
            name="Methane, fossil",
            type="emission",
            categories=["air"],
        )

        # insert processes
        for process_node in (
            carrot_process,
            potato_process,
            tomato_process,
            onion_process,
            truck_process,
            egg_process,
            chicken_process,
            air_process,
            sea_process,
            greenhouse_glass_process,
            greenhouse_plastic_process,
            methane_emission,
        ):
            await self.pg_graph_mgr.upsert_node_by_uid(process_node)

        # insert glossary links
        for process, xid in (
            (carrot_process, "A1791"),
            (potato_process, "A00ZT"),
            (tomato_process, "A0DMX"),
            (onion_process, "A1480"),
            (egg_process, "A0725"),
            (chicken_process, "A01SP"),
            (air_process, "EOS_AIR"),
            (sea_process, "EOS_SEA"),
            (truck_process, "EOS_GROUND"),
        ):
            terms: list[Term] = await self.pg_term_mgr.get_terms_by_xid(xid)

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                    term_uids=[t.uid for t in terms],
                    linked_node_uid=process.uid,
                )
            )

        terms: list[Term] = [(await self.pg_term_mgr.get_terms_by_xid(xid))[0] for xid in ("A1791", "A01BS", "J0116")]
        await self.insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[t.uid for t in terms],
                linked_node_uid=carrot_process.uid,
            )
        )

        # link carrot (process) to methane (emission)
        flow = await graph_mgr.upsert_node_by_uid(
            FlowNode(
                amount_in_original_source_unit={"value": 0.01, "unit": "kilogram"},
                type="biosphere",
            )
        )
        await graph_mgr.add_edge(
            Edge(parent_uid=onion_process.uid, child_uid=flow.uid, edge_type=EdgeTypeEnum.lca_link)
        )
        await graph_mgr.add_edge(
            Edge(parent_uid=flow.uid, child_uid=methane_emission.uid, edge_type=EdgeTypeEnum.lca_link)
        )

        # link nutrient_terms to terms
        for term_xids, nutr_xid in (
            (("A1791",), "131"),
            (
                (
                    "A1791",  # CARROTS (GS1 GPC)
                    "A01BS",  # FRUIT AND FRUIT PRODUCTS (EFSA FOODEX2)
                ),
                "131",
            ),
            (
                (
                    "A1791",  # CARROTS (GS1 GPC)
                    "J0116",  # DEHYDRATED OR DRIED
                ),
                "132",
            ),
            (
                (
                    "A1791",  # CARROTS (GS1 GPC)
                    "J0136",  # PRESERVED BY FREEZING
                ),
                "131",
            ),
            (("A00ZT",), "508"),
            (
                (
                    "A00ZT",
                    "J0116",
                ),
                "508",
            ),
            (("A0DMX",), "491"),
            (("A1480",), "110"),
            (("A0725",), "680"),
            (("A01SP",), "244"),
        ):
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]
            nutr_term = await term_mgr.get_term_by_xid_and_access_group_uid(nutr_xid, eurofir_access_group_uid)

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="Nutrients",
                    term_uids=term_uids,
                    linked_term_uid=nutr_term.uid,
                )
            )

        # link FAO terms to terms
        for term_xids, fao_xid in (
            (("A1791",), "426"),
            (
                (
                    "A1791",
                    "J0116",
                ),
                "426",
            ),
            (
                (
                    "A1791",
                    "J0136",
                ),
                "426",
            ),
            (("A0DMX",), "388"),
        ):
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]

            fao_term = await term_mgr.get_term_by_xid_and_access_group_uid(fao_xid, fao_access_group_uid)

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="FAO",
                    term_uids=term_uids,
                    linked_term_uid=fao_term.uid,
                )
            )

        # link water scarcity terms to terms
        for term_xids, water_scarcity_xid in {
            ("A1791",): "WS_sample_1",
            ("A1791", "J0116"): "WS_sample_1",
            ("A00ZT",): "WS_sample_2",
            ("A00ZT", "J0116"): "WS_sample_2",
            ("A0DMX",): "WS_sample_3",
            ("A1480",): "WS_sample_4",
        }.items():
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]

            water_scarcity_term = await term_mgr.get_term_by_xid_and_access_group_uid(
                water_scarcity_xid, default_eaternity_access_group_uid
            )

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="WaterScarcity",
                    term_uids=term_uids,
                    linked_term_uid=water_scarcity_term.uid,
                )
            )

        # link critical product content terms to terms
        for term_xids, critical_product_content_xid in {
            ("A1791",): "CriticalProduct_sample_1",
            ("A1791", "J0116"): "CriticalProduct_sample_1",
            ("A1480",): "CriticalProduct_sample_2",
        }.items():
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]

            critical_product_content_term = await term_mgr.get_term_by_xid_and_access_group_uid(
                critical_product_content_xid, default_eaternity_access_group_uid
            )

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="CriticalProductContent",
                    term_uids=term_uids,
                    linked_term_uid=critical_product_content_term.uid,
                )
            )

        # link Food category terms to terms
        for term_xids, food_category_xids in {
            ("A1791",): ("EOS_Diet-low-in-vegetables", "EOS_Vegetables", "EOS_Plant-based-products"),
            ("A1791", "J0116"): ("EOS_Diet-low-in-vegetables", "EOS_Vegetables", "EOS_Plant-based-products"),
            ("A00ZT",): (
                "EOS_Plant-based-products",
                "EOS_Vegetables",
            ),
            ("A00ZT", "J0116"): (
                "EOS_Plant-based-products",
                "EOS_Vegetables",
            ),
            ("A0DMX",): ("EOS_Diet-low-in-vegetables", "EOS_Vegetables", "EOS_Plant-based-products"),
            ("A1480",): ("EOS_Diet-low-in-vegetables", "EOS_Vegetables", "EOS_Plant-based-products"),
            ("A0725",): ("EOS_Eggs", "EOS_Animal-products"),
            ("A01SP",): ("EOS_Poultry", "EOS_Meat-products", "EOS_Animal-products"),
        }.items():
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]

            food_category_terms = [
                await term_mgr.get_term_by_xid_and_access_group_uid(
                    food_category_xid, default_eaternity_access_group_uid
                )
                for food_category_xid in food_category_xids
            ]

            for food_category_term in food_category_terms:
                await self.insert_glossary_link(
                    GlossaryLink(
                        gap_filling_module="FoodCategories",
                        term_uids=term_uids,
                        linked_term_uid=food_category_term.uid,
                    )
                )

        # link perishability terms to terms
        for term_xids, perishability_term_xid in {
            ("A1791",): "EOS_HIGH-PERISHABLE",
            ("A1791", "J0116"): "EOS_STABLE",
            ("A00ZT",): "EOS_PERISHABLE",
            ("A00ZT", "J0116"): "EOS_STABLE",
            ("A0DMX",): "EOS_HIGH-PERISHABLE",
            ("A1480",): "EOS_STABLE",
        }.items():
            term_uids = [
                (await term_mgr.get_term_by_xid_and_access_group_uid(term_xid, foodex2_access_group_uid)).uid
                for term_xid in term_xids
            ]

            perishability_term = await term_mgr.get_term_by_xid_and_access_group_uid(
                perishability_term_xid, perishability_access_group_uid
            )

            await self.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="Perishability",
                    term_uids=term_uids,
                    linked_term_uid=perishability_term.uid,
                )
            )

        return methane_emission.uid
