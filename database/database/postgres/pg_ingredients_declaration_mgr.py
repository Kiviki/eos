from typing import Optional

import asyncpg
from structlog import get_logger

from core.domain.ingredients_declaration import IngredientsDeclarationMapping

logger = get_logger()


class PostgresIngredientsDeclarationMappingMgr:
    def __init__(self, pool: asyncpg.pool.Pool):
        self.pool = pool

    async def get_ingredients_declaration_mapping_by_text(
        self,
        declaration_text: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> IngredientsDeclarationMapping | None:
        if conn is None:
            conn = self.pool

        declaration = await conn.fetchrow(
            """
            SELECT ingredients_declaration, fixed_ingredients_declaration, declaration_uid 
            FROM ingredients_declaration_matching 
            WHERE ingredients_declaration ILIKE $1 -- keeping LIKE statement case insensitive
            """,
            declaration_text,
        )

        return (
            IngredientsDeclarationMapping(
                faulty_declaration=declaration.get("ingredients_declaration"),
                fixed_declaration=declaration.get("fixed_ingredients_declaration"),
                # TODO: test this one more time
                uid=str(declaration.get("declaration_uid")),
            )
            if declaration
            else None
        )

    async def get_ingredients_declaration_mapping_by_uid(
        self,
        declaration_uid: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> IngredientsDeclarationMapping | None:
        if conn is None:
            conn = self.pool

        declaration = await conn.fetchrow(
            "SELECT ingredients_declaration, fixed_ingredients_declaration, declaration_uid "
            "FROM ingredients_declaration_matching "
            "WHERE declaration_uid = $1",
            declaration_uid,
        )

        return (
            IngredientsDeclarationMapping(
                faulty_declaration=declaration.get("ingredients_declaration"),
                fixed_declaration=declaration.get("fixed_ingredients_declaration"),
                # TODO: test this one more time
                uid=str(declaration.get("declaration_uid")),
            )
            if declaration
            else None
        )

    async def search_ingredients_declaration_mappings_by_text(
        self,
        declaration_part: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[IngredientsDeclarationMapping]:
        if conn is None:
            conn = self.pool

        # doing this separately since `%$1%` value below causes a syntax error
        declaration_part = f"%{declaration_part}%"

        records = await conn.fetch(
            """
            SELECT ingredients_declaration, fixed_ingredients_declaration, declaration_uid 
            FROM ingredients_declaration_matching 
            WHERE ingredients_declaration ILIKE $1 -- keeping LIKE statement case insensitive
            """,
            declaration_part,
        )

        declaration_objects = [
            IngredientsDeclarationMapping(
                faulty_declaration=declaration.get("ingredients_declaration"),
                fixed_declaration=declaration.get("fixed_ingredients_declaration"),
                # TODO: test this one more time
                uid=str(declaration.get("declaration_uid")),
            )
            for declaration in records
        ]

        return declaration_objects

    async def get_all_ingredients_declaration_mappings(
        self,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[IngredientsDeclarationMapping]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            "SELECT ingredients_declaration, fixed_ingredients_declaration, declaration_uid "
            "FROM ingredients_declaration_matching",
        )

        declaration_objects = [
            IngredientsDeclarationMapping(
                faulty_declaration=declaration.get("ingredients_declaration"),
                fixed_declaration=declaration.get("fixed_ingredients_declaration"),
                # TODO: test this one more time
                uid=str(declaration.get("declaration_uid")),
            )
            for declaration in records
        ]

        return declaration_objects

    async def upsert_ingredients_declaration_mapping(
        self,
        declaration_to_upsert: IngredientsDeclarationMapping,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> IngredientsDeclarationMapping:
        if conn is None:
            conn = self.pool

        record = await conn.fetch(
            """
            INSERT INTO ingredients_declaration_matching(
                ingredients_declaration,
                fixed_ingredients_declaration,
                created_at,
                updated_at
            ) 
            VALUES($1, $2, NOW(), NOW())
            ON CONFLICT (ingredients_declaration)
            DO UPDATE SET fixed_ingredients_declaration = $2, updated_at = NOW()
            RETURNING ingredients_declaration, fixed_ingredients_declaration, declaration_uid;
            """,
            declaration_to_upsert.faulty_declaration,
            declaration_to_upsert.fixed_declaration,
        )

        upserted_declaration = IngredientsDeclarationMapping(
            faulty_declaration=record[0].get("ingredients_declaration"),
            fixed_declaration=record[0].get("fixed_ingredients_declaration"),
            # TODO: test this one more time
            uid=str(record[0].get("declaration_uid")),
        )

        return upserted_declaration

    async def delete_ingredients_declaration_mapping_by_uid(
        self,
        declaration_uid: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        if conn is None:
            conn = self.pool

        result = await conn.execute(
            """
            DELETE FROM ingredients_declaration_matching WHERE declaration_uid = $1
            """,
            declaration_uid,
        )

        # checking if DELETE operation finished successfully
        if result.startswith("DELETE "):
            logger.info(
                "removed ingredients declaration mapping in db.",
                declaration_uid=declaration_uid,
            )
            return True
        else:
            return False

    async def delete_ingredients_declaration_mapping_by_text(
        self,
        declaration: str,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        if conn is None:
            conn = self.pool

        result = await conn.execute(
            """
            DELETE FROM ingredients_declaration_matching 
            WHERE ingredients_declaration LIKE $1 -- keeping LIKE statement case insensitive
            """,
            declaration,
        )

        # checking if DELETE operation finished successfully
        if result.startswith("DELETE "):
            logger.info(
                "removed ingredients declaration mapping in db.",
                declaration=declaration,
            )
            return True
        else:
            return False
