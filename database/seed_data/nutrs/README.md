## how this data got in:

    git checkout https://github.com/Eaternity/eaternity-edb-data.git
    cd nutrs
    
    cp 508_Potato__peeled__raw-nutr.json                eos/database/seed_data/nutrs/
    cp 131_Carrot__raw-nutr.json                        eos/database/seed_data/nutrs/
    cp 491_Tomato__raw-nutr.json                        eos/database/seed_data/nutrs/
    cp 110_Onion__raw-nutr.json                         eos/database/seed_data/nutrs/
    cp 732_Cocoa_powder_partially_defatted-nutr.json    eos/database/seed_data/nutrs/
    cp 733_Cocoa_powder_low_fat-nutr.json               eos/database/seed_data/nutrs/