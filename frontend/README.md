# EOS frontend

## Setup

To install dependencies run:

```bash
npm ci
```

## Developing

To start a development server:

```bash
npm run dev
```


## Building

To create a production version:

```bash
npm run build
```

## Preview

To preview the production build:

```bash
npm run preview
```