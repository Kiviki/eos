import type { PageLoad } from './$types';
import { dev } from '$app/environment';

// we don't need any JS on this page, though we'll load
// it in dev so that we get hot module replacement
export const csr = dev;

// since there's no dynamic data here, we can prerender
// it so that it gets served as a static asset in production
export const prerender = true;

interface Term {
    uid: string;
    xid: string;
    name: string;
    sub_class_of: string;
}

interface TreeTerm {
    id: string;
    text: string;
    children?: Array<TreeTerm>;
}

export const load: PageLoad = async ({ fetch }) => {

    const url = `http://localhost:8040/v2/glossary/subtree/?depth=2&term_uid=1fd259be-9057-44d0-92cb-175d79a3d0e6`;
    const response = await fetch(url);

    const root_terms = await response.json();

    let term_tree: Array<TreeTerm> = [];

    const terms_by_id: Record<string, TreeTerm> = {};
    root_terms.terms.forEach((term: Term) => {
        let new_tree_node = {
            id: term.uid,
            text: term.name
        };

        if (term.sub_class_of == "1fd259be-9057-44d0-92cb-175d79a3d0e6") {
            term_tree.push(new_tree_node)
        }
        else {
            let parent_node = terms_by_id[term.sub_class_of];
            if (parent_node) {
                if (parent_node.children == null) {
                    parent_node.children = [];
                }
                parent_node.children.push(new_tree_node);
            }
        }
        terms_by_id[term.uid] = new_tree_node;
    });

    return {
        term_tree: term_tree
    };
}